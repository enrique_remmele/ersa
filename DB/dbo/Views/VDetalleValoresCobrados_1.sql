﻿CREATE View [dbo].[VDetalleValoresCobrados]
As

--CREDITO
--Efectivo
Select
--'Fecha'=Convert(DateTime,E.Fec),
'Fecha'=Convert(DateTime, C.FechaEmision),
E.IDTipoComprobante,
--'TipoComprobante'=E.CodigoComprobante,
'TipoComprobante'=(Case when C.AnticipoCliente = 'False' then E.CodigoComprobante else concat(E.TipoComprobante,' - ANTICIPO') end),
'T. Comp.'=E.TipoComprobante,
'IDBanco'=0,
'Banco'='---',
'Comprobante'=E.Comprobante,
'Emision'=Convert(DateTime,E.Fecha),
'FechaPago'=NULL,
'IDCliente'=C.IDCliente,
'Cliente'=C.Cliente,
'Referencia'=C.Referencia,
'Importe'=E.ImporteMoneda,
'Equivalente' = E.ImporteMoneda * E.Cotizacion,
'Cobranza'=C.Numero,
'Planilla'=C.NroPlanilla,
'Recibo'=C.Comprobante,
C.IDCobrador,
C.Cobrador,
C.IDSucursal,
C.IDCiudad,
C.IdSucursal as IdSucursalOperacion,
--select * from tipocomprobante where id = 128
--Moneda
E.IDMoneda,
E.Moneda,
E.Cotizacion,
M.Decimales,
C.IDUsuario,
'ImporteTotalValor' = E.ImporteMoneda,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante,
'IDTransaccionCobranza'=C.IDTransaccion
,'AnticipoCliente'=Isnull(C.AnticipoCliente,'False')
,'RENDICION'=(Case when C.AnticipoCliente = 'True' and C.IDTipoComprobante = 128 then (Case when C.CANCELADO = 'False' then 'PENDIENTE DE RENDICION' else 'RENDIDO' end) else 'RENDIDO' end)
From VEfectivo E
Join VCobranzaCredito C On E.IDTransaccion=C.IDTransaccion
Join Moneda M On E.IDMoneda=M.ID

Union All

--Cheque Cliente
Select
'Fecha'=Convert(DateTime, C.FechaEmision),
'IDTipoComprobante'=0,
--'TipoComprobante'=E.CodigoTipo,
'TipoComprobante'=(Case when C.AnticipoCliente = 'False' then E.CodigoTipo else concat(E.CodigoTipo,' - ANTICIPO') end),
'T. Comp.'=(Case When (E.Diferido) = 'True' Then 'CHEQUE DIFERIDO' Else 'CHEQUE AL DIA' End),
'IDBanco'=E.IDBanco,
'Banco'=E.CodigoBanco,
'Comprobante'=Concat(E.NroCheque,' - ',E.numero),
'Emision'=Convert(DateTime, E.Fecha),
'FechaPago'=(Case When (E.Diferido) = 'True' Then Convert(DateTime, E.FechaVencimiento) Else NULL End),
'IDCliente'=E.IDCliente,
'Cliente'=E.Cliente,
'Referencia'=E.CodigoCliente,
'Importe'=FP.ImporteCheque, --OBSERVACION: VER GUARDAR IMPORTE UTILIZADO DEL CHEQUE EN SU MONEDA DE OPERACION EN TABLA FORMA DE PAGO
'Equivalente' = FP.ImporteCheque * isnull(FP.Cotizacion,1),
'Cobranza'=C.Numero,
'Planilla'=C.NroPlanilla,
'Recibo'=C.Comprobante,
C.IDCobrador,
C.Cobrador,
C.IDSucursal,
C.IDCiudad,
C.IdSucursal as IdSucursalOperacion,

--Moneda
E.IDMoneda,
E.Moneda,
E.Cotizacion,
M.Decimales,
C.IDUsuario,
'ImporteTotalValor' = E.ImporteMoneda,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante
,'IDTransaccionCobranza'=C.IDTransaccion
,'AnticipoCliente'=Isnull(C.AnticipoCliente,'False')
,'RENDICION'=(Case when C.AnticipoCliente = 'True' and C.IDTipoComprobante = 128 then (Case when C.CANCELADO = 'False' then 'PENDIENTE DE RENDICION' else 'RENDIDO' end) else 'RENDIDO' end)
From VChequeCliente E
Join FormaPago FP On E.IDTransaccion=FP.IDTransaccionCheque
Join VCobranzaCredito C On FP.IDTransaccion=C.IDTransaccion
Join Moneda M On E.IDMoneda=M.ID

Union All

--Documento
Select
'Fecha'=Convert(DateTime, C.FechaEmision),
'IDTipoComprobante'=FPD.IDTipoComprobante,
--'TipoComprobante'=FPD.CodigoComprobante,
'TipoComprobante'=(Case when C.AnticipoCliente = 'False' then FPD.CodigoComprobante else concat(FPD.CodigoComprobante,' - ANTICIPO') end),
'T. Comp.'=FPD.TipoComprobante,
'IDBanco'=isnull((FPD.IdBanco),0),
'Banco'=ISnull(FPD.BAnco,'---'),
'Comprobante'=FPD.Comprobante,
'Emision'=Convert(DateTime, FPD.Fecha),
'FechaPago'=NULL,
'IDCliente'=C.IDCliente,
'Cliente'=C.Cliente,
'Referencia'=C.Referencia,
'Importe'=FP.ImporteMonedaDocumento,
'Equivalente' = FP.ImporteMonedaDocumento * isnull(FPD.Cotizacion,1),
'Cobranza'=C.Numero,
'Planilla'=C.NroPlanilla,
'Recibo'=C.Comprobante,
C.IDCobrador,
C.Cobrador,
C.IDSucursal,
C.IDCiudad,
C.IdSucursal as IdSucursalOperacion,

--Moneda
FPD.IDMoneda,
FPD.Moneda,
FPD.Cotizacion,
M.Decimales,
C.IDUsuario,
'ImporteTotalValor' = FP.ImporteMonedaDocumento,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante
,'IDTransaccionCobranza'=C.IDTransaccion
,'AnticipoCliente'=Isnull(C.AnticipoCliente,'False')
,'RENDICION'=(Case when C.AnticipoCliente = 'True' and C.IDTipoComprobante = 128 then (Case when C.CANCELADO = 'False' then 'PENDIENTE DE RENDICION' else 'RENDIDO' end) else 'RENDIDO' end)
From VCobranzaCredito C
Join FormaPago FP On C.IDTransaccion=FP.IDTransaccion
Join VFormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join Moneda M On FPD.IDMoneda=M.ID

Union All

--Tarjeta
Select
'Fecha'=Convert(DateTime, C.FechaEmision),
'IDTipoComprobante'=FPD.IDTipoComprobante,
--'TipoComprobante'=FPD.TipoComprobante,
'TipoComprobante'=(Case when C.AnticipoCliente = 'False' then FPD.TipoComprobante else concat(FPD.TipoComprobante,' - ANTICIPO') end),
'T. Comp.'=FPD.TipoComprobante,
'IDBanco'=0,
'Banco'='---',
'Comprobante'=FPD.Comprobante,
'Emision'=Convert(DateTime, FPD.Fecha),
'FechaPago'=NULL,
'IDCliente'=C.IDCliente,
'Cliente'=C.Cliente,
'Referencia'=C.Referencia,
'Importe'=FPD.ImporteMoneda,
'Equivalente' = FPD.Importe * FPD.Cotizacion,
'Cobranza'=C.Numero,
'Planilla'=C.NroPlanilla,
'Recibo'=C.Comprobante,
C.IDCobrador,
C.Cobrador,
C.IDSucursal,
C.IDCiudad,
C.IdSucursal as IdSucursalOperacion,

--Moneda
FPD.IDMoneda,
FPD.Moneda,
FPD.Cotizacion,
M.Decimales,
C.IDUsuario,
'ImporteTotalValor' = FPD.ImporteMoneda,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante
,'IDTransaccionCobranza'=C.IDTransaccion
,'AnticipoCliente'=Isnull(C.AnticipoCliente,'False')
,'RENDICION'=(Case when C.AnticipoCliente = 'True' and C.IDTipoComprobante = 128 then (Case when C.CANCELADO = 'False' then 'PENDIENTE DE RENDICION' else 'RENDIDO' end) else 'RENDIDO' end)
From VCobranzaCredito C
Join FormaPago FP On C.IDTransaccion=FP.IDTransaccion
Join FormaPagoTarjeta FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join Moneda M On FPD.IDMoneda=M.ID

Union All

--COBRANZA LOTE
--Efectivo
Select
--'Fecha'=Convert(DateTime,E.Fecha),
'Fecha'=Convert(DateTime, C.Fecha),
E.IDTipoComprobante,
'TipoComprobante'=E.CodigoComprobante,
'T. Comp.'=E.TipoComprobante,
'IDBanco'=0,
'Banco'='---',
'Comprobante'=E.Comprobante,
'Emision'=Convert(DateTime,E.Fecha),
'FechaPago'=NULL,
'IDCliente'=0,
'Cliente'='COBRANZA POR LOTE',
'Referencia'='',
'Importe'=E.ImporteMoneda,
'Equivalente' = E.Importe,
'Cobranza'=C.Numero,
'Planilla'=C.NumeroLote,
'Recibo'=C.Comprobante,
'IDCobrador'=0,
'Cobrador'='',
C.IDSucursal,
C.IDCiudad,
C.IdSucursal as IdSucursalOperacion,

--Moneda
E.IDMoneda,
E.Moneda,
E.Cotizacion,
M.Decimales,
C.IDUsuario,
'ImporteTotalValor' = E.ImporteMoneda,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante
,'IDTransaccionCobranza'=C.IDTransaccion
,'AnticipoCliente'='False'
,'RENDIDO'
From VEfectivo E
Join VCobranzaContado C On E.IDTransaccion=C.IDTransaccion
Join Moneda M On E.IDMoneda=M.ID

Union All

--Cheque Cliente
Select
'Fecha'=Convert(DateTime, C.Fecha),
'IDTipoComprobante'=0,
'TipoComprobante'=E.CodigoTipo,
'T. Comp.'=(Case When (E.Diferido) = 'True' Then 'CHEQUE DIFERIDO' Else 'CHEQUE AL DIA' End),
'IDBanco'=E.IDBanco,
'Banco'=E.CodigoBanco,
'Comprobante'=Concat(E.NroCheque,' - ',E.numero),
'Emision'=Convert(DateTime, E.Fecha),
'FechaPago'=(Case When (E.Diferido) = 'True' Then Convert(DateTime, E.FechaVencimiento) Else NULL End),
'IDCliente'=E.IDCliente,
'Cliente'=E.Cliente,
'Referencia'=E.CodigoCliente,
'Importe'=FP.ImporteCheque,
'Equivalente' = FP.ImporteCheque * Isnull(FP.Cotizacion,1),
'Cobranza'=C.Numero,
'Planilla'=C.NumeroLote,
'Recibo'=C.Comprobante,
'IDCobrador'=0,
'Cobrador'='',
C.IDSucursal,
C.IDCiudad,
C.IdSucursal as IdSucursalOperacion,

--Moneda
E.IDMoneda,
E.Moneda,
E.Cotizacion,
M.Decimales,
C.IDUsuario,
'ImporteTotalValor' = E.ImporteMoneda,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante
,'IDTransaccionCobranza'=C.IDTransaccion
,'False'
,'RENDIDO'
From VChequeCliente E
Join FormaPago FP On E.IDTransaccion=FP.IDTransaccionCheque
Join VCobranzaContado C On FP.IDTransaccion=C.IDTransaccion
Join Moneda M On E.IDMoneda=M.ID

--Union All

----Documento
--Select
--'Fecha'=Convert(DateTime, C.Fecha),
--'IDTipoComprobante'=FPD.IDTipoComprobante,
--'TipoComprobante'=FPD.CodigoComprobante,
--'T. Comp.'=FPD.CodigoComprobante,
--'IDBanco'=0,
--'Banco'='---',
--'Comprobante'=FPD.Comprobante,
--'Emision'=Convert(DateTime, FPD.Fecha),
--'FechaPago'=NULL,
--'IDCliente'=VV.IDCliente,
--'Cliente'=VV.Cliente,
--'Referencia'=VV.ReferenciaCliente,
--'Importe'=FP.ImporteMonedaDocumento,
--'Equivalente' = FP.ImporteMonedaDocumento * isnull(FPD.Cotizacion,1),
--'Cobranza'=C.Numero,
--'Planilla'=C.NumeroLote,
--'Recibo'=C.Comprobante,
--'IDCobrador'='',
--'Cobrador'='',
--C.IDSucursal,
--C.IDCiudad,
--C.IdSucursal as IdSucursalOperacion,

----Moneda
--FPD.IDMoneda,
--FPD.Moneda,
--FPD.Cotizacion,
--M.Decimales,
--C.IDUsuario,
--'ImporteTotalValor' = Fp.ImporteMonedaDocumento

--From VCobranzaContado C
--Join FormaPago FP On C.IDTransaccion=FP.IDTransaccion
--Join VFormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
--Join Moneda M On FPD.IDMoneda=M.ID
--join VFormaPagoDocumentoRetencion FPDR on FPD.IDtransaccion = FPDR.IDTransaccion and FP.ID = FPDR.ID
--join VVenta VV on FPDR.IDTransaccionVenta = VV.IDTransaccion
--Where FPD.IDTipoComprobante = (Select Top(1) Cf.CobranzaCreditoTipoComprobanteRetencion from Configuraciones Cf where Cf.IDSucursal = C.IDSucursal )

Union All

--Documento
Select
'Fecha'=Convert(DateTime, C.Fecha),
'IDTipoComprobante'=FPD.IDTipoComprobante,
'TipoComprobante'=FPD.CodigoComprobante,
'T. Comp.'=FPD.CodigoComprobante,
'IDBanco'=0,
'Banco'='---',
'Comprobante'=FPD.Comprobante,
'Emision'=Convert(DateTime, FPD.Fecha),
'FechaPago'=NULL,
'IDCliente'='',
'Cliente'='COBRANZA POR LOTE',
'Referencia'='',
'Importe'=FP.ImporteMonedaDocumento,
'Equivalente' = FP.ImporteMonedaDocumento * isnull(FPD.Cotizacion,1),
'Cobranza'=C.Numero,
'Planilla'=C.NumeroLote,
'Recibo'=C.Comprobante,
'IDCobrador'='',
'Cobrador'='',
C.IDSucursal,
C.IDCiudad,
C.IdSucursal as IdSucursalOperacion,

--Moneda
FPD.IDMoneda,
FPD.Moneda,
FPD.Cotizacion,
M.Decimales,
C.IDUsuario,
'ImporteTotalValor' = Fp.ImporteMonedaDocumento,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante
,'IDTransaccionCobranza'=C.IDTransaccion
,'False'
,'RENDIDO'
From VCobranzaContado C
Join FormaPago FP On C.IDTransaccion=FP.IDTransaccion
Join VFormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join Moneda M On FPD.IDMoneda=M.ID
--Where FPD.IDTipoComprobante <> (Select Top(1) Cf.CobranzaCreditoTipoComprobanteRetencion from Configuraciones Cf where Cf.IDSucursal = C.IDSucursal )

Union All
--select * from VFormaPagoDocumento where IDTransaccion = 625650
--select * from VCobranzaContado where NumeroLote = '12543'
--Select * From VDetalleValoresCobrados where Cobranza = '12543' 

--Tarjeta
Select
'Fecha'=Convert(DateTime, C.Fecha),
'IDTipoComprobante'=FPD.IDTipoComprobante,
'TipoComprobante'=FPD.TipoComprobante,
'T. Comp.'=FPD.TipoComprobante,
'IDBanco'=0,
'Banco'='---',
'Comprobante'=FPD.Comprobante,
'Emision'=Convert(DateTime, FPD.Fecha),
'FechaPago'=NULL,
'IDCliente'='',
'Cliente'='COBRANZA POR LOTE',
'Referencia'='',
'Importe'=FP.ImporteMonedaTarjeta,
'Equivalente' = FP.Importe * FPD.Cotizacion,
'Cobranza'=C.Numero,
'Planilla'=C.NumeroLote,
'Recibo'=C.Comprobante,
'IDCobrador'='',
'Cobrador'='',
C.IDSucursal,
C.IDCiudad,
C.IdSucursal as IdSucursalOperacion,

--Moneda
FPD.IDMoneda,
FPD.Moneda,
FPD.Cotizacion,
M.Decimales,
C.IDUsuario,
'ImporteTotalValor' = FP.ImporteMonedaTarjeta,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante
,'IDTransaccionCobranza'=C.IDTransaccion
,'False'
,'RENDIDO'
From VCobranzaContado C
Join FormaPago FP On C.IDTransaccion=FP.IDTransaccion
Join FormaPagoTarjeta FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join Moneda M On FPD.IDMoneda=M.ID






















﻿

CREATE View [dbo].[VCobranzaContado]
As

Select 

--Cabecera
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
S.IDCiudad,
CC.Numero,
CC.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
CC.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
CC.NroComprobante,
'Comprobante'=CC.NroComprobante,
CC.Fecha,
'Fec'=CONVERT(varchar(50), CC.Fecha, 6),
'FechaEmision'=CONVERT(varchar(50), CC.Fecha, 6),
CC.Observacion,
CC.Anulado,

--Totales
CC.Total,

--Lote
CC.IDTransaccionLote,
'NumeroLote'=LD.Numero,
'ComprobanteLote'=LD.Comprobante,
'FechaLote'=convert(varchar(50),LD.Fecha,5),
'Distribuidor'=LD.Distribuidor,

--Transaccion
CC.IDTransaccion,
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'Estado'=Case When CC.Anulado='True' Then 'Anulado' Else '---' End,
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=CC.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=CC.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=CC.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=CC.IDTransaccion),
CC.IDMoneda,
CC.Cotizacion,
'Moneda'= (select Referencia from Moneda where ID = CC.IDMoneda)

From CobranzaContado CC
Join Transaccion T On CC.IDTransaccion=T.ID
Join VLoteDistribucion LD On CC.IDTransaccionLote=LD.IDTransaccion
Left Outer Join TipoComprobante TC On CC.IDTipoComprobante=TC.ID
Left Outer Join Sucursal S On CC.IDSucursal=S.ID










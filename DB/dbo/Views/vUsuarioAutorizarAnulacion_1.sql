﻿create view vUsuarioAutorizarAnulacion
as 
select
UAA.ID,
UAA.IDUsuario,
U.Nombre,
UAA.PeriodoFueraDelMes,
UAA.Estado
from UsuarioAutorizarAnulacion UAA
join Usuario U on UAA.IDUsuario = U.ID

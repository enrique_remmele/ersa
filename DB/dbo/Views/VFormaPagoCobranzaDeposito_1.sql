﻿
CREATE View [dbo].[VFormaPagoCobranzaDeposito]
As
--Cheques 
Select distinct FP.IDTransaccion,FP.TipoComprobante, FP.Banco,Formapago,
FP.Comprobante,FP.IDMoneda, FP.Moneda, CCL.ImporteMoneda, 
Cambio, FP.IDTRansaccionCheque, FP.FechaFormapago, FP.Observacion,
'Operacion'= (case when CO.Numero is not null then concat('COB.LOTE - ',CO.Suc,'-',CO.Numero,'-',FP.Observacion) 
				  when CC.Numero is not null then concat('COBRANZA - ',CC.Suc,'-',CC.Numero, '-',FP.Observacion) end), 
'Rechazo'= PCC.Operacion,
'FechaCobranza'= (case when CO.Numero is not null then CO.fecha
				  when CC.Numero is not null then CC.fechaEmision
				  end),
'Deposito'= (Case when DB.Numero is not null then concat(DB.Suc,'-',DB.Numero,' ',DB.ReferenciaBanco,'-',DB.Cuenta,' - BD-', DB.NroComprobante) else '- -' end),
'FechaDeposito'=DB.Fecha,
'IDCliente'= IsNull(CCL.IdCliente,0),
CCL.IDSucursal,
'ImporteCobrado'=FP.Importe
from vFormaPago FP 
Join VChequeCliente CCL on CCL.IdTransaccion = FP.IDTransaccionCheque
left Join VCobranzaCredito CC on CC.IDTransaccion = FP.IDTransaccion
Left Join VCobranzaContado CO on CO.IDTransaccion = FP.IDTransaccion
Left Join VDetallePagoChequeCliente PCC on PCC.IDTransaccion = FP.IDTransaccionCheque
Left Join DetalleDepositoBancario DDB on DDB.IDTransaccionCheque = FP.IDTransaccionCheque
Left Join VDepositoBancario DB on DB.IDTransaccion = DDB.IDTransaccionDepositoBancario

where Cheque = 1

--UNION ALL

----Cheques 
--Select distinct CCL.IDTransaccion,'TipoComprobante'='CHQ', CCL.Banco,'Formapago'='Cheque',
--'Comprobante'=CCL.NroCheque,CCL.IDMoneda, CCL.Moneda, CCL.ImporteMoneda, 
--'Cambio'=CCL.COtizacion, 'IDTRansaccionCheque'=CCL.IDTransaccion, 'FechaFormapago'=cast(CCL.Fecha as date), 'Observacion'='',
--'Operacion'= '', 
--'Rechazo'= PCC.Operacion,
--'FechaCobranza'= CCL.FechaCobranza,
--'Deposito'= concat(DB.Suc,'-',DB.Numero,' ',DB.ReferenciaBanco,'-',DB.Cuenta),
--'FechaDeposito'=DB.Fecha,
--'IDCliente'= IsNull(CCL.IdCliente,0),
--CCL.IDSucursal
--from VChequeCliente CCL
--Left Join VDetallePagoChequeCliente PCC on PCC.IDTransaccion = CCL.IDTransaccion
--Left Join DetalleDepositoBancario DDB on DDB.IDTransaccionCheque = CCL.IDTransaccion
--Left Join VDepositoBancario DB on DB.IDTransaccion = DDB.IDTransaccionDepositoBancario
--Where CCL.IDTransaccion not in (select IDTransaccion from FormaPago where Cheque is not null)

UNION ALL

--Efectivo
Select FP.IDTransaccion,FP.TipoComprobante, FP.Banco,Formapago,
FP.Comprobante,FP.IDMoneda, FP.Moneda, FP.ImporteMoneda, 
Cambio, FP.IDTRansaccionCheque, FP.FechaFormapago, FP.Observacion,
'Operacion'= (case when CO.Numero is not null then concat('COB.LOTE - ',CO.Suc,'-',CO.Numero) 
				  when CC.Numero is not null then concat('COBRANZA - ',CC.Suc,'-',CC.Numero,'-',CC.Cliente) end), 
'Rechazo'='--',
'FechaCobranza'= (case when CO.Numero is not null then CO.fecha
				  when CC.Numero is not null then CC.fechaEmision
				  end),
'Deposito'= (Case when DB.Numero is not null then concat(DB.Suc,'-',DB.Numero,' ',DB.ReferenciaBanco,'-',DB.Cuenta,' - BD-', DB.NroComprobante) else '- -' end),
'FechaDeposito'=DB.Fecha,
'IDCliente'= IsNull(CC.IdCliente,0),
E.IDSucursal,
'ImporteCobrado'=FP.Importe
from vFormaPago FP 
Join Efectivo E on E.IDTransaccion = FP.IDTransaccion
left Join VCobranzaCredito CC on CC.IDTransaccion = FP.IDTransaccion
Left Join VCobranzaContado CO on CO.IDTransaccion = FP.IDTransaccion
Left Join VPagoChequeCliente PCC on PCC.IDTransaccion = FP.IDTransaccion
Left Join DetalleDepositoBancario DDB on DDB.IDTransaccionEfectivo = FP.IDTransaccion
Left Join VDepositoBancario DB on DB.IDTransaccion = DDB.IDTransaccionDepositoBancario

where Efectivo = 1 and FP.FormaPago <> 'Tarjeta'

UNION ALL


--Documentos y Tarjetas
Select FP.IDTransaccion,FP.TipoComprobante, FP.Banco,Formapago,
'Comprobante'=concat(FP.TipoComprobante,' - ',FP.Comprobante),
FP.IDMoneda, FP.Moneda, FP.ImporteMoneda, 
Cambio, FP.IDTRansaccionCheque, FP.FechaFormapago, FP.Observacion,
'Operacion'= (case when CO.Numero is not null then concat('COB.LOTE - ',CO.Suc,'-',CO.Numero) 
				  when CC.Numero is not null then concat('COBRANZA - ',CC.Suc,'-',CC.Numero,'-',CC.Cliente) end), 
'Rechazo'='--',
'FechaCobranza'= (case when CO.Numero is not null then CO.fecha
				  when CC.Numero is not null then CC.fechaEmision
				  end),
'Deposito'= (Case when DB.Numero is not null then concat(DB.Suc,'-',DB.Numero,' ',DB.ReferenciaBanco,'-',DB.Cuenta,' - BD-', DB.NroComprobante) else '- -' end),
'FechaDeposito'=DB.Fecha,
'IDCliente'= IsNull(CC.IdCliente,0),
FPD.IDSucursal,
'ImporteCobrado'=FP.Importe
from vFormaPago FP 
Join vFormaPagoDocumento FPD on FPD.IDTransaccion = FP.IDTransaccion and FP.ID = FPD.ID
left Join VCobranzaCredito CC on CC.IDTransaccion = FP.IDTransaccion 
Left Join VCobranzaContado CO on CO.IDTransaccion = FP.IDTransaccion
Left Join VPagoChequeCliente PCC on PCC.IDTransaccion = FP.IDTransaccion
Left Join DetalleDepositoBancario DDB on DDB.IDTransaccionDocumento = FP.IDTransaccion and DDB.IDDetalleDocumento = FP.ID
Left Join VDepositoBancario DB on DB.IDTransaccion = DDB.IDTransaccionDepositoBancario

where (Documento = 1  and FPD.Deposito = 1
and FP.IDTransaccion not in (select IDTransaccion from OrdenPago))

UNION ALL

-- Tarjetas
Select FP.IDTransaccion,FP.TipoComprobante, FP.Banco,Formapago,
'Comprobante'=concat(FP.TipoComprobante,' - ',FP.Comprobante),
FP.IDMoneda, FP.Moneda, FP.ImporteMoneda, 
Cambio, FP.IDTRansaccionCheque, FP.FechaFormapago, FP.Observacion,
'Operacion'= (case when CO.Numero is not null then concat('COB.LOTE - ',CO.Suc,'-',CO.Numero) 
				  when CC.Numero is not null then concat('COBRANZA - ',CC.Suc,'-',CC.Numero,'-',CC.Cliente) end), 
'Rechazo'='--',
'FechaCobranza'= (case when CO.Numero is not null then CO.fecha
				  when CC.Numero is not null then CC.fechaEmision
				  end),
'Deposito'= (Case when DB.Numero is not null then concat(DB.Suc,'-',DB.Numero,' ',DB.ReferenciaBanco,'-',DB.Cuenta,' - BD-', DB.NroComprobante) else '- -' end),
'FechaDeposito'=DB.Fecha,
'IDCliente'= IsNull(CC.IdCliente,0),
FPD.IDSucursal,
'ImporteCobrado'=FP.Importe
from vFormaPago FP 
Join vFormaPagoTarjeta FPD on FPD.IDTransaccion = FP.IDTransaccion and FP.ID = FPD.ID
left Join VCobranzaCredito CC on CC.IDTransaccion = FP.IDTransaccion 
Left Join VCobranzaContado CO on CO.IDTransaccion = FP.IDTransaccion
Left Join VPagoChequeCliente PCC on PCC.IDTransaccion = FP.IDTransaccion
Left Join DetalleDepositoBancario DDB on DDB.IDTransaccionTarjeta = FP.IDTransaccion and DDB.IDDetalleDocumento = FP.ID
Left Join VDepositoBancario DB on DB.IDTransaccion = DDB.IDTransaccionDepositoBancario

where FP.IDTransaccion not in (select IDTransaccion from OrdenPago)















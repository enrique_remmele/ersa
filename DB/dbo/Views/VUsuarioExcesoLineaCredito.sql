﻿
CREATE View [dbo].[VUsuarioExcesoLineaCredito]
As

Select  distinct 
U.ID,
U.Nombre,
U.Usuario,
U.[Password],

--Perfil
U.IDPerfil,
'Perfil'=IsNull(P.Descripcion, '---'),

U.Identificador,
U.Estado,
U.Email,

--Vendedor
'EsVendedor'=IsNull(U.EsVendedor, 'False'),
U.IDVendedor,
'Vendedor'=IsNull(V.Nombres, '---'),

--Chofer
'EsChofer'=IsNull(U.EsChofer, 'False'),
U.IDChofer,
'Chofer'=IsNull(C.Nombres, '---'),
'VerCosto'=IsNull(P.VerCosto, 'False')

From Usuario U
Left Outer Join Perfil P On U.IDPerfil=P.ID
Left Outer Join Vendedor V On U.IDvendedor=V.ID
Left Outer Join Chofer C On U.IDChofer=C.ID
Join vAccesoPerfil AEP On P.ID= AEP.IDPerfil
Join VMenu M on M.NombreControl = AEP.NombreControl
where ((M.tag = 'frmPorcentajeExcesoLineaCredito') or (M.Tag = 'frmAprobarExcesoLineaCredito') or (P.ID = 1))
and U.Estado = 1









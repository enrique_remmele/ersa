﻿CREATE view [dbo].[VDetalleNotaCreditoVentaAsociada]
as 
Select 
'VentaIDTransaccion' = V.IDTransaccion,
'VentaComprobante'=V.Comprobante,
'VentaFechaEmision'=V.FechaEmision,
'VentaTotal'=V.Total,
'VentaSaldo'=V.Saldo,
NC.IDPuntoExpedicion,
NC.PE,
NC.Ciudad,
NC.IDCiudad,
NC.IDSucursal,
NC.Sucursal,
NC.ReferenciaSucursal,
NC.ReferenciaPunto,

--Cliente
NC.IDCliente,
NC.idtipocliente,
NC.Cliente,
NC.Direccion,
NC.Telefono,
NC.Referencia,
NC.RUC ,
NC.IDEstado, 
NC.Estado,
NC.IDZonaVenta, 
--'ConComprobantes'=Case When (Select Top(1) IDTransaccionVenta From DetalleNotaCredito D Where D.IDTransaccion=N.IDTransaccion And D.IDTransaccion Is Not Null) Is Not Null Then 'True' Else 'False' End,
NC.ConComprobantes,
NC.IDCiudadCliente,

NC.IDTransaccion,
NC.IDTipoComprobante,
NC.DescripcionTipoComprobante,
NC.TipoComprobante,
NC.[Cod.],
NC.NroComprobante,
NC.Comprobante,


NC.IDDeposito,
NC.Deposito,
NC.Suc,

NC.Fecha,
NC.Fec,
NC.Credito,
NC.CreditoContado,
NC.Condicion,
NC.IDMoneda,
NC.Moneda,
NC.DescripcionMoneda,
NC.Cotizacion,

NC.Observacion,
NC.Aplicar ,
NC.Descuento ,
NC.Devolucion ,
NC.Tipo,
NC.Anulado,
NC.Procesado,
NC.[Estado NC],

--Totales
NC.Total,
NC.TotalImpuesto,
NC.TotalDiscriminado,
NC.Saldo,
NC.Aplicado,

--Importe a Aplicar
NCV.Importe,

--Transaccion
NC.FechaTransaccion,
NC.IDDepositoTransaccion,
NC.IDSucursalTransaccion,
NC.IDTerminalTransaccion,
NC.IDUsuario,
NC.Usuario,

--Anulacion
NC.IDUsuarioAnulacion,
NC.UsuarioAnulacion,
NC.UsuarioIdentificacionAnulacion,
NC.FechaAnulacion,

--Mes y Año
NC.DiaP,
NC.MesP,
NC.Año,
NC.IDMotivo,
NC.Motivo,
NC.IDSucursalCliente,
NC.IDTipoProducto,
NC.IDSubMotivoNotaCredito,

NC.SubMotivo,
NC.NroPedidoNC,
NC.UsuarioPedido,
NC.FechaPedido


from VNotaCredito NC
join NotaCreditoVenta NCV on NC.IDTransaccion = NCV.IDTransaccionNotaCredito
join Venta V on NCV.IDTransaccionVentaGenerada = V.IDTransaccion
group by V.IDTransaccion, V.Comprobante, V.FechaEmision, V.Total, V.Saldo,NC.IDPuntoExpedicion,
NC.PE,
NC.Ciudad,
NC.IDCiudad,
NC.IDSucursal,
NC.Sucursal,
NC.ReferenciaSucursal,
NC.ReferenciaPunto,

--Cliente
NC.IDCliente,
NC.idtipocliente,
NC.Cliente,
NC.Direccion,
NC.Telefono,
NC.Referencia,
NC.RUC ,
NC.IDEstado, 
NC.Estado,
NC.IDZonaVenta, 
--'ConComprobantes'=Case When (Select Top(1) IDTransaccionVenta From DetalleNotaCredito D Where D.IDTransaccion=N.IDTransaccion And D.IDTransaccion Is Not Null) Is Not Null Then 'True' Else 'False' End,
NC.ConComprobantes,
NC.IDCiudadCliente,

NC.IDTransaccion,
NC.IDTipoComprobante,
NC.DescripcionTipoComprobante,
NC.TipoComprobante,
NC.[Cod.],
NC.NroComprobante,
NC.Comprobante,


NC.IDDeposito,
NC.Deposito,
NC.Suc,

NC.Fecha,
NC.Fec,
NC.Credito,
NC.CreditoContado,
NC.Condicion,
NC.IDMoneda,
NC.Moneda,
NC.DescripcionMoneda,
NC.Cotizacion,

NC.Observacion,
NC.Aplicar ,
NC.Descuento ,
NC.Devolucion ,
NC.Tipo,
NC.Anulado,
NC.Procesado,
NC.[Estado NC],

--Totales
NC.Total,
NC.TotalImpuesto,
NC.TotalDiscriminado,
NC.Saldo,
NC.Aplicado,
NCV.Importe,

--Transaccion
NC.FechaTransaccion,
NC.IDDepositoTransaccion,
NC.IDSucursalTransaccion,
NC.IDTerminalTransaccion,
NC.IDUsuario,
NC.Usuario,

--Anulacion
NC.IDUsuarioAnulacion,
NC.UsuarioAnulacion,
NC.UsuarioIdentificacionAnulacion,
NC.FechaAnulacion,

--Mes y Año
NC.DiaP,
NC.MesP,
NC.Año,
NC.IDMotivo,
NC.Motivo,
NC.IDSucursalCliente,
NC.IDTipoProducto,
NC.IDSubMotivoNotaCredito,

NC.SubMotivo,
NC.NroPedidoNC,
NC.UsuarioPedido,
NC.FechaPedido


﻿











CREATE  View [dbo].[VNotaCreditoTransferencia]
As
Select



--Cabecera
N .IDCliente,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
N.IDSucursal,
'Cliente'=C.RazonSocial,

'Ciudad'=S.CodigoCiudad,
N.IDTransaccion,
N.IDTipoComprobante,
N.Numero,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
N.NroComprobante,
'Comprobante'=N.NroComprobante,


N .Fecha,
'Fec'=CONVERT(varchar(50), N .Fecha, 6),
N .IDMoneda,
'Moneda'=M.Referencia,
N .Cotizacion, 
N.Observacion,



--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'Usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario)

----Anulacion
--'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=N.IDTransaccion), 0)),
--'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=N.IDTransaccion), '---')),
--'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=N.IDTransaccion), '---')),
--'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=N.IDTransaccion)


From TransferenciaNotaCredito   N
Join Transaccion T On N.IDTransaccion=T.ID
Left Outer Join TipoComprobante TC On N.IDTipoComprobante=TC.ID
Left Outer Join Cliente C On N.IDCliente=C.ID
Left Outer Join VSucursal S On N.IDSucursal=S.ID
Left Outer Join Moneda M On N.IDMoneda=M.ID
















﻿




CREATE View [dbo].[VCFDepositoBancario]
As
Select
V.*,
'BuscarEncuentaBancaria'=IsNull(CFV.BuscarEncuentaBancaria, 'False'),
'Efectivo'=IsNull(CFV.Efectivo, 'False'),
'ChequeAlDia'=IsNull(CFV.ChequeAlDia, 'False'),
'ChequeDiferido'=IsNull(CFV.ChequeDiferido, 'False'),
'ChequeRechazado'=IsNull(CFV.ChequeRechazado, 'False'),
'DiferenciaCambio'=IsNull(CFV.DiferenciaCambio, 'False'),
'Documento'=IsNull(CFV.Documento, 'False'),
CFV.TipoCuenta,
'Tipo'=Case When CFV.TipoCuenta = 'True' Then 'CAJA AHORRO' Else 'CTA. CTE.' End

From VCF V
Join CFDepositoBancario CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID








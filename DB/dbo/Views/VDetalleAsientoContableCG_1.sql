﻿CREATE View [dbo].[VDetalleAsientoContableCG]

As

Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'=A.Detalle,
D.Debito,
D.Credito,
'Tipo'=A.TipoOperacion,
'Observacion'=D.Observacion,
D.Orden,
D.IDCentroCosto,
'CentroCosto'=D.CentroCosto,
'CodigoCentroCosto'=D.CodigoCentroCosto

From VDetalleAsientoCG D
Join VAsientoCG A on D.Numero=A.Numero
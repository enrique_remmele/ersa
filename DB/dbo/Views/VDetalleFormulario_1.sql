﻿
CREATE View  [dbo].[VDetalleFormulario]
As

Select 

DF.IDFormulario,
DF.IDDetalleFormulario,

DF.NombreCampo,
DF.Campo,
DF.PosicionX,
DF.PosicionY,

--Tipo de Letra
'TipoLetra'=IsNull(DF.TipoLetra, 'Arial'),
'TamañoLetra'=IsNull(DF.TamañoLetra, 8),
'Formato'=IsNull(DF.Formato, 'Normal'),
'Valor'=IsNull(DF.Valor, ''),

DF.Detalle,

'Numerico'=IsNull(DF.Numerico, 'False'),
'Alineacion'=IsNull(DF.Alineacion, 'Izquierda'), --Izquierda, Derecha, Centrada 
'Largo'=IsNull(DF.Largo, 0),
'PuedeCrecer'=IsNull(DF.PuedeCrecer, 'False'),
'Lineas'=IsNull(DF.Lineas, 0)

From DetalleFormulario DF

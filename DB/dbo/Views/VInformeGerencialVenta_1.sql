﻿CREATE View [dbo].[VInformeGerencialVenta]
As
Select V.IDTransaccion,
'Fecha'=V.FechaEmision,
'Dia'= Day(V.FechaEmision),
'Mes'= Month(V.FechaEmision),
'Año'= Year(V.FechaEmision),
V.IDSucursal,
V.IDVendedor,
DV.IDTipoProducto,
DV.IDUnidadMedida,
DV.IDProducto,
V.IDListaPrecio,
DV.Cantidad,
V.IDMoneda,
'Importe'= (Case when V.CancelarAutomatico = 0 then (DV.TotalDiscriminado * V.Cotizacion) else 0 end),
V.Sucursal,
V.Vendedor,
DV.TipoProducto,
DV.UnidadMedida,
DV.Producto,
'ListaPrecio'=V.[Lista de Precio],
V.Moneda,
'Meta'=M.Importe,
'Peso'= DV.Cantidad * P.Peso,
MetaKg
from vDetalleVenta DV
Join VVenta V on V.IDTransaccion = DV.IDTransaccion
Join Producto P on P.ID = DV.IDProducto
Join Meta M on M.IDSucursal = DV.IDSucursal and M.IDTipoProducto = DV.IDTipoProducto and
				M.IDUnidadMedida = DV.IDUnidadMedida and M.IDProducto = DV.IDProducto and
				M.IDListaPrecio = V.IDListaPrecio and Month(V.FechaEmision) = M.Mes and Year(V.FechaEmision) = M.Año
and V.Anulado = 'False'

Union all

Select V.IDTransaccion,
V.Fecha,
'Dia'= Day(V.Fecha),
'Mes'= Month(V.Fecha),
'Año'= Year(V.Fecha),
V.IDSucursal,
'IDVendedor'=(Case when IDSucursalCliente = 0 then (Select IDVendedor From Cliente where ID = V.IDCliente)
				else (Select IDVendedor from ClienteSucursal where ID = V.IDSucursalCliente and IDCliente = V.IDCliente) end),
DV.IDTipoProducto,
DV.IDUnidadMedida,
DV.IDProducto,
'IDListaPrecio'=(Case when IDSucursalCliente = 0 then (Select IDListaPrecio From Cliente where ID = V.IDCliente)
				else (Select IDListaPrecio from ClienteSucursal where ID = V.IDSucursalCliente and IDCliente = V.IDCliente) end),
'Cantidad' = Cantidad * (-1), 
V.IDMoneda,
'Importe'= (DV.TotalDiscriminado * V.Cotizacion) * (-1),
V.Sucursal,
'Vendedor'=(Case when IDSucursalCliente = 0 then (Select Vendedor From vCliente where ID = V.IDCliente)
				else (Select Vendedor from vClienteSucursal where ID = V.IDSucursalCliente and IDCliente = V.IDCliente) end),
DV.TipoProducto,
DV.UnidadMedida,
DV.Producto,
'ListaPrecio'=(Case when IDSucursalCliente = 0 then (Select ListaPrecio From vCliente where ID = V.IDCliente)
				else (Select ListaPrecio from vClienteSucursal where ID = V.IDSucursalCliente and IDCliente = V.IDCliente) end),
V.Moneda,
'Meta'=M.Importe,
'Peso'= DV.Cantidad * P.Peso * (-1),
MetaKg
from VDetalleNotaCredito DV
Join VNotaCredito V on V.IDTransaccion = DV.IDTransaccion
Join Producto P on P.ID = DV.IDProducto
Join Meta M on M.IDSucursal = DV.IDSucursal and M.IDTipoProducto = DV.IDTipoProducto and
				M.IDUnidadMedida = DV.IDUnidadMedida and M.IDProducto = DV.IDProducto and
				--M.IDListaPrecio = V.IDListaPrecio 
				Month(V.Fecha) = M.Mes and Year(V.Fecha) = M.Año
and V.Anulado = 'False'









﻿CREATE View [dbo].[VPlanCuenta] 
As 
Select 
ID,
Descripcion,
Resolucion173,
Titular,
Estado,
'Activo'=Case When Estado = 'True' Then '---' Else 'INACTIVO' End,
'Tipo'=Case When Titular = 'False' Then '---' Else 'TITULAR' End
From PlanCuenta

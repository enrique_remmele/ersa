﻿CREATE View [dbo].[VVentasPendientes]

As

Select
V.IDCliente,
V.Cliente,
V.IDTransaccion,
'Sel'='False',
V.Comprobante,
'Tipo'=V.TipoComprobante,
V.Condicion,
V.Credito,
V.FechaEmision,
V.Fec,
'Vencimiento'=V.[Fec. Venc.],
V.FechaVencimiento,
V.Total,
V.Cobrado,
V.Descontado,
V.Acreditado ,
V.Saldo,
'SaldoGs'= 0,
'Importe'=V.Saldo,
'ImporteGs'=0,
'Cancelar'='False',
V.IDMoneda,
V.Moneda,
V.Cotizacion,
V.IDSucursal,
V.IDTipoComprobante,
V.TotalImpuesto,
'CotizacionHoy'=isnull((Select top(1)(cotizacion) from VCotizacion where IDMoneda = V.IDmoneda order by fecha Desc),1)

From VVenta V
Where V.Cancelado = 'False'
And V.Anulado='False'



















﻿




CREATE View [dbo].[VExistenciaDepositoInventario]
As

Select

--Sergio 15-03-2014

EDI.*,
'Fisico'=EDI.Equipo1 + EDI.Averiados1,
P.Costo,
'Dif Costo'= convert (decimal (18,2), P.Costo)*EDI.DiferenciaSistema,

--Producto
'Producto'=P.Descripcion,
P.CodigoBarra,
P.Ref,
P.Referencia,
P.CostoPromedio,
P.UnidadMedida,
P.Peso,


--Clasificacion
P.TipoProducto,
P.IDTipoProducto,
P.Linea,
P.IDLinea,
P.SubLinea,
P.IDSubLinea,
P.SubLinea2,
P.IDSubLinea2,
P.Marca,
P.IDMarca,
P.Presentacion,
P.IDPresentacion,

--Deposito
D.Deposito,
D.IDSucursal,
D.Sucursal,

--Control Inventario
CI.FechaAjuste

From ExistenciaDepositoInventario EDI
Join ControlInventario CI On EDI.IDTransaccion=CI.IDTransaccion
Join VProducto P On EDI.IDProducto=P.ID
Join VDeposito D On EDI.IDDeposito=D.ID 










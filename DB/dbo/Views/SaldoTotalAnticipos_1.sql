﻿

CREATE View [dbo].[SaldoTotalAnticipos]
as

Select
C.IDTransaccion,
'Monto'=C.Total - Isnull((Select Sum(VC.ImporteReal) from VVentaDetalleCobranza VC where VC.IDTransaccionCobranza = C.IDTransaccion),0),
C.IDCliente

From
VCobranzaCredito C
WHERE C.Anulado='False' 
and C.Total > Isnull((Select Sum(VC.ImporteReal) from VVentaDetalleCobranza VC where VC.IDTransaccionCobranza = C.IDTransaccion),0)


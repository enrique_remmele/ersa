﻿CREATE view [dbo].[VInventarioBoletaContraBoleta]
 as 

Select
V.IDCliente, 
'Cliente' = C.RazonSocial + ' - ' +  C.Referencia,
V.Direccion,
V.Referencia ,
V.IDTipoCliente,
V.IDSucursalCliente,
V.SucursalCliente,
V.IDMoneda, 
v.IDZonaVenta, 
V.Telefono,
V.RUC , 
V.Condicion ,
V.IDTipoComprobante,
V.Comprobante ,
V.NroComprobante,
'Vendedor'=ISNULL(V.Vendedor,'-----') ,
'Cobrador'=ISNULL(V.Cobrador,'-----'),
V.IDSucursal,
V.Sucursal,
V.IDCiudad,
V.Ciudad ,
V.IDDeposito, 
V.Fecha,
V.IDEstado,
--V.Estado,
'Estado'= (Case when C.Judicial = 'True' then Concat('JUDICIAL',' - ',(select Descripcion from Abogado where Id = C.IdAbogado)) else V.Estado End ),
V.IDVendedor,
V.IDCobrador, 
'Fecha Emision'=FechaEmision,
'Fecha Vencimiento'= ISNULL(V.[Fec. Venc.], V.Fec),
'Ven.'= CONVERT (varchar (50), FechaVencimiento ,6),
'Plazo'= DATEDIFF (D, FechaEmision, FechaVencimiento ),
'Ultimo Pago'=(select MAX(C.FechaCobranza) from VVentaDetalleCobranza C Where C.IDTransaccion = V.IDTransaccion),
'Dias Vencidos'= DATEDIFF (D,   V.FechaVencimiento, GETDATE() ),
V.Total ,
V.Saldo,
V.Cancelado,
V.Anulado,
'Documento'='Venta',
'IVA 5%'=IsNull((Select Total From DetalleImpuesto DI Where DI.IDTransaccion=V.IDTransaccion And DI.IDImpuesto=2),0),
VLD.Chofer,
VLD.IDChofer,
--V.FechaVencimiento,
'FechaVencimiento'= (Case When(V.Condicion) = 'CONT' Then V.FechaEmision Else V.FechaVencimiento End),
'ChequeDiferido'='False',
'ChequeDescontado'='False',
v.Idtransaccion,
'FormaPagoFactura'=(Select referencia from FormaPagoFactura where ID = V.IDFormaPagoFactura),
ISnull((select Cotizacion from Cotizacion where IDMoneda = V.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1) as Cotizacion,
'Moneda'=V.Moneda,
'CotizacionOrig'=V.Cotizacion,
V.IDSucursalFiltro --Sucursal del cliente
--V.*
From VVenta V
--Left Outer Join VVentaLoteDistribucion VLD on V.IDTransaccion=VLD.IDTransaccionVenta and VLD.Anulado='False'
outer apply (
    select top 1 * --top N rows
    from VVentaLoteDistribucion r
    where r.IDTransaccionVenta = V.IDTransaccion
	and r.Anulado='False'
) VLD

join cliente C on C.Id = V.Idcliente
Where V.Cancelado = 'False' 
And V.CancelarAutomatico = 'False' 
and c.Boleta = 'True' 
and V.saldo<> 0
and V.Anulado = 0

 



﻿CREATE View [dbo].[VVentaDevoluciones]
As
Select
V.IDTransaccion,

--Punto de Expedicion
V.IDPuntoExpedicion,
'PE'=PE.Descripcion,
PE.Ciudad,
PE.[CiudadDesc.],
PE.IDCiudad,
PE.IDSucursal,
PE.Sucursal,
PE.ReferenciaSucursal,
PE.ReferenciaPunto,
PE.Timbrado,

V.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
V.NroComprobante,
'Comprobante'=V.Comprobante,

--Cliente
V.IDCliente,
'Cliente'=C.RazonSocial,
'ReferenciaCliente'=C.Referencia,
C.RUC,
V.IDSucursalCliente,
'SucursalCliente'=(Case When (V.IDSucursalCliente) IS Null Then 'MATRIZ' Else (Case When (V.IDSucursalCliente) = 0 Then 'MATRIZ' Else (Select Top(1) CS.Sucursal From ClienteSucursal CS Where V.IDCliente=CS.IDCliente And CS.ID=V.IDSucursalCliente) End) End),
'IDCiudadCliente'=C.IDCiudad,
V.Direccion,
'Telefono'=(Case When (C.Telefono) = '' Then 'X' Else C.Telefono End),
C.IDZonaVenta,
C.IDTipoCliente,
'TipoCliente'=IsNull(TP.Descripcion, '---'),
C.Referencia,
C.IDCobrador ,
C.Cobrador ,
C.IDEstado,
C.Estado ,

--Opciones
V.IDDeposito,
'Deposito'=D.Descripcion,
V.FechaEmision,
'Fec'=CONVERT(varchar(50), V.FechaEmision, 3),
'Fecha'=V.FechaEmision,
V.Credito,
'CreditoContado'=(Case When (V.Credito) = 'True' Then 'CREDITO' Else 'CONTADO' End),
'Condicion'=(Case When (V.Credito) = 'True' Then 'CRED' Else 'CONT' End),
'Operacion'=convert(varchar(50),TC.Codigo) +'   '+convert(Varchar(50),V.NroComprobante)+'  '+ (Case When (V.Credito) = 'True' Then 'CRED' Else 'CONT' End),
V.FechaVencimiento,
'Fec. Venc.'= (Case When V.Credito='True' Then (CONVERT(varchar(50), V.FechaVencimiento, 3)) Else '---' End),
V.Observacion,
V.IDMoneda,
'Moneda'=M.Referencia,
'DescripcionMoneda'=M.Descripcion,
V.Cotizacion,
V.Anulado,

--Otros
V.IDListaPrecio,
'Lista de Precio'=IsNull((Select LP.Descripcion From ListaPrecio LP Where LP.ID=V.IDListaPrecio), '---'),
'Descuento'=ISNULL(V.Descuento, 0),
V.IDVendedor,
'Vendedor'=ISNULL((Select VE.Nombres From Vendedor VE Where VE.ID=V.IDVendedor), '---'),
V.IDPromotor,
'Promotor'=ISNULL((Select PRO.Nombres From Promotor PRO Where PRO.ID=V.IDPromotor), '---'),
V.NroComprobanteRemision,
'Remision'=V.NroComprobanteRemision,
V.EsVentaSucursal,

--Totales
DTC.Total,
DTC.TotalImpuesto,
DTC.TotalDiscriminado,
DTC.TotalDescuento,
'TotalBruto'=isnull(DTC.Total,0) + isnull(DTC.TotalDescuento,0),

--Credito
V.Saldo,
V.Cobrado,
V.Descontado,
V.Cancelado,
V.Acreditado,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Estado
V.Procesado,
'EstadoVenta'=dbo.FVentaEstado(V.IDTransaccion),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=V.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=V.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=V.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=V.IDTransaccion),

--Mes y Año
'DiaP'=dbo.FFormatoDosDigitos(DatePart(dd,V.FechaEmision )),
'MesP'=dbo.FMes(DatePart(MM,V.FechaEmision )),
'Año'= Convert (Varchar (50),YEAR(V.FechaEmision)),
'Fec.Vencimiento'= Convert (Varchar (50),YEAR (V.FechaVencimiento))+Convert (Varchar(50),dbo.FFormatoDosDigitos (DATEPART (MM,V.FechaVencimiento)))+Convert (Varchar (50), dbo.FFormatoDosDigitos (DATEPART (DD,V.FechaVencimiento ))),

V.Autoriza,
'IDMotivo'=MAV.ID,
'MotivoAnulacion'=ISNULL(MAV.Descripcion,'---'),

--Direccion Alternativa
'DireccionAlternativa'=(Case When V.EsVentaSucursal='False' Then C.RazonSocial Else C.RazonSocial + ' - ' + (Case When (V.IDSucursalCliente) IS Null Then 'MATRIZ' Else (Case When (V.IDSucursalCliente) = 0 Then 'MATRIZ' Else (Select Top(1) CS.Sucursal From ClienteSucursal CS Where V.IDCliente=CS.IDCliente And CS.ID=V.IDSucursalCliente) End) End) End),

--Condicion Contado
'Contado'=(Case When (V.Credito) = 'True' Then 'False' Else 'True' End)

From Venta V
Join Transaccion T On V.IDTransaccion=T.ID
Left Outer Join VPuntoExpedicion PE On V.IDPuntoExpedicion=PE.ID
Left Outer Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Left Outer Join VCliente C On V.IDCliente=C.ID
Left Outer Join TipoCliente TP on TP.ID=C.IDTipoCliente
Left Outer Join Sucursal S On V.IDSucursal=S.ID
Left Outer Join Deposito D On V.IDDeposito=D.ID
Left Outer Join Moneda M On V.IDMoneda=M.ID
Left Outer Join MotivoAnulacionVenta MAV On MAV.ID=V.IDMotivo
Join DetalleNotaCredito DTC On V.IDTransaccion=DTC.IDTransaccionVenta

Where V.Procesado='True'

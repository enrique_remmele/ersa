﻿



CREATE view [dbo].[VExtractoMovimientoProveedorFacturaDebito]

As

--Nota Débito Proveedor
Select
ND.IDTransaccion,
'Operación'='Db ' + isnull(convert(varchar(50),Numero),convert(varchar(50),'--')),
'Codigo'=IDProveedor,
'Fecha'=ND.Fecha,
'Documento'=convert(varchar(50),ND.NroComprobante),
'Detalle/Concepto'='',
'Debito'=0,
'Credito'=NDC.Importe,
'Saldo'=0,
'IDMoneda'=ND.IDMoneda,
'Movimiento'='ND',
'ComprobanteAsociado'=NDC.IDTransaccionEgreso
--'CodigoCC' = DA.Codigo
From
NotaDebitoProveedor ND
Join NotaDebitoProveedorCompra NDC On  ND.IDTransaccion = NDC.IDTransaccionNotaDebitoProveedor
--Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = NDC.IDTransaccionEgreso and (Da.Descripcion like 'Proveedo%' or Da.Descripcion like 'Anticipo%') and Da.Credito>0
where ND.Anulado = 'False'

Union All

--Nota de Crédito Proveedor
Select
NC.IDTransaccion,
'Operación'='Db ' + isnull(convert(varchar(50),NC.Numero),convert(varchar(50),'--')),
'Codigo'=IDProveedor,
'Fecha'=NC.Fecha,
'Documento'=convert(varchar(50),NC.NroComprobante),
'Detalle/Concepto'='',
'Debito'=NCC.Importe,
'Credito'=0,
'Saldo'=0,
NC.IDMoneda,
'Movimiento'='NC',
'ComprobanteAsociado'=NCC.IDTransaccionEgreso
--'CodigoCC' = DA.Codigo
From
NotaCreditoProveedor NC
Join NotaCreditoProveedorCompra NCC On NC.IDTransaccion = NCC.IDTransaccionNotaCreditoProveedor
--Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = NCC.IDTransaccionNotaCreditoProveedor and (Da.Descripcion like 'Proveedo%' or Da.Descripcion like 'Anticipo%') and Da.Debito>0
where NC.Anulado = 'False'

union all

--Ordenes de Pago
Select
OPE.IDTransaccionOrdenPago	,
'Operación'='OPE ' + isnull(convert(varchar(50),OP.Numero),convert(varchar(50),'--')),
'Codigo'=OP.IDProveedor,
'Fecha'=OP.Fecha,
'Documento'=convert(varchar(50),OP.Numero),
'Detalle/Concepto'=OP.Observacion,
--'Debito' = Da.Debito,
'Debito'=OPE.Importe,
'Credito'=0,
'Saldo'=0,
'IDMoneda'=OP.IDMoneda,
'Movimiento'='OPE',
'ComprobanteAsociado'=OPE.IDTransaccionEgreso
--'CodigoCC'=DA.Codigo
From
OrdenPagoEgreso OPE
--Join VCompraGastoMinimosDatos P on OPE.IDTransaccionEgreso = P.IDTransaccion
join OrdenPago OP on OPE.IDTransaccionOrdenPago=OP.IDTransaccion 
--left outer join EntregaChequeOP EOP on OP.IDTransaccion = EOP.IDTransaccionOP and EgresoRendir = 0
--Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = isnull(EOP.IDTransaccion,OPE.IDTransaccionOrdenPago) and (Da.Descripcion like 'Proveedo%' or Da.Descripcion like 'Anticipo%')  and Da.Debito>0
Where OP.Anulado='False'
--And OP.EgresoRendir = 0

union all

--Compras Importadas con cobros parciales en sistema anterior
Select
C.IDTransaccion, 
'Operación'='Migracion',
'Codigo'=C.IDProveedor,
'Fecha'=Convert(date, '20151231'),
'Documento'=convert(varchar(50),C.NroComprobante), 
'Detalle/Concepto'='Pagos parciales en sistema anterior',
'Debito'=GI.Pagado,
'Credito'=0,
'Saldo'=0,
'IDMoneda'=C.IDMoneda,
'Movimiento'='PAG. MIGRACION',
'ComprobanteAsociado'=C.IDTransaccion
--'CodigoCC'='0'
From 
Gasto C
Join GastoImportado GI On C.IDTransaccion=GI.IDTransaccion 
--Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = C.IDTransaccion and DA.Descripcion like '%Proveed%' and Da.Credito>0
Where IsNull(GI.Pagado,0)>0






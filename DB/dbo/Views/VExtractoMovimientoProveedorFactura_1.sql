﻿CREATE view [dbo].[VExtractoMovimientoProveedorFactura]

As

--Egreso
Select
C.IDTransaccion, 
'Operacion'=CodigoOperacion + ' ' + isnull(convert(varchar(50),C.Numero),convert(varchar(50),'--')),
'Codigo'=C.IDProveedor,
'RazonSocial'=P.RazonSocial ,
'Referencia'=P.Referencia ,
'Fecha'=C.Fecha,
'Documento'=C.TipoComprobante + ' ' + convert(varchar(50),C.NroComprobante), 
'Detalle/Concepto'=C.Observacion,
'Debito'=0,
'Credito'=C.Total,
'Saldo'=0.00,
'IDMoneda'=C.IDMoneda,
'DescripcionMoneda'=C.Moneda,
'Movimiento'='EG',
'ComprobanteAsociado'=C.IDTransaccion,
'ComprobanteGasto' =(select Concat(Operacion,' ',Numero,' - Comprobante ', TipoComprobante,' ',NroComprobante) from VCompraGasto where idtransaccion = C.IDTransaccion),
'FechaEgreso' = C.Fecha
--'CodigoCC' = DA.Codigo
From 
VCompraGasto C 
Join VProveedor P on C.IDProveedor=P.ID 
--Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = C.IDTransaccion and DA.Descripcion like '%Proveed%'  and Da.Credito>0

union all

--Nota Débito Proveedor
Select
ND.IDTransaccion,
'Operación'='Cr ' + isnull(convert(varchar(50),Numero),convert(varchar(50),'--')),
'Codigo'=IDProveedor,
'RazonSocial'=Proveedor,
'Referencia'=Referencia,
'Fecha'=ND.Fecha,
'Documento'=[Cod.] + ' ' + convert(varchar(50),ND.NroComprobante),
'Detalle/Concepto'='',
'Debito'=0,
'Credito'=Total,
'Saldo'=0,
'IDMoneda'=1,
'DescripcionMoneda'=DescripcionMoneda,
'Movimiento'='ND',
'ComprobanteAsociado'=NDC.IDTransaccionEgreso,
'ComprobanteGasto' =(select Concat(Operacion,' ',Numero,' - Comprobante ', TipoComprobante,' ',NroComprobante) from VCompraGasto where idtransaccion = NDC.IDTransaccionEgreso),
'FechaEgreso' = (Select Fecha from VCompraGasto where IDTransaccion =   NDC.IDTransaccionEgreso)
--'CodigoCC' = DA.Codigo
From
VNotaDebitoProveedor ND
Join NotaDebitoProveedorCompra NDC On  ND.IDTransaccion = NDC.IDTransaccionNotaDebitoProveedor
--Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = NDC.IDTransaccionEgreso and DA.Descripcion like '%Proveed%'  and Da.Credito>0
where ND.Anulado = 'False'

union all

--Nota de Crédito Proveedor
Select
NC.IDTransaccion,
'Operación'='Db ' + isnull(convert(varchar(50),NC.Numero),convert(varchar(50),'--')),
'Codigo'=IDProveedor,
'RazonSocial'=Proveedor,
NC.Referencia,
'Fecha'=NC.Fecha,
'Documento'=[Cod.] + ' ' + convert(varchar(50),NC.NroComprobante),
'Detalle/Concepto'='',
'Debito'=NCC.Importe,
'Credito'=0,
'Saldo'=0,
NC.IDMoneda,
'DescripcionMoneda'=M.Descripcion,
'Movimiento'='NC',
'ComprobanteAsociado'=NCC.IDTransaccionEgreso,
'ComprobanteGasto' =(select Concat(Operacion,' ',Numero,' - Comprobante ', TipoComprobante,' ',NroComprobante) from VCompraGasto where idtransaccion = NCC.IDTransaccionEgreso),
'FechaEgreso' = (Select Fecha from VCompraGasto where IDTransaccion =  NCC.IDTransaccionEgreso)
--'CodigoCC' = DA.Codigo
From
VNotaCreditoProveedor NC
Join VMoneda M on NC.IDMoneda=M.ID 
Join NotaCreditoProveedorCompra NCC On NC.IDTransaccion = NCC.IDTransaccionNotaCreditoProveedor
--Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = NCC.IDTransaccionEgreso and DA.Descripcion like '%Proveed%' and Da.Debito>0
where NC.Anulado = 'False'

union all

--Ordenes de Pago
Select
OPE.IDTransaccionOrdenPago	,
'Operación'='OPE ' + isnull(convert(varchar(50),OP.Numero),convert(varchar(50),'--')),
'Codigo'=OPE.IDProveedor,
'RazonSocial'=OPE.Proveedor,
'Referencia'=C.Referencia,
'Fecha'=OP.Fecha,
'Documento'=CHQ.Banco + ' ' + CHQ.CuentaBancaria + ' ' + CHQ.Mon + ' - ' + CHQ.NroCheque,
'Detalle/Concepto'=OP.Observacion,
--'Debito'=(Case When (OP.AplicarRetencion) = 'False' Then OPE.Importe Else OPE.Importe-C.RetencionIVA End),
'Debito' = OPE.Importe,
'Credito'=0,
'Saldo'=0,
'IDMoneda'=M.ID,
'DescripcionMoneda'=M.Referencia,
'Movimiento'='OPE',
'ComprobanteAsociado'=OPE.IDTransaccion,
'ComprobanteGasto' =(select Concat(Operacion,' ',Numero,' - Comprobante ', TipoComprobante,' ',NroComprobante) from VCompraGasto where idtransaccion = OPE.IDTransaccion),
'FechaEgreso' = (Select Fecha from VCompraGasto where IDTransaccion =  OPE.IDTransaccion)
--'CodigoCC'=DA.Codigo
From
VOrdenPagoEgreso OPE
join OrdenPago OP on OPE.IDTransaccionOrdenPago=OP.IDTransaccion
Join Moneda M On OP.IDMoneda=M.ID
--Join VCheque CHQ On OP.IDTransaccion=CHQ.IDTransaccion
left Join VCheque CHQ On OP.IDTransaccion=CHQ.IDTransaccion
Join VCompraGasto C On OPE.IDTransaccion=C.IDTransaccion
--Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = OPE.IDTransaccion and DA.Descripcion like '%Proveed%'  and Da.Debito>0
Where OP.Anulado='False'

--Union All

--Retenciones

--Select
--R.IDTransaccion,
--'Operación'='RET ' + isnull(convert(varchar(50),R.Numero),convert(varchar(50),'--')),
--'Codigo'=R.IDProveedor,
--'RazonSocial'=R.Proveedor,
--'Referencia'=C.Referencia,
--'Fecha'=OP.Fecha,
--'Documento'=R.TipoComprobante + ' ' + convert(varchar(50),R.NroComprobante),
--'Detalle/Concepto'=R.Observacion,
--'Debito'=Round((R.TotalIVA + R.TotalRenta),2),
--'Credito'=0,
--'Saldo'=0,
--'IDMoneda'=R.IDMoneda,
--'DescripcionMoneda'='',
--'Movimiento'='RET',
--'ComprobanteAsociado'=OPE.IDTransaccionEgreso,
--'ComprobanteGasto' =(select Concat(Operacion,' ',Numero,' - Comprobante ', TipoComprobante,' ',NroComprobante) from VCompraGasto where idtransaccion = OPE.IDTransaccionEgreso),
--'FechaEgreso' = (Select Fecha from VCompraGasto where IDTransaccion =  OPE.IDTRansaccionEgreso),
----'CodigoCC'=IsNull((Select top(1)Codigo from vDetalleAsiento where IDTransaccion = OPE.IDTRansaccionEgreso and Descripcion like '%Proveed%'),'')
--'CodigoCC'=DA.Codigo
--From
--VRetencionIVA R
--join OrdenPago OP on R.IDTransaccionOrdenPago=OP.IDTransaccion
--join OrdenPagoEgreso OPE on OP.IDTransaccion = OPE.IDTransaccionOrdenPago
--join VCompraGasto C on OPE.IDTransaccionEgreso = C.IDTransaccion
--Left Join vDetalleAsiento DA on DA.IDTransaccion = OPE.IDTransaccionEgreso and DA.Descripcion like '%Proveed%'
--Where R.Anulado='FALSE' AND OP.Anulado ='FALSE'

--Union All

----Retenciones / Sin Ordenes de Pago
--Select
--R.IDTransaccion,
--'Operación'='RET ' + isnull(convert(varchar(50),R.Numero),convert(varchar(50),'--')),
--'Codigo'=R.IDProveedor,
--'RazonSocial'=R.Proveedor,
--'Referencia'=C.Referencia,
--'Fecha'=R.Fecha,
--'Documento'=R.TipoComprobante + ' ' + convert(varchar(50),R.NroComprobante),
--'Detalle/Concepto'=R.Observacion,
--'Debito'=Round((R.TotalIVA + R.TotalRenta),2),
--'Credito'=0,
--'Saldo'=0,
--'IDMoneda'=R.IDMoneda,
--'DescripcionMoneda'=R.Moneda,
--'Movimiento'='RET',
--'ComprobanteAsociado'=C.IDTransaccion,
--'ComprobanteGasto' =(select Concat(Operacion,' ',Numero,' - Comprobante ', TipoComprobante,' ',NroComprobante) from VCompraGasto where idtransaccion = C.IDTransaccion),
--'FechaEgreso' = (Select Fecha from VCompraGasto where IDTransaccion =  C.IDTransaccion),
----'CodigoCC'=IsNull((Select top(1)Codigo from vDetalleAsiento where IDTransaccion = C.IDTransaccion and Descripcion like '%Proveed%'),'')
--'CodigoCC'=DA.COdigo
--From
--VRetencionIVA R
--Join DetalleRetencion DR On R.IDTransaccion=DR.IDTransaccion
--Join VCompraGasto C On DR.TipoyComprobante=dbo.FFormatoComprobante(C.NroComprobante)  
--And R.IDProveedor=C.IDProveedor 
----And DR.Fecha=C.Fecha
--Left Join vDetalleAsiento DA on DA.IDTransaccion = C.IDTransaccion and DA.Descripcion like '%Proveed%'
--Where R.Anulado='False' And IsNull(R.IDTransaccionOrdenPago, 0) = 0
--and DR.IDTransaccionEgreso is null

--union all

----Retenciones / Sin Ordenes de Pago
--Select
--R.IDTransaccion,
--'Operación'='RET ' + isnull(convert(varchar(50),R.Numero),convert(varchar(50),'--')),
--'Codigo'=R.IDProveedor,
--'RazonSocial'=R.Proveedor,
--'Referencia'=C.Referencia,
--'Fecha'=R.Fecha,
--'Documento'=R.TipoComprobante + ' ' + convert(varchar(50),R.NroComprobante),
--'Detalle/Concepto'=R.Observacion,
--'Debito'=Round((R.TotalIVA + R.TotalRenta),2),
--'Credito'=0,
--'Saldo'=0,
--'IDMoneda'=R.IDMoneda,
--'DescripcionMoneda'=R.Moneda,
--'Movimiento'='RET',
--'ComprobanteAsociado'=C.IDTransaccion,
--'ComprobanteGasto' =(select Concat(Operacion,' ',Numero,' - Comprobante ', TipoComprobante,' ',NroComprobante) from VCompraGasto where idtransaccion = C.IDTransaccion),
--'FechaEgreso' = (Select Fecha from VCompraGasto where IDTransaccion =  C.IDTransaccion),
----'CodigoCC'=IsNull((Select top(1)Codigo from vDetalleAsiento where IDTransaccion = C.IDTransaccion and Descripcion like '%Proveed%'),'')
--'CodigoCC'=DA.Codigo
--From
--VRetencionIVA R
--Join DetalleRetencion DR On R.IDTransaccion=DR.IDTransaccion
--Join VCompraGasto C On DR.IDTransaccionEgreso = C.IDTransaccion
--And R.IDProveedor=C.IDProveedor 
----And DR.Fecha=C.Fecha
--Left Join vDetalleAsiento DA on DA.IDTransaccion = C.IDTransaccion and DA.Descripcion like '%Proveed%'
--Where R.Anulado='False' And IsNull(R.IDTransaccionOrdenPago, 0) = 0
--and DR.IDTransaccionEgreso is not null

Union All

--Compras Importadas con cobros parciales en sistema anterior
Select
C.IDTransaccion, 
'Operación'='Migracion',
'Codigo'=C.IDProveedor,
'RazonSocial'=C.Proveedor,
'Referencia'=C.Referencia ,
'Fecha'=Convert(date, '20151231'),
'Documento'=C.TipoComprobante + ' ' + convert(varchar(50),C.NroComprobante), 
'Detalle/Concepto'='Pagos parciales en sistema anterior',
'Debito'=GI.Pagado,
'Credito'=0,
'Saldo'=0,
'IDMoneda'=C.IDMoneda,
'DescripcionMoneda'=C.Moneda,
'Movimiento'='PAG. MIGRACION',
'ComprobanteAsociado'=C.IDTransaccion,
'ComprobanteGasto' =(select Concat(Operacion,' ',Numero,' - Comprobante ', TipoComprobante,' ',NroComprobante) from VCompraGasto where idtransaccion = C.IDTransaccion),
'FechaEgreso' = (Select Fecha from VCompraGasto where IDTransaccion =  C.IDTransaccion)
--'CodigoCC'=DA.Codigo
From 
VGasto C
Join GastoImportado GI On C.IDTransaccion=GI.IDTransaccion 
--Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = C.IDTransaccion and DA.Descripcion like '%Proveed%' and Da.Credito>0
Where IsNull(GI.Pagado,0)>0
















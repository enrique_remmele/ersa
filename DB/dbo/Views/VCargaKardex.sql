﻿

CREATE View [dbo].[VCargaKardex]
As
	
	--Compra
	Select 
	'FechaEntrada'=C.Fecha, 
	D.IDTransaccion, 
	D.IDProducto, 
	D.ID, 
	'Orden'=1,
	C.IDSucursal, 
	'Comprobante'=convert(varchar(50), C.NroComprobante), 
	'CantidadEntrada'=convert(decimal(10,2), D.Cantidad),
	'CantidadSalida'=0, 
	'CostoEntrada'=convert(decimal(18,6), (D.TotalDiscriminado/D.Cantidad) * isnull(C.Cotizacion,1)), 
	'CostoSalida'=0, 
	'Tipo'='COMPRA',
	'Cotizacion' = (Select top(1) cotizacion from cotizacion where idmoneda = 2 and fecha = C.Fecha),
	C.IDTipoComprobante
	From Compra C 
	Join DetalleCompra D On C.IDTransaccion=D.IDTransaccion 
	Join Impuesto I On D.IDImpuesto=I.ID

	Union all

	---Entrada Ticket de Bascula
	Select 
	C.Fecha, 
	C.IDTransaccion, 
	C.IDProducto, 
	'ID'=1, 
	'Orden'=1, 
	DEP.IDSucursal, 
	'Comprobante'=convert(varchar(50),C.NroComprobante), 
	'CantidadEntrada'=C.PesoBascula, 
	'CantidadSalida'=0, 
	'CostoEntrada'=Round(C.PrecioUnitario/I.FactorDiscriminado,12),--no se  ultiplica por la cotizacion porque el precio unitario USD se guarda en columna a parte
	'CostoSalida'=0, 
	'Tipo'='COMPRA' 
	,'Cotizacion' = (Select top(1) cotizacion from cotizacion where idmoneda = 2 and fecha = C.Fecha),
	C.IDTipoComprobante
	From TicketBascula C 
	Join Impuesto I On C.IDImpuesto=I.ID 
	Join Deposito DEP On C.IDDeposito=DEP.ID 
	Join Sucursal S On DEP.IDSucursal=S.ID 
	Join Producto P On C.IDProducto=P.ID 
	Where C.Anulado='False' 
	--And P.ControlarExistencia='True' 
	and Procesado = 'True'
	
	Union all

	---Macheo Ajuste de contrato
	Select 
	C.Fecha, 
	C.IDTransaccion, 
	C.IDProducto, 
	'ID'=1, 
	'Orden'=1, 
	C.IDSucursal, 
	'Comprobante'=convert(varchar(50),C.NroComprobante), 
	'CantidadEntrada'=0, 
	'CantidadSalida'=0, 
	'CostoEntrada'=(select Sum(Debito) from DetalleAsiento where CuentaContable = (Select CuentaContableCosto from vTipoProducto where ID=P.IDTipoProducto) and IDTransaccion = C.IDTransaccion),
	'CostoSalida'=(select Sum(Credito) from DetalleAsiento where CuentaContable = (Select CuentaContableCosto from vTipoProducto where ID=P.IDTipoProducto) and IDTransaccion = C.IDTransaccion), 
	'Tipo'='MACHEO FACTURA TICKET' 
	,'Cotizacion' = (Select top(1) cotizacion from cotizacion where idmoneda = 2 and fecha = C.Fecha),
	C.IDTipoComprobante
	From VMacheoFacturaTicket C 
	Join Sucursal S On C.IDSucursal=S.ID 
	Join Producto P On C.IDProducto=P.ID 
	Where C.Anulado='False' 
	--And P.ControlarExistencia='True' 
	
	Union all

	--Ajuste de costo
	Select 
	'FechaEntrada'=PC.Fecha, 
	D.IDTransaccion, 
	D.IDProducto, 
	'ID'=1, 
	'Orden'=0,
	'IDSucursal' =1,
	'Comprobante'=convert(varchar(50), PC.Numero), 
	'CantidadEntrada'=0, 
	'CantidadSalida'=0, 
	'CostoEntrada'=D.Costo,
	'CostoSalida'=0, 
	'Tipo'='AJUSTECOSTO',
	'Cotizacion' = 1,
	0
	From ProductoCosto PC
	Join DetalleProductoCosto D On PC.IDTransaccion=D.IDTransaccion 
	Where PC.Anulado = 0
	And isnull(PC.CostoProduccion,0) = 0

	union all

	--Venta
	Select  
	C.FechaEmision, 
	D.IDTransaccion, 
	D.IDProducto, 
	D.ID, 
	'Orden'=10, 
	C.IDSucursal, 
	'Comprobante'=convert(varchar(50), 
	C.Comprobante), 
	'CantidadEntrada'=0, 
	'CantidadSalida'=D.Cantidad, 
	'CostoEntrada'=0, 
	'CostoSalida'=D.CostoUnitario, 
	'Tipo'='VENTA',
	'Cotizacion' = (Select top(1) cotizacion from cotizacion where idmoneda = 2 and fecha = C.FechaEmision),
	C.IDTipoComprobante
	From Venta C 
	Join DetalleVenta D On C.IDTransaccion=D.IDTransaccion 
	Join Impuesto I On D.IDImpuesto=I.ID 
	Where C.Procesado='True' 
	And C.Anulado='False'

	Union All

	--Ajuste Inicial Positivo
	Select 
	C.Fecha, 
	D.IDTransaccion, 
	D.IDProducto, 
	'ID'=0, 
	'Orden'=0, 
	D.IDSucursal, 
	'Comprobante'=convert(varchar(50), C.Numero), 
	'CantidadEntrada'=Sum(D.Existencia), 
	'CantidadSalida'=0, 
	'CostoEntrada'=D.Costo, 
	'CostoSalida'=0, 
	'Tipo'='AJUSTE INICIAL POSITIVO',
	'Cotizacion' = (Select top(1) cotizacion from cotizacion where idmoneda = 2 and fecha = C.Fecha),
	0
	From AjusteInicial C 
	Join DetalleAjusteInicial D On C.IDTransaccion=D.IDTransaccion 
	Join Producto P On D.IDProducto=P.ID 
	Join Impuesto I On P.IDImpuesto=I.ID
	Where D.Diferencia>0 
	And C.Procesado = 1
	Group By C.Fecha, D.IDTransaccion, D.IDProducto, D.IDSucursal, C.Numero, D.Costo

	Union All

	---Ajuste Inicial Negativo
	Select 
	C.Fecha, 
	D.IDTransaccion, 
	D.IDProducto, 
	'ID'=0, 
	'Orden'=0, 
	D.IDSucursal, 
	'Comprobante'=convert(varchar(50), C.Numero), 
	'CantidadEntrada'=0, 
	'CantidadSalida'=Sum(D.Existencia)*-1, 
	'CostoEntrada'=0, 
	'CostoSalida'=D.Costo, 
	'Tipo'='AJUSTE INICIAL NEGATIVO',
	'Cotizacion' = (Select top(1) cotizacion from cotizacion where idmoneda = 2 and fecha = C.Fecha),
	0
	From AjusteInicial C 
	Join DetalleAjusteInicial D On C.IDTransaccion=D.IDTransaccion 
	Join Producto P On D.IDProducto=P.ID 
	Join Impuesto I On P.IDImpuesto=I.ID
	Where D.Diferencia<0 
	And C.Procesado = 1 
	Group By C.Fecha, D.IDTransaccion, D.IDProducto, D.IDSucursal, C.Numero, D.Costo

	Union All

	--Notas de Credito Venta
	Select 
	C.Fecha, 
	D.IDTransaccion, 
	D.IDProducto, 
	D.ID, 
	'Orden'=15, 
	C.IDSucursal, 
	'Comprobante'=convert(varchar(50), C.NroComprobante), 
	'CantidadEntrada'=0, 
	'CantidadSalida'=D.Cantidad*-1, 
	'CostoEntrada'=0, 
	'CostoSalida'=D.CostoUnitario, 
	'Tipo'='NOTA CREDITO VENTA',
	'Cotizacion' = (Select top(1) cotizacion from cotizacion where idmoneda = 2 and fecha = C.Fecha),
	C.IDTipoComprobante
	From NotaCredito C 
	Join DetalleNotaCredito D On C.IDTransaccion=D.IDTransaccion 
	Join Impuesto I On D.IDImpuesto=I.ID 
	Join Producto P On D.IDProducto=P.ID 
	Where C.Anulado='False'
	And C.Procesado='True' 
	--And P.ControlarExistencia='True' 

	Union All

	--Notas de Credito Compra
	Select 
	C.Fecha, 
	D.IDTransaccion, 
	D.IDProducto, 
	D.ID, 
	'Orden'=5, 
	C.IDSucursal, 
	'Comprobante'=convert(varchar(50), C.NroComprobante), 
	'CantidadEntrada'=D.Cantidad*-1, 
	'CantidadSalida'=0, 
	'CostoEntrada'=D.CostoUnitario, 
	'CostoSalida'=0, 	
	'Tipo'='NOTA CREDITO COMPRA',
	'Cotizacion' = (Select top(1) cotizacion from cotizacion where idmoneda = 2 and fecha = C.Fecha),
	C.IDTipoComprobante
	From NotaCreditoProveedor C 
	Join DetalleNotaCreditoProveedor D On C.IDTransaccion=D.IDTransaccion 
	Join Impuesto I On D.IDImpuesto=I.ID 
	Join Producto P On D.IDProducto=P.ID 
	Where C.Procesado='True' 
	--And P.ControlarExistencia='True' 

	Union All

	--Entrada por Importacion
	Select 
	C.Fecha, 
	D.IDTransaccion, 
	D.IDProducto, 
	D.ID, 
	'Orden'=6, 
	DEP.IDSucursal, 
	'Comprobante'=convert(varchar(50),C.NroComprobante), 
	'CantidadEntrada'=D.Cantidad, 
	'CantidadSalida'=0, 
	'CostoEntrada'=D.PrecioUnitario, 
	'CostoSalida'=0, 
	'Tipo'='COMPRA',
	'Cotizacion' = (Select top(1) cotizacion from cotizacion where idmoneda = 2 and fecha = C.Fecha),
	C.IDTipoComprobante
	From Movimiento C 
	Join DetalleMovimiento D On C.IDTransaccion=D.IDTransaccion 
	Join Impuesto I On D.IDImpuesto=I.ID 
	Join Deposito DEP On D.IDDepositoEntrada=DEP.ID 
	Join Sucursal S On DEP.IDSucursal=S.ID 
	Join Producto P On D.IDProducto=P.ID
	left outer join TipoComprobante TC on C.IDTipoComprobante = TC.ID
	Where D.IDDepositoSalida Is Null 
	And C.Anulado='False'
	and isnull(TC.ID,0) = 120
	--and P.ControlarExistencia='True'

	Union All

	--Entradas
	Select 
	C.Fecha, 
	D.IDTransaccion, 
	D.IDProducto, 
	D.ID, 
	'Orden'=6, 
	DEP.IDSucursal, 
	'Comprobante'=convert(varchar(50),C.NroComprobante), 
	'CantidadEntrada'=D.Cantidad, 
	'CantidadSalida'=0, 
	'CostoEntrada'=D.PrecioUnitario, 
	'CostoSalida'=0, 
	'Tipo'=Case When C.IDTipoComprobante = (Select Top(1) IDTipoComprobanteProduccion from Configuraciones) then 'PRODUCCION' else 'ENTRADA' end,
	'Cotizacion' = (Select top(1) cotizacion from cotizacion where idmoneda = 2 and fecha = C.Fecha),
	C.IDTipoComprobante
	From Movimiento C 
	Join DetalleMovimiento D On C.IDTransaccion=D.IDTransaccion 
	Join Impuesto I On D.IDImpuesto=I.ID 
	Join Deposito DEP On D.IDDepositoEntrada=DEP.ID 
	Join Sucursal S On DEP.IDSucursal=S.ID 
	Join Producto P On D.IDProducto=P.ID
	left outer join TipoComprobante TC on C.IDTipoComprobante = TC.ID
	Where D.IDDepositoSalida Is Null 
	And C.Anulado='False'
	and isnull(TC.ID,0) <> 120
	--and P.ControlarExistencia='True'

	Union All

	--Salidas
	Select 
	C.Fecha, 
	D.IDTransaccion, 
	D.IDProducto, 
	D.ID, 
	'Orden'=11, 
	DEP.IDSucursal, 
	'Comprobante'=convert(varchar(50), C.NroComprobante), 
	'CantidadEntrada'=0, 
	'CantidadSalida'=D.Cantidad, 
	'CostoEntrada'=0, 
	'CostoSalida'=D.PrecioUnitario, 
	'Tipo'='SALIDA',
	'Cotizacion' = (Select top(1) cotizacion from cotizacion where idmoneda = 2 and fecha = C.Fecha),
	C.IDTipoComprobante
	From Movimiento C 
	Join DetalleMovimiento D On C.IDTransaccion=D.IDTransaccion 
	Join Impuesto I On D.IDImpuesto=I.ID 
	Join Deposito DEP On D.IDDepositoSalida=DEP.ID 
	Join Sucursal S On DEP.IDSucursal=S.ID 
	Join Producto P On D.IDProducto=P.ID 
	Where D.IDDepositoEntrada Is Null 
	And C.Anulado='False'
	-- and P.ControlarExistencia='True' 
	Union All

	--Ajuste Inventario Negativo
	Select 
	C.FechaAjuste, 
	D.IDTransaccion, 
	D.IDProducto, 
	'ID'=0, 
	'Orden'=0, 
	D.IDSucursal, 
	'Comprobante'=convert(varchar(50), C.Numero), 
	'CantidadEntrada'=0, 
	'CantidadSalida'=Sum(D.DiferenciaSistema)*-1, 
	'CostoEntrada'=0, 
	'CostoSalida'=D.Costo, 
	'Tipo'='AJUSTE NEGATIVO',						
	'Cotizacion' = (Select top(1) cotizacion from cotizacion where idmoneda = 2 and fecha = C.FechaAjuste),
	0
	From ControlInventario C 
	Join VExistenciaDepositoInventario D On C.IDTransaccion=D.IDTransaccion 
	Join Producto P On D.IDProducto=P.ID 
	Join Impuesto I On P.IDImpuesto=I.ID
	Where D.DiferenciaSistema<0 
	And C.Procesado = 1 
	And C.Anulado='False'
	Group By C.FechaAjuste, D.IDTransaccion, D.IDProducto, D.IDSucursal, C.Numero, D.Costo

	Union All

	--Ajuste Inventario Positivo
	Select 
	C.FechaAjuste, 
	D.IDTransaccion, 
	D.IDProducto, 
	'ID'=0, 
	'Orden'=0, 
	D.IDSucursal, 
	'Comprobante'=convert(varchar(50), C.Numero), 
	'CantidadEntrada'=Sum(D.DiferenciaSistema), 
	'CantidadSalida'=0, 
	'CostoEntrada'=D.Costo, 'CostoSalida'=0, 'Tipo'='AJUSTE POSITIVO'
	,'Cotizacion' = (Select top(1) cotizacion from cotizacion where idmoneda = 2 and fecha = C.FechaAjuste),
	0
	From ControlInventario C 
	Join VExistenciaDepositoInventario D On C.IDTransaccion=D.IDTransaccion 
	Join Producto P On D.IDProducto=P.ID 
	Join Impuesto I On P.IDImpuesto=I.ID
	Where D.DiferenciaSistema>0 
	And C.Procesado = 1 
	And C.Anulado='False'
	Group By C.FechaAjuste, D.IDTransaccion, D.IDProducto, D.IDSucursal, C.Numero, D.Costo

	Union All

	--CierreAperturaStock
	Select 
	C.Fecha, 
	D.IDTransaccion, 
	D.IDProducto, 
	'ID'=0, 
	'Orden'=100, 
	D.IDSucursal, 
	'Comprobante'=convert(varchar(50), C.Numero), 
	'CantidadEntrada'=D.Existencia, 
	'CantidadSalida'=D.Existencia, 
	'CostoEntrada'=D.Costo, 
	'CostoSalida'=D.Costo, 
	'Tipo'='CIERRE APERTURA STOCK',
	'Cotizacion' = (Select top(1) cotizacion from cotizacion where idmoneda = 2 and fecha = C.Fecha),
	0
	From CierreAperturaStock C 
	Join VDetalleCierreReAperturaStock D On C.IDTransaccion=D.IDTransaccion 
	Join Producto P On D.IDProducto=P.ID 
	Join Impuesto I On P.IDImpuesto=I.ID
	Where C.Anulado='False'






























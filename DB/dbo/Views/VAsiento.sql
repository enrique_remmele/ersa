﻿
CREATE View [dbo].[VAsiento]
As
Select 
A.IDTransaccion,
A.Numero,
A.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
S.IDCiudad,
'Ciudad'=C.Codigo,
A.Fecha,
A.IDMoneda,
'Moneda'=M.Referencia,
A.Cotizacion,
A.IDTipoComprobante,
'TipoComprobante'=IsNull(TP.Codigo, '---'),
'NroComprobante'=A.NroComprobante,
'Comprobante'=IsNull(TP.Codigo, '---') + ' ' + A.NroComprobante,
'Detalle'= (Case When T.IDOperacion = 65 then Concat((Select Observacion from MacheoFacturaTicket where idtransaccion = T.ID), A.Detalle) else A.Detalle end),
A.Debito,
A.Credito,
A.Saldo,
A.Total,
A.Anulado,
A.IDCentroCosto,
'CentroCosto'=(Select ISNULL((Select CC.Descripcion From CentroCosto CC Where CC.ID=A.IDCentroCosto),'---')),
'Conciliado'=IsNull(A.Conciliado, 'False'),
'Estado'=(Select Case When(IsNull(A.Conciliado, 'False')) = 'True' Then 'CONCILIADO' Else '---' End),
A.FechaConciliado,
A.IDUsuarioConciliado,
'Balanceado'=(Case When (A.Credito) != A.Debito Then 'False' Else 'True' End),
A.Bloquear,
'CajaChica'='False',

--Caja
T.IDTransaccionCaja,
'NroCaja'=IsNull(CJ.Numero, 0),
'CajaHabilitada'=IsNull(CJ.Habilitado, 'False'),
'FechaCaja'=CJ.Fecha,
'EstadoCaja'=IsNull(CONVERT(varchar(50), CJ.Fecha) + ' ' + (Case When(CJ.Habilitado) = 'True' Then 'Habilitado' Else 'Cerrado' End), '---'),
'FechaTransaccion'=(T.Fecha),
'Usuario'=(select usuario from usuario where id = T.IDUsuario),
T.IDUsuario,
'IDOperacion'= isnull(T.IDOperacion,0),
'Operacion'= (select Descripcion from operacion where id =isnull(T.IDOperacion,0))
From Asiento A
Join Transaccion T On A.IDTransaccion=T.ID
Left Outer Join Caja CJ On T.IDTransaccionCaja=CJ.IDTransaccion
Join Sucursal S On A.IDSucursal=S.ID
Join Ciudad C On S.IDCiudad=C.ID
Join Moneda M On A.IDMoneda=M.ID
Left Outer Join TipoComprobante TP On A.IDTipoComprobante=TP.ID

















﻿Create View [dbo].[VFormularioImpresionCopia]
As
Select
FC.IDFormularioImpresion,
FC.ID,
'Descripcion'='Copia ' + convert(varchar(10), FC.ID),
FC.PosicionX,
FC.PosicionY

From FormularioImpresionCopia FC
Join FormularioImpresion F On FC.IDFormularioImpresion=F.IDFormularioImpresion
﻿
CREATE view [dbo].[VFondoFijo]
as
Select
FF.IDSucursal,
'Sucursal'=S.Descripcion,
FF.IDGrupo,
'Grupo'=G.Descripcion,
Tope,
TotalRendir,
Saldo 
From
FondoFijo FF
Join Sucursal S on FF.IDSucursal=S.ID 
Join Grupo G on G.ID=FF.IDGrupo 

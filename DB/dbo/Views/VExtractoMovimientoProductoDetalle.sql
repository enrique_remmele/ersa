﻿CREATE view [dbo].[VExtractoMovimientoProductoDetalle]
as
--Entrada
--Compra
Select 
DC.IDProducto,
'Producto'=P.Descripcion,
P.IDTipoProducto,
'Referencia'=P.Referencia,
'Fecha'=convert(date, C.Fecha),
'Fec'=convert (varchar(50),C.Fecha,5),
'Movimiento'='COMPRA',
'Operación'='Cpra: ' + convert (varchar(50), C.Numero),
'Tipo'= 'COMPRA',
C.[Cod.],
'NroComprobante'=convert(varchar(50), C.NroComprobante),
C.IDDeposito,
C.IDSucursal,
'Cliente/Proveedor'=C.Proveedor,
'ComprobanteRelacionado'='',
'Entrada'=DC.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, C.FechaEntrada),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), C.FechaEntrada, 5),
'Anulado'= 'False',
'FechaAnulado'=convert(date, '19000101')

From VCompra C 
join DetalleCompra DC on C.IDTransaccion=DC.IDTransaccion 
join Producto P on DC.IDPRoducto = P.ID
Join Transaccion T On C.IDTransaccion=T.ID

Union all

--Ticket de Bascula
Select 
C.IDProducto,
'Producto'=P.Descripcion,
P.IDTipoProducto,
'Referencia'=P.Referencia,
'Fecha'=convert(date, C.Fecha),
'Fec'=convert (varchar(50),C.Fecha,5),
'Movimiento'='TICKET DE BASCULA',
'Operación'='TIBA: ' + convert (varchar(50), C.Numero),
'Tipo'= 'TICKET DE BASCULA',
'Cod.'=C.TipoComprobante,
'NroComprobante'=convert(varchar(50), C.NroComprobante),
C.IDDeposito,
C.IDSucursal,
'Cliente/Proveedor'=C.Proveedor,
'ComprobanteRelacionado'='',
'Entrada'=C.PesoBascula,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, C.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), C.Fecha, 5),
'Anulado'= C.Anulado,
'FechaAnulado'=convert(date, C.FechaAnulacion)

From vTicketBascula C 
join Producto P on C.IDPRoducto = P.ID
Join Transaccion T On C.IDTransaccion=T.ID
Where Procesado = 'True'
and C.NroAcuerdo > 0

union all

--Nota de Crédito Cliente
Select
DNC.IDProducto,
'Producto'=P.Descripcion,
P.IDTipoProducto,
'Referencia'=P.Referencia,
'Fecha'=convert(date, NC.Fecha),
'Fec'=convert (varchar(50),NC.Fecha,10),
'Movimiento'='NOTA DE CREDITO CLIENTE',
'Operación'='NOTA DE CREDITO CLIENTE',
'Tipo'= 'ENTRADA',
NC.[Cod.],
convert(varchar(50), NC.NroComprobante),
NC.IDDeposito,
NC.IDSucursal,
'Cliente/Proveedor'=NC.Cliente,
'ComprobanteRelacionado'=NC.Comprobante,
'Entrada'=DNC.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= NC.Anulado,
'FechaAnulado'=convert(date, NC.FechaAnulacion)

From VNotaCredito NC
join DetalleNotaCredito DNC on NC.IDTransaccion=DNC.IDTransaccion
join Producto P on DNC.IDProducto = P.ID
Join Transaccion T On NC.IDTransaccion=T.ID
Where NC.Procesado='True'

union all

--Movimiento Entrada
Select
DM.IDProducto,
'Producto'=P.Descripcion,
P.IDTipoProducto,
'Referencia'=P.Referencia,
'Fecha'= convert(date, M.Fecha),
'Fec'=convert (varchar(50),M.Fecha,10),
'Movimiento'=M.Motivo,
'Operación'=M.Operacion+' '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero)+ ' ' + TipoComprobante + '-' +convert (Varchar (30),M.NroComprobante) ,
'Tipo'=M.Operacion,
M.[Cod.],
M.NroComprobante,
M.IDDepositoEntrada,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=DM.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulado'=convert(date, M.FechaAnulacion)

From VMovimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion
join Producto P on DM.IDProducto = P.ID
Join Deposito D On DM.IDDepositoEntrada=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Where M.Entrada='True'and M.Salida='False'

union all

--Movimiento Salida Anulada
Select
DM.IDProducto,
'Producto'=P.Descripcion,
P.IDTipoProducto,
'Referencia'=P.Referencia,
'Fecha'=convert(date, M.FechaAnulacion ),
'Fec'=convert (varchar(50),M.FechaAnulacion ,10),
'Movimiento'=M.Motivo,
'Operación'=M.Operacion+' '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero)+' '+ 'ANULADA',
'Tipo'= M.Operacion +' '+ 'ANULADA',
M.[Cod.],
M.NroComprobante,
M.IDDepositoSalida,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=DM.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulado'=convert(date, M.FechaAnulacion)

From VMovimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
join Producto P on DM.IDProducto = P.ID
Join Deposito D On DM.IDDepositoSalida=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Where M.Entrada='False'And M.Salida='True'
And M.Anulado = 'True'

Union All

--Transferencia Entrada
Select
DM.IDProducto,
'Producto'=P.Descripcion,
P.IDTipoProducto,
'Referencia'=P.Referencia,
'Fecha'=convert(date, M.Fecha),
'Fec'=convert (varchar(50),M.Fecha,10),
'Movimiento'=M.Operacion+' '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero),
--'Operación'=M.Operacion +' - '+ M.NroComprobante,
'Operación'=M.Operacion+' '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero)+ ' ' + TipoComprobante + '-' +convert (Varchar (30),M.NroComprobante) ,
'OP'=M.Operacion,
M.[Cod.],
M.NroComprobante,
M.IDDepositoEntrada ,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=DM.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulado'=convert(date, M.FechaAnulacion)

From VMovimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
join Producto P on DM.IDProducto = P.ID
Join Deposito D On DM.IDDepositoEntrada=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Where M.Entrada='True'and M.Salida='True'

Union All

--Transferencia Salida Anulada
Select
DM.IDProducto,
'Producto'=P.Descripcion,
P.IDTipoProducto,
'Referencia'=P.Referencia,
'Fecha'=convert(date, M.FechaAnulacion),
'Fec'=convert (varchar(50),M.FechaAnulacion,10),
'Movimiento'=M.Motivo+' '+ 'ANULADA',
'Operación'=M.Operacion+' - '+M.NroComprobante+' - '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero)+' '+ 'ANULADA',
'Tipo'=M.Operacion,
M.[Cod.],
M.NroComprobante,
M.IDDepositoSalida ,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=DM.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulado'=convert(date, M.FechaAnulacion)

From VMovimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion
Join Producto P on DM.IDProducto = P.ID
Join Deposito D On DM.IDDepositoSalida=D.ID 
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Where M.Entrada='True'And M.Salida='True'
And M.Anulado = 'True'

Union all

--Ajuste Inventario Positivo
Select
EDI.IDProducto, 
EDI.Producto,
EDI.IDTipoProducto,
'Referencia'=EDI.Ref,
'Fecha'=convert(date, V.FechaAjuste),
'Fec'=convert (varchar(50),V.FechaAjuste,10),
'Movimiento'='AJUSTE POSITIVO',
'Operación'='AJUSTE INVENTARIO POSITIVO',
'Tipo'= 'ENTRADA',
'Cod.'='',
'NroComprobante'='',
EDI.IDDeposito,
EDI.IDSucursal,
'Cliente/Proveedor'=(Case When V.Observacion = Null Then '---' Else (Case When V.Observacion='' Then '---' Else V.Observacion End) End)  ,
'ComprobanteRelacionado'='--',
'Entrada'=EDI.DiferenciaSistema,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, V.FechaAjuste),
'FechaOperacion'=CONVERT(date, V.FechaAjuste),
--'Hora Ope.'=Convert(varchar(10), V.FechaAjuste, 8),
--'Fec. Ope.'=Convert(varchar(10), V.FechaAjuste, 5)
'Hora Ope.'=Convert(varchar(10), V.FechaTransaccion, 8),
'Fec. Ope.'=Convert(varchar(10), V.FechaTransaccion, 5),
'Anulado'= 'False',
'FechaAnulado'=convert(date, '19000101')

From VControlInventario V
 join VExistenciaDepositoInventario EDI on V.IDTransaccion=EDI.IDTransaccion 
 Join Transaccion T On V.IDTransaccion=T.ID
Where EDI.DiferenciaSistema >0 And V.Procesado='True' And V.Anulado='False'

union all

--Ventas Anuladas
Select 
DV.IDProducto,
'Producto'=P.Descripcion,
P.IDTipoProducto,
'Referencia'=P.Referencia,
--Se realiza este case when porque cuando se facturan para el dia siguiente y luego se anula 
--se guarda de esta forma Fecha Emision:02/01/2019, Fecha Anulacion:01/01/2019 y genera inconsistencias
--'Fecha'=Convert(date, V.FechaAnulacion),
'Fecha'=Case When DA.Fecha > V.FechaEmision then Convert(date, V.FechaAnulacion) else Convert(date, V.FechaEmision) end,
--'Fec'=convert (varchar(50),V.FechaAnulacion,10),
'Fec'=Case When DA.Fecha > V.FechaEmision then convert (varchar(50),V.FechaAnulacion,10) else Convert(varchar(50), V.FechaEmision,10) end,
'Movimiento'='VENTA',
'Operación'='VENTA ANULADA',
'Tipo'= 'ENTRADA',
V.[Cod.],
'NroComprobante'=convert(varchar(50), V.NroComprobante),
V.IDDeposito,
V.IDSucursal,
'Cliente/Proveedor'=V.Cliente,
'ComprobanteRelacionado'='Venta:'+IsNull(convert(varchar(50), (V.Comprobante)), '---'),
'Entrada'=DV.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
--'Hora'=CONVERT(time, DA.Fecha),
'Hora'=CONVERT(time, V.FechaEmision),
--'FechaOperacion'=CONVERT(date, DA.Fecha),
'FechaOperacion'=CONVERT(date, V.Fecha),
--'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Hora Ope.'=Convert(varchar(10), V.Fecha, 8),
--'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5)
'Fec. Ope.'=Convert(varchar(10), V.Fecha, 5),
'Anulado'= V.Anulado,
'FechaAnulado'=convert(date, V.FechaAnulacion)

From VVenta V
join DetalleVenta DV on V.IDTransaccion=DV.IDTransaccion 
join Producto P on DV.IDProducto = P.ID
Join Transaccion T On V.IDTransaccion=T.ID
Join DocumentoAnulado DA On V.IDTransaccion=DA.IDTransaccion
Where V.Anulado='True'
And V.Procesado='True'

Union All

--Ajuste Inicial positivo
Select 
D.IDProducto,
D.Producto,
0,
'Referencia'=D.Referencia,
'Fecha'=convert(date, A.Fecha),
'Fec'=convert (varchar(50),A.Fecha,10),
'Movimiento'='AJUSTE INICIAL POSITIVO',
'Operación'='AJUSTE Nro: ' + CONVERT(varchar(50), A.Numero),
'Tipo'= 'ENTRADA',
'Cod.'='',
'NroComprobante'=convert(varchar(50), A.Numero),
D.IDDeposito,
D.IDSucursal,
'Cliente/Proveedor'='AJUSTE INICIAL POSITIVO',
'ComprobanteRelacionado'=A.Observacion,
'Entrada'=D.Entrada,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
--'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
--'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5)
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= 'False',
'FechaAnulado'=convert(date, '19000101')

From AjusteInicial A 
join VDetalleAjusteInicial D on A.IDTransaccion=D.IDTransaccion 
Join Transaccion T On A.IDTransaccion=T.ID
Where A.Procesado='True'
And D.Entrada>0 

--SALIDAS

Union All

--Venta
Select 
DV.IDProducto,
P.Descripcion,
P.IDTipoProducto,
'Referencia'=P.Referencia,
'Fecha'=convert(date, V.FechaEmision),
'Fec'=convert (varchar(50),V.FechaEmision,10),
'Movimiento'='VENTA',
'Operación'='VENTA'+ ' '+'Fact:'+CONVERT (varchar (50), V.NroComprobante) ,
'Tipo'= 'VENTA',
TC.Codigo,
'NroComprobante'=convert(varchar(50), V.NroComprobante),
V.IDDeposito,
V.IDSucursal,
'Cliente/Proveedor'=C.RazonSocial,
'ComprobanteRelacionado'='Pedido(s):'+IsNull(convert(varchar(50), (Select Top(1) PV.NumeroPedido From VPedidoVenta PV Where PV.IDTransaccionVenta=V.IDTransaccion)), '---'),
'Entrada'=0.00,
'Salida'=DV.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= V.Anulado,
'FechaAnulado'=convert(date, V.FechaAnulacion)

From VVenta V
join DetalleVenta DV on V.IDTransaccion=DV.IDTransaccion 
Join Producto P On DV.IDProducto=P.ID
Join Cliente C On V.IDCliente=C.ID
join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Join Transaccion T On V.IDTransaccion=T.ID
--Left Outer join VPedidoVenta PV on V.IDTransaccion=PV.IDTransaccionVenta 
Where V.Procesado='True'


union all

--Nota de Crédito Cliente ANULADO
Select
DNC.IDProducto,
'Producto'=P.Descripcion,
P.IDTipoProducto,
'Referencia'=P.Referencia,
'Fecha'=convert(date, NC.FechaAnulacion),
'Fec'=convert (varchar(50),NC.FechaAnulacion,10),
'Movimiento'='NOTA DE CREDITO CLIENTE ANULADA',
'Operación'='NOTA DE CREDITO CLIENTE ANULADA',
'Tipo'= 'SALIDA',
NC.[Cod.],
convert(varchar(50), NC.NroComprobante),
NC.IDDeposito,
NC.IDSucursal,
'Cliente/Proveedor'=NC.Cliente,
'ComprobanteRelacionado'=NC.Comprobante,
'Entrada'=0.00,
'Salida'=DNC.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
'Anulado'= NC.Anulado,
'FechaAnulado'=convert(date, NC.FechaAnulacion)

From VNotaCredito NC
join DetalleNotaCredito DNC on NC.IDTransaccion=DNC.IDTransaccion
Join Producto P on DNC.IDProducto = P.ID
Join Transaccion T On NC.IDTransaccion=T.ID
Join DocumentoAnulado DA On NC.IDTransaccion=DA.IDTransaccion
WHERE NC.Anulado = 1
And NC.Procesado='True'

union all

--Nota Crédito Proveedor
Select 
DNP.IDProducto,
'Producto'=P.Descripcion,
P.IDTipoProducto,
'Referencia'=P.Referencia,
'Fecha'=convert(date, NCP.Fecha),
'Fec'=convert (varchar(50),NCP.Fecha,10),
'Movimiento'='NOTA DE CREDITO PROVEEDOR',
'Operación'='NOTA CREDITO PROVEEDOR',
'TIPO'= 'SALIDA',
NCP.[Cod.],
'NroComprobante'=NCP.Comprobante,
NCP.IDDeposito,
NCP.IDSucursal,
'Cliente/Proveedor'=NCP.Proveedor,
'ComprobanteRelacionado'=C.Comprobante +':'+' ' + NCP.Observacion ,
'Entrada'=0.00,
'Salida'=DNP.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= NCP.Anulado,
'FechaAnulado'=convert(date, NCP.FechaAnulacion)

From VNotaCreditoProveedor NCP
join DetalleNotaCreditoProveedor DNP on NCP.IDTransaccion=DNP.IDTransaccion 
Join Producto P on DNP.IDProducto = P.ID
join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor=NCP.IDTransaccion And DNP.ID=NCPC.ID
join VCompra C on C.IDTransaccion=NCPC.IDTransaccionEgreso
Join Transaccion T On NCP.IDTransaccion=T.ID

union all

--Movimiento Salida
Select
DM.IDProducto,
'Producto'=P.Descripcion,
P.IDTipoProducto,
'Referencia'=P.Referencia,
'Fecha'=convert(date, M.Fecha),
'Fec'=convert (varchar(50),M.Fecha,10),
'Movimiento'=M.Motivo,
'Operación'=M.Operacion+': '+ M.CodigoSucursalOperacion +' '+ CONVERT ( varchar (30), M.Numero)+ '  ' + TipoComprobante + ' - ' +convert (Varchar (30),M.NroComprobante) ,
'Tipo'= M.Operacion,
M.[Cod.],
M.NroComprobante,
M.IDDepositoSalida,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=0.00,
'Salida'=DM.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulado'=convert(date, M.FechaAnulacion)

From VMovimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
join Producto P on DM.IDProducto = P.ID
Join Deposito D On DM.IDDepositoSalida=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Where M.Entrada='False'and M.Salida='True'

Union All 

--Movimiento Entrada Anulado

Select
DM.IDProducto,
'Producto'=P.Descripcion,
P.IDTipoProducto,
'Referencia'=P.Referencia,
'Fecha'= convert(date, M.FechaAnulacion),
'Fec'=convert (varchar(50),M.FechaAnulacion ,10),
'Movimiento'=M.Motivo + ' ' + 'ANULADA',
'Operación'=M.Operacion+' '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero)+' '+ 'ANULADA',
'Tipo'=M.Operacion +' '+ 'ANULADA',
M.[Cod.],
M.NroComprobante,
M.IDDepositoEntrada,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=0.00,
'Salida'=DM.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulado'=convert(date, M.FechaAnulacion)

From VMovimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion
join Producto P on DM.IDProducto = P.ID
Join Deposito D On DM.IDDepositoEntrada=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Where M.Entrada='True'And M.Salida='False'
And M.Anulado = 'True'

Union all

--Transferencia Salida
Select
DM.IDProducto,
'Producto'=P.Descripcion,
P.IDTipoProducto,
'Referencia'=P.Referencia,
'Fecha'=convert(date, M.Fecha),
'Fec'=convert (varchar(50),M.Fecha,10),
'Movimiento'=M.Motivo,
--'Operación'=M.Operacion +' '+M.NroComprobante+': '+ M.CodigoSucursalOperacion +' '+ CONVERT ( varchar (30), M.Numero) ,
'Operación'=M.Operacion+' '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero)+ ' ' + TipoComprobante + '-' +convert (Varchar (30),M.NroComprobante) ,
'Tipo'=M.Operacion,
M.[Cod.],
M.NroComprobante,
M.IDDepositoSalida ,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=0.00,
'Salida'=DM.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulado'=convert(date, M.FechaAnulacion)

From VMovimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
join Producto P on DM.IDProducto = P.ID
Join Deposito D On DM.IDDepositoSalida=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Where M.Entrada='True'and M.Salida='True'

Union All

--Transferencia Entrada Anulada
Select
DM.IDProducto,
'Producto'=P.Descripcion,
P.IDTipoProducto,
'Referencia'=P.Referencia,
'Fecha'=convert(date, M.FechaAnulacion ),
'Fec'=convert (varchar(50),M.FechaAnulacion ,10),
'Movimiento'=M.Operacion+' '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero)+' '+ 'ANULADA',
'Operación'=M.Operacion+' '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero)+' '+ 'ANULADA',
'OP'=M.Operacion,
M.[Cod.],
M.NroComprobante,
M.IDDepositoEntrada ,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=0.00,
'Salida'=DM.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulado'=convert(date, M.FechaAnulacion)

From VMovimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
join Producto P On DM.IDPRoducto = P.ID
Join Deposito D On DM.IDDepositoEntrada=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Where M.Entrada='True'And M.Salida='True'
And M.Anulado = 'True'

union all

--Ajuste Inventario Negativo
Select 
EDI.IDProducto,
EDI.Producto,
EDI.IDTipoProducto,
'Referencia'=EDI.Ref,
'Fecha'=convert(date, V.FechaAjuste),
'Fec'=convert (varchar(50),V.FechaAjuste,10),
'Movimiento'='AJUSTE NEGATIVO',
'Operación'='AJUSTE INVENTARIO NEGATIVO ' + V.Suc + ': '  + convert(varchar(50), V.Num),
'Tipo'='SALIDA',
'Cod.'='',
'NroComprobante'='',
EDI.IDDeposito,
EDI.IDSucursal,
'Cliente/Proveedor'= (Case When V.Observacion = Null Then '---' Else (Case When V.Observacion='' Then '---' Else V.Observacion End) End)  ,
'ComprobanteRelacionado'='---',
'Entrada'=0.00,
'Salida'=EDI.DiferenciaSistema * -1,
'Saldo'=0.00,
'Existencia'=0.00,
--'Hora'=CONVERT(time, V.FechaAjuste),
'Hora'=CONVERT(time, V.FechaTransaccion),
'FechaOperacion'=CONVERT(date, V.FechaAjuste),
'Hora Ope.'=Convert(varchar(10), V.FechaAjuste, 8),
'Fec. Ope.'=Convert(varchar(10), V.FechaAjuste, 5),
'Anulado'= 'False',
'FechaAnulado'=convert(date, '19000101')

From VControlInventario V
 join VExistenciaDepositoInventario EDI on V.IDTransaccion=EDI.IDTransaccion 
 Join Transaccion T On V.IDTransaccion=T.ID
Where EDI.DiferenciaSistema < 0 And V.Procesado='True' And V.Anulado = 'False'

Union All

--Ajuste Inicial Negativo
Select 
D.IDProducto,
D.Producto,
0,
'Referencia'=D.Referencia,
'Fecha'=convert(date, A.Fecha),
'Fec'=convert (varchar(50),A.Fecha,10),
'Movimiento'='AJUSTE INICIAL NEGATIVO',
'Operación'='AJUSTE Nro: ' + CONVERT(varchar(50), A.Numero),
'Tipo'= 'SALIDA',
'Cod.'='',
'NroComprobante'=convert(varchar(50), A.Numero),
D.IDDeposito,
D.IDSucursal,
'Cliente/Proveedor'='AJUSTE INICIAL NEGATIVO',
'ComprobanteRelacionado'=A.Observacion,
'Entrada'=0.00,
'Salida'=D.Salida,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= 'False',
'FechaAnulado'=convert(date, '19000101')

From AjusteInicial A 
join VDetalleAjusteInicial D on A.IDTransaccion=D.IDTransaccion 
Join Transaccion T On A.IDTransaccion=T.ID
Where A.Procesado='True'
And D.Salida>0

union all

--Ticket de Bascula
Select 
C.IDProducto,
C.Producto,
C.IDTipoProducto,
'Referencia'=C.ReferenciaProducto,
'Fecha'=convert(date, C.FechaAnulacion),
'Fec'=convert (varchar(50),C.FechaAnulacion,5),
'Movimiento'='TICKET DE BASCULA',
'Operación'='TIBA: ' + convert (varchar(50), C.Numero),
'Tipo'= 'TICKET DE BASCULA',
'Cod.'=C.TipoComprobante,
'NroComprobante'=convert(varchar(50), C.NroComprobante),
C.IDDeposito,
C.IDSucursal,
'Cliente/Proveedor'=C.Proveedor,
'ComprobanteRelacionado'='',
'Entrada'=0.00,
'Salida'=C.PesoBascula,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, C.FechaAnulacion),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), C.Fecha, 5),
'Anulado'= C.Anulado,
'FechaAnulado'=convert(date, C.FechaAnulacion)

From VTicketBascula C 
Join Transaccion T On C.IDTransaccion=T.ID
And C.Anulado = 'True' And C.Procesado = 'True'



















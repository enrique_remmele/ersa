﻿CREATE View [dbo].[VResumenCobranzaDiariaFormaPago]
As
Select 
'Fecha'=Convert(Date, C.FechaEmision),
'TipoComprobante'='EFE',
C.IDSucursal,
'Cantidad'=COUNT(*),
'Valores'=SUM(FP.Importe),
'Planilla'=C.NroPlanilla,
C.IDCobrador
From FormaPago FP
Join CobranzaCredito C On FP.IDTransaccion=C.IDTransaccion
Where FP.Efectivo='True' And C.Anulado='False'
Group By C.FechaEmision, C.IDSucursal,C.NroPlanilla,C.IDCobrador

Union All

Select 
'Fecha'=Convert(Date, C.FechaEmision),
'TipoComprobante'=CHQ.CodigoTipo,
C.IDSucursal,
'Cantidad'=COUNT(*),
'Valores'=SUM(FP.ImporteCheque),
'Planilla'=C.NroPlanilla,
C.IDCobrador
From FormaPago FP
Join CobranzaCredito C On FP.IDTransaccion=C.IDTransaccion
Join VChequeCliente CHQ On FP.IDTransaccionCheque=CHQ.IDTransaccion
Where FP.Cheque='True' And C.Anulado='False'
Group By C.FechaEmision, C.IDSucursal,C.NroPlanilla,CHQ.CodigoTipo,C.IDCobrador

Union All

--Documento
Select 
'Fecha'=Convert(Date, C.FechaEmision),
'TipoComprobante'=TC.Codigo,
C.IDSucursal,
'Cantidad'=COUNT(*),
'Valores'=SUM(FP.ImporteDocumento),
'Planilla'=C.NroPlanilla,
C.IDCobrador
From FormaPago FP
Join CobranzaCredito C On FP.IDTransaccion=C.IDTransaccion
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Where FP.Documento='True'  And C.Anulado='False'
Group By C.FechaEmision, C.IDSucursal,C.NroPlanilla,TC.Codigo,C.IDCobrador






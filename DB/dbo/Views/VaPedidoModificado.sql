﻿
CREATE VIEW [dbo].[VaPedidoModificado]
as 
select 
a.idtransaccion,
a.FechaFacturar,
a.EntregaCliente,
a.IDUsuarioModificacion,
'FechaTransaccion'=a.FechaModificacion,
a.FechaModificacion,
u.Usuario 
from aPedidoModificado a
 join Usuario u on a.idusuariomodificacion = u.id

﻿CREATE View [dbo].[VVentasParaLote]
As

Select

V.IDTransaccion,
V.IDSucursal,
'Suc'=S.Descripcion,
V.Comprobante,
'FechaFact'=V.FechaEmision,
V.Saldo,
'Condicion'=(Case When (V.Credito) = 'True' Then 'CRED' Else 'CONT' End),
M.Referencia AS Moneda,
'Vendedor'=ISNULL((Select TOP 1 VE.Nombres From Vendedor VE Where VE.ID=V.IDVendedor), '---'),

V.IDCliente,
'Cliente'=C.RazonSocial,
'Suc. Cliente'=(Case When (V.EsVentaSucursal)='True' Then (Select CS.Sucursal From ClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else 'MATRIZ' End),
'ZonaVenta'=(Case When (V.EsVentaSucursal)='True' Then (Select CS.ZonaVenta From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else (Select IsNull((Select TOP 1 ZV.Descripcion From ZonaVenta ZV Where ZV.ID=C.IDZonaVenta), '')) End),
'Ciudad'=(Case When (V.EsVentaSucursal)='True' Then (Select CS.Ciudad From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else (Select IsNull((Select TOP 1 CC.Descripcion From Ciudad CC Where CC.ID=C.IDCiudad), '')) End),
'Barrio'=(Case When (V.EsVentaSucursal)='True' Then (Select CS.Barrio From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else (Select IsNull((Select TOP 1 B.Descripcion From Barrio B Where B.ID=C.IDBarrio), '')) End),
'Direccion'=(Case When (V.EsVentaSucursal)='True' Then (Select CS.Direccion From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Direccion End),
'Telefono'=(Case When (V.EsVentaSucursal)='True' Then (Select CS.Telefono From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Telefono End),
'Contacto'=(Case When (V.EsVentaSucursal)='True' Then (Select CS.Contacto From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.NombreFantasia End),

'Latitud'=(Case When (V.EsVentaSucursal)='True' Then (Select CS.Latitud From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Latitud End),
'Longitud'=(Case When (V.EsVentaSucursal)='True' Then (Select CS.Longitud From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Longitud End),

'IDSucursalCliente'= (Case When (V.EsVentaSucursal)='True' Then V.IDSucursalCliente Else 0 End),

--Totales
'Contado'=Case When V.Credito='True' Then 0 Else V.Saldo End,
'Credito'=Case When V.Credito='True' Then V.Saldo Else 0 End,
'Total'=V.Total,
'TotalImpuesto'=V.TotalImpuesto,
'TotalDiscriminado'=V.TotalDiscriminado,
'TotalDescuento'=V.TotalDescuento,
'Peso'= IsNull((select Sum((P.Peso * DV.Cantidad)) as Peso From DetalleVenta DV Join Producto P On P.ID = DV.IDProducto Where IDTransaccion = V.IDTransaccion), 0)

From Venta V
Join Cliente C On V.IDCliente = C.ID
Left Outer Join PuntoExpedicion PE On V.IDPuntoExpedicion=PE.ID
Left Outer Join Sucursal S ON PE.IDSucursal = S.ID
Left Outer Join Moneda M On V.IDMoneda=M.ID

Where YEAR(FechaEmision) > 2013
AND dbo.FVentaHabilitadaParaLote(V.IDTransaccion)='True' 

--And DATEDIFF(dd, GetDate(), FechaEmision) Between -10 And 2 --JGR 20140822 Controla limite de dias para no traer todo




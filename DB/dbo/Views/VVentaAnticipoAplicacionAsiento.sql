﻿Create View [dbo].[VVentaAnticipoAplicacionAsiento]
As

Select 
AP.IDTransaccionCobranza,
CV.IDtransaccionVenta,
AP.IDtransaccion,
'Importe'= CV.Importe * V.Cotizacion
From Venta V
Join AnticipoVenta CV on CV.IDTransaccionVenta = V.IDTransaccion
Join AnticipoAplicacion AP on AP.IDTransaccion = CV.IDTransaccionAnticipo
Join CobranzaCredito CC on AP.IDTransaccionCobranza = CC.IDTransaccion




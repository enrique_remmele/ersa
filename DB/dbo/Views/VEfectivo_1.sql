﻿

CREATE View [dbo].[VEfectivo]
As
Select 

E.IDTransaccion,
E.ID,
E.IDTipoComprobante,
'TipoComprobante'=TC.Descripcion,
'CodigoComprobante'=TC.Codigo,
E.IDSucursal,
E.Comprobante,
E.Numero,
E.Fecha,
'Fec'=CONVERT(varchar(50), E.Fecha, 6),
'VentasCredito'=(Case When (E.VentasCredito) != 0 Then 'True' Else 'False' End),
'VentasContado'=(Case When (E.VentasContado) != 0 Then 'True' Else 'False' End),
'Gastos'=(Case When (E.Gastos) != 0 Then 'True' Else 'False' End),
'Tipo'=(Case When (E.VentasCredito) != 0 Then 'VENTA CREDITO' Else (Case When (E.VentasContado) != 0 Then 'VENTA CONTADO' Else (Case When (E.Gastos) != 0 Then 'GASTO' Else '---' End) End) End),
E.IDMoneda,
'Moneda'=M.Referencia,
M.Decimales,
E.ImporteMoneda,
E.Cotizacion,
'Importe'=E.VentasCredito + E.VentasContado + E.Gastos,
E.Total,
E.Observacion,
E.Depositado,
'Saldo'=ISNULL(E.Saldo,0),
E.Cancelado,
'Estado'=(Case When (E.Cancelado) = 'True' Then 'Cancelado' Else 'Pendiente' End),
E.Habilitado,
'Habilitado Pago'= (Case when (E.Habilitado)='True' Then 'SI' Else '---' End),
'Select'=(Case when (E.Habilitado)='True' Then 'True' Else 'False' End),
'Generado'=O.Descripcion,
'ImporteHabilitado'=ISNUll(E.ImporteHabilitado,E.Saldo),
CC.IDCobrador,
CC.Cobrador,

--Motivo
E.IDMotivo,
'Motivo'=ME.Descripcion,

--Cobranza 
'NroOperacionCobranza'=CC.Numero,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
E.Anulado,
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=CC.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=CC.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=CC.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=CC.IDTransaccion),
'Cliente'=ISNULL(CC.Cliente,'COBR.POR LOTE'),
'Cobranza'=(Case when CC.IDCliente is not null then concat(CC.Suc,'-',CC.NUmero) else (Select concat(CO.Suc,'-',CO.Numero) from vCobranzaContado CO where CO.IDTransaccion = E.IDTransaccion) end )

From Efectivo E
JOin Transaccion T On E.IDTransaccion=T.ID
Join Operacion O On T.IDOperacion=O.ID
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID
Left Outer Join VCobranzaCredito  CC On CC.IDTransaccion=E.IDTransaccion
Left Outer Join MotivoEfectivo ME On E.IDMotivo=ME.ID
























﻿
CREATE View [dbo].[VPedidoExcesoLineaCredito]
As


Select 
P.Suc,
P.Numero,
P.Fecha,
P.FechaFacturar,
P.Cliente,
P.Moneda,
P.Total,
'AprobarPor' = 'CHEQUE RECHAZADO',
C.LimiteCredito,
C.SaldoCredito,
'Diferencia'= (P.Total * P.Cotizacion) - C.SaldoCredito,
'Porcentaje' = (Case when C.LimiteCredito > 1 then (Case when C.SaldoCredito > 0 then ((((P.Total * P.Cotizacion) - C.SaldoCredito)*100)/C.LimiteCredito) else ((((P.Total * P.Cotizacion))*100)/C.LimiteCredito) end ) else 0 end),
--'Porcentaje' = (Case when C.SaldoCredito > 0 then ((((P.Total * P.Cotizacion) - C.SaldoCredito)*100)/C.LimiteCredito) else ((((P.Total * P.Cotizacion))*100)/C.LimiteCredito) end ) ,
'Aprobado'=(Case when AutorizacionLineaCredito = 1 then 'APROBADO' else 
			(Case when AutorizacionLineaCredito = 0 then 'RECHAZADO' else
			'PENDIENTE' end) End),
P.IDTransaccion,
P.IDCliente,
'Aprobado por'= IsNull((Select Nombre from Usuario where ID = IDUsuarioAutorizacionLineaCredito and P.Aprobado = 'True'),''),
P.Atraso

From VPedido P
Join VCliente C on C.ID = P.IDCliente
Where P.Anulado = 'False'
and P.Facturado = 'False'
and P.IDFormaPagoFactura not in (select id from FormaPagoFactura where CancelarAutomatico = 1)
and C.ID in (select distinct IDCliente from ChequeCliente where rechazado = 1 and saldoacuenta > 0)

union all

Select 
P.Suc,
P.Numero,
P.Fecha,
P.FechaFacturar,
P.Cliente,
P.Moneda,
P.Total,
'AprobarPor' = 'DOBLE CONTADO',
C.LimiteCredito,
C.SaldoCredito,
'Diferencia'= (P.Total * P.Cotizacion) - C.SaldoCredito,
--'Porcentaje' = (Case when C.SaldoCredito > 0 then ((((P.Total * P.Cotizacion) - C.SaldoCredito)*100)/C.LimiteCredito) else 0 end ) ,
'Porcentaje' = (Case when C.LimiteCredito > 1 then (Case when C.SaldoCredito > 0 then ((((P.Total * P.Cotizacion) - C.SaldoCredito)*100)/C.LimiteCredito) else ((((P.Total * P.Cotizacion))*100)/C.LimiteCredito) end ) else 0 end),
'Aprobado'=(Case when AutorizacionLineaCredito = 1 then 'APROBADO' else 
			(Case when AutorizacionLineaCredito = 0 then 'RECHAZADO' else
			'PENDIENTE' end) End),
P.IDTransaccion,
P.IDCliente,
'Aprobado por'= IsNull((Select  Nombre from Usuario where ID = IDUsuarioAutorizacionLineaCredito and P.Aprobado = 'True'),''),
P.Atraso

From VPedido P
Join VCliente C on C.ID = P.IDCliente
Where P.Anulado = 'False'
and P.Facturado = 'False'
and P.Credito = 'False'
and P.IDFormaPagoFactura not in (select id from FormaPagoFactura where CancelarAutomatico = 1)
and C.ID not in (select distinct IDCliente from ChequeCliente where rechazado = 1 and saldoacuenta > 0)
and (select count(*) from venta where credito =0 and anulado = 0 and procesado = 1
		and saldo >1 and IDCliente = p.IDCLiente and FechaEmision < P.FechaFacturar) > 0

union all

Select 
P.Suc,
P.Numero,
P.Fecha,
P.FechaFacturar,
P.Cliente,
P.Moneda,
P.Total,
'AprobarPor' = 'EXCESO LINEA DE CREDITO',
C.LimiteCredito,
C.SaldoCredito,
'Diferencia'= (P.Total * P.Cotizacion) - C.SaldoCredito,
--'Porcentaje' = (Case when C.SaldoCredito > 0 then ((((P.Total * P.Cotizacion) - C.SaldoCredito)*100)/C.LimiteCredito) else 0 end ) ,
'Porcentaje' = (Case when C.SaldoCredito > 0 then ((((P.Total * P.Cotizacion) - C.SaldoCredito)*100)/C.LimiteCredito) else ((((P.Total * P.Cotizacion))*100)/C.LimiteCredito) end ) ,
'Aprobado'=(Case when AutorizacionLineaCredito = 1 then 'APROBADO' else 
			(Case when AutorizacionLineaCredito = 0 then 'RECHAZADO' else
			'PENDIENTE' end) End),
P.IDTransaccion,
P.IDCliente,
'Aprobado por'= IsNull((Select  Nombre from Usuario where ID = IDUsuarioAutorizacionLineaCredito and P.Aprobado = 'True'),''),
P.Atraso

From VPedido P
Join VCliente C on C.ID = P.IDCliente
Where P.Anulado = 'False'
and P.Facturado = 'False'
and C.SaldoCredito < (P.Total * P.Cotizacion)
and P.Credito = 'True'
--and P.Atraso <= C.Tolerancia
and P.IDFormaPagoFactura not in (select id from FormaPagoFactura where CancelarAutomatico = 1)
and C.ID not in (select distinct IDCliente from ChequeCliente where rechazado = 1 and saldoacuenta > 0)
and  (select count(*) from venta where credito =0 and anulado = 0 and procesado = 1
		and saldo >1 and IDCliente = p.IDCLiente and FechaEmision < P.FechaFacturar) = 0

union all

Select 
P.Suc,
P.Numero,
P.Fecha,
P.FechaFacturar,
P.Cliente,
P.Moneda,
P.Total,
'AprobarPor' = 'FACTURA ATRASADA',
C.LimiteCredito,
C.SaldoCredito,
'Diferencia'= (P.Total * P.Cotizacion) - C.SaldoCredito,
'Porcentaje' = (Case when C.LimiteCredito > 1 then (Case when C.SaldoCredito > 0 then ((((P.Total * P.Cotizacion) - C.SaldoCredito)*100)/C.LimiteCredito) else ((((P.Total * P.Cotizacion))*100)/C.LimiteCredito) end ) else 0 end),
--'Porcentaje' = (Case when C.SaldoCredito > 0 then ((((P.Total * P.Cotizacion) - C.SaldoCredito)*100)/C.LimiteCredito) else ((((P.Total * P.Cotizacion))*100)/C.LimiteCredito) end ) ,
'Aprobado'=(Case when AutorizacionLineaCredito = 1 then 'APROBADO' else 
			(Case when AutorizacionLineaCredito = 0 then 'RECHAZADO' else
			'PENDIENTE' end) End),
P.IDTransaccion,
P.IDCliente,
'Aprobado por'= IsNull((Select Nombre from Usuario where ID = IDUsuarioAutorizacionLineaCredito and P.Aprobado = 'True'),''),
P.Atraso

From VPedido P
Join VCliente C on C.ID = P.IDCliente
Where P.Anulado = 'False'
and P.Facturado = 'False'
and C.SaldoCredito >= (P.Total * P.Cotizacion)
and P.Atraso >= C.Tolerancia
and P.IDFormaPagoFactura not in (select id from FormaPagoFactura where CancelarAutomatico = 1)
and C.ID not in (select distinct IDCliente from ChequeCliente where rechazado = 1 and saldoacuenta > 0)
and (select count(*) from venta where credito =0 and anulado = 0 and procesado = 1
		and saldo >1 and IDCliente = p.IDCLiente and FechaEmision < P.FechaFacturar) =0















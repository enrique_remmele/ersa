﻿






CREATE View [dbo].[VNotaDebitoVenta]

AS

Select

--Venta
'IDTransaccion'=NDV.IDTransaccionNotaDebito, 
'IDTransaccionVenta'=V.IDTransaccion , 
V.Comprobante,
V.IDCliente,
V.Saldo,
V.FechaEmision,

--Nota de Credito
ND.Fecha,
'Comp. ND'=ND.[Cod.] + ' ' + ND.Comprobante,

'Usuario ND'=ND.Usuario, 
'Total'=ROUND(ND.Total,0),
ND.DeBito ,
ND.Reposicion ,
'Importe'=(Case When (ND.Anulado) = 'True' Then 0 Else NDV.Importe End),
'IDTransaccionNotaCreditoAplicacion'=0,
ND.Observacion,
'Aplicado'='True',
'Anulado'=ND.Anulado

From NotaDebitoVenta  NDV
Join VNotaDebito ND On NDV.IDTransaccionNotaDebito=ND.IDTransaccion
join VVenta  V on NDV.IDTransaccionVentaGenerada  = V .IDTransaccion 
Where ND.Aplicar = 'False'

Union All

Select
NDVA.IDTransaccionNotaDebito, 
V.IDTransaccion, 
V.Comprobante,
V.IDCliente,
V.Saldo,
V.FechaEmision,

--Nota de Credito
'Fecha'= (Select Fecha From NotaDebito ND Where ND.Idtransaccion= NDVA.IDTransaccionNotaDebito),
'Comp. ND'=NDA.[Cod.] + ' (' + Convert(varchar(50), NDA.Numero) + ') ' + NDA.Comprobante,
'Usuario ND'=NDA.Usuario,

'Total'=ROUND(ND.Total,0),
ND.DeBito ,
ND.Reposicion ,
'Importe'=(Case When (NDA.Anulado) = 'True' Then 0 Else NDVA.Importe End),
NDVA.IDTransaccionNotaDebitoAplicacion ,
NDA.Observacion,
'Aplicado'='True',
'Anulado'=NDA.Anulado
From NotaDebitoVentaAplicada   NDVA
join VVenta  V on NDVA.IDTransaccionVenta  = V .IDTransaccion 
--No se si es correcto el Join con Nota de Credito...
Join VNotaDebito ND On NDVA.IDTransaccionNotaDebito=ND.IDTransaccion
Join VNotaDebitoAplicacion NDA On NDVA.IDTransaccionNotaDebitoAplicacion=NDA.IDTransaccion









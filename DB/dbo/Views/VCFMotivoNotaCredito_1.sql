﻿Create view VCFMotivoNotaCredito 
as 
select
'IDSubMotivoNotaCredito'=M.ID,
'SubMotivo'=M.Descripcion,
M.Motivo,
CC.IDCuentaContable,
'CodigoCuentaContable'=CC.Codigo,
'CuentaContable'=CC.Descripcion,
'IDTipoProducto'=TP.ID,
'TipoPRoducto'= TP.Descripcion

from 
CFMotivoNotaCredito CF
Right join SubMotivoNotaCredito M on CF.IDSUbMotivoNotaCREdito = M.ID
left outer join vCuentaContable CC on CF.IDCuentaContable = CC.ID and CC.PlanCuentaTitular = 1
left outer join TipoProducto TP on CF.IDTipoProducto = TP.ID

﻿CREATE View [dbo].[VDetalleImpuestoDesglosadoTotales]

As

Select 
IDTransaccion, 
'GRAVADO10%'=IsNull([1],0), 
'GRAVADO5%'=IsNull([2],0), 
'EXENTO'=IsNull([3],0),
'DISCRIMINADO10%'=IsNull([11],0),
'IVA10%'=IsNull([21],0),
'DISCRIMINADO5%'=IsNull([12],0),
'IVA5%'=IsNull([22],0)

From (SELECT IDTransaccion, 
			IDImpuesto, 
			IDImpuesto+10 as IDImpuesto2,
			IDImpuesto+20 as IDImpuesto3,
			SUM(Total) As Total, 
			Sum(TotalDiscriminado) as TotalDiscriminado,
			Sum(TotalImpuesto) as TotalImpuesto
FROM VDetalleImpuesto Group By IDTransaccion, IDImpuesto) AS SourceTable


PIVOT
(
	Sum(Total)
	FOR IDImpuesto IN ([1], [2], [3])
) 

AS P1

PIVOT
(
	Sum(TotalDiscriminado)
	FOR IDImpuesto2 IN ([11], [12])
) 

AS P2

PIVOT
(
	Sum(TotalImpuesto)
	FOR IDImpuesto3 IN ([21], [22])
) 

AS P3





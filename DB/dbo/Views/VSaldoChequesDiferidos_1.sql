﻿CREATE View [dbo].[VSaldoChequesDiferidos]
As
Select
'Fecha'=V.FechaCobranza,
'Dia'=dbo.FDiaSemana(DATEPART(dw , V.FechaCobranza)),
'Ingreso'=V.Importe,
'Deposito'=0,
'DescuentoCheque'=0,
'Saldo'=0,
v.IDSucursal ,
v.[Cond.] 
From VChequeClienteVenta V

Union All

Select
V.FechaCobranza,
'Dia'=dbo.FDiaSemana(DATEPART(dw, V.Fecha)),
'Ingreso'=0,
'Deposito'=V.Importe,
'DescuentoCheque'=0,
'Saldo'=0,
v.IDsucursal ,
v.[Cond.] 
From VChequesDiferidosDepositados V
Where DescuentoCheque='False'

Union All

Select
V.FechaCobranza,
'Dia'=dbo.FDiaSemana(DATEPART(dw, V.Fecha)),
'Ingreso'=0,
'Deposito'=0,
'DescuentoCheque'=V.Importe,
'Saldo'=0,
v.IDSucursal ,
v.[Cond.] 
From VChequesDiferidosDepositados V
Where DescuentoCheque='True'






﻿
CREATE View [dbo].[VMeta]
As

Select 
M.ID,
Mes,
'MesDescripcion'=(Case when Mes = 1 then 'ENERO'
				   when Mes = 2 then 'FEBRERO'	
				   when Mes = 3 then 'MARZO'
				   when Mes = 4 then 'ABRIL'
				   when Mes = 5 then 'MAYO'
				   when Mes = 6 then 'JUNIO'
				   when Mes = 7 then 'JULIO'
				   when Mes = 8 then 'AGOSTO'
				   when Mes = 9 then 'SEPTIEMBRE'
				   when Mes = 10 then 'OCTUBRE'
				   when Mes = 11 then 'NOVIEMBRE'
				   when Mes = 12 then 'DICIEMBRE' end),
Año,
M.IDSucursal,
IDVendedor,
M.IDTipoProducto,
M.IDUnidadMedida,
IDProducto,
IDListaPrecio,
Importe,
M.Estado,
'Sucursal'= S.Codigo,
'Vendedor'= V.Nombres,
'TipoProducto'= TP.Descripcion,
'UnidadMedida'= UM.referencia,
'ReferenciaProducto'= P.Referencia,
'Producto'= P.Descripcion,
'ListaPrecio' =  LP.Lista,
'MetaKg'=Isnull(M.MetaKg,0)
From Meta M
Join Sucursal S on S.ID = M.IDSucursal
Join Vendedor V on V.ID = M.IDVendedor
Join TipoProducto TP on TP.ID = M.IDTipoProducto
Join UnidadMedida UM on UM.ID = M.IDUnidadMedida
Join Producto P on P.ID = M.IDProducto
Join vListaPrecio LP on LP.ID = M.IDListaPrecio





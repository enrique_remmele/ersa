﻿CREATE View [dbo].[VDetalleMacheo]
As
Select 
DM.*,
T.IDTransaccion,
T.PesoBascula,
T.PesoRemision,
T.Diferencia,
T.Cotizacion,
T.NroAcuerdo,
T.Numero,
T.Fecha,
T.Total,
T.TotalUS,
'ReferenciaTipoComprobante'= TC.Codigo,
'TipoComprobante'= TC.Descripcion,
'ReferenciaProveedor'=P.Referencia,
'Proveedor'=P.RazonSocial,
'ReferenciaSucursal'= S.Codigo,
'Sucursal'= S.Descripcion,
'ReferenciaMoneda'=M.Referencia,
'Moneda'=M.Descripcion,
'Deposito'= D.Descripcion,
'ReferenciaProducto'=Pr.Referencia,
'Producto'= Pr.Descripcion,
'Gasto'= IsNull((Select Comprobante from VGasto where IDTransaccion = T.IDTransaccionGasto),''),
'Impuesto'= I.Descripcion,
'ReferenciaImpuesto'= I.Referencia,
--Transaccion
'FechaTransaccion'=TR.Fecha,
'IDDepositoTransaccion'=TR.IDDeposito,
'IDSucursalTransaccion'=TR.IDSucursal,
'IDTerminalTransaccion'=TR.IDTerminal,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=TR.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=TR.IDUsuario),
--Anulacion
'UsuarioAnulacion'=ISnull((Select U.Usuario From Usuario U Where U.ID=T.IDUsuarioAnulacion),'---'),
'UsuarioIdentificacionAnulacion'=ISnull((Select U.Identificador From Usuario U Where U.ID=T.IDUsuarioAnulacion),'---'),
'Chofer' = CP.Nombres,
'Camion'= CA.Descripcion,
'Chapa'=CA.Patente,
T.IDProducto,
T.IDMoneda,
T.IDCamion,
T.IDChofer,
T.IDProveedor,
T.IDDeposito

From DetalleMacheo DM
Join TicketBascula T on T.IDTransaccion = DM.IDTransaccionTicket
Join Sucursal S on S.ID = T.IDSucursal
Join TipoComprobante TC on TC.ID = T.IDTipoComprobante
Join Proveedor P on P.ID = T.IDProveedor
Join Moneda M on M.ID = T.IDMoneda
Join Deposito D on D.ID = T.IDDeposito
Join Producto Pr on Pr.ID = T.IDProducto
Join Impuesto I on I.ID = T.IDImpuesto
Join Transaccion TR on TR.ID = T.IDTransaccion
Join CamionProveedor CA on CA.ID = T.IDCamion
Join ChoferProveedor CP on CP.ID = T.IDChofer











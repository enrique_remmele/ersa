﻿CREATE View [dbo].[VGastoProducto2]
As
Select 
G.*,
GP.NroAcuerdo,
GP.Cantidad,
A.IDProducto,
Pr.IDTipoProducto,
'Producto'=Pr.Descripcion,
'IDTransaccionMacheo'=Isnull(M.IDTransaccion, 0),
'FechaMacheo'=M.Fecha,
'NumeroMacheo'=ISnull(M.Numero,0)

From VGasto G
join GastoProducto GP on G.IDTransaccion = GP.IDTransaccionGasto
Join Acuerdo A on GP.NroAcuerdo = A.NroAcuerdo
Join vProducto Pr on A.IDProducto = Pr.ID
left outer join MacheoFacturaTicket M on G.IDTransaccion = M.IDTransaccionGasto













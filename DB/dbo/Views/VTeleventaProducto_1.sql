﻿
CREATE View [dbo].[VTeleventaProducto]
As
Select 
DV.IdProducto,
P.Referencia,
P.Descripcion,
V.IdCliente,
'UltimaCompra'=(Select Max(FechaEmision) from DetalleVenta 
Join Venta on  Venta.Idtransaccion = DetalleVenta.Idtransaccion
where IdProducto = DV.IdProducto
and IdCliente = V.IdCliente),
'UltimoPrecio'=(select top(1) PrecioUnitario from Venta 
join DetalleVenta on Venta.IdTransaccion = DetalleVenta.Idtransaccion
Join Cliente Cliente on Venta.IdCliente = Cliente.Id
where Venta.idcliente = V.IdCliente
and DetalleVenta.idproducto = DV.IDProducto
order by DetalleVenta.idtransaccion desc),
'UltimaCantidad'=(select top(1) Cantidad from Venta 
join DetalleVenta on Venta.IdTransaccion = DetalleVenta.Idtransaccion
Join Cliente Cliente on Venta.IdCliente = Cliente.Id
where Venta.idcliente = V.IdCliente
and DetalleVenta.idproducto = DV.IDProducto
order by DetalleVenta.idtransaccion desc),
'Total'=(select sum(Cantidad) 
		 from DetalleVenta join Venta on Venta.Idtransaccion = DetalleVenta.Idtransaccion
		 where DetalleVenta.IDProducto = DV.IDProducto and Venta.Idcliente = V.IdCliente),
'Maximo'=(select max(Cantidad) 
		 from DetalleVenta join Venta on Venta.Idtransaccion = DetalleVenta.Idtransaccion
		 where DetalleVenta.IDProducto = DV.IDProducto and Venta.Idcliente = V.IdCliente),
'Promedio'=(select Round(sum(Cantidad)/Count(Venta.Idtransaccion),0) 
		 from DetalleVenta join Venta on Venta.Idtransaccion = DetalleVenta.Idtransaccion
		 where DetalleVenta.IDProducto = DV.IDProducto and Venta.Idcliente = V.IdCliente),
'Veces'=(select count(Venta.Idtransaccion) 
		 from DetalleVenta join Venta on Venta.Idtransaccion = DetalleVenta.Idtransaccion
		 where DetalleVenta.IDProducto = DV.IDProducto and Venta.Idcliente = V.IdCliente),
--V.IdVendedor,
'IDVendedor'=0,
--'Vendedor'=Vr.Nombres,
'Vendedor'='',
'Plazo'=C.PlazoCredito		 
from Venta V
Join DetalleVenta DV on DV.Idtransaccion = V.Idtransaccion
Join Producto P on P.Id = DV.IdProducto
Join Vendedor Vr on Vr.Id = V.Idvendedor
Join CLiente C on C.iD = V.Idcliente
Where V.Procesado = 1
and Anulado = 0
Group by DV.IdProducto,
P.Referencia,
P.Descripcion,
V.IdCliente,
--V.IdVendedor,
--Vr.Nombres,
C.PlazoCredito








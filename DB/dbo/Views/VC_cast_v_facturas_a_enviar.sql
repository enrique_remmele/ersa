﻿CREATE view [dbo].[VC_cast_v_facturas_a_enviar]
as  Select
'cod_cliente'=V.IDCliente,
'cod_sucursal'=V.IDSucursal,
'numero_factura'=V.Comprobante,
'monto_factura'=V.Total,
'fecha_factura'=V.Fec
From VVenta V
Where V.Procesado='True' And 
(Select Top(1) VL.IDTransaccionVenta From VentaLoteDistribucion VL Where VL.IDTransaccionVenta = V.IDTransaccion) Is Null
And V.Anulado = 'False'


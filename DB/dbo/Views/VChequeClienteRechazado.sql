﻿CREATE view [dbo].[VChequeClienteRechazado]
as
Select 
---Cabecera
C.IDTRansaccion ,
C.Numero,
C.Fecha ,
'Fec'= CONVERT (varchar(50),C.Fecha,6),
C.IDCuentaBancaria,
CB.CuentaBancaria,
C.IDTransaccionCheque, 
C.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
'Ciudad'=S.CodigoCiudad,
C.Observacion,
C.Anulado,
C.Conciliado,
--Cuenta BAncaria
CB.Banco,

--Cheque Cliente
'Referencia'=CC.CodigoCliente,
CC.Cliente,
CC.RUC,
CC.NroCheque,
CC.Moneda,
CC.Cotizacion,
CC.Importe,
CC.SaldoACuenta,
'FechaCheque'=CC.Fecha,

'IDMotivoRechazo'=MRC.ID,
'MotivoRechazo'=MRC.Descripcion,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'Usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=C.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=C.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=C.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=C.IDTransaccion),
CC.IDCliente

    

From ChequeClienteRechazado C
Join Transaccion T On C.IDTRansaccion =T.ID
Join VSucursal S On C.IDSucursal=S.ID   
Join VCuentaBancaria CB On C.IDCuentaBancaria=CB.ID 
Join VChequeCliente CC On CC.IDTransaccion= C.IDTransaccionCheque 
Left Outer Join MotivoRechazoCheque MRC On MRC.ID=C.IDMotivoRechazo



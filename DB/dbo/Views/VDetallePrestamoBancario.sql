﻿CREATE View [dbo].[VDetallePrestamoBancario]

As

Select 
DP.IDTransaccion,
P.NroComprobante,
DP.NroCuota,
'Cuota'=DP.NroCuota,
DP.FechaVencimiento,
DP.Amortizacion,
DP.Interes,
DP.Impuesto,
DP.ImporteCuota,
DP.Pagar,
'FechaPago'= cast(DP.FechaPago as date),
DP.ImporteAPagar,
DP.PagosVarios,
'ObservacionPago'=(Case When (DP.ObservacionPago) ='' Then '--' Else DP.ObservacionPago End),
--DP.TotalPagado,
'TotalPagado'=(Case When (DP.TotalPagado) Is Null Then 0 Else DP.TotalPagado End),
DP.Saldo,
DP.Cancelado,
'Cancel'=(Case When DP.Cancelado = 'True' Then 'Si' Else 'No' End),
'Estado'=(Case When DP.Cancelado = 'True' Then 'Pendiente' Else 'Cancelado' End),

--CuentaBancaria
--Transaccion
'FechaTransaccion'=TR.Fecha,
TR.IDDeposito,
TR.IDTerminal,
TR.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=TR.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=TR.IDUsuario),

--Cabecera
P.Numero,
P.TipoComprobante,
TC.Descripcion,
p.Anulado,
P.Fec,
P.Fecha,
P.IDCuentaBancaria,
P.CuentaBancaria,
P.IDBanco,
P.Banco,
P.IDMoneda,
P.Moneda,
P.Cotizacion,
P.PlazoDias,
P.PorcentajeInteres,
P.Capital,
'TotalInteres'=P.Interes,
P.ImpuestoInteres,
P.Gastos,
P.Neto,
'Observacion'=(Case When (P.Observacion) = '' Then '--' Else P.Observacion End),
P.TipoGarantia,
--Credito Bancario
'IDTransaccionDebitoCreditoBancario'=CB.IDTransaccion,
'Credito Bancario'=(Case When (CB.IDTransaccion) Is Null Then '' Else (CB.Suc + ' ' + Convert(varchar(10), CB.Numero) + ' - ' + CB.[Cod.] + ' ' + Convert(varchar(10), CB.Comprobante)) End),
'CreditoBancarioAsociado'=(Case When (CB.IDTransaccion) Is Null Then 'False' Else 'True' End),
'DebitoAutomatico'=IsNull(DP.DebitoAutomatico, 'True'),
P.IDSucursal,
'ReferenciaMoneda' =(Select Referencia from Moneda where ID = P.IDMoneda)

From DetallePrestamoBancario DP
Left outer Join VPrestamoBancario P On DP.IDTransaccion = P.IDTransaccion
Left outer Join Transaccion TR On DP.IDTransaccion=TR.ID
Join TipoComprobante TC On P.IDTipoComprobante = TC.ID
Left Outer Join VDebitoCreditoBancario CB On DP.IDTransaccionDebitoCreditoBancario=CB.IDTransaccion








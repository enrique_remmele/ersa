﻿
CREATE View [dbo].[VEquipoConteo]
As
Select 
EC.*,
'ZonaDeposito'= ZD.Descripcion,
ZD.CodigoSucursal,
ZD.Sucursal,
ZD.Deposito
From EquipoConteo EC
Join VZonaDeposito ZD On EC.IDZonaDeposito = ZD.ID


﻿Create view [dbo].[VCobranzaDiaria2]
as

--COBRANZA CREDITO

--EFECTIVO
Select
FP.IDTransaccion,
VDC.IDTransaccionCobranza,
'Fec'=convert(varchar(50),CC.FechaEmision,3),
'Fecha'=CC.FechaEmision,
VDC.IDCliente,
VDC.Cliente,
VDC.Referencia,
VDC.[Cod.],

--Cantidad de Comprobantes
'CantComprobantes'=0,

VDC.Cobrador,
VDC.IDCobrador,

VDC.IDVendedor,

VDC.IDSucursal,
VDC.IDDeposito,
VDC.IDTipoComprobante,
'ComprobanteFactura'=VDC.Comprobante,
'Cobranza'=VDC.Numero,
'FormaPago'='Efectivo',
'Moneda'=M.Referencia,
'Cambio'=E.Cotizacion,
'Banco'='---',
'CodigoBanco'='---',
'Comprobante'=E.Comprobante,
'FechaPago'='',
'TipoComprobante'=TC.Descripcion,
'Tipo'=TC.Codigo,

--Cantidad de Efectivos
'Cant'=0,

--Total Cobranza
'TotalCobrado'=CC.Total,

--Importe Efectivo
'Importe'=FP.Importe,

'CancelarCheque'=FP.CancelarCheque,
CC.Anulado,
VDC.Credito,
'Planilla'=convert(int,CC.NroPlanilla),
'Recibo'=CC.Comprobante

From FormaPago FP
Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID
Join VCobranzaCredito CC On CC.IDTransaccion=FP.IDTransaccion
Join VVentaDetalleCobranzaCredito VDC On VDC.IDTransaccionCobranza=FP.IDTransaccion
Where CC.Anulado = 'False'

Union All

--CHEQUE CLIENTE
Select
FP.IDTransaccion,
VDC.IDTransaccionCobranza,
'Fec'=convert(varchar(50),CC.FechaEmision,3),
'Fecha'=CC.FechaEmision,
VDC.IDCliente,
VDC.Cliente,
VDC.Referencia,
VDC.[Cod.],

--Cantidad Comprobantes
'CantComprobantes'=0,

VDC.Cobrador,
VDC.IDCobrador,
VDC.IDVendedor,
VDC.IDSucursal,
VDC.IDDeposito,
VDC.IDTipoComprobante,
'ComprobanteFactura'=VDC.Comprobante,
'Cobranza'=VDC.Numero,
'FormaPago'='Cheque',
'Moneda'=V.Moneda,
'Cambio'=V.Cotizacion,
'Banco'=V.Banco,
'CodigoBanco'=V.CodigoBanco,
'Comprobante'=V.NroCheque,
'FechaPago'=V.Fecha,
'TipoComprobante'=(Case When (V.Diferido)='True' Then 'DIF' Else 'CHQ' End),
'Tipo'=V.CodigoTipo,

--Cantidad de Cheques
'Cant'=0,

--Total Cobranza
'TotalCobrado'=(Select Sum(CC2.Total)From CobranzaContado CC2 
Where CC.FechaEmision=CC2.Fecha),

--Importe Cheque
'Importe'=FP.Importe,


FP.CancelarCheque,
CC.Anulado,
VDC.Credito,
'Planilla'=convert(int,CC.NroPlanilla),
'Recibo'=CC.Comprobante

From FormaPago FP
Join VChequeCliente V On FP.IDTransaccionCheque=V.IDTransaccion 
Join VCobranzaCredito CC On CC.IDTransaccion=FP.IDTransaccion
Join VVentaDetalleCobranzaCredito VDC On VDC.IDTransaccionCobranza=FP.IDTransaccion

Where CC.Anulado='False'

Union All

--DOCUMENTO
Select
FP.IDTransaccion,
VDC.IDTransaccionCobranza,
'Fec'=convert(varchar(50),VDC.Fecha,3),
'Fecha'=VDC.Fecha,
VDC.IDCliente,
VDC.Cliente,
VDC.Referencia,
VDC.[Cod.],

--Cantidad de Comprobantes
'CantComprobantes'=0,

VDC.Cobrador,
VDC.IDCobrador,
VDC.IDVendedor,

VDC.IDSucursal,
VDC.IDDeposito,
VDC.IDTipoComprobante,
'ComprobanteFactura'=VDC.Comprobante,
'Cobranza'=VDC.Numero,
'FormaPago'='Documento',
'Moneda'=M.Referencia,
'Cambio'=FPD.Cotizacion,
'Banco'='---',
'CodigoBanco'='---',
'Comprobante'=FPD.Comprobante,
'FechaPago'=convert(varchar(50),VDC.Fecha,3),
'TipoComprobante'=TC.Descripcion,
'Tipo'=TC.Codigo,

--Cantidad de Documentos
'Cant'=0,

--Total Cobranza
'TotalCobrado'=VDC.Importe,

--Importe Documento
'Importe'=FP.Importe,

FP.CancelarCheque,
CC.Anulado,
VDC.Credito,
'Planilla'=convert(int,CC.NroPlanilla),
'Recibo'=CC.Comprobante

From FormaPago FP
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Join Moneda M On FPD.IDMoneda=M.ID
Join VCobranzaCredito CC On CC.IDTransaccion=FP.IDTransaccion
Join VVentaDetalleCobranzaCredito VDC On VDC.IDTransaccionCobranza=FP.IDTransaccion


Where CC.Anulado='False'


Union All

--COBRANZA CONTADO

--EFECTIVO
Select
FP.IDTransaccion,
VDC.IDTransaccionCobranza,
'Fec'=convert(varchar(50),CC.Fecha,3),
CC.Fecha,
VDC.IDCliente,
VDC.Cliente,
VDC.ReferenciaCliente,
VDC.[Cod.],

--Cantidad de Comprobantes
'CantComprobantes'=0,

VDC.Cobrador,
VDC.IDCobrador,


VDC.IDVendedor,

VDC.IDSucursal,
VDC.IDDeposito,
VDC.IDTipoComprobante,
'ComprobanteFactura'=VDC.Comprobante,
'Cobranza'=VDC.Numero,
'FormaPago'='Efectivo',
'Moneda'=M.Referencia,
'Cambio'=E.Cotizacion,
'Banco'='---',
'CodigoBanco'='---',
'Comprobante'=E.Comprobante,
'FechaPago'='',
'TipoComprobante'=TC.Descripcion,
'Tipo'=TC.Codigo,

--Cantidad de Efectivos
'Cant'=0,

--Total Cobranza

'TotalCobrado'=CC.Total,

--Importe Efectivo

'Importe'=FP.Importe,

'CancelarCheque'=FP.CancelarCheque,
CC.Anulado,
VDC.Credito,
'Planilla'=convert(int,CC.ComprobanteLote),
'Recibo'=CC.Comprobante

From FormaPago FP
Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID
Join VCobranzaContado CC On CC.IDTransaccion=FP.IDTransaccion
Join VVentaDetalleCobranzaContado VDC On VDC.IDTransaccionCobranza=FP.IDTransaccion
Join VVentaLoteDistribucion VLD On VLD.IDTransaccionVenta=VDC.IDTransaccion

Where CC.Anulado='False'


Union All

--CHEQUE CLIENTE
Select
FP.IDTransaccion,
VDC.IDTransaccionCobranza,
'Fec'=convert(varchar(50),CC.Fecha,3),
CC.Fecha,
VDC.IDCliente,
VDC.Cliente,
VDC.ReferenciaCliente,
VDC.[Cod.],

--Cantidad Comprobantes
'CantComprobantes'=0,

VDC.Cobrador,
VDC.IDCobrador,


VDC.IDVendedor,

VDC.IDSucursal,
VDC.IDDeposito,
VDC.IDTipoComprobante,
'ComprobanteFactura'=VDC.Comprobante,
'Cobranza'=VDC.Numero,
'FormaPago'='Cheque',
'Moneda'=V.Moneda,
'Cambio'=V.Cotizacion,
'Banco'=V.Banco,
'CodigoBanco'=V.CodigoBanco,
'Comprobante'=V.NroCheque,
'FechaPago'=V.Fecha,
'TipoComprobante'=(Case When (V.Diferido)='True' Then 'DIF' Else 'CHQ' End),
'Tipo'=V.Tipo,

--Cantidad de Cheques
'Cant'=0,

--Total Cobranza
'TotalCobrado'=CC.Total,

--Importe Cheque
'Importe'=FP.Importe,


FP.CancelarCheque,
CC.Anulado,
VDC.Credito,
'Planilla'=convert(int,CC.ComprobanteLote),
'Recibo'=CC.Comprobante

From FormaPago FP
Join VChequeCliente V On FP.IDTransaccionCheque=V.IDTransaccion 
Join VCobranzaContado CC On CC.IDTransaccion=FP.IDTransaccion
Join VVentaDetalleCobranzaContado VDC On VDC.IDTransaccionCobranza=FP.IDTransaccion
Join VVentaLoteDistribucion VLD On VLD.IDTransaccionVenta=VDC.IDTransaccion

Where CC.Anulado='False'

Union All

--DOCUMENTO
Select
FP.IDTransaccion,
VDC.IDTransaccionCobranza,
'Fec'=convert(varchar(50),CC.Fecha,3),
CC.Fecha,
VDC.IDCliente,
VDC.Cliente,
VDC.ReferenciaCliente,
VDC.[Cod.],

--Cantidad de Comprobantes
'CantComprobantes'=0,

VDC.Cobrador,
VDC.IDCobrador,
VDC.IDVendedor,

VDC.IDSucursal,
VDC.IDDeposito,
VDC.IDTipoComprobante,
'ComprobanteFactura'=VDC.Comprobante,
'Cobranza'=VDC.Numero,
'FormaPago'='Documento',
'Moneda'=M.Referencia,
'Cambio'=FPD.Cotizacion,
'Banco'='---',
'CodigoBanco'='---',
'Comprobante'=FPD.Comprobante,
'FechaPago'='',
'TipoComprobante'=TC.Descripcion,
'Tipo'=TC.Codigo,

--Cantidad de Documentos
'Cant'=0,

--Total Cobranza

'TotalCobrado'=CC.Total,


--Importe Documento
'Importe'=FP.Importe,


FP.CancelarCheque,
CC.Anulado,
VDC.Credito,
'Planilla'=convert(int,CC.ComprobanteLote),
'Recibo'=CC.Comprobante

From FormaPago FP
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Join Moneda M On FPD.IDMoneda=M.ID
Join VCobranzaContado CC On CC.IDTransaccion=FP.IDTransaccion
Join VVentaDetalleCobranzaContado VDC On VDC.IDTransaccionCobranza=FP.IDTransaccion
Join VVentaLoteDistribucion VLD On VLD.IDTransaccionVenta=VDC.IDTransaccion

Where CC.Anulado='False'





﻿
CREATE View [dbo].[VCFVencimientoChequeDescontado]
As
Select
V.*,

'ChequeDiferido'=IsNull(CFV.ChequeDiferido, 'False'),
'ChequeDescontado'=IsNull(CFV.ChequeDescontado, 'False'),
'SucursalChequeDiferido'=(Case When(IsNull(CFV.ChequeDiferido, 'False')) = 'True' Then S.Descripcion Else '---' End),
'SucursalChequeDescontado'=(Case When(IsNull(CFV.ChequeDescontado, 'False')) = 'True' Then S2.Descripcion Else '---' End),
'IDSucursalChequeDiferido'=CFV.IDSucursalDiferido,
'IDSucursalChequeDescontado'=CFV.IDSucursalDescontado,
'Tipo'=(Case When (CFV.ChequeDiferido) = 'True' Then 'DIFERIDO' Else Case When (CFV.ChequeDescontado) = 'True' Then 'DESCONTADO' Else '---' End  End)

From VCF V
Join CFVencimientoChequeDescontado CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID
Left Outer Join Sucursal S on CFV.IDSucursalDiferido=S.ID
Left Outer Join Sucursal S2 on CFV.IDSucursalDescontado=S2.ID


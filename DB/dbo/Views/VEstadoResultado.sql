﻿Create View [dbo].[VEstadoResultado]
As
Select 
CC.ID,
'IDCuentaContable'=CC.ID,
CC.IDPlanCuenta,
'PlanCuenta'=PC.Descripcion,
'PlanCuentaTitular'=PC.Titular,
'Resolucion173'=PC.Resolucion173,
CC.Codigo,
CC.Descripcion,
'Cuenta'=CC.Codigo + ' - ' + CC.Descripcion,
'CuentaContable'=CC.Codigo + ' - ' + CC.Descripcion,
CC.Alias,
CC.Categoria,
CC.Imputable,
'Tipo'=(Select Case When(Select TOP (1) CC2.ID From CuentaContable CC2 Where CC2.IDPadre=CC.ID) Is Null Then 'IMPUTABLE' Else 'TOTALIZADOR' End),
'IDPadre'=IsNull(CC.IDPadre, 0),
'Padre'=(ISNULL((Select CC3.Codigo + ' - ' + CC3.Descripcion From CuentaContable CC3 Where CC3.ID=CC.IDPadre), '---')),
'CodigoPadre'=(ISNULL((Select Convert(varchar(50), CC3.Codigo) From CuentaContable CC3 Where CC3.ID=CC.IDPadre), '---')),
'Estado'=Case When (CC.Estado) = 'True' Then 'ACTIVO' Else 'INACTIVO' End,
'Activo'=CC.Estado,
'Sucursal'=IsNull(CC.Sucursal, 'False'),
'IDSucursal'=IsNull(CC.IDSucursal, 0),
'Suc'=ISNULL((Select S.Descripcion From Sucursal S Where S.ID=CC.IDSucursal), '---'),
'DebitoAnterior'=CONVERT(money, 0),
'CreditoAnterior'=CONVERT(money, 0),
'SaldoAnterior'=CONVERT(money, 0),
'DebitoMes'=CONVERT(money, 0),
'CreditoMes'=CONVERT(money, 0),
'SaldoMes'=CONVERT(money, 0),
'SaldoAcumulado'=CONVERT(money, 0),

'MostrarCodigo'=0

From CuentaContable CC
Join PlanCuenta PC On CC.IDPlanCuenta=PC.ID

--Esto es necesario para el programa, TITULAR = 'True'
--Si en parte del codigo falla, crear otra vista para tal caso... No tocar este.
Where PC.Titular = 'True'

And (SubString(CC.Codigo, 0, 2) = '4' Or SubString(CC.Codigo, 0, 2) = '5' Or SubString(CC.Codigo, 0, 2) = '6' Or SubString(CC.Codigo, 0, 2) = '7')

















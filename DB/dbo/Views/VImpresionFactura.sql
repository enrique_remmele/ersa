﻿


CREATE View [dbo].[VImpresionFactura]
As
Select 
V.IDTransaccion,
'Comprobante'=V.Comprobante,
'Fecha'=dbo.FFormatoFecha(V.Fecha),
'Contado'=Case When (V.Contado) = 'True' Then 'X' Else '' End,
'Credito'=Case When (V.Credito) = 'True' Then 'X' Else '' End,
--'Vencimiento'=Case When (V.Credito) = 'True' Then V.FechaVencimiento Else NULL End,
'Vencimiento'=Case When (V.Credito) = 'True' Then [Fec. Venc.] Else NULL End,
'RUC'=RUC,
'Razon Social'=V.Cliente,--concat(V.Cliente,' (',V.ReferenciaCliente,')'),
--Totales
'Total EX'=dbo.FFormatoNumericoImpresion((Select Exento From VDetalleImpuestoDesglosadoGravado DI Where DI.IDTransaccion=V.IDTransaccion), M.decimales),
'Total 5%'=dbo.FFormatoNumericoImpresion((Select [Gravado5%] From VDetalleImpuestoDesglosadoGravado DI Where DI.IDTransaccion=V.IDTransaccion), M.decimales),
--
'Total 10%'=dbo.FFormatoNumericoImpresion((Select [Gravado10%] From VDetalleImpuestoDesglosadoGravado DI Where DI.IDTransaccion=V.IDTransaccion), M.decimales),
--
'LETRAS'=(Case when M.Decimales = 'False' then dbo.FCantidadConLetra(V.Total) else concat(M.Impresion,' ',UPPER(dbo.ConvertirNumero(V.Total))) end),
--'IVA 5%'=dbo.FFormatoNumericoImpresion((Select [IVA5%] From VDetalleImpuestoDesglosado DI Where DI.IDTransaccion=V.IDTransaccion), M.decimales),
'IVA 5%'=isnull(dbo.FFormatoNumericoImpresion((select Round(sum(((precioUnitario - descuentounitario) * cantidad)/21),0) from detalleventa where idimpuesto = 2 and idtransaccion =V.IDTransaccion), M.decimales),0),
--'IVA 10%'=dbo.FFormatoNumericoImpresion((Select [IVA10%] From VDetalleImpuestoDesglosado DI Where DI.IDTransaccion=V.IDTransaccion), M.decimales),
'IVA 10%'=isnull(dbo.FFormatoNumericoImpresion((select Round(sum(((precioUnitario - descuentounitario) * cantidad)/11),0) from detalleventa where idimpuesto = 1 and idtransaccion =V.IDTransaccion), M.decimales),0),
--'Total IVA'=dbo.FFormatoNumericoImpresion(V.TotalImpuesto, M.decimales),
'Total IVA'=dbo.FFormatoNumericoImpresion(Round(isnull((select sum(((precioUnitario - descuentounitario) * cantidad)/21) from detalleventa where idimpuesto = 2 and idtransaccion =V.IDTransaccion),0) + isnull((select sum(((precioUnitario - descuentounitario) * cantidad)/11) from detalleventa where idimpuesto = 1 and idtransaccion =V.IDTransaccion),0),0), M.decimales), 

'Total'=dbo.FFormatoNumericoImpresion(V.Total, M.Decimales),
'FormaPago'=(Case when CancelarAutomatico =  'True' then FormaPago else(
			Case When FormaPago = 'CH.DIFERIDO' then 'CH.DIF. VTO.:' else (
			Case When V.Credito = 'True' then 'VTO.: ' else '' 
			end ) End) end),
'Emision' =  Concat((Select Ciudad from VSucursal where ID = IDSucursal),' ',dbo.FFormatoDosDigitos(DatePart(dd, FechaEmision)),' de ', dbo.FMes(DatePart(MM, FechaEmision)),' de ',DatePart(yyyy, FechaEmision)),
V.Direccion,
'Telefono'= concat(V.Telefono,'  (',V.ReferenciaCliente,')'),
M.Referencia,
V.IDMoneda,
'Leyenda' = (Case when CancelarAutomatico =  'True' then '# Sin valor Comercial # A solo efecto de pago de Impuesto' else '' end)

From VVenta V
Join Moneda M on M.ID = V.IDMOneda
Where Anulado = 'False'
--and idtransaccion =1119098 


--select * from vventa where comprobante like '%172749%'
--select dbo.FFormatoNumericoImpresion((select sum(((precioUnitario - descuentounitario) * cantidad)/21) from detalleventa where idtransaccion = 1119098),0)






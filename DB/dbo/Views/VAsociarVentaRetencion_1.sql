﻿
CREATE View [dbo].[VAsociarVentaRetencion]
As
Select 
A.IDTransaccion,
A.Numero,
A.Fecha,

--Sucursal
A.IDSucursal,
'Suc'=S.Codigo,
'Sucursal'=S.Descripcion,

--Cliente
A.IDCliente,
'Referencia'=C.Referencia,
'Cliente'=C.RazonSocial,
'RUC'=C.RUC,

--Retencion
A.IDTransaccionRetencion,
A.IDFormaPago,
FP.Comprobante,
FP.IDTipoComprobante,
A.PorcentajeRetencion,
'FechaRetencion'=FP.Fecha,
'ObservacionRetencion'=FP.Observacion,
A.Total,
A.Anulado,
A.Observacion,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,

T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'Estado'=Case When A.Anulado='True' Then 'Anulado' Else '---' End,
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=A.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=A.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=A.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=A.IDTransaccion)
 
From AsociarVentaRetencion A
Join Transaccion T On A.IDTransaccion=T.ID
Join Usuario U On T.IDUsuario=U.ID
Join FormaPagoDocumento FP On A.IDTransaccionRetencion=FP.IDTransaccion And A.IDFormaPago=FP.ID
Left Outer Join Cliente C On A.IDCliente=C.ID
Left Outer Join TipoCliente TCLI On TCLI.ID=C.IDTipoCliente
Left Outer Join Sucursal S On A.IDSucursal=S.ID

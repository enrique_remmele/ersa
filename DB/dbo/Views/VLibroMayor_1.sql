﻿
CREATE view [dbo].[VLibroMayor]

as

Select

DA.IDTransaccion,
'Codigo'=convert(varchar(50),C.Codigo),
C.Descripcion,
a.Fecha, 
'Asiento'=A.Numero,
DA.NroComprobante,
'Comprobante'=concat(A.TipoComprobante,' ',(Case When (DA.NroComprobante) Is Null Then A.NroComprobante 
					   When (DA.NroComprobante) = '' Then A.NroComprobante 
					   Else (DA.NroComprobante) End)),
DA.IDSucursal,
'Suc'=S.Descripcion,
'CodSucursal'=S.Codigo,
A.IDTipoComprobante,
--'Concepto'=Concat(A.Moneda,'-',A.Cotizacion,'-',(Case When(DA.Observacion) = '' Then (Case When(A.Detalle) = '' Then O.Descripcion Else A.Detalle End) Else concat(dbo.FComprobanteLibroMayor(A.IDTransaccion,O.FormName),'-',DA.Observacion) End)),
'Concepto'=Concat('',(Case When(DA.Observacion) = '' Then (Case When(A.Detalle) = '' Then O.Descripcion Else A.Detalle End) Else concat(dbo.FComprobanteLibroMayor(A.IDTransaccion,O.FormName),'-',DA.Observacion) End)),
DA.Debito,
DA.Credito,
'TipoCuenta'=dbo.FCuentaContableTipo(convert(varchar(50),C.Codigo)),

--Operacion
T.IDOperacion,
'Operacion'=O.Descripcion,

--Centro de Costos
DA.IDCentroCosto,
'CentroCosto'=ISNull(CC.Descripcion, '---'),
'CodigoCentroCosto'=IsNull(CC.Codigo, ''),
--Unidad de negocio
DA.IDUnidadNegocio,
'UnidadNegocio'=ISNull(UN.Descripcion, '---'),
'CodigoUnidadNegocio'=IsNull(UN.Codigo, ''),
A.Usuario,
--A.FechaTransaccion,
'FechaTransaccion'=T.Fecha,
A.Cotizacion,
O.FormName,
A.IDMoneda,
A.Moneda

From DetalleAsiento DA 
Join VAsiento A on A.IDTransaccion=DA.IDTransaccion
Join Transaccion T On A.IDTransaccion=T.ID
Join CuentaContable C on DA.CuentaContable = C.Codigo
Left Outer Join Operacion O On T.IDOperacion=O.ID
Left Outer JOin Sucursal S On DA.IDSucursal=S.ID
Left Outer Join CentroCosto CC On DA.IDCentroCosto=CC.ID
Left Outer Join UnidadNegocio UN On DA.IDUnidadNegocio=UN.ID












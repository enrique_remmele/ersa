﻿

CREATE View [dbo].[VCFNotaDebitoProveedor]
As
Select
V.*,
CFV.BuscarEnProveedor,
CFV.BuscarEnProducto,

--Tipo Cuenta Fija
CFV.IDTipoCuentaFija,
'TipoCuentaFija'=TC.Tipo,
'CuentaFija'=TC.Descripcion,
TC.IDImpuesto,

--Tipo de Cuenta
TC.Campo,
TC.IncluirDescuento,
TC.IncluirImpuesto

From VCF V
Join CFNotaDebitoProveedor CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID
Join VTipoCuentaFija TC On CFV.IDTipoCuentaFija=TC.ID




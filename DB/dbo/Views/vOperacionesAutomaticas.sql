﻿CREATE View [dbo].[vOperacionesAutomaticas]
as
Select 
O.ID,
O.DB,
O.Titulo,
O.Descripcion,
O.IDTipoOperacion,
'TipoOperacion'=TOA.Descripcion,
O.HoraOperacion,
O.Recurrencia,
'RecurrenciaObs'=Concat(O.Recurrencia,' - ',(Case When O.Recurrencia = 'SEMANAL' then (Select [dbo].FDiaSemana2(O.DiaSemana)) else
					(Case when O.Recurrencia = 'MENSUAL' THEN Convert(varchar(10),O.DiaMes) else 
					(Case when O.Recurrencia = 'UNICA VEZ' THEN convert(varchar, O.FechaOperacion, 103) else
					'DIARIO' end)end)end)),
					
O.FechaOperacion,
O.DiaSemana,
O.DiaMes,
'IDTipoRecalculoKardex'=isnull(O.IDTipoRecalculoKardex,0),
'TipoRecalculoKardex'=ISNULL(TRK.Descripcion,'---'),
'IDTipoPRoducto'=isnull(O.IDTipoPRoducto,0),
'TipoProducto'=isnull(TP.Descripcion,'---'),
'IDProducto'=isnull(O.IDProducto,0),
'Producto'=P.Descripcion,
O.FechaInicioKardex,
O.[SQL],
'Activo'=isnull(O.Activo,0),
'Ejecutando'=isnull(O.Ejecutando,0),
'SiguienteEjecucion'= (select [dbo].FSiguienteEjecucion(O.ID)),
'Pendiente'=(select [dbo].FEjecutarOperacionAutomatica(O.ID))


from OperacionAutomatica O
join TipoOperacionAutomatica TOA on O.IDTipoOperacion = TOA.ID
left outer join TipoRecalculoKardex TRK on O.IDTipoRecalculoKardex = TRK.ID
left outer join TipoProducto TP on O.IDTipoProducto = TP.ID
left outer join Producto P on O.IDProducto = P.ID




﻿CREATE View [dbo].[VFormaPagoCobranzaCredito]

As

--Efectivo
Select
FP.IDTransaccion,
FP.ID,
TC.IDOperacion,
'FormaPago'='Efectivo',
'IDMoneda'=M.ID,
'Moneda'=M.Referencia,
'ImporteMoneda'=E.ImporteMoneda,
'Cambio'=Isnull(FP.Cotizacion,1),
0 as IdBanco,
'Banco'='---',
'Comprobante'=E.Comprobante,
'FechaEmision'=convert(varchar(50),E.Fecha,3),
'Vencimiento'=NULL,
'TipoComprobante'=TC.Descripcion,
'CodigoTipoComprobante'=TC.Codigo,
'Tipo'='---',
'Importe'=FP.Importe,
'CancelarCheque'=FP.CancelarCheque,
TC.Restar,
'Efectivo'='True',
'Cheque'='False',
'Tarjeta'='False' 

From FormaPago FP
Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID

Union All

--Cheque Cliente
Select
FP.IDTransaccion,
FP.ID,
V.IDOperacion,
'FormaPago'='Cheque',
'IDMoneda'=V.IDMoneda,
'Moneda'=V.Moneda,
'ImporteMoneda'=FP.ImporteCheque,
'Cambio'=ISnull(FP.Cotizacion,1),
V.IDBanco,
'Banco'=V.CodigoBanco,
'Comprobante'=V.NroCheque,
'FechaEmision'=convert(varchar(50),V.Fecha,3),
'Vencimiento'=convert(varchar(50),V.FechaVencimiento,3),
'TipoComprobante'=(Case When (V.Diferido)='True' Then 'CHEQUE DIFERIDO' Else 'CHEQUE' End),
'CodigoTipoComprobante'=(Case When (V.Diferido)='True' Then 'DIF' Else 'CHQ' End),
'Tipo'=V.Tipo,
'Importe'=FP.Importe,
FP.CancelarCheque,
'Restar'=0,
'Efectivo'='False',
'Cheque'='True',
'Tarjeta'='False' 


From FormaPago FP
Join VChequeCliente V On FP.IDTransaccionCheque=V.IDTransaccion

Union All

--Documento
Select
FP.IDTransaccion,
FP.ID,
TC.IDOperacion,
'FormaPago'='Documento',
'IDMoneda'=M.ID,
'Moneda'=M.Referencia,
'ImporteMoneda'=FPD.ImporteMoneda,
'Cambio'=FPD.Cotizacion,
Isnull(FPD.IDBanco,0) as IDBanco,
'Banco'=Isnull((B.referencia),'-'),
'Comprobante'=FPD.Comprobante,
'FechaEmision'=convert(varchar(50),FPD.Fecha,3),
'Vencimiento'=NULL,
'TipoComprobante'=TC.Descripcion,
'CodigoTipoComprobante'=TC.Codigo,
'Tipo'='---',
'Importe'=FP.Importe,
FP.CancelarCheque,
TC.Restar,
'Efectivo'='False',
'Cheque'='False' ,
'Tarjeta'='False'


From FormaPago FP
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Join Moneda M On FPD.IDMoneda=M.ID
Left join Banco B on B.ID = FPD.IDBanco


UNION ALL

--Tarjeta
Select
FP.IDTransaccion,
FP.ID,
TC.IDOperacion,
'FormaPago'='Efectivo',
'IDMoneda'=M.ID,
'Moneda'=M.Referencia,
'ImporteMoneda'=E.ImporteMoneda,
'Cambio'=E.Cotizacion,
0 as IdBanco,
'Banco'='---',
'Comprobante'=E.Comprobante,
'FechaEmision'=convert(varchar(50),E.Fecha,3),
'Vencimiento'=NULL,
'TipoComprobante'=TC.Descripcion,
'CodigoTipoComprobante'=TC.Codigo,
'Tipo'='---',
'Importe'=FP.Importe,
'CancelarCheque'=FP.CancelarCheque,
TC.Restar,
'Efectivo'='False',
'Cheque'='False' ,
'Tarjeta'='True'

From FormaPago FP
JOIN dbo.FormaPagoTarjeta E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID





﻿CREATE view [dbo].[VInventarioDocumentosPendientesPago]
as

--Compra
Select
C.IDTransaccion,
C.IDProveedor, 
C.Proveedor ,
C.Referencia ,
C.IDTipoProveedor,
C.IDMoneda, 
C.Moneda,
C.Cotizacion,
C.Telefono,
C.RUC , 
C.IDTipoComprobante ,
C.Comprobante ,
C.[Cod.],
C.NroComprobante,
C.Condicion,
C.IDSucursal,
C.Sucursal ,
C.IDCiudad,
'Origen'=C.Ciudad ,
C.IDDeposito, 
'Emision'=C.Fecha,
'FechaIngreso'=T.Fecha,
'FechaVencimiento'= ISNULL (C.FechaVencimiento,C.Fecha   ),
'Plazo'= DATEDIFF (D, C.Fecha, FechaVencimiento ),
'UltimoPago'=(select MAX (OPE.Fecha)from VOrdenPagoEgreso OPE Where OPE.IDTransaccion = C.IDTransaccion ),
C.Total ,
C.Saldo,
C.Cancelado,
'Cuota'='1/1',
'Anulado'='False',
C.Numero,
C.Observacion,
Isnull((select Cotizacion from Cotizacion where IDMoneda = C.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1) as CotizacionHoy
 
 from VCompra C
 Join Transaccion T On C.IDTransaccion=T.ID
 
 Union All

 --NotaCréditoProveedor
Select
NC.IDTransaccion,
NC.IDProveedor,  
NC.Proveedor ,
NC.Referencia,
NC.IDTipoProveedor,
NC.IDMoneda, 
NC.Moneda,
NC.Cotizacion,
NC.Telefono,
NC.RUC,
NC.IDTipoComprobante ,
NC.Comprobante,
NC.[Cod.],
NC.NroComprobante,
'Condicion'='',
NC.IDSucursal,
NC.Suc ,
NC.IDCiudad,
'Origen'=NC.Ciudad,
NC.IDDeposito, 
'Emision'=NC.Fecha,
'FechaIngreso'=T.Fecha,
'FechaVencimiento'= CONVERT(date, DATEAdd(D, -1, GETDATE())),
'Plazo'= '',
'UltimoPago'='',
NC.Total,
NC .Saldo,  
'Cancelado'= (Case When(NC.Saldo) = 0 Then 'True' Else 'False' End),
'Cuota'='1/1',
NC.Anulado,
NC.Numero,
NC.Observacion,
Isnull((select Cotizacion from Cotizacion where IDMoneda = NC.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1) as CotizacionHoy
 
 From VNotaCreditoProveedor NC
 Join Transaccion T On NC.IDTransaccion=T.ID

Union All

--NotaDébitoProveedor

Select 
ND.IDTransaccion,
ND.IDProveedor,
ND.Proveedor ,
ND.Referencia,
ND.IDTipoProveedor,
ND.IDMoneda, 
ND.Moneda,
ND.Cotizacion,
ND.Telefono,
ND.RUC,
ND.IDTipoComprobante ,
ND.Comprobante,
ND.[Cod.],
ND.NroComprobante,
'Condicion'='',
ND.IDSucursal,
ND.Suc ,
ND.IDCiudad,
'Origen'= ND.Ciudad,
ND.IDDeposito, 
'Emision'=ND.Fecha,
'FechaIngreso'=T.Fecha,
'FechaVencimiento'= CONVERT(date, DATEAdd(D, -1, GETDATE())),
'Plazo'= '',
'UltimoPago'='',
ND.Total,
ND.Saldo,
'Cancelado'= (Case When(ND.Saldo) = 0 Then 'True' Else 'False' End),
'Cuota'='1/1',
ND.Anulado,
ND.Numero,
ND.Observacion,
Isnull((select Cotizacion from Cotizacion where IDMoneda = ND.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1) as CotizacionHoy

From VNotaDebitoProveedor ND 
Join Transaccion T On ND.IDTransaccion=T.ID

Union All

--Gastos
Select
C.IDTransaccion,
C.IDProveedor, 
C.Proveedor ,
C.Referencia ,
P.IDTipoProveedor,
C.IDMoneda, 
C.Moneda,
C.Cotizacion,
P.Telefono,
C.RUC , 
C.IDTipoComprobante ,
C.Comprobante ,
C.[Cod.],
C.NroComprobante,
C.Condicion,
C.IDSucursal,
C.Sucursal ,
S.IDCiudad,
'Origen'=C.Ciudad ,
C.IDDeposito, 
'Emision'=C.Fecha,
'FechaIngreso'=T.Fecha,
'FechaVencimiento'= ISNULL (C.FechaVencimiento,C.Fecha   ),
'Plazo'= DATEDIFF (D, C.Fecha, FechaVencimiento ),
'UltimoPago'=(select MAX (OPE.Fecha)from VOrdenPagoEgreso OPE Where OPE.IDTransaccion = C.IDTransaccion ),
C.Total ,
C.Saldo,
C.Cancelado,
'Cuota'='1/1',
'Anulado'='False',
C.Numero,
C.Observacion,
Isnull((select Cotizacion from Cotizacion where IDMoneda = C.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1) as CotizacionHoy
 
from VGasto C
Join Sucursal S On C.IDSucursal=S.ID
Join Transaccion T On C.IDTransaccion=T.ID
Left Outer Join VProveedor P On C.IDProveedor=P.ID
Join GastoTipoComprobante GTC On C.IDTransaccion=GTC.IDTransaccion

Where C.Cuota=0
and ISnull(C.CajaChica,0) = 0

Union All

--Gastos / Cuotas
Select
C.IDTransaccion,
C.IDProveedor, 
C.Proveedor ,
C.Referencia ,
P.IDTipoProveedor,
C.IDMoneda, 
C.Moneda,
C.Cotizacion,
P.Telefono,
C.RUC , 
C.IDTipoComprobante ,
C.Comprobante + '/' + Convert(varchar(50), CU.ID),
C.[Cod.],
C.NroComprobante + '/' + Convert(varchar(50), CU.ID),
C.Condicion,
C.IDSucursal,
C.Sucursal ,
S.IDCiudad,
'Origen'=C.Ciudad ,
C.IDDeposito, 
'Emision'=C.Fecha,
'FechaIngreso'=T.Fecha,
'FechaVencimiento'= ISNULL (CU.Vencimiento, C.Fecha),
'Plazo'= DATEDIFF (D, C.Fecha, CU.Vencimiento),
'UltimoPago'=(select MAX (OPE.Fecha)from VOrdenPagoEgreso OPE Where OPE.IDTransaccion = C.IDTransaccion ),
'Total'=CU.Importe,
CU.Saldo,
'Cancelado'= Case when C.Cancelado = 1 then C.Cancelado else CU.Cancelado end,
'Cuota'=Convert(varchar(50), CU.ID) + '/' + Convert(varchar(50), C.Cuota),
'Anulado'='False',
C.Numero,
C.Observacion,
Isnull((select Cotizacion from Cotizacion where IDMoneda = C.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1) as CotizacionHoy
 
from VGasto C
Join Sucursal S On C.IDSucursal=S.ID
Join Transaccion T On C.IDTransaccion=T.ID
Join Cuota CU On C.IDTransaccion=CU.IDTransaccion
Left Outer Join VProveedor P On C.IDProveedor=P.ID
Join GastoTipoComprobante GTC On C.IDTransaccion=GTC.IDTransaccion
Where CU.Cancelado = 'False' And C.Cuota>0
and isnull(c.CajaChica,0) = 0




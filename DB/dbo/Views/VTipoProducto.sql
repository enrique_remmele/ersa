﻿

CREATE View [dbo].[VTipoProducto]
as
Select
TP.ID,
TP.Referencia,
TP.Descripcion,
TP.Estado,
TP.CuentaContableCliente,
TP.CuentaContableVenta,
TP.CuentaContableCosto,
TP.CuentaContableProveedor,
'Producido'= Isnull(TP.Producido, 'False')
 
From TipoProducto TP





﻿CREATE View [dbo].[VLineaSubLinea]
As
Select
LSL.*,
'TipoProducto'=TP.Descripcion,
'Linea'=L.Descripcion,
'SubLinea'=SL.Descripcion

From LineaSubLinea LSL
Join TipoProducto TP On LSL.IDTipoProducto=TP.ID
Join Linea L On LSL.IDLinea=L.ID
JOin SubLinea SL On LSL.IDSubLinea=SL.ID

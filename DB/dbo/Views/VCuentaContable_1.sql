﻿
CREATE View [dbo].[VCuentaContable]
As
Select 
CC.ID,
'IDCuentaContable'=CC.ID,
CC.IDPlanCuenta,
'PlanCuenta'=PC.Descripcion,
'PlanCuentaTitular'=PC.Titular,
'Resolucion173'=PC.Resolucion173,
CC.Codigo,
CC.Descripcion,
'DescripcionAbuelo'= Concat((case when CC.Imputable = 0 then CC.Descripcion
 when CC.Imputable = 1 then  Concat(CC.Descripcion,' - ',Isnull((select CC1.Descripcion from CuentaContable CC1 where CC1.Codigo = Substring(CC.Codigo,1,5) and CC1.IDPlanCuenta = CC.IdPlanCuenta),(select CC1.Descripcion from CuentaContable CC1 where CC1.Codigo = Substring(CC.Codigo,1,6) and CC1.IDPlanCuenta = CC.IdPlanCuenta))) 
 end),' - ', (ISNULL((select CC1.Descripcion from CuentaContable CC1 where CC1.Codigo = Substring(CC.Codigo,1,4) and CC1.IDPlanCuenta = CC.IdPlanCuenta), (select CC1.Descripcion from CuentaContable CC1 where CC1.Codigo = Substring(CC.Codigo,1,3) and CC1.IDPlanCuenta = CC.IdPlanCuenta)))),
'Cuenta'=CC.Codigo + ' - ' + CC.Descripcion,
'CuentaContable'=CC.Codigo + ' - ' + CC.Descripcion,
CC.Alias,
CC.Categoria,
CC.Imputable,
'Tipo'=(Select Case When(Select TOP (1) CC2.ID From CuentaContable CC2 Where CC2.IDPadre=CC.ID) Is Null Then 'IMPUTABLE' Else 'TOTALIZADOR' End),
'IDPadre'=IsNull(CC.IDPadre, 0),
'Padre'=(ISNULL((Select CC3.Codigo + ' - ' + CC3.Descripcion From CuentaContable CC3 Where CC3.ID=CC.IDPadre), '---')),
'CodigoPadre'=(ISNULL((Select Convert(varchar(50), CC3.Codigo) From CuentaContable CC3 Where CC3.ID=CC.IDPadre), '---')),
'Estado'=Case When (CC.Estado) = 'True' Then 'ACTIVO' Else 'INACTIVO' End,
'Activo'=CC.Estado,
'Sucursal'=IsNull(CC.Sucursal, 'False'),
'IDSucursal'=IsNull(CC.IDSucursal, 0),
'Suc'=ISNULL((Select S.Descripcion From Sucursal S Where S.ID=CC.IDSucursal), '---'),
'IDUnidadNegocio'=Isnull(CC.IDUnidadNegocio,0),
'UnidadNegocio'=(Case When Isnull(CC.IDUnidadNegocio,0) = 0 then '---' else (Select Descripcion from UnidadNegocio where id = Isnull(CC.IDUnidadNegocio,0)) end),
'IDCentroCosto'=Isnull(CC.IDCentroCosto,0),
'CentroCosto'=(Case When Isnull(CC.IDCentroCosto,0) = 0 then '---' else (Select Descripcion from CentroCosto where id = Isnull(CC.IDCentroCosto,0)) end),
'Padre2'=(ISNULL((Select CC3.Descripcion From CuentaContable CC3 Where CC3.ID=CC.IDPadre), '---'))

From CuentaContable CC
Join PlanCuenta PC On CC.IDPlanCuenta=PC.ID

--Esto es necesario para el programa, TITULAR = 'True'
--Si en parte del codigo falla, crear otra vista para tal caso... No tocar este.
Where PC.Titular = 'True'

























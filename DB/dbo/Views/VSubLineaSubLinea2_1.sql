﻿CREATE View [dbo].[VSubLineaSubLinea2]
As
Select
SLSL2.*,
'TipoProducto'=TP.Descripcion,
'Linea'=L.Descripcion,
'SubLinea'=SL.Descripcion,
'SubLinea2'=SL2.Descripcion

From SubLineaSubLinea2 SLSL2
Join TipoProducto TP On SLSL2.IDTipoProducto=TP.ID
Join Linea L On SLSL2.IDLinea=L.ID
JOin SubLinea SL On SLSL2.IDSubLinea=SL.ID
JOin SubLinea2 SL2 On SLSL2.IDSubLinea2=SL2.ID

﻿


CREATE View [dbo].[VVentasPendientesParaLote]

As

Select
V.IDCliente,
V.Cliente,
V.IDTransaccion,
'Sel'=Case When V.Credito = 'True' Then 'False' Else 'True' End,
V.Comprobante,
'Tipo'=V.TipoComprobante,
V.Condicion,
V.Credito,
'Fecha'=convert(varchar(50),V.Fecha,5),
V.Fec,
'Vencimiento'=V.[Fec. Venc.],
V.FechaVencimiento,
V.Total,
V.Cobrado,
V.Descontado,
V.Saldo,
'Cancelar'='True',
V.IDMoneda,
V.Moneda,
V.Cotizacion,
V.IDSucursal,
V.IDTipoComprobante,

--Lote
VL.IDTransaccionLote,
VL.IDTransaccionVenta,
--'ComprobanteLote'= Convert (numeric(18,0), L.Comprobante),
'ComprobanteLote'= L.Comprobante,
'NumeroLote'=L.Numero,
'FechaLote'=convert(varchar(50),L.Fecha,5),
'Distribuidor'=L.Distribuidor,
'Importe'=V.Saldo,
V.TotalImpuesto


From VVenta V
Join VentaLoteDistribucion VL On V.IDTransaccion=VL.IDTransaccionVenta
Join VLoteDistribucion L On VL.IDTransaccionLote=L.IDTransaccion
Where V.Cancelado = 'False' And V.Anulado = 'False'










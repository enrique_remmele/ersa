﻿CREATE view VaUsuarioOperacionFueraRangoFecha as 
select 
A.IDUsuario,
'Usuario'= U.Nombre,
A.IDOperacion,
'Operacion'=O.Descripcion,
A.Desde,
A.Hasta,
'OperacionAuditoria'=A.Operacion,
'IDUsuarioAuditoria'=A.IDUsuarioOperacion,
'UsuarioAuditoria'= UO.Nombre,
'FechaAuditoria'=A.FechaOperacion,
'IDTerminalAuditoria'=A.IDTerminalOperacion

from aUsuarioOperacionFueraRangoFecha A
join Usuario U on A.IDUsuario = U.ID
join Usuario UO on A.IDUsuarioOperacion = UO.ID
join Operacion O on A.IDOperacion = O.ID
﻿--USE [EXPRESS]
--GO

--/****** Object:  View [dbo].[VAccesoEspecificoPerfil]    Script Date: 02/08/2013 16:30:17 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO



CREATE View [dbo].[VAccesoEspecificoPerfil]
As
Select 

--Perfil
'IDPerfil'=P.ID,

--Acceso Perfil
'Acceso'=IsNull(A.Descripcion, '---'),
'Funcion'=IsNull(A.Funcion, 'False'),
'Control'=IsNull(A.[Control], 'False'),
'NombreFuncion'=IsNull(A.NombreFuncion, '---'),
'NombreControl'=IsNull(A.NombreControl, '---'),
'Enabled'=IsNull(A.[Enabled], 'False'),
'Visible'=IsNull(A.Visible, 'False'),

'Habilitado'=(IsNull(AF.Habilitar, 'False')),
'IDAccesoEspecifico'=IsNull(AF.IDAccesoEspecifico, '0'),

--Menu
'IDMenu'=IsNull(A.IDMenu, '0'),
'Tag'=IsNull(M.Tag, '---'),
'Form'=IsNull(M.Tag, '---')



From Perfil P 
Left Outer JOin AccesoEspecificoPerfil AF On P.ID=AF.IDPerfil
Left Outer Join AccesoEspecifico A On AF.IDAccesoEspecifico=A.ID And AF.IDMenu=A.IDMenu
Left Outer Join Menu M On A.IDMenu=M.Codigo


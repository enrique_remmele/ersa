﻿
CREATE View [dbo].[VNotaCredito]
As
Select

--Punto de Expedicion
N.IDPuntoExpedicion,
'PE'=PE.Descripcion,
PE.Ciudad,
PE.IDCiudad,
PE.IDSucursal,
PE.Sucursal,
PE.ReferenciaSucursal,
PE.ReferenciaPunto,

--Cliente
N .IDCliente,
C.idtipocliente,
'Cliente'=C.RazonSocial,
'Direccion'=ISNULL(N.Direccion, ISNULL(CS.Direccion,C.Direccion)),
'Telefono'=ISNULL(N.Telefono, ISNULL(CS.Telefono,C.Telefono)),
c.Referencia ,
C.RUC ,
C.IDEstado, 
'Estado'= (Select Isnull  ((Select E.Descripcion  From EstadoCliente E Where E .ID = C.IDEstado ), '')),
C.IDZonaVenta, 
--'ConComprobantes'=Case When (Select Top(1) IDTransaccionVenta From DetalleNotaCredito D Where D.IDTransaccion=N.IDTransaccion And D.IDTransaccion Is Not Null) Is Not Null Then 'True' Else 'False' End,
N.ConComprobantes,
'IDCiudadCliente'=C.IDCiudad,
'IDVendedor'=ISNULL(CS.IDVendedor,C.IDVendedor),
'Vendedor'=Isnull((select Nombres from vendedor where id = ISNULL(CS.IDVendedor,C.IDVendedor)),''),
'IDPromotor'=ISNULL(CS.IDPromotor,C.IDPromotor),
'Promotor'=Isnull((select Nombres from promotor where id = ISNULL(CS.IDPromotor,C.IDPromotor)),''),
'SucursalCliente'=ISNULL(CS.Sucursal,''),
N.IDTransaccion,
N.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
N.NroComprobante,
'Comprobante'=PE.ReferenciaSucursal + '-' + PE.ReferenciaPunto + '-' + Convert(varchar(50), N.NroComprobante),


N .IDDeposito,
'Deposito'=D.Descripcion,
'Suc'=S.Codigo,

N .Fecha,
'Fec'=CONVERT(varchar(50), N .Fecha, 5),
C.Credito,
'CreditoContado'=(Case When (C.Credito) = 'True' Then 'CREDITO' Else 'CONTADO' End),
'Condicion'=(Case When (C.Credito) = 'True' Then 'CRED' Else 'CONT' End),
N .IDMoneda,
'Moneda'=M.Referencia,
'DescripcionMoneda'=M.Descripcion,
N .Cotizacion,

N .Observacion,
N.Aplicar ,
N.Descuento ,
N.Devolucion ,
'Tipo'=(Case When (N.Descuento) = 'True' Then 'DESC.' Else (Case When (N.Devolucion) = 'True' Then 'DEVOLUCION' Else '---' End) End),
N.Anulado,
N.Procesado,
'Estado NC'=(Case When N.Procesado='False' Then 'No se proceso correctamente' Else 
			(Case When N.Anulado='True' Then 'Anulado' Else 
			'---' 
			End) 
			End),

--Totales
N.Total,
N.TotalImpuesto,
N.TotalDiscriminado,
N.Saldo,
'Aplicado'=N.Total - N.Saldo ,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'Usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=N.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=N.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=N.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=N.IDTransaccion),

--Mes y Año
'DiaP'=dbo.FFormatoDosDigitos(DatePart(dd,N.Fecha )),
'MesP'=dbo.FMes(DatePart(MM,N.Fecha )),
'Año'= Convert (Varchar (50),YEAR(N.Fecha)),
'IDMotivo'=Isnull(N.IDMotivo,0),
'Motivo'=ISnull((select Descripcion from MotivoAnulacionNotaCredito where ID = Isnull(N.IDMotivo,0)),''),
'IDSucursalCliente'= IsNull(N.IDSucursalCliente,0),
--'IDTipoProducto' = IsNull((Select top(1) DV.IDTipoProducto from vDetalleVenta DV join NotaCreditoVenta ncv on dv.IDTransaccion = ncv.IDTransaccionVentaGenerada  where ncv.IDTransaccionNotaCredito= N.IDTransaccion),0),
'IDTipoProducto' = IsNull((Select top(1) IDTipoProducto from Producto where id = (select top(1) idproducto from DetalleNotaCredito Where IDTransaccion = N.IDTransaccion)),0),
N.IDSubMotivoNotaCredito,

'SubMotivo' = (Select Descripcion From SubMotivoNotaCredito where Id = N.IDSubMotivoNotaCredito ),
'NroPedidoNC' = PNC.Numero,
'UsuarioPedido' = (Select Nombre from Usuario where Id = (Select IDUsuario from Transaccion where Id = PNC.IDTransaccion)),
'FechaPedido' = (Select Fecha from Transaccion where Id = PNC.IDTransaccion),
'NumeroSolicitudCliente'=Isnull(PNC.NumeroSolicitudCliente,'')

From NotaCredito  N
Join Transaccion T On N.IDTransaccion=T.ID
Left Outer Join VPuntoExpedicion PE On N.IDPuntoExpedicion=PE.ID
Left Outer Join TipoComprobante TC On N.IDTipoComprobante=TC.ID
Left Outer Join Cliente C On N.IDCliente=C.ID
Left Outer Join Sucursal S On N.IDSucursal=S.ID
Left Outer Join Deposito D On N.IDDeposito=D.ID
Left Outer Join Moneda M On N.IDMoneda=M.ID
Left join ClienteSucursal CS on N.IDSucursalCliente = CS.ID AND n.IDCliente = cs.IDCliente
Left Outer Join PedidoNcNotaCredito PNCNC On N.IDTransaccion = PNCNC.IDTransaccionNotaCredito
Left Outer Join PedidoNotaCredito PNC On PNCNC.IdTransaccionPedido = PNC.IdTransaccion
Where N.Procesado=1





































﻿



CREATE View [dbo].[VDebitoCreditoBancario] 

As

Select 

---Cabecera
DB.IDTransaccion,
DB.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
'Ciudad'=S.CodigoCiudad,
DB.Numero,
'Num'=DB.Numero,
DB.IDTipoComprobante,

DB.Debito,
DB.Credito,

'Tipo'=Case When (DB.Credito)= 'True' Then 'CREDITO' Else 'DEBITO'End ,

'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
DB.NroComprobante,
'Comprobante'=DB.NroComprobante,
DB.Facturado ,
DB.Fecha,
'Fec'=CONVERT(varchar(50), DB.Fecha, 6),

--Cuenta Bancaria
'IDCuentaBancaria'=CB.ID,
'Cuenta' = CB.CuentaBancaria,
'Banco'=CB.Banco,
'Moneda'=CB.Mon,
CB.IDMoneda,

DB.Cotizacion,
DB.Observacion,
DB.Total,
DB.TotalImpuesto,
DB.TotalDiscriminado,
DB.Conciliado,
'EsCobranza'= ISNULL(DB.EsCobranza, 'False'),
'EsProcesado'= ISNULL(DB.EsProcesado,'False'),


--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario) 

 
From DebitoCreditoBancario  DB
Join Transaccion T On DB.IDTransaccion=T.ID
Join VSucursal S On DB.IDSucursal=S.ID
JOin VCuentaBancaria CB On DB.IDCuentaBancaria=CB.ID 
Join TipoComprobante TC On DB.IDTipoComprobante=TC.ID
















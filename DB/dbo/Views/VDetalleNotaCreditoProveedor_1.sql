﻿


CREATE View [dbo].[VDetalleNotaCreditoProveedor]
As
Select


DNCP.IDTransaccion,
NC.NroComprobante,

--Producto
DNCP.IDProducto,
DNCP.ID,
'Producto'=P.Descripcion,
DNCP.Observacion,
'Descripcion'=(Case When DNCP.Observacion='' Then P.Descripcion Else P.Descripcion + ' - ' + DNCP.Observacion End),
'CodigoBarra'=P.CodigoBarra,
P.Referencia ,
P.Peso,
P.IDTipoProducto,
P.IDLinea,
'ControlarExistencia'=isnull(P.ControlarExistencia,'False'),

--Deposito
DNCP.IDDeposito,
'Deposito'=D.Descripcion,
--'CuentaContableDeposito'=(Case when D.IDTipoDeposito = 5 then P.CuentaContableCompra else  D.CuentaContableMercaderia end),
'CuentaContableDeposito'=(Case when D.IDTipoDeposito = 5 then P.CodigoCuentaCompra else  (Case when P.IDTipoProducto = 27 then D.CuentaContableCombustible else D.CuentaContableMercaderia end) end),

--Cantidad y Precio
DNCP.Cantidad,
DNCP.PrecioUnitario,
'Pre. Uni.'=DNCP.PrecioUnitario,
DNCP.Total,

--Impuesto
DNCP.IDImpuesto,
'Impuesto'=I.Descripcion,
'Ref Imp'=I.Referencia,
DNCP.TotalImpuesto,
DNCP.TotalDiscriminado,
'Exento'=I.Exento,

--Descuento
DNCP.TotalDescuento,
'Descuento'=DNCP.TotalDescuento,
DNCP.DescuentoUnitario,
'Desc Uni'=DNCP.DescuentoUnitario,
DNCP.PorcentajeDescuento,
'Desc %'=DNCP.PorcentajeDescuento,

--Costos
DNCP.CostoUnitario,
DNCP.TotalCosto,
DNCP.TotalCostoImpuesto,
DNCP.TotalCostoDiscriminado,

--Compra
DNCP.IDTransaccionCompra,
'CuentaContableProveedorTipoProducto'=ISnull(TP.CuentaContableProveedor,''),
'CuentaContableCompra'=ISnull(P.CodigoCuentaCompra,''),

'Comprobante'= IsNull(C.Comprobante , '---'),
--Cantidad de Caja
'Caja'=IsNull(DNCP.Caja, 'False'),
DNCP.CantidadCaja,
'Cajas'=  ISNULL ((DNCP.Cantidad ),1) / ISNULL((Case When (P.UnidadPorCaja) = 0 Then 1 Else P.UnidadPorCaja End),1),
NC.Fecha,
NC.IDTipoComprobante,

NC.IDSucursal,
NC.Anulado

From DetalleNotaCreditoProveedor  DNCP
Join NotaCreditoProveedor NC On DNCP.IDTransaccion=NC.IDTransaccion
Left Outer Join  VCompra  C On DNCP.IDTransaccionCompra=C.IDTransaccion
Join vProducto P On DNCP.IDProducto=P.ID
Join Deposito D On DNCP.IDDeposito=D.ID
Join Impuesto I On DNCP.IDImpuesto=I.ID
Left Outer Join TipoProducto TP on TP.ID=P.IDTipoProducto















﻿

CREATE view [dbo].[VExtractoMovimientoBancarioSaldo]
as
select
EMB.ID,
EMB.IDCuentaBancaria,
'CuentaBanco'= C.Banco,
C.Cuenta,
'CuentaNombre'=C.Nombre,
C.[Codigo CC], 
EMB.Fecha, 
'Fec'= CONVERT (varchar (50), EMB.Fecha, 6),
EMB.IDsucursal,
'Sucursal'=S.Codigo,
EMB.Operacion,
EMB.CompCheque,
EMB.OrdenDetalleConcepto,
EMB.Debito,
EMB.Credito,
EMB.Movimiento,
EMB.SaldoInicial,
EMB.SaldoAnterior,
EMB.Saldo

From ExtractoMovimientoBancario EMB
Join VSucursal S On EMB.IDSucursal=S.ID 
Join VCuentaBancaria C On EMB.IDCuentaBancaria=C.ID

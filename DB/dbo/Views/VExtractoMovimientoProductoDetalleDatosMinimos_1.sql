﻿CREATE view [dbo].[VExtractoMovimientoProductoDetalleDatosMinimos]
as
--Entrada
--Ticket de Bascula
Select 
C.IDTransaccion,
C.IDProducto,
'FechaEmision'=convert(date, C.Fecha),
'Fecha'=convert(date, C.Fecha),
'Fec'=convert (varchar(50),C.Fecha,5),
C.IDDeposito,
C.IDSucursal,
'Entrada'=C.PesoBascula,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, C.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), C.Fecha, 5),
'Anulado'= C.Anulado,
'FechaAnulacion'=C.FechaAnulacion

From TicketBascula C 
Join Transaccion T On C.IDTransaccion=T.ID
Where Procesado = 1
and C.NroAcuerdo > 0

union all

--Compra
Select 
C.IDTransaccion,
DC.IDProducto,
'FechaEmision'=convert(date, C.Fecha),
'Fecha'=convert(date, C.Fecha),
'Fec'=convert (varchar(50),C.Fecha,5),
C.IDDeposito,
C.IDSucursal,
'Entrada'=DC.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, C.FechaEntrada),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), C.FechaEntrada, 5),
'Anulado'= 0,
'FechaAnulacion'=null

From Compra C 
join DetalleCompra DC on C.IDTransaccion=DC.IDTransaccion 
Join Transaccion T On C.IDTransaccion=T.ID

Union all


--Nota de Crédito Cliente
Select
NC.IDTransaccion,
DNC.IDProducto,
'FechaEmision'=convert(date, NC.Fecha),
'Fecha'=convert(date, NC.Fecha),
'Fec'=convert (varchar(50),NC.Fecha,10),
NC.IDDeposito,
NC.IDSucursal,
'Entrada'=DNC.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= NC.Anulado,
'FechaAnulacion'=NC.FechaAnulado

From NotaCredito NC
join DetalleNotaCredito DNC on NC.IDTransaccion=DNC.IDTransaccion
Join Transaccion T On NC.IDTransaccion=T.ID
Where NC.Procesado=1

union all

--Movimiento Entrada
Select
M.IDTransaccion,
DM.IDProducto,
'FechaEmision'=convert(date, M.Fecha),
'Fecha'= convert(date, M.Fecha),
'Fec'=convert (varchar(50),M.Fecha,10),
M.IDDepositoEntrada,
D.IDSucursal,
'Entrada'=DM.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulacion'=(select Fecha from DocumentoAnulado where IDTransaccion = M.IDTransaccion)

From Movimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion
Join Deposito D On DM.IDDepositoEntrada=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join TipoOperacion Ti On M.IDTipoOperacion=Ti.ID
Where Cast(Ti.Entrada as bit)=1 and Cast(Ti.Salida as bit)=0

union all

--Movimiento Salida Anulada
Select
M.IDTransaccion,
DM.IDProducto,
'FechaEmision'=convert(date, M.Fecha),
'Fecha'=convert(date, DA.Fecha ),
'Fec'=convert (varchar(50),DA.Fecha ,10),
M.IDDepositoSalida,
D.IDSucursal,
'Entrada'=DM.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulacion'=Da.Fecha

From Movimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
Join Deposito D On DM.IDDepositoSalida=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Join TipoOperacion Ti On M.IDTipoOperacion=Ti.ID
Where Cast(Ti.Entrada as bit)=0 and Cast(Ti.Salida as bit)=1
And M.Anulado = 1

Union All

--Transferencia Entrada
Select
M.IDTransaccion,
DM.IDProducto,
'FechaEmision'=convert(date, M.Fecha),
'Fecha'=convert(date, M.Fecha),
'Fec'=convert (varchar(50),M.Fecha,10),
M.IDDepositoEntrada ,
D.IDSucursal,
'Entrada'=DM.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulacion'=(select Fecha from DocumentoAnulado where IDTransaccion = M.IDTransaccion)

From Movimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
Join Deposito D On DM.IDDepositoEntrada=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join TipoOperacion Ti On M.IDTipoOperacion=Ti.ID
Where Cast(Ti.Entrada as bit)=1 and Cast(Ti.Salida as bit)=1

Union All

--Transferencia Salida Anulada
Select
M.IDTransaccion,
DM.IDProducto,
'FechaEmision'=convert(date, M.Fecha),
'Fecha'=convert(date, DA.Fecha),
'Fec'=convert (varchar(50),DA.Fecha,10),
M.IDDepositoSalida ,
D.IDSucursal,
'Entrada'=DM.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulacion'=Da.Fecha

From Movimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion
Join Deposito D On DM.IDDepositoSalida=D.ID 
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Join TipoOperacion Ti On M.IDTipoOperacion=Ti.ID
Where Cast(Ti.Entrada as bit)=1 and Cast(Ti.Salida as bit)=1
And M.Anulado = 1

Union all

--Ajuste Inventario Positivo
Select
V.IDTransaccion,
EDI.IDProducto, 
'FechaEmision'=convert(date, V.FechaAjuste),
'Fecha'=convert(date, V.FechaAjuste),
'Fec'=convert (varchar(50),V.FechaAjuste,10),
EDI.IDDeposito,
EDI.IDSucursal,
'Entrada'=EDI.DiferenciaSistema,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, V.FechaAjuste),
'FechaOperacion'=CONVERT(date, V.FechaAjuste),
'Hora Ope.'=Convert(varchar(10), V.FechaTransaccion, 8),
'Fec. Ope.'=Convert(varchar(10), V.FechaTransaccion, 5),
'Anulado'= 0,
'FechaAnulacion'=V.FechaAnulacion


From VControlInventario V
 join VExistenciaDepositoInventario EDI on V.IDTransaccion=EDI.IDTransaccion 
 Join Transaccion T On V.IDTransaccion=T.ID
Where EDI.DiferenciaSistema >0 And V.Procesado=1 And V.Anulado=0

union all

--Ventas Anuladas
Select 
V.IDTransaccion,
DV.IDProducto,
'FechaEmision'=convert(date, V.FechaEmision),
'Fecha'=Case When DA.Fecha > V.FechaEmision then Convert(date, da.Fecha) else Convert(date, V.FechaEmision) end,
'Fec'=Case When DA.Fecha > V.FechaEmision then convert (varchar(50),da.Fecha,10) else Convert(varchar(50), V.FechaEmision,10) end,
V.IDDeposito,
V.IDSucursal,
'Entrada'=DV.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, V.FechaEmision),
'FechaOperacion'=CONVERT(date, V.FechaEmision),
'Hora Ope.'=Convert(varchar(10), V.FechaEmision, 8),
'Fec. Ope.'=Convert(varchar(10), V.FechaEmision, 5),
'Anulado'= V.Anulado,
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=V.IDTransaccion)

From Venta V
join DetalleVenta DV on V.IDTransaccion=DV.IDTransaccion 
Join Transaccion T On V.IDTransaccion=T.ID
Join DocumentoAnulado DA On V.IDTransaccion=DA.IDTransaccion
Where V.Anulado=1
And V.Procesado=1

Union All

--Ajuste Inicial positivo
Select 
A.IDTransaccion,
D.IDProducto,
'FechaEmision'=convert(date, A.Fecha),
'Fecha'=convert(date, A.Fecha),
'Fec'=convert (varchar(50),A.Fecha,10),
D.IDDeposito,
D.IDSucursal,
'Entrada'=D.Entrada,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= 0,
'FechaAnulacion'=null

From AjusteInicial A 
join DetalleAjusteInicial D on A.IDTransaccion=D.IDTransaccion 
Join Transaccion T On A.IDTransaccion=T.ID
Where A.Procesado=1
And D.Entrada>0

--SALIDAS

Union All

--Venta
Select 
V.IDTransaccion,
DV.IDProducto,
'FechaEmision'=convert(date, V.FechaEmision),
'Fecha'=convert(date, V.FechaEmision),
'Fec'=convert (varchar(50),V.FechaEmision,10),
V.IDDeposito,
V.IDSucursal,
'Entrada'=0.00,
'Salida'=DV.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= V.Anulado,
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=V.IDTransaccion)

From Venta V
join DetalleVenta DV on V.IDTransaccion=DV.IDTransaccion 
Join Transaccion T On V.IDTransaccion=T.ID
Where V.Procesado=1


union all

--Nota de Crédito Cliente ANULADO
Select
NC.IDTransaccion,
DNC.IDProducto,
'FechaEmision'=convert(date, NC.Fecha),
'Fecha'=convert(date, DA.Fecha),
'Fec'=convert (varchar(50),DA.Fecha,10),
NC.IDDeposito,
NC.IDSucursal,
'Entrada'=0.00,
'Salida'=DNC.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
'Anulado'= NC.Anulado,
'FechaAnulacion'=NC.FechaAnulado

From notaCredito NC
join DetalleNotaCredito DNC on NC.IDTransaccion=DNC.IDTransaccion
Join Transaccion T On NC.IDTransaccion=T.ID
Join DocumentoAnulado DA On NC.IDTransaccion=DA.IDTransaccion
WHERE NC.Anulado = 1
And NC.Procesado=1

union all

--Nota Crédito Proveedor
Select 
NCP.IDTransaccion,
DNP.IDProducto,
'FechaEmision'=convert(date, NCP.Fecha),
'Fecha'=convert(date, NCP.Fecha),
'Fec'=convert (varchar(50),NCP.Fecha,10),
NCP.IDDeposito,
NCP.IDSucursal,
'Entrada'=0.00,
'Salida'=DNP.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= NCP.Anulado,
'FechaAnulacion'=NCP.FechaAnulado

From NotaCreditoProveedor NCP
join DetalleNotaCreditoProveedor DNP on NCP.IDTransaccion=DNP.IDTransaccion 
join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor=NCP.IDTransaccion And DNP.ID=NCPC.ID
Join Transaccion T On NCP.IDTransaccion=T.ID

union all

--Movimiento Salida
Select
M.IDTransaccion,
DM.IDProducto,
'FechaEmision'=convert(date, M.Fecha),
'Fecha'=convert(date, M.Fecha),
'Fec'=convert (varchar(50),M.Fecha,10),
M.IDDepositoSalida,
D.IDSucursal,
'Entrada'=0.00,
'Salida'=DM.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=M.IDTransaccion)

From Movimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
Join Deposito D On DM.IDDepositoSalida=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join TipoOperacion Ti On M.IDTipoOperacion=Ti.ID
Where Cast(Ti.Entrada as bit)=0 and Cast(Ti.Salida as bit)=1

Union All 

--Movimiento Entrada Anulado
Select
M.IDTransaccion,
DM.IDProducto,
'FechaEmision'=convert(date, M.Fecha),
'Fecha'= convert(date, DA.Fecha),
'Fec'=convert (varchar(50), DA.Fecha,10),
M.IDDepositoEntrada,
D.IDSucursal,
'Entrada'=0.00,
'Salida'=DM.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulacion'=DA.Fecha

From Movimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion
Join Deposito D On DM.IDDepositoEntrada=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Join TipoOperacion Ti On M.IDTipoOperacion=Ti.ID
Where Cast(Ti.Entrada as bit)=1 and Cast(Ti.Salida as bit)=0
And M.Anulado = 1

Union all

--Transferencia Salida
Select
M.IDTransaccion,
DM.IDProducto,
'FechaEmision'=convert(date, M.Fecha),
'Fecha'=convert(date, M.Fecha),
'Fec'=convert (varchar(50),M.Fecha,10),
M.IDDepositoSalida ,
D.IDSucursal,
'Entrada'=0.00,
'Salida'=DM.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=M.IDTransaccion)

From Movimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
Join Deposito D On DM.IDDepositoSalida=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join TipoOperacion Ti On M.IDTipoOperacion=Ti.ID
Where Cast(Ti.Entrada as bit)=1 and Cast(Ti.Salida as bit)=1

Union All

--Transferencia Entrada Anulada
Select
M.IDTransaccion,
DM.IDProducto,
'FechaEmision'=convert(date, M.Fecha),
'Fecha'=convert(date, DA.Fecha),
'Fec'=convert (varchar(50),DA.Fecha,10),
M.IDDepositoEntrada ,
D.IDSucursal,
'Entrada'=0.00,
'Salida'=DM.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulacion'=DA.Fecha

From Movimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
Join Deposito D On DM.IDDepositoEntrada=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Join TipoOperacion Ti On M.IDTipoOperacion=Ti.ID
Where Cast(Ti.Entrada as bit)=1 and Cast(Ti.Salida as bit)=1
And M.Anulado = 1

union all

--Ajuste Inventario Negativo
Select 
V.IDTransaccion,
EDI.IDProducto,
'FechaEmision'=convert(date, V.FechaAjuste),
'Fecha'=convert(date, V.FechaAjuste),
'Fec'=convert (varchar(50),V.FechaAjuste,10),
EDI.IDDeposito,
EDI.IDSucursal,
'Entrada'=0.00,
'Salida'=EDI.DiferenciaSistema * -1,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, V.FechaTransaccion),
'FechaOperacion'=CONVERT(date, V.FechaAjuste),
'Hora Ope.'=Convert(varchar(10), V.FechaAjuste, 8),
'Fec. Ope.'=Convert(varchar(10), V.FechaAjuste, 5),
'Anulado'= 0,
'FechaAnulacion'=V.FechaAnulacion

From VControlInventario V
 join VExistenciaDepositoInventario EDI on V.IDTransaccion=EDI.IDTransaccion 
 Join Transaccion T On V.IDTransaccion=T.ID
Where EDI.DiferenciaSistema < 0 And V.Procesado=1 And V.Anulado = 0

Union All

--Ajuste Inicial Negativo
Select 
A.IDTransaccion,
D.IDProducto,
'FechaEmision'=convert(date, A.Fecha),
'Fecha'=convert(date, A.Fecha),
'Fec'=convert (varchar(50),A.Fecha,10),
D.IDDeposito,
D.IDSucursal,
'Entrada'=0.00,
'Salida'=D.Salida,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
'Anulado'= 0,
'FechaAnulacion'=null

From AjusteInicial A 
join DetalleAjusteInicial D on A.IDTransaccion=D.IDTransaccion 
Join Transaccion T On A.IDTransaccion=T.ID
Where A.Procesado=1
And D.Salida>0

union all

--Ticket de Bascula
Select 
C.IDTransaccion,
C.IDProducto,
'FechaEmision'=convert(date, C.Fecha),
'Fecha'=convert(date, C.FechaAnulacion),
'Fec'=convert (varchar(50),C.FechaAnulacion,5),
C.IDDeposito,
C.IDSucursal,
'Entrada'=0.00,
'Salida'=C.PesoBascula,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, C.FechaAnulacion),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), C.Fecha, 5),
'Anulado'= C.Anulado,
'FechaAnulacion'=C.FechaAnulacion

From TicketBascula C 
Join Transaccion T On C.IDTransaccion=T.ID
And C.Anulado = 1 And C.Procesado = 1


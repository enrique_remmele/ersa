﻿

CREATE View [dbo].[VDetalleCuotaPrestamoBancario]

As

Select 

DC.*,

--DP.IDTransaccion,
'NroPrestamo'=P.NroComprobante,
'NroCreditoBancario'=concat('(',CB.Suc,'-',CB.Numero,')-',CB.NroComprobante)
--'Cuota'=DP.NroCuota,
--DP.FechaVencimiento,
--DP.ImporteCuota,
--'FechaPago'= cast(DP.FechaPago as date),
--DP.ImporteAPagar,
--DP.Cancelado,
--'Cancel'=(Case When DP.Cancelado = 'True' Then 'Si' Else 'No' End),
--'Estado'=(Case When DP.Cancelado = 'True' Then 'Pendiente' Else 'Cancelado' End),

----CuentaBancaria
----Transaccion
--'FechaTransaccion'=TR.Fecha,
--TR.IDDeposito,
--TR.IDTerminal,
--TR.IDUsuario,
--'usuario'=(Select U.Usuario From Usuario U Where U.ID=TR.IDUsuario),
--'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=TR.IDUsuario),

----Credito Bancario
--'IDTransaccionDebitoCreditoBancario'=CB.IDTransaccion,
--'Credito Bancario'=(Case When (CB.IDTransaccion) Is Null Then '' Else (CB.Suc + ' ' + Convert(varchar(10), CB.Numero) + ' - ' + CB.[Cod.] + ' ' + Convert(varchar(10), CB.Comprobante)) End)
--'CreditoBancarioAsociado'=(Case When (CB.IDTransaccion) Is Null Then 'False' Else 'True' End),
--'DebitoAutomatico'=IsNull(DP.DebitoAutomatico, 'True'),
--'PagoCuota'=CB.Total,

--'SaldoCuota'=DP.Saldo

--Cabecera
--P.Numero,
--P.Fecha

From DetalleCuotaPrestamoBancario DC
--Left Outer Join DetallePrestamoBancario DP On DC.IDTransaccionPrestamoBancario = DP.IDTransaccion
Left Outer Join VPrestamoBancario P On DC.IDTransaccionPrestamoBancario = P.IDTransaccion
--Left Outer Join Transaccion TR On DP.IDTransaccion=TR.ID
--Left Outer Join TipoComprobante TC On P.IDTipoComprobante = TC.ID
Left Outer Join VDebitoCreditoBancario CB On DC.IDTransaccionDebitoCreditoBancario=CB.IDTransaccion







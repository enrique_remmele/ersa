﻿Create View [dbo].[VDevolucionLote]
As
Select 

--Cabecera
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
S.IDCiudad,
DL.Numero,
DL.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
DL.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
'Comprobante'=DL.Comprobante,
'Lote'=LD.Comprobante,
DL.Fecha,
'Fec'=CONVERT(varchar(50), DL.Fecha, 6),
'Distribuidor'=LD.Distribuidor,
LD.IDDistribuidor,
'Chofer'=LD.Chofer,
'Vehiculo'=LD.Camion,
'Zona'=LD.Zona,
DL.Observacion,

--Lote
LD.FechaReparto,
'Fec Reparto'=CONVERT(varchar(50), LD.FechaReparto, 6),

--Totales

--Transaccion
DL.IDTransaccion,
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
DL.Anulado,
'Estado'=Case When DL.Anulado='True' Then 'Anulado' Else '---' End,
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=DL.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=DL.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=DL.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=DL.IDTransaccion)

From DevolucionLote DL
Join Transaccion T On DL.IDTransaccion=T.ID
Join VLoteDistribucion LD On DL.IDTransaccionLote=LD.IDTransaccion
Left Outer Join Sucursal S On DL.IDSucursal=S.ID
Left Outer Join TipoComprobante TC On DL.IDTipoComprobante=TC.ID










﻿
--USE [EXPRESS]
--GO

--/****** Object:  View [dbo].[VDetalleCargaMercaderia]    Script Date: 08/27/2012 12:54:53 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO



CREATE View [dbo].[VDetalleCargaMercaderia]
As
Select

DC.IDTransaccion,

--Producto
DC.IDProducto,
'ID'=DC.IDProducto,
'Producto'=P.Descripcion,
'Descripcion'=P.Descripcion,
'CodigoBarra'=P.CodigoBarra,
'Ref'=P.Referencia,
P.UnidadPorCaja,

--Cantidad y Precio
DC.Cantidad,
--DC.Total,

--DC.TotalDescuento,
--'Descuento'=DC.TotalDescuento,
'Cajas'=IsNull((DC.Cantidad / P.UnidadPorCaja), 0)

From DetalleCargaMercaderia DC
Join Producto P On DC.IDProducto=P.ID









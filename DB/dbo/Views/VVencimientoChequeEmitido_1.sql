﻿

CREATE View [dbo].[VVencimientoChequeEmitido]
As

Select 

E.IDTransaccion,
E.Fecha,
'Anulado'=IsNull(E.Anulado, 'False'),
'Procesado'=IsNull(E.Procesado, 'False'),

--OP
E.IDTransaccionOP, 
'NumeroOP' = VO.Numero,
VO.IDSucursal,
VO.IDMoneda,
'Cotizacion'=(select dbo.FCotizacionAlDiafecha(VO.IDMoneda,0,E.Fecha))

From VencimientoChequeEmitido E
Join VOrdenPago VO On E.IDTransaccionOP = VO.IDTransaccion



﻿
CREATE View [dbo].[VExistenciaDeposito]
As

Select
'IDDeposito'=D.ID,
'Deposito'=D.Descripcion,
'IDSucursal'=D.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc-Dep'=S.Descripcion + ' - ' + D.Descripcion,

--Producto
'IDProducto'=P.ID,
'Producto'=P.Descripcion,
'Referencia'=P.Referencia,
'CodigoBarra'=P.CodigoBarra,
'ControlarExistencia'=P.ControlarExistencia,
P.UnidadMedida ,
P.Estado,
P.UltimaEntrada,
'PesoUnitario'=convert(decimal(18,2),Isnull(P.Peso,1)),

--Clasificacion
P.IDTipoProducto,
P.TipoProducto,
P.IDLinea,
P.Linea,
P.IDSubLinea,
P.SubLinea,
P.IDSubLinea2,
P.SubLinea2,
P.IDMarca,
P.Marca,
P.IDPresentacion,
P.IDCategoria,
P.Categoria,
P.IDProveedor,
P.Proveedor,

--Existencias
'Existencia'=ED.Existencia,
'ExistenciaMinima'=ED.ExistenciaMinima,
'ExistenciaCritica'=ED.ExistenciaCritica,
'ExistenciaGeneral'=P.ExistenciaGeneral,
'PlazoReposicion'=ED.PlazoReposicion,

--Costos
P.Costo,
P.CostoPromedio,
P.UltimoCosto ,

P.CostoPromedio * ED.Existencia AS TotalValorizadoCostoPromedio,
--Deposito
D.IDTipoDeposito,
'TipoDeposito'=TD.Descripcion

From Deposito D
join TipoDeposito TD on D.IDTipoDeposito = TD.ID
Join Sucursal S On D.IDSucursal=S.ID
Join ExistenciaDeposito ED On D.ID=ED.IDDeposito
Join VProducto P On ED.IDProducto=P.ID
WHERE P.ControlarExistencia = 1


















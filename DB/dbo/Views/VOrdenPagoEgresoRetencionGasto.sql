﻿
CREATE View [dbo].[VOrdenPagoEgresoRetencionGasto]
As
with t1 as (
--Datos de Gastos
Select
C.IDTransaccion,
C.NroComprobante,
'T. Comp.'=C.TipoComprobante,
C.TipoComprobante,
'TipoyComprobante'= concat (C.TipoComprobante , ' ',C.NroComprobante),
C.Proveedor,
C.IDProveedor,
P.Retentor,
C.Fecha,
C.Fec,
C.FechaVencimiento,
C.[Fec. Venc.],
Cuota=isnull(OPE.Cuota,1),
--Totales
OPE.Saldo,
OPE.Importe,
'Total'=C.Total,
'RetencionIVA'=isnull(OPE.Retencion,0),
'RetencionRenta' = isnull(c.RetencionRenta,0),
C.IDMoneda,
C.Moneda,
C.Credito ,
'Cotizacion' = (Case when isnull(C.IDMoneda,0) = 1 then 1 else (case when isnull(C.Cotizacion,0)=0 then 1 else C.Cotizacion end) end),
C.Observacion,
'Seleccionado'='False',
'Cancelar'='False',
'PorcRetencion'=(Select Top 1 CompraPorcentajeRetencion From Configuraciones),
'PorcRenta'=0,
'Renta'=0,
--Orden de Pago
OPE.IDTransaccionOrdenPago,
--IDTransaccion
OPE.IDTransaccionEgreso,
OP.Numero,
OP.IDSucursal,
OP.Suc,
'NroOP'=OP.NroComprobante,
'TipoComprobanteOP'= OP.TipoComprobante,

--Cheque
OP.NroCheque,
'FechaOP' = OP.Fecha,
'RRHH'=C.RRHH,
'Procesado'=ISNULL(RI.Procesado,0),
'ProcesaRetencion'= OPE.ProcesaRetencion

From OrdenPagoEgreso OPE
Join VOrdenPago OP On OPE.IDTransaccionOrdenPago=OP.IDTransaccion
Join VGasto C On OPE.IDTransaccionEgreso=C.IDTransaccion
Join VProveedor P on P.ID=C.IDProveedor
left join VRetencionIVA RI on RI.IDTransaccionOrdenPago = OPE.IDTransaccionOrdenPago 
                          and OPE.IDTransaccionEgreso = RI.IDTransaccionEgreso
                          and RI.anulado = 0
--where OPE.IDTransaccionOrdenPago = 1387633
),
t2 as (
Select
C.IDTransaccion,
--'Total10'=sum(DI.Total),
'TotalIVA10'= sum(ISNULL(case when DI.IDImpuesto= 1 then DI.TotalImpuesto end,0)),
'IVA10'= sum(ISNULL(case when DI.IDImpuesto= 1 then DI.TotalDiscriminado end,0))
From OrdenPagoEgreso OPE
Join VOrdenPago OP On OPE.IDTransaccionOrdenPago=OP.IDTransaccion
Join VGasto C On OPE.IDTransaccionEgreso=C.IDTransaccion
join VDetalleImpuesto DI on C.IDTransaccion = DI.IDTransaccion and DI.IDImpuesto = 1
Join VProveedor P on P.ID=C.IDProveedor
left join VRetencionIVA RI on RI.IDTransaccionOrdenPago = OP.IDTransaccion and RI.Anulado = 0 
--where OPE.IDTransaccionOrdenPago = 1387565
group by C.IDTransaccion
),
t3 as (
Select
C.IDTransaccion,
--'Total5'=sum(DI.Total),
'TotalIVA5'= sum(ISNULL(case when DI.IDImpuesto= 2 then DI.TotalImpuesto end,0)),
'IVA5'= sum(ISNULL(case when DI.IDImpuesto= 2 then DI.TotalDiscriminado end,0))
From OrdenPagoEgreso OPE
Join VOrdenPago OP On OPE.IDTransaccionOrdenPago=OP.IDTransaccion
Join VGasto C On OPE.IDTransaccionEgreso=C.IDTransaccion
join VDetalleImpuesto DI on C.IDTransaccion = DI.IDTransaccion and DI.IDImpuesto = 2
Join VProveedor P on P.ID=C.IDProveedor
left join VRetencionIVA RI on RI.IDTransaccionOrdenPago = OP.IDTransaccion and RI.Anulado = 0 
--where OPE.IDTransaccionOrdenPago = 1387565
group by C.IDTransaccion
),
t4 as (
Select
C.IDTransaccion,
'TotalExento'= sum(ISNULL(case when DI.IDImpuesto= 3 then DI.Total end,0))
From OrdenPagoEgreso OPE
Join VOrdenPago OP On OPE.IDTransaccionOrdenPago=OP.IDTransaccion
Join VGasto C On OPE.IDTransaccionEgreso=C.IDTransaccion
join VDetalleImpuesto DI on C.IDTransaccion = DI.IDTransaccion and DI.IDImpuesto = 3
Join VProveedor P on P.ID=C.IDProveedor
left join VRetencionIVA RI on RI.IDTransaccionOrdenPago = OP.IDTransaccion and RI.Anulado = 0 
--where OPE.IDTransaccionOrdenPago = 1387565
group by C.IDTransaccion
)
select t1.*, 
      'TotalIVA10'=ISNULL(t2.TotalIVA10,0),'IVA10'=ISNULL(t2.IVA10,0),
       'TotalIVA5'=ISNULL(t3.TotalIVA5,0),'IVA5'=ISNULL(t3.IVA5,0),
	   'TotalExento'=ISNULL(t4.TotalExento,0)
from t1
left join t2 on t1.IDTransaccion = t2.IDTransaccion
left join t3 on t1.IDTransaccion = t3.IDTransaccion
left join t4 on t1.IDTransaccion = t4.IDTransaccion











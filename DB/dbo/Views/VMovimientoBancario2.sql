﻿



CREATE View [dbo].[VMovimientoBancario2]

As

--CARLOS 26-02-2014

--DEPOSITO

Select
IDCuentaBancaria,
'Fec'=CONVERT (varchar (50),fecha,6),
'Movimiento'='DEPOSITO',
'Operacion'=Numero,
'Comprobante'=TipoComprobante + ' ' + Convert(varchar(50), Comprobante),
'Sucursal'=Suc,
'Debito'=Total,
'Credito'=0,
'Saldo'=Total,
'Observacion'=Observacion,
'A la Orden'='',
'Conciliado'=Case When(Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= IDTransaccion,
'Fecha'=Fecha, 
'Diferido'='---',
'FechaVencimiento'=CONVERT (varchar (50),fecha,6)
From VDepositoBancario


Union All

--DEBITO

Select
IDCuentaBancaria,
'Fecha'=CONVERT (varchar (50),fecha,6),
'Movimiento'='DEBITO',
'Operacion'=Numero,
'Comprobante'=TipoComprobante + ' ' + Convert(varchar(50), Comprobante),
'Sucursal'= Suc ,
'Debito'=Total,
'Credito'=0,
'Saldo'=Total,
'Observacion'=Observacion,
'A la Orden'='',
'Conciliado'=Case When(Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= IDTransaccion,
'Fecha'=Fecha, 
'Diferido'='---',
'FechaVencimiento'=CONVERT (varchar (50),fecha,6)
From VDebitoCreditoBancario
Where Debito = 'True'

Union All

--CREDITO
Select
IDCuentaBancaria,
'Fecha'=CONVERT (varchar (50),fecha,6),
'Movimiento'='CREDITO',
'Operacion'=Numero,
'Comprobante'=TipoComprobante + ' ' + Convert(varchar(50), Comprobante),
'Sucursal'=Suc ,
'Debito'=0,
'Credito'=Total,
'Saldo'=Total,
'Observacion'=Observacion,
'A la Orden'='',
'Conciliado'=Case When(Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= IDTransaccion,
'Fecha'=Fecha,  
'Diferido'='---',
'FechaVencimiento'= CONVERT (varchar (50),fecha,6)
From VDebitoCreditoBancario
Where Credito = 'True'

Union All

--ORDEN DE PAGO
Select
op.IDCuentaBancaria,
'Fecha'=CONVERT (varchar (50),op.fecha,6),
'Movimiento'='PAGO',
'Operacion'=op.Numero,
'Comprobante'=(Case When op.Diferido ='True'Then 'DIF'+'  '+CONVERT(varchar(50),op.NroCheque) When op.Diferido='False'Then 'CHQ'+'  '+CONVERT(varchar(50),op.NroCheque) Else '' End) ,
'Sucursal'= op.Suc ,
'Debito'=0,
'Credito'=op.ImporteMoneda,
'Saldo'=op.TotalImporteMoneda,
'Observacion'=op.Observacion,
'A la Orden'=op.ALaOrden,
'Conciliado'=Case When(op.Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= op.IDTransaccion,
'Fecha'=op.Fecha, 
'Diferido'=Case When(c.Diferido) = 'True' Then 'SI' Else '---' End,
'FechaVencimiento'=CONVERT (varchar (50),c.FechaVencimiento,6)
From VOrdenPago op
JOIN VCheque c on op.IDTransaccion = c.IDTransaccion 
Where op.Cheque = 'True'And op.Anulado ='False'

Union All

--Descuento Cheque
Select
IDCuentaBancaria,
'Fecha'=CONVERT (varchar (50),fecha,6),
'Movimiento'='DEPOSITO',
'Operacion'=Numero,
'Comprobante'=TipoComprobante + ' ' + Convert(varchar(50), Comprobante),
'Sucursal'= Suc ,
'Debito'=TotalAcreditado,
'Credito'=0,
'Saldo'=TotalAcreditado,
'Observacion'=Observacion,
'A la Orden'='',
'Conciliado'=Case When(Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= IDTransaccion,
'Fecha'=Fecha, 
'Diferido'='---',
'FechaVencimiento'=CONVERT (varchar (50),fecha,6)
From VDescuentoCheque 

Union All

--Cheque Rechazado
Select
V.IDCuentaBancaria,
'Fecha'=CONVERT (varchar (50), V.fecha,6),
'Movimiento'='CHEQUE RECHAZADO',
'Operacion'=V.Numero,
--'Operacion'=CHQ.Numero,
'Comprobante'= CHQ.CodigoTipo + ' ' + Convert(varchar(50), CHQ.NroCheque),
'Sucursal'= Suc ,
'Debito'=0,
'Credito'=CHQ.Importe,
'Saldo'=CHQ.Importe,
'Observacion'=V.MotivoRechazo,
'A la Orden'=CHQ.Librador,
'Conciliado'=Case When(V.Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= V.IDTransaccion,
'Fecha'=V.Fecha, 
'Diferido'='---',
'FechaVencimiento'=CONVERT (varchar (50),v.fecha,6)
From VChequeClienteRechazado V
Join VChequeCliente CHQ On V.IDTransaccionCheque=CHQ.IDTransaccion

Where V.Anulado='False'






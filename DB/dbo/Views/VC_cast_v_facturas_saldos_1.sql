﻿create view [VC_cast_v_facturas_saldos]
as  Select
'cod_cliente'=V.IDCliente,
'nombre'=V.Cliente,
'numero_factura'=V.Comprobante,
'tipo_factura'=V.Condicion,
'monto_factura'=V.Total,
'saldo'=V.Saldo,
'fecha_factura'=V.Fec,
'fecha_vencimiento_factura'=V.[Fec. Venc.],
'cod_cobrador'= V.IDCobrador,
'cod_moneda' =V.IDMoneda,
'moneda'=V.Moneda,
'cotizacion'=V.Cotizacion,
'CotizacionHoy'=ISNull((select Cotizacion from Cotizacion where idmoneda = V.IdMOneda and Fecha = cast(GETDATE() as date)),1)
From VVenta V
Where V.Cancelado = 'False'
And V.Anulado='False'
﻿CREATE View [dbo].[VTipoComprobante]
As
Select
TC.ID, 
TC.Cliente,
TC.Proveedor,
'Tipo'=(Case When (TC.Cliente) = 'True' Then 'CLIENTE' Else (Case When (TC.Proveedor) = 'True' Then 'PROVEEDOR' Else '---' End) End),
TC.IDOperacion,
'Operacion'=O.Descripcion,
TC.Descripcion,
TC.Codigo,
TC.Resumen,
TC.Estado,

TC.ComprobanteTimbrado,
TC.CalcularIVA,
TC.IVAIncluido,
TC.LibroVenta,
TC.LibroCompra,
'Libro'=(Case When TC.LibroVenta = 'True' Then 'True' Else (Case When TC.LibroCompra = 'True' Then 'True' Else 'False' End) End),
TC.Signo,
TC.Autonumerico,
TC.Restar,
TC.Deposito,

--Hechauka
 'HechaukaTipoDocumento'=ISNULL(HechaukaTipoDocumento, '0'), 
 'HechaukaTimbradoReemplazar'=ISNULL(HechaukaTimbradoReemplazar, 'False'), 
 'HechaukaNumeroTimbrado'=ISNULL(HechaukaNumeroTimbrado, ''),

--Operacion
O.FormName
,TC.EsInterno,
TC.Exento,
'AjusteStock'=Isnull(TC.AjusteStock,0),
'ComprobanteConsumo'=Isnull(TC.ComprobanteConsumo,0)


From TipoComprobante TC
Join Operacion O On TC.IDOperacion=O.ID




﻿



CREATE View [dbo].[VC_cast_v_clientes]
As
Select
'idcliente'=C.ID,
'cod_cliente'=C.Referencia,
'nombre'=C.RazonSocial,
'nombre_fantasia'= C.NombreFantasia,
'direccion'=C.Direccion,
'barrio' = C.Barrio,
'ciudad'= C.Ciudad,
'telefono'=C.Telefono,
'latitud'=C.Latitud,
'longitud'=C.Longitud,
'ruc'=C.RUC,
'credito_asignado'=C.LimiteCredito,
'deuda_total'=C.Deuda,
'deuda_vencida'=IsNull((Select Sum(VV.Saldo) From VVenta VV Where VV.IDCliente=C.ID And FechaVencimiento<GetDate()),0),
'credito_disponible'=Case When C.Contado='True' Then 0 else C.LimiteCredito-C.Deuda End,
'cod_vendedor'=V.ID,
'cod_condicion_venta'=C.Condicion,
'estado'=C.Estado,
'cod_rubro'=C.IDTipoCliente,
'cod_lisa_de_precio'=C.IDListaPrecio


From VCliente C
Join VVendedor V On C.IDVendedor=V.ID






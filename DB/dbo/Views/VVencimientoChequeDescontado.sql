﻿CREATE view [dbo].[VVencimientoChequeDescontado]
as
Select 
---Cabecera
C.IDTRansaccion ,
C.Numero,
C.Fecha ,
'Fec'= CONVERT (varchar(50),C.Fecha,6),
C.IDTransaccionCheque, 
C.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
'Ciudad'=S.CodigoCiudad,
C.Observacion,
C.Anulado,
C.Conciliado,
'IDBanco' = dbo.FBancoDescuentoCheque(C.IDTransaccionCheque),
'Banco' = (select descripcion from banco where id =(dbo.FBancoDescuentoCheque(C.IDTransaccionCheque))),

--Cheque Cliente
'Referencia'=CC.CodigoCliente,
CC.Cliente,
CC.RUC,
CC.NroCheque,
CC.Moneda,
CC.Cotizacion,
CC.Importe,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'Usuario'=(Select top(1)  U.Usuario From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select top(1) DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=C.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select top(1) U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=C.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select top(1) U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=C.IDTransaccion), '---')),
'FechaAnulacion'=(Select top(1) DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=C.IDTransaccion)

    

From VencimientoChequeDescontado C
Join Transaccion T On C.IDTRansaccion =T.ID
Join VSucursal S On C.IDSucursal=S.ID   
Join VChequeCliente CC On CC.IDTransaccion= C.IDTransaccionCheque 




﻿CREATE view [dbo].[VOperacionPermitida] as
select
OP.ID,
OP.IDOperacion,
'Operacion' = O.Descripcion,
OP.IDTipoComprobante,
'CodigoComprobante' = TC.Codigo,
'TipoComprobante'=TC.Descripcion,
OP.IDTipoOperacion,
'TipoOperacion'= T.Descripcion,
OP.Estado
from OperacionPermitida OP
join Operacion O on O.ID = OP.IDOperacion
join TipoComprobante TC on TC.ID = OP.IDTipoComprobante
join TipoOperacion T on T.ID = OP.IDTipoOperacion

﻿

CREATE view [dbo].[VInventarioDocumentosPendientesLote]
as

--Sergio 22-03-2014 , cambio de ultimo pago y agregar fecha de hoy y dias de retraso. 

Select
'Documento'=VLD.[Cod.]+VLD.Comprobante,
'NroComprobante'=VLD.Comprobante,
V.IDTipoComprobante,
V.Condicion,
'Lote'=Numero,
Chofer,
VLD.Vendedor,
V.IDVendedor,
V.IDCobrador,
V.IDCliente,
V.Direccion,
C.RazonSocial as cliente,
V.IDTipoCliente,
V.IDEstado,
V.Estado,
VLD.Referencia,
V.IDSucursal,
VLD.Suc,
V.IDCiudad,
V.IDDeposito,
V.IDZonaVenta,
V.FechaEmision,
'Fecha Vencimiento'=IsNull(V.FechaVencimiento, V.FechaEmision),
'Fecha de hoy' = GetDate (),
'Dias de Retraso' = DateDiff (DAY, V.FechaEmision, GetDate () ),
V.Anulado,
V.Cancelado,
V.IDMoneda,
'UltimoPago'=(select MAX (C.FechaCobranza)from VVentaDetalleCobranza C Where C.IDTransaccion = V.IDTransaccion ),
'IMPORTE'=VLD.Total,
V.Cotizacion,
'ImporteGs'=V.Total * V.Cotizacion,
VLD.SaldoComprobante,
V.FechaVencimiento,
V.IdTransaccion,
V.IDSucursalFiltro, --Sucursal del cliente
'IDTipoProducto'=(Select top(1) IDTipoProducto from vdetalleVenta where IDTransaccion = V.IDTransaccion),
'TipoProducto'= ISnull((Select Top(1) TipoProducto from vdetalleventa where IDTransaccion = V.IDTransaccion), '')

From VVentaLoteDistribucion VLD
join Cliente C on C.ID = VLD.IDCliente
Join VVenta V on VLD.IDTransaccionVenta=V.IDTransaccion

--Ventas que no tienen lotes

Union All

Select
'Documento'=V.TipoComprobante + V.Comprobante,
'NroComprobante'=V.Comprobante,
V.IDTipoComprobante,
V.Condicion,
'Lote'=0,
'---',
V.Vendedor,
V.IDVendedor,
V.IDCobrador,
V.IDCliente,
V.Direccion,
V.Cliente,
V.IDTipoCliente,
V.IDEstado,
V.Estado,
V.Referencia,
V.IDSucursal,
V.Sucursal,
V.IDCiudad,
V.IDDeposito,
V.IDZonaVenta,
V.FechaEmision,
'Fecha Vencimiento'=IsNull(V.FechaVencimiento, V.FechaEmision),
'Fecha de hoy' = GetDate (),
'Dias de Retraso' = DateDiff (DAY, V.FechaEmision, GetDate () ),
V.Anulado,
V.Cancelado,
V.IDMoneda,
'UltimoPago'=(select MAX (C.FechaCobranza)from VVentaDetalleCobranza C Where C.IDTransaccion = V.IDTransaccion ),
'IMPORTE'=V.Total,
V.Cotizacion,
'ImporteGs'=V.Total * V.Cotizacion,
V.Saldo,
V.FechaVencimiento,
V.Idtransaccion,
V.IDSucursalFiltro, --Sucursal del cliente
'IDTipoProducto'=(Select top(1) IDTipoProducto from vdetalleVenta where IDTransaccion = V.IDTransaccion),
'TipoProducto'= ISnull((Select Top(1) TipoProducto from vdetalleventa where IDTransaccion = V.IDTransaccion), '')
From VVenta V
Where (Select Count(*) From VentaLoteDistribucion VV Where VV.IDTransaccionVenta=V.IDTransaccion) = 0






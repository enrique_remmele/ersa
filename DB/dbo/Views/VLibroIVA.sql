﻿CREATE view [dbo].[VLibroIVA]

as

--Compra
Select 
'Fecha'=Convert(date, C.Fecha),
'Comprobante'=convert(varchar(50),C.Comprobante),
C.IDTipoComprobante,
C.DescripcionTipoComprobante,
C.[Cod.],
'Sucursal'=S.Descripcion,
C.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Compra',
'Credito'=NULL,
'IDCliente'=0,
C.IDProveedor,
'RazonSocial'=C.Proveedor,
C.RUC,
C.Directo,
'EX'=Case When C.IDMoneda<>1 Then D.EXENTO * C.Cotizacion Else D.EXENTO End,
'GRAVADO5%'=Case When C.IDMoneda<>1 Then D.[DISCRIMINADO5%]*C.Cotizacion Else D.[DISCRIMINADO5%] End,
'GRAVADO10%'=Case When C.IDMoneda<>1 Then D.[DISCRIMINADO10%]*C.Cotizacion Else D.[DISCRIMINADO10%] End,
'EXENTO'=Case When C.IDMoneda<>1 Then D.EXENTO * C.Cotizacion Else D.EXENTO End,
'IVA5%'=Case When C.IDMoneda<>1 Then D.[IVA5%] * C.Cotizacion Else D.[IVA5%] End,
'IVA10%'=Case When C.IDMoneda<>1 Then D.[IVA10%] * C.Cotizacion Else D.[IVA10%] End,
'Total'=Case When C.IDMoneda<>1 Then C.Total * C.Cotizacion Else C.Total End,
'Descripcion'='Compras del ' + convert(varchar(50), C.Fecha, 5),
C.IDTransaccion,
C.Timbrado, 
'IDMoneda'=ISnull(C.IDMoneda,1),
'Moneda'=(select Referencia from moneda where id = ISnull(C.IDMoneda,1)),
C.Cotizacion

From VCompra C
Join VDetalleImpuestoDesglosadoTotales2 D on C.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=C.IDSucursal 
Join VTipoComprobante TC On C.IDTipoComprobante=TC.ID And TC.Libro='True'

Union All

--Compra
Select 
'Fecha'=Convert(date, C.Fecha),
'Comprobante'=convert(varchar(50),C.Comprobante),
C.IDTipoComprobante,
C.DescripcionTipoComprobante,
C.[Cod.],
'Sucursal'=S.Descripcion,
C.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Compra',
'Credito'=NULL,
'IDCliente'=0,
C.IDProveedor,
'RazonSocial'=C.Proveedor,
C.RUC,
C.Directo,
'EX'=Case When C.IDMoneda<>1 Then D.EXENTO * C.Cotizacion Else D.EXENTO End,
'GRAVADO5%'=Case When C.IDMoneda<>1 Then D.[DISCRIMINADO5%]*C.Cotizacion Else D.[DISCRIMINADO5%] End,
'GRAVADO10%'=Case When C.IDMoneda<>1 Then D.[DISCRIMINADO10%]*C.Cotizacion Else D.[DISCRIMINADO10%] End,
'EXENTO'=Case When C.IDMoneda<>1 Then D.EXENTO * C.Cotizacion Else D.EXENTO End,
'IVA5%'=Case When C.IDMoneda<>1 Then D.[IVA5%] * C.Cotizacion Else D.[IVA5%] End,
'IVA10%'=Case When C.IDMoneda<>1 Then D.[IVA10%] * C.Cotizacion Else D.[IVA10%] End,
'Total'=Case When C.IDMoneda<>1 Then C.Total * C.Cotizacion Else C.Total End,
'Descripcion'='Compras del ' + convert(varchar(50), C.Fecha, 5),
C.IDTransaccion,
C.NroTimbrado,
'IDMoneda'=ISnull(C.IDMoneda,1),
'Moneda'=(select Referencia from moneda where id = ISnull(C.IDMoneda,1)),
C.Cotizacion

From VGasto C
Join VDetalleImpuestoDesglosadoTotales2 D on C.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=C.IDSucursal 
Join VTipoComprobante TC On C.IDTipoComprobante=TC.ID And TC.Libro='True'
Where C.IncluirLibro='True'

Union All

--Comprobantes de Libro Compra y Venta Sin las NC de Cliente y PRoveedor
Select 
'Fecha'=Convert(date, C.Fecha),
'Comprobante'=convert(varchar(50),C.NroComprobante),
C.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'Cod.'=TC.Codigo,
'Sucursal'=S.Descripcion,
C.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'=(Case When (TC.Tipo) = 'PROVEEDOR' Then 'Compra' Else 'Venta' End),
'Credito'=NULL,
C.IDCliente,
C.IDProveedor,
'RazonSocial'=C.RazonSocial,
C.RUC,
C.Directo,
'EX'=round(D.EXENTO * C.Cotizacion,0),
'GRAVADO5%'=round((Case When (TC.Signo) = '+' Then D.[DISCRIMINADO5%] Else (D.[DISCRIMINADO5%])*-1 End) * C.Cotizacion,0),
'GRAVADO10%'=round((Case When (TC.Signo) = '+' Then D.[DISCRIMINADO10%] Else (D.[DISCRIMINADO10%])*-1 End) * C.Cotizacion,0),
'EXENTO'=round(D.EXENTO * C.Cotizacion,0),
'IVA5%'=round(((Case When (TC.Signo) = '+' Then D.[IVA5%] Else (D.[IVA5%])*-1 End)) * C.Cotizacion,0),
'IVA10%'=round(((Case When (TC.Signo) = '+' Then D.[IVA10%] Else (D.[IVA10%])*-1 End)) * C.Cotizacion,0),
'Total'=round(((Case When (TC.Signo) = '+' Then C.Total Else (C.Total)*-1 End)) * C.Cotizacion,0),
'Descripcion'=(Case When (TC.Tipo) = 'PROVEEDOR' Then 'Compras' Else 'Ventas' End)+ ' del ' + convert(varchar(50), C.Fecha, 5),
C.IDTransaccion,
'Timbrado'='',
'IDMoneda'=ISnull(C.IDMoneda,1),
'Moneda'=(select Referencia from moneda where id = ISnull(C.IDMoneda,1)),
C.Cotizacion

From VComprobanteLibroIVA C
Join VTipoComprobante TC On C.IDTipoComprobante=TC.ID and TC.ID not in (30,12) -- que no sea NC o NCP
Join VDetalleImpuestoDesglosadoTotales2 D on C.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=C.IDSucursal 
WHERE TC.LibroCompra='True' Or TC.LibroVenta='True'

union all

--Comprobantes de Libro Compra y Venta
Select 
'Fecha'=Convert(date, C.Fecha),
'Comprobante'=convert(varchar(50),C.NroComprobante),
C.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'Cod.'=TC.Codigo,
'Sucursal'=S.Descripcion,
C.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Nota Credito Emitido',
'Credito'=NULL,
C.IDCliente,
C.IDProveedor,
'RazonSocial'=C.RazonSocial,
C.RUC,
C.Directo,
'EX'=round(((D.EXENTO * C.Cotizacion) * (-1)),0),
'GRAVADO5%'=round(((D.[DISCRIMINADO5%] * C.Cotizacion) * (-1)),0),
'GRAVADO10%'=round(((D.[DISCRIMINADO10%] * C.Cotizacion) * (-1)),0),
'EXENTO'=round(((D.EXENTO * C.Cotizacion) * (-1)),0),
'IVA5%'=round(((D.[IVA5%] * C.Cotizacion)*(-1)),0),
'IVA10%'=round(((D.[IVA10%]  * C.Cotizacion)*(-1)),0),
'Total'=round(((C.Total * C.Cotizacion)*(-1)),0),
'Descripcion'=S.Descripcion + ' - ' + convert(varchar(50), C.Fecha, 5) + ' - Punto Exp. ' + Substring(C.NroComprobante,0,4),
C.IDTransaccion,
'Timbrado'='',
'IDMoneda'=ISnull(C.IDMoneda,1),
'Moneda'=(select Referencia from moneda where id = ISnull(C.IDMoneda,1)),
C.Cotizacion

From VComprobanteLibroIVA C
Join VTipoComprobante TC On C.IDTipoComprobante=TC.ID and TC.ID in (30) -- Nota de credito de Cliente
Join VDetalleImpuestoDesglosadoTotales2 D on C.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=C.IDSucursal 
WHERE TC.LibroCompra='True' Or TC.LibroVenta='True'

union all

--Comprobantes de Libro Compra y Venta
Select 
'Fecha'=Convert(date, C.Fecha),
'Comprobante'=convert(varchar(50),C.NroComprobante),
C.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'Cod.'=TC.Codigo,
'Sucursal'=S.Descripcion,
C.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Nota Credito Recibido',
'Credito'=NULL,
C.IDCliente,
C.IDProveedor,
'RazonSocial'=C.RazonSocial,
C.RUC,
C.Directo,
'EX'=round(((D.EXENTO * C.Cotizacion) * (-1)),0),
'GRAVADO5%'=round(((D.[DISCRIMINADO5%] * C.Cotizacion) * (-1)),0),
'GRAVADO10%'=round(((D.[DISCRIMINADO10%] * C.Cotizacion) * (-1)),0),
'EXENTO'=round(((D.EXENTO * C.Cotizacion) * (-1)),0),
'IVA5%'=round(((D.[IVA5%] * C.Cotizacion)*(-1)),0),
'IVA10%'=round(((D.[IVA10%]  * C.Cotizacion)*(-1)),0),
'Total'=round(((C.Total * C.Cotizacion)*(-1)),0),
'Descripcion'='Creditos de Proveedores del ' + convert(varchar(50), C.Fecha, 5),
C.IDTransaccion,
'Timbrado'='',
'IDMoneda'=ISnull(C.IDMoneda,1),
'Moneda'=(select Referencia from moneda where id = ISnull(C.IDMoneda,1)),
C.Cotizacion

From VComprobanteLibroIVA C
Join VTipoComprobante TC On C.IDTipoComprobante=TC.ID and TC.ID in (12) -- Nota de credito de Proveedor
Join VDetalleImpuestoDesglosadoTotales2 D on C.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=C.IDSucursal 
WHERE TC.LibroCompra='True' Or TC.LibroVenta='True'

Union All

--Venta
Select 
'Fecha'=convert(date, V.FechaEmision),
'Comprobante'=convert(varchar(50),V.Comprobante),
V.IDTipoComprobante,
V.DescripcionTipoComprobante,
V.[Cod.],
'Sucursal'=S.Descripcion,
V.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Venta',
'Credito'=V.Credito,
V.IDCliente,
'IDProveedor'=0,
'RazonSocial'=V.Cliente,
V.RUC,
'Directo'=NULL,
'EX'=round(D.EXENTO * V.Cotizacion,0),
'GRAVADO5%'=round(D.[DISCRIMINADO5%] * V.Cotizacion,0),
'GRAVADO10%'=round(D.[DISCRIMINADO10%] * V.Cotizacion,0),
'EXENTO'=round(D.EXENTO * V.Cotizacion,0),
'IVA5%'=round(D.[IVA5%] * V.Cotizacion,0),
'IVA10%'=round(D.[IVA10%] * V.Cotizacion,0),
'Total'=round(V.Total * V.Cotizacion,0),
'Descripcion'= V.Sucursal + ' - ' + convert(varchar(50), v.FechaEmision, 5) + ' - Punto Exp. ' + V.ReferenciaPunto,
V.IDTransaccion,
V.Timbrado,
'IDMoneda'=ISnull(V.IDMoneda,1),
'Moneda'=(select Referencia from moneda where id = ISnull(V.IDMoneda,1)),
V.Cotizacion

From VVenta V
Join VDetalleImpuestoDesglosadoTotales2 D on v.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=V.IDSucursal 
Join VTipoComprobante TC On V.IDTipoComprobante=TC.ID And TC.Libro='True'

Where V.Anulado='False'

Union All

--Venta Anulada
Select 
'Fecha'=Convert(date, V.FechaEmision),
'Comprobante'=convert(varchar(50),V.Comprobante),
V.IDTipoComprobante,
V.DescripcionTipoComprobante,
V.[Cod.],
'Sucursal'=S.Descripcion,
V.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Venta',
'Credito'=V.Credito,
V.IDCliente,
'IDProveedor'=0,
'RazonSocial'='-----Comprobante Anulado-----',
'RUC'='',
'Directo'=NULL,
'EX'=0,
'GRAVADO5%'= 0,
'GRAVADO10%'=0,
'EXENTO'=0,
'IVA5%'=0,
'IVA10%'=0,
'Total'=0,
'Descripcion'= V.Sucursal + ' - ' + convert(varchar(50), v.FechaEmision, 5) + ' - Punto Exp. ' + V.ReferenciaPunto,
V.IDTransaccion,
V.Timbrado,
'IDMoneda'=ISnull(V.IDMoneda,1),
'Moneda'=(select Referencia from moneda where id = ISnull(V.IDMoneda,1)),
V.Cotizacion

From VVenta V
Join VSucursal S on S.ID=V.IDSucursal 
Join VTipoComprobante TC On V.IDTipoComprobante=TC.ID And TC.Libro='True'

Where V.Anulado='True'

Union All

--Nota Crédito Cliente
Select 
'Fecha'=Convert(date, NC.Fecha),
'Comprobante'=convert(varchar(50),NC.Comprobante),
NC.IDTipoComprobante,
NC.DescripcionTipoComprobante,
NC.[Cod.],
'Sucursal'=S.Descripcion,
NC.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Nota Credito Emitido',
'Credito'=NULL,
NC.IDCliente,
'IDProveedor'=0,
'RazonSocial'=NC.Cliente,
NC.RUC,
'Directo'=NULL,
'EX'=round((D.EXENTO*-1) * NC.Cotizacion,0),
'GRAVADO5%'=round((D.[DISCRIMINADO5%]*-1) * NC.Cotizacion,0),
'GRAVADO10%'=round((D.[DISCRIMINADO10%]*-1) * NC.Cotizacion,0),
'EXENTO'=round((D.EXENTO*-1) * NC.Cotizacion,0),
'IVA5%'=round((D.[IVA5%]*-1) * NC.Cotizacion,0),
'IVA10%'=round((D.[IVA10%]*-1) * NC.Cotizacion,0),
'Total'=round((NC.Total*-1) * NC.Cotizacion,0),
'Descripcion'= NC.Sucursal + ' - ' + convert(varchar(50), NC.Fecha, 5) + ' - Punto Exp. ' + NC.ReferenciaPunto,
NC.IDTransaccion,
'Timbrado'='',
'IDMoneda'=ISnull(NC.IDMoneda,1),
'Moneda'=(select Referencia from moneda where id = ISnull(NC.IDMoneda,1)),
NC.Cotizacion

From VNotaCredito NC
Join VDetalleImpuestoDesglosadoTotales2 D on nC.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=NC.IDSucursal 
Join VTipoComprobante TC On NC.IDTipoComprobante=TC.ID And TC.Libro='True'
Where Anulado = 'False'

Union All

--Nota Crédito Cliente Anulado
Select 
'Fecha'=Convert(date, NC.Fecha),
'Comprobante'=convert(varchar(50),NC.Comprobante),
NC.IDTipoComprobante,
NC.DescripcionTipoComprobante,
NC.[Cod.],
'Sucursal'=S.Descripcion,
NC.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Nota Credito Emitido',
'Credito'=NULL,
NC.IDCliente,
'IDProveedor'=0,
'RazonSocial'='-----Comprobante Anulado-----',
'RUC'='',
'Directo'=NULL,
'EX'=0,
'GRAVADO5%'= 0,
'GRAVADO10%'=0,
'EXENTO'=0,
'IVA5%'=0,
'IVA10%'=0,
'Total'=0,
'Descripcion'= NC.Sucursal + ' - ' + convert(varchar(50), NC.Fecha, 5) + ' - Punto Exp. ' + NC.ReferenciaPunto,
NC.IDTransaccion,
'Timbrado'='',
'IDMoneda'=ISnull(NC.IDMoneda,1),
'Moneda'=(select Referencia from moneda where id = ISnull(NC.IDMoneda,1)),
nC.Cotizacion

From VNotaCredito NC
Join VDetalleImpuestoDesglosadoTotales2 D on nC.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=NC.IDSucursal 
Join VTipoComprobante TC On NC.IDTipoComprobante=TC.ID And TC.Libro='True'
Where Anulado = 'True'

Union all

--Nota Débito Cliente
Select 
'Fecha'=Convert(date, ND.Fecha),
'Comprobante'=convert(varchar(50),ND.Comprobante),
ND.IDTipoComprobante,
ND.DescripcionTipoComprobante,
ND.[Cod.],
'Sucursal'=S.Descripcion,
ND.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Nota Debito Emitido',
'Credito'=NULL,
ND.IDCliente,
'IDProveedor'=0,
'RazonSocial'=ND.Cliente,
ND.RUC,
'Directo'=NULL,
'EX'=round(D.EXENTO * ND.Cotizacion,0),
'GRAVADO5%'=round(D.[DISCRIMINADO5%] * ND.Cotizacion,0),
'GRAVADO10%'=round(D.[DISCRIMINADO10%] * ND.Cotizacion,0),
'EXENTO'=round(D.EXENTO * ND.Cotizacion,0),
'IVA5%'=round(D.[IVA5%] * ND.Cotizacion,0),
'IVA10%'=round(D.[IVA10%] * ND.Cotizacion,0),
'Total'=round(ND.Total * ND.Cotizacion,0),
'Descripcion'='Debitos a Clientes del ' + convert(varchar(50), ND.Fecha, 5),
ND.IDTransaccion,
'Timbrado'='',
'IDMoneda'=ISnull(ND.IDMoneda,1),
'Moneda'=(select Referencia from moneda where id = ISnull(ND.IDMoneda,1)),
ND.Cotizacion

From VNotaDebito ND
Join VDetalleImpuestoDesglosadoTotales2 D on ND.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=ND.IDSucursal 
Join VTipoComprobante TC On ND.IDTipoComprobante=TC.ID And TC.Libro='True'

Union All

--Nota Crédito Proveedor
Select 
'Fecha'=Convert(date, NC.Fecha),
'Comprobante'=convert(varchar(50),NC.Comprobante),
NC.IDTipoComprobante,
NC.DescripcionTipoComprobante,
NC.[Cod.],
'Sucursal'=S.Descripcion,
NC.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Nota Credito Recibido',
'Credito'=NULL,
'IDCliente'=0,
NC.IDProveedor,
'RazonSocial'=NC.Proveedor,
NC.RUC,
'Directo'=NULL,
'EX'=round((D.EXENTO * -1 * NC.Cotizacion),0) ,
'GRAVADO5%'=round((D.[DISCRIMINADO5%] *-1 * NC.Cotizacion),0) ,
'GRAVADO10%'=round((D.[DISCRIMINADO10%] *-1 * NC.Cotizacion),0) ,
'EXENTO'=round((D.EXENTO * -1 * NC.Cotizacion),0),
'IVA5%'=round((D.[IVA5%] * -1 *  NC.Cotizacion),0),
'IVA10%'=round((D.[IVA10%] * -1 * NC.Cotizacion),0),
'Total'=round((NC.Total * -1 * NC.Cotizacion),0),
'Descripcion'='Creditos de Proveedores del ' + convert(varchar(50), NC.Fecha, 5),
NC.IDTransaccion,
'Timbrado'='',
'IDMoneda'=ISnull(NC.IDMoneda,1),
'Moneda'=(select Referencia from moneda where id = ISnull(NC.IDMoneda,1)),
NC.Cotizacion
From VNotaCreditoProveedor NC
Join VDetalleImpuestoDesglosadoTotales2 D on NC.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=NC.IDSucursal 
Join VTipoComprobante TC On NC.IDTipoComprobante=TC.ID And TC.Libro='True'


Union All

--Nota Débito Proveedor
Select 
'Fecha'=Convert(date, ND.Fecha),
'Comprobante'=convert(varchar(50),ND.Comprobante),
ND.IDTipoComprobante,
ND.DescripcionTipoComprobante,
ND.[Cod.],
'Sucursal'=S.Descripcion,
ND.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Nota Debito Recibido',
'Credito'=NULL,
'IDCliente'=0,
ND.IDProveedor,
'RazonSocial'=ND.Proveedor,
ND.RUC,
'Directo'=NULL,
'EX'=round(D.EXENTO *  ND.Cotizacion,0),
'GRAVADO5%'=round(D.[DISCRIMINADO5%] * ND.Cotizacion,0),
'GRAVADO10%'=round(D.[DISCRIMINADO10%] * ND.Cotizacion,0),
'EXENTO'=round(D.EXENTO * ND.Cotizacion,0),
'IVA5%'=round(D.[IVA5%] * ND.Cotizacion,0),
'IVA10%'=round(D.[IVA10%] *  ND.Cotizacion,0),
'Total'=round(ND.Total * ND.Cotizacion,0),
'Descripcion'='Debitos de Proveedores del ' + convert(varchar(50), ND.Fecha, 5),
ND.IDTransaccion,
'Timbrado'='',
'IDMoneda'=ISnull(ND.IDMoneda,1),
'Moneda'=(select Referencia from moneda where id = ISnull(ND.IDMoneda,1)),
ND.Cotizacion

From VNotaDebitoProveedor ND
Join VDetalleImpuestoDesglosadoTotales2 D on ND.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=ND.IDSucursal 
Join VTipoComprobante TC On ND.IDTipoComprobante=TC.ID And TC.Libro='True'

Union All

--Libro Retencion
Select 
'Fecha'=Convert(date, ND.Fecha),
'Comprobante'=convert(varchar(50),ND.Comprobante),
ND.IDTipoComprobante,
ND.DescripcionTipoComprobante,
ND.[Cod.],
'Sucursal'=S.Descripcion,
ND.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Retencion',
'Credito'=NULL,
'IDCliente'=0,
ND.IDProveedor,
'RazonSocial'=ND.Proveedor,
ND.RUC,
'Directo'=NULL,
'EX'=0,
'GRAVADO5%'= round((Case When (ND.IDMoneda) = 1 Then Sum(DR.Gravado5)  - Sum(DR.IVA5) Else ND.Cotizacion * (Sum(DR.Gravado5)  - Sum(DR.IVA5)) End) * ND.Cotizacion,0),
'GRAVADO10%'= round((Case When (ND.IDMoneda) = 1 Then Sum(DR.Gravado10)  - Sum(DR.IVA10) Else ND.Cotizacion * (Sum(DR.Gravado10)  - Sum(DR.IVA10)) End) * ND.Cotizacion,0),
'EXENTO'=0,
Case When (ND.IDMoneda) = 1 Then Sum(DR.IVA5) Else ND.Cotizacion * Sum(DR.IVA5) End,
Case When (ND.IDMoneda) = 1 Then Sum(DR.IVA10) Else ND.Cotizacion * Sum(DR.IVA10) End,
'Total'= Case When (ND.IDMoneda) = 1 Then Sum(DR.RetencionIVA) Else ND.Cotizacion * Sum(DR.RetencionIVA) End,
'Descripcion'='Retenciones del ' + convert(varchar(50), ND.Fecha, 5),
ND.IDTransaccion,
ND.Timbrado,
'IDMoneda'=ISnull(ND.IDMoneda,1),
'Moneda'=(select Referencia from moneda where id = ISnull(ND.IDMoneda,1)),
ND.Cotizacion
From VRetencionIVA  ND
Join VComprobanteRetencion DR On DR.IDTransaccion = ND.IDTransaccion 
Join VSucursal S on S.ID=ND.IDSucursal 
Join VTipoComprobante TC On ND.IDTipoComprobante=TC.ID And TC.Libro='True'
Where Anulado = 'False'
Group By ND.IDTransaccion, ND.Fecha, ND.Comprobante, ND.IDTipoComprobante, ND.DescripcionTipoComprobante, ND.[Cod.],
S.Descripcion, ND.IDSucursal, S.IDCiudad, S.Ciudad, ND.IDProveedor, ND.Proveedor, ND.RUC, ND.Timbrado, ND.IDMoneda, ND.Cotizacion

Union All

--Libro Retencion Anulado
Select 
'Fecha'=Convert(date, ND.Fecha),
'Comprobante'=convert(varchar(50),ND.Comprobante),
ND.IDTipoComprobante,
ND.DescripcionTipoComprobante,
ND.[Cod.],
'Sucursal'=S.Descripcion,
ND.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Retencion',
'Credito'=NULL,
'IDCliente'=0,
ND.IDProveedor,
'RazonSocial'='-----Comprobante Anulado-----',
ND.RUC,
'Directo'=NULL,
'EX'=0,
'GRAVADO5%'=0,
'GRAVADO10%'=0,
'EXENTO'=0,
0 ,
0,
'Total'=0,
'Descripcion'='Retenciones del ' + convert(varchar(50), ND.Fecha, 5),
ND.IDTransaccion,
ND.Timbrado,
'IDMoneda'=ISnull(ND.IDMoneda,1),
'Moneda'=(select Referencia from moneda where id = ISnull(ND.IDMoneda,1)),
ND.Cotizacion

From VRetencionIVA  ND
Join VSucursal S on S.ID=ND.IDSucursal 
Join VTipoComprobante TC On ND.IDTipoComprobante=TC.ID And TC.Libro='True'
Where Anulado = 'True'
Group By ND.IDTransaccion, ND.Fecha, ND.Comprobante, ND.IDTipoComprobante, ND.DescripcionTipoComprobante, ND.[Cod.],
S.Descripcion, ND.IDSucursal, S.IDCiudad, S.Ciudad, ND.IDProveedor, ND.Proveedor, ND.RUC, ND.Timbrado, ND.IDMoneda,ND.Cotizacion














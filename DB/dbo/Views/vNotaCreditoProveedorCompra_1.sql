﻿create view vNotaCreditoProveedorCompra 
as 
select 
ncpc.*,
ncp.Numero,
e.Comprobante
from 
NotaCreditoProveedorCompra ncpc
join NotaCreditoProveedor ncp on ncpc.IDTransaccionNotaCreditoProveedor = ncp.IDTransaccion
join VEgreso e on ncpc.IDTransaccionEgreso = e.IDTransaccion
﻿
CREATE view [dbo].[VVentasSinLotes]

As
Select
V.IDTransaccion,

V.IDTipoComprobante,
'Comprobante'=TC.Codigo + '-'+ Convert (varchar(50) ,V.NroComprobante),

--Punto de Expedicion
PE.Ciudad,
PE.[CiudadDesc.],
PE.IDCiudad,
PE.IDSucursal,
PE.Sucursal,

--Cliente
V.IDCliente,
'Cliente'=C.RazonSocial,
'ReferenciaCliente'=C.Referencia,
C.RUC,
V.IDSucursalCliente,
'SucursalCliente'=(Case When (V.IDSucursalCliente) IS Null Then 'MATRIZ' Else (Case When (V.IDSucursalCliente) = 0 Then 'MATRIZ' Else (Select Top(1) CS.Sucursal From ClienteSucursal CS Where V.IDCliente=CS.IDCliente And CS.ID=V.IDSucursalCliente) End) End),
'IDCiudadCliente'=C.IDCiudad,
V.Direccion,
'Telefono'=(Case When (C.Telefono) = '' Then 'X' Else C.Telefono End),
C.IDZonaVenta,
C.IDTipoCliente,
'TipoCliente'=IsNull(TP.Descripcion, '---'),
C.Referencia,
C.IDCobrador ,
C.Cobrador ,
C.IDEstado,
C.Estado ,

--Opciones
V.IDDeposito,
'Deposito'=D.Descripcion,
V.FechaEmision,
'Fec'=CONVERT(varchar(50), V.FechaEmision, 3),
'Fecha'=V.FechaEmision,
V.Credito,
'CreditoContado'=(Case When (V.Credito) = 'True' Then 'CREDITO' Else 'CONTADO' End),
'Condicion'=(Case When (V.Credito) = 'True' Then 'CRED' Else 'CONT' End),

V.FechaVencimiento,
'Fec. Venc.'= (Case When V.Credito='True' Then (CONVERT(varchar(50), V.FechaVencimiento, 3)) Else '---' End),
'Fec.UltimoPag'= Isnull ((Select convert (varchar(50), Max(C.FechaCobranza) ,1 ) From VVentaDetalleCobranza C Where C.IDTransaccion=V.IDTransaccion),'---'), 
V.Observacion,
V.IDMoneda,
'Moneda'=M.Referencia,
'DescripcionMoneda'=M.Descripcion,
V.Cotizacion,
V.Anulado,

--Otros

V.IDVendedor,
'Vendedor'=ISNULL((Select VE.Nombres From Vendedor VE Where VE.ID=V.IDVendedor), '---'),


--Totales
V.Total,
V.TotalImpuesto,
V.TotalDiscriminado,
V.TotalDescuento,
'TotalBruto'=isnull(V.Total,0) + isnull(V.TotalDescuento,0),

--Credito
V.Saldo,
V.Cobrado,
V.Descontado,
V.Cancelado,
V.Acreditado,

--Mes y Año
'DiaP'=dbo.FFormatoDosDigitos(DatePart(dd,V.FechaEmision )),
'MesP'=dbo.FMes(DatePart(MM,V.FechaEmision )),
'Año'= Convert (Varchar (50),YEAR(V.FechaEmision)),
'Fec.Vencimiento'= Convert (Varchar (50),YEAR (V.FechaVencimiento))+Convert (Varchar(50),dbo.FFormatoDosDigitos (DATEPART (MM,V.FechaVencimiento)))+Convert (Varchar (50), dbo.FFormatoDosDigitos (DATEPART (DD,V.FechaVencimiento )))

From Venta V
Join Transaccion T On V.IDTransaccion=T.ID
Left Outer Join VPuntoExpedicion PE On V.IDPuntoExpedicion=PE.ID
Left Outer Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Left Outer Join VCliente C On V.IDCliente=C.ID
Left Outer Join TipoCliente TP on TP.ID=C.IDTipoCliente
Left Outer Join Sucursal S On V.IDSucursal=S.ID
Left Outer Join Deposito D On V.IDDeposito=D.ID
Left Outer Join Moneda M On V.IDMoneda=M.ID

Where V.Procesado='True' And (Select Top(1) VL.IDTransaccionVenta From VentaLoteDistribucion VL Where VL.IDTransaccionVenta = V.IDTransaccion) Is Null
And V.Anulado = 'False'


﻿CREATE View [dbo].[VProductoListaPrecioExcepciones]
As
Select 
PLPE.IDProducto,
'ReferenciaProducto'=P.Referencia,
'Producto'=P.Descripcion,
P.IDTipoProducto,
PLPE.IDListaPrecio,
PLPE.IDCliente,
PLPE.IDSucursal,
PLPE.IDTipoDescuento,
PLPE.IDMoneda,
PLPE.Descuento,
PLPE.Porcentaje,
PLPE.Desde,
PLPE.Hasta,
'TipoDescuento'=TD.Descripcion,
'TipoDescuentoCodigo'=TD.Codigo,
'FechaDesde'=IsNull(CONVERT(varchar(50), PLPE.Desde, 5), '---'),
'FechaHasta'=IsNull(CONVERT(varchar(50), PLPE.Hasta, 5), '---'),
'Cliente'=C.RazonSocial,
'Moneda'=M.Referencia,
'Sucursal'=S.Descripcion,
'Referencia' = C.Referencia,
'CantidadLimite'=convert(decimal,Isnull(PLPE.CantidadLimite,0.00)),
'CantidadLimiteSaldo'=convert(decimal,isnull(PLPE.CantidadLimiteSaldo,0.00)),
'CantidadLimiteKG' = convert(decimal,ISnull(PLPE.CantidadLimite,0.00) * P.Peso),
'CantidadLimiteSaldoKG' = convert(decimal,Isnull(PLPE.CantidadLimiteSaldo,0.00) * P.Peso),
'ListaPrecio' = LP.Descripcion,
'UnicaVez'=Isnull(PLPE.UnicaVez,0),
'IDTransaccionPedido' = (Case when Isnull(PLPE.UnicaVez,0) = 0 then 0 else isnull(IDTransaccionPedido,0) end)

From ProductoListaPrecioExcepciones PLPE
Join VProducto P On PLPE.IDProducto=P.ID
Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID
Join VCliente C On PLPE.IDCliente = C.ID
Join VMoneda M On PLPE.IDMoneda = M.ID
Join Sucursal S On PLPE.IDSucursal=S.ID









﻿CREATE View [dbo].[VListaPrecio]
As

Select 
LP.ID,
LP.IDSucursal,
'Descripcion' = IsNull(S.Codigo, S.Descripcion) + '-' + LP.Descripcion,
LP.Estado,
LP.Referencia,
'Sucursal'=IsNull(S.Descripcion, ''),
'Lista'=IsNull(S.Codigo, S.Descripcion) + '-' + LP.Descripcion,
'CodigoSucursal'= S.Codigo

From ListaPrecio LP
Left Outer Join Sucursal S On LP.IDSucursal=S.ID


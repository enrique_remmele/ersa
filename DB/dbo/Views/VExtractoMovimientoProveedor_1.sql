﻿CREATE view [dbo].[VExtractoMovimientoProveedor]
as
--Egreso
Select
C.IDTransaccion, 
'Operación'=CodigoOperacion + ' ' + isnull(convert(varchar(50),C.Numero),convert(varchar(50),'--')),
'Codigo'=C.IDProveedor,
'RazonSocial'=P.RazonSocial ,
'Referencia'=P.Referencia ,
'Fecha'=C.Fecha,
'Documento'=C.TipoComprobante + ' ' + convert(varchar(50),C.NroComprobante) + (Case when C.Cotizacion = 1 then '' else concat(' (',cast(C.Cotizacion as integer),')') end),  
'Detalle/Concepto'=Observacion,
'Debito'=0,
'Credito'=C.Total,
'IDMoneda'=C.IDMoneda,
'DescripcionMoneda'=C.Moneda,
'Movimiento'='EG',
C.RRHH
From 
VCompraGasto C 
Join VProveedor P on C.IDProveedor=P.ID 

union all

--Nota Débito Proveedor
Select
IDTransaccion,
'Operación'='Cr ' + isnull(convert(varchar(50),Numero),convert(varchar(50),'--')),
'Codigo'=IDProveedor,
'RazonSocial'=Proveedor,
'Referencia'=Referencia,
'Fecha'=Fecha,
'Documento'=[Cod.] + ' ' + convert(varchar(50),NroComprobante) + (Case when Cotizacion = 1 then '' else concat(' (',cast(Cotizacion as integer),')') end), 
'Detalle/Concepto'='',
'Debito'=0,
'Credito'=Total,
'IDMoneda'='',
'DescripcionMoneda'=DescripcionMoneda,
'Movimiento'='ND',
'RRHH'='False'
From
VNotaDebitoProveedor 

union all

--Nota de Crédito Proveedor
Select
IDTransaccion,
'Operación'='Db ' + isnull(convert(varchar(50),NC.Numero),convert(varchar(50),'--')),
'Codigo'=IDProveedor,
'RazonSocial'=Proveedor,
NC.Referencia,
'Fecha'=Fecha,
'Documento'=[Cod.] + ' ' + convert(varchar(50),NroComprobante) + (Case when NC.Cotizacion = 1 then '' else concat(' (',cast(NC.Cotizacion as integer),')') end), 
'Detalle/Concepto'='',
'Debito'=Total,
'Credito'=0,
NC.IDMoneda,
'DescripcionMoneda'=M.Descripcion,
'Movimiento'='NC',
'RRHH'='False'
From
VNotaCreditoProveedor NC
Join VMoneda M on NC.IDMoneda=M.ID 

union all

--Orden Pago
Select
IDTransaccion,
'Operación'='OP '+isnull(convert(varchar(50),Numero),convert(varchar(50),'--')),
'Codigo'=IDProveedor,
'RazonSocial'=Proveedor,
'Referencia'=Referencia,
'Fecha'=Fecha,
'Documento'=[TipoComprobante] + ' ' + convert(varchar(50),NroComprobante) + + (Case when Cotizacion = 1 then '' else concat(' (',cast(Cotizacion as integer),')') end), 
'Detalle/Concepto'=Observacion,
--'Debito'=Case When (IDMonedaComprobante) = 1 Then Importe Else (Case When (IDMoneda) = 1 Then ImporteMoneda / Cotizacion Else ImporteMoneda End) End,
'Debito'=isnull(Case When (IDMonedaComprobante) = 1 Then Importe Else (Case When (IDMoneda) = 1 Then ImporteMoneda / Cotizacion Else ImporteMoneda End) End,0) + ImporteMonedaDocumento,
--'Debito'=Case When (IDMonedaComprobante) = 1 Then Importe Else ImporteMoneda End,
'Credito'=0,
'IDMoneda'=IDMonedaComprobante,
'DescripcionMoneda'=MonedaComprobante,
'Movimiento'='OP',
'RRHH'='False'
From
VOrdenPago
Where Anulado='False'


Union All

-- Orden Pago Retencion
Select
OP.IDTransaccion,
'Operación'='RET OP '+isnull(convert(varchar(50),OP.Numero),convert(varchar(50),'--')),
'Codigo'=OP.IDProveedor,
'RazonSocial'=OP.Proveedor,
'Referencia'=OP.Referencia,
'Fecha'=OP.Fecha,
'Documento'=OP.[TipoComprobante] + ' ' + convert(varchar(50),OP.NroComprobante) + + (Case when OP.Cotizacion = 1 then '' else concat(' (',cast(OP.Cotizacion as integer),')') end), 
'Detalle/Concepto'=OP.Observacion,
--'Debito'=Case When (IDMonedaComprobante) = 1 Then Importe Else (Case When (IDMoneda) = 1 Then ImporteMoneda / Cotizacion Else ImporteMoneda End) End,
'Debito'=isnull(Case When (OP.IDMonedaComprobante) = 1 Then Sum(OPE.Retencion) Else (Case When (OP.IDMoneda) = 1 Then Sum(OPE.Retencion) / OP.Cotizacion Else Sum(OPE.Retencion) End) End,0),
--'Debito'=Case When (IDMonedaComprobante) = 1 Then Importe Else ImporteMoneda End,
'Credito'=0,
'IDMoneda'=OP.IDMonedaComprobante,
'DescripcionMoneda'=OP.MonedaComprobante,
'Movimiento'='RET OP',
'RRHH'='False'
From 
VOrdenPago OP
join OrdenPagoEgreso OPE on OP.IDTransaccion = OPE.IDTransaccionOrdenPago
Where Anulado='False' and OP.AplicarRetencion = 'True'
group by OP.IDTransaccion, OP.Numero, OP.IDProveedor, OP.Proveedor, OP.Referencia, OP.Fecha, 
OP.[TipoComprobante], OP.NroComprobante, OP.Cotizacion, OP.Observacion, OP.IDMonedaComprobante, 
OP.Importe, OP.ImporteMoneda, OP.ImporteMonedaDocumento, OP.MonedaComprobante, OP.IDMoneda


Union All

--Orden Pago Egreso
Select
OPE.IDTransaccion,
'Operación'='OPE ' + isnull(convert(varchar(50),OP.Numero),convert(varchar(50),'--')),
'Codigo'=OPE.IDProveedor,
'RazonSocial'=OPE.Proveedor,
'Referencia'='',
'Fecha'=OP.Fecha,
'Documento'=[TipoComprobante] + ' ' + convert(varchar(50),OPE.NroComprobante) + (Case when OP.Cotizacion = 1 then '' else concat(' (',cast(OP.Cotizacion as integer),')') end), 
'Detalle/Concepto'=OP.Observacion,
'Debito'=OPE.Importe,
'Credito'=0,
'IDMoneda'=M.ID,
'DescripcionMoneda'=M.Referencia,
'Movimiento'='OPE',
'RRHH'='False'
From
VOrdenPagoEgreso OPE
join OrdenPago OP on OPE.IDTransaccionOrdenPago=OP.IDTransaccion
Join Moneda M On OP.IDMoneda=M.ID
Where OP.IDProveedor Is Null And OP.Anulado='False'

--Union All

----Retenciones / Con Ordenes de Pago
--Select
--R.IDTransaccion,
--'Operación'='RET ' + isnull(convert(varchar(50),R.Numero),convert(varchar(50),'--')),
--'Codigo'=R.IDProveedor,
--'RazonSocial'=R.Proveedor,
--'Referencia'='',
--'Fecha'=OP.Fecha,
--'Documento'=R.TipoComprobante + ' ' + convert(varchar(50),R.NroComprobante) + (Case when R.Cotizacion = 1 then '' else concat(' (',cast(R.Cotizacion as integer),')') end), 
--'Detalle/Concepto'=R.Observacion,
--'Debito'=Round((R.TotalIVA + R.TotalRenta),2),
--'Credito'=0,
--'IDMoneda'=R.IDMoneda,
--'DescripcionMoneda'='',
--'Movimiento'='RET',
--'RRHH'='False'
--From
--VRetencionIVA R
--join OrdenPago OP on R.IDTransaccionOrdenPago=OP.IDTransaccion
--Where R.Anulado='False' And OP.Anulado='False'

--Union All

----Retenciones / Sin Ordenes de Pago
--Select
--R.IDTransaccion,
--'Operación'='RET ' + isnull(convert(varchar(50),R.Numero),convert(varchar(50),'--')),
--'Codigo'=R.IDProveedor,
--'RazonSocial'=R.Proveedor,
--'Referencia'='',
--'Fecha'=R.Fecha,
--'Documento'=R.TipoComprobante + ' ' + convert(varchar(50),R.NroComprobante) + (Case when R.Cotizacion = 1 then '' else concat(' (',cast(R.Cotizacion as integer),')') end), 
--'Detalle/Concepto'=R.Observacion,
--'Debito'=Round((R.TotalIVA + R.TotalRenta),2),
--'Credito'=0,
--'IDMoneda'=R.IDMoneda,
--'DescripcionMoneda'=R.Moneda,
--'Movimiento'='RET',
--'RRHH'='False'
--From
--VRetencionIVA R
--Where R.Anulado='False' And IsNull(R.IDTransaccionOrdenPago, 0) = 0

Union All

--Compras Importadas con cobros parciales en sistema anterior
Select
C.IDTransaccion, 
'Operación'='Migracion',
'Codigo'=C.IDProveedor,
'RazonSocial'=C.Proveedor,
'Referencia'=C.Referencia ,
'Fecha'=Convert(date, '20151231'),
'Documento'=C.TipoComprobante + ' ' + convert(varchar(50),C.NroComprobante) + (Case when C.Cotizacion = 1 then '' else concat(' (',cast(C.Cotizacion as integer),')') end), 
'Detalle/Concepto'='Pagos parciales en sistema anterior',
'Debito'=GI.Pagado,
'Credito'=0,
'IDMoneda'=C.IDMoneda,
'DescripcionMoneda'=C.Moneda,
'Movimiento'='PAG. MIGRACION',
C.RRHH
From 
VGasto C
Join GastoImportado GI On C.IDTransaccion=GI.IDTransaccion 
Where IsNull(GI.Pagado,0)>0







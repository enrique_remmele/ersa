﻿CREATE View [dbo].[VClienteFechaFestiva]
As
Select 
'IDCliente'=C.ID,
'Cliente' = C.RazonSocial,
'Nombre' = C.RazonSocial,
'Cargo' = ' ',
'ReferenciaCliente' = C.Referencia,
C.IDTipoCLiente,
'TipoCliente'=(select descripcion from TipoCliente where id = C.IDTipoCliente),
C.IDSucursal,
'Sucursal'=(Select descripcion from Sucursal where id = C.IDSucursal),
C.IDVendedor,
'Vendedor'=(Select Nombres from Vendedor where id = C.IDVendedor),
C.Telefono,
C.Email,
'FechaFestiva' = C.Aniversario,
'Cumpleaños/Aniversario'= dbo.FFormatoFecha(C.Aniversario)

From Cliente C 
where C.Aniversario is not null

Union all

Select 
CC.IDCliente,
'Cliente' = C.RazonSocial,
CC.Nombres,
CC.Cargo,
'ReferenciaCliente' = C.Referencia,
C.IDTipoCLiente,
'TipoCliente'=(select descripcion from TipoCliente where id = C.IDTipoCliente),
C.IDSucursal,
'Sucursal'=(Select descripcion from Sucursal where id = C.IDSucursal),
C.IDVendedor,
'Vendedor'=(Select Nombres from Vendedor where id = C.IDVendedor),
CC.Telefono,
CC.Email,
'FechaFestiva' = CC.Cumpleaños,
'Cumpleaños'= dbo.FFormatoFecha(CC.Cumpleaños)

From ClienteContacto CC
join Cliente C on C.ID = CC.IDCliente
where cc.Cumpleaños is not null



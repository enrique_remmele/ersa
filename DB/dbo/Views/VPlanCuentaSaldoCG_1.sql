﻿



CREATE View [dbo].[VPlanCuentaSaldoCG]
as
Select 
PCS.Año,
PCS.Mes,
'M'=dbo.FMes(PCS.Mes),
'Codigo'=PCS.Cuenta,
'Denominacion'=IsNull((Select Descripcion From VCuentaContable CC Where CC.Codigo=PCS.Cuenta And CC.PlanCuentaTitular='True'),'---'),

--Totales
'Debito'=PCS.Debito,
'Credito'=PCS.Credito,
'Saldo'=PCS.Saldo,

--Centro de Costo
PCS.IDCentroCosto,
'CentroCosto'=IsNull(CC.Descripcion, '---'),
'CodigoCentroCosto'=IsNull(CC.Codigo, '---')

From PlanCuentaSaldoCG PCS
Left outer Join CentroCosto CC On PCS.IDCentroCosto=CC.ID
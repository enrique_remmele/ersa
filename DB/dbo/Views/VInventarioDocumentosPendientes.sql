﻿CREATE view [dbo].[VInventarioDocumentosPendientes]
 as 

-- Daniel 21/01/2015 se agrega Idtransaccion para realacion con vDetlleVenta y poder filtrar por IdTipoproducto
Select
V.IDCliente, 
'Cliente' = C.RazonSocial + ' - ' +  C.Referencia,
C.RazonSocial,
'Direccion'= Substring(V.Direccion,0,20),
V.Referencia ,
V.IDTipoCliente,
V.TipoCliente,
'Judicial'= Isnull(C.Judicial,0),
V.IDSucursalCliente,
V.SucursalCliente,
V.[Lista de Precio],
'TipoProducto'= ISnull((Select Top(1) TipoProducto from vdetalleventa where IDTransaccion = V.IDTransaccion), ''),
V.IDMoneda, 
v.IDZonaVenta, 
V.Telefono,
V.RUC , 
V.Condicion ,
V.IDTipoComprobante,
V.Comprobante ,
V.NroComprobante,
'Vendedor'=ISNULL(V.Vendedor,'-----') ,
'Cobrador'=ISNULL(V.Cobrador,'-----'),
V.IDSucursal,
V.Sucursal,
V.IDCiudad,
V.Ciudad ,
V.IDDeposito, 
V.Fecha,
V.IDEstado,
--V.Estado,
'Estado'= (Case when C.Judicial = 'True' then Concat('JUDICIAL',' - ',(select Descripcion from Abogado where Id = C.IdAbogado)) else V.Estado End ),
V.IDVendedor,
V.IDCobrador, 
'Fecha Emision'=FechaEmision,
'Fecha Vencimiento'= ISNULL(V.[Fec. Venc.], V.Fec),
'Ven.'= CONVERT (varchar (50), FechaVencimiento ,6),
'Plazo'= DATEDIFF (D, FechaEmision, FechaVencimiento ),
'Ultimo Pago'=(select MAX(C.FechaCobranza) from VVentaDetalleCobranza C Where C.IDTransaccion = V.IDTransaccion),
'Dias Vencidos'= DATEDIFF (D,   V.FechaVencimiento, GETDATE() ),
V.Total ,
V.Saldo,
'SaldoGs'=V.Saldo*(ISnull((select Cotizacion from Cotizacion where IDMoneda = V.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1)),
V.Cancelado,
V.Anulado,
'Documento'='Venta',
'IVA 5%'=IsNull((Select Total From DetalleImpuesto DI Where DI.IDTransaccion=V.IDTransaccion And DI.IDImpuesto=2),0),
VLD.Chofer,
VLD.IDChofer,
--V.FechaVencimiento,
'FechaVencimiento'= (Case When(V.Condicion) = 'CONT' Then V.FechaEmision Else V.FechaVencimiento End),
'ChequeDiferido'='False',
'ChequeDescontado'='False',
v.Idtransaccion,
'FormaPagoFactura'=(Select referencia from FormaPagoFactura where ID = V.IDFormaPagoFactura),
ISnull((select Cotizacion from Cotizacion where IDMoneda = V.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1) as Cotizacion,
'Moneda'=V.Moneda,
'CotizacionOrig'=V.Cotizacion,
V.IDSucursalFiltro,--Sucursal del cliente
'IDTipoProducto'=(Select top(1) IDTipoProducto from vdetalleVenta where IDTransaccion = V.IDTransaccion),
'BoletacontraBoleta' = (case when (c.Boleta = 'True') then 'True' else 'False' end)

From VVenta V
Left Outer Join VVentaLoteDistribucion VLD on V.IDTransaccion=VLD.IDTransaccionVenta and VLD.Anulado = 'False'
join cliente C on C.Id = V.Idcliente
Where V.Cancelado = 'False' And V.CancelarAutomatico = 'False'


Union All
 
Select
NC.IDCliente,  
'Cliente' = C.RazonSocial + ' - ' +  C.Referencia,
C.RazonSocial,
NC.Direccion,
NC.Referencia,
NC.IDTipoCliente,
'TipoCliente' = (Select descripcion from tipocliente where id = NC.IDTipoCliente),
'Judicial'= Isnull(C.Judicial,0),
'IDSucursalCliente'=NULL,
'SucursalCliente'=NULL,
'ListaPrecio'=(Select descripcion from listaprecio where id = C.IDListaPrecio),
'TipoProducto'= ISnull((Select Top(1) TipoProducto from vdetalleNotaCredito where IDTransaccion = NC.IDTransaccion), ''),
NC.IDMoneda, 
NC.IDZonaVenta,
NC.Telefono,
NC.RUC,
--NC.Condicion  ,
'Condicion'='CRED',
NC.IDTipoComprobante ,
'Comprobante'=Concat(NC.Comprobante, ' NC'),
NC.NroComprobante,
'Vendedor'= '-----', 
'Cobrador'= '-----',
NC.IDSucursal,
NC.Sucursal,
NC.IDCiudad,
NC.Ciudad,
NC.IDDeposito, 
NC.Fecha,
NC.IDEstado,
--NC.Estado,
'Estado'= (Case when C.Judicial = 'True' then Concat('JUDICIAL',' - ',(select Descripcion from Abogado where Id = C.IdAbogado)) else NC.Estado End ),
'IDVendedor'= '',
'IDCobrador'='', 
'Fecha Emision'= NC.Fecha,
'Fecha Vencimiento'= NC.Fec ,
'Ven.'= NC.Fec,
'Plazo'= '',
'Ultimo Pago'=NULL,
'Dias Vencidos'= '',
NC.Total,
NC.Saldo * -1,  
'SaldoGs'= (NC.Saldo*-1)*(ISnull((select Cotizacion from Cotizacion where IDMoneda = NC.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1)),
'Cancelado'= (Case When(NC.Aplicar) = 'True' Then (Case When(NC.Saldo) = 0 Then 'True' Else 'False' End) Else 'True' End),
NC.Anulado,
'Documento'='Nota de Credito',
'IVA 5%'=IsNull((Select Total From DetalleImpuesto DI Where DI.IDTransaccion=NC.IDTransaccion And DI.IDImpuesto=2),0),
'',
0,
NC.Fecha,
'ChequeDiferido'='False',
'ChequeDescontado'='False',
NC.Idtransaccion,
'FormaPagoFactura'='--',
Isnull((select Cotizacion from Cotizacion where IDMoneda = NC.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1) as Cotizacion,
'Moneda'=NC.Moneda,
'CotizacionOrig'=NC.Cotizacion,
'IDSucursalFiltro'= NC.IDSucursal, --Sucursal del cliente
'IDTipoProducto'=(Select top(1) IDTipoProducto from VDetalleNotaCredito where IDTransaccion = NC.IDTransaccion),
'BoletacontraBoleta' = (case when (c.Boleta = 'True') then 'True' else 'False' end)

From VNotaCredito NC
Join Cliente C on C.ID = NC.IDCliente
Where (Case When(NC.Aplicar) = 'True' 
	   Then (Case When(NC.Saldo) > 0 Then 'True' Else 'False' End) 
	   Else 'False' End) = 'True' 

Union All

Select 
ND.IDCliente,
'Cliente' = C.RazonSocial + ' - ' +  C.Referencia,
C.RazonSocial,
ND.Direccion,
ND.Referencia,
ND.IDTipoCliente,
'TipoCliente'=(Select descripcion from tipocliente where id = ND.IDTipoCliente),
'Judicial'= Isnull(C.Judicial,0),
'IDSucursalCliente'=NULL,
'SucursalCliente'=NULL,
'ListaPrecio'= (select descripcion from listaprecio where id = C.IDListaPrecio),
'TipoProducto'= '',
ND.IDMoneda, 
ND.IDZonaVenta,
ND.Telefono,
ND.RUC,
'Condicion'='CRED',
ND.IDTipoComprobante ,
'Comprobante'=Concat(ND.Comprobante, ' ND'),
ND.NroComprobante,
'Vendedor'= '-----', 
'Cobrador'= '-----',
ND.IDSucursal,
ND.Sucursal,
ND.IDCiudad,
ND.Ciudad,
ND.IDDeposito, 
ND.Fecha,
ND.IDEstado,
--ND.Estado,
'Estado'= (Case when C.Judicial = 'True' then Concat('JUDICIAL',' - ',(select Descripcion from Abogado where Id = C.IdAbogado)) else ND.Estado End ),
'IDVendedor'= '',
'IDCobrador'='', 
'Fecha Emision'=ND.Fecha,
'FechaVencimiento'= ND.Fec,
'Ven.'=  ND.Fec,
'Plazo'= '',
'Ultimo Pago'=NULL,
'Dias Vencidos'= '',
ND.Total,
ND.Saldo,
'SaldoGs'=ND.Saldo*(ISnull((select Cotizacion from Cotizacion where IDMoneda = ND.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1)),
'Cancelado'= (Case When(ND.Aplicar) = 'True' Then (Case When(ND.Saldo) = 0 Then 'True' Else 'False' End) Else 'True' End),
ND.Anulado,
'Documento'='Nota de Debito',
'IVA 5%'=IsNull((Select Total From DetalleImpuesto DI Where DI.IDTransaccion=ND.IDTransaccion And DI.IDImpuesto=2),0), 
'',
0,
ND.Fecha,
'ChequeDiferido'='False',
'ChequeDescontado'='False',
ND.Idtransaccion,
'FormaPagoFactura'='--',
Isnull((select Cotizacion from Cotizacion where IDMoneda = ND.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1) as Cotizacion,
'Moneda'=ND.Moneda,
'CotizacionOrig'=ND.Cotizacion,
'IDSucursalFiltro'= ND.IDSucursal, --Sucursal del cliente
'IDTipoProducto'=0,
'BoletacontraBoleta' = (case when (c.Boleta = 'True') then 'True' else 'False' end)


From VNotaDebito ND 
Join Cliente C on C.ID = ND.IDCliente
Where (Case When(ND.Aplicar) = 'True' 
Then (Case When(ND.Saldo) = 0 Then 'True' Else 'False' End) Else 'False' End) = 'True'  

Union All

--Cheque Diferidos pendientes
Select
V.IDCliente, 
'Cliente' = C.RazonSocial + ' - ' +  C.Referencia,
C.RazonSocial,
C.Direccion,
C.Referencia ,
C.IDTipoCliente,
C.TipoCliente,
'Judicial'= Isnull(C.Judicial,0),
'IDSucursalCliente'=0,
'SucursalCliente'='',
C.ListaPrecio,
'TipoProducto'= '',
V.IDMoneda, 
C.IDZonaVenta, 
C.Telefono,
C.RUC , 
--'Condicion'='DIF - ' + V.CodigoBanco,
'Condicion'='CRED',
'IDTipoComprobante'=0,
'Comprobante'=Concat(V.NroCheque,' DIF - ',V.CodigoBanco),
'NroComprobante'=V.NroCheque,
'Vendedor'='-----',
'Cobrador'='-----',
V.IDSucursal,
V.Sucursal,
S.IDCiudad,
V.Ciudad ,
'IDDeposito'=0, 
V.Fecha,
'IDEstado'=0,
'Estado'= (Case when C.Judicial = 'True' then Concat('JUDICIAL',' - ',(select Descripcion from Abogado where Id = C.IdAbogado)) else C.Estado End ),
'IDVendedor'=0,
'IDCobrador'=0, 
'Fecha Emision'=Fecha,
'Fecha Vencimiento'= ISNULL(V.Vencimiento, V.Fec),
'Ven.'= CONVERT (varchar (50), V.FechaVencimiento ,6),
'Plazo'= DATEDIFF (D, V.Fecha, V.FechaVencimiento ),
'Ultimo Pago'=NULL,
'Dias Vencidos'= DATEDIFF (D,   V.FechaVencimiento, GETDATE() ),
V.Total,
--El saldo del cheque va a ser siempre el total
--Queda sin saldo al depositar unicamente, el saldo de cobranza no quieren saber.
--17/06/2021 se nesecita ver el saldo correcto del cliente
--'Saldo'=V.Total,
'Saldo'=V.SaldoACuenta,
'SaldoGs'=V.SaldoACuenta*(ISnull((select Cotizacion from Cotizacion where IDMoneda = V.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1)),
--V.Saldo,
'Cancelado'='False',
--V.Cancelado,
'Anulado'='False',
'Documento'='Cheque Diferido',
'IVA 5%'=0,
'Chofer'='',
'IDChofer'=0,
V.FechaVencimiento,
'ChequeDiferido'='True',
'ChequeDescontado'=(Case when V.Descontado = 1 then 'True' else 'false' end),
V.Idtransaccion,
'FormaPagoFactura'='--',
Isnull((select Cotizacion from Cotizacion where IDMoneda = V.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1) as Cotizacion,
'Moneda'= V.moneda,
'CotizacionOrig'=V.Cotizacion,
'IDSucursalFiltro'= V.IDSucursal, --Sucursal del cliente
'IDTipoProducto'=0,
'BoletacontraBoleta' = (case when (c.Boleta = 'True') then 'True' else 'False' end)


From VChequeCliente V
Join VCliente C On V.IDCliente=C.ID
Join VSucursal S On V.IDSucursal=S.ID
Where ((V.Cartera='True') or (V.Depositado = 'True' and V.Descontado = 'True')) -- Si esta descontado debe mostrar en inventario

And V.Diferido='True'

UNion all

--Cheque Diferidos pendientes
Select
V.IDCliente, 
'Cliente' = C.RazonSocial + ' - ' +  C.Referencia,
C.RazonSocial,
C.Direccion,
C.Referencia ,
C.IDTipoCliente,
C.TipoCliente,
'Judicial'= Isnull(C.Judicial,0),
'IDSucursalCliente'=0,
'SucursalCliente'='',
C.ListaPrecio,
'TipoProducto'= '',
V.IDMoneda, 
C.IDZonaVenta, 
C.Telefono,
C.RUC , 
'Condicion'='CRED',
'IDTipoComprobante'=0,
--'Comprobante'=V.NroCheque,
'Comprobante'=Concat(V.NroCheque,' RECH - ' + V.CodigoBanco),
'NroComprobante'=V.NroCheque,
'Vendedor'='-----',
'Cobrador'='-----',
V.IDSucursal,
V.Sucursal,
S.IDCiudad,
V.Ciudad ,
'IDDeposito'=0, 
V.Fecha,
'IDEstado'=0,
'Estado'= (Case when C.Judicial = 'True' then Concat('JUDICIAL',' - ',(select Descripcion from Abogado where Id = C.IdAbogado)) else C.Estado End ),
'IDVendedor'=0,
'IDCobrador'=0, 
'Fecha Emision'=Fecha,
'Fecha Vencimiento'= ISNULL(V.Vencimiento, V.Fec),
'Ven.'= CONVERT (varchar (50), V.FechaVencimiento ,6),
'Plazo'= DATEDIFF (D, V.Fecha, V.FechaVencimiento ),
'Ultimo Pago'=NULL,
'Dias Vencidos'= DATEDIFF (D,   V.FechaVencimiento, GETDATE() ),
V.Total,
--El saldo del cheque va a ser siempre el total
--Queda sin saldo al depositar unicamente, el saldo de cobranza no quieren saber.
--17/06/2021 se quiere ver el saldo correcto del cliente 
--'Saldo'=V.Total,
'Saldo'=V.SaldoACuenta,
'SaldoGs'=V.SaldoACuenta*(ISnull((select Cotizacion from Cotizacion where IDMoneda = V.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1)),
--V.Saldo,
'Cancelado'='False',
--V.Cancelado,
'Anulado'='False',
'Documento'='Cheque Rechazado',
'IVA 5%'=0,
'Chofer'='',
'IDChofer'=0,
V.FechaVencimiento,
'ChequeDiferido'='True',
'ChequeDescontado'=(Case when V.Descontado = 1 then 'True' else 'false' end),
V.Idtransaccion,
'FormaPagoFactura'='--',
Isnull((select Cotizacion from Cotizacion where IDMoneda = V.IDMoneda and Cast(Fecha as date) = cast(getdate() as date)),1) as Cotizacion,
'Moneda'=V.Moneda,
'CotizacionOrig'=V.Cotizacion,
'IDSucursalFiltro'= V.IDSucursal, --Sucursal del cliente
'IDTipoProducto'=0,
'BoletacontraBoleta' = (case when (c.Boleta = 'True') then 'True' else 'False' end)

From VChequeCliente V
Join VCliente C On V.IDCliente=C.ID
Join VSucursal S On V.IDSucursal=S.ID
Where V.Rechazado='True'
--and V.Cancelado = 'False'
and V.SaldoACuenta > 0
--17/06/2021 Se quiere ver el saldo correcto del cliente 
--and V.IDTransaccion not in (select DPC.IDTransaccionChequeCliente from DetallePagoChequeCliente DPC
--join PagoChequeCliente PCC on PCC.IDtransaccion = DPC.IdTransaccionPagoChequeCliente 
--and PCC.Anulado = 0)

























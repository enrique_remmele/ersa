﻿CREATE View [dbo].[VDetalleAsientoContable]

As

--Ventas
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Venta - ' + '  ' + D.Observacion,
D.Debito,
D.Credito,
'Tipo'='VENTA DETALLADA',
'Observacion'=V.Cliente,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
V.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VVenta V On D.IDTransaccion=V.IDTransaccion

Union All

--Ventas Resumidas
Select
'NumeroAsiento'=0,
D.Codigo,
D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Suc,
A.Moneda,
'Operacion'= ' Ventas del ' + CONVERT(varchar(50),A.Fecha) + ' - ' + A.Sucursal,
'Debito'=SUM(D.Debito),
'Credito'=SUM(D.Credito),
'Tipo'='VENTA RESUMIDA',
'Observacion'='Ventas del dia',
D.Orden,
'Resumido'='True',
'IDTransaccion'=0,
0,
'NroComprobante'='',
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
'Usuario'='',
'IDUsuario'=0,
'FechaTransaccion'='',
A.IDOperacion,
'OperacionAgrupador'=A.Operacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VVenta V On D.IDTransaccion=V.IDTransaccion
Group By A.Fecha, D.Codigo, D.Descripcion, A.Suc, A.IDSucursal, A.Sucursal,A.Moneda, D.Orden, D.ID, D.Observacion, A.IDCentroCosto, A.CentroCosto,A.IDOperacion,A.Operacion

Union All

--Nota de Débito Cliente
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Deb. Cli '+' '+ND.[Cod.] + '  ' +CONVERT(varchar(50),ND.NroComprobante)+'  '+ND.Condicion,
D.Debito,
D.Credito,
'Tipo'='NOTA DEBITO CLIENTE',
'Observacion'=ND.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
ND.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VNotaDebito ND On D.IDTransaccion=ND.IDTransaccion

union all

--Nota de Crédito Cliente
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Cred. Cli '+' '+NC.[Cod.] + '  ' +CONVERT(varchar(50),NC.NroComprobante)+'  '+NC.Condicion,
D.Debito,
D.Credito,
'Tipo'='NOTA CREDITO CLIENTE',
'Observacion'=NC.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
NC.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VNotaCredito NC On D.IDTransaccion=NC.IDTransaccion

union all

--Cobranza Contado Detallado
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cob. por Lote: '+'  '+ Convert(varchar(50), CCO.Numero),
D.Debito,
D.Credito,
'Tipo'='COBRANZA CONTADO DETALLADO',
'Observacion'='Nro de Lote: ' + CCO.ComprobanteLote,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
CCO.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VCobranzaContado CCO On D.IDTransaccion=CCO.IDTransaccion

union all

--Cobranza Crédito Detallado
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cob. Crédito: '+'  '+ Convert(varchar(50), CCR.Numero),
D.Debito,
D.Credito,
'Tipo'='COBRANZA CREDITO DETALLADO',
'Observacion'=CCR.[Cod.] + '  ' +CONVERT(varchar(50),CCR.NroComprobante)+' - '+CCR.Cliente,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
CCR.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VCobranzaCredito CCR On D.IDTransaccion=CCR.IDTransaccion

union all

--Cobranza Contado Resumido
Select
'NumeroAsiento'=0,
D.Codigo,
D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cob. Contado del ' + CONVERT(varchar(50),A.Fecha),
'Debito'=SUM(D.Debito),
'Credito'=SUM(D.Credito),
'Tipo'='COBRANZA CONTADO RESUMIDO',
'Observacion'='Cobranzas del dia',
D.Orden,
'Resumido'='True',
'IDTransaccion'=0,
0,
'NroComprobante'='',
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
'Usuario'='',
'IDUsuario'=0,
'FechaTransaccion'='',
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VCobranzaContado CCO On D.IDTransaccion=CCO.IDTransaccion
Group By A.Fecha, D.Codigo, D.Descripcion,A.IDSucursal,A.Sucursal,A.Moneda, D.Orden, D.ID, D.Observacion, A.IDCentroCosto, A.CentroCosto,A.IDOperacion, A.Operacion

union all

--Cobranza Crédito Resumido
Select
'NumeroAsiento'=0,
D.Codigo,
D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cob. Crédito del ' + CONVERT(varchar(50),A.Fecha),
'Debito'=SUM(D.Debito),
'Credito'=SUM(D.Credito),
'Tipo'='COBRANZA CREDITO RESUMIDO',
'Observacion'='Cobranzas del dia',
D.Orden,
'Resumido'='True',
'IDTransaccion'=0,
0,
'NroComprobante'='',
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
'Usuario'='',
'IDUsuario'=0,
'FechaTransaccion'='',
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VCobranzaCredito CCR On D.IDTransaccion=CCR.IDTransaccion
Group By A.Fecha, D.Codigo, D.Descripcion,A.IDSucursal,A.Sucursal,A.Moneda, D.Orden, D.ID, D.Observacion, A.IDCentroCosto, A.CentroCosto,A.IDOperacion,A.Operacion

union all

--Compra
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Compra: '+'  '+ Convert(varchar(50), C.Numero),
D.Debito,
D.Credito,
'Tipo'='COMPRA',
'Observacion'= C.TipoComprobante + ' ' + CONVERT(varchar(50),C.NroComprobante)+' - '+C.Proveedor,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
C.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VCompra C On D.IDTransaccion=C.IDTransaccion

union all

--Gasto Contado
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= ' Gasto Contado: ' + convert(varchar(50),G.Numero),
D.Debito,
D.Credito,
'Tipo'='GASTO CONTADO',
'Observacion'=G.[Cod.]+' '+CONVERT(varchar(50),G.NroComprobante)+' : '+CONVERT(varchar(50),G.Proveedor),
D.Orden,
'Resumido'='False',
A.IDTransaccion,
G.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VGasto G On D.IDTransaccion=G.IDTransaccion

Union All

--Gasto Credito
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= ' Gasto Credito: ' + convert(varchar(50),G.Numero),
D.Debito,
D.Credito,
'Tipo'='GASTO CREDITO',
'Observacion'=G.[Cod.]+' '+CONVERT(varchar(50),G.NroComprobante)+' : '+CONVERT(varchar(50),G.Proveedor),
D.Orden,
'Resumido'='False',
A.IDTransaccion,
G.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VGasto G On D.IDTransaccion=G.IDTransaccion
Where G.Credito = 'True'

union all

--Nota de Débito Proveedor

Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Deb. Prov: '+'  '+ convert(varchar(50), ND.Numero),
D.Debito,
D.Credito,
'Tipo'='NOTA DEBITO PROVEEDOR',
'Observacion'=ND.[Cod.] + '  ' +CONVERT(varchar(50),ND.NroComprobante)+' - '+ND.Proveedor,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
ND.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VNotaDebitoProveedor ND On D.IDTransaccion=ND.IDTransaccion

union all

--Nota de Crédito Proveedor

Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Cred. Prov '+'  '+ convert(varchar(50),NC.Numero),
D.Debito,
D.Credito,
'Tipo'='NOTA CREDITO PROVEEDOR',
'Observacion'=NC.[Cod.] + '  ' +CONVERT(varchar(50),NC.NroComprobante)+' - '+NC.Proveedor,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
NC.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VNotaCreditoProveedor NC On D.IDTransaccion=NC.IDTransaccion

union all

--Depósito Bancario Detallado
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Dep. Bancario '+'  ' + convert(varchar(50), DB.Numero),
D.Debito,
D.Credito,
'Tipo'='DEPOSITO DETALLADO',
'Observacion'= DB.TipoComprobante + ' ' + convert(varchar(50), DB.Comprobante) + ' - ' +  DB.Banco + ' - Nro Cuenta: ' + DB.Cuenta,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
DB.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VDepositoBancario DB On D.IDTransaccion=DB.IDTransaccion

union all

--Depósito Bancario Resumido
Select
'NumeroAsiento'=0,
D.Codigo,
D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= ' Depósito del  ' + CONVERT(varchar(50),A.Fecha),
'Debito'=SUM(D.Debito),
'Credito'=SUM(D.Credito),
'Tipo'='DEPOSITO RESUMIDO',
'Observacion'='Depositos del Dia',
D.Orden,
'Resumido'='True',
'IDTransaccion'=0,
0,
'NroComprobante'='',
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
'Usuario'='',
'IDUsuario'=0,
'FechaTransaccion'='',
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VDepositoBancario DB On D.IDTransaccion=DB.IDTransaccion
Group By A.Fecha, D.Codigo, D.Descripcion,A.IDSucursal,A.Sucursal,A.Moneda, D.Orden, D.ID, D.Observacion, A.IDCentroCosto, A.CentroCosto,A.IDOperacion,A.Operacion

union all

--Descuento Cheque
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Desc. Cheque '+'  '+ convert(varchar(50), DC.Numero),
D.Debito,
D.Credito,
'Tipo'='DESCUENTO CHEQUE',
'Observacion'= DC.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
DC.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VDescuentoCheque DC On D.IDTransaccion=DC.IDTransaccion

Union All

--Débito Bancario
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Deb. Bancario '+'  '+ convert(varchar(50), DCB.Numero),
D.Debito,
D.Credito,
--EL TIPO ES DEBITO/CREDITO PORQUE EN EL FORM DE ASIENTOS CONTABLES NO DISCRIMINA
--SI SE CAMBIA, CAMBIAR EL FORM TAMBIEN
'Tipo'='DEBITO/CREDITO BANCARIO',
'Observacion'=DCB.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
DCB.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VDebitoCreditoBancario DCB On D.IDTransaccion=DCB.IDTransaccion
Where DCB.Debito = 'True'

Union All

--Crédito Bancario
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cred. Bancario '+'  '+CONVERT(varchar(50),DCB.Numero),
D.Debito,
D.Credito,
--EL TIPO ES DEBITO/CREDITO PORQUE EN EL FORM DE ASIENTOS CONTABLES NO DISCRIMINA
--SI SE CAMBIA, CAMBIAR EL FORM TAMBIEN
'Tipo'='DEBITO/CREDITO BANCARIO',
'Observacion'=DCB.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
DCB.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VDebitoCreditoBancario DCB On D.IDTransaccion=DCB.IDTransaccion
Where DCB.Credito = 'True'

Union All

--Orden de Pago
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Orden Pago '+'  '+CONVERT(varchar(50),OP.Numero),
D.Debito,
D.Credito,
'Tipo'='ORDEN PAGO',
'Observacion'=OP.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
OP.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VOrdenPago OP On D.IDTransaccion=OP.IDTransaccion

union all
--Entrega de cheques
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Entrega de Cheque OP '+'  '+CONVERT(varchar(50),OP.NumeroOP),
D.Debito,
D.Credito,
'Tipo'='ENTREGA DE CHEQUE',
'Observacion'=OP.RetiradoPor,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
'IDTipoComprobante'=(select IDTipoComprobante from OrdenPago where Idtransaccion = OP.IdTransaccionOP),
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VEntregaChequeOP OP On D.IDTransaccion=OP.IDTransaccion

Union All

--Movimiento
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Mov. Stock '+'  '+CONVERT(varchar(50),M.Operacion) + ' ' + convert(varchar(50),M.Numero),
D.Debito,
D.Credito,
'Tipo'='MOVIMIENTO',
'Observacion'=M.Motivo,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
M.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VMovimiento M On D.IDTransaccion=M.IDTransaccion
Join TipoComprobante TC on TC.ID = M.IDTipoComprobante
Join Operacion O on O.ID = TC.IDOperacion
where M.MovimientoStock = 'True'

Union All

--Movimiento
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Desc. Stock '+'  '+CONVERT(varchar(50),M.Operacion) + ' ' + convert(varchar(50),M.Numero),
D.Debito,
D.Credito,
'Tipo'='DESCARGASTOCK',
'Observacion'=M.Motivo,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
M.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VMovimiento M On D.IDTransaccion=M.IDTransaccion
Join TipoComprobante TC on TC.ID = M.IDTipoComprobante
Join Operacion O on O.ID = TC.IDOperacion
where M.DescargaStock = 'True'

UNION ALL

--Movimiento
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cons. Combustible '+'  '+CONVERT(varchar(50),M.Operacion) + ' ' + convert(varchar(50),M.Numero),
D.Debito,
D.Credito,
'Tipo'='COMBUSTIBLE',
'Observacion'=M.Motivo,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
M.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VMovimiento M On D.IDTransaccion=M.IDTransaccion
Join TipoComprobante TC on TC.ID = M.IDTipoComprobante
Join Operacion O on O.ID = TC.IDOperacion
where M.MovimientoCombustible = 'True'

UNION ALL

--Movimiento
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Mov. Mat. Prima Balanceado '+'  '+CONVERT(varchar(50),M.Operacion) + ' ' + convert(varchar(50),M.Numero),
D.Debito,
D.Credito,
'Tipo'='MATERIAPRIMA',
'Observacion'=M.Motivo,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
M.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VMovimiento M On D.IDTransaccion=M.IDTransaccion
Join TipoComprobante TC on TC.ID = M.IDTipoComprobante
Join Operacion O on O.ID = TC.IDOperacion
where M.MovimientoMateriaPrima = 'True'

union all

--Ajuste
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Ajuste ',
D.Debito,
D.Credito,
'Tipo'='AJUSTE',
'Observacion'=AI.Sucursal + ' - ' + AI.Deposito,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
0,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VControlInventario AI On D.IDTransaccion=AI.IDTransaccion

union all
--Movimiento
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Descarga de Compras '+'  '+CONVERT(varchar(50),M.Operacion) + ' ' + convert(varchar(50),M.Numero),
D.Debito,
D.Credito,
'Tipo'='DESCARGACOMPRA',
'Observacion'=M.Motivo,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
M.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VMovimiento M On D.IDTransaccion=M.IDTransaccion
Join TipoComprobante TC on TC.ID = M.IDTipoComprobante
Join Operacion O on O.ID = TC.IDOperacion
where M.DescargaCompra = 'True'

Union All

--Asiento Importado
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= '* ' + V.Operacion,
D.Debito,
D.Credito,
'Tipo'='* ASIENTOS IMPORTADOS',
'Observacion'=Convert(varchar(50), V.Operacion) + ' - Nro:' + Convert(varchar(50), V.Numero),
D.Orden,
'Resumido'='False',
A.IDTransaccion,
0,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VAsientoImportado V On D.IDTransaccion=V.IDTransaccion

Union All

--Asiento con Operaciones sin Formularios de Carga
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= O.Descripcion + ' ' + A.Comprobante,
D.Debito,
D.Credito,
'Tipo'='OTROS',
'Observacion'=A.Detalle,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
0,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Transaccion T On A.IDTransaccion=T.ID
Join Operacion O On T.IDOperacion=O.ID
Where O.FormName Is Null

Union All

--Asiento con Operaciones en Comprobantes para Libro IVA
Select
'NumeroAsiento'=A.Numero,
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= TC.Codigo + ' ' + C.NroComprobante,
D.Debito,
D.Credito,
'Tipo'='OTROS',
'Observacion'=C.Detalle,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
0,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion,
A.IDOperacion,
'OperacionAgrupador'=A.Operacion


From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join ComprobanteLibroIVA C On A.IDTransaccion=C.IDTransaccion
Join Transaccion T On A.IDTransaccion=T.ID
Join Operacion O On T.IDOperacion=O.ID
Join TipoComprobante TC On C.IDTipoComprobante=TC.ID













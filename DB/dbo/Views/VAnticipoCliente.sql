﻿CREATE View [dbo].[VAnticipoCliente]
As

Select 
CC.Cliente,
CC.Banco,
CC.ImporteMoneda,
CC.Moneda,
CC.Fecha,
CC.FechaCobranza,
T.IDUsuario,
CC.FecCobranza
From
VChequeCliente CC
Join Transaccion T on T.ID = CC.IDTransaccion
Where CC.Saldo > 0
and CC.IDTransaccion is not null
and CC.IDTransaccion not in(select idtransaccionCheque from formaPago where idtransaccionCheque is not null) 



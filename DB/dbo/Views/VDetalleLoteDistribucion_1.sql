﻿


CREATE View [dbo].[VDetalleLoteDistribucion]
As
Select

DL.IDTransaccion,

--Producto
DL.IDProducto,
'ID'=DL.IDProducto,
'Producto'=P.Descripcion,
'Descripcion'=P.Descripcion,
'CodigoBarra'=P.CodigoBarra,
'Ref'=P.Referencia,
P.UnidadPorCaja,
P.UnidadMedida,
'Cajas'=CONVERT(int, DL.Cantidad/P.UnidadPorCaja),

'PesoUnitario'= ISNULL(CONVERT(decimal(18, 7), P.Peso) / 1.0, 0 ),
'Peso'= DL.Cantidad * ISNULL(CONVERT(decimal(18, 7), P.Peso) / P.UnidadPorCaja, 0),

--Cantidad y Precio
'Cantidad'=DL.Cantidad,
'Cantidad Unitaria'=DL.Cantidad - (CONVERT(int, DL.Cantidad/P.UnidadPorCaja) * P.UnidadPorCaja),
'Cantidad Total'=DL.Cantidad,
DL.Total,

DL.TotalDescuento,
'TotalDiscriminado'=IsNull(DL.TotalDiscriminado, 0),
'Descuento'=DL.TotalDescuento,
LD.IDDistribuidor,
P.Referencia,
P.IDTipoProducto

From DetalleLoteDistribucion DL
Join LoteDistribucion LD On DL.IDTransaccion=LD.IDTransaccion
Join vProducto P On DL.IDProducto=P.ID





﻿--USE [EXPRESS]
--GO

--/****** Object:  View [dbo].[VDetalleImpuestoCreditoDebitoBancario]    Script Date: 10/11/2012 08:47:43 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

CREATE View [dbo].[VDetalleImpuestoCreditoDebitoBancario]
As

Select 
DI.IDTransaccion,
DI.Total ,
DI.TotalDiscriminado ,
DI.TotalImpuesto,
'Impuesto'=I.Descripcion,
DB.Facturado,
DB.IDSucursal

From DetalleImpuesto DI
Join DebitoCreditoBancario DB On DI.IDTransaccion =DB.IDTransaccion
Join Impuesto I On DI.IDImpuesto=I.ID 





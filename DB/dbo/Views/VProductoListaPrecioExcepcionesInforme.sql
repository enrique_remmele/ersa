﻿


CREATE View [dbo].[VProductoListaPrecioExcepcionesInforme]
As
Select 
PLPE.*,
'TipoDescuento'=TD.Descripcion,
'TipoDescuentoCodigo'=TD.Codigo,
'FechaDesde'=IsNull(CONVERT(varchar(50), PLPE.Desde, 5), '---'),
'FechaHasta'=IsNull(CONVERT(varchar(50), PLPE.Hasta, 5), '---'),
'ReferenciaCliente'=C.referencia,
'Cliente'=C.RazonSocial,
'Moneda'=M.Referencia,
'Sucursal'=S.Descripcion,
'Producto'=P.Referencia,
'DescripcionProducto'=P.Descripcion,
'ListaPrecio'=LP.Descripcion,
isnull((select precio from ProductoListaPrecio where idListaPrecio = PLPE.IDListaPrecio and IDProducto = PLPE.IdProducto),1) as precio,
P.IDTipoProducto

From ProductoListaPrecioExcepciones PLPE
Join VProducto P On PLPE.IDProducto=P.ID
Join VListaPrecio LP On PLPE.IDListaPrecio=LP.ID
Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID
Join VCliente C On PLPE.IDCliente = C.ID
Join VMoneda M On PLPE.IDMoneda = M.ID
Join Sucursal S On PLPE.IDSucursal=S.ID









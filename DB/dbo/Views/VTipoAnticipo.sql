﻿Create View [dbo].[VTipoAnticipo]
As
Select  TA.ID,
TA.Descripcion,
TA.IDCuentaContable,
'Estado'= (Case when TA.Estado = 1 then 'True' else 'False' end),
CC.Codigo,
'DescripcionCC'=CC.Descripcion
From TipoAnticipo TA
Join CuentaContable CC on CC.ID = TA.IDCuentaContable



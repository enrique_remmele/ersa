﻿CREATE Function [dbo].[FVerificarChequesDepositados]
(--Entrada
	@IDTransaccion numeric(18,0)
)

Returns Varchar(16)
As
Begin
	--Declarar variables Variables
	declare @vIDTransaccionCheque numeric (18,0)
	declare @vFechaDeposito as date
	declare @vRetorno varchar(16) = ''
	
	Begin
			Declare db_cursor cursor for
			Select D.IDTransaccionCheque , DB.Fecha
			From DetalleDepositoBancario D 
			Join ChequeCliente C On D.IDTransaccioncheque=C.IDTransaccion 
			Join DepositoBancario DB on DB.IDTransaccion = D.IDTransaccionDepositoBancario
			Where IDTransaccionDepositoBancario=@IDTransaccion
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionCheque, @vFechaDeposito
			While @@FETCH_STATUS = 0 Begin  
				--Verificar si tiene deposito con fecha mayor
			IF @vRetorno = '' Begin
				IF Exists(Select DB.IDTransaccion From DetalleDepositoBancario D Join ChequeCliente C On D.IDTransaccioncheque=C.IDTransaccion 
				   Join DepositoBancario DB on DB.IDTransaccion = D.IDTransaccionDepositoBancario Where D.IDTransaccionCheque = @vIDTransaccionCheque
				   and DB.IDTransaccion <> @IDTransaccion and DB.Fecha > @vFechaDeposito) begin
				   set @vRetorno = 'DEPOSITO'
				end
			End
			IF @vRetorno = '' Begin
				IF Exists(Select * from ChequeClienteRechazado where IDTransaccionCheque = @vIDTransaccionCheque
				and Fecha > @vFechaDeposito and Anulado = 'False') begin
				   set @vRetorno = 'RECHAZO'
				end
			End
				
				Fetch Next From db_cursor Into @vIDTransaccionCheque, @vFechaDeposito
								
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		End	
	
	return @vRetorno
End




﻿CREATE Function [dbo].[FVentaHabilitadaParaCobranzaCredito]
(
	--Entrada
	@IDTransaccion numeric(18,0)
)

Returns bit

As

Begin

	Declare @vRetorno bit
	Set @vRetorno = 'True'
	
	--Vemos que la venta exista
	If Not Exists(Select * From Venta Where IDTransaccion=@IDTransaccion) Begin
		Set @vRetorno = 'False'
	End
	
	--Vemos si la venta no esta anulada
	If (Select Anulado From Venta Where IDTransaccion=@IDTransaccion) = 'True' Begin
		Set @vRetorno = 'False'
	End
	
	--Si ya esta en un lote
	--If Exists(Select * From VentaLoteDistribucion Where IDTransaccionVenta=@IDTransaccion) Begin
	--	Set @vRetorno = 'False'
	--End
	
	--Si ya esta despachado
	If (Select Despachado From Venta Where IDTransaccion=@IDTransaccion) = 'True' Begin
		Set @vRetorno = 'False'
	End
		
	Return @vRetorno

End





﻿CREATE Function [dbo].[FCuentaContableCosto]
(

	--Entrada
	@IDProducto int,
	@IDSucursal tinyint
)

Returns varchar(50)

As


Begin

	declare @vRetorno varchar(50)
	declare @vIDTipoProducto varchar(50)
	
	--Si tiene configurado en la ficha de producto, usar este
	Set @vRetorno = ISNull((Select CuentaContableCosto From Producto Where ID=@IDProducto), '')
	
	If @vRetorno != '' Begin
		
		--Solo si la cuenta existe en el predeterminado
		If Exists(Select * From VCuentaContable Where Codigo=@vRetorno And Imputable='True') Begin
			GoTo Salir
		End
		
	End
	
	--Obtener el tipo de producto Select * From TipoProducto
	Set @vIDTipoProducto = (Select Top(1) IDTipoProducto From Producto where ID=@IDProducto)
	Set @vRetorno = Isnull((Select Top(1) CuentaContableCosto From TipoProducto Where ID=@vIDTipoProducto), '')
	
	
Salir:
	return @vRetorno
	
End


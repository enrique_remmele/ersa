﻿
CREATE Function [dbo].[FVentaEstado]
(

	--Entrada
	@IDTransaccion numeric(18,0)
)

Returns varchar(50)

As

Begin

	--Variables
	Declare @vRetorno varchar(50)
	
	Set @vRetorno = '---'
	
	--Si no se proceso
	If (Select Top(1) Procesado From Venta Where IDTransaccion=@IDTransaccion) = 'False' Begin
		Set @vRetorno = 'No se proceso correctamente'
		GoTo Salir
	End
	
	--Si Tiene Saldo 0 y No esta Cancelado
	If (Select Top(1) Saldo From Venta Where IDTransaccion=@IDTransaccion) = 0 Begin
		If (Select Top(1) Cancelado From Venta Where IDTransaccion=@IDTransaccion) = 'False' Begin
			Set @vRetorno = 'No cancelado, saldo=0'
			GoTo Salir
		End
	End
	
	--Si esta anulado
	If (Select Top(1) Anulado From Venta Where IDtransaccion=@IDtransaccion)= 'True' Begin
		Set @vRetorno = 'Anulado'
		Goto Salir
	End
		
Salir:
	Return @vRetorno
End


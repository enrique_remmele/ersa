﻿CREATE Function [dbo].[FPrecioUnitarioNeto]
(

	--Entrada 
	@IDProducto int,
	@IDTransaccion int

)

Returns money

As

Begin
	
	--Variables
	Declare @vRetorno money
	Declare @PrecioUnitario money
	Declare @DescuentoUnitario money
	Declare @DiferenciaPrecioUnitario money
	Declare @DiferenciaPorAcuerdo money
	Declare @DiferenciaPesoUnitario money
				
	Set @vRetorno = 0
	
	--Obtener la lista de precio del cliente
	Set @PrecioUnitario = (Select Isnull((Select Top(1) PrecioUnitario from detalleventa where IDTransaccion = @IDTransaccion and IDProducto = @IDProducto),0))
	Set @DescuentoUnitario = (Select Isnull((Select Top(1) DescuentoUnitario from detalleventa where IDTransaccion = @IDTransaccion and IDProducto = @IDProducto),0))
	
	Set @DiferenciaPrecioUnitario = (Select isnull((select Top(1) Isnull(PrecioOriginal,0) - isnull(NuevoPrecio,0) from DetalleNotaCreditoDiferenciaPrecio where IDTransaccionVenta = @IDTransaccion and Isnull(IDTransaccionNotaCredito,0)>0 and IDProducto = @IDProducto),0))
	
	Set @DiferenciaPorAcuerdo = (Select Isnull((Select Top(1) DescuentoUnitario from DetalleNotaCreditoAcuerdo where IDTransaccionVenta = @IDTransaccion and Isnull(IDTransaccionNotaCredito,0)>0 and  IDProducto = @IDProducto),0))
	
	Set @DiferenciaPesoUnitario = (Select isnull((select Top(1) Isnull(ImporteDescuentoUnitario,0) from DetalleNotaCreditoDiferenciaPeso where IDTransaccionVenta = @IDTransaccion and Isnull(IDTransaccionNotaCredito,0)>0 and IDProducto = @IDProducto),0))

		
	Set @vRetorno = @PrecioUnitario - @DescuentoUnitario - @DiferenciaPrecioUnitario - @DiferenciaPorAcuerdo - @DiferenciaPesoUnitario
	
	Return @vRetorno
End



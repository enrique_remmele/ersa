﻿CREATE Function [dbo].[FDiaSemana]
(

	--Entrada
	@Dia tinyint
	
)

Returns varchar(50)

As


Begin

	declare @vRetorno varchar(50)
	Set @vRetorno = '---'
	
	If @Dia = 1 Set @vRetorno = 'lun'
	If @Dia = 2 Set @vRetorno = 'mar'
	If @Dia = 3 Set @vRetorno = 'mié'
	If @Dia = 4 Set @vRetorno = 'jue'
	If @Dia = 5 Set @vRetorno = 'vie'
	If @Dia = 6 Set @vRetorno = 'sáb'
	If @Dia = 7 Set @vRetorno = 'dom'
	
	return @vRetorno
	
End




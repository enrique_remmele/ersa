﻿create Function [dbo].[FMesExample](@Desde date,@Hasta Date)

returns Table
as
return(Select
	   FP.IDTransaccion,
	   VDC.IDTransaccionCobranza,
       'Fec'=convert(varchar(50),CC.Fecha,5),
       CC.Fecha,
       VDC.IDCliente,
       VDC.Cliente,
       VDC.ReferenciaCliente,
       VDC.[Cod.],
      'CantComprobantes'=(Select Count(*) From  FormaPago FP
					Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
					Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
					Join Moneda M On E.IDMoneda=M.ID
					Join CobranzaContado CC On CC.IDTransaccion=FP.IDTransaccion
					Where CC.Anulado='False' And CC.Fecha Between @Desde And @Hasta ),
       VDC.Cobrador,
VDC.IDCobrador,
VDC.IDVendedor,
VDC.IDSucursal,
VDC.IDDeposito,
VDC.IDTipoComprobante,
'ComprobanteFactura'=VDC.Comprobante,
'NumeroCobranza'=VDC.Numero,
'NumeroLote'=VLD.Numero,
'FormaPago'='Efectivo',
'Moneda'=M.Referencia,
'Cambio'=E.Cotizacion,
'Banco'='---',
'Comprobante'=E.Comprobante,
'FechaPago'='',
'TipoComprobante'=TC.Descripcion,
'Tipo'='EFE',
'Cant'=0,
'TotalCobrado'=(VDC.Cobrado),
'Importe'=FP.Importe,
'CancelarCheque'=FP.CancelarCheque

From FormaPago FP
Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID
Join CobranzaContado CC On CC.IDTransaccion=FP.IDTransaccion
Join VVentaDetalleCobranzaContado VDC On VDC.IDTransaccionCobranza=FP.IDTransaccion
Join VVentaLoteDistribucion VLD On VLD.IDTransaccionVenta=VDC.IDTransaccion

Where CC.Anulado='False'

Union All

----Cheque Cliente
Select
FP.IDTransaccion,
VDC.IDTransaccionCobranza,
'Fecha'=convert(varchar(50),CC.Fecha,5),
CC.Fecha,
VDC.IDCliente,
VDC.Cliente,
VDC.ReferenciaCliente,
VDC.[Cod.],
'CantComprobantes'=0,
VDC.Cobrador,
VDC.IDCobrador,
VDC.IDVendedor,
VDC.IDSucursal,
VDC.IDDeposito,
VDC.IDTipoComprobante,
'ComprobanteFactura'=VDC.Comprobante,
'NumeroCobranza'=VDC.Numero,
'NumeroLote'=VLD.Numero,
'FormaPago'='Cheque',
'Moneda'=V.Moneda,
'Cambio'=V.Cotizacion,
'Banco'=V.Banco,
'Comprobante'=V.NroCheque,
'FechaPago'=V.Fecha,
'TipoComprobante'=(Case When (V.Diferido)='True' Then 'CHEQUE DIFERIDO' Else 'CHEQUE' End),
'Tipo'=V.Tipo,
'Cant'=0,
'TotalCobrado'=(VDC.Cobrado),
'Importe'=FP.Importe,
FP.CancelarCheque

From FormaPago FP
Join VChequeCliente V On FP.IDTransaccionCheque=V.IDTransaccion 
Join CobranzaContado CC On CC.IDTransaccion=FP.IDTransaccion
Join VVentaDetalleCobranzaContado VDC On VDC.IDTransaccionCobranza=FP.IDTransaccion
Join VVentaLoteDistribucion VLD On VLD.IDTransaccionVenta=VDC.IDTransaccion

Where CC.Anulado='False'

Union All

--Documento
Select
FP.IDTransaccion,
VDC.IDTransaccionCobranza,
'Fecha'=convert(varchar(50),CC.Fecha,5),
CC.Fecha,
VDC.IDCliente,
VDC.Cliente,
VDC.ReferenciaCliente,
VDC.[Cod.],
'CantComprobantes'=0,
VDC.Cobrador,
VDC.IDCobrador,
VDC.IDVendedor,
VDC.IDSucursal,
VDC.IDDeposito,
VDC.IDTipoComprobante,
'ComprobanteFactura'=VDC.Comprobante,
'NumeroCobranza'=VDC.Numero,
'NumeroLote'=VLD.Numero,
'FormaPago'='Documento',
'Moneda'=M.Referencia,
'Cambio'=FPD.Cotizacion,
'Banco'='---',
'Comprobante'=FPD.Comprobante,
'FechaPago'='',
'TipoComprobante'=TC.Descripcion,
'Tipo'='---',
'Cant'=0,
'TotalCobrado'=(VDC.Cobrado),
'Importe'=FP.Importe,
FP.CancelarCheque

From FormaPago FP
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Join Moneda M On FPD.IDMoneda=M.ID
Join CobranzaContado CC On CC.IDTransaccion=FP.IDTransaccion
Join VVentaDetalleCobranzaContado VDC On VDC.IDTransaccionCobranza=FP.IDTransaccion
Join VVentaLoteDistribucion VLD On VLD.IDTransaccionVenta=VDC.IDTransaccion

Where CC.Anulado='False')
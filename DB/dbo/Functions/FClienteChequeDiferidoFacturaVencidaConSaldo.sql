﻿CREATE Function [dbo].[FClienteChequeDiferidoFacturaVencidaConSaldo]
(

	--Entrada
	@IDCliente int
	
)

Returns bit

As

Begin

	Declare @vRetorno bit = 0
	Declare @vSaldo as money
	
	if exists (select * from cliente where id = @IDCliente) begin
		if (select PlazoChequeDiferido from cliente where id = @IDCliente) > 0 begin
			Set @vSaldo = (Select sum(saldo) from venta where anulado = 0 and procesado = 1 and  IDCliente = @IDCliente and saldo > 0)
			if (@vSaldo > 1) begin
				Set @vRetorno = 1
			end
		end
	End
	
	Return @vRetorno	

End





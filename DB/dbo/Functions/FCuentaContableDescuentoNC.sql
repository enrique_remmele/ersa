﻿CREATE Function [dbo].[FCuentaContableDescuentoNC]
(

	--Entrada
	@IDTransaccion numeric(18,0)
)

Returns varchar(50)

As


Begin

	declare @vRetorno varchar(50)
	declare @vIDTipoProducto int
	declare @vIDProducto int
	declare @vTipoDescuento varchar(50)
	declare @vIDSubMotivoNC int
	declare @vIDTransaccionVenta int

	--Sacar el primer producto
	Set @vIDTransaccionVenta = (Select Top(1) IDTransaccionVentaGenerada from NotaCreditoVenta where IDTransaccionNotaCredito = @IDTransaccion)

	Set @vIDProducto = (Select Top(1) IDProducto From DetalleVenta Where IDTransaccion=@vIDTransaccionVenta)
	Set @vIDTipoProducto = (Select Top(1) IDTipoProducto From Producto where ID=@vIDProducto)
	Set @vIDSubMotivoNC = (SElect Top(1) IDSubMotivoNotaCredito from NotaCredito where IDTransaccion = @IDTransaccion)
	Set @vTipoDescuento = (Select Tipo from VSubMotivoNotaCredito where id =@vIDSubMotivoNC)
	
	if @vTipoDescuento = 'Financiero' begin
		Set @vRetorno = (Select Top(1) CuentaContableDescuento From VTipoProductoCuentaContableMotivoNC Where IDTipoProducto = @vIDTipoProducto)
	end
	else if @vTipoDescuento = 'Acuerdo' begin
		Set @vRetorno = (Select Top(1) CuentaContableAcuerdo From VTipoProductoCuentaContableMotivoNC Where IDTipoProducto = @vIDTipoProducto)
	end
	else if @vTipoDescuento = 'Diferencia de Precio' begin
		Set @vRetorno = (Select Top(1) CuentaContableDiferenciaPrecio From VTipoProductoCuentaContableMotivoNC Where IDTipoProducto = @vIDTipoProducto)
	end
		
	If @vRetorno != '' Begin
		
		--Solo si la cuenta existe en el predeterminado
		If Exists(Select * From VCuentaContable Where Codigo=@vRetorno And Imputable='True') Begin
			GoTo Salir
		End
	End

	Set @vRetorno = ''
Salir:
	return @vRetorno
	
End


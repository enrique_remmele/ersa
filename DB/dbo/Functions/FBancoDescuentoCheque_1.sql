﻿
CREATE Function [dbo].[FBancoDescuentoCheque]
(

	--Entrada
	@IDTransaccion int
)

Returns int

As

Begin
	Declare @vRetorno int = 0
	Declare @vIDTransaccionDescuentoCheque int
	Declare @vIDCuentaBancariaDescuento int
	Declare @vIDBanco int
	
	Set @vIDTransaccionDescuentoCheque = Isnull((select IDTransaccionDescuentoCheque from detalledescuentocheque where idtransaccionchequecliente = @IDTransaccion),0)
	
	if @vIDTransaccionDescuentoCheque = 0 begin
		goto Salir
	end
	
	Set @vIDCuentaBancariaDescuento = (select IDCuentaBancaria from DescuentoCheque where idtransaccion = @vIDTransaccionDescuentoCheque)
	
	if @vIDCuentaBancariaDescuento = 0 begin
		goto Salir
	end
	
	Set @vIDBanco = (Select IDBanco from CuentaBancaria where id = @vIDCuentaBancariaDescuento)
	
	if @vIDBanco = 0 begin
		goto Salir
	end
	
	Set @vRetorno = @vIDBanco
Salir:
	
	Return @vRetorno	
	


End





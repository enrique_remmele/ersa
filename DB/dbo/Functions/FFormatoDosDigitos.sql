﻿CREATE Function [dbo].[FFormatoDosDigitos]
(

	--Entrada
	@Valor smallint
	
)

Returns varchar(50)

As


Begin

	declare @vRetorno varchar(50)
	Set @vRetorno = '00'
	if @Valor < 10 Set @vRetorno = '0' + convert(varchar(50), @Valor)
	if @Valor >= 10 Set @vRetorno = convert(varchar(50), @Valor)
	
	return @vRetorno
	
End



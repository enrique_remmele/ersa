﻿CREATE Function [dbo].[FCorreoCCConciliacionContable]
(

	--Entrada
	@IDUsuario int
)

Returns varchar(300)

As

Begin
	
	Declare @vRetorno varchar(300)
	Declare @vCorreoAuditoria varchar(99)
	Declare @vCorreoContabilidad varchar(99)
	Declare @vCorreoJefeDepartamento varchar(99)
	
	Set @vCorreoAuditoria = (Select CorreoJefe from vDepartamentoEmpresa where Departamento = 'AUDITORIA')
	Set @vCorreoContabilidad = (Select CorreoJefe from vDepartamentoEmpresa where Departamento = 'CONTABILIDAD')
	Set @vCorreoJefeDepartamento=(Select CorreoJefe from vDepartamentoEmpresa 
												where ID = (Select IDDepartamentoEmpresa from Perfil 
												where ID = (Select IDPerfil from usuario where ID = @IDUsuario)))

	Set @vRetorno = Concat(@vCorreoAuditoria,';', @vCorreoContabilidad, ';', @vCorreoJefeDepartamento)

	Return @vRetorno

End





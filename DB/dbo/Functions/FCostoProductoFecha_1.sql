﻿CREATE Function [dbo].[FCostoProductoFecha]
(

	--Entrada
	@IDProducto INT
	, @Fecha DATE
)

Returns money

As

Begin

	Declare @vRetorno money
	Declare @vConfiguracionCosto varchar(50)
	
	--Obtenemos la configuracion del Costo
	Set @vConfiguracionCosto = (Select IsNull((Select Top(1) MovimientoCostoProducto From Configuraciones), 'PROMEDIO'))
	
	--Establecemos el monto
	
	Set @vRetorno = Case 
						When @vConfiguracionCosto = 'ULTIMO' Then
							--(Select IsNull((Select UltimoCosto From RegistroCosto Where IDProducto=@IDProducto),0))
							(Select IsNull((SELECT TOP 1 VDC.PrecioUnitario
												FROM VDetalleCompra VDC 
												JOIN Compra C ON VDC.IDTransaccion = C.IDTransaccion 
												WHERE VDC.IDProducto = @IDProducto
												AND C.Fecha <= @Fecha ORDER BY C.Fecha DESC),0))
						When @vConfiguracionCosto = 'PROMEDIO' Then
							--(Select IsNull((Select Promedio From RegistroCosto Where IDProducto=@IDProducto),0))
							(Select IsNull((Select AVG(VDC.TotalDiscriminado / VDC.Cantidad)
												FROM VDetalleCompra VDC 
												JOIN Compra C ON VDC.IDTransaccion = C.IDTransaccion 
												WHERE VDC.IDProducto = @IDProducto
												AND C.Fecha <= @Fecha),0))
						When @vConfiguracionCosto = '' Then 0
					End
	
	Return @vRetorno

End





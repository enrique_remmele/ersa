﻿
CREATE Function [dbo].[FCostoProduccion]
(

	--Entrada
	@IDProducto int,
	@Fecha date
)

Returns money

As

Begin

	Declare @vRetorno money
	--Establecemos el monto
	Set @vRetorno = ISNULL((Select Top(1) Costo from vDetalleProductoCosto where Fecha <= @Fecha and IDProducto = @IDProducto order by Fecha DESC),0)
	IF @vRetorno = 0 begin
		Set @vRetorno = (Select isnull(CostoPromedio,0) from vProducto where id = @IDProducto)
	end
	
	Return @vRetorno

End





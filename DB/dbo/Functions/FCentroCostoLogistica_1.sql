﻿
CREATE Function [dbo].[FCentroCostoLogistica]
(

	--Entrada
	@IDTransaccion numeric(18,0)
	
)

Returns bit

As


Begin

	declare @vRetorno bit = 'False'
	declare @vIDCentroCosto int
	

	--Sacar el primer producto
	Set @vIDCentroCosto = (Select ID From CentroCosto Where Descripcion='Logistica')

	if exists (select * from vDetalleAsiento where IDTransaccion = @IDTransaccion and IDCentroCosto = @vIDCentroCosto) begin
		Set @vRetorno = 'True'
	end
	

Salir:
	return @vRetorno
	
End


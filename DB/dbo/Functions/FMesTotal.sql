﻿CREATE Function [dbo].[FMesTotal](@Año int)
returns Table
as
return(Select 
'Mes'=dbo.fmes(MesNumero),
'Total'=Sum(Total)
 From VDetalleVenta
 Where Year(FechaEmision)=@Año
 Group By MesNumero)
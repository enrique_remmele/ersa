﻿--RETORNA LA CANTIDAD DE DECIMALES QUE UTILIZA UNA MONEDA
--JGR 20140604
CREATE Function [dbo].[FDecimalMoneda]
(

	--Entrada 
	@IDMoneda int

)

Returns money

As

Begin
	
	--Variables
	Declare @vRetorno int

	Set @vRetorno = (Select IsNull((Select 
	CASE
	WHEN Decimales = 0 THEN 0
	WHEN Decimales = 1 THEN 2 --2 decimales
	ELSE 0
	END 
	From Moneda Where ID=@IDMoneda), 0))
	
	Return @vRetorno
	
End



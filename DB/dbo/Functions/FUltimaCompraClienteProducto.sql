﻿CREATE Function [dbo].[FUltimaCompraClienteProducto]
(

	--Entrada
	@IDCliente int,
	@IDProducto int
)

Returns date

As

Begin

	--Variables
	Declare @vRetorno date
	
	Set @vRetorno = (Select max(FechaEmision) from VDetalleVenta where IDProducto = @IDProducto and IDCliente = @IDCliente and Anulado = 'False')
		
	Return @vRetorno
End


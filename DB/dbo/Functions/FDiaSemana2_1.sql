﻿CREATE Function [dbo].[FDiaSemana2]
(

	--Entrada
	@Dia tinyint
	
)

Returns varchar(50)

As


Begin

	declare @vRetorno varchar(50)
	Set @vRetorno = '---'
	
	If @Dia = 1 Set @vRetorno = 'DOMINGO'
	If @Dia = 2 Set @vRetorno = 'LUNES'
	If @Dia = 3 Set @vRetorno = 'MARTES'
	If @Dia = 4 Set @vRetorno = 'MIERCOLES'
	If @Dia = 5 Set @vRetorno = 'JUENVES'
	If @Dia = 6 Set @vRetorno = 'VIERNES'
	If @Dia = 7 Set @vRetorno = 'SABADO'
	
	return @vRetorno
	
End




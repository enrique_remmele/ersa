﻿CREATE Function [dbo].[FDescuentoUnitarioAcuerdo]
(

	--Entrada 
	@IDProducto int,
	@IDTransaccion int

)

Returns money

As

Begin
	
	--Variables
	Declare @vRetorno money
	Declare @DiferenciaPorAcuerdo money
				
	Set @vRetorno = 0
	
	--Obtener la lista de precio del cliente
	Set @DiferenciaPorAcuerdo = (Select Isnull((Select Sum(DescuentoUnitario) from DetalleNotaCreditoAcuerdo where IDTransaccionVenta = @IDTransaccion and Isnull(IDTransaccionNotaCredito,0)>0 and  IDProducto = @IDProducto),0))
		
	Set @vRetorno = @DiferenciaPorAcuerdo
	
	Return @vRetorno
End



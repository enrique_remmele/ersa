﻿CREATE Function [dbo].[FExistenciaProducto]
(

	--Entrada
	@IDProducto int,
	@IDDeposito tinyint
)

Returns decimal(10,2)

As


Begin

	declare @vRetorno decimal(10,2)
	declare @vExistenciaReal decimal(10,2)
	declare @vReservado decimal(10,2)
	
	--Si no controla stock, retornar 1
	--If (Select ControlarExistencia From Producto Where ID=@IDProducto) = 'False' Begin
	--	Set @vRetorno = 1
	--	return @vRetorno
	--End
	
	Set @vExistenciaReal = (Select dbo.FExistenciaProductoReal(@IDProducto, @IDDeposito))
	--Set @vReservado = (Select dbo.FExistenciaProductoReservado(@IDProducto, @IDDeposito)) 						
	
	--Esto no funciona para VENTAS, solo para PEDIDOS, crear uno nuevo 
	--que incluya RESERVADOS						
	--Set @vRetorno = @vExistenciaReal - @vReservado
	Set @vRetorno = @vExistenciaReal
	
	return @vRetorno
	
End


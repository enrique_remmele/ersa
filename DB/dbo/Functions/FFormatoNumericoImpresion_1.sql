﻿CREATE Function [dbo].[FFormatoNumericoImpresion]
(	--Entrada 
	@Importe as money,
	@Decimal as bit
)

Returns Varchar(20)
As
Begin
	--variables							)
	Declare @vRetorno as varchar(256)
	Declare @Comprobante as varchar(16)	
	
	IF @Decimal = 'False' begin
	  set @vRetorno = Replace(LEFT(CONVERT(varchar(17), CAST(@Importe AS money), 1), 
					  ISNULL(NULLIF(CHARINDEX('.', CONVERT(varchar(17), CAST(@Importe AS money), 1)) - 1, -1), 
					  LEN(CONVERT(varchar(17), CAST(@Importe AS money), 1)))),',','.')
	End 
	IF @Decimal = 'True' begin
	  set @vRetorno = Replace(Replace(Replace(CONVERT(varchar(50),  CONVERT(money, @Importe), 1),',','-'),'.',','),'-','.')
	End 
	IF @vRetorno = '0' or @vRetorno = '0,00' begin
	   set @vRetorno = ''
	end

	return @vRetorno

End


--select * from vImpresionFactura where idtransaccion = 38325
﻿CREATE FUNCTION [dbo].[FRemoverAcento] ( @Cadena VARCHAR(100) )
RETURNS VARCHAR(100)
AS BEGIN
 
 --Reemplazamos las vocales acentuadas
    RETURN upper( REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Cadena, 'á', 'a'), 'é','e'), 'í', 'i'), 'ó', 'o'), 'ú','u'), 'ñ','n'), 'ý','y'), '"',''), '$',''), '%',''), '&',''), '/',''), '#',''), '*',''), '@',''), '(',''), ')',''), '!',''), '?',''), '\',''), ':',''), ';',''), '-',''), '_',''), '[',''), ']',''), '<',''), '>',''), '°',''), '´',''))
 
   END
 


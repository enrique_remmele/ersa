﻿CREATE Function [dbo].[FCuentaContableCliente]
(

	--Entrada
	@IDTransaccion numeric(18,0),
	@IDSucursal tinyint
)

Returns varchar(50)

As


Begin

	declare @vRetorno varchar(50)
	declare @vIDTipoProducto varchar(50)
	declare @vIDProducto int

	--Sacar el primer producto
	Set @vIDProducto = (Select Top(1) IDProducto From DetalleVenta Where IDTransaccion=@IDTransaccion)

	--Si tiene configurado en la ficha de producto, usar este
	Set @vRetorno = ISNull((Select CuentaContableDeudor From Producto Where ID=@vIDProducto), '')
	
	If @vRetorno != '' Begin
		
		--Solo si la cuenta existe en el predeterminado
		If Exists(Select * From VCuentaContable Where Codigo=@vRetorno And Imputable='True') Begin
			GoTo Salir
		End
		
	End
	
	--Obtener el tipo de producto Select * From TipoProducto
	Set @vIDTipoProducto = (Select Top(1) IDTipoProducto From Producto where ID=@vIDProducto)
	Set @vRetorno = Isnull((Select Top(1) CuentaContableCliente From TipoProducto Where ID=@vIDTipoProducto), '')
	
Salir:
	return @vRetorno
	
End


﻿CREATE FUNCTION [dbo].[FValidarAnulacionDocumento]
(
	@IDTransaccion int,
	@Operacion varchar(10),
	@IDUsuario int,
	@IDMotivo int,

	--Transaccion
	@IDSucursal int,
    @IDDeposito int,
    @IDTerminal int
)
RETURNS varchar(500)
AS
BEGIN
	Declare @Mensaje varchar(500)
	Declare @vDocumento varchar(50)
	Declare @vIDOperacion int
	Set @Mensaje = 'OK'

	--IDTransaccion
	If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
		set @Mensaje = 'El sistema no encontro el registro solicitado.'
		return @Mensaje
	End
	
	Set @vIDOperacion = (Select IDOperacion from Transaccion where id = @IDTransaccion)
	Set @vDocumento = (Select Descripcion from Operacion where ID = @vIDOperacion)


	if @vDocumento = 'VENTAS CLIENTES' begin
		--No existe el motivo
		If not Exists(Select * From MotivoAnulacionVenta where ID = @IDMotivo) Begin
			set @Mensaje = 'Motivo seleccionado invalido.'
			return @Mensaje
		End

		
		--IDTransaccion
		If Not Exists(Select * From Venta Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			return @Mensaje
		End			
		
		--Verificar que el documento ya este anulado
		If Exists(Select * From Venta Where IDTransaccion=@IDTransaccion And Anulado='True') Begin
			set @Mensaje = 'El Documento ya esta anulado'
			return @Mensaje
		End
	

		--Verificar que el documento no tenga otros registros asociados
		--Recibos
		If Exists(Select * From VVentaDetalleCobranza Where IDTransaccion=@IDTransaccion And AnuladoCobranza='False') Begin
			set @Mensaje = 'El documento tiene recibos asociados! Elimine los recibos para continuar.'
			return @Mensaje
		End
	
		--Notas de Credito/Debito
		If Exists(Select * From NotaCreditoVenta A Join NotaCredito on NOtaCredito.IdTransaccion =  A.IdTransaccionNotaCredito Where A.IDTransaccionVentaGenerada =@IDTransaccion and NotaCredito.Anulado = 'False') Begin
			set @Mensaje = 'La Venta tiene Nota de Credito Aplicada! No se puede anular.'
			return @Mensaje
		End

	end

	if @vDocumento = 'NOTA CREDITO' begin
		declare @vNroOperacion int
		--Validar
		If Exists(Select * From NotaCreditoVentaAplicada NCVA Join NotaCreditoAplicacion NCA On NCVA.IDTransaccionNotaCreditoAplicacion = NCA.IDTransaccion Where NCVA.IDTransaccionNotaCredito=@IDTransaccion And NCA.Anulado='False') Begin
			set @vNroOperacion = (Select top(1) NCA.Numero From NotaCreditoVentaAplicada NCVA Join NotaCreditoAplicacion NCA On NCVA.IDTransaccionNotaCreditoAplicacion = NCA.IDTransaccion Where NCVA.IDTransaccionNotaCredito=@IDTransaccion And NCA.Anulado='False')
			set @Mensaje = Concat('La Nota de Credito tiene facturas aplicadas! Anule primeramente la aplicacion para continuar. N° de Operacion de Aplicación: ', @vNroOperacion)
			return @Mensaje
		End
	end
	return @Mensaje

END

﻿CREATE Function [dbo].[FCuentaContableTipo]
(

	--Entrada
	@Codigo varchar(50)
)

Returns varchar(50)

As

Begin

	Declare @vRetorno varchar(50)
	Declare @vPrefijo varchar(50)
	Set @vRetorno = '---'
	
	Set @vPrefijo = SubString(@Codigo, 0, 2)
				
	--Cuentas del DEBE
	If @vPrefijo = '1' Or @vPrefijo = '5' Begin
		set @vRetorno = 'DEBE'
	End

	--Cuentas del HABER
	If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
		set @vRetorno = 'HABER'
	End
			
	Return @vRetorno

End





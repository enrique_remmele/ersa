﻿CREATE Function [dbo].[FPrecioProducto]
(

	--Entrada 
	@IDProducto int,
	@IDCliente int,
	@IDSucursal tinyint

)

Returns money

As

Begin
	
	--Variables
	Declare @vRetorno money
	Declare @vIDListaPrecio int
	Declare @vPrecio money	
			
	Set @vRetorno = 0
	
	--Obtener la lista de precio del cliente
	Set @vIDListaPrecio = (Select IsNull((Select IDListaPrecio From Cliente Where ID=@IDCliente), 0))
		
	--Precio 
	-- Le saque la SUCURSAL, porque no me estaba funcionando para Vender a un cliente de una sucursal a otra.
	--Set @vPrecio = IsNull((Select Precio From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDSucursal=@IDSucursal), 0)
	Set @vPrecio = IsNull((Select Precio From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio), 0)
	
	Set @vRetorno = @vPrecio
	
	Return @vRetorno
	
End



﻿CREATE Function FVentaCalcularSaldo
(

	--Entrada
	@IDTransaccion numeric(18,0)
)

Returns money

As

Begin

	--Variables
	Declare @vCancelarAutomatico bit
	Declare @vTotal money
	Declare @vSaldo money
	Declare @vCobrado money
	Declare @vCobradoMigracion money
	Declare @vDescontado money
	Declare @vAcreditado money
	
	--Hayar valores
	Set @vCancelarAutomatico = (Select CancelarAutomatico From VVenta Where IDTransaccion=@IDTransaccion)

	--Si cancela automatico, el saldo es 0
	If @vCancelarAutomatico = 1 Begin
		Set @vSaldo = 0
		GoTo Salir
	End

	Set @vTotal = (Select Total From Venta Where IDTransaccion=@IDTransaccion)

	Set @vCobrado = IsNull((Select Sum(Importe) From VVentaDetalleCobranza 
							Where IDTransaccion=@IDTransaccion
							And Anulado='False'),0)
	Set @vCobradoMigracion = IsNull((Select Cobrado 
									  From VentaImportada 
									  Where IDTransaccion=@IDTransaccion),0)

	Set @vDescontado = IsNull((Select Sum(V.Importe) From vNotaCreditoVenta V 
								Where V.IDTransaccionVenta=@IDTransaccion
								And V.Anulado='False'),0)

	Set @vAcreditado = IsNull((Select Sum(Importe) From VNotaDebitoVenta 
								Where IDTransaccionVenta=@IDTransaccion),0)
	
	Set @vSaldo = (@vTotal + @vAcreditado) - (@vCobrado + @vDescontado + @vCobradoMigracion)
	
Salir:
	Return @vSaldo
End
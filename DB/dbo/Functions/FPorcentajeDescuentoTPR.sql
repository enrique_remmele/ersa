﻿Create Function [dbo].[FPorcentajeDescuentoTPR]
(

	--Entrada 
	@IDProducto int,
	@IDCliente int,
	@IDSucursal tinyint

)

Returns tinyint

As

Begin
	
	--Variables
	Declare @vRetorno tinyint
	Declare @vIDListaPrecio int
		
	--Listas de Precio
	Declare @vTPR money
	Declare @vTPRPorcentaje tinyint
	Declare @vTPRDesde date
	Declare @vTPRHasta date
			
	Set @vRetorno = 0
	
	--Obtener la lista de precio del cliente
	Set @vIDListaPrecio = (Select IsNull((Select IDListaPrecio From Cliente Where ID=@IDCliente), 0))
		
	--TPR
	Set @vTPRPorcentaje = IsNull((Select TPRPorcentaje From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDSucursal=@IDSucursal), 0)
	set @vRetorno = @vTPRPorcentaje
	
	Return @vRetorno
	
End


﻿CREATE Function [dbo].[FVentaTotalMesAño]

(
	--@IDCliente int,
	@Mes tinyint,
	@Año smallint,
	@IDCliente int
)

Returns money

As

Begin
	
	Declare @vRetorno money
	
	Set @vRetorno = IsNull((Select Sum(Total) From Venta Where IDCliente=@IDCliente And Month(FechaEmision)=@Mes And Year(FechaEmision)=@Año) ,0)
	
	Return @vRetorno

End

--Select 
--Cliente,
--'Junio'=dbo.FVentaTotalMesAño(6, 2012)
--From VVenta V
--Group By Cliente


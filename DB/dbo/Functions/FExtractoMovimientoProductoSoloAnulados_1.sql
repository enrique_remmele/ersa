﻿CREATE Function [dbo].[FExtractoMovimientoProductoSoloAnulados]
(

	--Entrada
	@IDProducto int,
	@Desde date,
	@Hasta date,
	@FechaOperacion bit = 'False'
)

Returns decimal(18,2)

As


Begin

	--Variables
	Declare @vRetorno Decimal (18,2)
	Declare @vEntrada decimal(18,2)
	Declare @vSalida decimal(18,2)
	
	If @FechaOperacion = 'False' Begin	
		Select @vEntrada = IsNull(Sum(Entrada), 0),
				@vSalida = IsNull(Sum(Salida), 0)
		From VExtractoMovimientoProductoDetalle 
		Where IDProducto=@IDProducto And Fecha >= @Desde and Fecha < @Hasta 
		--and FechaAnulado >= @Desde and FechaAnulado < @Hasta
		and Anulado = 1
	End

	If @FechaOperacion = 'True' Begin	
		Select @vEntrada = IsNull(Sum(Entrada), 0),
				@vSalida = IsNull(Sum(Salida), 0)
		From VExtractoMovimientoProductoDetalle 
		Where IDProducto=@IDProducto And FechaOperacion >= @Desde and FechaOperacion < @Hasta 
		--and FechaAnulado >= @Desde and FechaAnulado < @Hasta
		and Anulado = 1
	End

	--Resultado
	Set @vRetorno = @vEntrada - @vSalida


	Return @vRetorno 
	
End

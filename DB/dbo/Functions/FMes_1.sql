﻿
CREATE Function [dbo].[FMes]
(

	--Entrada
	@Mes tinyint
	
)

Returns varchar(50)

As


Begin

	declare @vRetorno varchar (50)
	Set @vRetorno = '---'
	If @Mes = 1 Set @vRetorno = 'Enero'
	If @Mes = 2 Set @vRetorno = 'Febrero'
	If @Mes = 3 Set @vRetorno = 'Marzo'
	If @Mes = 4 Set @vRetorno = 'Abril'
	If @Mes = 5 Set @vRetorno = 'Mayo'
	If @Mes = 6 Set @vRetorno = 'Junio'
	If @Mes = 7 Set @vRetorno = 'Julio'
	If @Mes = 8 Set @vRetorno = 'Agosto'
	If @Mes = 9 Set @vRetorno = 'Setiembre'
	If @Mes = 10 Set @vRetorno = 'Octubre'
	If @Mes = 11 Set @vRetorno = 'Noviembre'
	If @Mes = 12 Set @vRetorno = 'Diciembre'
		
	
	return @vRetorno
	
End




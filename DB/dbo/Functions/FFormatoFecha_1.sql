﻿CREATE Function [dbo].[FFormatoFecha]
(

	--Entrada
	@Fecha date
	
)

Returns varchar(50)

As


Begin

	declare @vRetorno varchar(50)
	Set @vRetorno = CONVERT(varchar(2), dbo.FFormatoDosDigitos(Day(GetDate()))) + '/' + CONVERT(varchar(2), dbo.FFormatoDosDigitos(Month(GetDate()))) + '/' + CONVERT(varchar(4), Year(GetDate()))
	Set @vRetorno = CONVERT(varchar(2), dbo.FFormatoDosDigitos(Day(@Fecha))) + '/' + CONVERT(varchar(2), dbo.FFormatoDosDigitos(Month(@Fecha))) + '/' + CONVERT(varchar(4), Year(@Fecha))
	return @vRetorno
	
End



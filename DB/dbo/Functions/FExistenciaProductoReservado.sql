﻿
CREATE Function [dbo].[FExistenciaProductoReservado]
(

	--Entrada
	@IDProducto int,
	@IDDeposito tinyint
)

Returns decimal(10,2)

As


Begin

	declare @vRetorno decimal(10,2)
	declare @vReservado decimal(10,2)
	
	Set @vReservado = (IsNull((Select 'Cantidad'=Sum(Cantidad) 
						From DetallePedido DP 
						Join Pedido P On DP.IDTransaccion=P.IDTransaccion
						Where DP.IDProducto=@IDProducto 
						And DP.IDDeposito=@IDDeposito
						And P.Procesado='True'
						And P.Anulado='False'
						--And P.Facturado='False'
						
						--Que no este facturado
						And (Case When (Select Max(PV.IDTransaccionVenta) From PedidoVenta PV Where PV.IDTransaccionPedido=P.IDTransaccion) Is Null Then 'False' Else 'True' End) = 'False'
						
						--Que el pedido este dentro delos 3 dias proximos
						And DateDiff(d, GetDate(), P.FechaFacturar) Between 0 And 3
						) ,0))
							
	Set @vRetorno = @vReservado
	
	return @vRetorno
	
End


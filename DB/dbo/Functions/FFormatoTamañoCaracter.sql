﻿CREATE function [dbo].[FFormatoTamañoCaracter]
(
	@Tamaño tinyint	,
	@Texto varchar (50)
	
)

Returns varchar (50)

As

Begin
	
	Declare @vRetorno varchar (50)
	Declare @vCantidad int
	Declare @vI int
	
	Set @vRetorno = ''
	Set @vCantidad = @Tamaño - Len(@Texto)
	Set @vI = 0
	
	While @vI < @vCantidad Begin
		Set @vRetorno = @vRetorno + ' '
		Set @vI = @vI + 1
	End
	
	Set @vRetorno = @Texto + @vRetorno 
	
	select @vretorno = substring (@vretorno ,0, @Tamaño+1 )
	return @vRetorno
	
End


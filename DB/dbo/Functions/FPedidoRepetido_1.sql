﻿CREATE Function [dbo].[FPedidoRepetido]
(

	--Entrada
	@IDTransaccion numeric(18,0)
)

Returns bit

As

Begin

	--Variables
	Declare @vRetorno bit 
	Set @vRetorno = 'False'

	Declare @vCantidad decimal(18,0)
	Declare @vIDProducto int
	Declare @vIDDeposito tinyint

	Declare @vIDCliente int
	Declare @vIDSucursal int
	Declare @vIDFormaPagoFactura int 
	Declare @vIDListaPrecio int
	Declare @vDescuentoUnitario money
	Declare @vFechaFacturar date
	Declare @vTotal money
	Declare @vIDSucursalCliente int
	Declare @vIDUsuario int 
	Declare @vFecha date

	--Obtener valores
	Set @vIDCliente = (Select IDCliente From Pedido Where IDTransaccion=@IDTransaccion)
	set @vIDSucursal = (Select IDSucursal  from Pedido where IDTransaccion= @IDTransaccion)
	Set @vIDFormaPagoFactura = (Select IDFormaPagoFactura From Pedido Where IDTransaccion=@IDTransaccion)
	Set @vIDListaPrecio = isnull((select IDListaPrecio from Pedido where IDTransaccion = @IDTransaccion),0)
	Set @vFechaFacturar = (Select FechaFacturar from Pedido where IDTransaccion = @IDTransaccion)
	Set @vFecha = (Select Fecha from Pedido where IDTransaccion = @IDTransaccion)
	Set @vIDUsuario = (Select IDUsuario From vPedido Where IDTransaccion=@IDTransaccion)
	Set @vTotal = (Select Total from Pedido where IDTransaccion = @IDTransaccion)
	Set @vIDSucursalCliente = isnull((select IDSucursalCliente from Pedido where IDTransaccion = @IDTransaccion),0)

	--Si no existe uno igual en la cabecera
	If not exists (Select * From vPedido Where IDTransaccion<>@IDTransaccion 
										and Anulado = 'False'
										and Total = @vTotal
										and IDCliente = @vIDCliente
										and IDFormaPagoFactura = @vIDFormaPagoFactura
										and IDListaPRecio = @vIDListaPrecio
										and FechaFacturar = @vFechaFacturar
										and Fecha = @vFecha
										and IDSucursal = @vIDSucursal
										and IDUsuario = @vIDUsuario
										and IDSucursalCliente = @vIDSucursalCliente)Begin
		GoTo Salir
	End
	--si no existe uno igual en el detalle
	Declare db_cursor cursor for
	Select Cantidad, IDProducto
	From DetallePedido Where IDTransaccion=@IDTransaccion
	Open db_cursor   
	Fetch Next From db_cursor Into @vCantidad, @vIDProducto
	While @@FETCH_STATUS = 0 Begin  
		
		If not exists (Select * From vdetallePedido Where IDTransaccion<>@IDTransaccion 
													and Anulado = 'False' 
													and IDCliente = @vIDCliente 
													and Cantidad = @vCantidad 
													and IDProducto = @vIDProducto
													and IDCliente = @vIDCliente
													and IDFormaPagoFactura = @vIDFormaPagoFactura
													and IDListaPRecio = @vIDListaPrecio
													and FechaFacturar = @vFechaFacturar
													and Fecha = @vFecha
													and IDSucursal = @vIDSucursal
													and IDUsuario = @vIDUsuario
													and IDSucursalCliente = @vIDSucursalCliente) Begin
			GoTo Salir
		End

		Fetch Next From db_cursor Into @vCantidad, @vIDProducto

	End

	Close db_cursor   
	Deallocate db_cursor	
		
	Set @vRetorno = 'True'
		
Salir:
	Return @vRetorno
End


﻿CREATE FUNCTION [dbo].[ConvertirNumero]( @valor numeric(18,2))
RETURNS varchar(150)
AS
BEGIN
DECLARE 
@decenas varchar(150), 
@unidades varchar(150),  
@especial varchar(150),
@veintenas varchar(150), 
@centenas varchar(150), 
@letras varchar(150), 
@convertir varchar(150),
@dec int

SELECT @decenas = 'diez      veinte    treinta   cuarenta  cincuenta sesenta   setenta   ochenta   noventa'
SELECT @veintenas = 'veintiuno     veintidos     veintitres    veinticuatro  veinticinco   veintiseis    veintisiete   veintiocho    veintinueve   '
SELECT @unidades = 'uno       dos       tres      cuatro    cinco     seis      siete     ocho      nueve'
SELECT @especial = 'once          doce          trece         catorce       quince        dieciseis     diecisiete    dieciocho     diecinueve    '
SELECT @centenas = 'ciento          doscientos          trescientos         cuatrocientos       quinientos        seiscientos     setecientos    ochocientos     novecientos    '
SELECT @letras = CONVERT(VARCHAR,CONVERT(INT,@valor))
SELECT @convertir = ''
WHILE Len(@letras) > 0
BEGIN
IF Len(@letras) in (4, 1, 7)
BEGIN
If Substring(@letras, 1, 1) <> '0'
BEGIN
If Len(@letras) = 1 Or CONVERT(INT,Substring(@letras, 1, 1)) <> 1
SELECT @convertir = @convertir + RTrim(Substring(@unidades, CONVERT(INT,Substring(@letras, 1, 1)) * 10 - 9, 10))
If Len(@letras) = 4
BEGIN
If Substring(@letras, 1, 1) = '1'
SELECT @convertir = @convertir + ' un mil '
ELSE
SELECT @convertir = @convertir + ' mil '
END
If Len(@letras) = 7
BEGIN
If Substring(@letras, 1, 1) = '1'
SELECT @convertir = @convertir + ' un millon '
Else
SELECT @convertir = @convertir + ' millones '
END
END
END
IF Len(@letras) in (5, 2, 8)
BEGIN
If Substring(@letras, 1, 1) <> '0'
BEGIN
IF Substring(@letras, 1, 1) NOT IN ('1','2')
BEGIN
If Substring(@letras, 2, 1) = 0
BEGIN
SELECT @convertir = @convertir + RTrim(Substring(@decenas, CONVERT(INT,Substring(@letras, 1, 1)) * 10 - 9, 10))
If Len(@letras) = 5
SELECT @convertir = @convertir + ' mil '
If Len(@letras) = 8
SELECT @convertir = @convertir + ' millones '
SELECT @letras = Substring(@letras, 2, Len(@letras))
END   
Else
SELECT @convertir = @convertir + RTrim(Substring(@decenas, CONVERT(INT,Substring(@letras, 1, 1)) * 10 - 9, 10)) + ' y '
END
ELSE
BEGIN
If Substring(@letras, 1, 1) = '1'
BEGIN
If Substring(@letras, 2, 1) = 0
SELECT @convertir = @convertir + RTrim(Substring(@decenas, CONVERT(INT,Substring(@letras, 1, 1)) * 10 - 9, 10))
Else
SELECT @convertir = @convertir + RTrim(Substring(@especial, CONVERT(INT,Substring(@letras, 2, 1)) * 14 - 13, 14))
If Len(@letras) = 5
SELECT @convertir = @convertir + ' mil '
If Len(@letras) = 8
SELECT @convertir = @convertir + ' millones '
END
ELSE
BEGIN
If Substring(@letras, 2, 1) = 0
SELECT @convertir = @convertir + RTrim(Substring(@decenas, CONVERT(INT,Substring(@letras, 1, 1)) * 10 - 9, 10))
Else
SELECT @convertir = @convertir + RTrim(Substring(@veintenas, CONVERT(INT,Substring(@letras, 2, 1)) * 14 - 13, 14))
If Len(@letras) = 5
SELECT @convertir = @convertir + ' mil '
If Len(@letras) = 8
SELECT @convertir = @convertir + ' millones '
END
SELECT @letras = Substring(@letras, 2, Len(@letras))
END
END
END   
IF Len(@letras) in (6, 3, 9)
BEGIN
IF Substring(@letras, 1, 1) <> '0'
BEGIN
IF Substring(@letras, 1, 1) = '1'
BEGIN
If Substring(@letras, 2, 2) = '00'
BEGIN
IF Len(@letras) = 6
SELECT @convertir = @convertir + 'cien mil '
IF Len(@letras) = 3
SELECT @convertir = @convertir + 'cien '
IF Len(@letras) = 9
SELECT @convertir = @convertir + 'cien millones '
END
ELSE
SELECT @convertir = @convertir + 'ciento '
END
ELSE
BEGIN
	If Substring(@letras, 1, 1) = '5'
	BEGIN
		If Substring(@letras, 2, 2) = '00'
		BEGIN
		IF Len(@letras) = 6
			SELECT @convertir = @convertir + 'quinientos mil '
		IF Len(@letras) = 3
			SELECT @convertir = @convertir + 'quinientos '
		IF Len(@letras) = 9
			SELECT @convertir = @convertir + 'quinientos millones '
		END
	ELSE
		SELECT @convertir = @convertir + 'quinientos '
	END  
	
	If Substring(@letras, 1, 1) = '7'
	BEGIN
		If Substring(@letras, 2, 2) = '00'
		BEGIN
		IF Len(@letras) = 6
			SELECT @convertir = @convertir + 'setecientos mil '
		IF Len(@letras) = 3
			SELECT @convertir = @convertir + 'setecientos '
		IF Len(@letras) = 9
			SELECT @convertir = @convertir + 'setecientos millones '
		END
	ELSE
		SELECT @convertir = @convertir + 'setecientos '
	END
	
	If Substring(@letras, 1, 1) = '9'
	BEGIN
		If Substring(@letras, 2, 2) = '00'
		BEGIN
		IF Len(@letras) = 6
			SELECT @convertir = @convertir + 'novecientos mil '
		IF Len(@letras) = 3
			SELECT @convertir = @convertir + 'novecientos '
		IF Len(@letras) = 9
			SELECT @convertir = @convertir + 'novecientos millones '
		END
	ELSE
		SELECT @convertir = @convertir + 'novecientos '
	END
ELSE
BEGIN
If Substring(@letras, 2, 2) = '00'
BEGIN
IF Len(@letras) = 6
SELECT @convertir = @convertir + RTrim(Substring(@unidades, CONVERT(INT,Substring(@letras, 1, 1)) * 10 - 9, 10)) + 'cientos mil '
IF Len(@letras) = 3
SELECT @convertir = @convertir + RTrim(Substring(@unidades, CONVERT(INT,Substring(@letras, 1, 1)) * 10 - 9, 10)) + 'cientos '
IF Len(@letras) = 9
SELECT @convertir = @convertir + RTrim(Substring(@unidades, CONVERT(INT,Substring(@letras, 1, 1)) * 10 - 9, 10)) + 'cientos millones '
END
ELSE
SELECT @convertir = @convertir + RTrim(Substring(@unidades, CONVERT(INT,Substring(@letras, 1, 1)) * 10 - 9, 10)) + 'cientos '
END
END
END
END
SELECT @letras = Substring(@letras, 2, Len(@letras)) 
END
SELECT @dec = (@valor - CONVERT(Int,@valor)) * 100
If @dec > 0
SELECT @convertir = @convertir + ' con ' + RTrim(CONVERT(VARCHAR,@dec)) + '/100'
ELSE
SELECT @convertir = @convertir + ' con 00/100'
SELECT @convertir = Upper(Substring(@convertir, 1, 1)) + Substring(@convertir, 2, Len(@convertir))

set @convertir = Replace(@convertir,'CINCOCIENTOS','')
set @convertir = Replace(@convertir,'SIETECIENTOS','')
--set @convertir = Replace(@convertir,'NUEVECIENTOS','')

RETURN @convertir
END

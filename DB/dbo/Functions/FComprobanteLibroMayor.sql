﻿CREATE Function [dbo].[FComprobanteLibroMayor]
(

	--Entrada
	@ID int,
	@Form varchar(64)

)

Returns varchar(50)

As


Begin

	declare @vRetorno varchar(50)

	--If @Form = 'frmVenta' Set @vRetorno = (select Comprobante from venta where IDTransaccion = @ID)		
	If @Form = 'frmVenta' Set @vRetorno = 'VT'		
	If @Form = 'frmGastos' Set @vRetorno = (select Top(1) concat('GCO-',Suc,'-',Numero) from vGasto where IDTransaccion = @ID and CajaChica = 0)		
	If @Form = 'frmGastos' Set @vRetorno = (select Top(1) concat('GFF-',Suc,'-',Numero) from vGasto where IDTransaccion = @ID and CajaChica = 1)				
	If @Form = 'frmCobranzaContado' Set @vRetorno = (select Top(1) concat('CCO-',Suc,'-',Numero) from vCobranzaContado where IDTransaccion = @ID)		
	If @Form = 'frmCobranzaCredito' Set @vRetorno = (select Top(1) concat('CCR-',Suc,'-',Numero) from VCobranzaCredito where IDTransaccion = @ID)		
	If @Form = 'frmNotaCreditoCliente' Set @vRetorno = (select Top(1) concat('NCC-',Sucursal,'-',NroComprobante) from vNotaCredito where IDTransaccion = @ID)		
	If @Form = 'frmCompra' Set @vRetorno = (select Top(1) concat('CR-',Suc,'-',Numero) from vCompra where IDTransaccion = @ID)		
	If @Form = 'frmMovimientoStock' Set @vRetorno = (select Top(1) concat('MS-',CodigoSucursalOperacion,'-',Numero) from VMovimiento where IDTransaccion = @ID)	
	If @Form = 'frmDescargaStock' Set @vRetorno = (select Top(1) concat('DS-',CodigoSucursalOperacion,'-',Numero) from VMovimiento where IDTransaccion = @ID)	
	If @Form = 'frmDepositoBancario' Set @vRetorno = (select Top(1) concat('DB-',Suc,'-',Numero) from VDepositoBancario where IDTransaccion = @ID)	
	If @Form = 'frmEntregaChequeOP' Set @vRetorno = concat('ENT.CHQ - ',(select NumeroOP from VEntregaChequeOP where IDTransaccion = @ID))
	If @Form = 'frmOrdenPago' Set @vRetorno = concat('OP-',(select Top(1) Numero from VOrdenPago where IDTransaccion = @ID))
	If @Form = 'frmDebitoCreditoBancario' Set @vRetorno = (select Top(1) concat('DCB-',Suc,'-',Numero) from VDebitoCreditoBancario where IDTransaccion = @ID)
	If @Form = 'frmComprobanteLibroIVA' Set @vRetorno = (select Top(1) concat('CLI-',CodigoSucursal,'-',Numero) from VComprobanteLibroIVA where IDTransaccion = @ID)		
	If @Form = 'frmNotaCreditoProveedor' Set @vRetorno = (select Top(1) concat('NCP-',Suc,'-',Numero) from vNotaCreditoProveedor where IDTransaccion = @ID)		
	If @Form = 'frmRechazoChequeCliente' Set @vRetorno = (select Top(1) concat('CH.REC-',Suc,'-',Numero) from vChequeClienteRechazado where IDTransaccion = @ID)		
	If @Form = 'frmVale' Set @vRetorno = (select Top(1) concat('VALE-',Suc,'-',Numero) from vvale where IDTransaccion = @ID)		
	If @Form = 'frmCanjeChequeCliente' Set @vRetorno = (select Top(1) concat(Suc,'-',Numero) from vPagoChequeCliente where IDTransaccion = @ID)		
	
	
	
	

	Return @vRetorno

End


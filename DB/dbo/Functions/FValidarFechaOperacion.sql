﻿CREATE Function [dbo].[FValidarFechaOperacion]
(
	@IDSucursal as smallint,
	@Fecha as date,
	@IDUsuario as smallint,
	@IDOperacion as smallint
)
Returns bit
As
Begin
	--return 'True'
	

	--Si el bloqueo esta activo
	If (Select Isnull(OperacionBloquearFechaActiva, 'False') From Configuraciones Where IDSucursal=@IDSucursal) = 'False' Begin
		Return 'True'
	End

	--Si el usuario tiene perfil Admin carga igual
	If (Select IDPerfil from Usuario where id = @IDUsuario) = 1 Begin
		Return 'True'
	End

	If Exists(Select Top 1  IDSucursal From Configuraciones Where IDSucursal=@IDSucursal And @Fecha between OperacionBloquearFechaDesde And OperacionBloquearFechaHasta) Begin
		Return 'True'
	End

	If Exists(Select Top 1  IDUsuario From UsuarioOperacionFueraRangoFecha Where  IDUsuario = @IDUsuario And IDOperacion = @IDOperacion And @Fecha between Desde And Hasta) Begin
		Return 'True'
	End

	If (Select IDPerfil from Usuario where id = @IDUsuario) in (select IDPerfil from PerfilesHabilitadosFechaEspecial) Begin
		if exists (select * from RangoFechaContabilidad where @Fecha between Desde and Hasta) begin
			Return 'True'
		End
	End
	

	Return 'False'

	

End
﻿CREATE Function [dbo].[FFechaCotizacionAlDia]
(

	--Entrada
	@IDMoneda tinyint,
	@IDOperacion tinyint,
	@Formato tinyint
)

Returns varchar(50)

As

Begin

	Declare @vRetorno varchar(50)
	Set @vRetorno = ''
	
	--Si no existe ninguna cotizacion, emitir '---'
	If Not Exists(Select * From Cotizacion Where IDMoneda = @IDMoneda) Begin
		Set @vRetorno = '---'
	End
	
	--Si el IDMoneda = 0, significa que es de la moneda local, retornar solo '---'
	If @IDMoneda = 1 Begin
		Set @vRetorno = '---'
	End
	
	--En caso de que ninguna de las condiciones de arriba se cumplan
	--Buscamos la ultima cotizacion de la moneda
	If @vRetorno = '' Begin
		Set @vRetorno = Convert(varchar(50), (Select C1.Fecha From Cotizacion C1 Where C1.ID = (Select Max(C2.ID) From Cotizacion C2 Where C2.IDMoneda=@IDMoneda)), @Formato)		
	End
	
	Return @vRetorno	

End





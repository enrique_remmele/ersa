﻿CREATE Function [dbo].[FImporteProporcional]
(

	--Entrada 
	@Importe decimal(18,2),
	@Total decimal(18,2),
	@Saldo decimal(18,2)

)

Returns decimal(18,2)

As

Begin
	
	--Variables
	Declare @vRetorno decimal(18,0)
	Declare @vPorcentaje decimal(18,12)

	Set @vPorcentaje = (@Importe/@Saldo)
	Set @vRetorno = Round(@vPorcentaje * @Total,0)
	
	Return @vRetorno
	
End



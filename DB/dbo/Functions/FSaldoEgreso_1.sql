﻿CREATE Function [dbo].[FSaldoEgreso]
(
	@IDTransaccion numeric(18,0)
)

Returns money

As

Begin

	Declare @vRetorno money
	Declare @vTotal money
	Declare @vSaldo money
	Declare @vPagado money
	Declare @vPagadoSistemaAnterior money
	Declare @vDescontado money

	--Total
	Set @vTotal = (Select Top(1) Total From VEgresos Where IDTransaccion=@IDTransaccion)
	
	--Pagado
	Set @vPagado = IsNull((Select Sum(OPE.Importe) 
					From OrdenPagoEgreso OPE 
					Join OrdenPago OP On OPE.IDTransaccionOrdenPago=OP.IDTransaccion
					Where OP.Anulado='False' 
					And OPE.IDTransaccionEgreso=@IDTransaccion
					And OPE.Procesado='True'), 0)
		
	--Pagado sistema anterior
	Set @vPagadoSistemaAnterior = IsNull((Select Top(1) GI.Pagado 
											From GastoImportado GI 
											Where GI.IDTransaccion=@IDTransaccion), 0)

	Set @vDescontado = IsNull((Select Sum(NCPC.Importe)
						From NotaCreditoProveedorCompra NCPC
						Join NotaCreditoProveedor NCP On NCPC.IDTransaccionNotaCreditoProveedor=NCP.IDTransaccion
						Where NCP.Procesado='True'
						And NCP.Anulado='False'
						And NCPC.IDTransaccionEgreso=@IDTransaccion),0)

	Set @vSaldo = @vTotal - (@vPagado + @vDescontado + @vPagadoSistemaAnterior)
	set @vRetorno = @vSaldo

	Return @vRetorno

End
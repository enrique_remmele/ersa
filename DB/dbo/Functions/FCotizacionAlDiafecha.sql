﻿CREATE Function [dbo].[FCotizacionAlDiafecha]
(

	--Entrada
	@IDMoneda tinyint,
	@IDOperacion tinyint=0,
	@fecha date

)

Returns money

As

Begin

	Declare @vRetorno money
	
	Set @vRetorno = 0
	--Comento esta condicion para que, si no encuentra cotizacion en una fecha, tome la ultima cotizacion cargada anterior a esa fecha
	--Si no existe ninguna cotizacion, emitir 1
	--If Not Exists(Select * From Cotizacion Where IDMoneda = @IDMoneda and cast(fecha as date) = @fecha) Begin
	--	Set @vRetorno = 1
	--End
	
	--Si el IDMoneda = 0, significa que es de la moneda local, retornar solo 1
	If @IDMoneda = 1 Begin
		Set @vRetorno = 1		
	End
	
	--En caso de que ninguna de las condiciones de arriba se cumplan
	--Buscamos la ultima cotizacion de la moneda
	If @vRetorno = 0 Begin
		Set @vRetorno = (Select C1.Cotizacion From Cotizacion C1 Where C1.ID = (Select Max(C2.ID) From Cotizacion C2 Where C2.IDMoneda=@IDMoneda and cast(C2.fecha as date) <= cast(@fecha as date)))
	End
	
	Return @vRetorno	

End





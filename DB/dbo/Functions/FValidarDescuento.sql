﻿CREATE Function [dbo].[FValidarDescuento]
(

	--Entrada
	@IDUsuario integer,
	@IDListaPrecio integer,
	@Porcentaje decimal(18,2)
)

Returns bit

As

Begin

	--Variables
	Declare @vRetorno bit
	
	Set @vRetorno = 0
	
	--Si no se controla el porcentaje de descuentos retorna true
	if (select Top(1) ControlarPorcentajeDescuento from configuraciones) = 'False' begin
		Set @vRetorno = 1
		Goto Salir
	End 

	--Si no existe control para el usuario y la lista de precio retorna true
	if not exists (Select * from LimiteDescuentoUsuario where IDUsuario = @IDUsuario and IDListaPrecio = @IDListaPrecio)begin
		Set @vRetorno = 1
		Goto Salir
	end

	/*Si el porcentaje limite asignado a ese usuario y esa lista de precio 
	es mayor o igual al porcentaje que se desea asignar al producto retorna true*/
	if (Select Porcentaje from LimiteDescuentoUsuario where IDUsuario = @IDUsuario and IDListaPrecio = @IDListaPrecio) >= @Porcentaje begin
		Set @vRetorno = 1
		Goto Salir
	End
	
		
Salir:
	Return @vRetorno
End


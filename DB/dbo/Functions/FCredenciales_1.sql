﻿CREATE Function [dbo].[FCredenciales]
(

	--Entrada 
	@Usuario varchar(50),
	@Password varchar(50),
	@IDOperacion int,
	@Formulario varchar(50),
	@Funcion varchar(50)
)

Returns bit

As

Begin
	
	--Variables
	Declare @vRetorno bit
	Declare @vIDPerfil int
	Declare @vIDMenu int
	Declare @vIDAccesoEspecifico int
	Declare @vHabilitado bit
	
	Set @vRetorno = 'False'
	
	Set @Password = Convert(varchar(300), HASHBYTES('Sha1', @Password))
	
	--Corroborar el usuario y contraseña
	If Not Exists(Select * From Usuario Where Usuario=@Usuario And [Password]=@Password) Begin
		Set @vRetorno = 'False'
		GoTo Salir
	End
	
	--Obtener el Perfil
	Set @vIDPerfil = (Select Top(1) IDPerfil From Usuario Where Usuario=@Usuario And [Password]=@Password)
	
	--Obtenemos el IDMenu
	Set @vIDMenu = (Select Top(1) Codigo From Menu Where tag=@Formulario)
	
	--Obtener el ID de Acceso
	Set @vIDAccesoEspecifico = (Select Top(1) ID From AccesoEspecifico Where NombreFuncion=@Funcion And IDMenu=@vIDMenu)
	
	--Validar
	Set @vHabilitado  = IsNull((Select Habilitar From AccesoEspecificoPerfil Where IDPerfil=@vIDPerfil And IDAccesoEspecifico=@vIDAccesoEspecifico And IDMenu=@vIDMenu),'False')
	
	Set @vRetorno = @vHabilitado 
	 	
Salir:
	Return @vRetorno
End


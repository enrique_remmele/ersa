﻿CREATE Function [dbo].[FCostoProductoFechaKardex]
(

	--Entrada
	@IDProducto INT
	, @Fecha DATE
)

Returns money

As

Begin

	Declare @vRetorno money
	Declare @vConfiguracionCosto varchar(50)
	
	--Obtenemos la configuracion del Costo
	Set @vConfiguracionCosto = (Select IsNull((Select Top(1) MovimientoCostoProducto From Configuraciones), 'PROMEDIO'))
	
	--Establecemos el monto
	--select TOP(1)* from kardex where idproducto = 3 and CONVERT(DATE, FECHA) <= '20181022' order by indice DESC
	Set @vRetorno =isnull((Select Top(1) CostoPromedio FROM Kardex
												WHERE IDProducto = @IDProducto
												AND Fecha <= @Fecha 
												order by indice desc),0)
					
	Return @vRetorno

End





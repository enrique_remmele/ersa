﻿CREATE Function [dbo].[FExistenciaProductoFechaKardex]
(

--Entrada
	@IDProducto int,
	@Fecha date

)

Returns Decimal(18,2)

As
Begin
	
	--Variables
	Declare @vRetorno Decimal (18,2)
	
	Set @vRetorno = Isnull((Select Top(1) CantidadSaldo from Kardex
						Where IDProducto = @IDProducto 
						and Fecha <= @Fecha 
						order by Indice desc),0)

					

	Return @vRetorno 

End 


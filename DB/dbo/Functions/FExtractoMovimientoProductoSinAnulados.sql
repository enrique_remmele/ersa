﻿create Function [dbo].[FExtractoMovimientoProductoSinAnulados]
(

	--Entrada
	@IDProducto int,
	@Desde date,
	@Hasta date,
	@FechaOperacion bit = 'False'
)

Returns decimal(18,2)

As


Begin

	--Variables
	Declare @vRetorno Decimal (18,2)
	Declare @vEntrada decimal(18,2)
	Declare @vSalida decimal(18,2)
	
	If @FechaOperacion = 'False' Begin	
		Select @vEntrada = IsNull(Sum(Entrada), 0),
				@vSalida = IsNull(Sum(Salida), 0)
		From VExtractoMovimientoProductoDetalle 
		Where IDProducto=@IDProducto And Fecha < @Desde
		and Anulado = 0
	End

	If @FechaOperacion = 'True' Begin	
		Select @vEntrada = IsNull(Sum(Entrada), 0),
				@vSalida = IsNull(Sum(Salida), 0)
		From VExtractoMovimientoProductoDetalle 
		Where IDProducto=@IDProducto And FechaOperacion < @Desde
		and Anulado = 0
	End

	--Resultado
	Set @vRetorno = @vEntrada - @vSalida


	Return @vRetorno 
	
End
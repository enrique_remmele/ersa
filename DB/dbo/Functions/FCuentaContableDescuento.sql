﻿CREATE Function FCuentaContableDescuento
(

	--Entrada
	@IDProducto int,
	@IDTransaccion numeric(18,0)

)

Returns varchar(50)

As


Begin

	declare @vRetorno varchar(50)
	declare @vIDTipoProducto varchar(50)
	
	--Obtener el tipo de producto Select * From TipoProducto
	Set @vIDTipoProducto = (Select Top(1) IDTipoProducto From Producto where ID=@IDProducto)
	
	--Productos Molinos de Trigo / Sales Minerales
	If @vIDTipoProducto = 1 Or @vIDTipoProducto = 6 Begin
		Set @vRetorno = '5330601'
		GoTo Salir
	End
	
	--Productos Balanceados
	If @vIDTipoProducto = 2 Begin
		Set @vRetorno = '5330603'
		GoTo Salir
	End
	
	--Si es productos varios
	If @vIDTipoProducto = 4 Begin
		Set @vRetorno = '5330601'
		GoTo Salir
	End

Salir:
	return @vRetorno	
End
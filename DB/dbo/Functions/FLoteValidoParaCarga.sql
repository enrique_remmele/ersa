﻿CREATE Function [dbo].[FLoteValidoParaCarga]
(

	--Entrada
	@IDTransaccion numeric(18,0)
)

Returns bit

As

Begin

	Declare @vRetorno bit
	Set @vRetorno = 'True'
	
	--Vemos que el lote exista
	If Not Exists(Select * From LoteDistribucion Where IDTransaccion=@IDTransaccion) Begin
		Set @vRetorno = 'False'
	End
	
	--Vemos si el lote no esta anulado
	If (Select Anulado From LoteDistribucion Where IDTransaccion=@IDTransaccion) = 'True' Begin
		Set @vRetorno = 'False'
	End
	
	--Si ya esta en una carga y este no esta anulado
	If Exists(Select * From CargaMercaderia Where IDTransaccionLote=@IDTransaccion And Anulado='False') Begin
		Set @vRetorno = 'False'
	End
	
	--Si ya esta en una cobranza
	If Exists (Select * From CobranzaContado Where IDTransaccionLote=@IDTransaccion And Anulado='False') Begin
		Set @vRetorno = 'False'
	End
		
	Return @vRetorno

End





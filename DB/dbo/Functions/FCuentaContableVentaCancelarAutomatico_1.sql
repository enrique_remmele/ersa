﻿CREATE Function [dbo].[FCuentaContableVentaCancelarAutomatico]
(

	--Entrada
	@IDProducto int,
	@IDSucursal tinyint,
	@IDFormaPagoFactura int

)

Returns varchar(50)

As


Begin

	declare @vRetorno varchar(50)
	declare @vIDTipoProducto int

	IF @IDFormaPagoFactura = 0 Begin
	 set @IDFormaPagoFactura = (select top(1) ID from FormaPagoFactura where CancelarAutomatico = 'False')
	ENd
	
	--Si tiene configurado en la ficha de producto, usar este
	Set @vIDTipoProducto = isnull((Select Top(1) IDTipoProducto From Producto where ID=@IDProducto),0)
	Set @vRetorno = ISNull((Select CuentaContableVenta From Producto Where ID=@IDProducto), '')
	
	If @vRetorno != '' Begin
		
		--Solo si la cuenta existe en el predeterminado
		If Exists(Select * From VCuentaContable Where Codigo=@vRetorno And Imputable='True') Begin
			GoTo Salir
		End
		
	End
	
	--Obtener el tipo de producto Select * From TipoProducto
	Set @vRetorno = Isnull((Select Top(1) CuentaContableVenta From TipoProducto Where ID=@vIDTipoProducto), '')
	
Salir:
	
	--3 Donacion 
	--4 Bonificacion
	--5 Muestra
	If @IDFormaPagoFactura != 3 And @IDFormaPagoFactura != 4 And @IDFormaPagoFactura != 5 and
	@IDFormaPagoFactura != 10 And @IDFormaPagoFactura != 11 And @IDFormaPagoFactura != 12
	And @IDFormaPagoFactura != 13 Begin
		return @vRetorno	
	End
	
	If (@vIDTipoProducto = '1') begin

		-- Donacion
		If @IDFormaPagoFactura = 3 Set @vRetorno = '4112001'
					
		-- Bonificacion
		If @IDFormaPagoFactura = 4 Set @vRetorno = '4112002'

		-- Muestra
		If @IDFormaPagoFactura = 5 Set @vRetorno = '4112003'

	end
	If (@vIDTipoProducto = '2') begin

		-- Donacion
		If @IDFormaPagoFactura = 3 Set @vRetorno = '4112006'
					
		-- Bonificacion
		If @IDFormaPagoFactura = 4 Set @vRetorno = '4112007'

		-- Muestra
		If @IDFormaPagoFactura = 5 Set @vRetorno = '4112008'

		
	end

	-- Consumo interno panaderia
	If @IDFormaPagoFactura = 10 Set @vRetorno = '4112004'

	-- Consumo interno balanceado
	If @IDFormaPagoFactura = 11 Set @vRetorno = '4112009'

	-- Consumo interno aquay
	If @IDFormaPagoFactura = 12 Set @vRetorno = '4112010'

	--Muestra para prueba
	If @IDFormaPagoFactura = 13 Set @vRetorno = '4112005' 
	

	Return @vRetorno

End

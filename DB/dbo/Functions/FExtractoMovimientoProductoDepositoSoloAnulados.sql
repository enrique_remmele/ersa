﻿CREATE Function [dbo].[FExtractoMovimientoProductoDepositoSoloAnulados]
(

	--Entrada
	@IDProducto int,
	@IDDeposito int,
	@Desde date,
	@Hasta date,
	@FechaOperacion bit = 'False'
)

Returns decimal(18,2)

As


Begin

	--Variables
	Declare @vRetorno Decimal (18,2)
	Declare @vEntrada decimal(18,2)=0
	Declare @vSalida decimal(18,2)=0
	
	If @FechaOperacion = 'False' Begin	
		

		Select @vEntrada = IsNull(Sum(Entrada), 0),
				@vSalida = IsNull(Sum(Salida), 0)
		From VExtractoMovimientoAnuladoProductoDetalleDatosMinimos
		Where IDDeposito=@IDDeposito and  IDProducto=@IDProducto 
		And FechaAnulacion >= @Hasta and FechaEmision < @Hasta 
		and FechaEmision >=@Desde
		and Anulado = 1



		--Select @vEntrada = IsNull(Sum(Entrada), 0),
		--		@vSalida = IsNull(Sum(Salida), 0)
		--From VExtractoMovimientoAnuladoProductoDetalleDatosMinimos
		--Where IDDeposito=@IDDeposito and  IDProducto=@IDProducto 
		--And FechaAnulacion >= @Hasta and FechaEmision < @Hasta 
		--and Anulado = 1

		
		
		
		
		--Select @vEntrada = IsNull(Sum(Entrada), 0),
		--		@vSalida = IsNull(Sum(Salida), 0)
		--From VExtractoMovimientoProductoDetalleDatosMinimos
		--Where IDDeposito=@IDDeposito and  IDProducto=@IDProducto 
		--And FechaAnulacion >= @Desde and FechaEmision <= @Hasta 
		----and (Case when year(@Hasta)<2020 then '20200101' else FechaEmision end)>=(Case when year(@Hasta)<2020 then '20200101' else '20200101' end)
		--and (Case when year(DateADD(day,-1,@Hasta))<2020 then '20200101' else FechaEmision end)>=(Case when year(DateADD(day,-1,@Hasta))<2020 then '20200101' else '20200101' end)
		--and Anulado = 1


		--Select @vEntrada = IsNull(Sum(Entrada), 0),
		--		@vSalida = IsNull(Sum(Salida), 0)
		--From VExtractoMovimientoProductoDetalle 
		--Where IDDeposito=@IDDeposito and IDProducto=@IDProducto And Fecha >= @Desde and Fecha < @Hasta 
		--and Anulado = 1
		
		
		--Select @vEntrada = IsNull(Sum(Entrada), 0),
		--		@vSalida = IsNull(Sum(Salida), 0)
		--From VExtractoMovimientoProductoDetalleDatosMinimos 
		--Where IDDeposito=@IDDeposito and IDProducto=@IDProducto And FechaEmision < @Desde
		--and Fecha >= @Desde 
		--and Anulado = 1
	End

	--If @FechaOperacion = 'True' Begin	
	--	Select @vEntrada = IsNull(Sum(Entrada), 0),
	--			@vSalida = IsNull(Sum(Salida), 0)
	--	From VExtractoMovimientoProductoDetalleDatosMinimos 
	--	Where IDDeposito=@IDDeposito And IDProducto=@IDProducto And FechaOperacion < @Desde
	--	and Fecha >= @Desde 
	--	and Anulado = 1
	--End

	--Resultado
	Set @vRetorno = @vEntrada - @vSalida


	Return @vRetorno 
	
End

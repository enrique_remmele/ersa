﻿Create Function [dbo].[FSaldoBancario]
(

	--Entrada
	@IDCuentaBancaria int
)

Returns money

As

Begin

	Declare @vRetorno money
	Set @vRetorno = (Select 'Saldo'=(IsNull(Sum(Debito), 0)) - (IsNull(Sum(Credito), 0))  From VExtractoMovimientoBancario Where IDCuentaBancaria=@IDCuentaBancaria)
	
	Return @vRetorno

End





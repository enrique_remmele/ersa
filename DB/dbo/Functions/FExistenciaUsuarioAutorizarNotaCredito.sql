﻿
CREATE Function [dbo].[FExistenciaUsuarioAutorizarNotaCredito]
(

	--Entrada
	@IDUsuario int,
	@IDSubMotivoNotaCredito int
)

Returns bit

As


Begin

	declare @vRetorno bit = 0
	
	IF Exists(Select * from UsuarioAutorizarPedidoNotaCredito where IDUsuario = @IDUsuario and IDSubMotivo = @IDSubMotivoNotaCredito) begin
		set @vRetorno = 1
	end
	
	return @vRetorno
	
End


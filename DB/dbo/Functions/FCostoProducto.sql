﻿CREATE Function [dbo].[FCostoProducto]
(

	--Entrada
	@IDProducto int
)

Returns money

As

Begin

	Declare @vRetorno money
	Declare @vConfiguracionCosto varchar(50)
	
	--Obtenemos la configuracion del Costo
	Set @vConfiguracionCosto = (Select IsNull((Select Top(1) MovimientoCostoProducto From Configuraciones), 'PROMEDIO'))
	
	--Establecemos el monto
	
	Set @vRetorno = Case 
						When @vConfiguracionCosto = 'ULTIMO' Then
							--(Select IsNull((Select UltimoCosto From RegistroCosto Where IDProducto=@IDProducto),0))
							(Select IsNull((Select CostoSinIVA From Producto Where ID=@IDProducto),0))
						When @vConfiguracionCosto = 'PROMEDIO' Then
							--(Select IsNull((Select Promedio From RegistroCosto Where IDProducto=@IDProducto),0))
							(Select IsNull((Select CostoPromedio From Producto Where ID=@IDProducto),0))
						When @vConfiguracionCosto = '' Then 0
					End
	
	Return @vRetorno

End





﻿
CREATE Function [dbo].[FRendicionVenta]
(

	--Entrada
	@IDTransaccionVenta numeric(18,0)
)

Returns varchar(50)

As

Begin

	--Variables
	Declare @vRetorno varchar(50)
	Declare @OPRendicion as varchar(20)
	Declare @vIDTransaccionRendicion as int
	
	Set @vRetorno = '0'
	
	--Rendicion de cheque 
	Set @vIDTransaccionRendicion = ISNULL((Select IDTransaccion From DetalleRendicionLoteCheque Where IDTransaccionVenta=@IDTransaccionVenta),'0')
		If @vIDTransaccionRendicion <> '0' Begin
			
			if (Select Anulado From RendicionLote Where IDTransaccion=@vIDTransaccionRendicion) = 0 begin
				
				 Set @OPRendicion =(Select numero From RendicionLote Where IDTransaccion=@vIDTransaccionRendicion)

				set @vRetorno = @OPRendicion
				goto Salir
			end
		End
		
		--Rendicion de documentos
		Set @vIDTransaccionRendicion = ISNULL((Select IDTransaccion From DetalleRendicionLoteDocumento Where IDTransaccionVenta=@IDTransaccionVenta),'0')
		If @vIDTransaccionRendicion <> '0' Begin
			if (Select Anulado From RendicionLote Where IDTransaccion=@vIDTransaccionRendicion) = 0 begin
				
				Set @OPRendicion = (Select numero From RendicionLote Where IDTransaccion=@vIDTransaccionRendicion)

				set @vRetorno = @OPRendicion
				goto Salir
			end
		End

		--Rendicion de venta anulada
		Set @vIDTransaccionRendicion = ISNULL((Select IDTransaccion From detallerendicionloteventaanulada Where IDTransaccionVenta=@IDTransaccionVenta),'0')
		If @vIDTransaccionRendicion <> '0' Begin
			if (Select Anulado From RendicionLote Where IDTransaccion=@vIDTransaccionRendicion) = 0 begin
				
				Set @OPRendicion = (Select numero From RendicionLote Where IDTransaccion=@vIDTransaccionRendicion)

				set @vRetorno = @OPRendicion
				goto Salir
			end
		End
		
		
Salir:
	Return @vRetorno
End


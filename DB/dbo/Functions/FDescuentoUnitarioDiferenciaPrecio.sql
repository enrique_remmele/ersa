﻿CREATE Function [dbo].[FDescuentoUnitarioDiferenciaPrecio]
(

	--Entrada 
	@IDProducto int,
	@IDTransaccion int

)

Returns money

As

Begin
	
	--Variables
	Declare @vRetorno money
	Declare @DiferenciaPrecioUnitario money
				
	Set @vRetorno = 0
	
	--Obtener la lista de precio del cliente
	Set @DiferenciaPrecioUnitario = (Select isnull((select Sum(Isnull(PrecioOriginal,0) - isnull(NuevoPrecio,0)) from DetalleNotaCreditoDiferenciaPrecio where IDTransaccionVenta = @IDTransaccion and isnull(IDTransaccionNotaCredito,0) > 0  and IDProducto = @IDProducto),0))
		
	Set @vRetorno = @DiferenciaPrecioUnitario
	
	Return @vRetorno
End



﻿CREATE Function [dbo].[FCuentaContableVenta]
(

	--Entrada
	@IDProducto int,
	@IDSucursal tinyint,
	@IDFormaPagoFactura int

)

Returns varchar(50)

As


Begin

	declare @vRetorno varchar(50)
	declare @vIDTipoProducto varchar(50)
	
	--Si tiene configurado en la ficha de producto, usar este
	Set @vRetorno = ISNull((Select CuentaContableVenta From Producto Where ID=@IDProducto), '')
	
	If @vRetorno != '' Begin
		
		--Solo si la cuenta existe en el predeterminado
		If Exists(Select * From VCuentaContable Where Codigo=@vRetorno And Imputable='True') Begin
			GoTo Salir
		End
		
	End
	
	--Obtener el tipo de producto Select * From TipoProducto
	Set @vIDTipoProducto = (Select Top(1) IDTipoProducto From Producto where ID=@IDProducto)
	Set @vRetorno = Isnull((Select Top(1) CuentaContableVenta From TipoProducto Where ID=@vIDTipoProducto), '')
	
Salir:
	
	--3 Donacion 
	--4 Bonificacion
	--5 Muestra
	If @IDFormaPagoFactura != 3 And @IDFormaPagoFactura != 4 And @IDFormaPagoFactura != 5 Begin
		return @vRetorno	
	End

	--Harina
	If @vRetorno = '4110101' Begin

		-- Donacion
		If @IDFormaPagoFactura = 3 Set @vRetorno = '541010603'
				
		-- Bonificacion
		If @IDFormaPagoFactura = 4 Set @vRetorno = '541010601'

		-- Muestra
		If @IDFormaPagoFactura = 5 Set @vRetorno = '541010602'

	End
	
	--Balanceados
	If @vRetorno = '4110201' Begin

		-- Donacion
		If @IDFormaPagoFactura = 3 Set @vRetorno = '541020603'
				
		-- Bonificacion
		If @IDFormaPagoFactura = 4 Set @vRetorno = '541020601'

		-- Muestra
		If @IDFormaPagoFactura = 5 Set @vRetorno = '541020602'

	End

	Return @vRetorno

End


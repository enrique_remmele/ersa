﻿CREATE Function [dbo].[FMovimientoHabilitado]
(--Entrada
	@IDTipoComprobante smallint,
	@IDTipoOperacion int	
)

Returns bit
As
Begin--select * from VOperacionPermitida

	declare @vRetorno bit = 'False'

	if exists (select * from VOperacionPermitida where IDTipoOperacion = @IDTipoOperacion and IDTipoComprobante = @IDTipoComprobante) begin
		set @vRetorno = 'True'  
	end
	

	----select * from tipocomprobante where codigo = 'ass'
	----select * from operacion where id in (15, 59, 63, 62)
	----MOVIMIENTO DE STOCK (ENTRADA 1) (SALIDA 2) (TRANSFERENCIA 3)
	----PRODUCCION
	--If @IDTipoComprobante = 29 and @IDTipoOperacion = 1 begin 
	--	set @vRetorno = 'True'   
	--End
	----AJUSTE SALDO DE STOCK
	--If @IDTipoComprobante = 39 and (@IDTipoOperacion = 1 or @IDTipoOperacion = 2) begin 
	--	set @vRetorno = 'True'   
	--End

	----REMISION
	--If @IDTipoComprobante = 45 and (@IDTipoOperacion = 3) begin 
	--	set @vRetorno = 'True'   
	--End

	----NOTA DE CREDITO
	--If @IDTipoComprobante = 53 and (@IDTipoOperacion = 1) begin 
	--	set @vRetorno = 'True'   
	--End

	----DAñADOS INTERNOS
	--If @IDTipoComprobante = 60 and (@IDTipoOperacion = 3) begin 
	--	set @vRetorno = 'True'   
	--End

	----REPROCESO
	--If @IDTipoComprobante = 71 and (@IDTipoOperacion = 1 OR @IDTipoOperacion = 2) begin 
	--	set @vRetorno = 'True'   
	--End

	----DESTRUCCION
	--If @IDTipoComprobante = 72 and (@IDTipoOperacion = 2) begin 
	--	set @vRetorno = 'True'   
	--End

	----RECUPERO
	--If @IDTipoComprobante = 73 and (@IDTipoOperacion = 3) begin 
	--	set @vRetorno = 'True'   
	--End

	----CONVERSION
	--If @IDTipoComprobante = 74 and (@IDTipoOperacion = 1 OR @IDTipoOperacion = 2) begin 
	--	set @vRetorno = 'True'   
	--End

	----MOVIMIENTO INTERNO
	--If @IDTipoComprobante = 89 and (@IDTipoOperacion = 1 OR @IDTipoOperacion = 2) begin 
	--	set @vRetorno = 'True'   
	--End

	----PRODUCCION PANADERIA
	--If @IDTipoComprobante = 91 and (@IDTipoOperacion = 2) begin 
	--	set @vRetorno = 'True'   
	--End

	----DESCARGA POR CONSUMO
	----no tiene tipo de operacion
	
	
	----NOTA DE REQUISICION NRE (SALIDA 4) ( ENTRADA 5)
	----NOTA DE REQUISICION
	--If @IDTipoComprobante = 70 and (@IDTipoOperacion = 4 OR @IDTipoOperacion = 5) begin 
	--	set @vRetorno = 'True'   
	--End
	----DAñADO INTERNO ALMACEN
	--If @IDTipoComprobante = 90 and (@IDTipoOperacion = 12 OR @IDTipoOperacion = 4) begin 
	--	set @vRetorno = 'True'   
	--End

	----AJUSTE ASS
	--If @IDTipoComprobante = 104 and (@IDTipoOperacion = 4 OR @IDTipoOperacion = 5) begin 
	--	set @vRetorno = 'True'   
	--End

	----REMICO REMISION DE COMPRAS DE ASU A MOL
	--If @IDTipoComprobante = 105 and (@IDTipoOperacion = 12) begin 
	--	set @vRetorno = 'True'   
	--End

	----CONSUMO DE COMBUSTIBLE (SALIDA 6) (ENTRADA 7)
	----CONSUMO DE COMBUSTIBLE
	--If @IDTipoComprobante = 75 and (@IDTipoOperacion = 6) begin 
	--	set @vRetorno = 'True'   
	--End
	----RECLASIFICACION
	--If @IDTipoComprobante = 75 and (@IDTipoOperacion = 6 OR @IDTipoOperacion = 7) begin 
	--	set @vRetorno = 'True'   
	--End

	--	--select * from Operacion where ID in (15,57,59,61,62,63)
	----select ID, IDOperacion, Descripcion, Codigo from TipoComprobante where IDOperacion in (62)
	----select * from TipoOperacion where idoperacion = 62
	----MOVIMIENTO DE MATERIA PRIMA (SALIDA 9) ( ENTRADA 10) (TRANSFERENCIA 11) 
	----CONSUMO
	--If @IDTipoComprobante = 78 and (@IDTipoOperacion = 9) begin 
	--	set @vRetorno = 'True'   
	--End
	----SALIDA BALANCEADO
	--If @IDTipoComprobante = 79 and (@IDTipoOperacion = 9) begin 
	--	set @vRetorno = 'True'   
	--End
	----DAñADO INTERNO 
	--If @IDTipoComprobante = 80 and (@IDTipoOperacion = 11) begin 
	--	set @vRetorno = 'True'   
	--End
	----DESTRUCCION -B
	--If @IDTipoComprobante = 81 and (@IDTipoOperacion = 9) begin 
	--	set @vRetorno = 'True'   
	--End
	----RECUPERO -B
	--If @IDTipoComprobante = 82 and (@IDTipoOperacion = 9 OR @IDTipoOperacion = 10) begin 
	--	set @vRetorno = 'True'   
	--End
	----ENTRADA BLANCEADO
	--If @IDTipoComprobante = 83 and (@IDTipoOperacion = 10) begin 
	--	set @vRetorno = 'True'   
	--End

	----CONTR CONSUMO TRIGO
	--If @IDTipoComprobante = 95 and (@IDTipoOperacion = 9) begin 
	--	set @vRetorno = 'True'   
	--End

	----TRANS TRANSFERENCIA TRIGO
	--If @IDTipoComprobante = 96 and (@IDTipoOperacion = 11) begin 
	--	set @vRetorno = 'True'   
	--End

	----DEST DESECHO TRIGO
	--If @IDTipoComprobante = 97 and (@IDTipoOperacion = 9) begin 
	--	set @vRetorno = 'True'   
	--End

	----PROD PRODUCCION TRIGO
	--If @IDTipoComprobante = 98 and (@IDTipoOperacion = 10) begin 
	--	set @vRetorno = 'True'   
	--End

	----CONBA CONSUMO BALANCEADO
	--If @IDTipoComprobante = 99 and (@IDTipoOperacion = 9) begin 
	--	set @vRetorno = 'True'   
	--End

	----TRANSBA TRANSFERENCIA BALANCEADO
	--If @IDTipoComprobante = 100 and (@IDTipoOperacion = 11) begin 
	--	set @vRetorno = 'True'   
	--End

	----PRODBA PRODUCCION BALANCEADO
	--If @IDTipoComprobante = 101 and (@IDTipoOperacion = 10) begin 
	--	set @vRetorno = 'True'   
	--End
	----RECUP RECUPERO BALANCEADO
	--If @IDTipoComprobante = 102 and (@IDTipoOperacion = 10) begin 
	--	set @vRetorno = 'True'   
	--End

	----select * from vtipooperacion where id = 8
	----select * from vtipocomprobante where idoperacion = 63
	----MOVIMIENTO DESCARGA DE COMPRAS (SALIDA 9) ( ENTRADA 10) (TRANSFERENCIA 11) 
	----CONSUMO
	--If @IDTipoComprobante = 77 and (@IDTipoOperacion = 8) begin 
	--	set @vRetorno = 'True'   
	--End
	
	----REMICO REMISION POR COMPRAS
	--If @IDTipoComprobante = 103 and (@IDTipoOperacion = 13 or @IDTipoOperacion = 14) begin 
	--	set @vRetorno = 'True'   
	--End

	----AJUSTE SALDO DE STOCK DESCARGA DE COMPRAS
	--If @IDTipoComprobante = 107 and @IDTipoOperacion = 8 begin 
	--	set @vRetorno = 'True'   
	--End
	----select * from tipooperacion where idoperacion = 62
	----AJUSTE DE STOCK MATERIA PRIMA
	--IF @IDTipoComprobante = 111 and @IDTipoOperacion in (9,10,11) begin
	--	set @vRetorno = 'True'   
	--end

	----TRIGUILLO
	--If @IDTipoComprobante = 109 and (@IDTipoOperacion = 9) begin 
	--	set @vRetorno = 'True'   
	--End
	----DESECHOS
	--If @IDTipoComprobante = 110 and (@IDTipoOperacion = 9) begin 
	--	set @vRetorno = 'True'   
	--End
	----PRODUCCION AFRECHO
	--If @IDTipoComprobante = 112 and (@IDTipoOperacion = 10) begin 
	--	set @vRetorno = 'True'   
	--End

	return @vRetorno

End



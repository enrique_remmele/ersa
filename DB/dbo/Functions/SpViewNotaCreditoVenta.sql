﻿--execute SpViewNotaCreditoVenta 34979

CREATE Function [dbo].[SpViewNotaCreditoVenta]
(

	--Entrada 
	@IDTransaccion Numeric

)

Returns Varchar(256)

As

Begin
	
	--variables							)
	Declare @vDocumentos as varchar(256)
	Declare @Comprobante as varchar(16)							

	Declare db_cursor cursor for
	Select distinct Comprobante
	From vNotaCreditoVenta 
	where IDTransaccion = @IDTransaccion
	and Anulado = 0
	Open db_cursor   
	Fetch Next From db_cursor Into @Comprobante
	While @@FETCH_STATUS = 0 Begin 
		set @vDocumentos = CONCAT(@vDocumentos,'  ',@Comprobante)
Siguiente:
		
		Fetch Next From db_cursor Into	@Comprobante
		
	End

	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor	
	
	Return @vDocumentos
	
End

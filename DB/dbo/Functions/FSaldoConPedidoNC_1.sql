﻿CREATE Function [dbo].[FSaldoConPedidoNC]
(

	--Entrada
	@IDTransaccion int
	
)

Returns varchar(50)

As


Begin

	declare @vRetorno decimal
	declare @vSumaPedidoNC decimal
	Set @vSumaPedidoNC = ISNULL((Select Sum(Importe) from VPedidoNotaCreditoVenta where ProcesadoNC = 'PENDIENTE' and IDTransaccionVenta = @IDTransaccion),0)

	Set @vRetorno = ISNULL((Select Saldo From Venta where IDTransaccion = @IDTransaccion),0) - @vSumaPedidoNC
	
	
	return @vRetorno
	
End



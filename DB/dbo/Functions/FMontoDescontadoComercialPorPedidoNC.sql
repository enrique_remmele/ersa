﻿
CREATE FUNCTION [dbo].[FMontoDescontadoComercialPorPedidoNC]
(
	@IDTransaccion int
)
RETURNS money
AS
BEGIN
	Declare @vIDSubmotivo as int 
	Declare @vTipoSubMotivo as varchar(50)
	Declare @vTotalRetorno as money = 0
	
	Set @VIDSubMotivo = (select IDSubMotivoNotaCredito from PedidoNotaCredito where IDTransaccion = @IDTransaccion)
	
	Set @vTipoSubMotivo = (Select (Case When DiferenciaPrecio = 1 then 'DIFERENCIAPRECIO' else
									(Case when DiferenciaPeso = 1 then 'DIFERENCIAPESO' else
									(Case when AcuerdoComercial = 1 then 'ACUERDO' else 
									(Case when Financiero = 1 then 'FINANCIERO' else
									(Case When Anulacion = 1 then 'ANULACION' else
									'DEVOLUCION' END)END)END)END)END) from SubMotivoNotaCredito where id =@vIDSubMotivo)
	
	if @vTipoSubMotivo = 'DIFERENCIAPRECIO' begin
		Set @vTotalRetorno = (Select sum((PrecioOriginal-NuevoPrecio)*Cantidad) from DetalleNotaCreditoDiferenciaPrecio where IDTransaccionPedidoNotaCredito = @IDTransaccion)
	end

	if @vTipoSubMotivo = 'DIFERENCIAPESO' begin
		Set @vTotalRetorno = (Select Sum(ImporteDescuento) from DetalleNotaCreditoDiferenciaPeso where IDTransaccionPedidoNotaCredito = @IDTransaccion)
	end

	if @vTipoSubMotivo = 'ACUERDO' begin
		--Set @vTotalRetorno = (Select sum(TotalDescuentoProducto) from vDetalleNotaCreditoAcuerdo where IDTransaccionPedidoNotaCredito = @IDTransaccion)
		Set @vTotalRetorno = (Select Total from vPedidoNotaCredito where IDTransaccion = @IDTransaccion)
	end
	
	if @vTipoSubMotivo = 'FINANCIERO' begin
		Set @vTotalRetorno = 0
	end
	
	if @vTipoSubMotivo = 'ANULACION' begin
		Set @vTotalRetorno = 0
	end

	if @vTipoSubMotivo = 'DEVOLUCION' begin
		Set @vTotalRetorno = 0
	end

	return @vTotalRetorno




END

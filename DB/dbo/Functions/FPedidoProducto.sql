﻿CREATE Function [dbo].[FPedidoProducto]
(

	--Entrada
	@IDProducto int,
	@IDSucursal tinyint,
	@Desde date,
	@Hasta Date
)

Returns decimal(10,2)

As


Begin

	declare @vRetorno decimal(10,2)
	declare @vExistenciaReal decimal(10,2)
	declare @vReservado decimal(10,2)

	
	Set @vRetorno = IsNull((select Sum(DP.Cantidad) from vdetallePedido DP
	Join Pedido P on P.IDTransaccion = DP.IDTransaccion 
	where P.Anulado = 'False'
	and P.Facturado = 'False'
	and DP.IDProducto = @IDProducto
	and P.IDSucursal = @IDSucursal
	and P.FechaFacturar between @Desde and @Hasta),0)
	
	return @vRetorno
	
End


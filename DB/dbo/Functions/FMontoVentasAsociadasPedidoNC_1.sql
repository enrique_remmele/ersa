﻿
CREATE FUNCTION [dbo].[FMontoVentasAsociadasPedidoNC]
(
	@IDTransaccion int
)
RETURNS money
AS
BEGIN
	Declare @vMotivo as varchar(50)
	Declare @vTotalRetorno as money = 0
	Declare @vDesdePedidoVenta as bit
	
	Set @vDesdePedidoVenta = (select DesdePedidoVenta from PedidoNotaCredito where IDTransaccion = @IDTransaccion)
	
	Set @vMotivo = (Select (Case When Descuento = 1 then 'DESCUENTO' else 'DEVOLUCION' END) from PedidoNotaCredito where IDTransaccion =@IDTransaccion)
	
	if @vDesdePedidoVenta = 1 begin
		Set @vTotalRetorno = (Select sum(TotalVenta) from vPedidoNotaCreditoPedidoVenta where IDTransaccionPedidoNotaCredito = @IDTransaccion)
	end
	else begin

		--if @vMotivo = 'DESCUENTO' begin
			Set @vTotalRetorno = (Select sum(total) from venta where idtransaccion in (SELECT distinct idtransaccionventagenerada FROM PedidoNotaCreditoVenta where IDTransaccionPedidoNotaCredito= @IDTransaccion))
		--end

		--if @vMotivo = 'DEVOLUCION' begin
		--	Set @vTotalRetorno = (Select sum(total) from venta where idtransaccion in (SELECT distinct idtransaccionventagenerada FROM PedidoNotaCreditoVenta where IDTransaccionPedidoNotaCredito= @IDTransaccion))
		--end
	end

	return @vTotalRetorno




END

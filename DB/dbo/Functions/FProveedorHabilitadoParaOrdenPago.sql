﻿CREATE Function [dbo].[FProveedorHabilitadoParaOrdenPago]
(

	--Entrada
	@ID int,
	@IDSucursal tinyint
)

Returns bit

As

Begin

	Declare @vRetorno bit
	Set @vRetorno = 'False'
	
	--Si es que el Proveedor tiene compras pendientes
	If Exists(Select * From Compra Where IDProveedor=@ID And Cancelado='False' And IDSucursal=@IDSucursal) Begin
		Set @vRetorno = 'True'
	End
	
	--Si es que el Proveedor tiene gastos pendientes
	If @vRetorno = 'False' And Exists(Select * From Gasto Where IDProveedor=@ID And Cancelado='False' And IDSucursal=@IDSucursal) Begin
		Set @vRetorno = 'True'
	End
		
	Return @vRetorno

End





﻿CREATE Function [dbo].[FSaldoTotalAnticipos]
(

	--Entrada
	@IDCliente tinyint = 0
	
)

Returns money

As

Begin

	Declare @vRetorno money
	
	if @IDCliente = 0 begin
		Set @vRetorno = (Select C.Total - Isnull((Select Sum(VC.ImporteReal) from VVentaDetalleCobranza VC where VC.IDTransaccionCobranza = C.IDTransaccion),0)
			From CobranzaCredito C
			WHERE C.Anulado='False'
			and C.AnticipoCliente = 'True'
			and C.Total > ISNULL((Select Sum(VC.ImporteReal) from VVentaDetalleCobranza VC where VC.IDTransaccionCobranza = C.IDTransaccion),0))
	end

	if @IDCliente > 0 begin
		Set @vRetorno = (Select C.Total - Isnull((Select Sum(VC.ImporteReal) from VVentaDetalleCobranza VC where VC.IDTransaccionCobranza = C.IDTransaccion),0)
			From CobranzaCredito C
			WHERE C.Anulado='False'
			and C.AnticipoCliente = 'True'
			and C.Total > ISNULL((Select Sum(VC.ImporteReal) from VVentaDetalleCobranza VC where VC.IDTransaccionCobranza = C.IDTransaccion),0)
			and C.IDCliente= @IDCliente)
	End
	Return @vRetorno	

End





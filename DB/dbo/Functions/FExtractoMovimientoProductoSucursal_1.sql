﻿CREATE Function [dbo].[FExtractoMovimientoProductoSucursal]
(
	--Entrada
	@IDProducto int,
	@IDSucursal int,
	@Desde date,
	@Hasta date,
	@FechaOperacion bit = 'False'
)
Returns Decimal (10,2)

As

Begin

	--Variables
	Declare @vRetorno Decimal (10,2)
	Declare @vEntrada decimal(10,2)
	Declare @vSalida decimal(10,2)
	
	If @FechaOperacion = 'False' Begin	
		Select @vEntrada = IsNull(Sum(Entrada), 0),
				@vSalida = IsNull(Sum(Salida), 0)
		From VExtractoMovimientoProductoDetalle 
		Where IDSucursal=@IDSucursal And IDProducto=@IDProducto And Fecha < @Desde
	End

	If @FechaOperacion = 'True' Begin	
		Select @vEntrada = IsNull(Sum(Entrada), 0),
				@vSalida = IsNull(Sum(Salida), 0)
		From VExtractoMovimientoProductoDetalle 
		Where IDSucursal=@IDSucursal And IDProducto=@IDProducto And FechaOperacion < @Desde
	End

	--Resultado
	Set @vRetorno = @vEntrada - @vSalida


	Return @vRetorno 

End



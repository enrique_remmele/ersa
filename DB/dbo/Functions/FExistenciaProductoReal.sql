﻿CREATE Function [dbo].[FExistenciaProductoReal]
(

	--Entrada
	@IDProducto int,
	@IDDeposito tinyint
)

Returns decimal(10,2)

As


Begin

	declare @vRetorno decimal(10,2)
	declare @vExistenciaReal decimal(10,2)
	Set @vExistenciaReal = (IsNull((Select Existencia From ExistenciaDeposito Where IDDeposito=@IDDeposito And IDProducto=@IDProducto),0))
	Set @vRetorno = @vExistenciaReal 
	return @vRetorno
	
End


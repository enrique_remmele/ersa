﻿CREATE Function [dbo].[FExistenciaProductoValida]
(

	--Entrada
	@IDProducto int,
	@IDDeposito tinyint,
	@Cantidad decimal(10, 2)
)

Returns bit

As


Begin

	--Variables
	declare @vRetorno bit
	declare @vExistencia decimal(10,2)
	
	--Obtener valores
	Set @vRetorno = 'True'
	Set @vExistencia = (Select IsNull((Select Existencia From ExistenciaDeposito Where IDDeposito=@IDDeposito And IDProducto=@IDProducto),0))
	
	--Validar
	If @vExistencia < @Cantidad Begin
		Set @vRetorno = 'False'
	End
	
	return @vRetorno
	
End



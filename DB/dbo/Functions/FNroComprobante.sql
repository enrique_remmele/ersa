﻿CREATE Function [dbo].[FNroComprobante]
(

	--Entrada
	@IDPuntoExpedicion int
)

Returns numeric(18,0)

As

Begin

	--Variables
	Declare @vRetorno numeric(18, 0)
	Declare @vUltimoNumero numeric(18, 0)
	Declare @vNumeracionDesde numeric(18,0)
	
	--Hayar el ultimo numero
	Set @vUltimoNumero = IsNull((Select PE.UltimoNumero From PuntoExpedicion PE Where PE.ID=@IDPuntoExpedicion), 0)
	Set @vNumeracionDesde = (Select PE.NumeracionDesde From PuntoExpedicion PE Where PE.ID=@IDPuntoExpedicion)
	
	--Controlar 
	If @vUltimoNumero < @vNumeracionDesde Begin
		Set @vUltimoNumero = @vNumeracionDesde
	End Else Begin
		Set @vUltimoNumero = @vUltimoNumero + 1
	End
	
	Set @vRetorno = @vUltimoNumero
		
	Return @vRetorno
End


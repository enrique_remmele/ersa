﻿
CREATE Function [dbo].[FNotaCreditoPorVenta]
(

	--Entrada
	@IDTransaccion int
	
)

Returns TABLE 

As
 
return(
select 'CantidadNC'=(select count(*) from NotaCreditoVenta where IDTransaccionVentaGenerada = @IDTransaccion), 
'CantidadPedidoNCPendientes' = (select count(*) from vPedidoNotaCreditoVenta where ProcesadoNC = 'PENDIENTE' And [Estado NC] in ('APROBADO', 'PENDIENTE') and IDTransaccionVenta = @IDTransaccion), 
'CantidadNCDiferenciaPrecio' = (select count(*) from vPedidoNotaCreditoVenta where ProcesadoNC = 'PROCESADO' and IDTransaccionVenta = @IDTransaccion and Tipo = 'Diferencia de Precio'),
'CantidadPedidoNCDiferenciaPrecio'=(select count(*) from vPedidoNotaCreditoVenta where ProcesadoNC = 'PENDIENTE' and IDTransaccionVenta = @IDTransaccion And [Estado NC] in ('APROBADO', 'PENDIENTE') and Tipo = 'Diferencia de Precio')
);

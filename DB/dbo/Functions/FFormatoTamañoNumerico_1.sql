﻿CREATE function [dbo].[FFormatoTamañoNumerico]
(
	@Tamaño varchar (50),
	@Texto varchar (50)
	
)

Returns varchar (50)

As

Begin
	
	Declare @vRetorno varchar (50)
	Declare @vRetorno2 varchar (50)
	Declare @vCantidad int
	Declare @vCantidad2 int
	Declare @vComa int
	Declare @vSignoNegativo int
	Declare @vNumeroAntesdelaComa int
	Declare @vNroDespuesdelaComa int
	Declare @vTamaño2 varchar (50)
	Declare @vTexto2 int 
	Declare @vI int
	Declare @vI2 int
	
	Declare @vNegativo bit
	Set @vNegativo = 'False'
	
	If (Select charindex ('-', @Texto )) != 0 Begin
		Set @vNegativo = 'True'
		Set @Texto = Replace(@Texto, '-', '')
	End
	
	Set @vComa = (Select charindex ('.',@Tamaño ))
	Set @vTamaño2 = (Select SUBSTRING (@Tamaño,charindex (',',@Tamaño,0)+1, LEN (@Tamaño)))
	
	
	--SI ES EN DECIMALES
	If (Select charindex ('.',@Tamaño )) != 0 Begin
		
		Declare @vEnteroTamaño int		
		Declare @vDecimalTamaño int			
		
		Set @vDecimalTamaño = Convert(int, SubString(@Tamaño, charindex ('.', @Tamaño,0) + 1, Len(@Tamaño)))
		Set @vEnteroTamaño = Convert(int, SubString(@Tamaño, 1, charindex ('.', @Tamaño,0) -1))
		
		--Parte Entera
		Begin
			
			Set @vEnteroTamaño = @vEnteroTamaño - @vDecimalTamaño
				
			Declare @vEnteroTexto varchar(50)
			
			If (Select charindex ('.', @Texto)) != 0 Begin
				Set @vEnteroTexto = SubString(@Texto, 1, charindex ('.', @Texto,0) -1)
			End Else Begin
				Set @vEnteroTexto = @Texto
			End
			
			Set @vRetorno = ''
			Set @vCantidad = Convert(int, @vEnteroTamaño) - Len(@vEnteroTexto)
			Set @vI = 0
			
			While @vI < @vCantidad Begin
				Set @vRetorno = @vRetorno + '0'
				Set @vI = @vI + 1
			End
			
			Set @vRetorno = @vRetorno + @vEnteroTexto
			
		End
		
		--Parte Decimal
		Begin
					
			Declare @vDecimalTexto varchar(50)					
						
			If (Select charindex ('.', @Texto)) != 0 Begin
				Set @vDecimalTexto = SubString(@Texto, charindex ('.', @Texto,0) +1, Len(@Texto))
			End Else Begin
				Set @vDecimalTexto = @Texto
			End
			
			Set @vCantidad = Convert(int, @vDecimalTamaño) - Len(@vDecimalTexto)
			Set @vI = 0
			
			Set @vRetorno = @vRetorno + @vDecimalTexto  
			
			While @vI < @vCantidad Begin
				Set @vRetorno = @vRetorno + '0'
				Set @vI = @vI + 1
			End
			
		End	
		
		
	End
	
	--SI ES ENTERO
	If (Select charindex ('.',@Tamaño )) = 0 Begin
		
		Set @vRetorno = ''
		Set @vCantidad = Convert(int, @Tamaño) - Len(@Texto)
		Set @vI = 0
		
		While @vI < @vCantidad Begin
			Set @vRetorno = @vRetorno + '0'
			Set @vI = @vI + 1
		End
		
		Set @vRetorno = @vRetorno + @Texto  
		
	End
	
	--SI ES NEGATIVO
	If @vNegativo = 'True' Begin
		Set @vRetorno = '-' + SubString(@vRetorno, 2, len(@vRetorno))		
	End
	

	return @vRetorno
	
End





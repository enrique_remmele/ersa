﻿CREATE Function [dbo].[FExtractoMovimientoProductoDepositoSinAnulados]
(

	--Entrada
	@IDProducto int,
	@IDDeposito int,
	@Desde date,
	@Hasta date,
	@FechaOperacion bit = 'False'
)

Returns decimal(18,2)

As


Begin

	--Variables
	Declare @vRetorno Decimal (18,2)
	Declare @vEntrada decimal(18,2)
	Declare @vSalida decimal(18,2)
	
	If @FechaOperacion = 'False' Begin	
		Select @vEntrada = IsNull(Sum(Entrada), 0),
				@vSalida = IsNull(Sum(Salida), 0)
		--From VExtractoMovimientoProductoDetalle 
		From VExtractoMovimientoProductoDetalleDatosMinimos
		Where IDDeposito=@IDDeposito And IDProducto=@IDProducto And Fecha < @Desde
		And isnull(Anulado,0) = 0
	End

	If @FechaOperacion = 'True' Begin	
		Select @vEntrada = IsNull(Sum(Entrada), 0),
				@vSalida = IsNull(Sum(Salida), 0)
		--From VExtractoMovimientoProductoDetalle 
		from VExtractoMovimientoProductoDetalleDatosMinimos
		Where IDDeposito=@IDDeposito And IDProducto=@IDProducto And FechaOperacion < @Desde
		And isnull(Anulado,0) = 0
	End

	--Resultado
	Set @vRetorno = @vEntrada - @vSalida


	Return @vRetorno 
	
End

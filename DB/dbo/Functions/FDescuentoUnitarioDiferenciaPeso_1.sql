﻿CREATE Function [dbo].[FDescuentoUnitarioDiferenciaPeso]
(

	--Entrada 
	@IDProducto int,
	@IDTransaccion int

)

Returns money

As

Begin
	
	--Variables
	Declare @vRetorno money
	Declare @DiferenciaPesoUnitario money
				
	Set @vRetorno = 0
	
	--Obtener la lista de precio del cliente
	Set @DiferenciaPesoUnitario = (Select isnull((select Sum(Isnull(ImporteDescuentoUnitario,0)) from DetalleNotaCreditoDiferenciaPeso where IDTransaccionVenta = @IDTransaccion and Isnull(IDTransaccionNotaCredito,0)>0 and IDProducto = @IDProducto),0))
		
	Set @vRetorno = @DiferenciaPesoUnitario
	
	Return @vRetorno
End



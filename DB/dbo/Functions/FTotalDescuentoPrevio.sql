﻿CREATE FUNCTION [dbo].[FTotalDescuentoPrevio]
(
	@IDTransaccionPedidoNotaCredito int
)
RETURNS decimal
AS
BEGIN
	Declare @vRetorno as decimal = 0
	Declare @vDesdePedidoVenta as bit = 0
	Declare @vTotalDescuentoPedidoVenta as decimal = 0
	Declare @vTotalDescuentoVenta as decimal = 0
	Declare @vTotalNCVenta as decimal = 0
	Declare @vIDTransaccionVentaRelacionada as integer = 0
	
	Set @vDesdePedidoVenta = (select isnull(DesdePedidoVenta,0) from PedidoNotaCredito where IDTransaccion = @IDTransaccionPedidoNotaCredito)
	
	if @vDesdePedidoVenta = 1 begin

		Set @vTotalDescuentoPedidoVenta = (Select 
									sum(p.totaldescuento)
									from PedidoNotaCreditoPedidoVenta pnc 
									join Pedido p on pnc.IDTransaccionPedidoVenta = p.IDTransaccion
									where p.Anulado = 0
									and p.totaldescuento > 0
									and pnc.IDTransaccionPedidoNotaCredito = @IDTransaccionPedidoNotaCredito)

	end
	else begin
		
		Set @vTotalDescuentoVenta = (select 
									Sum(V.TotalDescuento)
									from PedidoNotaCreditoVenta pnv
									join Venta V on pnv.IDTransaccionVentaGenerada = V.IDTransaccion
									where V.Procesado = 1
									and V.Anulado = 0 
									and V.TotalDescuento >0
									and pnv.IDTransaccionPedidoNotaCredito = @IDTransaccionPedidoNotaCredito)
		
		Set @vTotalNCVenta = (select 
								Sum(NCV.Importe)
								from PedidoNotaCreditoVenta pnv
								join Venta V on pnv.IDTransaccionVentaGenerada = v.IDTransaccion
								join NotaCreditoVenta NCV on V.IDTransaccion = NCV.IDTransaccionVentaGenerada
								where V.Procesado = 1
								and V.Anulado = 0 
								and pnv.IDTransaccionPedidoNotaCredito = @IDTransaccionPedidoNotaCredito)
		
	end
	
	Set @vRetorno = Isnull(@vTotalDescuentoPedidoVenta,0) + Isnull(@vTotalDescuentoVenta,0) + isnull(@vTotalNCVenta,0)
	
	return @vRetorno




END

﻿CREATE Function [dbo].[FEjecutarOperacionAutomatica]
(

	--Entrada
	@ID int
)

Returns bit

As



Begin

	declare @vRetorno bit = 1
	declare @vSiguienteEjecucion date = (select [dbo].FSiguienteEjecucion(@ID))
	
	if exists(select * from vOperacionesAutomaticas where isnull(Ejecutando,0) = 1 and ID=@ID) begin
		Set @vRetorno = 0
		goto salir
	end

	if exists(select * from OperacionAutomaticaRealizada where ID=@ID and FechaInicio>=@vSiguienteEjecucion) begin
		Set @vRetorno = 0
		goto salir
	end

salir:

	return @vRetorno
	
End


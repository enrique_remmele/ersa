﻿
CREATE Function [dbo].[FAprobarVentaSinPedido]
(

	--Entrada
	@IDSucursal int,
	@IDFormaPago int
	
)

Returns bit

As


Begin

	if (select FacturacionSinPedido from FormaPagoFactura where ID = @IDFormaPago) = 'True' begin
		return 1
	end
	
	if (Select VentaSinPedido from Sucursal where ID = @IDSucursal) = 'True' begin
		return 1
	end
		
	
	return 0
	
End




﻿CREATE Function [dbo].[FValidarVenta]
(

	--Entrada
	@IDTransaccion numeric(18,0)
)

Returns varchar(100)

As

Begin

	--Variables
	Declare @vRetorno varchar(100)
	
	Set @vRetorno = '---'
	
	--Si no se proceso
	If (Select Top(1) Procesado From Venta Where IDTransaccion=@IDTransaccion) = 'False' Begin
		Set @vRetorno = 'No se proceso correctamente'
		GoTo Salir
	End
	
	--Si Tiene Saldo 0 y No esta Cancelado
	If Exists (Select Top(1) IDTransaccion From Venta Where IDTransaccion=@IDTransaccion And Saldo=0 And Cancelado='False') Begin
		Set @vRetorno = 'No cancelado, saldo=0'
		GoTo Salir		
	End
	
	--Si no tiene Saldo y esta Cancelado
	If Exists (Select Top(1) IDTransaccion From Venta Where IDTransaccion=@IDTransaccion And (Saldo>1 Or Saldo<-1) And Cancelado='True') Begin
		Set @vRetorno = 'Cancelado pero con saldo'
		GoTo Salir
	End
	--Si el saldo no es igual al calculado
	Declare @vSaldoCalculado money
	Set @vSaldoCalculado = dbo.FVentaCalcularSaldo(@IDTransaccion)
	
	If Exists (Select Top(1) IDTransaccion From Venta Where IDTransaccion=@IDTransaccion And Saldo<>@vSaldoCalculado) Begin
		
		Declare @vSaldoSistema money
		set @vSaldoSistema = (Select Top(1) Saldo From Venta Where IDTransaccion=@IDTransaccion)

		Set @vRetorno = 'El saldo no es correcto! Saldo calculado=' + convert(varchar(20), @vSaldoCalculado) + ' - Saldo sistema=' + convert(varchar(20), @vSaldoSistema)  + ''
		GoTo Salir

	End

		
Salir:
	Return @vRetorno
End


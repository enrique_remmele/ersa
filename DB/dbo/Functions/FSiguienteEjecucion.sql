﻿CREATE Function [dbo].[FSiguienteEjecucion]
(

	--Entrada
	@ID int
)

Returns date

As

Begin

	Declare @vRecurrencia varchar(50)
	Declare @vDiaSemana int
	Declare @vDiaMes int
	Declare @vFechaUnica date
	Declare @vRetorno date
	--Establecemos el monto select * from operacionautomatica
	Select @vRecurrencia = Recurrencia, @vDiaSemana = DiaSemana, @vDiaMes = DiaMes, @vFechaUnica=FechaOperacion from OperacionAutomatica where id = @ID
	If @vRecurrencia = 'UNICA VEZ' begin
		Set @vRetorno = @vFechaUnica
		goto Salir
	end
	
	If @vRecurrencia = 'DIARIO' begin
		Set @vRetorno = Convert(date,GetDate())
	end
	
	If @vRecurrencia = 'SEMANAL' begin
		Set @vRetorno = Convert(date,(select convert(date,dateadd(dd,(7 - datepart(dw,GETDATE()) + @vDiaSemana) % 7, getdate()))))
		goto Salir
	end

	If @vRecurrencia = 'MENSUAL' begin
		if Day(GetDate())> @vDiaMes begin
			Set @vRetorno = Convert(date,Concat(LEFT(CONVERT(varchar, DATEADD(month,1,GetDate()),112),6),@vDiaMes))
		end
		else begin
			Set @vRetorno = Convert(date,Concat(LEFT(CONVERT(varchar, GetDate(),112),6),@vDiaMes))
		end
	end
	
	Salir:
	Return @vRetorno

End





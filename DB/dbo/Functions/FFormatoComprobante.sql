﻿CREATE Function FFormatoComprobante
(

	@Comprobante varchar(20)

)

Returns varchar(20)

As

Begin

	Declare @vRetorno varchar(20)
	Declare @vSucursal varchar(5) = '001'
	Declare @vPuntoExpedicion varchar(5) = '001'
	Declare @vNroComprobante varchar(5) = ''
	Declare @vResto varchar(20)

	Set @vResto = @Comprobante

	--Primera parte
	If CharIndex('-', @vResto, 0) = 4 Begin
		Set @vSucursal = SubString(@vResto, 0, 4)
		Set @vResto = SubString(@vResto, 5, Len(@vResto))
	End Else Begin
		Set @vRetorno = @Comprobante
		GoTo Salir
	End
	
	--Segunda parte
	If CharIndex('-', @vResto, 0) = 4 Begin
		Set @vPuntoExpedicion = SubString(@vResto, 0, 4)
		Set @vResto = SubString(@vResto, 5, Len(@vResto))
	End Else Begin
		Set @vRetorno = @Comprobante
		GoTo Salir
	End

	--Quitar 0 a la izquirda
	If IsNumeric(@vResto) = 0 Begin
		Set @vRetorno = @Comprobante
		GoTo Salir
	End

	Set @vNroComprobante = Convert(varchar(15), Convert(int, @vResto))

	Set @vRetorno = @vSucursal + '-' + @vPuntoExpedicion + '-' + @vNroComprobante

Salir:
	Return @vRetorno
End

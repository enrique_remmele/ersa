﻿CREATE Function [dbo].[FVerificarCantidadExcepciones]
(
	--Entrada
	@Fecha Date,
	@IDSucursal int,
	@IDListaPrecio int,
	@IDProducto int,
	@IDCliente int,	
	@Cantidad decimal(18,3)
)

Returns TABLE 

As
 
 --select * from ProductoListaPrecioExcepciones where idcliente = 2049
 --select * from cliente where Referencia = '01sst'

return(

select top(1)
'TipoExcepcion'='TACTICO',
'Mensaje' = concat('El saldo en cantidades para el descuento del producto es de ', isnull(PLPE.CantidadLimite,0) - isnull(PLPE.CantidadLimiteSaldo,0), '. Si desea el descuento no sobrepase la cantidad.')
from vProductoListaPrecioExcepciones PLPE
--Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
Where PLPE.IDProducto=@IDProducto 
And PLPE.ListaPrecio=(Select top(1) descripcion from ListaPrecio where ID = @IDListaPrecio)
And PLPE.IDCliente=@IDCliente
And @Fecha between PLPE.Desde And PLPE.Hasta
And TD.Descripcion = 'TACTICO'
And (isnull(PLPE.CantidadLimite,0) - Isnull(PLPE.CantidadLimiteSaldo,0)) >0
And (isnull(PLPE.CantidadLimite,0) - Isnull(PLPE.CantidadLimiteSaldo,0)) < @Cantidad
And (Case When isnull(PLPE.CantidadLimite,0)=0 then 0 else PLPE.CantidadLimite end)>(Case When Isnull(PLPE.CantidadLimite,0)=0 then 0 else PLPE.CantidadLimiteSaldo end) 
And Isnull(PLPE.IDTransaccionPedido,0)=0
union all

select top(1)
'TipoExcepcion'='Nota de Credido por Diferencia de Credito',
'Mensaje' = concat('El saldo en cantidades para la NC por Diferencia de Precio del producto es de ',PLPE.CantidadLimite - PLPE.CantidadLimiteSaldo, '. Si desea la NC no sobrepase la cantidad.')
from vProductoListaPrecioExcepciones PLPE
--Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
Where PLPE.IDProducto=@IDProducto 
And PLPE.ListaPrecio=(Select top(1) descripcion from ListaPrecio where ID = @IDListaPrecio)
And PLPE.IDCliente=@IDCliente
And @Fecha between PLPE.Desde And PLPE.Hasta
And TD.Descripcion = 'DIFERENCIA PRECIO'
And (isnull(PLPE.CantidadLimite,0) - Isnull(PLPE.CantidadLimiteSaldo,0)) < @Cantidad
And (Case When isnull(PLPE.CantidadLimite,0)=0 then 0 else PLPE.CantidadLimite end)>(Case When Isnull(PLPE.CantidadLimite,0)=0 then 0 else PLPE.CantidadLimiteSaldo end) 
And Isnull(PLPE.IDTransaccionPedido,0)=0



);

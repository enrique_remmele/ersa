﻿
CREATE Function [dbo].[FVentaHabilitadaParaLote]
(

	--Entrada
	@IDTransaccion numeric(18,0)
)

Returns bit

As

Begin

	Declare @vRetorno bit
	Set @vRetorno = 'True'
	
	--Vemos que la venta exista
	If Not Exists(Select TOP 1 * From Venta Where IDTransaccion=@IDTransaccion) Begin
		Set @vRetorno = 'False'
	End
	
	--Vemos si la venta no esta anulada
	If (Select TOP 1 Anulado From Venta Where IDTransaccion=@IDTransaccion) = 'True' Begin
		Set @vRetorno = 'False'
	End
	
	--Si ya esta en un lote venta
	If Exists(Select TOP 1 * From VentaLoteDistribucion Where IDTransaccionVenta=@IDTransaccion) Begin
		Set @vRetorno = 'False'
	END
	
	--Si ya esta despachado
	If (Select TOP 1 Despachado From Venta Where IDTransaccion=@IDTransaccion) = 'True' Begin
		Set @vRetorno = 'False'
	End

	--Si esta procesada
	If (Select TOP 1 Procesado From Venta Where IDTransaccion=@IDTransaccion) = 'False' Begin
		Set @vRetorno = 'False'
	End


	--Si tiene detalle
	If Not Exists(Select TOP 1 * From DetalleVenta Where IDTransaccion=@IDTransaccion) Begin
		Set @vRetorno = 'False'
	END

	----JGR 20140822 Comentando aqui y agregando este control de limite de fechas en VVentasParaLote.
	--If Not Exists(Select TOP 1 * From Venta Where IDTransaccion=@IDTransaccion And DATEDIFF(dd, GetDate(), FechaEmision) Between -10 And 2) Begin
	--	Set @vRetorno = 'False'
	--End
		
	Return @vRetorno

End






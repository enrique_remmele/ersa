﻿CREATE Procedure [dbo].[SpCiudadInsUpd]

	--Entrada
	@Descripcion varchar(100),
	@IDPais int,
	@IDDepartamento int
	
As

Begin

	Declare @vID int
	
	If @Descripcion = '' Begin
		Set @Descripcion = 'ASUNCION'
	End
	
	--Si existe
	If Exists(Select * From Ciudad Where Descripcion = @Descripcion And IDPais=@IDPais And IDDepartamento=@IDDepartamento) Begin
		Set @vID = (Select Top(1) ID From Ciudad Where Descripcion = @Descripcion And IDPais=@IDPais And IDDepartamento=@IDDepartamento)
		
	End Else Begin
		Set @vID = (Select IsNull(Max(ID)+1,1) From Ciudad)
		Insert Into Ciudad(ID, IDPais, IDDepartamento, Descripcion, Codigo, Orden, Estado)
		Values(@vID, @IDPais, @IDDepartamento, @Descripcion, SUBSTRING(@Descripcion, 0, 4), 0, 'True')
		
	End
	
	Return @vID
		
End


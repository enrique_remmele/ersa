﻿
CREATE Procedure [dbo].[SpViewExtractoMovimientoBancario]

	--Entrada
	@FechaDesde Date,
	@FechaHasta Date,
	
	--Salida
	@Mensaje varchar(200) output ,
	@IDTransaccionSalida numeric(18,0) output
As

Begin
  
	--Declarar variables Variables
	declare @vID tinyint
	declare @vIDSucursal tinyint
	declare @vIDCuentaBancaria tinyint
	declare @vFecha Date
	declare @vOperacion varchar (50)
	declare @vCompCheque varchar (50)
	declare @vOrdendeDetalleConcepto varchar (100)
	declare @vDebito money
	declare @vCredito money
	declare @vMovimiento varchar (50)
	declare @vSaldoAnterior money
	declare @vSaldoInicial money
	declare @vSaldo money
	declare @vDeditoTemporal money
	declare @vCreditoTemporal money
	
	--Hallar ID
	set @vID = (Select IsNull(MAX(ID)+1,1) From ExtractoMovimientoBancario)
	
	--Insertar datos	
	Begin
		--Calcular Saldo Anterior
		set @vDeditoTemporal= ISNull((Select SUM (debito) From VExtractoMovimientoBancario Where Fecha < @FechaDesde),0) 
		set @vCreditoTemporal = ISnull((Select SUM (Credito) From VExtractoMovimientoBancario Where Fecha < @FechaDesde),0)
		set @vSaldoAnterior = @vDeditoTemporal - @vCreditoTemporal 
		Set @vSaldoInicial= @vSaldoAnterior  
	
		Declare db_cursor cursor for
		Select IDSucursal, IDCuentaBancaria, Fecha, Operacion, [Comp/Cheque], [Ordende/Detalle/Concepto], Debito, Credito,Movimiento From VExtractoMovimientoBancario 
		Where Fecha between @FechaDesde And @FechaHasta
		
		Open db_cursor   
		Fetch Next From db_cursor Into  @vIDSucursal, @vIDCuentaBancaria, @vFecha, @vOperacion, @vCompCheque, @vOrdendeDetalleConcepto, @vDebito, @vCredito, @vMovimiento   
		While @@FETCH_STATUS = 0 Begin  
			--Saldo
			set @vSaldo= (@vSaldoAnterior + @vDebito ) - @vCredito 
			
			--Insertar en la tabla Extracto de Movimiento Bancario
			Insert Into ExtractoMovimientoBancario (ID, IDSucursal, IDCuentaBancaria, Fecha, Operacion, CompCheque, OrdenDetalleConcepto, Debito, Credito, Movimiento, SaldoInicial, SaldoAnterior, Saldo)  
			Values(@vID, @vIDSucursal, @vIDCuentaBancaria,  @vFecha, @vOperacion, @vCompCheque, @vOrdendeDetalleConcepto, @vDebito, @vCredito, @vMovimiento,@vSaldoInicial, @vSaldoAnterior, @vSaldo)
			
			--Actualizar Saldo
			set @vSaldoAnterior = @vSaldo 
			
			Fetch Next From db_cursor Into @vIDSucursal,@vIDCuentaBancaria, @vFecha, @vOperacion, @vCompCheque, @vOrdendeDetalleConcepto, @vDebito, @vCredito, @vMovimiento    
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		
	End	
	set @Mensaje = 'Sin error!'
	Set @IDTransaccionSalida = @vID
	--Insertar fechas son datos	
	
End
	





﻿CREATE Procedure [dbo].[SpCuentaBancaria]

	--Entrada
	@ID tinyint = NULL,
	@IDBanco tinyint = NULL,
	@IDMoneda tinyint = NULL,
	@CuentaBancaria varchar (50) = NULL,
	@Nombre varchar (50) = NULL,
	@Titulares varchar (50) = NULL,
	@Firma1 varchar (50) = NULL,
	@Firma2 varchar (50) = NULL,
	@Firma3 varchar (50) = NULL,
	@CodigoCuentaContable varchar(50) = NULL,
	@IDTipoCuentaBancaria tinyint = NULL,
	@Apertura date = NULL,
	@Estado bit = NULL,
	@Observacion varchar (50) = NULL,
	@FormatoAlDia Varchar(50)=NULL,
	@FormatoDiferido varchar(50)=NULL,
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
	
	--Operacion
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From CuentaBancaria  Where CuentaBancaria=@CuentaBancaria) begin
			set @Mensaje = 'La cuenta bancaria ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vID tinyint
		set @vID = (Select IsNull((Max(ID)+1), 1) From CuentaBancaria )

		--Insertamos
		Insert Into CuentaBancaria (ID, IDBanco ,  IDMoneda , CuentaBancaria , Nombre , Titulares,Firma1,Firma2, Firma3, CodigoCuentaContable,IDTipoCuentaBancaria,Apertura,Estado,Observacion,FormatoAlDia,FormatoDiferido  )
		Values(@vID,   @IDBanco , @IDMoneda , @CuentaBancaria , @Nombre ,@Titulares ,@Firma1,@Firma2 ,@Firma3 ,@CodigoCuentaContable,@IDTipoCuentaBancaria,@Apertura ,@Estado ,@Observacion,@FormatoAlDia,@FormatoDiferido  )		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CUENTA BANCARIA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From CuentaBancaria  Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From CuentaBancaria  Where CuentaBancaria =@CuentaBancaria And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update CuentaBancaria  Set	ID =@ID , 
									IDBanco =@IDBanco ,
									IDMoneda =@IDMoneda , 
									CuentaBancaria =@CuentaBancaria ,
									Nombre = @Nombre, 
									Titulares = @Titulares,
									Firma1 = @Firma1 ,
									Firma2 = @Firma2 ,
									Firma3 = @Firma3 ,
									CodigoCuentaContable = @CodigoCuentaContable ,
									IDTipoCuentaBancaria = @IDTipoCuentaBancaria ,
									Apertura = @Apertura , 
									Estado = @Estado,
									Observacion = @Observacion,  
									FormatoAlDia = @FormatoAlDia,
									FormatoDiferido = @FormatoDiferido
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CUENTA BANCARIA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From CuentaBancaria  Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con CHEQUE
		if exists(Select * From Cheque  Where IDCuentaBancaria=@ID) begin
			set @Mensaje = 'El registro tiene cheques asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From CuentaBancaria  
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CUENTA BANCARIA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿CREATE Procedure [dbo].[SpExistenciaDeposito]

	--Entrada
	@IDDeposito tinyint,
	@IDProducto int,
	@ExistenciaMinima decimal(10,2),
	@ExistenciaCritica decimal(10,2),
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--Verificar que existe el producto
	If Not Exists(Select * From Producto Where ID=@IDProducto) Begin
		Set @Mensaje = 'El sistema no encontro el producto seleccionado!'
		Set @Procesado = 'False'
		return @@rowcount	
	End
	
	--Verificar que existe el deposito
	If Not Exists(Select * From Deposito Where ID=@IDDeposito) Begin
		Set @Mensaje = 'El sistema no encontro el deposito seleccionado!'
		Set @Procesado = 'False'
		return @@rowcount	
	End		
	
	--Si existe, actualizar
	Declare @vInsertar bit
	Set @vInsertar = 'True'
	
	If Exists(Select * From ExistenciaDeposito Where IDDeposito=@IDDeposito And IDProducto=@IDProducto) Begin
		Set @vInsertar = 'False'	
	End
	
	if @vInsertar = 'False' Begin
		
		Update ExistenciaDeposito Set ExistenciaMinima=@ExistenciaMinima,
										ExistenciaCritica=@ExistenciaCritica
		where IDDeposito=@IDDeposito And IDProducto=@IDProducto
		
		Set @Mensaje = 'Registro actualizado!'
		Set @Procesado = 'True'
		return @@rowcount
	
	End 
	
	If @vInsertar='True' Begin
		Insert Into ExistenciaDeposito(IDDeposito, IDProducto, Existencia, ExistenciaMinima, ExistenciaCritica, PlazoReposicion)
		Values(@IDDeposito, @IDProducto, 0, @ExistenciaMinima, @ExistenciaCritica, 0)		
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
	
	End
			
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	return @@rowcount
		
End


﻿CREATE Procedure [dbo].[SpProductRegistroFirma]

	--Entrada
	@IDCliente int,
	@Imagen image,

			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	--ACTUALIZAR
		
		--Si el ID existe
		If not exists(Select * From Cliente Where ID=@IDCliente) begin
			set @Mensaje = 'No existe el cliente seleccionado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Cliente Set Firma = @Imagen
		Where ID=@IDCliente
		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿CREATE Procedure [dbo].[SpViewKardexExtractoDetalladoContraOperativo]

	--Entrada
	@IDProducto int = 0,
	@FechaDesde date,
	@FechaHasta date
	
As

Begin

	--Variables
	Begin
		declare @vIDTransaccion int,
				@vIDProducto int,
				@vEntradaOperacion decimal(18,2),
				@vSalidaOperacion decimal(18,2),
				@vCostoPromedioOperacion decimal(18,2),
				@vEntradaKardex decimal(18,2),
				@vSalidaKardex decimal(18,2),
				@vCostoPromedioKardex decimal(18,2)
		
	End

	--Crear la tabla temporal
	create table #TablaTemporal(IDTransaccion int,
								IDProducto int,
								EntradaOperacion decimal(18,2),
								SalidaOperacion decimal(18,2),
								CostoPromedioOperacion decimal(18,2),
								EntradaKardex decimal(18,2),
								SalidaKardex decimal(18,2),
								CostoPromedioKardex decimal(18,2))
																	
		Begin
				
			Declare db_cursorCompararTodos cursor for
			
			Select  
			E.IDTransaccion,
			E.IDProducto,
			Sum(E.CantidadEntrada),
			Sum(E.CantidadSalida),
			'Costo'=Case when Sum(E.CantidadEntrada)>0 then AVG(E.CostoEntrada) else AVG(E.CostoSalida) end
			From VCargaKardex E  
			Join Producto P on E.IDProducto = P.ID and P.ControlarExistencia = 1
			where FechaEntrada between @FechaDesde and @FechaHasta
			and (Case when @IDProducto = 0 then 0 else IDProducto end)=(Case when @IDProducto = 0 then 0 else @IDProducto end)
			group by IDTransaccion, IDProducto
			Open db_cursorCompararTodos 
			Fetch Next From db_cursorCompararTodos Into @vIDTransaccion,@vIDProducto, @vEntradaOperacion,@vSalidaOperacion,@vCostoPromedioOperacion
			While @@FETCH_STATUS = 0 Begin 
			
			Select @vEntradaKardex=SUm(CantidadEntrada)
				  ,@vSalidaKardex=Sum(CantidadSalida)
				  ,@vCostoPromedioKardex=AVG(CostoPromedioOperacion)
			from Kardex 
			where IDTransaccion = @vIDTransaccion 
			and IDProducto = @vIDProducto
			Group by IDTransaccion,IDProducto

			insert into #TablaTemporal(IDTransaccion,IDProducto,EntradaOperacion,SalidaOperacion,CostoPromedioOperacion,EntradaKardex,SalidaKardex,CostoPromedioKardex)
				values(@vIDTransaccion, @vIDProducto,@vEntradaOperacion,@vSalidaOperacion,@vCostoPromedioOperacion,@vEntradaKardex,@vSalidaKardex,@vCostoPromedioKardex)
							
			Fetch Next From db_cursorCompararTodos Into @vIDTransaccion, @vIDProducto, @vEntradaOperacion,@vSalidaOperacion,@vCostoPromedioOperacion
				
			End
			
			--Cierra el cursor
			Close db_cursorCompararTodos   
			Deallocate db_cursorCompararTodos		   			
			
		End	
		
			Select * from #TablaTemporal

End

--exec SpViewKardexExtractoDetalladoContraOperativo 0,'20200101','20200831'

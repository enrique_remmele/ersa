﻿CREATE Procedure [dbo].[SpAsientoMovimiento]

				
	@IDTransaccion numeric(18,0)
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint = 0
	
	--Movimiento
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoOperacion smallint
	Declare @vIDTipoComprobante smallint
	Declare @vIDDepositoEntrada smallint
	Declare @vIDDepositoSalida smallint
	Declare @vEntrada bit
	Declare @vSalida bit
	Declare @vComprobante varchar(50)
	Declare @vFecha varchar(50)
	Declare @vObservacion varchar(100)
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money

	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	Declare @vCodigoDepositoEntrada varchar(50)
	Declare @vCodigoDepositoSalida varchar(50)
	Declare @vCodigoProducto varchar(50)

	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money

	Declare @MovimientoStock bit
	Declare @DescargaStock bit
	Declare @DescargaCompra bit
	Declare @ConsumoCombustible bit
	Declare @vBuscarEnProducto  bit
	
	--Obtener valores
	Begin
	
		Set @vIDMoneda = 1
		Set @vCotizacion = 1
		
		Select	@vIDSucursal=isnull(IDSucursal,1),
				@vIDTipoOperacion=IDTipoOperacion,
				@vTipoComprobante=TipoComprobante,
				@vIDTipoComprobante=IDTipoComprobante,
				@vIDDepositoEntrada=IDDepositoEntrada,
				@vIDDepositoSalida=IDDepositoSalida,
				@vEntrada=Entrada,
				@vSalida=Salida,
				@vComprobante=Comprobante,
				@vFecha=Fecha,
				@vObservacion=Observacion,
				@MovimientoStock = MovimientoStock,
				@DescargaStock = DescargaStock,
				@DescargaCompra = DescargaCompra,
				@ConsumoCombustible= MovimientoCombustible

		From VMovimiento Where IDTransaccion=@IDTransaccion 
		--and (MovimientoStock = 'True' or DescargaStock = 'True' or DescargaCompra  ='True' or MovimientoCombustible='True')
		
		if @DescargaStock = 'True' begin
			Exec SpAsientoMovimientoDescargaStock @IDTransaccion = @IDTransaccion
			goto Salir
		end

		if @DescargaCompra = 'True' begin
			Exec SpAsientoMovimientoDescargaCompra @IDTransaccion = @IDTransaccion
			goto Salir
		end

		--if @ConsumoCombustible = 'True' and @vEntrada='True' And @vSalida='True' begin
		if @ConsumoCombustible = 'True' and @vSalida='True' begin
			print concat('Entro a consumo ', @IDTRansaccion)
			Exec SpAsientoMovimientoConsumoCombustible @IDTransaccion = @IDTransaccion
			goto Salir
		end

		if @ConsumoCombustible = 'True' begin
			print concat('NO Entro a consumo :', @ConsumoCombustible, @IDTRansaccion)
			goto Salir
		end
		
		Set @vCodigoDepositoEntrada = IsNull((Select CuentaContableMercaderia From Deposito Where ID=@vIDDepositoEntrada), '')
		Set @vCodigoDepositoSalida = IsNull((Select CuentaContableMercaderia From Deposito Where ID=@vIDDepositoSalida), '')

	End
					
	--Verificar que el asiento se pueda modificar
	Begin
	
		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta anulado
		If (Select Anulado From Movimiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El registro esta anulado'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		
	End
				
	--Eliminar primero el asiento
	Begin
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = @vObservacion
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	--DescargaStock
	if @DescargaStock = 'true' begin
		--Cuentas para Transferencias
		If @vEntrada=1 And @vSalida=1 Begin
	
			--Variables
			Declare cCFTranferencia cursor for
			Select Codigo From VCFMovimiento 
			Where IDTipoOperacion=@vIDTipoOperacion 
			And IDTipoComprobante=@vIDTipoComprobante
			Open cCFTranferencia
			fetch next from cCFTranferencia into @vCodigo
		
			While @@FETCH_STATUS = 0 Begin  
				Declare @vCodigoTemp varchar(15) = @vCodigo

				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				Print 'Deposito Entrada: ' + @vCodigoDepositoEntrada
				Print 'Deposito Salida: ' + @vCodigoDepositoSalida

				--Entrada
				--Cuenta contable Entrada
				If @vCodigoDepositoEntrada != '' and @DescargaStock = 'false' Begin
					Set @vCodigoTemp = @vCodigoDepositoEntrada
				End
				Select @vDebe = Debe,
						@vHaber = Haber
						from vcfMovimiento where codigo = @vCodigoTemp

				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoTemp And PlanCuentaTitular='True')
				Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
				if @vDebe = 'True' begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoTemp, 0, @vImporte, 0, '')
				end 
				else begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoTemp, @vImporte, 0, 0, '')
				end
				Set @vCodigoTemp = @vCodigo

				--Salida
				--Cuenta contable Salida
				If @vCodigoDepositoSalida != '' and @DescargaStock = 'false' Begin
					Set @vCodigoTemp = @vCodigoDepositoSalida
				End
				Select @vDebe = Debe,
						@vHaber = Haber
						from vcfMovimiento where codigo = @vCodigoTemp

				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoTemp And PlanCuentaTitular='True')
				Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			
				if @vDebe = 'True' begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoTemp, 0, @vImporte, 0, '')
				end 
				else begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoTemp, @vImporte, 0, 0, '')
				end
				fetch next from cCFTranferencia into @vCodigo
			
			End
		
			close cCFTranferencia
			deallocate cCFTranferencia

		End
	end

	--MovimientoStock
	if @MovimientoStock = 'true' begin
		--Cuentas para Transferencias
		If @vEntrada=1 And @vSalida=1 Begin
	
			Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
			Print 'Deposito Entrada: ' + @vCodigoDepositoEntrada
			Print 'Deposito Salida: ' + @vCodigoDepositoSalida

			--Entrada
			--Cuenta contable Entrada
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoDepositoEntrada And PlanCuentaTitular='True')
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
			Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoDepositoEntrada, 0, Round(@vImporte,@vRedondeo), 0, '')
			
			--Salida
			--Cuenta contable Salida
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoDepositoSalida And PlanCuentaTitular='True')
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			
			if exists(select * from detalleasiento where idtransaccion = @IDTransaccion and IDCuentaContable = @vIDCuentaContable) begin
				Update DetalleAsiento
					Set Credito = Round(@vImporte,@vRedondeo)
				Where IDTransaccion = @IDTransaccion 
				and IDCuentaContable = @vIDCuentaContable
			end
			else begin
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoDepositoSalida, Round(@vImporte,@vRedondeo), 0, 0, '')
			end
		End
	end

	--Cuentas para Entrada
	If @vEntrada=1 And @vSalida=0 Begin
	print 'Entrada'
	print @vIDTipoOperacion
	print @vIDTipoComprobante
		Declare cCFEntrada cursor for
		Select Codigo, Debe, Haber From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFEntrada 
		fetch next from cCFEntrada into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			--Cuenta del debe
			If @vDebe = 1 and @vIDTipoComprobante not in (74,71,126) Begin
			
				Declare cCFDetalleMovimiento cursor for
				Select TP.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join TipoProducto TP On P.IDTipoProducto=TP.ID 
				Join VCuentaContable CC On CC.Codigo=TP.CuentaContableCosto 
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, TP.CuentaContableCosto, CC.IDCuentaContable
				Open cCFDetalleMovimiento 
				fetch next from cCFDetalleMovimiento into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
		
					If @vCodigoProducto != '' Begin
						Set @vCodigo = @vCodigoProducto 
					End
					
					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporte,@vRedondeo), 0, Round(@vImporte,@vRedondeo), '')
					
					End
			
					fetch next from cCFDetalleMovimiento into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimiento
				deallocate cCFDetalleMovimiento

			End

			If @vDebe = 1 and @vIDTipoComprobante = 74 Begin -- que sea CONV
				
				--insertamos cuenta mercaderia	
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				
				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				If @vImporte > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')
				
				End

				--insertamos cuenta costo
				Declare cCFDetalleMovimientoSalida cursor for
				Select TP.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
				From vDetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join TipoProducto TP On P.IDTipoProducto=TP.ID 
				Join VCuentaContable CC On CC.Codigo=TP.CuentaContableCosto 
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, TP.CuentaContableCosto, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					
					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto, Round(@vImporte,@vRedondeo), 0, Round(@vImporte,@vRedondeo), '')
					
					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

				

			End	
			
			If @vHaber = 1 and @vIDTipoComprobante = 71 Begin
			
				Declare cCFDetalleMovimiento cursor for
				Select TP.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D Join Producto P On D.IDProducto=P.ID Join TipoProducto TP On P.IDTipoProducto=TP.ID Join VCuentaContable CC On CC.Codigo=TP.CuentaContableCosto 
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, TP.CuentaContableCosto, CC.IDCuentaContable
				Open cCFDetalleMovimiento 
				fetch next from cCFDetalleMovimiento into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
		
					If @vCodigoProducto != '' Begin
						Set @vCodigo = @vCodigoProducto 
					End

					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo,Round(@vImporte,@vRedondeo),  0, Round(@vImporte,@vRedondeo), '')
					
					End
			
					fetch next from cCFDetalleMovimiento into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimiento
				deallocate cCFDetalleMovimiento

			End

			If @vHaber = 1 and @vIDTipoComprobante = 126 Begin
				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo,Round(@vImporte,@vRedondeo),  0, Round(@vImporte,@vRedondeo), '')
					
					End
			
			End

			If @vDebe = 1 and @vIDTipoComprobante in(71,126) Begin
				
				print 'Debe'	
				If @vCodigoDepositoEntrada != '' Begin
					Set @vCodigo = @vCodigoDepositoEntrada
				End	
				print @vCodigo		
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)

				If @vImporte > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')
				
				End
			End

			If @vHaber = 1 and @vIDTipoComprobante not in(71,126)  Begin

				If @vCodigoDepositoEntrada != '' Begin
					Set @vCodigo = @vCodigoDepositoEntrada
				End

				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				Set @vImporteHaber = @vImporte

				If @vImporteHaber > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')
				
				End

			End				
			
Seguir:

			fetch next from cCFEntrada into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFEntrada
		deallocate cCFEntrada

	End

	--Cuentas para Salida
	If @vEntrada=0 And @vSalida=1 Begin
		print concat('salida',' @vIDTipoComprobante:',@vIDTipoComprobante,' @vIDTipoOperacion:',@vIDTipoOperacion)
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber, BuscarEnProducto From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber, @vBuscarEnProducto
		print concat('Codigo Salida ',@vCodigo)
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			--Cuenta del debe
			If @vDebe = 1  and @vIDTipoComprobante not in(89,91) and @vBuscarEnProducto = 0 Begin -- QUe no sea MOVINT
			
				print @vCodigoDepositoSalida

				IF @vIDTipoComprobante not in(72,81) begin --Si es distinto a Destruccion
					If @vCodigoDepositoSalida != '' Begin
						Set @vCodigo = @vCodigoDepositoSalida
					End
				end
				 
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				
				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				Set @vImporteHaber = @vImporte

				If @vImporteHaber > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')
					Print concat('Primero',@vCodigo)
				End

			End

			If @vDebe = 1  and @vIDTipoComprobante not in(89,91) and @vBuscarEnProducto = 1 Begin -- QUe no sea MOVINT
			print 'entra a buscar en producto'
			Declare cCFDetalleMovimientoSalida cursor for
				Select TP.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D Join Producto P On D.IDProducto=P.ID Join TipoProducto TP On P.IDTipoProducto=TP.ID Join VCuentaContable CC On CC.Codigo=TP.CuentaContableCosto 
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, TP.CuentaContableCosto, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
				
				print @vCodigoDepositoSalida

				IF @vIDTipoComprobante not in(72,81) begin --Si es distinto a Destruccion
					If @vCodigoDepositoSalida != '' Begin
						Set @vCodigo = @vCodigoProducto
					End
				end
				 
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
				
				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				Set @vImporteHaber = @vImporte

				If @vImporteHaber > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')
				
				End

				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End

			If @vDebe = 1 and @vIDTipoComprobante = 89 Begin -- Solo para MOVINT	
							 
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				
				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				Set @vImporteHaber = @vImporte

				If @vImporteHaber > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')
				
				End

			End
			
			If @vDebe = 1  and @vIDTipoComprobante = 91 Begin -- que sea CONV
				
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
				If @vImporte > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')
				
				End

				
			End
			
			If @vHaber = 1 and @vIDTipoComprobante in(71) Begin -- Solo para REPRO	
			
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				
				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				Set @vImporteHaber = @vImporte

				If @vImporteHaber > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')
				
				End

			End
			
			If @vHaber = 1  and @DescargaStock = 'False' and @vIDTipoComprobante not in(89,71,74,91) Begin -- que no sea MOVINT

				Declare cCFDetalleMovimientoSalida cursor for
				Select TP.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D Join Producto P On D.IDProducto=P.ID Join TipoProducto TP On P.IDTipoProducto=TP.ID Join VCuentaContable CC On CC.Codigo=TP.CuentaContableCosto 
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, TP.CuentaContableCosto, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					
					IF @vIDTipoComprobante not in(72,81) begin --Si es distinto a Destruccion
						If @vCodigoProducto != '' Begin
							Set @vCodigo = @vCodigoProducto 
						End
					end

					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporte,@vRedondeo), 0, Round(@vImporte,@vRedondeo), '')
					
					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End
			
			If @vHaber = 1  and @DescargaStock = 'False' and @vIDTipoComprobante = 89 Begin -- que sea MOVINT
				print 'entro'
				Declare cCFDetalleMovimientoSalida cursor for
				Select TP.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D Join Producto P On D.IDProducto=P.ID 
				Join TipoProducto TP On P.IDTipoProducto=TP.ID 
				Join VCuentaContable CC On CC.Codigo=TP.CuentaContableCosto 
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, TP.CuentaContableCosto, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					
					If @vImporte > 0 Begin		
					print 'aqui'	
					print @vCodigo
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto, Round(@vImporte,@vRedondeo), 0, Round(@vImporte,@vRedondeo), '')
					
					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End	
			
			If @vHaber = 1  and @DescargaStock = 'False' and @vIDTipoComprobante = 74 Begin -- que sea CONV
				
				--insertamos cuenta mercaderia	
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				
				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				If @vImporte > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporte,@vRedondeo), 0, Round(@vImporte,@vRedondeo), '')
				
				End

				--insertamos cuenta costo
				Declare cCFDetalleMovimientoSalida cursor for
				Select TP.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
				From vDetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join TipoProducto TP On P.IDTipoProducto=TP.ID 
				Join VCuentaContable CC On CC.Codigo=TP.CuentaContableCosto 
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, TP.CuentaContableCosto, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					
					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto, 0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')
					
					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

				

			End	

			If @vHaber = 1  and @DescargaStock = 'False' and @vIDTipoComprobante = 91 Begin -- que sea CONV
				
				--insertamos cuenta mercaderia	
				
				
				Set @vCodigo = @vCodigoDepositoSalida
				 
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				
				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				Set @vImporteHaber = @vImporte

				If @vImporteHaber > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo,Round(@vImporte,@vRedondeo),  0, Round(@vImporte,@vRedondeo), '')
				
				End
				
			End

			If @vHaber = 1  and @DescargaStock = 'True' Begin

				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCompra, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D Join Producto P On D.IDProducto=P.ID 
				--Join TipoProducto TP On P.IDTipoProducto=TP.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCosto 
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCompra, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
		
					If @vCodigoProducto != '' Begin
						Set @vCodigo = @vCodigoProducto 
					End

					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporte,@vRedondeo), 0, Round(@vImporte,@vRedondeo), '')
					
					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End				
			
			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber, @vBuscarEnProducto
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End

	--Actualizamos la cabecera, el total
	Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
	Set @vImporteHaber = ROUND(@vImporteHaber, @vRedondeo)
	Set @vImporteDebe = Round(@vImporteHaber,@vRedondeo)
	
	Update Asiento Set Total = @vImporteHaber,
						Credito = @vImporteHaber,
						Debito = @vImporteDebe,
						Saldo = @vImporteHaber - @vImporteDebe
	Where IDTransaccion=@IDTransaccion
	
	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
	
End


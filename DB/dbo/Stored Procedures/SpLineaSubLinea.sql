﻿CREATE Procedure [dbo].[SpLineaSubLinea]

	--Entrada
	@IDLinea smallint,
	@IDSubLinea smallint,
	@Relacionar bit
	
				
As

Begin

	--BLOQUES
	
	--RELACIONAR
	if @Relacionar='True' begin
		
		--Solo agregar si no existen
		If Exists(Select * From LineaSubLinea Where IDLinea=@IDLinea And IDSubLinea=@IDSubLinea) Begin
			return @@rowcount
		End
		
		--Insertamos
		Insert Into LineaSubLinea(IDLinea, IDSubLinea)
		Values(@IDLinea, @IDSubLinea)		
		
		return @@rowcount
			
	End
	
	--NO RELACIONAR
	if @Relacionar='False' begin
		
		Delete From LineaSubLinea
		Where IDLinea=@IDLinea And IDSubLinea=@IDSubLinea
		
		return @@rowcount
			
	End

End


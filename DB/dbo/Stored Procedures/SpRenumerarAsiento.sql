﻿CREATE Procedure [dbo].[SpRenumerarAsiento]
	
	--Entrada
	@Operacion varchar(10),
	@IDTransaccion numeric(18,0),
	@Numero varchar(50),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	

As

Begin
	
	If @Operacion = 'UPD' Begin
	
		-- Validadar si es que no esta conciliado
		If Exists (Select * from Asiento Where Conciliado= 'True' And IDTransaccion= @IDTransaccion)Begin
			set @Mensaje = 'El registrio esta conciliado'
			set @Procesado = 'False'
			return @@rowcount
		End	
		
		--Actualizar Numero
		Update Asiento  Set Numero  = @Numero
		Where IDTransaccion = @IDTransaccion
	
		Set @Mensaje = 'Registro procesado!'
		Set @Procesado = 'True'	
		Return @@rowcount
		
	End 
End


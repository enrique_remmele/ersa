﻿CREATE Procedure [dbo].[SpImportarDetallePedido]

	
	--Entrada
	@NroPedido int,
	@NroFactura varchar(50),
	@Referencia varchar(50),
	@Deposito varchar(50),
	@Observacion varchar(50),
	@Impuesto varchar(50),
	@Cantidad decimal(10,2),
	
	@PrecioUnitario money,
	@Total money,
	
	@PorcentajeDescuento varchar(50),
	@DescuentoUnitario money,
	@TotalDescuento money,
	
	@Total10 money,
	@Total5 money,
	
	@TotalTactico money,
	@PorcentajeTactico decimal(5,3),
	@TotalTPR money,
	@PorcentajeTPR decimal(5,3),
	@TotalExpress money,
	@PorcentajeExpress decimal(5,3),
	
	@Secuencia varchar(50),
	
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Opciones
	@Actualizar bit
	
As

Begin
		
	--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		Declare @vIDTransaccion numeric(18,0)
		Declare @vOperacion varchar(10)
		
		Declare @vIDProducto int
		Declare @vID tinyint
		Declare @vIDDeposito int
		Declare @vIDImpuesto int
		Declare @vTotalImpuesto money
		Declare @vTotalDiscriminado money
		Declare @vDescuentoUnitarioDiscriminado money
		Declare @vTotalDescuentoDiscriminado money
		Declare @vCostoUnitario money
		Declare @vTotalCosto money
		Declare @vTotalCostoImpuesto money
		Declare @vTotalCostoDiscriminado money
		Declare @vCantidadCaja int
			
		
	End
	
	--Hayar valores
	Begin
		
		Set @vMensaje = 'No se proceso'
		Set @vProcesado = 'False'
		Set @vOperacion = 'INS'
		
		If @NroPedido = 0 Begin
			Set @NroPedido = @NroFactura
		End
			
		--Obtener Pedido
		Set @vIDTransaccion = IsNull((Select IDTransaccion From PedidoImportado Where NroPedido=@NroPedido),0)
		If @vIDTransaccion = 0 Begin
			Set @vMensaje = 'No se encontro el pedido!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Obtener Producto
		Set @vIDProducto = IsNull((Select Top(1) ID From Producto Where Referencia=@Referencia And Estado='True'),0)
		If @vIDProducto = 0 Begin
			Set @vMensaje = 'No se encontro el producto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--ID
		Set @vID = IsNull((Select MAX(ID)+1 From DetallePedido Where IDTransaccion=@vIDTransaccion),0)

		--Deposito
		Set @vIDDeposito = IsNull((Select Top(1) ID From Deposito Where Descripcion=@Deposito),0)
		If @vIDTransaccion = 0 Begin
			Set @vMensaje = 'No se encontro el deposito!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Impuesto
		Set @vIDImpuesto=0
		If @Impuesto = '10' Set @vIDImpuesto=1
		If @Impuesto = '5' Set @vIDImpuesto=2
		
		If @vIDImpuesto = 0 Begin
			Set @vMensaje = 'No se encontro el impuesto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
	
		Declare @vFactorDiscriminado decimal(10,7)
		Declare @vFactorImpuesto decimal(10,7)
		
		Set @vFactorDiscriminado = IsNull((Select Top(1) FactorDiscriminado From  Impuesto Where ID=@vIDImpuesto),0)
		Set @vFactorImpuesto = IsNull((Select Top(1) FactorImpuesto From  Impuesto Where ID=@vIDImpuesto),0)
	
		--Totales
		Set @vTotalImpuesto = @Total / @vFactorImpuesto
		Set @vTotalDiscriminado = @Total - @vTotalImpuesto
		
		----Descuento
		Set @vDescuentoUnitarioDiscriminado = @DescuentoUnitario / @vFactorDiscriminado
		Set @vTotalDescuentoDiscriminado = @TotalDescuento / @vFactorDiscriminado
		
		Set @vCostoUnitario = IsNull((Select CostoPromedio From Producto Where ID=@vIDProducto),0)
		Set @vTotalCosto = @vCostoUnitario * @Cantidad
		
		Set @vTotalCostoImpuesto = @vTotalCosto / @vFactorImpuesto
		Set @vTotalCostoDiscriminado = @vTotalCosto - @vTotalCostoImpuesto
		
		Set @vCantidadCaja = 1
		
	End
	
	--Ver si existe
	If Exists(Select * From DetallePedido Where Secuencia=@Secuencia And IDProducto=@vIDProducto And IDTransaccion=@vIDTransaccion) Begin
		Set @vOperacion = 'UPD'
	End	
	
	--ACTUALIZAR
	If @vOperacion = 'UPD' Begin
	
		If @Actualizar = 'True' Begin
						
			Set @vMensaje = 'Actualizado MILNER PUTO!'
			Set @vProcesado = 'True'
			GoTo Salir
			
		End
		
		Set @vMensaje = 'Sin Act.!'
		Set @vProcesado = 'True'
		GoTo Salir
		
	End
	
	--INSERTAR
	If @vOperacion = 'INS' Begin
		
		--Validar Informacion
		 		
		--Insertar en el Detalle
		Insert Into DetallePedido(IDTransaccion, IDProducto, ID, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, Total, TotalImpuesto, TotalDiscriminado, PorcentajeDescuento, DescuentoUnitario, DescuentoUnitarioDiscriminado, TotalDescuento, TotalDescuentoDiscriminado, CostoUnitario, TotalCosto, TotalCostoImpuesto, TotalCostoDiscriminado, Caja, CantidadCaja, Secuencia)
		Values(@vIDTransaccion, @vIDProducto, @vID, @vIDDeposito, @Observacion, @vIDImpuesto, @Cantidad, @PrecioUnitario, @Total, @vTotalImpuesto, @vTotalDiscriminado, @PorcentajeDescuento, @DescuentoUnitario, @vDescuentoUnitarioDiscriminado, @TotalDescuento, @vTotalDescuentoDiscriminado, @vCostoUnitario, @vTotalCosto, @vTotalCostoImpuesto, @vTotalCostoDiscriminado, 'False', @vCantidadCaja, @Secuencia)		
				
		Set @vMensaje = 'Registro guardado INS!!!!'
		Set @vProcesado = 'True'
						
	End
	
	--Insertar el Detalle Impuesto
	Begin
		
		If Not Exists(Select * from DetalleImpuesto Where IDTransaccion=@vIDTransaccion And IDImpuesto=@vIDImpuesto) Begin
			Insert Into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, RetencionIVA, RetencionRenta)
			values(@vIDTransaccion, @vIDImpuesto, 0, 0, 0, 0, 0, 0)
		End
		
		Update DetalleImpuesto Set Total=Total+@Total,
									TotalImpuesto=TotalImpuesto+@vTotalImpuesto,
									TotalDiscriminado=TotalDiscriminado+@vTotalDiscriminado,
									TotalDescuento=TotalDescuento+@TotalDescuento,
									RetencionIVA=RetencionIVA+(@vTotalImpuesto*0.3)
		Where IDTransaccion=@vIDTransaccion And IDImpuesto=@vIDImpuesto
	End
	
	--Insertar el Detalle Descuento
	Begin
		
		--AGREGAR TIPO DE DESCUENTO EN LA BASE DE DATOS
		Declare @vIDDescuento tinyint
		Declare @vIDTipoDescuento tinyint
		Declare @vDescuento money
		Declare @vPorcentajeDescuento money
		Declare @vDescuentoDiscriminado money
		Declare @vDecuentoImpuesto money
		Declare @vIDActividad int
		Declare @vFecha date
		
		--TACTICO
		If @TotalTactico > 0 Begin
		
			Set @vIDTipoDescuento = 1
			Set @vDescuento = @TotalTactico
			Set @vPorcentajeDescuento = @PorcentajeTactico
			Set @vDecuentoImpuesto = @vDescuento / @vFactorImpuesto
			Set @vDescuentoDiscriminado = @vDescuento - @vDecuentoImpuesto
			
			Set @vIDDescuento = IsNull((Select MAx(IDDescuento)+1 from DescuentoPedido Where IDTransaccion=@vIDTransaccion And IDProducto=@vIDProducto And ID=@vID),0)
			
			Set @vFecha = (Select FechaFacturar From Pedido Where IDTransaccion=@vIDTransaccion)
			
			Set @vIDActividad = (Select top 1 A.ID From DetalleActividad DA Join Actividad A On DA.IDActividad=A.ID Join DetallePlanillaDescuentoTactico DPDT On A.ID=DPDT.IDActividad Join PlanillaDescuentoTactico PDT On DPDT.IDTransaccion=PDT.IDTransaccion
								Where (@vFecha Between PDT.Desde And PDT.Hasta) And DA.IDProducto=@vIDProducto)
		
		
			Insert Into DescuentoPedido(IDTransaccion, IDProducto, ID, IDDescuento, IDDeposito, IDTipoDescuento, IDActividad, Descuento, Porcentaje, DescuentoDiscriminado)
			Values(@vIDTransaccion, @vIDProducto, @vID, @vIDDescuento, @vIDDeposito, @vIDTipoDescuento, @vIDActividad, @vDescuento, @vPorcentajeDescuento, @vDescuentoDiscriminado)
		
			
			
				
		End
		
		----TPR
		If @TotalTPR> 0 Begin
		
			Set @vIDTipoDescuento = 0
			Set @vDescuento = @TotalTPR
			Set @vPorcentajeDescuento = @PorcentajeTPR
			Set @vDecuentoImpuesto = @vDescuento / @vFactorImpuesto
			Set @vDescuentoDiscriminado = @vDescuento - @vDecuentoImpuesto
			
			Set @vIDDescuento = IsNull((Select MAx(IDDescuento)+1 from DescuentoPedido Where IDTransaccion=@vIDTransaccion And IDProducto=@vIDProducto And ID=@vID),0)
		
			Insert Into DescuentoPedido(IDTransaccion, IDProducto, ID, IDDescuento, IDDeposito, IDTipoDescuento, IDActividad, Descuento, Porcentaje, DescuentoDiscriminado)
			Values(@vIDTransaccion, @vIDProducto, @vID, @vIDDescuento, @vIDDeposito, @vIDTipoDescuento, NULL, @vDescuento, @vPorcentajeDescuento, @vDescuentoDiscriminado)		
			
		End
		
		----EXPRESS
		If @TotalExpress > 0 Begin
			
			Set @vIDTipoDescuento = 2
			Set @vDescuento = @TotalExpress
			Set @vPorcentajeDescuento = @PorcentajeExpress
			Set @vDecuentoImpuesto = @vDescuento / @vFactorImpuesto
			Set @vDescuentoDiscriminado = @vDescuento - @vDecuentoImpuesto
			
			Set @vIDDescuento = IsNull((Select MAx(IDDescuento)+1 from DescuentoPedido Where IDTransaccion=@vIDTransaccion And IDProducto=@vIDProducto And ID=@vID),0)
		
			Insert Into DescuentoPedido(IDTransaccion, IDProducto, ID, IDDescuento, IDDeposito, IDTipoDescuento, IDActividad, Descuento, Porcentaje, DescuentoDiscriminado)
			Values(@vIDTransaccion, @vIDProducto, @vID, @vIDDescuento, @vIDDeposito, @vIDTipoDescuento, NULL, @vDescuento, @vPorcentajeDescuento, @vDescuentoDiscriminado)
			
		End
		
	End
	
	--Actualizar totales de Pedido
	Begin
	
		Declare @vTotal money
		Declare @vTotalDescuento money
				
		Set @vTotal = IsNull((Select SUM(Total) From DetallePedido Where IDTransaccion=@vIDTransaccion),0)
		Set @vTotalDiscriminado = IsNull((Select SUM(TotalDiscriminado) From DetallePedido Where IDTransaccion=@vIDTransaccion),0)
		Set @vTotalImpuesto = IsNull((Select SUM(TotalImpuesto) From DetallePedido Where IDTransaccion=@vIDTransaccion),0)
		Set @vTotalDescuento = IsNull((Select SUM(TotalDescuento) From DetallePedido Where IDTransaccion=@vIDTransaccion),0)
		
		Update Pedido Set Total=@vTotal,
							TotalDiscriminado=@vTotalDiscriminado,
							TotalImpuesto=@vTotalImpuesto,
							TotalDescuento=@vTotalDescuento
		Where IDTransaccion=@vIDTransaccion
		
	End
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


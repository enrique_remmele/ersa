﻿CREATE Procedure [dbo].[SpViewExtractoMovimientoProveedorMonedasCC]

	--Entrada
	@Fecha1 Date,
	@Fecha2 Date,
	@IDProveedor int=0,
	@Comprobante varchar (30) = '%%',
	@IDMoneda int = 0,
	@Cuenta varchar(100)
As

Begin
	--Crear la tabla temporal
    create table #TablaTemporal(
		IDTransaccion int,
		Codigo int,
		RazonSocial varchar(50),
		Referencia varchar(50),
		Fecha date,
		Operacion varchar(50),
		Documento varchar(50),
		[Detalle/Concepto] varchar(50),
		Debitos money,
		Creditos money,
		Saldo money,
		SaldoAnterior money,
		SaldoInicial money,
		IDMoneda int,
		DescripcionMoneda varchar(50),
		Movimiento char(20),
		CodigoCuentaContable varchar(30),
		CuentaContable varchar(100)
	)
								
	--Variables para calcular saldo
	declare @vSaldo money
	declare @vTotalDebito money
	declare @vTotalCredito money
	declare @vSaldoAnterior money
	declare @vSaldoInicial money
	declare @vIDMonedaCambio tinyint = 0
	
	--Declarar variables
	declare @vIDTransaccion Numeric (18,0)
	declare @vIDProveedor int
	declare @vRazonSocial varchar (100)
	declare @vReferencia varchar(50)
	declare @vFecha date
	declare @vOperacion varchar(50)
	declare @vDocumento varchar(50)
	declare @vDetalleConcepto varchar(50)
	declare @vDebitos money
	declare @vCreditos money
	declare @vIDMoneda int
	declare @vMoneda varchar(50)
	declare @vMovimiento varchar(20)
	declare @vCodigoCuentaContable varchar(100)

	if @IDProveedor > 0 begin
			
			Declare cExtractoMoneda cursor for
			Select
				IDTransaccion,
				Codigo,
				RazonSocial,
				Referencia,
				Fecha,
				Operacion,
				Documento,
				[Detalle/Concepto],
				Debito,
				Credito,
				IDMoneda,
				DescripcionMoneda,
				Movimiento,
				CodigoCuentaContable
			From VExtractoMovimientoProveedorCC
			where Fecha between @Fecha1 and @Fecha2 And Documento Like @Comprobante
			and CodigoCuentaContable = @Cuenta
			and IDProveedor = @IDProveedor
			Order By Fecha, RazonSocial, IdTransaccion Desc
			
			
			Open cExtractoMoneda  
			Fetch cExtractoMoneda Into @vIDTransaccion,@vIDProveedor,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vDebitos,@vCreditos,@vIDMoneda,@vMoneda,@vMovimiento,@vCodigoCuentaContable

			--Hallar Totales para Saldo Anterior
			if @IDProveedor is null begin
				Set @vTotalDebito  = IsNull((Select Sum(Debito) From VExtractoMovimientoProveedor Where Fecha < @Fecha1 And IDMoneda=@vIDMoneda),0)
				Set @vTotalCredito = IsNull((Select Sum(Credito) From VExtractoMovimientoProveedor Where Fecha < @Fecha1 And IDMoneda=@vIDMoneda),0)
			end
			else begin
				Set @vTotalDebito  = IsNull((Select Sum(Debito) From VExtractoMovimientoProveedor Where Codigo=@IDProveedor And Fecha < @Fecha1 And IDMoneda=@vIDMoneda),0)
				Set @vTotalCredito = IsNull((Select Sum(Credito) From VExtractoMovimientoProveedor Where Codigo=@IDProveedor And Fecha < @Fecha1 And IDMoneda=@vIDMoneda),0)				
			end
			--Hallar Saldo Anterior
			Set @vSaldoAnterior =  @vTotalCredito - @vTotalDebito
			Set @vSaldoAnterior = 0
			Set @vSaldoInicial = @vSaldoAnterior 
			
			
			Insert Into  #TablaTemporal(IDTransaccion, Codigo, RazonSocial, Referencia, Fecha, Operacion, Documento, [Detalle/Concepto], Saldo, SaldoAnterior, SaldoInicial, Debitos, Creditos, IDMoneda, DescripcionMoneda, Movimiento,CodigoCuentaContable) 
			Values (0, @vIDProveedor, @vRazonSocial, @vReferencia, @Fecha1, '', '', '==== SALDO ANTERIOR ' + @vMoneda + '====' , @vSaldoInicial, @vSaldoInicial, 0, 0, 0, @vIDMoneda, @vMoneda, 'SALDO', @Cuenta)

			IF OBJECT_ID('tempdb..#Total') IS NOT NULL DROP TABLE #Total
			create table #Total(debito money, credito money)
			WHILE (@@FETCH_STATUS = 0 ) BEGIN

				--Hallar Saldo			
				set @vSaldo = (@vSaldoAnterior + @vCreditos) - @vDebitos

				--Insertar Fila en la Tabla Temporal
				--if @IDProveedor is null begin
				--	set @vRazonSocial = 'Todos'
				--end
				Insert Into  #TablaTemporal(IDTransaccion,Codigo,RazonSocial,Referencia,Fecha,Operacion,Documento,[Detalle/Concepto],Saldo,SaldoAnterior,SaldoInicial,Debitos,Creditos,IDMoneda,DescripcionMoneda,Movimiento, CodigoCuentaContable) 
				Values (@vIDTransaccion,@vIDProveedor,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vSaldo,@vSaldoAnterior,@vSaldoInicial,@vDebitos,@vCreditos,@vIDMoneda,@vMoneda,@vMovimiento, @vCodigoCuentaContable)

				insert into #Total(debito, credito) values (@vDebitos, @vCreditos)

				--Actualizar Saldo
				set @vSaldoAnterior = @vSaldo 

				Fetch cExtractoMoneda Into @vIDTransaccion,@vIDProveedor,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vDebitos,@vCreditos,@vIDMoneda,@vMoneda,@vMovimiento, @vCodigoCuentaContable
		
			end

			--Insertar total acumulado
			select @vDebitos = ISNULL(SUM(debito),0), @vCreditos = ISNULL(sum(credito),0) from #Total
			--if @IDProveedor is null begin
			--	set @vRazonSocial = 'Todos'
			--end
			Insert Into  #TablaTemporal(IDTransaccion, Codigo, RazonSocial, Referencia, Fecha, Operacion, Documento, [Detalle/Concepto], Saldo, SaldoAnterior, SaldoInicial, Debitos, Creditos, IDMoneda, DescripcionMoneda, Movimiento, CodigoCuentaContable) 
			Values (1, @vIDProveedor, @vRazonSocial, @vReferencia, @Fecha2, '', '', '==== SALDO ACUMULADO ' + @vMoneda + '====' , @vSaldo, 0, 0, @vDebitos, @vCreditos, @vIDMoneda, @vMoneda, 'ACUMULADO', @Cuenta)

	
	end
	else
	begin
	
			Declare cExtractoMoneda cursor for
			Select
				IDTransaccion,
				Codigo,
				RazonSocial,
				Referencia,
				Fecha,
				Operacion,
				Documento,
				[Detalle/Concepto],
				Debito,
				Credito,
				IDMoneda,
				DescripcionMoneda,
				Movimiento,
				CodigoCuentaContable
			From VExtractoMovimientoProveedorCC
			where Fecha between @Fecha1 and @Fecha2 And Documento Like @Comprobante
			and CodigoCuentaContable = @Cuenta
			--and IDProveedor = @vIDProveedor
			Order By RazonSocial, Fecha, IDTransaccion Desc
			

			Open cExtractoMoneda  
			Fetch cExtractoMoneda Into @vIDTransaccion,@vIDProveedor,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vDebitos,@vCreditos,@vIDMoneda,@vMoneda,@vMovimiento,@vCodigoCuentaContable

			--Hallar Totales para Saldo Anterior
			if @IDProveedor is null begin
				Set @vTotalDebito  = IsNull((Select Sum(Debito) From VExtractoMovimientoProveedor Where Fecha < @Fecha1 And IDMoneda=@vIDMoneda),0)
				Set @vTotalCredito = IsNull((Select Sum(Credito) From VExtractoMovimientoProveedor Where Fecha < @Fecha1 And IDMoneda=@vIDMoneda),0)
			end
			else begin
				Set @vTotalDebito  = IsNull((Select Sum(Debito) From VExtractoMovimientoProveedor Where Codigo=@IDProveedor And Fecha < @Fecha1 And IDMoneda=@vIDMoneda),0)
				Set @vTotalCredito = IsNull((Select Sum(Credito) From VExtractoMovimientoProveedor Where Codigo=@IDProveedor And Fecha < @Fecha1 And IDMoneda=@vIDMoneda),0)				
			end
			--Hallar Saldo Anterior
			Set @vSaldoAnterior =  @vTotalCredito - @vTotalDebito
			Set @vSaldoAnterior = 0
			Set @vSaldoInicial = @vSaldoAnterior 
			
			
			Insert Into  #TablaTemporal(IDTransaccion, Codigo, RazonSocial, Referencia, Fecha, Operacion, Documento, [Detalle/Concepto], Saldo, SaldoAnterior, SaldoInicial, Debitos, Creditos, IDMoneda, DescripcionMoneda, Movimiento,CodigoCuentaContable) 
			Values (0, @vIDProveedor, @vRazonSocial, @vReferencia, @Fecha1, '', '', '==== SALDO ANTERIOR ' + @vMoneda + '====' , @vSaldoInicial, @vSaldoInicial, 0, 0, 0, @vIDMoneda, @vMoneda, 'SALDO', @Cuenta)

			IF OBJECT_ID('tempdb..#TotalProveedor') IS NOT NULL DROP TABLE #Total
				create table #TotalProveedor(debito money, credito money)
			WHILE (@@FETCH_STATUS = 0 ) BEGIN

				--Hallar Saldo			
				set @vSaldo = (@vSaldoAnterior + @vCreditos) - @vDebitos

				--Insertar Fila en la Tabla Temporal
				--if @IDProveedor is null begin
				--	set @vRazonSocial = 'Todos'
				--end
				Insert Into  #TablaTemporal(IDTransaccion,Codigo,RazonSocial,Referencia,Fecha,Operacion,Documento,[Detalle/Concepto],Saldo,SaldoAnterior,SaldoInicial,Debitos,Creditos,IDMoneda,DescripcionMoneda,Movimiento, CodigoCuentaContable) 
				Values (@vIDTransaccion,@vIDProveedor,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vSaldo,@vSaldoAnterior,@vSaldoInicial,@vDebitos,@vCreditos,@vIDMoneda,@vMoneda,@vMovimiento, @vCodigoCuentaContable)

				insert into #TotalProveedor(debito, credito) values (@vDebitos, @vCreditos)

				--Actualizar Saldo
				set @vSaldoAnterior = @vSaldo 

				Fetch cExtractoMoneda Into @vIDTransaccion,@vIDProveedor,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vDebitos,@vCreditos,@vIDMoneda,@vMoneda,@vMovimiento, @vCodigoCuentaContable
		
			end

			--Insertar total acumulado
			select @vDebitos = ISNULL(SUM(debito),0), @vCreditos = ISNULL(sum(credito),0) from #TotalProveedor
			--if @IDProveedor is null begin
			--	set @vRazonSocial = 'Todos'
			--end
			Insert Into  #TablaTemporal(IDTransaccion, Codigo, RazonSocial, Referencia, Fecha, Operacion, Documento, [Detalle/Concepto], Saldo, SaldoAnterior, SaldoInicial, Debitos, Creditos, IDMoneda, DescripcionMoneda, Movimiento, CodigoCuentaContable) 
			Values (1, @vIDProveedor, @vRazonSocial, @vReferencia, @Fecha2, '', '', '==== SALDO ACUMULADO ' + @vMoneda + '====' , @vSaldo, 0, 0, @vDebitos, @vCreditos, @vIDMoneda, @vMoneda, 'ACUMULADO', @Cuenta)

	end
	Select *, Fec=CONVERT(varchar(50), Fecha, 5) From #TablaTemporal --ORder by Fecha, RazonSocial, IdTransaccion
end

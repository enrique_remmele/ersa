﻿CREATE Procedure [dbo].[SpKardexVerificarOperaciones]

	--Entrada
	@IDProducto int = 0,
	@FechaDesde date,
	@FechaHasta date
	
As

Begin

	--Variables
	Begin
		declare @vIDTransaccion int
		
	End
																	
		Begin
				
			Declare db_cursorControlarTodos cursor for
			
			Select  
			IDTransaccion
			From VCargaKardex E  
			where FechaEntrada between @FechaDesde and @FechaHasta
			and (Case when @IDProducto = 0 then 0 else IDProducto end)=(Case when @IDProducto = 0 then 0 else @IDProducto end)
			
			Open db_cursorControlarTodos 
			Fetch Next From db_cursorControlarTodos Into @vIDTransaccion
			While @@FETCH_STATUS = 0 Begin 
				print @vIDTransaccion
				exec SpKardexVerificarOperacion @vIDTransaccion
							
			Fetch Next From db_cursorControlarTodos Into @vIDTransaccion
				
			End
			
			--Cierra el cursor
			Close db_cursorControlarTodos   
			Deallocate db_cursorControlarTodos		   			
			
		End	
		
			

End



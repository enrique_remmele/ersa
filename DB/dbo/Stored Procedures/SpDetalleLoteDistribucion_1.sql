﻿CREATE Procedure [dbo].[SpDetalleLoteDistribucion]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Operacion varchar(10),
	
	--Salida	
	@Mensaje varchar(200) output,
	@Procesado bit output
	
As

Begin

	--Cargar Detalle de Ventas a Detalle de Lotes
	If @Operacion = 'INS' Begin
	
		--Verificamos que el lote exista
		If Not Exists(Select * From LoteDistribucion Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El Registro no existe!'
			Set @Procesado = 'True'
			return @@rowcount
		End
		
		--Si tiene detalle, Eliminar us detalle
		If Exists(Select * From DetalleLoteDistribucion Where IDTransaccion=@IDTransaccion) Begin
			Delete From DetalleLoteDistribucion Where IDTransaccion=@IDTransaccion
		End
		
		--Cargamos el detalle del lote con un cursor de ventas
		--Variables
		Declare @vIDProducto int
		Declare @vCantidad decimal(10, 2)
		Declare @vTotal money
		Declare @vTotalDescuento money
		Declare @vTotalDiscriminado money
		Declare @vCantidadCaja decimal(10, 2)
				 
		--Cursor
		Declare db_cursor cursor for
		Select DV.IDProducto, 'Cantidad'=SUM(DV.Cantidad), 'Total'=Sum(DV.Total), 'TotalDescuento'=Sum(DV.TotalDescuento), 'TotalDiscriminado'=Sum(DV.TotalDiscriminado), 'CantidadCaja'=SUM(DV.CantidadCaja) From DetalleVenta DV Join VentaLoteDistribucion VLD On DV.IDTransaccion=VLD.IDTransaccionVenta Where VLD.IDTransaccionLote=@IDTransaccion Group By IDProducto
		
		--Abrir
		Open db_cursor   
		fetch next from db_cursor into @vIDProducto, @vCantidad, @vTotal, @vTotalDescuento, @vTotalDiscriminado, @vCantidadCaja
		While @@FETCH_STATUS = 0 Begin  			  

			Insert Into DetalleLoteDistribucion(IDTransaccion, IDProducto, Cantidad, Total, TotalDescuento, TotalDiscriminado, CantidadCaja)		
			Values(@IDTransaccion, @vIDProducto, @vCantidad, @vTotal, @vTotalDescuento, @vTotalDiscriminado, @vCantidadCaja)
			
			fetch next from db_cursor into @vIDProducto, @vCantidad, @vTotal, @vTotalDescuento, @vTotalDiscriminado, @vCantidadCaja
		End   

		Close db_cursor   
		deallocate db_cursor
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
				
	End
	
End


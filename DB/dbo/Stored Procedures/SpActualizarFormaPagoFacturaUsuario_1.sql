﻿Create Procedure [dbo].[SpActualizarFormaPagoFacturaUsuario]

	--Entrada
	@IDFormaPagoFactura int,
	@IDUsuario int,
	@Activar bit
	
As

Begin

	Declare @vMensaje varchar(100)
	Declare @vProcesado bit
	
	Set @vMensaje = 'No se proceso'
	Set @vProcesado = 'False'
	
	If @Activar = 'True' Begin
		
		If Exists(Select * From FormaPagoFacturaUsuario Where IDFormaPagoFactura=@IDFormaPagoFactura And IDUsuario=@IDUsuario) Begin
			Set @vMensaje = 'El usuario ya esta registrado!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		Insert Into FormaPagoFacturaUsuario(IDFormaPagoFactura, IDUsuario)
		Values(@IDFormaPagoFactura, @IDUsuario)
		
		Set @vMensaje = 'Registro procesado!'
		Set @vProcesado = 'True'
		GoTo Salir
			
	End
	
	If @Activar = 'False' Begin
		
		If Not Exists(Select * From FormaPagoFacturaUsuario Where IDFormaPagoFactura=@IDFormaPagoFactura And IDUsuario=@IDUsuario) Begin
			Set @vMensaje = 'El usuario no esta registrado!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		Delete From FormaPagoFacturaUsuario
		Where IDFormaPagoFactura=@IDFormaPagoFactura And IDUsuario=@IDUsuario
		
		Set @vMensaje = 'Registro procesado!'
		Set @vProcesado = 'True'
		GoTo Salir
			
	End
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


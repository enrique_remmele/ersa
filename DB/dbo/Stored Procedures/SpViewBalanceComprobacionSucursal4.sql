﻿CREATE Procedure [dbo].[SpViewBalanceComprobacionSucursal4]

	--Entrada
	@Año smallint,
	@Mes tinyint,
	@IDSucursal int,
	
	@Factor int,
		
	@CuentaInicial varchar(50),
	@CuentaFinal varchar(50)
	
As

Begin

	--variables
	Declare @vID tinyint
	Declare @vM varchar(50)
	Declare @vCodigo varchar(50)
	Declare @vDenominacion varchar(200)
	Declare @vCategoria tinyint
	Declare @vImputable bit
	Declare @vDebito money
	Declare @vCredito money
	Declare @vSaldo money
	Declare @vDebitoAnterior money
	Declare @vCreditoAnterior money
	Declare @vSaldoAnterior money
	Declare @vDebitoActual money
	Declare @vCreditoActual money
	Declare @vSaldoActual money
	
	--Crear la tabla
	Create table #BalanceComprobacion4(ID tinyint,
								Año smallint,
								Mes tinyint,
								M varchar(50),
								Codigo varchar(50),
								Denominacion varchar(200),
								Categoria tinyint,
								Imputable bit,
								MostrarCodigo varchar(50),
								
								SaldoAnterior money,
								Debito money,
								Credito money,
								SaldoActual money
								)
								
	set @vID = (Select IsNull(MAX(ID)+1,1) From #BalanceComprobacion4)
	
	If @Factor = 0 Begin
		Set @Factor = 1
	End
	
	Declare db_cursor cursor for
	Select	CC.Codigo, CC.Descripcion, CC.Categoria, CC.Imputable	
	From VCuentaContable CC	
	--Where (SUBSTRING(CC.Codigo, 0, 2) Between @CuentaInicial And @CuentaFinal)	
	Open db_cursor   
	Fetch Next From db_cursor Into	@vCodigo, @vDenominacion, @vCategoria, @vImputable
	While @@FETCH_STATUS = 0 Begin 

		--Tipo de Cuenta
		Declare @vCuentaTipo varchar(50)
		Declare @vPrefijo varchar(50)

		Set @vPrefijo = SubString(@vCodigo, 0, 2)

		--Cuentas del Debe
		If @vPrefijo = '1' Or @vPrefijo = '5' Begin
			Set @vCuentaTipo = 'DEBE'
		End

		--Cuentas del Haber
		If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
			Set @vCuentaTipo = 'HABER'
		End

		Set @vDebitoAnterior = 0
		Set @vCreditoAnterior = 0
		
		--Saldo Anterior
		If @Mes > 1 Begin
			Set @vDebitoAnterior = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes<@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
			Set @vCreditoAnterior = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes<@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
		End
		
		--Saldo Actual
		Set @vDebito = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes=@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
		Set @vCredito = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes=@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
		
		If @vCuentaTipo = 'DEBE' Begin
			Set @vSaldoAnterior = IsNUll(@vDebitoAnterior  - @vCreditoAnterior,0)
			Set @vSaldoActual = @vSaldoAnterior + (@vDebito - @vCredito)
		End
		
		If @vCuentaTipo = 'HABER' Begin
			Set @vSaldoAnterior = IsNUll(@vCreditoAnterior-@vDebitoAnterior,0)
			Set @vSaldoActual = @vSaldoAnterior + (@vCredito - @vDebito)
		End
		
		--Transformar
		Set @vSaldoAnterior = @vSaldoAnterior / @Factor
		Set @vSaldoActual = @vSaldoActual / @Factor
		Set @vDebito = @vDebito / @Factor
		Set @vCredito = @vCredito / @Factor
				
		--Si ya existe, actualizar
		If Exists(Select * From #BalanceComprobacion4 Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo) Begin
		
			Update #BalanceComprobacion4 Set 
			SaldoAnterior=SaldoAnterior+@vSaldoAnterior,
			Debito=Debito+@vDebito,
			Credito=Credito+@vCredito,
			SaldoActual=SaldoActual+@vSaldoActual
			Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo
			
		End Else Begin
			Insert Into #BalanceComprobacion4(ID, Año, Mes, M, Codigo, Denominacion, Categoria, Imputable, SaldoAnterior, Debito, Credito, SaldoActual, MostrarCodigo)
			Values(@vID, @Año, @Mes, dbo.FMes(@Mes), @vCodigo, @vDenominacion, @vCategoria, @vImputable, @vSaldoAnterior, @vDebito, @vCredito, @vSaldoActual, '1')
		End
		
		

Siguiente:
		
		Fetch Next From db_cursor Into	@vCodigo, @vDenominacion, @vCategoria, @vImputable
		
	End

	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor		   		
	
	--RESULTADO DEL EJERCICIO
	Begin
		Declare @vIDSucursal int
		Declare @vCuentaContableSucursal varchar(50)
		
		Declare Sucursal_Cursor cursor for
		Select ID, CuentaContable From Sucursal Where ID=@IDSucursal
		Open Sucursal_Cursor   
		Fetch Next From Sucursal_Cursor Into @vIDSucursal, @vCuentaContableSucursal
		While @@FETCH_STATUS = 0 Begin
		
			Set @vDenominacion = (Select Descripcion From VCuentaContable Where Codigo=@vCuentaContableSucursal)
			Set @vCategoria = (Select Categoria From VCuentaContable Where Codigo=@vCuentaContableSucursal)
			Set @vImputable = (Select Imputable From VCuentaContable Where Codigo=@vCuentaContableSucursal)
			
			--Inicializar
			Set @vSaldoAnterior = 0
			
			--Obtener Saldos Anteriores
			If @Mes > 1 Begin
				Set @vSaldoAnterior = IsNull((Select Sum(Credito) - Sum(Debito) From PlanCuentaSaldo Where IDSucursal=@vIDSucursal And Año=@Año And Mes<@Mes And SubString(Cuenta, 0, 2)='4'),0) 
							         -IsNull((Select Sum(Debito) - Sum(Credito) From PlanCuentaSaldo Where IDSucursal=@vIDSucursal And Año=@Año And Mes<@Mes And SubString(Cuenta, 0, 2)='5'),0)
			End
			
			Set @vDebito = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where IDSucursal=@vIDSucursal And Año=@Año And Mes=@Mes And SubString(Cuenta, 0, 2)='4'),0) 
						  +IsNull((Select Sum(Debito) From PlanCuentaSaldo Where IDSucursal=@vIDSucursal And Año=@Año And Mes=@Mes And SubString(Cuenta, 0, 2)='5'),0)
			Set @vCredito = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where IDSucursal=@vIDSucursal And Año=@Año And Mes=@Mes And SubString(Cuenta, 0, 2)='4'),0) 
						  +IsNull((Select Sum(Credito) From PlanCuentaSaldo Where IDSucursal=@vIDSucursal And Año=@Año And Mes=@Mes And SubString(Cuenta, 0, 2)='5'),0)
						  				         
			Set @vSaldoActual = (@vSaldoAnterior + @vCredito) - @vDebito
			
			Insert Into #BalanceComprobacion4(ID, Año, Mes, M, Codigo, Denominacion, Categoria, Imputable, SaldoAnterior, Debito, Credito, SaldoActual, MostrarCodigo)
			Values(@vID, @Año, @Mes, dbo.FMes(@Mes), @vCuentaContableSucursal, @vDenominacion, @vCategoria, @vImputable, @vSaldoAnterior, @vDebito, @vCredito, @vSaldoActual, '1')
			
			Fetch Next From Sucursal_Cursor Into @vIDSucursal, @vCuentaContableSucursal
			
		End

		--Cierra el cursor
		Close Sucursal_Cursor   
		Deallocate Sucursal_Cursor	
	 
	End
	
	Select * From #BalanceComprobacion4 Where ID=@vID
End





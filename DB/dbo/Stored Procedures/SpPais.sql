﻿CREATE Procedure [dbo].[SpPais]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Nacionalidad varchar(50),
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Pais Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDPais tinyint
		set @vIDPais = (Select IsNull((Max(ID)+1), 1) From Pais)

		--Insertamos
		Insert Into Pais(ID, Descripcion, Nacionalidad, Estado)
		Values(@vIDPais, @Descripcion, @Nacionalidad, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PAIS', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From Pais Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Pais Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Pais Set Descripcion=@Descripcion, 
						Nacionalidad=@Nacionalidad,
						Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PAIS', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Pais Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con DEPARTAMENTO
		if exists(Select * From Departamento  Where IDPais=@ID) begin
			set @Mensaje = 'El registro tiene departamentos asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con CIUDAD
		if exists(Select * From Ciudad Where IDPais=@ID) begin
			set @Mensaje = 'El registro tiene ciudades asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con CLIENTES
		if exists(Select * From Cliente Where IDPais=@ID) begin
			set @Mensaje = 'El registro tiene clientes asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Pais 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PAIS', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


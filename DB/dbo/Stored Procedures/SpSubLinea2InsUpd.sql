﻿CREATE Procedure [dbo].[SpSubLinea2InsUpd]

	--Entrada
	@Descripcion varchar(100)
	
As

Begin

	Declare @vID int
	
	If @Descripcion = '' Begin
		Set @Descripcion = '---'
	End
	
	--Si existe
	If Exists(Select * From SubLinea2 Where Descripcion = @Descripcion) Begin
		Set @vID = (Select Top(1) ID From SubLinea2 Where Descripcion = @Descripcion)
		
	End Else Begin
		Set @vID = (Select IsNull(Max(ID)+1,1) From SubLinea2)
		Insert Into SubLinea2(ID, Descripcion, Estado)
		Values(@vID, @Descripcion, 'True')
	End
	
	return @vID
	
End


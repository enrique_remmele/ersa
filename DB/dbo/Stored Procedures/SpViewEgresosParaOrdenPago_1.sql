﻿CREATE Procedure [dbo].[SpViewEgresosParaOrdenPago]

	--Entrada
		@IDSucursal tinyint,
		@SoloAPagar bit = 'False',
		@IDTransaccion int = 0
	
As
Begin
	Declare @vCuentaContable varchar(10)='%%'

	if @IDTransaccion <> 0 Begin
		Set @vCuentaContable = isnull((Select top(1)
		DA.Codigo
		from vdetalleasiento DA
		join VOrdenPago OP on DA.IDTransaccion = OP.IDTransaccion
		Where OP.IDTransaccion=@IDTransaccion
		and Da.Debito > 1
		and (Da.Descripcion like 'Proveedo%' or Da.Descripcion like 'Anticipo%'or Da.Descripcion like 'Fondo Fijo%')),'%%')

		if @vCuentaContable = '%%' begin
			Set @vCuentaContable = isnull((Select Top(1)
			P.CuentaContableAnticipo
			from vOrdenPago OP 
			Join vProveedor P on OP.IDProveedor = P.ID
			Where OP.IDTransaccion=@IDTransaccion),'%%')

		end

	end
end
if @vCuentaContable <> '%%' Begin

	---Compras
	Select 
	'Tipo'='COMPRA',
	C.IDTransaccion,
	C.Numero,
	C.NroComprobante, 
	C.TipoComprobante,
	'T. Comp.'=C.TipoComprobante, 
	'Comprobante'=C.TipoComprobante + ' ' + C.NroComprobante,
	C.Timbrado,
	'NumeroTC'=C.Numero,
	
	--Proveedor
	'Codigo'=P.Referencia,
	C.Proveedor, 
	C.IDProveedor,
	P.Retentor,
	P.TipoProveedor,
	P.Ruc,
	P.Direccion,

	--Cuenta del Proveedor
	'IDCuentaContable'=ISNULL(P.IDCuentaContableCompra, 0), 
	'CodigoCuenta'=(IsNull((Select convert(varchar(50), CC.Codigo) From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'CuentaContable'=(IsNull((Select CC.Descripcion From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'Alias'=(IsNull((Select CC.Alias From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),

	C.Fecha, 
	'Fec'=IsNull((Convert(varchar(15), C.Fecha,5)), '---'),
	'FechaVencimiento'=Convert(date, IsNull(C.FechaVencimiento, C.Fecha)), 
	'Fec. Venc.'=IsNull((Convert(varchar(15), C.FechaVencimiento,5)), '---'),

	C.Total, 
	C.Saldo, 
	'Importe'=C.ImporteAPagar,
	'IVA'= Case when C.IDMoneda = 1 then round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion and NCPC.ID=0
			Join VCompra CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VCompra CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),0)
			--Si es moneda extranjera
			Else round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion and NCPC.ID=0
			Join vCompra CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VCompra CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),2)end,
	'RetencionIVA'= 
			Case when ((select count(*) from OrdenPagoEgreso where IDTransaccionEgreso = C.IDTransaccion and Procesado = 1)=0) then
			(Case when C.IDMoneda = 1 then Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion and NCPC.ID=0
			Join VCompra CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VCompra CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),2)
			--Moneda Extranjera
			Else Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion and NCPC.ID=0
			Join VCompra CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VCompra CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),2) 
			end)
			Else
			0 end,
	'RetencionRenta'=ISNULL(C.RetencionRenta,0),
	--'Retener'=(Case When (C.Total) <> C.Saldo Then 'False' Else 'True' End),
	--se modifica porque no debe retener solo cuando tiene un pago, pero cuando tiene una nota de credito si debe retener
	'Retener'=(Case When (Select Count(*) from ordenpagoegreso where idtransaccionegreso = C.IDTransaccion) > 0 Then 'False' Else 'True' End),
	
	C.TotalImpuesto,
	C.TotalDiscriminado,
	C.IDMoneda, 
	C.Moneda, 
	C.Cotizacion, 
	'Cotiz.'=C.Cotizacion, 
	C.Observacion, 
	C.Credito, 
	'Cuota'=1,
	'CantidadCuota'=1,
	'Pago'='1/1',

	'Seleccionado'=Convert(bit, 'False'), 
	'Cancelar'=Convert(bit, 'False'), 
	'Efectivo'='False', 
	'Cheque'='True',
		
	'FondoFijo'='False',
	C.Pagar,
	C.ImporteAPagar,
	C.IDCuentaBancaria,
	C.CuentaBancaria,
	C.ObservacionPago,
	'EstadoProceso'='PENDIENTE',
	0 as IdGrupo,
	(select convert(Date,fecha) from Transaccion where id = C.IDTransaccion) as FechaTransaccion,
	'IVA10'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 1 ),0),
	'IVA5'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 2 ),0)

	From VCompra C
	Join VProveedor P On C.IDProveedor=P.ID
	join vdetalleasientoAgrupado DA on C.IDTransaccion = DA.IDTransaccion and DA.Credito > 0 and Da.Codigo like @vCuentaContable
	Where C.Cancelado = 'False' 
	And (Case When (@SoloAPagar) = 'True' Then C.Pagar Else 'False' End) = @SoloAPagar
	and c.saldo > 0.99

	Union All
	
	--Gasto Tipo Comprobante
	Select 
	'Tipo'='GASTO',
	C.IDTransaccion, 
	C.Numero,
	C.NroComprobante, 
	C.TipoComprobante,
	'T. Comp.'=C.TipoComprobante, 
	'Comprobante'=C.TipoComprobante + ' ' + C.NroComprobante,
	'Timbrado'=C.NroTimbrado,
	'NumeroTC'=(Select GTC.Numero From GastoTipoComprobante GTC Where GTC.IDTransaccion=C.IDTransaccion),
	
	--Proveedor
	'Codigo'=P.Referencia,
	C.Proveedor, 
	C.IDProveedor,
	P.Retentor,
	P.TipoProveedor,
	P.Ruc,
	P.Direccion,

	--Cuenta del Proveedor
	'IDCuentaContable'=ISNULL(P.IDCuentaContableCompra, 0), 
	'CodigoCuenta'=(IsNull((Select convert(varchar(50), CC.Codigo) From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'CuentaContable'=(IsNull((Select CC.Descripcion From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'Alias'=(IsNull((Select CC.Alias From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	
	C.Fecha, 
	'Fec'=IsNull((Convert(varchar(15), C.Fecha,5)), '---'),
	'FechaVencimiento'=Convert(date, IsNull(C.FechaVencimiento, C.Fecha)), 
	'Fec. Venc.'=IsNull((Convert(varchar(15), C.FechaVencimiento,5)), '---'),

	C.Total, 
	C.Saldo, 
	'Importe'=(Case When (IsNull(C.ImporteAPagar, 0)) = 0 Then C.Saldo Else C.ImporteAPagar End),
	'IVA'= Case when C.IDMoneda = 1 then round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),0)
			--Si es moneda extranjera
			Else round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),2)end,
	'RetencionIVA'= 
			Case when ((select count(*) from OrdenPagoEgreso where IDTransaccionEgreso = C.IDTransaccion and Procesado = 1)=0) then
			(Case when C.IDMoneda = 1 then Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),0)
			--Moneda Extranjera
			Else Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),2) end)
			Else
			0 end,
	'RetencionRenta'=ISNULL(C.RetencionRenta,0),
	--'Retener'=(Case When (C.Total) <> C.Saldo Then 'False' Else 'True' End),
	--se modifica porque no debe retener solo cuando tiene un pago, pero cuando tiene una nota de credito si debe retener
	'Retener'=(Case When (Select Count(*) from ordenpagoegreso where idtransaccionegreso = C.IDTransaccion) > 0 Then 'False' Else 'True' End),
	
	C.TotalImpuesto,
	C.TotalDiscriminado,
	C.IDMoneda, 
	C.Moneda, 
	C.Cotizacion, 
	'Cotiz.'=C.Cotizacion,
	C.Observacion, 
	C.Credito, 
	'Cuota'=1,
	'CantidadCuota'=1,
	'Pago'='1/1',

	'Seleccionado'='False', 'Cancelar'='False', C.Efectivo, C.Cheque,
	'FondoFijo'='False',
	C.Pagar,
	C.ImporteAPagar,
	C.IDCuentaBancaria,
	C.CuentaBancaria,
	C.ObservacionPago,
	'EstadoProceso'='PENDIENTE',
	(Case When (IsNull(IdGrupo, 0)) = 0 Then 0 Else IdGrupo End) as IdGrupo,
	convert(date,C.FechaTransaccion) as FechaTransaccion,
	'IVA10'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 1 ),0),
	'IVA5'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 2 ),0)
		
	From VGastoTipoComprobante C
	join vdetalleasientoAgrupado DA on C.IDTransaccion = DA.IDTransaccion and DA.Credito > 0 and Da.Codigo like @vCuentaContable
	Join VProveedor P On C.IDProveedor=P.ID
	Where C.Cancelado = 'False' And Cuota=0
	And (Case When (@SoloAPagar) = 'True' Then C.Pagar Else 'False' End) = @SoloAPagar
	and c.saldo > 0.99

	Union All
		
	--Gasto Fondo Fijo
	Select 
	'Tipo'='FONDO FIJO',
	C.IDTransaccion, 
	C.Numero,
	C.NroComprobante, 
	C.TipoComprobante,
	'T. Comp.'=C.TipoComprobante, 
	'Comprobante'=C.TipoComprobante + ' ' + C.NroComprobante,
	'Timbrado'=C.NroTimbrado,
	'NumeroTC'=(Select GFF.Numero From GastoFondoFijo GFF Where GFF.IDTransaccion=C.IDTransaccion),
	
	--Proveedor
	'Codigo'=P.Referencia,
	C.Proveedor, 
	C.IDProveedor,
	P.Retentor,
	P.TipoProveedor,
	P.Ruc,
	P.Direccion,
	--Cuenta del Proveedor
	'IDCuentaContable'=ISNULL(P.IDCuentaContableCompra, 0), 
	'CodigoCuenta'=(IsNull((Select convert(varchar(50), CC.Codigo) From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'CuentaContable'=(IsNull((Select CC.Descripcion From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'Alias'=(IsNull((Select CC.Alias From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	
	C.Fecha, 
	'Fec'=IsNull((Convert(varchar(15), C.Fecha,5)), '---'),
	'FechaVencimiento'=Convert(date, IsNull(C.FechaVencimiento, C.Fecha)), 
	'Fec. Venc.'=IsNull((Convert(varchar(15), C.FechaVencimiento,5)), '---'),
	
	C.Total, 
	C.Saldo, 
	'Importe'=(Case When (IsNull(C.ImporteAPagar, 0)) = 0 Then C.Saldo Else C.ImporteAPagar End),
	'IVA'= Case when C.IDMoneda = 1 then round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion 
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),0)
			--Si es moneda extranjera
			Else round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),2)end,
'RetencionIVA'= 
			Case when ((select count(*) from OrdenPagoEgreso where IDTransaccionEgreso = C.IDTransaccion and Procesado = 1)=0) then
			(Case when C.IDMoneda = 1 then Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),0)
			--Moneda Extranjera
			Else Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),2) end)
			Else
			0 end,
	'RetencionRenta'=ISNULL(C.RetencionRenta,0),
	--'Retener'=(Case When (C.Total) <> C.Saldo Then 'False' Else 'True' End),
	--se modifica porque no debe retener solo cuando tiene un pago, pero cuando tiene una nota de credito si debe retener
	'Retener'=(Case When (Select Count(*) from ordenpagoegreso where idtransaccionegreso = C.IDTransaccion) > 0 Then 'False' Else 'True' End),
	
	C.TotalImpuesto,
	C.TotalDiscriminado,
	C.IDMoneda, 
	C.Moneda, 
	C.Cotizacion, 
	'Cotiz.'=C.Cotizacion, 
	C.Observacion, 
	C.Credito,
	'Cuota'=1, 
	'CantidadCuota'=1,
	'Pago'='1/1',

	'Seleccionado'='False', 
	'Cancelar'='False', 
	C.Efectivo, 
	C.Cheque,
	'FondoFijo'='True',
	'True', --C.Pagar,
	C.Saldo, --C.ImporteAPagar,
	C.IDCuentaBancaria,
	C.CuentaBancaria,
	C.ObservacionPago,
	'EstadoProceso'='PENDIENTE',
	(Case When (IsNull(IdGrupo, 0)) = 0 Then 0 Else IdGrupo End) as IdGrupo,
	convert(date,C.FechaTransaccion) as FechaTransaccion,
	'IVA10'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 1 ),0),
	'IVA5'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 2 ),0)
		
	From VGastoFondoFijo C
	join vdetalleasientoAgrupado DA on C.IDTransaccion = DA.IDTransaccion and DA.Credito > 0 and Da.Codigo like @vCuentaContable
	Join VProveedor P On C.IDProveedor=P.ID
	Where C.Cancelado = 'False' 
	And C.CajaChica='True' And Cuota=0
	--And (Case When (@SoloAPagar) = 'True' Then C.Pagar Else 'False' End) = @SoloAPagar
	 and c.saldo > 0.99
		
	Union All
	
	--Vales
	Select 
	'Tipo'='VALE',
	C.IDTransaccion, 
	C.Numero,
	'NroComprobante'=convert(Varchar(50),C.NroComprobante), 
	'TipoComprobante'=Cod,
	'T. Comp.'=Cod, 
	'Comprobante'=C.Cod + ' ' + convert(Varchar(50),C.NroComprobante),
	'Timbrado'='',
	'NumeroTC'=C.Numero,
	'0000',
	C.Nombre, 
	0,
	'False',
	'---',
	'Ruc'='',
	'Direccion'='',
	'IDCuentaContable'=0, 
	'CodigoCuenta'='',
	'CuentaContable'='',
	'Alias'='',
	C.Fecha,
	'Fec'=IsNull((Convert(varchar(15), C.Fecha,5)), '---'),
	'FechaVencimiento'=Convert(date, C.Fecha),
	'Fec. Venc.'=IsNull((Convert(varchar(15), C.Fecha,5)), '---'),
	
	C.Total, 
	C.Saldo, 
	'Importe'=(Case When (IsNull(C.ImporteAPagar, 0)) = 0 Then C.Saldo Else C.ImporteAPagar End),
	'IVA'= 0,
	'RetencionIVA'= 0,
	'RetencionRenta'=0,
	'Retener'='False',

	0,
	0,
	C.IDMoneda, 
	C.Moneda, 
	C.Cotizacion, 
	'Cotiz.'=C.Cotizacion, 
	'', 
	NULL, 
	'Cuota'=1,
	'CantidadCuota'=1,
	'Pago'='1/1',

	'Seleccionado'='False', 
	'Cancelar'='False', 
	NULL, 
	NULL,
	'FondoFijo'='True',
	C.Pagar,
	C.ImporteAPagar,
	C.IDCuentaBancaria,
	C.CuentaBancaria,
	C.ObservacionPago,
	'EstadoProceso'='PENDIENTE',
	(Case When (IsNull(IdGrupo, 0)) = 0 Then 0 Else IdGrupo End) as IdGrupo,
	convert(date,C.FechaTransaccion) as FechaTransaccion,
	'IVA10'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 1 ),0),
	'IVA5'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 2 ),0)
	From VVale C
	join vdetalleasientoAgrupado DA on C.IDTransaccion = DA.IDTransaccion and DA.Credito > 0 and Da.Codigo like @vCuentaContable
	Where  C.Pagado='False' And C.Anulado ='False'
	And (Case When (@SoloAPagar) = 'True' Then C.Pagar Else 'False' End) = @SoloAPagar
	and c.saldo > 0.99

	Union All
	
	--Gasto Tipo Comprobante / Cuotas
	Select 
	'Tipo'='GASTO',
	C.IDTransaccion, 
	C.Numero,
	'NroComprobante'=C.NroComprobante + '/' + Convert(varchar(50), CU.ID), 
	C.TipoComprobante,
	'T. Comp.'=C.TipoComprobante, 
	'Comprobante'=C.TipoComprobante + ' ' + C.NroComprobante + '/' + Convert(varchar(50), CU.ID),
	'Timbrado'=C.NroTimbrado,
	'NumeroTC'=(Select GTC.Numero From GastoTipoComprobante GTC Where GTC.IDTransaccion=C.IDTransaccion),
	
	--Proveedor
	'Codigo'=P.Referencia,
	C.Proveedor, 
	C.IDProveedor,
	P.Retentor,
	P.TipoProveedor,
	P.Ruc,
	P.Direccion,

	--Cuenta del Proveedor
	'IDCuentaContable'=ISNULL(P.IDCuentaContableCompra, 0), 
	'CodigoCuenta'=(IsNull((Select convert(varchar(50), CC.Codigo) From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'CuentaContable'=(IsNull((Select CC.Descripcion From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'Alias'=(IsNull((Select CC.Alias From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	
	C.Fecha, 
	'Fec'=IsNull((Convert(varchar(15), C.Fecha,5)), '---'),
	'FechaVencimiento'=Convert(Date, IsNull(CU.Vencimiento, C.Fecha)), 
	'Fec. Venc.'=IsNull((Convert(varchar(15), CU.Vencimiento,5)), '---'),

	C.Total, 
	CU.Saldo, 
	'Importe'=(Case When (IsNull(CU.ImporteAPagar, 0)) = 0 Then CU.Saldo Else CU.ImporteAPagar End),
	'IVA'= Case when C.IDMoneda = 1 then round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),0)
			--Si es moneda extranjera
			Else round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),2)end,
'RetencionIVA'= 
			Case when ((select count(*) from OrdenPagoEgreso where IDTransaccionEgreso = C.IDTransaccion and Procesado = 1)=0) then
			(Case when C.IDMoneda = 1 then Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),0)
			--Moneda Extranjera
			Else Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),2) end)
			Else
			0 end,
	'RetencionRenta'=ISNULL(C.RetencionRenta,0),
	--'Retener'=(Case When (C.Total) <> C.Saldo Then 'False' Else 'True' End),
	--se modifica porque no debe retener solo cuando tiene un pago, pero cuando tiene una nota de credito si debe retener
	'Retener'=(Case When (Select Count(*) from ordenpagoegreso where idtransaccionegreso = C.IDTransaccion) > 0 Then 'False' Else 'True' End),
	
	C.TotalImpuesto,
	C.TotalDiscriminado,
	C.IDMoneda, 
	C.Moneda, 
	C.Cotizacion,
	'Cotiz.'=C.Cotizacion,  
	C.Observacion, 
	C.Credito, 
	'Cuota'=CU.ID,
	'CantidadCuota'=C.Cuota,
	'Pago'=Convert(varchar(50), CU.ID) + '/' + Convert(varchar(50), C.Cuota),

	'Seleccionado'='False', 'Cancelar'='False', C.Efectivo, C.Cheque,
	'FondoFijo'='False',
	'Pagar'=IsNull(CU.Pagar, 'False'),
	'ImporteAPagar'=IsNull(CU.ImporteAPagar, 0),
	CU.IDCuentaBancaria,
	CB.Descripcion,
	CU.ObservacionPago,
	'EstadoProceso'='PENDIENTE',
	(Case When (IsNull(IdGrupo, 0)) = 0 Then 0 Else IdGrupo End) as IdGrupo,
	convert(date,C.FechaTransaccion) as fechaTransaccion,
	'IVA10'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 1 ),0),
	'IVA5'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 2 ),0)
		
	From VGastoTipoComprobante C
	join vdetalleasientoAgrupado DA on C.IDTransaccion = DA.IDTransaccion and DA.Credito > 0 and Da.Codigo like @vCuentaContable
	Join VProveedor P On C.IDProveedor=P.ID
	Join Cuota CU On C.IDTransaccion=CU.IDTransaccion
	Left Outer Join VCuentaBancaria CB On CU.IDCuentaBancaria=CB.ID
	Where CU.Cancelado = 'False' And Cuota>0	
	--Esto es solo si se quiere mostrar el primero no pagado
	--And CU.ID=(Select Min(CU2.ID) From Cuota CU2 Where CU2.IDTransaccion=CU.IDTransaccion And CU2.Cancelado='False')
	And (Case When (@SoloAPagar) = 'True' Then CU.Pagar Else 'False' End) = @SoloAPagar
	and c.saldo > 0.99

		

End
else begin
---Compras
	Select 
	'Tipo'='COMPRA',
	C.IDTransaccion,
	C.Numero,
	C.NroComprobante, 
	C.TipoComprobante,
	'T. Comp.'=C.TipoComprobante, 
	'Comprobante'=C.TipoComprobante + ' ' + C.NroComprobante,
	C.Timbrado,
	'NumeroTC'=C.Numero,
	
	--Proveedor
	'Codigo'=P.Referencia,
	C.Proveedor, 
	C.IDProveedor,
	P.Retentor,
	P.TipoProveedor,
	P.Ruc,
	P.Direccion,

	--Cuenta del Proveedor
	'IDCuentaContable'=ISNULL(P.IDCuentaContableCompra, 0), 
	'CodigoCuenta'=(IsNull((Select convert(varchar(50), CC.Codigo) From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'CuentaContable'=(IsNull((Select CC.Descripcion From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'Alias'=(IsNull((Select CC.Alias From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),

	C.Fecha, 
	'Fec'=IsNull((Convert(varchar(15), C.Fecha,5)), '---'),
	'FechaVencimiento'=Convert(date, IsNull(C.FechaVencimiento, C.Fecha)), 
	'Fec. Venc.'=IsNull((Convert(varchar(15), C.FechaVencimiento,5)), '---'),

	C.Total, 
	C.Saldo, 
	'Importe'=C.ImporteAPagar,
	'IVA'= Case when C.IDMoneda = 1 then round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion and NCPC.ID=0
			Join VCompra CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VCompra CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),0)
			--Si es moneda extranjera
			Else round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion and NCPC.ID=0
			Join vCompra CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VCompra CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),2)end,
	'RetencionIVA'= 
			Case when ((select count(*) from OrdenPagoEgreso where IDTransaccionEgreso = C.IDTransaccion and Procesado = 1)=0) then
			(Case when C.IDMoneda = 1 then Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion and NCPC.ID=0
			Join VCompra CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VCompra CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),2)
			--Moneda Extranjera
			Else Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion and NCPC.ID=0
			Join VCompra CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VCompra CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),2) 
			end)
			Else
			0 end,
	'RetencionRenta'=ISNULL(C.RetencionRenta,0),
	--'Retener'=(Case When (C.Total) <> C.Saldo Then 'False' Else 'True' End),
	--se modifica porque no debe retener solo cuando tiene un pago, pero cuando tiene una nota de credito si debe retener
	'Retener'=(Case When (Select Count(*) from ordenpagoegreso where idtransaccionegreso = C.IDTransaccion) > 0 Then 'False' Else 'True' End),
	
	C.TotalImpuesto,
	C.TotalDiscriminado,
	C.IDMoneda, 
	C.Moneda, 
	C.Cotizacion, 
	'Cotiz.'=C.Cotizacion, 
	C.Observacion, 
	C.Credito, 
	'Cuota'=1,
	'CantidadCuota'=1,
	'Pago'='1/1',

	'Seleccionado'=Convert(bit, 'False'), 
	'Cancelar'=Convert(bit, 'False'), 
	'Efectivo'='False', 
	'Cheque'='True',
		
	'FondoFijo'='False',
	C.Pagar,
	C.ImporteAPagar,
	C.IDCuentaBancaria,
	C.CuentaBancaria,
	C.ObservacionPago,
	'EstadoProceso'='PENDIENTE',
	0 as IdGrupo,
	(select convert(Date,fecha) from Transaccion where id = C.IDTransaccion) as FechaTransaccion,
	'IVA10'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 1 ),0),
	'IVA5'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 2 ),0)

	From VCompra C
	Join VProveedor P On C.IDProveedor=P.ID
	--join vdetalleasientoAgrupado DA on C.IDTransaccion = DA.IDTransaccion and DA.Credito > 0 and Da.Codigo like @vCuentaContable
	Where C.Cancelado = 'False' 
	And (Case When (@SoloAPagar) = 'True' Then C.Pagar Else 'False' End) = @SoloAPagar
	and c.saldo > 0.99

	Union All
	
	--Gasto Tipo Comprobante
	Select 
	'Tipo'='GASTO',
	C.IDTransaccion, 
	C.Numero,
	C.NroComprobante, 
	C.TipoComprobante,
	'T. Comp.'=C.TipoComprobante, 
	'Comprobante'=C.TipoComprobante + ' ' + C.NroComprobante,
	'Timbrado'=C.NroTimbrado,
	'NumeroTC'=(Select GTC.Numero From GastoTipoComprobante GTC Where GTC.IDTransaccion=C.IDTransaccion),
	
	--Proveedor
	'Codigo'=P.Referencia,
	C.Proveedor, 
	C.IDProveedor,
	P.Retentor,
	P.TipoProveedor,
	P.Ruc,
	P.Direccion,

	--Cuenta del Proveedor
	'IDCuentaContable'=ISNULL(P.IDCuentaContableCompra, 0), 
	'CodigoCuenta'=(IsNull((Select convert(varchar(50), CC.Codigo) From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'CuentaContable'=(IsNull((Select CC.Descripcion From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'Alias'=(IsNull((Select CC.Alias From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	
	C.Fecha, 
	'Fec'=IsNull((Convert(varchar(15), C.Fecha,5)), '---'),
	'FechaVencimiento'=Convert(date, IsNull(C.FechaVencimiento, C.Fecha)), 
	'Fec. Venc.'=IsNull((Convert(varchar(15), C.FechaVencimiento,5)), '---'),

	C.Total, 
	C.Saldo, 
	'Importe'=(Case When (IsNull(C.ImporteAPagar, 0)) = 0 Then C.Saldo Else C.ImporteAPagar End),
	'IVA'= Case when C.IDMoneda = 1 then round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),0)
			--Si es moneda extranjera
			Else round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),2)end,
	'RetencionIVA'= 
			Case when ((select count(*) from OrdenPagoEgreso where IDTransaccionEgreso = C.IDTransaccion and Procesado = 1)=0) then
			(Case when C.IDMoneda = 1 then Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),0)
			--Moneda Extranjera
			Else Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),2) end)
			Else
			0 end,
	'RetencionRenta'=ISNULL(C.RetencionRenta,0),
	--'Retener'=(Case When (C.Total) <> C.Saldo Then 'False' Else 'True' End),
	--se modifica porque no debe retener solo cuando tiene un pago, pero cuando tiene una nota de credito si debe retener
	'Retener'=(Case When (Select Count(*) from ordenpagoegreso where idtransaccionegreso = C.IDTransaccion) > 0 Then 'False' Else 'True' End),
	
	C.TotalImpuesto,
	C.TotalDiscriminado,
	C.IDMoneda, 
	C.Moneda, 
	C.Cotizacion, 
	'Cotiz.'=C.Cotizacion,
	C.Observacion, 
	C.Credito, 
	'Cuota'=1,
	'CantidadCuota'=1,
	'Pago'='1/1',

	'Seleccionado'='False', 'Cancelar'='False', C.Efectivo, C.Cheque,
	'FondoFijo'='False',
	C.Pagar,
	C.ImporteAPagar,
	C.IDCuentaBancaria,
	C.CuentaBancaria,
	C.ObservacionPago,
	'EstadoProceso'='PENDIENTE',
	(Case When (IsNull(IdGrupo, 0)) = 0 Then 0 Else IdGrupo End) as IdGrupo,
	convert(date,C.FechaTransaccion) as FechaTransaccion,
	'IVA10'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 1 ),0),
	'IVA5'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 2 ),0)
		
	From VGastoTipoComprobante C
	--join vdetalleasientoAgrupado DA on C.IDTransaccion = DA.IDTransaccion and DA.Credito > 0 and Da.Codigo like @vCuentaContable
	Join VProveedor P On C.IDProveedor=P.ID
	Where C.Cancelado = 'False' And Cuota=0
	And (Case When (@SoloAPagar) = 'True' Then C.Pagar Else 'False' End) = @SoloAPagar
	and c.saldo > 0.99

	Union All
		
	--Gasto Fondo Fijo
	Select 
	'Tipo'='FONDO FIJO',
	C.IDTransaccion, 
	C.Numero,
	C.NroComprobante, 
	C.TipoComprobante,
	'T. Comp.'=C.TipoComprobante, 
	'Comprobante'=C.TipoComprobante + ' ' + C.NroComprobante,
	'Timbrado'=C.NroTimbrado,
	'NumeroTC'=(Select GFF.Numero From GastoFondoFijo GFF Where GFF.IDTransaccion=C.IDTransaccion),
	
	--Proveedor
	'Codigo'=P.Referencia,
	C.Proveedor, 
	C.IDProveedor,
	P.Retentor,
	P.TipoProveedor,
	P.Ruc,
	P.Direccion,
	--Cuenta del Proveedor
	'IDCuentaContable'=ISNULL(P.IDCuentaContableCompra, 0), 
	'CodigoCuenta'=(IsNull((Select convert(varchar(50), CC.Codigo) From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'CuentaContable'=(IsNull((Select CC.Descripcion From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'Alias'=(IsNull((Select CC.Alias From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	
	C.Fecha, 
	'Fec'=IsNull((Convert(varchar(15), C.Fecha,5)), '---'),
	'FechaVencimiento'=Convert(date, IsNull(C.FechaVencimiento, C.Fecha)), 
	'Fec. Venc.'=IsNull((Convert(varchar(15), C.FechaVencimiento,5)), '---'),
	
	C.Total, 
	C.Saldo, 
	'Importe'=(Case When (IsNull(C.ImporteAPagar, 0)) = 0 Then C.Saldo Else C.ImporteAPagar End),
	'IVA'= Case when C.IDMoneda = 1 then round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),0)
			--Si es moneda extranjera
			Else round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),2)end,
'RetencionIVA'= 
			Case when ((select count(*) from OrdenPagoEgreso where IDTransaccionEgreso = C.IDTransaccion and Procesado = 1)=0) then
			(Case when C.IDMoneda = 1 then Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),0)
			--Moneda Extranjera
			Else Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoFondoFijo CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),2) end)
			Else
			0 end,
	'RetencionRenta'=ISNULL(C.RetencionRenta,0),
	--'Retener'=(Case When (C.Total) <> C.Saldo Then 'False' Else 'True' End),
	--se modifica porque no debe retener solo cuando tiene un pago, pero cuando tiene una nota de credito si debe retener
	'Retener'=(Case When (Select Count(*) from ordenpagoegreso where idtransaccionegreso = C.IDTransaccion) > 0 Then 'False' Else 'True' End),
	
	C.TotalImpuesto,
	C.TotalDiscriminado,
	C.IDMoneda, 
	C.Moneda, 
	C.Cotizacion, 
	'Cotiz.'=C.Cotizacion, 
	C.Observacion, 
	C.Credito,
	'Cuota'=1, 
	'CantidadCuota'=1,
	'Pago'='1/1',

	'Seleccionado'='False', 
	'Cancelar'='False', 
	C.Efectivo, 
	C.Cheque,
	'FondoFijo'='True',
	'True', --C.Pagar,
	C.Saldo, --C.ImporteAPagar,
	C.IDCuentaBancaria,
	C.CuentaBancaria,
	C.ObservacionPago,
	'EstadoProceso'='PENDIENTE',
	(Case When (IsNull(IdGrupo, 0)) = 0 Then 0 Else IdGrupo End) as IdGrupo,
	convert(date,C.FechaTransaccion) as FechaTransaccion,
	'IVA10'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 1 ),0),
	'IVA5'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 2 ),0)
		
	From VGastoFondoFijo C
	--join vdetalleasientoAgrupado DA on C.IDTransaccion = DA.IDTransaccion and DA.Credito > 0 and Da.Codigo like @vCuentaContable
	Join VProveedor P On C.IDProveedor=P.ID
	Where C.Cancelado = 'False' 
	And C.CajaChica='True' And Cuota=0
	--And (Case When (@SoloAPagar) = 'True' Then C.Pagar Else 'False' End) = @SoloAPagar
	 and c.saldo > 0.99
		
	Union All
	
	--Vales
	Select 
	'Tipo'='VALE',
	C.IDTransaccion, 
	C.Numero,
	'NroComprobante'=convert(Varchar(50),C.NroComprobante), 
	'TipoComprobante'=Cod,
	'T. Comp.'=Cod, 
	'Comprobante'=C.Cod + ' ' + convert(Varchar(50),C.NroComprobante),
	'Timbrado'='',
	'NumeroTC'=C.Numero,
	'0000',
	C.Nombre, 
	0,
	'False',
	'---',
	'Ruc'='',
	'Direccion'='',
	'IDCuentaContable'=0, 
	'CodigoCuenta'='',
	'CuentaContable'='',
	'Alias'='',
	C.Fecha,
	'Fec'=IsNull((Convert(varchar(15), C.Fecha,5)), '---'),
	'FechaVencimiento'=Convert(date, C.Fecha),
	'Fec. Venc.'=IsNull((Convert(varchar(15), C.Fecha,5)), '---'),
	
	C.Total, 
	C.Saldo, 
	'Importe'=(Case When (IsNull(C.ImporteAPagar, 0)) = 0 Then C.Saldo Else C.ImporteAPagar End),
	'IVA'= 0,
	'RetencionIVA'= 0,
	'RetencionRenta'=0,
	'Retener'='False',

	0,
	0,
	C.IDMoneda, 
	C.Moneda, 
	C.Cotizacion, 
	'Cotiz.'=C.Cotizacion, 
	'', 
	NULL, 
	'Cuota'=1,
	'CantidadCuota'=1,
	'Pago'='1/1',

	'Seleccionado'='False', 
	'Cancelar'='False', 
	NULL, 
	NULL,
	'FondoFijo'='True',
	C.Pagar,
	C.ImporteAPagar,
	C.IDCuentaBancaria,
	C.CuentaBancaria,
	C.ObservacionPago,
	'EstadoProceso'='PENDIENTE',
	(Case When (IsNull(IdGrupo, 0)) = 0 Then 0 Else IdGrupo End) as IdGrupo,
	convert(date,C.FechaTransaccion) as FechaTransaccion,
	'IVA10'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 1 ),0),
	'IVA5'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 2 ),0)
	From VVale C
	--join vdetalleasientoAgrupado DA on C.IDTransaccion = DA.IDTransaccion and DA.Credito > 0 and Da.Codigo like @vCuentaContable
	Where  C.Pagado='False' And C.Anulado ='False'
	And (Case When (@SoloAPagar) = 'True' Then C.Pagar Else 'False' End) = @SoloAPagar
	and c.saldo > 0.99

	Union All
	
	--Gasto Tipo Comprobante / Cuotas
	Select 
	'Tipo'='GASTO',
	C.IDTransaccion, 
	C.Numero,
	'NroComprobante'=C.NroComprobante + '/' + Convert(varchar(50), CU.ID), 
	C.TipoComprobante,
	'T. Comp.'=C.TipoComprobante, 
	'Comprobante'=C.TipoComprobante + ' ' + C.NroComprobante + '/' + Convert(varchar(50), CU.ID),
	'Timbrado'=C.NroTimbrado,
	'NumeroTC'=(Select GTC.Numero From GastoTipoComprobante GTC Where GTC.IDTransaccion=C.IDTransaccion),
	
	--Proveedor
	'Codigo'=P.Referencia,
	C.Proveedor, 
	C.IDProveedor,
	P.Retentor,
	P.TipoProveedor,
	P.Ruc,
	P.Direccion,

	--Cuenta del Proveedor
	'IDCuentaContable'=ISNULL(P.IDCuentaContableCompra, 0), 
	'CodigoCuenta'=(IsNull((Select convert(varchar(50), CC.Codigo) From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'CuentaContable'=(IsNull((Select CC.Descripcion From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	'Alias'=(IsNull((Select CC.Alias From VCuentaContable CC Where CC.ID=P.IDCuentaContableCompra), '')),
	
	C.Fecha, 
	'Fec'=IsNull((Convert(varchar(15), C.Fecha,5)), '---'),
	'FechaVencimiento'=Convert(Date, IsNull(CU.Vencimiento, C.Fecha)), 
	'Fec. Venc.'=IsNull((Convert(varchar(15), CU.Vencimiento,5)), '---'),

	C.Total, 
	CU.Saldo, 
	'Importe'=(Case When (IsNull(CU.ImporteAPagar, 0)) = 0 Then CU.Saldo Else CU.ImporteAPagar End),
	'IVA'= Case when C.IDMoneda = 1 then round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),0)
			--Si es moneda extranjera
			Else round(ISNULL(C.TotalImpuesto,0) - Isnull((select Sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0),2)end,
'RetencionIVA'= 
			Case when ((select count(*) from OrdenPagoEgreso where IDTransaccionEgreso = C.IDTransaccion and Procesado = 1)=0) then
			(Case when C.IDMoneda = 1 then Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),0)
			--Moneda Extranjera
			Else Round(((ISNULL(C.TotalImpuesto,0) - Isnull((select sum(NCP.TotalImpuesto) from  NotaCreditoProveedor NCP
			Join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor = NCP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NCPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NCP.Anulado = 0),0) +
			Isnull((select sum(NDP.TotalImpuesto) from  NotaDebitoProveedor NDP
			Join NotaDebitoProveedorCompra NDPC on NDPC.IDTransaccionNotaDebitoProveedor = NDP.IDTransaccion
			Join VGastoTipoComprobante CA on CA.IDTransaccion =  NDPC.IDTransaccionEgreso
			where CA.IDTransaccion = C.IDtransaccion and NDP.Anulado = 0),0))*0.3),2) end)
			Else
			0 end,
	'RetencionRenta'=ISNULL(C.RetencionRenta,0),
	--'Retener'=(Case When (C.Total) <> C.Saldo Then 'False' Else 'True' End),
	--se modifica porque no debe retener solo cuando tiene un pago, pero cuando tiene una nota de credito si debe retener
	'Retener'=(Case When (Select Count(*) from ordenpagoegreso where idtransaccionegreso = C.IDTransaccion) > 0 Then 'False' Else 'True' End),
	
	C.TotalImpuesto,
	C.TotalDiscriminado,
	C.IDMoneda, 
	C.Moneda, 
	C.Cotizacion,
	'Cotiz.'=C.Cotizacion,  
	C.Observacion, 
	C.Credito, 
	'Cuota'=CU.ID,
	'CantidadCuota'=C.Cuota,
	'Pago'=Convert(varchar(50), CU.ID) + '/' + Convert(varchar(50), C.Cuota),

	'Seleccionado'='False', 'Cancelar'='False', C.Efectivo, C.Cheque,
	'FondoFijo'='False',
	'Pagar'=IsNull(CU.Pagar, 'False'),
	'ImporteAPagar'=IsNull(CU.ImporteAPagar, 0),
	CU.IDCuentaBancaria,
	CB.Descripcion,
	CU.ObservacionPago,
	'EstadoProceso'='PENDIENTE',
	(Case When (IsNull(IdGrupo, 0)) = 0 Then 0 Else IdGrupo End) as IdGrupo,
	convert(date,C.FechaTransaccion) as fechaTransaccion,
	'IVA10'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 1 ),0),
	'IVA5'=ISnull((Select Total from DetalleImpuesto where IDTransaccion = C.IDTransaccion and IDImpuesto = 2 ),0)
		
	From VGastoTipoComprobante C
	--join vdetalleasientoAgrupado DA on C.IDTransaccion = DA.IDTransaccion and DA.Credito > 0 and Da.Codigo like @vCuentaContable
	Join VProveedor P On C.IDProveedor=P.ID
	Join Cuota CU On C.IDTransaccion=CU.IDTransaccion
	Left Outer Join VCuentaBancaria CB On CU.IDCuentaBancaria=CB.ID
	Where CU.Cancelado = 'False' And Cuota>0	
	--Esto es solo si se quiere mostrar el primero no pagado
	--And CU.ID=(Select Min(CU2.ID) From Cuota CU2 Where CU2.IDTransaccion=CU.IDTransaccion And CU2.Cancelado='False')
	And (Case When (@SoloAPagar) = 'True' Then CU.Pagar Else 'False' End) = @SoloAPagar
	and c.saldo > 0.99
end




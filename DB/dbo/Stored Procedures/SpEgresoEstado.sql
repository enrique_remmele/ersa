﻿CREATE Procedure [dbo].[SpEgresoEstado]

	--Entrada
	@IDSucursal tinyint,
	@IDTransaccion numeric(18,0),
	@Cuota smallint = 1
	
	
As

Begin

	Declare @vProcesado bit
	Declare @vMensaje varchar(50)
	Declare @vComprobante varchar(50)
	Declare @vIDTransaccionOrdenPago numeric(18,0)

	Set @vProcesado = 'False'
	Set @vMensaje = 'No procesado'
	Set @vComprobante = '---'

	Set @vIDTransaccionOrdenPago = IsNull((Select Top(1) IDTransaccionOrdenPago From OrdenPagoEgreso Where IDTransaccionEgreso=@IDTransaccion), 0)

	--Si es un gasto, verificamos si tambien es una cuota
	If @Cuota>1 Begin
		
		If (Select Top(1) Cancelado From Cuota Where IDTransaccion=@IDTransaccion And ID=@Cuota) = 'False' Begin
			Set @vProcesado = 'False'
			Set @vMensaje = 'No procesado'
			Set @vComprobante = '---'
			GoTo Salir
		End Else Begin
			Set @vProcesado = 'True'
			Set @vMensaje = 'Procesado'
			Set @vComprobante = Convert(varchar(50), (Select Top(1) Numero From OrdenPago Where IDTransaccion=@vIDTransaccionOrdenPago))
			GoTo Salir
		End

	End

	If @vIDTransaccionOrdenPago <> 0 Begin
		Set @vProcesado = 'True'
		Set @vMensaje = 'Procesado'
		Set @vComprobante = Convert(varchar(50), (Select Top(1) Numero From OrdenPago Where IDTransaccion=@vIDTransaccionOrdenPago))
		GoTo Salir
	End
	
	If @vIDTransaccionOrdenPago = 0 Begin
		Set @vProcesado = 'False'
		Set @vMensaje = 'No procesado'
		Set @vComprobante = '---'
		GoTo Salir
	End

Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado, 'Comprobante'=@vComprobante
End

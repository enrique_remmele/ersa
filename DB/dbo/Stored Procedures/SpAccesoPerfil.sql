﻿CREATE Procedure [dbo].[SpAccesoPerfil]

	--Entrada
	@IDPerfil smallint,
	@IDMenu numeric(18,0),
	@NombreControl varchar(100),
	@Habilitar bit,
	
	@Visualizar bit,
	@Agregar bit,
	@Modificar bit,
	@Eliminar bit,
	@Anular bit,
	@Imprimir bit,
		
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int
	
	
As

Begin

	--BLOQUES
	
	
	--Agregar
	If @Habilitar = 'True' Begin
		
		If Not Exists (Select * From AccesoPerfil where IDPerfil=@IDPerfil And NombreControl = @NombreControl) Begin
			Insert Into AccesoPerfil(IDPerfil, IDMenu, Visualizar, Agregar, Modificar, Eliminar, Anular, Imprimir,NombreControl)
			Values(@IDPerfil, @IDMenu, @Visualizar, @Agregar, @Modificar, @Eliminar, @Anular, @Imprimir,@NombreControl)	
		End Else Begin
			Update AccesoPerfil Set Visualizar=@Visualizar,
									Agregar=@Agregar,
									Modificar=@Modificar,
									Eliminar=@Eliminar,
									Anular=@Anular,
									Imprimir=@Imprimir,
									NombreControl = @NombreControl
			Where IDPerfil=@IDPerfil And NombreControl = @NombreControl
			
		End
				
	End
	
	--Quitar
	If @Habilitar = 'False' Begin
		
		If Exists (Select * From AccesoPerfil where IDPerfil=@IDPerfil And NombreControl = @NombreControl) Begin
			Delete AccesoPerfil
			where IDPerfil=@IDPerfil and NombreControl = @NombreControl
		End
				
	End
	
	return @@rowcount

End


﻿
CREATE Procedure [dbo].[SpAsignacionCamion]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Numero int = NULL,
	@Fecha date = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante varchar(50) = NULL,
	@Observacion varchar(200) = NULL,
	--Distribucion
	@IDChofer int = NULL,
	@IDCamion int = NULL,
	@IDSucursalAbastecer tinyint = 1,

	@PedidoCliente bit = 'False',
	
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin	
		Declare @vNumero int
		Set @vNumero = ISNULL((Select MAX(Numero)+1 From AsignacionCamion Where IDSucursal=@IDSucursal),1)
	
			
		--Comprobante
		if Exists(Select * From AsignacionCamion Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante) Begin
			set @Mensaje = 'El sistema encontro el mismo tipo y numero de comprobante!.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Fecha de la compra que no sea mayor a hoy
		If @Fecha > getdate() Begin
				set @Mensaje = 'La fecha del comprobante no debe ser mayor a hoy!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		------Insertar
		Declare @vSaldo money
		Declare @vCancelado bit

		--Insertar el Registro de Gasto		
		Insert Into AsignacionCamion(IDTransaccion, IDSucursal, IDSucursalAbastecer , Numero, Fecha, IDTipoComprobante, NroComprobante, Observacion,IDCamion, IDChofer, Anulado,PedidoCliente)
		Values(@IDTransaccionSalida,@IDSucursal, @IDSucursalAbastecer, @vNumero, @Fecha, @IDTipoComprobante, @NroComprobante, @Observacion,@IDCamion, @IDChofer, 'False',@PedidoCliente)
		
								
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount			
	End
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From AsignacionCamion  Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		 
		--Eliminamos el registro
		Delete from DetalleAsignacionCamion  Where IDTransaccion = @IDTransaccion
		Delete AsignacionCamion  Where IDTransaccion = @IDTransaccion
		Delete Transaccion Where ID = @IDTransaccion
			
			--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='AsignacionCamion', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
			
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End

	IF @Operacion = 'ANULAR' Begin
    --IDTransaccion
		If Exists(Select * From AsignacionCamion Where IDtransaccion=@IDTransaccion and Anulado = 'True') Begin
			set @Mensaje = 'La Operacion ya esta anulada.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

	   ----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT

		Execute SpDetalleAsignacionCamion @IDTransaccion, 'ANULAR','', 'False'

		Update AsignacionCamion
		set Anulado = 'True'
		where IDTransaccion = @IDTransaccion

		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='AsignacionCamion', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount	
	End
	
	If @Operacion = 'UPD' Begin
	
	--Fecha de la compra que no sea mayor a hoy
		If @Fecha > getdate() Begin
				set @Mensaje = 'La fecha del comprobante no debe ser mayor a hoy!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
		End
		
		
		Update AsignacionCamion Set 
							IDTipoComprobante=@IDTipoComprobante,
							NroComprobante=@NroComprobante,	
							Fecha=@Fecha,	
							Observacion=@Observacion,
							IDChofer = @IDChofer,
							IDCamion = @IDCamion,
							PedidoCliente = @PedidoCliente
			Where IDTransaccion=@IDTransaccion
			
			Exec SpLogSuceso @Operacion=@Operacion, @Tabla='AsignacionCamion', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
			Set @Mensaje = 'Registro guardado!'
			Set @Procesado = 'True'

		Set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount	
		End

		
			
	
	set @Mensaje = 'No se proceso ninguna transacción!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End






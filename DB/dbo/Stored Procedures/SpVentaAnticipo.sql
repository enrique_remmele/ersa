﻿CREATE Procedure [dbo].[SpVentaAnticipo]

	--Entrada
	@IDTransaccionAnticipo numeric(18,0),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin Tran SAVE TRANSACTION TransaccionVentaAnticipo Begin
Begin Try

	--Variables
	Declare @vIDTransaccionVenta numeric(18,0)
	Declare @vIDTransaccionAnticipo numeric(18,0)
	Declare @vImporte money
	Declare @vCancelar bit
	Declare @vCancelarVenta bit
	Declare @vCobrado money
	Declare @vDescontado money
	Declare @vTotalVenta money
	Declare @vSaldoTotal money
	Declare @vCancelado bit
	
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
				
	--Procesar Saldos de Ventas
	If @Operacion = 'INS' Begin
	
		Declare db_cursor cursor for
		Select VC.IDTransaccionVenta, VC.IDTransaccionAnticipo, VC.Importe, VC.Cancelar,
		V.Total, V.Cobrado, V.Descontado 
		From AnticipoVenta VC 
		Join Venta V On VC.IDTransaccionVenta=V.IDTransaccion
		Join AnticipoAplicacion AP on AP.IDTransaccion = VC.IDTransaccionAnticipo
		Where VC.IDTransaccionAnticipo=@IDTransaccionAnticipo
		
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVenta, @vIDTransaccionAnticipo, @vImporte, @vCancelar, @vTotalVenta, @vCobrado, @vDescontado
		While @@FETCH_STATUS = 0 Begin  

			--Calcular
			Set @vCancelarVenta = 'False'
			Set @vCobrado = @vCobrado + @vImporte
			Set @vSaldoTotal = @vTotalVenta - (@vCobrado + @vDescontado)
			   
			--Si el saldo es 0, cancelamos la venta   
			If @vSaldoTotal < 1 Begin
				Set @vCancelarVenta = 'True'
			End
			 
			--Si el usuario cancela explicitamente la venta    	
			If @vCancelar = 'True' Begin
				Set @vCancelarVenta = 'True'
			End
			
			--Actualizar Venta			
			Update Venta Set	Saldo=@vSaldoTotal, 
								Cobrado=@vCobrado, 
								Cancelado=@vCancelarVenta
			Where IDTransaccion = @vIDTransaccionVenta
			
			--Siguiente
			Fetch Next From db_cursor Into @vIDTransaccionVenta, @vIDTransaccionAnticipo, @vImporte, @vCancelar, @vTotalVenta, @vCobrado, @vDescontado
			
		End
		
		Close db_cursor   
		Deallocate db_cursor		   			
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		Commit Tran TransaccionVentaAnticipo
		Return @@rowcount
		
	End
	
	--Eliminar Ventas
	If @Operacion = 'ANULAR' Begin
	
		Declare db_cursor cursor for
		Select VC.IDTransaccionVenta, VC.IDTransaccionAnticipo, VC.Importe, VC.Cancelar,
		V.Total, V.Cobrado, V.Descontado 
		From AnticipoVenta VC 
		Join Venta V On VC.IDTransaccionVenta=V.IDTransaccion
		Join AnticipoAplicacion AP on AP.IDTransaccion = VC.IDTransaccionAnticipo
		Where IDTransaccionAnticipo=@IDTransaccionAnticipo
		
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVenta, @vIDTransaccionAnticipo, @vImporte, @vCancelar, @vTotalVenta, @vCobrado, @vDescontado
		While @@FETCH_STATUS = 0 Begin  

			--Calcular
			Set @vCobrado = @vCobrado - @vImporte
			Set @vSaldoTotal = @vTotalVenta - (@vCobrado + 	@vDescontado)
								
			--Actualizar Venta			
			Update Venta Set	Saldo=@vSaldoTotal, 
								Cobrado=@vCobrado, 
								Cancelado='False'
			Where IDTransaccion = @vIDTransaccionVenta
					
			--Siguiente
			Fetch Next From db_cursor Into @vIDTransaccionVenta, @vIDTransaccionAnticipo, @vImporte, @vCancelar, @vTotalVenta, @vCobrado, @vDescontado
			
		End
		
		Close db_cursor   
		Deallocate db_cursor	
		
		--Eliminar las relaciones
		Delete From AnticipoVenta 
		Where IDTransaccionAnticipo=@IDTransaccionAnticipo
		
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		Commit Tran TransaccionVentaAnticipo
		Return @@rowcount
		
	End
	
	--Eliminar Ventas
	If @Operacion = 'DEL' Begin
	
		If (Select Anulado From AnticipoAplicacion Where IDTransaccion=@IDTransaccionAnticipo) = 'False' Begin
			Declare db_cursor cursor for
			Select IDTransaccionVenta, @IDTransaccionAnticipo, Importe, Cancelar From AnticipoVenta Where IDTransaccionAnticipo=@IDTransaccionAnticipo
			
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionVenta, @vIDTransaccionAnticipo, @vImporte, @vCancelar
			While @@FETCH_STATUS = 0 Begin  

				--Obtener Informacion
				Set @vTotalVenta = (Select IsNull((Select Total From Venta Where IDTransaccion=@vIDTransaccionVenta), 0))
				Set @vCobrado = (Select IsNull((Select Cobrado From Venta Where IDTransaccion=@vIDTransaccionVenta), 0))
				Set @vDescontado = (Select IsNull((Select Descontado From Venta Where IDTransaccion=@vIDTransaccionVenta), 0))
				
				--Calcular
				Set @vCobrado = @vCobrado - @vImporte
				
				If @vCobrado < 0 Begin
					Set @vCobrado = (@vCobrado * -1)
				End
				
				Set @vSaldoTotal = @vTotalVenta - (@vCobrado + @vDescontado)
				
				--Actualizar Venta			
				Update Venta Set	Saldo=@vSaldoTotal, 
									Cobrado=@vCobrado, 
									Cancelado='False'
				Where IDTransaccion = @vIDTransaccionVenta
				
				--Siguiente
				Fetch Next From db_cursor Into @vIDTransaccionVenta, @vIDTransaccionAnticipo, @vImporte, @vCancelar
				
			End
			
			Close db_cursor   
			Deallocate db_cursor		   			
			
		End
		
		--Eliminar las relaciones
		Delete From AnticipoVenta 
		Where IDTransaccionAnticipo=@IDTransaccionAnticipo
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		Commit Tran TransaccionVentaAnticipo
		Return @@rowcount
		
	End
		
	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'
	
	--Si no paso nada	
	RollBack Tran TransaccionVentaAnticipo

End Try
Begin Catch
	set @Mensaje = ERROR_MESSAGE()
	Set @Procesado = 'False'
	RollBack Tran TransaccionVentaAnticipo
	RAISERROR(@Mensaje, 16, 0)		 
End Catch
	
End
	


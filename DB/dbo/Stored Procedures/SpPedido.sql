﻿CREATE Procedure [dbo].[SpPedido]

	--Entrada
	@IDTransaccion as numeric(18,0),
	@AnularPedidoNotaCredito as bit = 0,
	
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion int = 0,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin
	Declare @vCantidad decimal(18,0)
	Declare @vIDProducto int
	Declare @vIDCliente int
	Declare @vIDListaPrecio int
	Declare @vFechaFacturar date
	Declare @vDescuentoUnitario money
	Declare @vIDTransaccionPedidoNotaCredito int
	Set @vIDCliente = (Select IDCliente From Pedido Where IDTransaccion=@IDTransaccion)
	Set @vIDListaPrecio = isnull((select IDListaPrecio from Pedido where IDTransaccion = @IDTransaccion),0)
	Set @vFechaFacturar = (Select FechaFacturar from Pedido where IDTransaccion = @IDTransaccion)
	Set @vIDTransaccionPedidoNotaCredito = (select IDTransaccionPedidoNotaCredito from VPedido where IDTransaccion = @IDTransaccion)
	

	If @Operacion = 'ANULAR' Begin
		print 'entro a anular'

		If @AnularPedidoNotaCredito = 1 begin
			
			Exec SpPedidoNotaCredito
				@IDTransaccion = @vIDTransaccionPedidoNotaCredito
				,@Operacion = 'ANULAR'
				,@IDUsuario = @IDUsuario
				,@IDSucursal = @IDSucursal
				,@IDDeposito = @IDDeposito
				,@IDTerminal = @IDTerminal
				,@IDOperacion = 0
				,@Mensaje = ''
				,@Procesado = 0
				,@IDTransaccionSalida = 0

				Delete from DetalleNotaCreditoDiferenciaPrecio Where idtransaccionpedidoventa = @IDTransaccion

		end


		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			print @Mensaje
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Pedido Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			print @Mensaje
			return @@rowcount
		End

		--Asignacion a Camion
		if exists(select * from AsignacionCamion AC join DetalleAsignacionCamion DAC on AC.IDTransaccion = DAC.IDTransaccion
			 where AC.Anulado = 0 and DAC.IDTransaccionPedido=@IDTransaccion) begin
			set @Mensaje = 'El Pedido esta asignado a una carga, se debe anular la carga antes de anular el pedido!'
			set @Procesado = 'False'
			print @Mensaje
			return @@rowcount
		end
		
		--Verificamos que el pedido no este facturado
		If Exists(Select * From PedidoVenta Where IDTransaccionPedido=@IDTransaccion) Begin
			set @Mensaje = 'El regitro esta facturado! No se puede anular.'
			set @Procesado = 'False'
			print @Mensaje
			return @@rowcount
		End
		
		----Insertamos el registro de anulacion
		print concat('insertar registro de anulacion pedido: ',@IDTransaccion)
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		print @Mensaje
		----Anular
		Update Pedido Set Anulado='True', IDUsuarioAnulado=@IDUsuario, FechaAnulado = GETDATE()
		Where IDTransaccion = @IDTransaccion

		

		Exec SpPedidoNotaCreditoProcesar @IDTransaccion= @IDTransaccion, @Operacion ='ANULAR'

		Begin

		Declare db_cursor cursor for
		Select Cantidad, IDProducto, DescuentoUnitario From DetallePedido Where IDTransaccion=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vCantidad, @vIDProducto, @vDescuentoUnitario
		While @@FETCH_STATUS = 0 Begin  

			--Si tiene excepcion con limite de cantidad, restar el saldo
			Update ProductoListaPrecioExcepciones 
			Set CantidadLimiteSaldo = Isnull(CantidadLimiteSaldo,0) - @vCantidad
			Where IDProducto = @vIDProducto 
			--and IDListaPrecio = @vIDListaPrecio 
			and IDCliente = @vIDCliente
			and (@vFechaFacturar Between Desde and Hasta)
			and Isnull(CantidadLimite,0)>0 
			--and round(Descuento,0) = round(@vDescuentoUnitario,0)
			and IDTipoDescuento <> 3
			and IDTransaccionPedido = @IDTransaccion


			--Si tiene excepcion con limite de cantidad, restar el saldo
			Update ProductoPrecio 
			Set IDTransaccionPedidoPrecioUnico=null
			Where IDProducto = @vIDProducto 
			and IDCliente = @vIDCliente
			and PrecioUnico = 1
			and IDTransaccionPedidoPrecioUnico = @IDTransaccion
			
			Siguiente:

			Fetch Next From db_cursor Into @vCantidad, @vIDProducto, @vDescuentoUnitario

		End

		Close db_cursor   
		Deallocate db_cursor	

		Update ProductoListaPrecioExcepciones set IDTransaccionPedido = 0 
		where IDTransaccionPedido = @IDTransaccion

	End


						
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Pedido Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Asignacion a Camion
		if exists(select * from DetalleAsignacionCamion where Anulado = 0 and IDTransaccionPedido=@IDTransaccion)begin
			set @Mensaje = 'El Pedido esta asignado a una carga, se debe anular la carga antes de eliminar el pedido!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Verificamos que el pedido no este facturado
		If Exists(Select * From PedidoVenta Where IDTransaccionPedido=@IDTransaccion) Begin
			set @Mensaje = 'El regitro esta facturado! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Eliminamos los descuentos
		Delete DescuentoPedido where IDTransaccion = @IDTransaccion				
		
		--Eliminamos el detalle
		Delete DetallePedido where IDTransaccion = @IDTransaccion				
				
		--Eliminamos el registro
		Delete Pedido Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion				
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
			
	End
	
	Set @Mensaje = 'No se proceso ningun registro!'
	Set @Procesado = 'False'
	Return @@rowcount
	
End


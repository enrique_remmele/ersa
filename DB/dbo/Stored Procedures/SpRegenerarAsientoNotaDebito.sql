﻿CREATE Procedure [dbo].[SpRegenerarAsientoNotaDebito]

	@Desde date,
	@Hasta date,
	@Todos bit = 'False',
	@SoloConDiferencias bit = 'False',
	@IDSucursal tinyint
	
As

Begin
	
	--Variables
	Begin
		Declare @vIDTransaccion numeric(18,0)		
	End
	
	Declare cNotaDebito cursor for
	Select IDTransaccion From VNotaDebito Where Fecha Between @Desde And @Hasta And IDSucursal=@IDSucursal
	Open cNotaDebito   
	fetch next from cNotaDebito into @vIDTransaccion

	While @@FETCH_STATUS = 0 Begin  			  

		--Generar
		Exec SpAsientoNotaDebito @IDTransaccion=@vIDTransaccion		
					
		--Siguiente
		fetch next from cNotaDebito into @vIDTransaccion
		
	End   
	
	close cNotaDebito   
	deallocate cNotaDebito
			
	Select 'Mensaje'='Registros procesados', 'Procesado'='True'
End

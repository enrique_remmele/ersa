﻿CREATE Procedure [dbo].[SpCFGasto]

	--Cuenta Fija
	@IDOperacion tinyint,
	@ID tinyint,
	@IDSucursal tinyint,
	@IDMoneda tinyint,
	@CuentaContable varchar(50),
	@Debe bit,
	@Haber bit,
	@IDTipoComprobante smallint,
	@Descripcion varchar(50),
	@Orden tinyint,
	@FondoFijo bit = 'False',
			
	--Cuenta Fija Gasto
	@IDTipoCuentaFija tinyint,
	@BuscarEnProveedor bit,
	
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
	
As

Begin

	--BLOQUES
	
	Declare	@vID tinyint
	
	--INSERTAR
	if @Operacion='INS' begin
			
		EXEC SpCF @ID = 0,
				@IDOperacion = @IDOperacion,
				@IDSucursal = @IDSucursal,
				@IDMoneda = @IDMoneda,
				@CuentaContable = @CuentaContable,
				@Debe = @Debe,
				@Haber = @Haber,
				@IDTipoComprobante = @IDTipoComprobante,
				@Descripcion = @Descripcion,
				@Orden = @Orden,
				@Operacion = @Operacion,
				@vID = @vID OUTPUT
			
		Insert Into CFGasto(IDOperacion, ID, IDTipoCuentaFija, BuscarEnProveedor, FondoFijo)
		Values(@IDOperacion, @vID, @IDTipoCuentaFija, @BuscarEnProveedor, @FondoFijo)
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		EXEC SpCF @ID = @ID,
				@IDOperacion = @IDOperacion,
				@IDSucursal = @IDSucursal,
				@IDMoneda = @IDMoneda,
				@CuentaContable = @CuentaContable,
				@Debe = @Debe,
				@Haber = @Haber,
				@IDTipoComprobante = @IDTipoComprobante,
				@Descripcion = @Descripcion,
				@Orden = @Orden,
				@Operacion = @Operacion,
				@vID = @vID OUTPUT
				
		Update CFGasto Set	BuscarEnProveedor=@BuscarEnProveedor, 
							IDTipoCuentaFija=@IDTipoCuentaFija,
							FondoFijo=@FondoFijo
		Where IDOperacion=@IDOperacion And ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		Delete From CFGasto Where IDOperacion=@IDOperacion And ID=@ID
		Delete From CF Where IDOperacion=@IDOperacion And ID=@ID
		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'  
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount
			
End
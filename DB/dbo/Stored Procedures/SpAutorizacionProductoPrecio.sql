﻿CREATE Procedure [dbo].[SpAutorizacionProductoPrecio]
	--Entrada
	@IDProducto int,
	@IDCliente int,
	@IDMoneda tinyint,
	@PrecioUnico bit,
	@Precio money,
	@Desde date,
	@CantidadMinimaPrecioUnico int = 0,

	@Operacion varchar(20),

	@IDUsuario int,
	@IDTerminal int = 0,

	@autorizado bit = 'False' output
As
Begin
		
	Declare @vMensaje varchar(300)
	Declare @vProcesado bit = 0
	
	if not exists (select * from PorcentajeProductoPrecio where IDUsuario = @IDUsuario) and @Operacion <> 'ELIMINAR'begin
		print 'Usuario sin acceso!'
		goto salir
	end

  if  @Operacion = 'APROBAR' begin	
		
		if @IDTerminal = 0 begin
			goto Salir
		end

		--select * from SolicitudProductoPrecio
		Update SolicitudProductoPrecio 
		Set Estado = 1,
		IDUsuarioAprobado= @IDUsuario,
		FechaAprobado = getdate(),
		IDTerminalAprobado = @IDTerminal
		Where IDProducto = @IDProducto
		and IDCliente = @IDCliente
		and PrecioUnico = @PrecioUnico
		and IDMoneda = @IDMoneda
		and Desde = @Desde
		and Estado is null

		exec spProductoPrecio @IDProducto = @IDProducto, 
											@IDCliente=@IDCliente,
											@IDMoneda=@IDMoneda,
											@PrecioUnico=@PrecioUnico,
											@Operacion = 'DEL',
											@IDUsuario = @IDUsuario,
											@IDTerminal = @IDTerminal

		exec spProductoPrecio @IDProducto = @IDProducto, 
											@IDCliente=@IDCliente,
											@IDMoneda=@IDMoneda,
											@Precio=@Precio,
											@PrecioUnico=@PrecioUnico,
											@Desde = @Desde,
											@CantidadMinimaPrecioUnico = @CantidadMinimaPrecioUnico,
											@Operacion = 'INS',
											@IDUsuario = @IDUsuario,
											@IDTerminal = @IDTerminal
			
		print concat(' INS', '|', @vProcesado, '|',@vMensaje)
			
	set @autorizado = 'True'
  end

  if @Operacion = 'RECHAZAR' begin
  
	if @IDTerminal = 0 begin
		goto Salir
	  end
		
		Update SolicitudProductoPrecio 
		Set Estado = 0,
		IDUsuarioAprobado= @IDUsuario,
		FechaAprobado = getdate(),
		IDTerminalAprobado = @IDTerminal
		Where IDProducto = @IDProducto
		and IDCliente = @IDCliente
		and PrecioUnico = @PrecioUnico
		and IDMoneda = @IDMoneda
		and Desde = @Desde
		and Estado is null

		set @autorizado = 'True'
  end

  if @Operacion = 'ELIMINAR' begin	
		----Actualizar
		Delete from  SolicitudProductoPrecio 
		Where IDProducto = @IDProducto
		and IDCliente = @IDCliente
		and PrecioUnico = @PrecioUnico
		and IDMoneda = @IDMoneda
		and Desde = @Desde
		--and Isnull(Estado,0) = 0


		exec spProductoPrecio @IDProducto = @IDProducto, 
										@IDCliente=@IDCliente,
										@IDMoneda=@IDMoneda,
										@Precio=@Precio,
										@Desde = @Desde,
										@PrecioUnico=@PrecioUnico,
										@CantidadMinimaPrecioUnico = @CantidadMinimaPrecioUnico,
										@Operacion = 'DEL',
										@IDUsuario = @IDUsuario,
										@IDTerminal = @IDTerminal
			
		print concat(' DEL', '|', @vProcesado, '|',@vMensaje)
		set @autorizado = 'True'
  end


  if @Operacion = 'RECUPERAR' begin	
		----Actualizar
		Update SolicitudProductoPrecio 
		Set Estado = null,
		IDUsuarioAprobado= null,
		FechaAprobado = null,
		IDTerminalAprobado = null
		Where IDProducto = @IDProducto
		and IDCliente = @IDCliente
		and PrecioUnico = @PrecioUnico
		and IDMoneda = @IDMoneda
		and Desde = @Desde
		and Estado is not null
		
		exec spProductoPrecio @IDProducto = @IDProducto, 
										@IDCliente=@IDCliente,
										@IDMoneda=@IDMoneda,
										@Precio=@Precio,
										@Desde = @Desde,
										@PrecioUnico=@PrecioUnico,
										@CantidadMinimaPrecioUnico = @CantidadMinimaPrecioUnico,
										@Operacion = 'DEL',
										@IDUsuario = @IDUsuario,
										@IDTerminal = @IDTerminal
			
		print concat(' DEL', '|', @vProcesado, '|',@vMensaje)
		set @autorizado = 'True'

	end
	

  salir:

  
End



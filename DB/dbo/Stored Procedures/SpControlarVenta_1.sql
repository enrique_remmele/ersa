﻿CREATE Procedure [dbo].[SpControlarVenta]

	--Entrada
	@IDTransaccion numeric(18,0)
	
As

Begin

	Declare @vProcesado bit
	Declare @vMensaje varchar(100)
	Declare @vHabilitado bit
	
	Declare @vIDProducto int
	Declare @vIDDeposito int
	Declare @vCantidad decimal(10,2)
	Declare @vExistencia decimal(10,2)
	Declare @vProducto varchar(50)
	Declare @vReferencia varchar(50)
	
	Set @vMensaje = 'No se proceso'
	Set @vProcesado = 'False'
	Set @vHabilitado = 'False'
	
	--CONTROLAR CANTIDAD DE PRODUCTOS
	Declare db_DetalleVenta cursor for
	Select IDProducto, IDDeposito, 'Cantidad'=Sum(Cantidad) 
	From VDetalleVenta 
	Where IDTransaccion=@IDTransaccion
	Group By IDProducto, IDDeposito
	
	Open db_DetalleVenta 
	Fetch Next From db_DetalleVenta Into @vIDProducto, @vIDDeposito, @vCantidad
	While @@FETCH_STATUS = 0 Begin 

		Set @vExistencia = IsNull((Select Sum(Existencia) From ExistenciaDeposito Where IDProducto=@vIDProducto And IDDeposito=@vIDDeposito),0)
		
		Set @vProducto = (Select Descripcion From Producto Where ID=@vIDProducto)
		Set @vReferencia = (Select Referencia From Producto Where ID=@vIDProducto)
		
		print @vProducto + ': ' +CONVERT(varchar(50), @vCantidad) + ' - ' + CONVERT(varchar(50), @vExistencia)	
		
		--Controlar Existencia
		If @vExistencia<@vCantidad Begin
			
			
			
			Set @vMensaje = 'No hay suciciente stock! ' + @vProducto + ' (' + @vReferencia + ')' + ' - Cantidad: ' + CONVERT(varchar(50), @vCantidad)
			Set @vProcesado = 'False'
			Set @vHabilitado = 'False'	
	
			--Cierra el cursor
			Close db_DetalleVenta
			Deallocate db_DetalleVenta
			
			GoTo Salir
			
		End
		
		Fetch Next From db_DetalleVenta Into @vIDProducto, @vIDDeposito, @vCantidad
									
	End

	--Cierra el cursor
	Close db_DetalleVenta
	Deallocate db_DetalleVenta

	Set @vMensaje = 'Habilitado'
	Set @vProcesado = 'True'
	Set @vHabilitado = 'True'	
	
Salir:
	Select 'Procesado'=@vProcesado, 'Mensaje'=@vMensaje, 'Habilidado'=@vHabilitado	
End


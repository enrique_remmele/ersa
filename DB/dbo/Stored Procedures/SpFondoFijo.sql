﻿CREATE Procedure [dbo].[SpFondoFijo]

	--Entrada
	@IDSucursal tinyint,
	@IDGrupo tinyint,
	@Tope money,
	@TotalRendir money=0,
	@Saldo money=0,
	@Operacion varchar(50),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Validar que no exista
		If Exists(Select * From FondoFijo Where IDSucursal=@IDSucursal and  IDGrupo=@IDGrupo) Begin
			set @Mensaje = 'Este FondoFijo ya tiene asignado estos parametros!'
			Set @Procesado = 'False'
			return @@rowcount
		End
				
		--Insertamos
		Insert Into FondoFijo(IDSucursal,IDGrupo,Tope,TotalRendir,Saldo)
		Values(@IDSucursal,@IDGrupo,@Tope,@TotalRendir,@Saldo)		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Actualizamos
		Update FondoFijo Set IDSucursal=@IDSucursal,IDGrupo=@IDGrupo,Tope=@Tope
		Where IDGrupo=@IDGrupo
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Validar que no exista en DetalleFondoFijo
		If Exists(Select * From DetalleFondoFijo Where IDSucursal=@IDSucursal and  IDGrupo=@IDGrupo) Begin
			set @Mensaje = 'Este FondoFijo ya tiene asignado estos parametros en DetalleFondoFijo!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--Eliminamos
		Delete From FondoFijo
		Where IDSucursal=@IDSucursal and IDGrupo=@IDGrupo
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End



﻿CREATE Procedure [dbo].[SpReciboVentaProcesar]
	
	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Operacion varchar(50),
	
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin

	--BLOQUES
	Declare @vIDCliente int 
	Declare @vMaxIDMotivo int
	Declare @CancelarAutomatico bit 
	Declare @NroRecibo int 
	
	--INSERTAR
	--If @Operacion = 'INS'Begin --Para todas las Sucursales
	--select * from sucursal
	
	If @Operacion = 'INS' and @IDSucursal <> 5 Begin --Habilitado para todas las sucursales 25/10/2018 4:21pm
														-- menos mercury 21/11/2018 3:16pm
	
		--Validar
		--Solo generar el recibo cuando el Total = Saldo
		If Exists(Select * From Venta Where IDTransaccion = @IDTransaccion and Saldo <> Total) Begin
			set @Mensaje = 'La venta ya fue cobrada parcial o totalmente.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Verificar que el documento ya este anulado
		If Exists(Select * From Venta Where IDTransaccion=@IDTransaccion And Anulado='True') Begin
			set @Mensaje = 'La Venta esta anulada'
			set @Procesado = 'False'
			return @@rowcount
		End
				
		------Insertar
		Declare @vSaldo money
		Declare @vCancelado bit
		--Inicializar con la fecha de la venta
		Declare @Fecha date = (Select FechaEmision from Venta where IDTransaccion = @IDTransaccion)
		Set @vCancelado = 'False'
		
		--Insertar el Registro de Venta		
		Declare @IDRecibo int = (Select top(1) ID from Recibo where Estado = 'True')
		--obtener el siguiente numero a utilizar
		set @NroRecibo = IsNull((Select UltimoNumero from Recibo where ID = @IDRecibo),0) + 1

		--verificar si la venta ya tiene recibo asociado, si tiene entonces asignar fecha del dia al recibo
		if exists(Select * from ReciboVenta where IDTransaccionVenta = @IDTransaccion) Begin
		  if (Select convert(date,FechaEmision) from Venta where IDTransaccion = @IDTransaccion) <= convert(date,GETDATE()) begin
				set @Fecha = getdate()
			end
		End 

		--insertar datos
		Insert Into ReciboVenta(IDTransaccionVenta, IDRecibo,NroRecibo, Fecha,Anulado, IDUsuarioAnulacion, FechaAnulacion, IDUsuario, IDTerminal)
		Values(@IDTransaccion, @IDRecibo,@NroRecibo,@Fecha, 'False', null, null, @IDUsuario, @IDTerminal)
		
			
		--Actualizar el ultimo numero utilizado del recibo
		Update Recibo 
		Set  UltimoNumero =@NroRecibo
		Where ID=@IDRecibo
		
		
								
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'

			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Venta Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End	
	
		--Anulamos el registro
		set @IDRecibo = (Select IDRecibo from ReciboVenta where IDTransaccionVenta = @IDTransaccion and Anulado = 'False')

		Update ReciboVenta Set Anulado = 'True',
						FechaAnulacion=getdate(),
						IDUsuarioAnulacion = @IDUsuario
		Where IDTransaccionVenta = @IDTransaccion
		and IDRecibo = @IDRecibo
			
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End

	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	return @@rowcount
		
End

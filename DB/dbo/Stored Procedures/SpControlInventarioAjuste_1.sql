﻿CREATE Procedure [dbo].[SpControlInventarioAjuste]

	--Entrada
	@IDTransaccion numeric,
	@Operacion varchar(10),
	
	--Transaccion	
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
			
As

Begin

	--Validar fecha de operacion
	Declare @vFecha datetime
	Declare @vIDOperacion integer

	Select	@vFecha = Fecha From ControlInventario Where IDTransaccion = @IDTransaccion
	Select	@vIDOperacion = IDOperacion From Transaccion Where ID = @IDTransaccion
		
	If dbo.FValidarFechaOperacion(@IDSucursal, @vFecha, @IDUsuario, @vIDOperacion) = 'False' Begin
		Set @Mensaje  = 'Fecha fuera de rango permitido!!'
		Set @Procesado = 'False'
		--set @IDTransaccionSalida  = 0
		Return @@rowcount
	End

	--Variables
	Declare @vIDDeposito int
	Declare @vIDProducto int
	Declare @vDiferenciaSistema decimal(10,2)
	Declare @vSigno char(1)
	
		--Variables Kardex
	Declare @vIDSucursal integer
	Declare @vNroComprobante varchar(50)
	Declare @vCosto decimal (18,2)

	Select	@vFecha = FechaAjuste, 
			@vIDSucursal = IDSucursal,
			@vNroComprobante = Numero
	From ControlInventario
	Where IDTransaccion = @IDTransaccion

	If @vFecha Is Null Begin
		Set @vFecha = (Select GETDATE())
	End

	--BLOQUES

	----Se bloquea la operacion porque en contabilidad se esta utilizando el idoperacion 36 para realizar asientos manuales
		set @Mensaje = 'Operacion Bloqueada, Contacte con el departamento de Informatica!'
		set @Procesado = 'False'
		return @@rowcount	

		
	--ACTUALIZAR
	If @Operacion='INS' Begin

		--Validar
		--Si no existe
		If Not Exists(Select * From ControlInventario Where IDTransaccion = @IDTransaccion) Begin
			set @Mensaje = 'El sistema no encuentra el registro seleccionado!'
			set @Procesado = 'False'
			return @@rowcount	
		End
		
		--COMENTADO TEMPORALMENTE!!!!
		--Si no esta controlado los equipos
		--If (Select ControlSistema From ControlInventario Where IDTransaccion = @IDTransaccion) = 'False' Begin
		--	set @Mensaje = 'Debe primeramente controlar los equipos de conteo y sus diferencias.!'
		--	set @Procesado = 'False'
		--	return @@rowcount	
		--End
		
		--Si ya esta controlado
		If (Select Procesado From ControlInventario Where IDTransaccion = @IDTransaccion) = 'True' Begin
			set @Mensaje = 'El registro ya esta controlado!'
			set @Procesado = 'False'
			return @@rowcount	
		End
		
		--Actualizamos segun la diferencias
		
		Declare db_cursor cursor for
		Select IDDeposito, IDProducto, DiferenciaSistema From ExistenciaDepositoInventario Where IDTransaccion=@IDTransaccion and DiferenciaSistema<>0
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDDeposito, @vIDProducto, @vDiferenciaSistema
		While @@FETCH_STATUS = 0 Begin  
			Set @vCosto = (Select CostoPromedio From Producto Where ID = @vIDProducto)
			If @vDiferenciaSistema < 0 Begin
				Set @vDiferenciaSistema = @vDiferenciaSistema * -1
				Set @vSigno = '-'
			End Else Begin
				Set @vSigno = '+'		
			End
						
			EXEC SpActualizarProductoDeposito
				@IDDeposito = @vIDDeposito,
				@IDProducto = @vIDProducto,
				@Cantidad = @vDiferenciaSistema,
				@Signo = @vSigno,
				@Mensaje = @Mensaje OUTPUT,
				@Procesado = @Procesado OUTPUT
			
			If @vSigno = '-' Begin
				--Actualiza el Kardex

				EXEC SpKardex
					@Fecha= @vFecha,
					@IDTransaccion = @IDTransaccion,
					@IDProducto = @vIDProducto,
					@ID = 1,
					@Orden = 20, 
					@CantidadEntrada = 0,
					@CantidadSalida = @vDiferenciaSistema,
					@CostoEntrada = 0,
					@CostoSalida = @vCosto,
					@IDSucursal = @vIDSucursal,
					@Comprobante = @vNroComprobante				
			End	

			If @vSigno = '+' Begin
					--Actualiza el Kardex
				EXEC SpKardex
					@Fecha= @vFecha,
					@IDTransaccion = @IDTransaccion,
					@IDProducto = @vIDProducto,
					@ID = 1,
					@Orden = 20, 
					@CantidadEntrada = @vDiferenciaSistema,
					@CantidadSalida = 0,
					@CostoEntrada = @vCosto,
					@CostoSalida = 0,
					@IDSucursal = @vIDSucursal,
					@Comprobante = @vNroComprobante				
			End
					
			Fetch Next From db_cursor Into @vIDDeposito, @vIDProducto, @vDiferenciaSistema
				
		End
		
		Close db_cursor   
		Deallocate db_cursor		
				
		Update ControlInventario Set Procesado = 'True', 
										IDUsuarioAjuste=@IDUsuario,
										FechaAjuste=GETDATE()
										
		Where IDTransaccion=@IDTransaccion				
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount	
		   
	End

	--ANULAR
	If @Operacion='ANULAR' Begin
		
		--Validar
		--Bloqueo por Fecha del cierre de stock
		If dbo.FValidarFechaBloqueoCierreStock(@vFecha) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido por cierre de stock!!'
			Set @Procesado = 'False'
			Return @@rowcount
		End

		--Si no existe
		If Not Exists(Select * From ControlInventario Where IDTransaccion = @IDTransaccion) Begin
			set @Mensaje = 'El sistema no encuentra el registro seleccionado!'
			set @Procesado = 'False'
			return @@rowcount	
		End
		
		--Si ya esta Procesado
		If (Select Procesado From ControlInventario Where IDTransaccion = @IDTransaccion) = 'False' Begin
			set @Mensaje = 'El registro no esta controlado aun!'
			set @Procesado = 'False'
			return @@rowcount	
		End
		
		Declare db_cursorAnular cursor for
		Select IDDeposito, IDProducto, DiferenciaSistema From ExistenciaDepositoInventario Where IDTransaccion=@IDTransaccion
		Open db_cursorAnular   
		Fetch Next From db_cursorAnular Into @vIDDeposito, @vIDProducto, @vDiferenciaSistema
		While @@FETCH_STATUS = 0 Begin  
			Set @vCosto = (Select CostoPromedio From Producto Where ID = @vIDProducto)
			If @vDiferenciaSistema < 0 Begin
				Set @vDiferenciaSistema = @vDiferenciaSistema * -1
				Set @vSigno = '+'
			End Else Begin
				Set @vSigno = '-'		
			End
						
			EXEC SpActualizarProductoDeposito
				@IDDeposito = @vIDDeposito,
				@IDProducto = @vIDProducto,
				@Cantidad = @vDiferenciaSistema,
				@Signo = @vSigno,
				@Mensaje = @Mensaje OUTPUT,
				@Procesado = @Procesado OUTPUT
			
			If @vSigno = '+' Begin
				--Actualiza el Kardex
				--Vuelvo a multiplicar la diferencia por -1 para revertir
				Set @vDiferenciaSistema = @vDiferenciaSistema*-1

				EXEC SpKardex
					@Fecha= @vFecha,
					@IDTransaccion = @IDTransaccion,
					@IDProducto = @vIDProducto,
					@ID = 1,
					@Orden = 20, 
					@CantidadEntrada = 0,
					@CantidadSalida = @vDiferenciaSistema,
					@CostoEntrada = 0,
					@CostoSalida = @vCosto,
					@IDSucursal = @vIDSucursal,
					@Comprobante = @vNroComprobante				
			End	

			If @vSigno = '-' Begin
				--Actualiza el Kardex
				--Vuelvo a multiplicar la diferencia por -1 para revertir
				Set @vDiferenciaSistema = @vDiferenciaSistema*-1
				EXEC SpKardex
					@Fecha= @vFecha,
					@IDTransaccion = @IDTransaccion,
					@IDProducto = @vIDProducto,
					@ID = 1,
					@Orden = 20, 
					@CantidadEntrada = @vDiferenciaSistema,
					@CantidadSalida = 0,
					@CostoEntrada = @vCosto,
					@CostoSalida = 0,
					@IDSucursal = @vIDSucursal,
					@Comprobante = @vNroComprobante				
			End
					
			Fetch Next From db_cursorAnular Into @vIDDeposito, @vIDProducto, @vDiferenciaSistema
				
		End
		
		Close db_cursorAnular   
		Deallocate db_cursorAnular	
		
		Update ControlInventario Set Procesado = 'False', 
										IDUsuarioAjuste=NULL,
										FechaAjuste=NULL
										
		Where IDTransaccion=@IDTransaccion
		
		--Eliminar el asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount	
		
	End
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End

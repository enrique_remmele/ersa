﻿CREATE Procedure [dbo].[SpPlanCuenta]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Titular bit,
	@Resolucion173 bit,
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From PlanCuenta Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDPlanCuenta tinyint
		set @vIDPlanCuenta = (Select IsNull((Max(ID)+1), 1) From PlanCuenta)

		If @Titular = 'True' Begin
			Update PlanCuenta Set Titular='False'
		End
		
		--Insertamos
		Insert Into PlanCuenta(ID, Descripcion, Estado, Titular, Resolucion173)
		Values(@vIDPlanCuenta, @Descripcion, @Estado, @Titular, @Resolucion173)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PLAN DE CUENTAS', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@vIDPlanCuenta
		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From PlanCuenta Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From PlanCuenta Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		If @Titular = 'True' Begin
			Update PlanCuenta Set Titular='False'
		End
		
		--Actualizamos
		Update PlanCuenta Set	Descripcion=@Descripcion,
								Titular = @Titular,
								Resolucion173=@Resolucion173,
						 Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PLAN DE CUENTAS', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From PlanCuenta Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Productos
		if exists(Select * From CuentaContable CC Join DetalleAsiento DA On CC.ID=DA.IDCuentaContable Where CC.IDPlanCuenta=@ID) begin
			set @Mensaje = 'El registro tiene cuentas asociadas! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From CuentaContable
		Where IDPlanCuenta=@ID
				
		Delete From PlanCuenta 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PLAN DE CUENTAS', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿
CREATE Procedure [dbo].[SpChequera]

	--Entrada
	@ID int,
	@NroComprobante int,
	@IDBanco tinyint,
	@IDCuentaBancaria int,
	@Observacion varchar(50),
	@NroDesde int,
	@NroHasta int,
	@Ultimo int,
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que el numero de comprobante ya existe
		if exists(Select * From Chequera Where NroComprobante = @NroComprobante) begin
			set @Mensaje = 'El Numero de comprobante ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Ya existe la chequera
		if exists(Select * From Chequera Where IdBanco = @IdBanco and IdCuentaBancaria = @IdCuentaBancaria
		and NroDesde = @NroDesde and NroHasta = @NroHasta) begin
			set @Mensaje = 'La Chequera ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDChequera int
		set @vIDChequera = (Select IsNull((Max(ID)+1), 1) From Chequera)

		--Insertamos
		Insert Into Chequera(ID, NroComprobante, IdBanco, IDCuentaBancaria,Observacion,NroDesde,NroHasta,AUtilizar, Estado)
		Values(@vIDChequera, @NroComprobante, @IdBanco, @IdCuentaBancaria, @Observacion, @NroDesde, @NroHasta, @Ultimo, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CHEQUERA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
				 		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From Chequera Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Chequera Where NroComprobante = @NroComprobante And ID!=@ID) begin
			set @Mensaje = 'El Numero de comprobante ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Ya existe la chequera
		--if exists(Select * From Chequera Where IdBanco = @IdBanco and IdCuentaBancaria = @IdCuentaBancaria
		--and NroDesde = @NroDesde and NroHasta = @NroHasta and NroComprobante = @NroComprobante) begin
		--	set @Mensaje = 'La Chequera ya existe!'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--end
		
		--Actualizamos
		Update Chequera Set NroComprobante = @NroComprobante,
						IDBanco = @IDBanco,
						IDCuentaBancaria = @IDCuentaBancaria,
						Observacion = @Observacion,
						NroDesde = @NroDesde,
						NroHasta = @NroHasta,
						Estado =@Estado,
						AUtilizar = @Ultimo
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CHEQUERA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Chequera Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con ChequeCliente
		if exists(Select * From Chequera Where ID=@ID and AUtilizar > 0) begin
			set @Mensaje = 'La Chequera ya fue utilizada! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		

		
		--Eliminamos
		Delete From Chequera 
		Where ID=@ID
		
	--	--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CHEQUERA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


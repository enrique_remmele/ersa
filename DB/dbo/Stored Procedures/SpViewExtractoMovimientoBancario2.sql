﻿
CREATE Procedure [dbo].[SpViewExtractoMovimientoBancario2]

	--Entrada
	@FechaDesde Date,
	@FechaHasta Date,
	@IDCuentaBancaria tinyint = 0,
	@ChequeDiferido bit = 'False'

As

Begin
  
	--Declarar variables 
	declare @vID int
	declare @vIDSucursal int
	declare @vIDCuentaBancaria int
	declare @vFecha Date
	declare @vOperacion varchar (50)
	declare @vCompCheque varchar (50)
	declare @vOrdendeDetalleConcepto varchar (250)
	declare @vDebito money
	declare @vCredito money
	declare @vMovimiento varchar (50)
	declare @vSaldoAnterior money
	declare @vSaldoInicial money
	declare @vSaldo money
	declare @vDeditoTemporal money
	declare @vCreditoTemporal money
	declare @vCuentaBanco varchar(50)
	declare @vCuenta varchar(50)
	declare @vCuentaNombre varchar(50)
	declare @vCodigoCC varchar(50)
	declare @vDecimales bit
	declare @vMoneda varchar(50)
	
	
	--Insertar datos	
	Begin
	
	--Crear la tabla temporal
    create table #TablaTemporal(ID int,
								IDSucursal int,
								IDCuentaBancaria int,
								Fecha date,
								Operacion varchar(50),
								CompCheque varchar(50),
								OrdenDetalleConcepto varchar(250),
								CuentaBanco varchar(50),
								Cuenta varchar(50),
								CuentaNombre varchar(50),
								CodigoCC varchar(50),
								Decimales bit,
								Moneda varchar(50),
								Debito money,
								Credito money,
								Movimiento varchar(50),
								SaldoInicial money,
								SaldoAnterior money,
								Saldo money)
								
		
		--Hallar ID
		Set @vID = (Select IsNull(MAX(ID)+1,1) From #TablaTemporal)
		
		--Calcular Saldo Anterior
		Set @vDeditoTemporal= ISNull((Select SUM (debito) From VExtractoMovimientoBancario Where Fecha < @FechaDesde And IDCuentaBancaria=@IDCuentaBancaria),0) 
		Set @vCreditoTemporal = ISnull((Select SUM (Credito) From VExtractoMovimientoBancario Where Fecha < @FechaDesde And IDCuentaBancaria=@IDCuentaBancaria),0)
		Set @vSaldoAnterior = @vDeditoTemporal - @vCreditoTemporal 
		Set @vSaldoInicial= @vSaldoAnterior  
	
		Declare db_cursor cursor for
		
		Select 
		IDSucursal,
		IDCuentaBancaria,
		Fecha,
		Operacion,
		[Comp/Cheque],
		[Ordende/Detalle/Concepto],
		CuentaBanco,
		Cuenta,
		CuentaNombre,
		CodigoCC,
		Decimales,
		Moneda,
		Debito,
		Credito,
		Movimiento 
		From VExtractoMovimientoBancario
		Where (Case When (@ChequeDiferido) = 'False' Then FechaVencimiento Else Fecha End) between @FechaDesde And @FechaHasta
		And IDCuentaBancaria=@IDCuentaBancaria
		Order By Fecha
		
		Open db_cursor   
		Fetch Next From db_cursor Into  @vIDSucursal,@vIDCuentaBancaria,@vFecha,@vOperacion,@vCompCheque,@vOrdendeDetalleConcepto,@vCuentaBanco,@vCuenta,@vCuentaNombre,@vCodigoCC,@vDecimales,@vMoneda,@vDebito,@vCredito,@vMovimiento
		While @@FETCH_STATUS = 0 Begin  
			--Saldo
			Set @vSaldo= (@vSaldoAnterior + @vDebito ) - @vCredito 
			
			--If @vSaldo < 0 Begin
			--	Set @vSaldo = @vSaldo * -1	
			--End
			
			--Insertar en la tabla Extracto de Movimiento Bancario
			Insert Into #TablaTemporal(ID,IDSucursal,IDCuentaBancaria,Fecha,Operacion,CompCheque,OrdenDetalleConcepto,CuentaBanco,Cuenta,CuentaNombre,CodigoCC,Decimales,Moneda,Debito,Credito,Movimiento,SaldoInicial,SaldoAnterior,Saldo)  
			Values(@vID,@vIDSucursal,@vIDCuentaBancaria,@vFecha,@vOperacion,@vCompCheque,@vOrdendeDetalleConcepto,@vCuentaBanco,@vCuenta,@vCuentaNombre,@vCodigoCC,@vDecimales,@vMoneda,@vDebito,@vCredito,@vMovimiento,@vSaldoInicial,@vSaldoAnterior,@vSaldo)  
			
			--Actualizar Saldo
			set @vSaldoAnterior = @vSaldo 
			
			Fetch Next From db_cursor Into @vIDSucursal,@vIDCuentaBancaria,@vFecha,@vOperacion,@vCompCheque,@vOrdendeDetalleConcepto,@vCuentaBanco,@vCuenta,@vCuentaNombre,@vCodigoCC,@vDecimales,@vMoneda,@vDebito,@vCredito,@vMovimiento
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		
	End	
	
	Select * From #TablaTemporal Where ID=@vID Order By Fecha

	
End
	





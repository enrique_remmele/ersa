﻿CREATE Procedure [dbo].[SpAsientoCobranzaContado]

	@IDTransaccion numeric(18,0)
		
As

Begin
	
	--Variables
	Declare @vIDSucursal tinyint
	
	--Cobranzas
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vFecha date
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money
	
	--Venta
	Declare @vCredito bit
	Declare @vContado bit
	
	--Efectivo
	Declare @vIDTipoComprobanteEfectivo tinyint
	
	--Cheque
	Declare @vDiferido bit
	Declare @vIDCuentaBancaria int
	
	--Documento
	Declare @vIDTipoComprobanteDocumento tinyint
	
	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vCodigo varchar(50)
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money
	Declare @vImporte money
	
	--Obtener valores
	Begin
	
		
		Select	@vIDSucursal=IDSucursal,
				@vFecha=Fecha,
				@vIDTipoComprobante=IDTipoComprobante,
				@vComprobante=Comprobante,
				@vIDMoneda=1,
				@vCotizacion=1
		From VCobranzaContado Where IDTransaccion=@IDTransaccion
		
	End
					
	--Verificar que el asiento se pueda modificar
	Begin
	
		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta procesado
		If (Select Procesado From CobranzaContado Where IDTransaccion=@IDTransaccion) = 'False' Begin
			print 'La cobranza no es valida'
			GoTo salir
		End 	
		
		--Si esta anulado
		If (Select Anulado From CobranzaContado Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'La cobranza esta anulada'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		
		print 'Los datos son correctos!'
		
	End
			
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
		
		print 'Registros de Asiento y Detalle eliminados!'
		
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = 'Cobranzas del ' + CONVERT(varchar(50), @vFecha)
		
		print 'Tipo de Comprobante:' + convert(varchar(50), @vTipoComprobante)
		print 'Comprobante:' + convert(varchar(50), @vComprobante)
				
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
		print 'Cabecera cargada!'
		
	End
	
	--Ventas
	Begin
		
		--Variables
		
		Declare cCFVenta cursor for
		Select Codigo, Debe, Haber, Credito, Credito From VCFCobranzaVenta 
		Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda and AnticipoCliente = 0
		Open cCFVenta   
		fetch next from cCFVenta into @vCodigo, @vDebe, @vHaber, @vCredito, @vContado
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			Set @vImporte = (Select SUM(Importe) From VentaCobranza Where IDTransaccionCobranza=@IDTransaccion)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFVenta into @vCodigo, @vDebe, @vHaber, @vCredito, @vContado
			
		End
		
		close cCFVenta 
		deallocate cCFVenta
		
	End
	
	--Efectivo
	Begin
		
		--Variables
		Declare cCFEfectivo cursor for
		Select Codigo, Debe, Haber, IDTipoComprobante From VCFCobranzaEfectivo
		Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFEfectivo   
		fetch next from cCFEfectivo into @vCodigo, @vDebe, @vHaber, @vIDTipoComprobanteEfectivo
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Hayar el importe
			Set @vImporte = (Select SUM(FP.Importe) From FormaPago FP Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID Where FP.IDTransaccion=@IDTransaccion And E.IDTipoComprobante=@vIDTipoComprobanteEfectivo)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFEfectivo into @vCodigo, @vDebe, @vHaber, @vIDTipoComprobanteEfectivo
			
		End
		
		close cCFEfectivo 
		deallocate cCFEfectivo
		
	End
	
	--Cheque
	Begin
		
		--Variables
		Declare cCFCheque cursor for
		Select Codigo, Debe, Haber, Diferido, IDCuentaBancaria From VCFCobranzaCheque
		Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFCheque 
		fetch next from cCFCheque into @vCodigo, @vDebe, @vHaber, @vDiferido, @vIDCuentaBancaria
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Hayar el importe
			Set @vImporte = IsNull((Select SUM(FP.Importe) From FormaPago FP Join ChequeCliente CQH On FP.IDTransaccionCheque=CQH.IDTransaccion Where FP.IDTransaccion=@IDTransaccion And CQH.Diferido=@vDiferido),0)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFCheque into @vCodigo, @vDebe, @vHaber, @vDiferido, @vIDCuentaBancaria
			
		End
		
		close cCFCheque 
		deallocate cCFCheque
		
	End
	
	--Documentos
	Begin
		
		--Variables
		Declare cCFDocumento cursor for
		Select Codigo, Debe, Haber, IDTipoComprobante From VCFCobranzaDocumento
		Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		--Where IDMoneda=@vIDMoneda
		Open cCFDocumento 
		fetch next from cCFDocumento into @vCodigo, @vDebe, @vHaber, @vIDTipoComprobanteDocumento
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Hayar el importe
			Set @vImporte = IsNull((Select SUM(FP.Importe) From FormaPago FP Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID Where FP.IDTransaccion=@IDTransaccion And FPD.IDTipoComprobante=@vIDTipoComprobanteDocumento),0)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFDocumento into @vCodigo, @vDebe, @vHaber, @vIDTipoComprobanteDocumento
			
		End
		
		close cCFDocumento 
		deallocate cCFDocumento
		
	End
	
	--Actualizamos la cabecera, el total
	Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
	Set @vImporteHaber = ROUND(@vImporteHaber, 0)
	Set @vImporteDebe = ROUND(@vImporteDebe, 0)
	
	Update Asiento Set Total = @vImporteHaber,
						Credito = @vImporteHaber,
						Debito = @vImporteDebe,
						Saldo = @vImporteHaber - @vImporteDebe
	Where IDTransaccion=@IDTransaccion
	
	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
	
End


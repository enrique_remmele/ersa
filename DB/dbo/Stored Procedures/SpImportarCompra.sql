﻿CREATE Procedure [dbo].[SpImportarCompra]

	--Cabecera
	@TipoComprobante varchar(10),
	@Comprobante varchar(50),
	@Ruc varchar (50),
	@Proveedor varchar(50),
	@Fecha date,
	@FechaEntrada date,
	@CondicionCompra bit,
	@FechaVencimiento date,
	@Sucursal varchar(50),
	@Deposito varchar(50),
		
	@Cotizacion money,
	@Observacion varchar(200),
	
	--Timbrado
	@NroTimbrado varchar(50),
	@FechaVencimientoTimbrado date,
	
	--Totales
	@Total money,
	@TotalImpuesto money,
	@Saldo money,
	
	--Impuesto
	@Exento as money,
	@Gravado10 as money,
	@IVA10 as money,
	@Gravado5 as money,
	@IVA5 as money,
			
	--Datos Extras
	@NroOperacion int,
	@RetencionIVA as money,
	
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
		
	@Actualizar bit
	
As

Begin
		
	--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		
		Declare @vCredito bit
		Declare @vIDMoneda int
		Declare @vOperacion varchar(50)
		Declare @vIDTransaccion int
		
		--Codigos
		Declare @vNumero int
		Declare @vIDTipoComprobante int
		Declare @vIDProveedor int
		Declare @vIDSucursal int
		Declare @vIDDeposito int
				
	End
	
	--Establecer valores predefinidos
	Begin
		
		Set @vOperacion = 'INS'
		Set @vIDMoneda = 1
	
		Set @vCredito = 'False'
		If @CondicionCompra = 2 Begin
			Set @vCredito = 'True'
		End
				
	End
	
	--Verificar si ya existe
	If Exists(Select * From CompraImportada Where NroOperacion=@NroOperacion) Begin
		set @vIDTransaccion = (Select IDTransaccion From CompraImportada Where NroOperacion=@NroOperacion)
		Set @vOperacion = 'UPD'
	End
	
	--ACTUALIZAR
	If @vOperacion = 'UPD' Begin
	
		If @Actualizar = 'True' Begin
			
			--Proveedor
			Set @vIDProveedor=(IsNull((Select Top(1) ID From Proveedor Where RUC =@Ruc),0))
			If @vIDProveedor = 0 Begin
				Set @vMensaje = 'Cliente incorrecto!'
				Set @vProcesado = 'False'
				GoTo Salir
			End
		
			--Venta
			Update Compra Set Credito=@vCredito,
								IDProveedor =@vIDProveedor,
								RetencionIVA=@RetencionIVA
			Where IDTransaccion = @vIDTransaccion
			
			Set @vMensaje = 'Actualizado!'
			Set @vProcesado = 'True'
			GoTo Salir
		End
		
		Set @vMensaje = 'Sin Act.!'
		Set @vProcesado = 'True'
		GoTo Salir
		
	End
	
	--Hayar Valores
	Begin
		
		Set @vMensaje = 'No se proceso!'
		Set @vProcesado = 'False'
				
		--Tipo de Comprobante
		Set @vIDTipoComprobante = (IsNull((Select Top(1) ID From TipoComprobante Where Codigo=@TipoComprobante And IDOperacion=@IDOperacion), 0))
		If @vIDTipoComprobante = 0 Begin
			Set @vMensaje = 'Tipo de comprobante incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Proveedor
		Set @vIDProveedor=(IsNull((Select Top(1) ID From Proveedor Where Referencia=@Proveedor),0))
		If @vIDProveedor = 0 Begin
			Set @vMensaje = 'Cliente incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Sucursal
		Set @vIDSucursal = (IsNull((Select Top(1) ID From Sucursal Where Descripcion=@Sucursal),0))
		If @vIDSucursal = 0 Begin
			Set @vMensaje = 'Sucursal incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Deposito
		Set @vIDDeposito = (IsNull((Select Top(1) ID From Deposito Where IDSucursal=@vIDSucursal And Descripcion=@Deposito),0))
		If @vIDDeposito = 0 Begin
			Set @vMensaje = 'Deposito incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
	End
	
	--INSERTAR
	If @vOperacion = 'INS' Begin
		
		--Validar Informacion
		Begin
		
			--Comprobante
			if Exists(Select * From Compra Where NroComprobante=@Comprobante) Begin
				set @vMensaje = 'El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
				set @vProcesado = 'False'
				GoTo Salir
			End
			
		End
		
		----Insertar la transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@MostrarTransaccion = 'False',
			@Mensaje = @vMensaje OUTPUT,
			@Procesado = @vProcesado OUTPUT,
			@IDTransaccion = @vIDTransaccion OUTPUT
		
		If @vProcesado = 'False' Begin
			GoTo Salir
		End
		
		Set @vNumero = ISNULL((Select MAX(Numero)+1 From Compra Where IDSucursal=@vIDSucursal),1)	
		
		--Insertar en Compra
		Insert Into Compra(IDTransaccion, Numero, IDTipoComprobante, NroComprobante,  NroTimbrado, IDProveedor, IDSucursal, IDDeposito, Fecha,  Credito, FechaVencimiento, FechaEntrada, FechaVencimientoTimbrado, IDMoneda, Cotizacion,  Observacion, Total, TotalImpuesto, TotalDiscriminado, Saldo,  Cancelado, Directo, RetencionIVA, RetencionRenta)
		Values(@vIDTransaccion, @vNumero, @vIDTipoComprobante, @Comprobante,  @NroTimbrado, @vIDProveedor, @vIDSucursal, @vIDDeposito, @Fecha,  @vCredito, @FechaVencimiento, @FechaEntrada, @FechaVencimientoTimbrado, @vIDMoneda, @Cotizacion,  @Observacion, @Total, @TotalImpuesto, @Total - @TotalImpuesto, @Saldo,  'False', 'True', @RetencionIVA, 0)
		
		--Insertar en CompraImportacion
		Insert Into CompraImportada(IDTransaccion, NroOperacion)
		Values(@vIDTransaccion, @NroOperacion)
		
		--Insertar Impuesto
		--10%
		Insert into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado)
		Values(@vIDTransaccion, 1, @Gravado10, @IVA10, 0, @Gravado10 - @IVA10)
		
		--5%
		Insert into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado)
		Values(@vIDTransaccion, 2, @Gravado5, @IVA5, 0, @Gravado5 - @IVA5)
		
		--Exento
		Insert into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado)
		Values(@vIDTransaccion, 3, @Exento, 0, 0, @Exento)
		
		Set @vMensaje = 'Registro guardado!'
		Set @vProcesado = 'True'
		GoTo Salir
				
	End
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


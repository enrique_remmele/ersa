﻿CREATE Procedure [dbo].[SpCompraLogistica]

	--CARLOS 12-02-2015
	
	--Entrada
	@IDTransaccion numeric(18,0),
	@IDProducto int,
	@IDCamion int,
	@Cantidad decimal(10,2),
	@Operacion varchar(20),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin
	
	--Validar
	--Transaccion
	If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--Transaccion
	If Not Exists(Select * From Compra Where IDTransaccion=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--Transaccion
	If Not Exists(Select * From DetalleCompra Where IDProducto=@IDProducto and IDTransaccion=@IDTransaccion) Begin
		Set @Mensaje = 'El producto no se encuentra asociado al comprobante!'
		Set @Procesado = 'False'
		return @@rowcount
	End


	--BLOQUES
	if @Operacion='INS' Begin
	
		--Cantidad
		If @Cantidad <= 0 Begin
			Set @Mensaje = 'La cantidad no puede ser igual ni menor a 0!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--Insertar el detalle
		Insert Into CompraLogistica(IDTransaccion, IDProducto, IDCamion, Cantidad)
		Values(@IDTransaccion, @IDProducto, @IDCamion, @Cantidad)
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'False'
		return @@rowcount	
			
	End
	
	if @Operacion='DEL' Begin
		
		Delete from CompraLogistica where idtransaccion=@IDTransaccion
		
		Set @Mensaje = 'Detalle eliminado!'
		Set @Procesado = 'True'
		return @@rowcount	
			
	End
	
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	return @@rowcount
	
End




﻿CREATE Procedure [dbo].[SpCuentaFijaGasto]

	--Entrada
	@ID tinyint = NULL,
	@IDCuentaContable smallint = NULL,
	@Debe bit = NULL,
	@Haber bit = NULL,
	@IDTipoComprobante smallint = NULL,
	@IDMoneda tinyint = NULL,
	@IDTipoCuentaFija tinyint = NULL,
	@Orden tinyint = NULL,
	@Descripcion varchar(50) = NULL,
	@BuscarEnProveedor bit = NULL,
	
	--Operacion
	@Operacion varchar(10) = NULL,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
As

Begin

	--INSERTAR
	If @Operacion='INS' Begin
	
		--Validar
		If Exists(Select * From CuentaFijaGasto Where IDCuentaContable=@IDCuentaContable And IDTipoComprobante=@IDTipoComprobante And IDMoneda=@IDMoneda And IDTipoCuentaFija=@IDTipoCuentaFija And Debe=@Debe And Haber=@Haber) Begin
			set @Mensaje = 'El registro ya existe!'
			set @Procesado = 'false'
			return @@rowcount
		End
		
		--Insertar
		Declare @vID tinyint
		Set @vID = (Select ISNULL((Max(ID)+1), 1) From CuentaFijaGasto)
		
		Insert Into CuentaFijaGasto(ID, IDCuentaContable, Debe, Haber, IDTipoComprobante, IDMoneda, IDTipoCuentaFija, Orden, Descripcion, BuscarProveedor)
		Values(@vID, @IDCuentaContable, @Debe, @Haber, @IDTipoComprobante, @IDMoneda, @IDTipoCuentaFija, @Orden, @Descripcion, @BuscarenProveedor)
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End	

	--ACTUALIZAR
	If @Operacion='UPD' Begin
	
		--Validar
		If Not Exists(Select * From CuentaFijaGasto Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro!'
			set @Procesado = 'True'
			return @@rowcount		
		End
		
		--El mismo tipo no debe existir
		If Exists(Select * From CuentaFijaGasto Where IDCuentaContable=@IDCuentaContable And IDTipoComprobante=@IDTipoComprobante And IDMoneda=@IDMoneda And IDTipoCuentaFija=@IDTipoCuentaFija And Debe=@Debe And Haber=@Haber And ID!=ID) Begin
			set @Mensaje = 'El tipo ya esta cargado en esta configuracion!'
			set @Procesado = 'True'
			return @@rowcount		
		End
		
		--Actualizar
		Update CuentaFijaGasto Set	IDCuentaContable=@IDCuentaContable,
									Debe=@Debe,
									Haber=@Haber,
									IDTipoComprobante=@IDTipoComprobante,
									IDMoneda=@IDMoneda,
									IDTipoCuentaFija=@IDTipoCuentaFija,
									Orden=@Orden,
									Descripcion=@Descripcion,
									BuscarProveedor=@BuscarEnProveedor
								
		Where ID=@ID 
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	--ELIMINAR
	If @Operacion='DEL' Begin
	
		--Validar
		If Not Exists(Select * From CuentaFijaGasto Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro!'
			set @Procesado = 'True'
			return @@rowcount		
		End
		
		Delete From CuentaFijaGasto Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
	End	
	
End


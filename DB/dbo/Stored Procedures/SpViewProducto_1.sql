﻿


CREATE Procedure [dbo].[SpViewProducto]

	--Opciones
		
	--Identificadores
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int
	
As

Begin


	Select 
	ID, 
	Descripcion, 
	CodigoBarra, 
	Ref,
	ProductoReferencia, 
	UnidadPorCaja,
	UnidadMedida, 
	'UnidadConvertir'=convert (decimal (10,3),UnidadConvertir ),
	UnidadMedidaConvertir,
	IDImpuesto, 
	Impuesto, 
	Costo,
	UltimoCosto,
	[Ref Imp],
	'Precio'=0,
	Exento,
	ControlarExistencia,
	Estado,
	
	--Descuentos
	'TotalDescuento'=0,
	'DescuentoUnitario'=0,
	'PorcentajeDescuento'=0,
	
	--Clasificaciones
	IDClasificacion,
	Clasificacion,
	CodigoClasificacion,
	NivelClasificacion,
	NumeroClasificacion,
	
	IDTipoProducto,
	IDLinea,
	IDSubLinea,
	IDSubLinea2,
	IDMarca,
	IDPresentacion,
	
	--'Existencia'=dbo.FExistenciaProducto(ID, @IDDeposito),
	'Existencia'=0,
	
	--CuentaContable
	CodigoCuentaVenta,
	CodigoCuentaCompra
		 
	From VProducto 
	Where Estado='True'
			
	Order By Descripcion
		
	
End




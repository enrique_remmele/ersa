﻿CREATE Procedure [dbo].[SpSolicitudProductoPrecio]


	--Entrada
	@Producto varchar(50),
	@Cliente varchar(50),
	@Moneda varchar(10),
	@TipoPrecio varchar(50),
	@Precio money,
	@Desde date,
	@CantidadMinimaPrecioUnico int = 0,

	--Transaccion
	@IDUsuario smallint,
	
	--Salida
	@Mensaje varchar(200) = '' output,
	@Procesado bit output
As

Begin

	--BLOQUES
	Declare @vIDMoneda tinyint
	Declare @vIDCLiente int
	Declare @vIDProducto int
	Declare @vIDListaPrecio int
	Declare @vPrecioUnico bit 
	Declare @vIDTipoProducto int 
	Declare @vListaPrecio varchar(50)

	
	
	Set @vIDProducto = ISnull((Select id from Producto where Referencia = @Producto),0)
	Set @vIDTipoProducto = ISnull((Select IDTipoProducto from Producto where Referencia = @Producto),0)
	
	if @vIDTipoProducto = 1 begin
		Set @vIDListaPrecio = 1
	end 
	else begin
		Set @vIDListaPrecio = ISnull((Select IDListaPrecio from Cliente where Referencia = @Cliente),0)
		Set @vListaPrecio = ISnull((Select Descripcion from ListaPrecio where ID = @vIDListaPrecio),0)
	end
	
	if @TipoPrecio = 'UNICO' begin
		Set @vPrecioUnico = 1
	end
	if @TipoPrecio = 'FIJO' begin
		Set @vPrecioUnico = 0
	end

	--Validar Datos ingresados
	If @vIDProducto = 0 begin
		Set @Mensaje = Concat('No se encuentra referencia de producto ', @Producto, '. ')
		Set @Procesado = 'False'
	end

	Set @vIDCLiente = ISnull((Select id from Cliente where Referencia = @Cliente),0)

	If @vIDCLiente = 0 begin
		Set @Mensaje = Concat(@Mensaje,'No se encuentra referencia de cliente ', @Cliente, '. ')
		Set @Procesado = 'False'
	end

	Set @vIDMoneda = ISnull((Select id from Moneda where Referencia = @Moneda),0)
	
	If @vIDMoneda = 0 begin
		Set @Mensaje = Concat(@Mensaje,'No se encuentra moneda ', @Moneda, ', intente con GS o US. ')
		Set @Procesado = 'False'
	end

	--Validar Solicitud
	if @vIDTipoProducto = 1 begin
		if not exists(select * from vProductoListaPrecio where IDProducto = @vIDProducto and IDListaPrecio = @vIDListaPrecio) begin
			Set @Mensaje = concat('El producto ', @Producto, ' no tiene un precio de lista asignado')
			Set @Procesado = 'False'
			return @@rowcount
		end

		if (select sum(Precio) from vProductoListaPrecio where IDProducto = @vIDProducto and IDListaPrecio = @vIDListaPrecio) = 0 begin
			Set @Mensaje = concat('El producto ', @Producto, ' no tiene un precio de lista asignado')
			Set @Procesado = 'False'
			return @@rowcount
		end
	end 
	else begin
		if not exists(select * from vProductoListaPrecio where IDProducto = @vIDProducto and ListaPrecio = @vListaPrecio) begin
			Set @Mensaje = concat('El producto ', @Producto, ' no tiene un precio de lista asignado')
			Set @Procesado = 'False'
			return @@rowcount
		end

		if (select sum(Precio) from vProductoListaPrecio where IDProducto = @vIDProducto and ListaPrecio = @vListaPrecio) = 0 begin
			Set @Mensaje = concat('El producto ', @Producto, ' no tiene un precio de lista asignado')
			Set @Procesado = 'False'
			return @@rowcount
		end
	end
	if Exists(select * from SolicitudProductoPrecio where IDProducto = @vIDProducto and IDCliente = @vIDCLiente and PrecioUnico = @vPrecioUnico and Estado is null) begin
		Set @Mensaje = 'La configuracion ya esta cargada y se encuentra pendiente de aprobacion'
		Set @Procesado = 'False'
		return @@rowcount
	end

		
	if Exists(select * from SolicitudProductoPrecio where IDProducto = @vIDProducto and IDCliente = @vIDCLiente and PrecioUnico = @vPrecioUnico and Estado = 0)begin
		Set @Mensaje = 'La configuracion ya esta cargada y fue rechazada'
		Set @Procesado = 'False'
		return @@rowcount
	end

	Insert Into SolicitudProductoPrecio(IDProducto, IDCliente, IDMoneda, PrecioUnico, Precio, Desde, CantidadMinimaPrecioUnico, FechaSolicitud,IDUsuarioSolicitud)
							Values(@vIDProducto, @vIDCliente, @vIDMoneda, @vPrecioUnico, @Precio, @Desde, @CantidadMinimaPrecioUnico, GETDATE(),@IDUsuario)		
	
	Set @Mensaje = 'Registro guardado'
	Set @Procesado = 'True'
	return @@rowcount	


End


﻿CREATE Procedure [dbo].[SpViewResumenCompraProveedor]




--Entrada
	@FechaDesde Date,
	@FechaHasta Date	
	
As

Begin
	--Crear la tabla temporal
    create table #TablaTemporal(IDTransaccion int,
								Proveedor varchar(50),
								RefProveedor varchar(30),
								Producto varchar(50),
								IDProducto int,
								RefProducto varchar(30),
								CantidadTotal int,
								Caja int,
								Peso varchar(50),
								CompraTotal money,
								PromedioCompra money,
								CostoTotal money,
								PromedioCosto money,
								PrimeraCompra varchar(50),
								UltimaCompra varchar(50),
								)
    

	
	--Declarar variables
	declare @vIDTransaccion Numeric (18,0)
	declare @vProveedor varchar (100)
	declare @vReferencia varchar (50)
	declare @vProducto varchar (50)
	declare @vIDProducto int
	declare @vRefProducto varchar (50)
	declare @vCantidad int
	declare @vCajas int 
	declare @vPeso varchar (50)
	declare @vPrecioUnitario money
	declare @vFactorImpuesto decimal (10,7)
	
	declare @vPromedioCosto money 
	declare @vCompraTotal money
	declare @vCostoTotal money
	declare @vPrimaraCompra date
	declare @vUltimaCompra date
	declare @vfecPrimera varchar(50)
	declare @vfecUltima varchar(50)
	
	--Insertar datos	
	Begin
	
		Declare db_cursor cursor for
		
		Select C.IDTransaccion, C.Proveedor, C.Referencia, DC.IDProducto, DC.Producto, DC.Referencia, DC.Cantidad, DC.Cajas, Peso, DC.PrecioUnitario, I.FactorImpuesto 
		From VCompra C
		Join VDetalleCompra DC On C.IDTransaccion=DC.IDTransaccion 
		Join Impuesto I On DC.IDImpuesto= I.ID
		Where C.Fecha Between @FechaDesde And @FechaHasta
			
		Open db_cursor   
		Fetch Next From db_cursor Into  @vIDTransaccion, @vProveedor, @vReferencia, @vIDProducto, @vProducto, @vRefProducto, @vCantidad, @vCajas, @vPeso, @vPrecioUnitario, @vFactorImpuesto  
		While @@FETCH_STATUS = 0 Begin 
		
			Set @vCompraTotal = @vPrecioUnitario * @vCantidad 	
			Set @vPromedioCosto  = @vPrecioUnitario/@vFactorImpuesto 
			Set @vCostoTotal = @vPromedioCosto * @vCantidad 
			set @vPrimaraCompra =  (Select MIN (Fecha) from VDetalleCompra where IDProducto = @vIDProducto  And Fecha Between @FechaDesde and @FechaHasta )
			set @vUltimaCompra = (Select  MAX  (Fecha) from VDetalleCompra where IDProducto = @vIDProducto  And Fecha Between @FechaDesde and @FechaHasta )
			
			set @vfecPrimera = CONVERT (varchar (50), @vPrimaraCompra , 10)
			set @vfecUltima  = CONVERT (varchar (50), @vUltimaCompra  , 10)
			
			Insert Into  #TablaTemporal(IDTransaccion, Proveedor, RefProveedor, Producto, IDProducto, RefProducto, CantidadTotal, Caja, Peso, CompraTotal, PromedioCompra, CostoTotal, PromedioCosto, PrimeraCompra, UltimaCompra) 
								Values (@vIDTransaccion, @vProveedor ,@vReferencia, @vProducto ,@vIDProducto, @vRefProducto, @vCantidad ,@vCajas, @vPeso, @vCompraTotal, @vPrecioUnitario, @vCostoTotal, @vPromedioCosto, @vfecPrimera , @vfecUltima )
			
			Fetch Next From db_cursor Into @vIDTransaccion, @vProveedor, @vReferencia, @vIDProducto, @vProducto, @vRefProducto, @vCantidad, @vCajas, @vPeso, @vPrecioUnitario, @vFactorImpuesto  
			
		End
 
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		
	End	
	
	Select * From #TablaTemporal
	
	
	
	
End



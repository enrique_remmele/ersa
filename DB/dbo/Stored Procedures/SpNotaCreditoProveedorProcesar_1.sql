﻿

CREATE Procedure [dbo].[SpNotaCreditoProveedorProcesar]

	--Entrada
	@IDTransaccion numeric(18),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
As	
	If @Operacion = 'INS' Begin
	PRINT 'Entro'
		Exec SpNotaCreditoProveedorActualizarStock @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output   
		Exec SpNotaCreditoProveedorActualizarCompra @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output     			
		--Exec SpAsientoNotaCreditoDevolucionProveedor @IDTransaccion=@IDTransaccion

	--Damos el OK Nota Credito
		If @Procesado = 'True' Begin
			Update NotaCreditoProveedor   Set Procesado='True' Where IDTransaccion=@IDTransaccion
			Set @Mensaje = 'Registro procesado!'
			Return @@rowcount
		End Else Begin
	
			--Si hubo un error, eliminar todo el registro
			Set @Operacion = 'DEL'			
			
		End
	
	
	
	End
	
    
	
	
	If @Operacion  = 'DEL' Begin
	
		If (Select Anulado From NotaCreditoProveedor  Where IDTransaccion=@IDTransaccion) = 'True' Begin
			Set @Mensaje = 'Registro guardado'
			Set @Procesado = 'True'
			return @@rowcount
	End
		Exec SpNotaCreditoProveedorActualizarStock @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output   
		Exec SpNotaCreditoProveedorActualizarCompra @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output     			
		
		Delete NotaCreditoProveedorCompra  Where IDTransaccionNotaCreditoProveedor=@IDTransaccion 
		
			
		
	END
	


﻿CREATE Procedure [dbo].[SpUsuarioRecepcionDocumento]

	--Entrada
	@IDUsuario int,
	@Operacion varchar(10),
	
	@Mensaje varchar(200) = '' output ,
	@Procesado bit = 0 output 
As

Begin

	--INSERTAR
	if @Operacion='INS' begin
		
		if exists(Select * from UsuarioRecepcionDocumento where idusuario = @IDUsuario) begin
			set @Mensaje = 'El usuario ya existe en la configuracion!'
			set @Procesado = 'False'
			return @@rowcount
		end
		--Insertamos
		Insert Into UsuarioRecepcionDocumento(IDUsuario)
		Values(@IDUsuario)		
				 		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End

	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From UsuarioRecepcionDocumento where idusuario = @IDUsuario) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
				
		--Eliminamos
		Delete From UsuarioRecepcionDocumento 
		Where IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿CREATE Procedure [dbo].[SpAsientoDebitoCreditoBancario]

	@IDTransaccion numeric(18,0)
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint = 0
	
	--
	Declare @vIDTransaccion numeric(18,0)
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vFechaEmision date
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money
	Declare @vTotalDiscriminado money
	Declare @vTotalDescuentoDiscriminado money
	Declare @vTotalCosto money
	

	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	Declare @vObservacion varchar(50)

	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money
	Declare @vIDFormaPagoFactura tinyint = 1
	Declare @vBuscarEnCuentaBancaria bit = 'True'
	Declare @vIDCuentaBancaria int
	--Obtener valores
	Begin
	
		Select	@vIDSucursal=IDSucursal,
				@vTipoComprobante=TipoComprobante,
				@vIDTipoComprobante=IDTipoComprobante,
				@vComprobante=Numero,
				@vFechaEmision=Fecha,
				@vIDMoneda=IDMoneda,
				@vCotizacion=Cotizacion,
				@vObservacion=Observacion,
				@vIDCuentaBancaria=IDCuentaBancaria
		From VDebitoCreditoBancario Where IDTransaccion=@IDTransaccion
	
	End
					
	--Verificar que el asiento se pueda modificar
	Begin
	
		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		
		print 'Los datos son correctos!'
		
	End
			
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFechaEmision, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vObservacion, 0, 0, 0, 0, 'False', NULL, 'False')
		print @vobservacion
	End
	
	--Cuentas Fijas Banco
	Begin
	
		--Variables
		Declare @vVentaCredito bit
		Declare @vVentaContado bit
		
		Declare cCFDebitoCreditoBancario cursor for
			Select Codigo, Debe, Haber 
			From VCFDebitoCreditoBancario 
			Where IDMoneda=@vIDMoneda and Denominacion like'%Banco%'

		Open cCFDebitoCreditoBancario   
		fetch next from cCFDebitoCreditoBancario into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			print 'Entro a Banco'
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			if @vBuscarEnCuentaBancaria = 'True' begin
				Set @vCodigo = (select CodigoCuentaContable from CuentaBancaria where id = @vIDCuentaBancaria)
				print 'cont'
			end
			set @vCodigo = '11010410'
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			Set @vImporte = (Select Sum (Total * Cotizacion) from VDebitoCreditoBancario
							Where IDTransaccion=@IDTransaccion)
			print concat('Importe = ',@vImporte)
				--If @vDebe = 1 Begin
				--	Set @vImporteDebe = @vImporte
				--	Set @vImporteHaber = 0
				--End
			
				--If @vHaber = 1 Begin
					Set @vImporteHaber = @vImporte
					Set @vImporteDebe = 0
				--End

			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin				
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
						Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
													Debito=Debito+Round(@vImporteDebe,@vRedondeo),
													Importe=Importe+Round(@vImporte,@vRedondeo)
						Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
					End Else Begin
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
											Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
					End
			End
			
			fetch next from cCFDebitoCreditoBancario into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFDebitoCreditoBancario 
		deallocate cCFDebitoCreditoBancario
	End
	
	--Cuentas Fijas Proveedor
	Begin
		
		--Variables
		
		Declare cCFDebitoCredito cursor for
			select Codigo,Debe,Haber
			From VCFDebitoCreditoBancario
			where IDMoneda=@vIDMoneda-- and Denominacion like'%Recau%'

		Open cCFDebitoCredito
		fetch next from cCFDebitoCredito into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			print 'Entro a Proveedor'
			set @vCodigo = '11010220'
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			set @vImporte = @vImporte

			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
				set @vImporteHaber = 0
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
				set @vImporteDebe = 0
			End				
			
			print concat('Debe = ',@vImporteDebe)
			print concat('Haber = ',@vImporteHaber)
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFDebitoCredito into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFDebitoCredito
		deallocate cCFDebitoCredito
	End
					
		
	--Actualizamos la cabecera, el total
	Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
	Set @vImporteHaber = ROUND(@vImporteHaber, 0)
	Set @vImporteDebe = ROUND(@vImporteDebe, 0)
	
	Update Asiento Set Total = @vImporteHaber,
						Credito = @vImporteHaber,
						Debito = @vImporteDebe,
						Saldo = @vImporteHaber - @vImporteDebe
	Where IDTransaccion=@IDTransaccion
	
	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
		
End


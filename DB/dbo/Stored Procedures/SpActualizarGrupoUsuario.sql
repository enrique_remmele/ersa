﻿CREATE Procedure [dbo].[SpActualizarGrupoUsuario]

	--Entrada
	@IDGrupo int,
	@IDUsuario int,
	@Activar bit
	
As

Begin

	Declare @vMensaje varchar(100)
	Declare @vProcesado bit
	
	Set @vMensaje = 'No se proceso'
	Set @vProcesado = 'False'
	
	If @Activar = 'True' Begin
		
		If Exists(Select * From GrupoUsuario Where IDGrupo=@IDGrupo And IDUsuario=@IDUsuario) Begin
			Set @vMensaje = 'El usuario ya esta registrado!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		Insert Into GrupoUsuario(IDGrupo, IDUsuario)
		Values(@IDGrupo, @IDUsuario)
		
		Set @vMensaje = 'Registro procesado!'
		Set @vProcesado = 'True'
		GoTo Salir
			
	End
	
	If @Activar = 'False' Begin
		
		If Not Exists(Select * From GrupoUsuario Where IDGrupo=@IDGrupo And IDUsuario=@IDUsuario) Begin
			Set @vMensaje = 'El usuario no esta registrado!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		Delete From GrupoUsuario
		Where IDGrupo=@IDGrupo And IDUsuario=@IDUsuario
		
		Set @vMensaje = 'Registro procesado!'
		Set @vProcesado = 'True'
		GoTo Salir
			
	End
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


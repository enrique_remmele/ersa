﻿CREATE procedure [dbo].[SpImportVisit]

@IDSucursal int,
@Desde datetime,
@Hasta datetime

As

Begin

	Select 
	'cdVisitInstance'= 'V'+Convert(varchar(50),Numero),
	'cdRegion'='R'+Sucursal+'_'+'VENDEDOR_'+Convert(varchar(50),C.IDVendedor),
	'cdStore'=[Ref.Cliente],
	'cdUser'=Convert(Varchar(10),C.IDVendedor),
	'cdStart'=CONVERT(varchar(5),Year(Fecha)) + '-' + CONVERT(varchar(5), dbo.FFormatoDosDigitos( Month(Fecha))) + '-' + CONVERT(varchar(5), dbo.FFormatoDosDigitos( Day(Fecha))),
	vp.Fecha,
	'cdStatus'= (Case When Facturado='True' Then 'FINAL'Else 'CANC'End)
	From VPedido VP  
	Join Cliente C On VP.IDCliente = C.ID
	Where VP.IDSucursal=@IDSucursal And (VP.Fecha between @Desde And  @Hasta) And Facturado = 'True' And [Ref.Cliente]<>'0245' And [Ref.Cliente]<>'1007'
		
End



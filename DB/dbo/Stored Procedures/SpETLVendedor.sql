﻿CREATE Procedure [dbo].[SpETLVendedor]

	@IDSucursal tinyint

As

Begin

	Declare @vCodigoDistribuidor int
	Set @vCodigoDistribuidor = (Select CodigoDistribuidor From Sucursal Where ID=@IDSucursal)
	
	Select
	'CodDistribuidor'=dbo.FFormatoTamañoCaracter (15, @vCodigoDistribuidor),
	'CodZona'=dbo.FFormatoTamañoCaracter(15, Referencia),
	'NombreZona'=dbo.FFormatoTamañoCaracter (50, Nombres)
	
	From VVendedor
	Where IDSucursal = @IDSucursal
	And Estado='True'

End


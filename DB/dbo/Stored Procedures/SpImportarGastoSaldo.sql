﻿CREATE Procedure [dbo].[SpImportarGastoSaldo]

	--Cabecera
	@TipoComprobante varchar(10),
	@ReferenciaSucursal varchar(50) = 1,
	@ReferenciaTerminal varchar(50) = 1,
	@Comprobante varchar(50),
	@NroTimbrado varchar(50)=NULL,

	@Referencia varchar(50),
	@Proveedor varchar(50),
	@RUC varchar(50),
	
	--Totales
	@Total money,
	@Saldo money,
	
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
		
	@Actualizar bit
	
		
As

Begin
		
	--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		
		Declare @vOperacion varchar(50)
		Declare @vIDTransaccion int
		Declare @vComprobante varchar(50)

		--Codigos
		Declare @vIDTipoComprobante int
		Declare @vIDProveedor int
					
	End
	
	--Establecer valores predefinidos
	Begin
		
		Set @vOperacion = 'INS'
					
	End
	
	
	--Hayar Valores
	Begin
		
		Set @vMensaje = 'No se procesó!'
		Set @vProcesado = 'False'
		
		--Tipo de Comprobante
		Set @vIDTipoComprobante = (IsNull((Select Top(1) ID From TipoComprobante Where Codigo=@TipoComprobante And IDOperacion=@IDOperacion), 0))
		If @vIDTipoComprobante = 0 Begin
			Set @vMensaje = 'Tipo de comprobante incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End

		--Proveedor
		Set @vIDProveedor=(IsNull((Select Top(1) ID From Proveedor Where RUC=@RUC),0))
		If @vIDProveedor= 0 Begin
			Set @vMensaje = 'Proveedor incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End

		Set @vComprobante = '0' + dbo.FFormatoDosDigitos(@ReferenciaSucursal) + '-' + '0' + dbo.FFormatoDosDigitos(@ReferenciaTerminal) + '-' + @Comprobante 		
	
	End
	
	--Verificar si ya existe
	If Exists(Select * From GastoImportado GI Join Gasto G On GI.IDTransaccion=G.IDTransaccion Join TipoComprobante TC On G.IDTipoComprobante=TC.ID Where G.NroComprobante=@vComprobante And G.IDProveedor=@vIDProveedor And G.NroTimbrado=@NroTimbrado And TC.Codigo=@TipoComprobante) Begin
		set @vIDTransaccion = (Select Top(1) GI.IDTransaccion From GastoImportado GI Join Gasto G On GI.IDTransaccion=G.IDTransaccion Join TipoComprobante TC On G.IDTipoComprobante=TC.ID Where G.NroComprobante=@vComprobante And G.IDProveedor=@vIDProveedor And G.NroTimbrado=@NroTimbrado And TC.Codigo=@TipoComprobante)
		Set @vOperacion = 'UPD'
	End Else Begin
		Set @vMensaje = 'No se encontro el comprobante!'
		Set @vProcesado = 'False'
		GoTo Salir		
	End
	
	--ACTUALIZAR
	If @vOperacion = 'UPD' Begin
	
		If @Actualizar = 'True' Begin
			
			Update GastoImportado Set Importe=@Total,
										Pagado=@Total-@Saldo,
										Saldo=@Saldo
			Where IDTransaccion=@vIDTransaccion

			Set @vMensaje = 'Actualizado!'
			Set @vProcesado = 'True'
			GoTo Salir
		End
		
		Set @vMensaje = 'Sin Act.!'
		Set @vProcesado = 'True'
		GoTo Salir
		
	End

Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


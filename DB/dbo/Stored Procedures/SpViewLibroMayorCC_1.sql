﻿CREATE Procedure [dbo].[SpViewLibroMayorCC]

	--Entrada
	@CuentaInicial varchar(100),
	@CuentaFinal varchar(100),
	@Fecha1 date,
	@Fecha2 date,
	
	--Tipo
	@Pantalla bit = 'False'

As

Begin
	--SV 20140821 Agregando campo resumen de la tabla comprobante para mostrar en Libro Mayor
	
	--Variables
	Begin
		declare @vID int
		declare @vIDTransaccion int
		declare @vCod varchar(10)
		declare @vCodigo varchar(100)
		declare @vDescripcion varchar (300)
		declare @vFecha date
		declare @vAsiento int
		declare @vComprobante varchar(50)
		declare @vSuc varchar(100)
		declare @vConcepto varchar(100)
		declare @vDebito money
		declare @vCredito money
		declare @vIDSucursal int
		declare @vIDTipoComprobante int
		declare @vPrimero bit
		Declare @vCuentaTipo varchar(100)
		Declare @vPrefijo varchar(100)
		Declare @vCodigoActual varchar(100)
		Declare @vIDUNActual int
		Declare @IIDCCActual int
		Declare @FechaActual date
		Declare @vUsuario varchar(32)
		Declare @vFechaTransaccion Date	
		Declare @vCotizacion money
		declare @IDUnidadNegocio int
		Declare @IDCentroCosto int
		Declare @Operacion as varchar(50)
		
		--Variables para calcular saldo
		declare @vSaldo money
		declare @vTotalDebito money
		declare @vTotalCredito money
		declare @vSaldoAnterior money
		declare @vSaldoFinal money
		declare @vSaldoInicial MONEY

		declare @IDMoneda int
		Declare @Moneda varchar(16)
		Declare @vAño varchar(4)
		
	End
	--Obtener año de la fecha 2
	set @vAño = substring(CONVERT(VARCHAR,@Fecha2, 112),1,4)
	print @vAño

	
	If @Pantalla = 'False' Begin
	
		--Crear la tabla temporal
		create table #TablaTemporal(ID int,
									IDTransaccion int,
									Codigo varchar(100),
									Descripcion varchar(300),
									Fecha date,
									Asiento int,
									Comprobante varchar(100),
									Suc varchar(100),
									Concepto varchar(100),
									Debito money,
									Credito money,
									Saldo money,
									SaldoInicial money,
									SaldoAnterior money,
									IDSucursal int,
									IDTipoComprobante int,
									Usuario varchar(32),
									FechaTransaccion Date,
									Cotizacion money,
									IDUnidadNegocio int,
									IDCentroCosto int,
									IDMoneda int,
									Moneda varchar(16),
									Operacion varchar(50))
																	
		set @vID = (Select IsNull(MAX(ID)+1,1) From #TablaTemporal)
		Set @vPrimero = 'True'
		Set @vCodigoActual = '0'
		
		--Insertar datos	
		Begin
				
			Declare db_cursor cursor for
			
			Select 
			IDTransaccion,
			Codigo,
			'Cod'=substring(Codigo,1,1),
			Descripcion,
			Fecha,
			Asiento,
			Comprobante,
			CodSucursal,
			Concepto,
			Debito,
			Credito,
			IDSucursal,
			IDTipoComprobante,
			Usuario,
			FechaTransaccion,
			Cotizacion,
			IDCentroCosto,
			IDUnidadNegocio,
			IDMoneda,
			Moneda,
			concat(Operacion,'-',Suc) as Operacion
			From VLibroMayor
			Where Codigo Between @CuentaInicial and  @CuentaFinal and Fecha between @Fecha1 and @Fecha2
			Order By IDUnidadNegocio, IDCentroCosto,Codigo, Operacion
			Open db_cursor   
			Fetch Next From db_cursor Into	@vIDTransaccion,@vCodigo,@vCod,@vDescripcion,@vFecha,@vAsiento,
			@vComprobante,@vSuc,@vConcepto,@vDebito,@vCredito,@vIDSucursal,@vIDTipoComprobante, @vUsuario, 
			@vFechaTransaccion, @vCotizacion, @IDCentroCosto, @IDUnidadNegocio, @IDMoneda, @Moneda, @Operacion
			While @@FETCH_STATUS = 0 Begin 
			
				--Reiniciar	el Saldo Inciial si se cambia el codigo de la cuenta
				--Esto es necesario cuando se listan varias cuentas en el informe.
				If @vCodigoActual != @vCodigo Begin
					Set @vPrimero = 'True'
				End
				If @vIDUNActual != @IDUnidadNegocio Begin
					Set @vPrimero = 'True'
				End
				If @IIDCCActual != @IDCentroCosto Begin
					Set @vPrimero = 'True'
				End


				Set @vPrefijo = SubString(@vCodigo, 0, 2)
					
				If @vPrimero = 'True' Begin	
					
					--Hallar Totales para Saldo Anterior -  Se agregar salgo porque debe traer el anterior solo del mismo año
					--Set @vTotalDebito  = IsNull((Select Sum(Debito) From VLibroMayor Where Codigo = @vCodigo And Fecha < @Fecha1 ),0)
					--Set @vTotalCredito = IsNull((Select Sum(Credito) From VLibroMayor Where Codigo = @vCodigo And Fecha < @Fecha1),0)
					Set @vTotalDebito  = IsNull((Select Sum(Debito) From VLibroMayor Where Codigo = @vCodigo And Fecha < @Fecha1 and YEAR(fecha) = @vAño and IDUnidadNegocio = @IDUnidadNegocio and IDCentroCosto = @IDCentroCosto and IDSucursal = @vIDSucursal),0)
					Set @vTotalCredito = IsNull((Select Sum(Credito) From VLibroMayor Where Codigo = @vCodigo And Fecha < @Fecha1 and YEAR(fecha) = @vAño and IDUnidadNegocio = @IDUnidadNegocio and IDCentroCosto = @IDCentroCosto and IDSucursal = @vIDSucursal),0)
							

					--Hallar Saldo Anterior
					If @vPrefijo = '1' Or @vPrefijo = '5' Begin
						Set @vSaldoAnterior = @vTotalDebito - @vTotalCredito
					End
					
					If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
						Set @vSaldoAnterior = @vTotalCredito - @vTotalDebito
					End
					
					Set @vSaldoInicial = @vSaldoAnterior
					
					Set @vPrimero = 'False'
					
				End
			
				
				--Cuentas del Debe
				If @vPrefijo = '1' Or @vPrefijo = '5' Begin
					set @vSaldo = (@vSaldoAnterior + @vDebito) - @vCredito
				End
				
				--Cuentas del Haber
				If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
					set @vSaldo = (@vSaldoAnterior + @vCredito) - @vDebito
				End

				If  Exists(Select * From #TablaTemporal Where Codigo = @vCodigo and IDUnidadNegocio = @IDUnidadNegocio and IDCentroCosto = @IDCentroCosto and Operacion = @Operacion and IDSucursal = @vIDSucursal) Begin
				  Update #TablaTemporal
				  set Debito = Debito + @vDebito,
				  Credito = Credito + @vCredito,
				  Saldo = @vSaldo,
				  SaldoInicial = @vSaldoInicial,
				  SaldoAnterior = @vSaldoanterior
				  where Codigo = @vCodigo 
				  and IDUnidadNegocio = @IDUnidadNegocio 
				  and IDCentroCosto = @IDCentroCosto
				  and Operacion = @Operacion
				  and IDSucursal = @vIDSucursal
				End

				If  not Exists(Select * From #TablaTemporal Where Codigo = @vCodigo and IDUnidadNegocio = @IDUnidadNegocio and IDCentroCosto = @IDCentroCosto and Operacion = @Operacion and IDSucursal = @vIDSucursal) Begin
					Insert Into  #TablaTemporal(ID,IDTransaccion,Codigo,Descripcion,Fecha,Asiento,Comprobante,Suc,Concepto,Debito,Credito,Saldo,SaldoInicial,SaldoAnterior,IDSucursal,IDTipoComprobante, Usuario, FechaTransaccion, cotizacion, IDCentroCosto, IDUnidadNegocio, IDMoneda, Moneda,Operacion) 
					Values (@vID,@vIDTransaccion,@vCodigo,@vDescripcion,@vFecha,@vAsiento,@vComprobante,@vSuc,@vConcepto,@vDebito,@vCredito,@vSaldo,@vSaldoInicial,@vSaldoAnterior,@vIDSucursal,@vIDTipoComprobante,@vUsuario,@vFechaTransaccion,@vCotizacion, @IDCentroCosto, @IDUnidadNegocio, @IDMoneda, @Moneda,@Operacion)
				End	
				
				--Actualizar Saldo
				set @vSaldoAnterior = @vSaldo 
				Set @vCodigoActual = @vCodigo
				set @vIDUNActual = @IDUnidadNegocio
				set @IIDCCActual = @IDCentroCosto
				
				Fetch Next From db_cursor Into	@vIDTransaccion,@vCodigo,@vCod,@vDescripcion,@vFecha,@vAsiento,@vComprobante,@vSuc,@vConcepto,@vDebito,@vCredito,@vIDSucursal,@vIDTipoComprobante, @vUsuario, @vFechaTransaccion, @vCotizacion, @IDCentroCosto, @IDUnidadNegocio, @IDMoneda, @Moneda, @Operacion
				
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
			
		End	
		--Select * From #TablaTemporal Where ID=@vID Order By Codigo
		Select T.* 
		, CASE TC.Resumen
		WHEN '' THEN T.Concepto
		--ELSE  CAST(T.Asiento AS VARCHAR(50)) + '  ' + TC.Resumen  + ' del ' + CAST(T.Fecha AS VARCHAR(50)) 
		ELSE  TC.Resumen  + ' del ' + CAST(T.Fecha AS VARCHAR(50)) 
		END AS Resumen,
		'UnidadNegocio'=UN.Descripcion,
		'CentroCosto'=CC.Descripcion
		From #TablaTemporal T
		LEFT JOIN dbo.TipoComprobante TC ON T.IDTipoComprobante = TC.ID
		Left Join VUnidadNegocio UN on UN.ID = IDUnidadNegocio
		Left Join VCentroCosto CC on CC.ID = IDCentroCosto
		Where T.ID=@vID Order By T.IDUnidadNegocio, T.IDCentroCosto, T.Codigo, T.Operacion
	
	END

End

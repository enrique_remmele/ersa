﻿CREATE Procedure [dbo].[SpViewVentasPendientesBoletaContraBoleta]

	--Entrada
		@IDCliente int,
		@FechaCotizacion date,
		@IDMoneda tinyint
	
As

Begin
	Select top(5)
	V.FechaVencimiento,
	V.IDCliente,
	V.Cliente,
	V.IDTransaccion,
	'Sel'='True',
	V.Comprobante,
	'Tipo'=V.TipoComprobante,
	V.Condicion,
	V.Credito,
	V.FechaEmision,
	V.Fec,
	'Vencimiento'=V.[Fec. Venc.],
	V.Total,
	V.Cobrado,
	V.Descontado,
	V.Acreditado ,
	V.Saldo,
	'SaldoGs'= 0,
	'Importe'=V.Saldo,
	'ImporteGs'=0,
	'Cancelar'='False',
	V.IDMoneda,
	V.Moneda,
	V.Cotizacion,
	V.IDSucursal,
	V.IDTipoComprobante,
	V.TotalImpuesto,
	'CotizacionHoy'=ISNull((select Cotizacion from Cotizacion where idmoneda = V.IdMOneda and Fecha = @FechaCotizacion),1),
	'IDTransaccionAnticipo' = 0

	From VVenta V
	Where V.Cancelado = 'False'
	And V.Anulado='False'
	and V.IDCliente = @IDCliente
	and V.IDMoneda = @IDMoneda
	and cast(FechaEmision as date) <=cast(@FechaCotizacion as date)
	Order by FechaVencimiento asc 
	
End




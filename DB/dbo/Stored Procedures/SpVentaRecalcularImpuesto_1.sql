﻿
CREATE Procedure [dbo].[SpVentaRecalcularImpuesto]

	--Entrada
	@IDTransaccion numeric(18,0)

As

Begin

	--Varible
	Declare @vIDImpuesto tinyint
	Declare @vTotal money
	Declare @vTotalImpuesto money
	Declare @vTotalDiscriminado money
	Declare @vTotalDescuento money

	Declare db_cursorDetalle cursor for
	Select IDImpuesto, 'Total'=Sum(Total), 'TotalImpuesto'=Sum(TotalImpuesto), 'TotalDiscriminado'=Sum(TotalDiscriminado)
	From DetalleVenta
	Where IDTransaccion=@IDTransaccion
	Group By IDImpuesto
	Open db_cursorDetalle
	Fetch Next From db_cursorDetalle Into @vIDImpuesto, @vTotal, @vTotalImpuesto, @vTotalDiscriminado
	While @@FETCH_STATUS = 0 Begin  

		Update DetalleImpuesto Set	Total=@vTotal,
									TotalImpuesto=@vTotalImpuesto,
									TotalDiscriminado=@vTotalDiscriminado
		Where IDTransaccion=@IDTransaccion And IDImpuesto=@vIDImpuesto
			
		Fetch Next From db_cursorDetalle Into @vIDImpuesto, @vTotal, @vTotalImpuesto, @vTotalDiscriminado

	End
		
	Close db_cursorDetalle
	Deallocate db_cursorDetalle

End


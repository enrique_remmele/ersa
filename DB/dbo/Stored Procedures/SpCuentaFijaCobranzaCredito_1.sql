﻿CREATE Procedure [dbo].[SpCuentaFijaCobranzaCredito]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Orden tinyint = NULL,
	@IDCuentaContable smallint = NULL,
	@IDTipoComprobante smallint = NULL,
	@Contado bit= NULL,
	@Credito bit = NULL,
	@IDMoneda tinyint = NULL,
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	If @Operacion='INS' Begin
		
		--Si es que la Nombres ya existe
		If Exists(Select * From CuentaFijaCobranzaCredito Where Descripcion=@Descripcion) Begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si la configuracion ya existe		
		If Exists(Select * From CuentaFijaCobranzaCredito Where IDTipoComprobante=@IDTipoComprobante And Credito=@Credito And Contado=@Contado And IDMoneda=@IDMoneda) Begin
			set @Mensaje = 'La configuracion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Obtenemos el nuevo ID
		declare @vID tinyint
		set @vID = (Select IsNull((Max(ID)+1), 1) From CuentaFijaCobranzaCredito)

		--Insertamos
		Insert Into CuentaFijaCobranzaCredito(ID, Descripcion, Orden, IDCuentaContable, IDTipoComprobante, Credito, Contado, IDMoneda)
		Values(@vID, @Descripcion, @Orden, @IDCuentaContable, @IDTipoComprobante, @Credito, @Contado, @IDMoneda)		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si es que no existe
		If Not Exists(Select * From CuentaFijaCobranzaCredito Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si es que la Nombres ya existe
		If Exists(Select * From CuentaFijaCobranzaCredito Where Descripcion=@Descripcion And ID!=@ID) Begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si la configuracion ya existe		
		If Exists(Select * From CuentaFijaCobranzaCredito Where IDTipoComprobante=@IDTipoComprobante And Credito=@Credito And Contado=@Contado And IDMoneda=@IDMoneda And ID!=@ID) Begin
			set @Mensaje = 'La configuracion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		Update CuentaFijaCobranzaCredito Set Descripcion=@Descripcion,
											 Orden=@Orden,
											 IDTipoComprobante=@IDTipoComprobante,
											 Credito=@Credito,
											 Contado=@Contado,
											 IDMoneda=@IDMoneda,
											 IDCuentaContable=@IDCuentaContable
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si es que no existe
		If Not Exists(Select * From CuentaFijaCobranzaCredito Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Eliminamos
		Delete From CuentaFijaCobranzaCredito Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End




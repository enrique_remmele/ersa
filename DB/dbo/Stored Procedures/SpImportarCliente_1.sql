﻿CREATE Procedure [dbo].[SpImportarCliente]

	--Entrada
	--Identificadores
	@Referencia varchar(50) = NULL,
	@RazonSocial varchar(150),
	@RUC varchar(50),
	@CI varchar(50) = '',
	@NombreFantasia varchar(150) = NULL,
	@Direccion1 varchar(200) = NULL,
	@Direccion2 varchar(200) = NULL,
	@Telefono varchar(200) = NULL,
	
	--Agrupadores
	@Estado varchar(20) = NULL,
	@Pais varchar(20) = NULL,
	@Departamento varchar(20) = NULL,
	@Ciudad varchar(20) = NULL,
	@Barrio varchar(20) = NULL,
	@ZonaVenta varchar(20) = NULL,
	@Area varchar(20) = NULL,
	@Ruta varchar(20) = NULL,

	@ListaPrecio varchar(20) = NULL,
	@TipoCliente varchar(100) = NULL,
	@CodigoCuentaContable varchar(100) = NULL,
		
	--Asignaciones
	@Sucursal varchar(100) = NULL,
	@Vendedor varchar(100) = NULL,
	@Cobrador varchar(100) = NULL,
	
	--Configuracion
	@CondicionVenta tinyint = 1,
		
	--Credito
	@LimiteCredito money = 0,
	@Deuda money = 0,
	@PlazoCredito tinyint = 0,
	@PlazoCobro tinyint = 0,
	@PlazoChequeDiferido tinyint = 0,
	
	--Otros
	@PaginaWeb varchar(100) = NULL,
	@Fax varchar(100) = NULL,
	@Email varchar(100) = NULL,
	@FechaAlta date = NULL,

	--Contactos
	@Contacto varchar(50) = '',
	@TelefonoContacto varchar(50) = NULL,
	
	--Opciones
	@Actualizar Bit = 'False'
	
As

Begin

	--ID's
	
	Declare @vIDEstado int
	Declare @vIDPais int
	Declare @vIDDepartamento int
	Declare @vIDCiudad int
	Declare @vIDBarrio int
	Declare @vIDZonaVenta int
	Declare @vIDListaPrecio int
	Declare @vIDTipoCliente int
	Declare @vIDSucursal int
	Declare @vIDVendedor int
	Declare @vIDCobrador int
	Declare @vDireccion varchar(200)
	Declare @vCI bit
	Declare @vContado bit
	Declare @vCredito bit
	
	Declare @vID int
	Declare @vOperacion varchar(10) 
	Declare @Mensaje varchar(200)
	Declare @Procesado bit
		
	Begin
	
		--Estado
		If @Estado = '1' Begin Set @Estado='ACTIVA' End
		If @Estado = '2' Begin Set @Estado='INACTIVA' End
		If @Estado = '3' Begin Set @Estado='CONSULTAR' End
		If @Estado = '4' Begin Set @Estado='BLOQUEADO' End
		If @Estado = '5' Begin Set @Estado='CERRADO' End
		If @Estado = '6' Begin Set @Estado='GEST.JUDIC' End
		If @Estado = '7' Begin Set @Estado='INACCESIBLE' End
		
		Exec @vIDEstado = SpEstadoClienteInsUpd @Estado
		
		--Pais
		Set @vIDPais = 1
		
		--Departamento
		Exec @vIDDepartamento = SpDepartamentoInsUpd @Descripcion=@Departamento, @IDPais=@vIDPais
		
		--Ciudad
		Exec @vIDCiudad = SpCiudadInsUpd @Descripcion=@Ciudad, @IDPais=@vIDPais, @IDDepartamento=@vIDDepartamento
		
		--Barrio
		Exec @vIDBarrio = SpBarrioInsUpd @Barrio
		
		--Zona de Venta
		Exec @vIDZonaVenta = SpZonaVentaInsUpd @ZonaVenta
		
		--Tipo de Cliente
		Exec @vIDTipoCliente = SpTipoClienteInsUpd @TipoCliente
		
		--Sucursal
		Set @vIDSucursal = (Select Top(1) ID From Sucursal Where Codigo=@Sucursal)
		
		--Lista de Precio
		Set @vIDListaPrecio = (Select Top(1) ID From ListaPrecio Where Referencia=@ListaPrecio And IDSucursal=@vIDSucursal)
		
		--Vendedor
		Exec @vIDVendedor = SpVendedorInsUpd @Descripcion=@Vendedor, @IDSucursal=@vIDSucursal 
		
		Set @vDireccion = @Direccion1
		
		If @Direccion2 != '' Begin
			Set @vDireccion = @Direccion1 + ' ' + @Direccion2
		End
		
		--Condicion Venta
		If @CondicionVenta = 1 Begin
			Set @vCredito = 'False'
			Set @vContado = 'True'
		End
		
		If @CondicionVenta = 2 Begin
			Set @vCredito = 'True'
			Set @vContado = 'False'
		End
	
		--Cedula
		Set @vCI = 'False'
		
		--Si el RC no contiene '-' entonces es CI
		If Charindex('-', @RUC) = 0 Or @RUC='-' Or @RUC='' begin
			Set @vCI = 'True'
			Set @RUC = @CI
		End
		
	End

	Set	@vID = 0
	Set @vOperacion = 'INS'
	
	--Verificamos si ya existe
	If Exists(Select * From Cliente Where Referencia=@Referencia) Begin
		Set @vID = (Select Top(1) ID From Cliente Where Referencia=@Referencia) 		
		Set @vOperacion = 'UPD'
	End		
	
	--Si es para actualizar, verificar la configuracion
	If @vOperacion = 'UPD' And @Actualizar='False' Begin
		Set @Mensaje = 'Sin actualizar.'
		Set @Procesado = 'True'
		GoTo Salir
	End
		
	--Ejecutamos
	EXEC [dbo].[SpCliente] 
		@ID = @vID, 
		@RazonSocial = @RazonSocial,
		@RUC = @RUC,
		@CI = @vCI,
		@Referencia = @Referencia,
		@Operacion = @vOperacion,
		@NombreFantasia = @NombreFantasia,
		@Direccion = @vDireccion,
		@Telefono = @Telefono,
		@IDEstado = @vIDEstado,
		@IDPais = @vIDPais,
		@IDDepartamento = @vIDDepartamento,
		@IDCiudad = @vIDCiudad,
		@IDBarrio = @vIDBarrio,
		@IDZonaVenta = @vIDZonaVenta,
		@IDListaPrecio = @vIDListaPrecio,
		@IDTipoCliente = @vIDTipoCliente,
		@Contado = @vContado,
		@Credito = @vCredito,
		@IDMoneda = 1,
		@IVAExento = 'False',
		@CodigoCuentaContable = @CodigoCuentaContable,
		@IDSucursal = @vIDSucursal,
		@IDVendedor = @vIDVendedor,
		@IDCobrador = @vIDCobrador,
		@LimiteCredito = @LimiteCredito,
		@PlazoCredito = @PlazoCredito,
		@PlazoCobro = @PlazoCobro,
		@PlazoChequeDiferido = @PlazoChequeDiferido,
		@PaginaWeb = @PaginaWeb,
		@Email = @Email,
		@Fax = @Fax,
		@IDUsuario = 1,
		@IDTerminal = 1,
		@Mensaje = @Mensaje OUTPUT,
		@Procesado = @Procesado OUTPUT

	
	--Verificar si se proceso
	If @Procesado = 0 Begin
		GoTo Salir
	End


	--Actualizamos la fecha de alta
	Update Cliente Set FechaAlta=@FechaAlta
	Where ID=@vID

	--Si tiene algun contacto, agregar
	If @Contacto != '' Begin
		
		--Verificar si ya existe
		If Not Exists(Select * From ClienteContacto Where IDCliente=@vID And Nombres=@Contacto) Begin
			Declare @vIDContacto tinyint
			Set @vIDContacto = IsNull((Select Max(ID) + 1 From ClienteContacto Where IDCliente=@vID), 1)
			
			--Volvemos a cargar el ID del Cliente
			Set @vID = (Select top(1) ID From Cliente Where Referencia=@Referencia And IDSucursal=@vIDSucursal)

			Insert Into ClienteContacto(IDCliente, ID, Nombres, Cargo, Telefono, Email)
			Values(@vID, @vIDContacto, @Contacto, '', @TelefonoContacto, '')
		End

	End

Salir:
	Select 'Mensaje'=@Mensaje + ' ' + @vOperacion, 'Procesado'=@Procesado, 'Cantidad'=@@ROWCOUNT
End

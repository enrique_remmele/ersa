﻿CREATE Procedure [dbo].[SpViewExistenciaValorizada]

	--Entrada
	@IDTipoProducto int = 0,
	@IDLinea int = 0,
	@IDSubLinea int = 0,
	@IDSucursal int = 0,
	@IDDeposito int = 0,
	@IDProducto int = 0,
	@Fecha date,
	@Resumido bit = 'False'
	
As

Begin

	--Variables
	Begin
		declare @vIDSucursal int
		declare @vIDDeposito int
		declare @vDeposito varchar(100)
		declare @vIDLinea int
		declare @vLinea varchar(100)
		declare @vIDTipoProducto int
		declare @vTipoProducto varchar(100)
		declare @vIDMarca int
		declare @vMarca varchar(100)
		declare @vIDProducto int
		declare @vReferencia varchar(100)
		declare @vProducto varchar(500)
		declare @vCostoPromedio money
		declare @vExistencia decimal(18,2)
		declare @vTotalValorizadoCostoPromedio money
		declare @vPesoUnitario money
		
	End
	
	
	--Crear la tabla temporal
	create table #TablaTemporal(IDSucursal int,
								IDDeposito int,
								Deposito varchar(100),
								IDLinea int,
								Linea varchar(100),
								IDTipoProducto int,
								TipoProducto varchar(100),
								IDMarca int,
								Marca varchar(100),
								IDProducto int,
								Referencia varchar(100),
								Producto varchar(500),
								CostoPromedio money, 
								Existencia decimal(18,2), 
								TotalValorizadoCostoPromedio money,  
								PesoUnitario money)
																	
		Begin
				
			Declare db_cursor cursor for
			
			Select  
			E.IDSucursal,
			E.IDDeposito,
			E.Deposito,
			E.IDLinea,
			E.Linea,
			E.IDTipoProducto,
			E.TipoProducto,
			E.IDMarca,
			E.Marca,
			E.IDProducto,
			E.Referencia,
			E.Producto,
			ISNULL(AVG(E.PesoUnitario),1) PesoUnitario  
			From VExistenciaDeposito E  

			Where E.Estado = 1
			and ((Case when @IDTipoProducto = 0 then 0  else E.IDTipoProducto end) = (Case when @IDTipoProducto = 0 then 0  else @IDTipoProducto end))
			and ((Case when @IDLinea = 0 then 0  else E.IDLinea end) = (Case when @IDLinea = 0 then 0  else @IDLinea end))
			and ((Case when @IDSubLinea = 0 then 0  else E.IDSubLinea end) = (Case when @IDSubLinea = 0 then 0  else @IDSubLinea end))
			and ((Case when @IDSucursal = 0 then 0  else E.IDSucursal end) = (Case when @IDSucursal = 0 then 0  else @IDSucursal end))
			and ((Case when @IDDeposito = 0 then 0  else E.IDDeposito end) = (Case when @IDDeposito = 0 then 0  else @IDDeposito end))
			and ((Case when @IDProducto = 0 then 0  else E.IDProducto end) = (Case when @IDProducto = 0 then 0  else @IDProducto end))

			GROUP BY E.IDSucursal,
			E.IDDeposito,
			E.Deposito,
			E.IDLinea,
			E.Linea,
			E.IDTipoProducto,
			E.TipoProducto, 
			E.IDMarca,
			E.Marca,
			E.IDProducto,
			E.Referencia,
			E.Producto 
			Order by E.TipoProducto, 
			E.Linea, 
			E.Producto 
			ASC
			Open db_cursor   
			Fetch Next From db_cursor Into	@vIDSucursal,@vIDDeposito,@vDeposito,@vIDLinea,@vLinea,@vIDTipoProducto,@vTipoProducto,@vIDMarca,@vMarca,@vIDProducto,@vReferencia,@vProducto,@vPesoUnitario 
			While @@FETCH_STATUS = 0 Begin 
			
				Set @vCostoPromedio = (dbo.FCostoProductoFechaKardex(@vIDProducto, @Fecha))
				Set @vExistencia = (dbo.FExtractoMovimientoProductoDeposito(@vIDProducto, @vIDDeposito, DateAdd(day,1,@Fecha), '', 'False'))
				Set @vTotalValorizadoCostoPromedio = @vCostoPromedio * @vExistencia
								
				Insert Into  #TablaTemporal(IDSucursal, IDDeposito, Deposito, IDLinea, Linea, IDTipoProducto, TipoProducto, IDMarca, Marca, IDProducto, Referencia, Producto, CostoPromedio, Existencia, TotalValorizadoCostoPromedio,  PesoUnitario) 
									Values (@vIDSucursal,@vIDDeposito,@vDeposito,@vIDLinea,@vLinea,@vIDTipoProducto,@vTipoProducto,@vIDMarca,@vMarca,@vIDProducto,@vReferencia,@vProducto,@vCostoPromedio, @vExistencia, @vTotalValorizadoCostoPromedio, @vPesoUnitario)
								
							
				
			Fetch Next From db_cursor Into	@vIDSucursal,@vIDDeposito,@vDeposito,@vIDLinea,@vLinea,@vIDTipoProducto,@vTipoProducto,@vIDMarca,@vMarca,@vIDProducto,@vReferencia,@vProducto,@vPesoUnitario 
				
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
			
		End	
		
		if @Resumido = 'True' begin
			SELECT  TipoProducto, SUM(Existencia) as Existencia, SUM(TotalValorizadoCostoPromedio) AS TotalValorizadoCostoPromedio From #TablaTemporal GROUP BY TipoProducto 
		end
		else begin

			Select * From #TablaTemporal
		end
	

End



﻿CREATE Procedure [dbo].[SpViewExtractoMovimientoProveedor]

	--Entrada
	@Fecha1 Date,
	@Fecha2 Date,
	@IDProveedor int,	
	@Comprobante varchar (30) = '% %',
	@IDMoneda int = 1
As

Begin
	--Crear la tabla temporal
    create table #TablaTemporal(ID tinyint,
								ID2 int,
								IDTransaccion int,
								Codigo int,
								RazonSocial varchar(50),
								Referencia varchar(50),
								Fecha date,
								Operacion varchar(50),
								Documento varchar(50),
								[Detalle/Concepto] varchar(50),
								Saldo money,
								SaldoAnterior money,
								SaldoInicial money,
								Debitos money,
								Creditos money,
								IDMoneda int,
								DescripcionMoneda varchar(50),
								Movimiento char(5),
								RRHH bit
								primary key(ID,ID2))
								
	--Variables para calcular saldo
	declare @vSaldo money
	declare @vTotalDebito money
	declare @vTotalCredito money
	declare @vSaldoAnterior money
	declare @vSaldoInicial money

	--Declarar variables
	declare @vID tinyint
	declare @vID2 int
	declare @vIDTransaccion Numeric (18,0)
	declare @vIDProveedor int
	declare @vRazonSocial varchar (100)
	declare @vReferencia varchar(50)
	declare @vFecha date
	declare @vOperacion varchar(50)
	declare @vDocumento varchar(50)
	declare @vDetalleConcepto varchar(50)
	declare @vDebitos money
	declare @vCreditos money
	declare @vIDMoneda int
	declare @vMoneda varchar(50)
	declare @vMovimiento varchar(5)
	declare @vRRHH bit

	set @vID = 1
	set @vID2 = 1

	--Insertar datos	
	Begin
	
		Declare db_cursor cursor for
		
		Select
		IDTransaccion,
		Codigo,
		RazonSocial,
		Referencia,
		Fecha,
		Operación,
		Documento,
		[Detalle/Concepto],
		Debito,
		Credito,
		IDMoneda,
		DescripcionMoneda,
		Movimiento,
		RRHH
		From VExtractoMovimientoProveedor
		Where Codigo=@IDProveedor And IDMoneda=@IDMoneda and Fecha between @Fecha1 and @Fecha2 And Documento Like @Comprobante
		Order By Fecha
		--Hallar Totales para Saldo Anterior
		Set @vTotalDebito  = IsNull((Select Sum(Debito) From VExtractoMovimientoProveedor Where Codigo=@IDProveedor And Fecha < @Fecha1 And IDMoneda=@IDMoneda),0)
		Set @vTotalCredito = IsNull((Select Sum(Credito) From VExtractoMovimientoProveedor Where Codigo=@IDProveedor And Fecha < @Fecha1 And IDMoneda=@IDMoneda),0)
					
		--Hallar Saldo Anterior
		--Set @vSaldoAnterior =  @vTotalDebito - @vTotalCredito 
		Set @vSaldoAnterior =  @vTotalCredito - @vTotalDebito
		Set @vSaldoInicial = @vSaldoAnterior 
				
		Open db_cursor  
		 
		Fetch Next From db_cursor Into   @vIDTransaccion,@vIDProveedor,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vDebitos,@vCreditos,@vIDMoneda,@vMoneda,@vMovimiento,@vRRHH
		
		While @@FETCH_STATUS = 0 Begin 
					
			--Hallar Saldo			
			--set @vSaldo = (@vSaldoAnterior + @vDebitos) - @vCreditos
			set @vSaldo = (@vSaldoAnterior + @vCreditos) - @vDebitos
									
			--Insertar Fila en la Tabla Temporal
			Insert Into  #TablaTemporal(ID,ID2,IDTransaccion,Codigo,RazonSocial,Referencia,Fecha,Operacion,Documento,[Detalle/Concepto],Saldo,SaldoAnterior,SaldoInicial,Debitos,Creditos,IDMoneda,DescripcionMoneda,Movimiento, RRHH) 
								Values (@vID,@vID2,@vIDTransaccion,@vIDProveedor,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vSaldo,@vSaldoAnterior,@vSaldoInicial,@vDebitos,@vCreditos,@vIDMoneda,@vMoneda,@vMovimiento, @vRRHH)
				
			--Actualizar Saldo
			set @vSaldoAnterior = @vSaldo 
			
			--Sumar 1 al ID2 de la Tabla Temporal
			Set @vID2 = @vID2 + 1
			
			Fetch Next From db_cursor Into   @vIDTransaccion,@vIDProveedor,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vDebitos,@vCreditos,@vIDMoneda,@vMoneda,@vMovimiento, @vRRHH
			
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		
	End	
	
	Select *, Fec=CONVERT(varchar(50), Fecha, 5) From #TablaTemporal Where ID=@vID
	
End
	




﻿CREATE Procedure [dbo].[SpAprobarPedido]
	--Entrada
	@IDTransaccionPedido as numeric(18,0),
	@Valor as bit,
	@IDUsuario int
As
Begin
  if @IDTransaccionPedido <> 0 begin	
		----Actualizar
		Update Pedido 
		Set Aprobado= @valor,
		IDUsuarioAprobacion = @IDUsuario,
		FechaAprobacion = getdate()
		Where IDTransaccion = @IDTransaccionPedido
			
  end
	
End



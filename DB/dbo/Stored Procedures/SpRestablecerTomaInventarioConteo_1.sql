﻿CREATE Procedure [dbo].[SpRestablecerTomaInventarioConteo]

	@IDTransaccion numeric (18,0),
	@IDProducto int,
	@IDDeposito tinyint,
	@IDEquipo tinyint

As

Declare @vEquipoConteo tinyint


Begin

	--Actualizar el Conteo del Equipo
	Update ExistenciaDepositoInventarioEquipo  Set Averiados=0,
												Conteo = 0,
												Total = 0														
	Where IDTransaccion= @IDTransaccion And IDDeposito = @IDDeposito And IDProducto = @IDProducto And IDEquipo = @IDEquipo 
	
	Set @vEquipoConteo = (Select EquipoConteo  From Equipoconteo Where ID=@IDEquipo) 
	
	If @vEquipoConteo = 1 Begin
		--Recalcular en DepositoInventario Equipo1
		Update ExistenciaDepositoInventario Set Equipo1=0,
											Averiados1 = 0,
											ConteoDiferencia = Equipo1 - Equipo2,
											DiferenciaSistema = Existencia - (Equipo1 + Averiados1 ) 
		Where IDTransaccion= @IDTransaccion And IDDeposito = @IDDeposito And IDProducto = @IDProducto 
	End
	
	If @vEquipoConteo = 2 Begin
		--Recalcular en DepositoInventario Equipo2
		Update ExistenciaDepositoInventario Set Equipo2=0,
											Averiados2 = 0,
											ConteoDiferencia = Equipo1 - Equipo2,
											DiferenciaSistema = Existencia - (Equipo1 + Averiados1 ) 
		Where IDTransaccion= @IDTransaccion And IDDeposito = @IDDeposito And IDProducto = @IDProducto 
	End											
											
End


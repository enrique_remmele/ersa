﻿CREATE Procedure  [dbo].[SpMovimientosOperativosContraContablesDetalle]
			
		@vCodigo as varchar(50)
		--,@IDTipoComprobante int
		,@vMes as int
		,@vAño as int
		,@SoloMostrarDiferencias as bit = 'True'

	as 
	begin
	
		--Crear la tabla temporal
    create table #TablaTemporal(IDTransaccion int,
								IDSucursal int,
								Operacion varchar(50),
								OperacionComprobante varchar(50),
								IDTipoComprobante int,
								TipoComprobante varchar(50),
								Entradas money,
								Salidas money,
								Debito money,
								Credito money)
		
		Declare @vIDTransaccion int
		Declare @vTipoComprobante varchar(50)
		Declare @vIDTipoComprobante int
		Declare @vDescripcionTipoComprobante varchar(50)
		Declare @vOperacion varchar(50)
		Declare @vCredito money
		Declare @vDebito money
		
		begin -- Movimientos
		Declare cMovimientos cursor for
		select 
		DA.IDTransaccion,
		DA.Operacion,
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante,
		'Credito'=Sum(DA.Credito),
		'Debito'=Sum(DA.Debito)
		from VDetalleAsiento DA
		Join vMovimiento M on DA.IDTransaccion = M.IDTransaccion
		where Year(DA.fecha)=@vAño
		and month(DA.fecha)=@vMes
		and DA.Operacion in('MOVIMIENTOS','DESCARGASTOCK','MOVIMIENTO MATERIA PRIMA','MOVIMIENTO DESCARGA DE COMPRA','CONSUMO COMBUSTIBLE')
		and DA.Codigo = @vCodigo
		--and M.IDTipoComprobante = @IDTipoComprobante
		Group by
		DA.IDTransaccion,
		DA.Operacion,
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante
		
		Open cMovimientos   
		fetch next from cMovimientos into @vIDTransaccion,@vOperacion,@vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
		
		While @@FETCH_STATUS = 0 Begin  
		
			Insert Into #TablaTemporal(IDTransaccion,IDSucursal,Operacion,OperacionComprobante, IDTipoComprobante,TipoComprobante,Entradas,Salidas, Debito, Credito)
			Select 
			IDTransaccion,
			IDSucursal,
			'Operacion'=@vOperacion,
			'OperacionComprobante'= concat(Suc,' ', NroComprobante),
			IDTipoComprobante,
			@vDescripcionTipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			@vDebito,
			@vCredito
			from
			(Select 
			IDTRansaccion,
			IDSucursal,
			Suc,
			NroComprobante,
			'TotalSalida' = sum(Case when CuentaContableSalida = @vCodigo then TotalDetalleSalida else 0 end), 
			'TotalEntrada' = sum(Case when CuentaContableEntrada = @vCodigo then TotalDetalleEntrada else 0 end),
			IDTipoComprobante
			From VDetalleMovimiento 
			Where year(Fecha)=@vAño
			and MONTH(fecha)=@vMes
			--And (CuentaContableEntrada = @vCodigo or CuentaContableSalida = @vCodigo)
			And (IDTipoComprobante=@vIDTipoComprobante)  
			And Anulado = 0
			and IDTransaccion = @vIDTransaccion
			group by 
			IDSucursal,
			DepositoSalida, 
			DepositoEntrada,
			IDTipoComprobante,
			Suc,
			NroComprobante,
			IDTransaccion) as c
			group by 
			IDSucursal,
			IDTransaccion,
			Suc,
			NroComprobante,
			IDTipoComprobante

		fetch next from cMovimientos into @vIDTransaccion,@vOperacion,@vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
			
		End
		
		close cMovimientos 
		deallocate cMovimientos
		end

		begin -- Ventas
		Declare cVenta cursor for
		select 
		DA.IDTransaccion,
		DA.Operacion,
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante,
		'Credito'=Sum(DA.Credito),
		'Debito'=Sum(DA.Debito)
		from VDetalleAsiento DA
		Join VVenta M on DA.IDTransaccion = M.IDTransaccion
		where Year(DA.fecha)=@vAño
		and month(DA.fecha)=@vMes
		and DA.Operacion = 'VENTAS CLIENTES'
		and DA.Codigo = @vCodigo
		Group by
		DA.IDTransaccion,
		DA.Operacion,
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante

		Open cVenta   
		fetch next from cVenta into @vIDTransaccion,@vOperacion,@vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
		
		While @@FETCH_STATUS = 0 Begin  
		

			Insert Into #TablaTemporal(IDTransaccion,IDSucursal,Operacion,OperacionComprobante, IDTipoComprobante,TipoComprobante,Entradas,Salidas, Debito, Credito)
			Select 
			IDTransaccion,
			IDSucursal,
			'Operacion'=@vOperacion,
			'OperacionComprobante'= concat(Sucursal,' ', Comprobante),
			IDTipoComprobante,
			@vDescripcionTipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			@vDebito,
			@vCredito
			from
			(Select
			IDTransaccion, 
			IDSucursal,
			Sucursal,
			Comprobante,
			'TotalSalida' = sum(Case when DV.CuentaContableDeposito = @vCodigo then DV.TotalCosto else 0 end), 
			'TotalEntrada' =0,
			DV.IDTipoComprobante
			From VDetalleVenta DV
			Where year(DV.Fecha)=@vAño
			and MONTH(DV.fecha)=@vMes
			and DV.ControlarExistencia = 'True'
			And DV.CuentaContableDeposito = @vCodigo 
			And DV.IDTipoComprobante=@vIDTipoComprobante
			And DV.Anulado = 0
			and IDTransaccion = @vIDTransaccion
			group by 
			DV.IDTransaccion,
			IDSucursal,
			DV.IDTipoComprobante,
			DV.Sucursal,
			DV.Comprobante) as c
			group by 
			IDTransaccion,
			IDSucursal,
			IDTipoComprobante,
			Sucursal,
			Comprobante


		fetch next from cVenta into @vIDTransaccion,@vOperacion,@vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
			
		End
		
		close cVenta 
		deallocate cVenta
		end

		begin -- nota credito
		Declare cNotaCredito cursor for
		select 
		DA.IDTransaccion,
		DA.Operacion,
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante,
		'Credito'=Sum(DA.Credito),
		'Debito'=Sum(DA.Debito)
		from VDetalleAsiento DA
		Join VNotaCredito M on DA.IDTransaccion = M.IDTransaccion
		where Year(DA.fecha)=@vAño
		and month(DA.fecha)=@vMes
		and DA.Operacion = 'NOTA CREDITO'
		and DA.Codigo = @vCodigo
		Group by
		DA.IDTransaccion,
		DA.Operacion,
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante

		Open cNotaCredito   
		fetch next from cNotaCredito into @vIDTransaccion,@vOperacion,@vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
		
		While @@FETCH_STATUS = 0 Begin  
		
			Insert Into #TablaTemporal(IDTransaccion,IDSucursal,Operacion,OperacionComprobante, IDTipoComprobante,TipoComprobante,Entradas,Salidas, Debito, Credito)
			Select 
			IDTransaccion,
			IDSucursal,
			'Operacion'=@vOperacion,
			'OperacionComprobante'= concat(Suc,' ', Comprobante),
			IDTipoComprobante,
			@vDescripcionTipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			@vDebito,
			@vCredito
			from
			(Select 
			IDTransaccion,
			'IDSucursal'=S.ID,
			'Suc'=S.Codigo,
			'Comprobante'=NroComprobante,
			'TotalSalida' = 0,
			'TotalEntrada' =sum(Case when DV.CuentaContableDeposito = @vCodigo then DV.TotalCosto else 0 end),
			DV.IDTipoComprobante
			From VDetalleNotaCredito DV
			Join Sucursal S on DV.IDSucursal = S.ID
			Where year(DV.Fecha)=@vAño
			and MONTH(DV.fecha)=@vMes
			and DV.ControlarExistencia = 'True'
			And DV.CuentaContableDeposito = @vCodigo 
			And DV.IDTipoComprobante=@vIDTipoComprobante
			And DV.Anulado = 0
			and IDTransaccion = @vIDTransaccion
			group by 
			DV.IDTipoComprobante
			,S.ID
			,IDTransaccion
			,S.Codigo
			,NroComprobante
			,IDTransaccion) as c
			group by 
			IDTipoComprobante
			,IDSucursal
			,IDTransaccion
			,Suc
			,Comprobante
			,IDTransaccion
		fetch next from cNotaCredito into @vIDTransaccion,@vOperacion,@vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
			
		End
		
		close cNotaCredito 
		deallocate cNotaCredito
		end

		begin -- compras

		Declare cCompras cursor for
		select 
		DA.IDTransaccion,
		DA.Operacion,
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante,
		'Credito'=Sum(DA.Credito),
		'Debito'=Sum(DA.Debito)
		from VDetalleAsiento DA
		Join VCompra M on DA.IDTransaccion = M.IDTransaccion
		where Year(DA.fecha)=@vAño
		and month(DA.fecha)=@vMes
		and DA.Operacion = 'COMPRA DE MERCADERIA'
		and DA.Codigo = @vCodigo
		Group by
		DA.IDTransaccion,
		DA.Operacion,
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante

		Open cCompras   
		fetch next from cCompras into @vIDTransaccion,@vOperacion,@vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
		
		While @@FETCH_STATUS = 0 Begin  
		
			Insert Into #TablaTemporal(IDTransaccion,IDSucursal,Operacion,OperacionComprobante, IDTipoComprobante,TipoComprobante,Entradas,Salidas, Debito, Credito)
			Select 
			IDTransaccion,
			IDSucursal,
			'Operacion'=@vOperacion,
			'OperacionComprobante'= concat(Suc,' ',Numero, ' ', Comprobante),
			IDTipoComprobante,
			@vDescripcionTipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			@vDebito,
			@vCredito 
			from
			(Select 
			IDTransaccion,
			'IDSucursal'=S.ID,
			'Suc'=S.Codigo,
			Numero,
			Comprobante,
			'TotalSalida' = 0, 
			'TotalEntrada' =sum(Case when DV.CuentaContableDeposito = @vCodigo then DV.TotalDiscriminado * DV.Cotizacion else 0 end),
			DV.IDTipoComprobante
			From VDetalleCompra DV
			join Sucursal S on DV.IDSucursal = S.ID
			Where year(DV.Fecha)=@vAño
			and MONTH(DV.fecha)=@vMes
			and DV.ControlarExistencia = 'True'
			And DV.CuentaContableDeposito = @vCodigo 
			And DV.IDTipoComprobante=@vIDTipoComprobante
			and DV.IDTransaccion = @vIDTransaccion
			group by 
			IDTransaccion
			,S.ID
			,DV.IDTipoComprobante
			,S.Codigo
			,Comprobante
			,DV.Numero) as c
			group by 
			IDTransaccion
			,IDSucursal
			,IDTipoComprobante
			,Suc
			,Comprobante
			,Numero

		fetch next from cCompras into @vIDTransaccion,@vOperacion,@vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
			
		End
		
		close cCompras 
		deallocate cCompras
		end

		begin -- nota credito proveedor
		Declare cNotaCreditoProveedor cursor for
		select 
		DA.IDTransaccion,
		DA.Operacion,
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante,
		'Credito'=Sum(DA.Credito),
		'Debito'=Sum(DA.Debito)
		from VDetalleAsiento DA
		Join VNotaCreditoProveedor M on DA.IDTransaccion = M.IDTransaccion
		where Year(DA.fecha)=@vAño
		and month(DA.fecha)=@vMes
		and DA.Operacion = 'NOTA CREDITO PROVEEDOR'
		and DA.Codigo = @vCodigo
		Group by
		DA.IDTransaccion,
		DA.Operacion,
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante

		Open cNotaCreditoProveedor   
		fetch next from cNotaCreditoProveedor into @vIDTransaccion,@vOperacion,@vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
		
		While @@FETCH_STATUS = 0 Begin  
		

			Insert Into #TablaTemporal(IDTransaccion,IDSucursal,Operacion,OperacionComprobante, IDTipoComprobante,TipoComprobante,Entradas,Salidas, Debito, Credito)
			Select 
			IDTransaccion,
			IDSucursal,
			'Operacion'=@vOperacion,
			'OperacionComprobante'= concat(Suc,' ', NroComprobante),
			IDTipoComprobante,
			@vDescripcionTipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			@vDebito,
			@vCredito 
			from
			(Select 
			IDTransaccion,
			'IDSucursal'=S.ID,
			'Suc'=S.Codigo,
			NroComprobante,
			'TotalSalida' = sum(Case when DV.CuentaContableDeposito = @vCodigo then DV.TotalDiscriminado else 0 end),
			'TotalEntrada' =0,
			DV.IDTipoComprobante
			From VDetalleNotaCreditoProveedor DV
			join Sucursal S on DV.IDSucursal = S.ID
			Where year(DV.Fecha)=@vAño
			and MONTH(DV.fecha)=@vMes
			and DV.ControlarExistencia = 'True'
			--And IDTipoProducto In (0, 2, 1, 6) 
			And DV.CuentaContableDeposito = @vCodigo 
			And DV.IDTipoComprobante=@vIDTipoComprobante
			And DV.Anulado = 0
			group by 
			DV.IDTipoComprobante
			,S.ID
			,IDTransaccion
			,S.Codigo
			,NroComprobante) as c
			group by 
			IDTipoComprobante
			,IDSucursal
			,IDTransaccion
			,Suc
			,NroComprobante


		fetch next from cNotaCreditoProveedor into @vIDTransaccion,@vOperacion,@vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
			
		End
		
		close cNotaCreditoProveedor 
		deallocate cNotaCreditoProveedor
		end


		begin --ticket bascula
		Declare cTicket cursor for
		select 
		DA.IDTransaccion,
		DA.Operacion,
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.TipoComprobante,
		'Credito'=Sum(DA.Credito),
		'Debito'=Sum(DA.Debito)
		from VDetalleAsiento DA
		Join VTicketBascula M on DA.IDTransaccion = M.IDTransaccion
		where Year(DA.fecha)=@vAño
		and month(DA.fecha)=@vMes
		and DA.Operacion = 'TICKET DE BASCULA'
		and DA.Codigo = @vCodigo
		and M.Anulado = 0
		Group by
		DA.IDTransaccion,
		DA.Operacion,
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.TipoComprobante

		Open cTicket   
		fetch next from cTicket into @vIDTransaccion,@vOperacion,@vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
		
		While @@FETCH_STATUS = 0 Begin  
		
			Insert Into #TablaTemporal(IDTransaccion,IDSucursal,Operacion,OperacionComprobante, IDTipoComprobante,TipoComprobante,Entradas,Salidas, Debito, Credito)
			Select 
			IDTransaccion,
			IDSucursal,
			'Operacion'=@vOperacion,
			'OperacionComprobante'= concat(Suc,' ', Comprobante),
			IDTipoComprobante,
			@vDescripcionTipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			@vDebito,
			@vCredito 
			from
			(Select 
			IDTransaccion,
			'IDSucursal'=S.ID,
			'Suc'=S.Codigo,
			'Comprobante'= concat(Numero,' ', NroComprobante),
			'TotalSalida' = 0, 
			'TotalEntrada' =sum(Case when DV.CuentaContableDeposito = @vCodigo then DV.TotalDiscriminado else 0 end),
			DV.IDTipoComprobante
			From VTicketBascula DV
			join Sucursal S on DV.IDSucursal = S.ID
			Where year(DV.Fecha)=@vAño
			and MONTH(DV.fecha)=@vMes
			And DV.CuentaContableDeposito = @vCodigo 
			And DV.IDTipoComprobante=@vIDTipoComprobante
			group by 
			DV.IDTipoComprobante,
			S.ID,
			IDTransaccion,
			S.Codigo,
			Numero,
			NroComprobante) as c
			group by 
			IDTipoComprobante
			,IDSucursal
			,IDTransaccion
			,Suc
			,Comprobante

		fetch next from cTicket into @vIDTransaccion,@vOperacion,@vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
			
		End
		
		close cTicket
		deallocate cTicket
		end

		------Otros

		--Declare cOtros cursor for
		
		--Select distinct
		--Operacion 
		--from vAsiento
		--Where year(fecha)=@vAño
		--and month(fecha)=@vMes
		--and Operacion not in('MOVIMIENTOS','DESCARGASTOCK','MOVIMIENTO MATERIA PRIMA','MOVIMIENTO DESCARGA DE COMPRA','CONSUMO COMBUSTIBLE','VENTAS CLIENTES','NOTA CREDITO','COMPRA DE MERCADERIA','NOTA CREDITO PROVEEDOR','TICKET DE BASCULA')
		
		--Open cOtros   
		--fetch next from cOtros into @vOperacion
		
		--While @@FETCH_STATUS = 0 Begin  
		
		--	Insert Into #TablaTemporal(Origen,Operacion,IDTipoComprobante,TipoComprobante,Entradas,Salidas, Orden)
		--	select 
		--	'Origen'=@vOperacion,
		--	'Operacion'='- OTROS',
		--	0,
		--	'OTROS',
		--	'Debito'=ISNULL((select Sum(DA.Debito) from VDetalleAsiento DA 
		--								where Year(DA.fecha)=@vAño
		--								and month(DA.fecha)=@vMes
		--								and DA.Codigo = @vCodigo
		--								and DA.Operacion=@vOperacion),0),
		--	'Credito'=ISNULL((Select Sum(DA.Credito) from VDetalleAsiento DA 
		--								where Year(DA.fecha)=@vAño
		--								and month(DA.fecha)=@vMes
		--								and DA.Codigo = @vCodigo
		--								and DA.Operacion=@vOperacion),0),
		--	7
			
		--	from  Operacion O
		--	where O.Descripcion=@vOperacion

		--fetch next from cOtros into @vOperacion
			
		--End
		
		--close cOtros 
		--deallocate cOtros


		begin -- Movimientos
			Insert Into #TablaTemporal(IDTransaccion,IDSucursal,Operacion,OperacionComprobante, IDTipoComprobante,TipoComprobante,Entradas,Salidas, Debito, Credito)
			Select 
			IDTransaccion,
			IDSucursal,
			Operacion,
			'OperacionComprobante'= concat(Suc,' ', NroComprobante),
			IDTipoComprobante,
			TipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			0,
			0 
			from
			(Select 
			DV.IDTransaccion,
			'IDSucursal'=S.ID,
			'Suc'=S.Codigo,
			M.NroComprobante,
			'TotalSalida' = sum(Case when CuentaContableSalida = @vCodigo then TotalDetalleSalida else 0 end), 
			'TotalEntrada' = sum(Case when CuentaContableEntrada = @vCodigo then TotalDetalleEntrada else 0 end),
			'Operacion'=O.Descripcion,
			M.TipoComprobante,
			DV.IDTipoComprobante
			From VDetalleMovimiento DV
			Join VMovimiento M on DV.IDTransaccion = M.IDTransaccion
			Join Transaccion T on DV.IDTransaccion = T.ID
			Join Operacion O on O.ID = T.IDOperacion
			Join Sucursal S on DV.IDSucursal = S.ID
			Where year(M.Fecha)=@vAño
			and MONTH(M.fecha)=@vMes
			and O.Descripcion in('MOVIMIENTOS','DESCARGASTOCK','MOVIMIENTO MATERIA PRIMA','MOVIMIENTO DESCARGA DE COMPRA','CONSUMO COMBUSTIBLE')
			And (DV.CuentaContableSalida = @vCodigo or CuentaContableEntrada = @vCodigo)
			and DV.IDTransaccion not in (select distinct IDTransaccion from #TablaTemporal)
			And M.Anulado = 0
			group by 
			S.ID,
			S.Codigo,
			O.Descripcion,
			M.DepositoSalida, 
			M.DepositoEntrada,
			M.TipoComprobante,
			DV.IDTipoComprobante,
			M.NroComprobante,
			DV.IDTransaccion) as c
			group by 
			IDSucursal,
			Operacion,
			IDTransaccion,
			Suc,
			NroComprobante,
			IDTipoComprobante,
			TipoComprobante
		end

		begin -- Ventas
			Insert Into #TablaTemporal(IDTransaccion,IDSucursal,Operacion,OperacionComprobante, IDTipoComprobante,TipoComprobante,Entradas,Salidas, Debito, Credito)
			Select 
			IDTransaccion,
			IDSucursal,
			Operacion,
			'OperacionComprobante'= concat(Suc,' ', Comprobante),
			IDTipoComprobante,
			TipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			0,
			0 
			from
			(Select
			DV.IDTransaccion,
			'IDSucursal'=S.ID,
			'Suc'=S.Codigo,
			DV.Comprobante,
			'TotalSalida' = sum(Case when DV.CuentaContableDeposito = @vCodigo then DV.TotalCosto else 0 end), 
			'TotalEntrada' =0,
			'Operacion'=O.Descripcion,
			M.TipoComprobante,
			DV.IDTipoComprobante
			From VDetalleVenta DV
			Join VVenta M on DV.IDTransaccion = M.IDTransaccion
			Join Transaccion T on DV.IDTransaccion = T.ID
			Join Operacion O on O.ID = T.IDOperacion
			Join Sucursal S on DV.IDSucursal = S.ID
			Where year(DV.Fecha)=@vAño
			and MONTH(DV.fecha)=@vMes
			and DV.ControlarExistencia = 'True'
			and O.Descripcion = 'VENTAS CLIENTES'
			And DV.CuentaContableDeposito = @vCodigo 
			and DV.IDTransaccion not in (select distinct IDTransaccion from #TablaTemporal)
			And DV.Anulado = 0
			group by 
			S.ID,
			S.Codigo,
			O.Descripcion,
			M.TipoComprobante,
			DV.IDTipoComprobante,
			DV.Comprobante,
			DV.IDTransaccion) as c
			group by 
			IDSucursal,
			Operacion,
			IDTransaccion,
			Suc,
			Comprobante,
			IDTipoComprobante,
			TipoComprobante
		end

		begin -- nota credito
			Insert Into #TablaTemporal(IDTransaccion,IDSucursal,Operacion,OperacionComprobante, IDTipoComprobante,TipoComprobante,Entradas,Salidas, Debito, Credito)
			Select 
			IDTransaccion,
			IDSucursal,
			Operacion,
			'OperacionComprobante'= concat(Suc,' ', Comprobante),
			IDTipoComprobante,
			TipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			0,
			0 
			from
			(Select 
			DV.IDTransaccion,
			'IDSucursal'=S.ID,
			'Suc'=S.Codigo,
			DV.Comprobante,
			'TotalSalida' = 0, 
			'TotalEntrada' =sum(Case when DV.CuentaContableDeposito = @vCodigo then DV.TotalCosto else 0 end),
			'Operacion'=O.Descripcion,
			M.TipoComprobante,
			DV.IDTipoComprobante
			From VDetalleNotaCredito DV
			Join VNotaCredito M on DV.IDTransaccion = M.IDTransaccion
			Join Transaccion T on DV.IDTransaccion = T.ID
			Join Operacion O on O.ID = T.IDOperacion
			Join Sucursal S on DV.IDSucursal = S.ID
			Where year(DV.Fecha)=@vAño
			and MONTH(DV.fecha)=@vMes
			and DV.ControlarExistencia = 'True'
			and O.Descripcion = 'NOTA CREDITO'
			And DV.CuentaContableDeposito = @vCodigo 
			and DV.IDTransaccion not in (select distinct IDTransaccion from #TablaTemporal)
			And DV.Anulado = 0
			group by 
			S.ID,
			S.Codigo,
			O.Descripcion,
			M.TipoComprobante,
			DV.IDTipoComprobante,
			DV.Comprobante,
			DV.IDTransaccion) as c
			group by 
			IDSucursal,
			Operacion,
			IDTransaccion,
			Suc,
			Comprobante,
			IDTipoComprobante,
			TipoComprobante

		end

		begin -- compras
			Insert Into #TablaTemporal(IDTransaccion,IDSucursal,Operacion,OperacionComprobante, IDTipoComprobante,TipoComprobante,Entradas,Salidas, Debito, Credito)
			Select 
			IDTransaccion,
			IDSucursal,
			Operacion,
			'OperacionComprobante'= concat(Suc,' ',Numero, ' ', Comprobante),
			IDTipoComprobante,
			TipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			0,
			0 
			from
			(Select 
			DV.IDTransaccion,
			'IDSucursal'=S.ID,
			'Suc'=S.Codigo,
			DV.Numero,
			DV.Comprobante,
			'TotalSalida' = 0, 
			'TotalEntrada' =sum(Case when DV.CuentaContableDeposito = @vCodigo then DV.TotalDiscriminado * DV.Cotizacion else 0 end),
			'Operacion'=O.Descripcion,
			M.TipoComprobante,
			DV.IDTipoComprobante
			From VDetalleCompra DV
			Join VCompra M on DV.IDTransaccion = M.IDTransaccion
			Join Transaccion T on DV.IDTransaccion = T.ID
			Join Operacion O on O.ID = T.IDOperacion
			join Sucursal S on DV.IDSucursal = S.ID
			Where year(DV.Fecha)=@vAño
			and MONTH(DV.fecha)=@vMes
			and DV.ControlarExistencia = 'True'
			and O.Descripcion = 'COMPRA DE MERCADERIA'
			And DV.CuentaContableDeposito = @vCodigo 
			and DV.IDTransaccion not in (select distinct IDTransaccion from #TablaTemporal)
			group by 
			S.ID,
			S.Codigo,
			O.Descripcion,
			M.TipoComprobante,
			DV.IDTipoComprobante,
			DV.Comprobante,
			DV.Numero,
			DV.IDTransaccion) as c
			group by 
			IDSucursal,
			Operacion,
			IDTransaccion,
			Suc,
			Comprobante,
			IDTipoComprobante,
			TipoComprobante,
			Numero

		end

		begin -- nota credito proveedor
			Insert Into #TablaTemporal(IDTransaccion,IDSucursal,Operacion,OperacionComprobante, IDTipoComprobante,TipoComprobante,Entradas,Salidas, Debito, Credito)
			Select 
			IDTransaccion,
			IDSucursal,
			Operacion,
			'OperacionComprobante'= concat(Suc,' ', NroComprobante),
			IDTipoComprobante,
			TipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			0,
			0 
			from
			(Select 
			DV.IDTransaccion,
			'IDSucursal'=S.ID,
			'Suc'=S.Codigo,
			DV.NroComprobante,
			'TotalSalida' = sum(Case when DV.CuentaContableDeposito = @vCodigo then DV.TotalDiscriminado else 0 end),
			'TotalEntrada' =0,
			'Operacion'=O.Descripcion,
			M.TipoComprobante,
			DV.IDTipoComprobante
			From VDetalleNotaCreditoProveedor DV
			Join VNotaCreditoProveedor M on DV.IDTransaccion = M.IDTransaccion
			Join Transaccion T on DV.IDTransaccion = T.ID
			Join Operacion O on O.ID = T.IDOperacion
			join Sucursal S on DV.IDSucursal = S.ID
			Where year(DV.Fecha)=@vAño
			and MONTH(DV.fecha)=@vMes
			and DV.ControlarExistencia = 'True'
			and O.Descripcion = 'NOTA CREDITO PROVEEDOR'
			And DV.CuentaContableDeposito = @vCodigo 
			and DV.IDTransaccion not in (select distinct IDTransaccion from #TablaTemporal)
			And DV.Anulado = 0
			group by 
			S.ID,
			S.Codigo,
			O.Descripcion,
			M.TipoComprobante,
			DV.IDTipoComprobante,
			DV.NroComprobante,
			DV.IDTransaccion) as c
			group by 
			IDSucursal,
			Operacion,
			IDTransaccion,
			Suc,
			NroComprobante,
			IDTipoComprobante,
			TipoComprobante

		end


		begin --ticket bascula
			Insert Into #TablaTemporal(IDTransaccion,IDSucursal,Operacion,OperacionComprobante, IDTipoComprobante,TipoComprobante,Entradas,Salidas, Debito, Credito)
			Select 
			IDTransaccion,
			IDSucursal,
			Operacion,
			'OperacionComprobante'= concat(Suc,' ', Comprobante),
			IDTipoComprobante,
			TipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			0,
			0 
			from
			(Select 
			IDTransaccion,
			'IDSucursal'=S.ID,
			'Suc'=S.Codigo,
			'Comprobante'= concat(Numero,' ', NroComprobante),
			'TotalSalida' = 0, 
			'TotalEntrada' =sum(Case when DV.CuentaContableDeposito = @vCodigo then DV.TotalDiscriminado else 0 end),
			'Operacion'=O.Descripcion,
			DV.TipoComprobante,
			DV.IDTipoComprobante
			From VTicketBascula DV
			Join Transaccion T on DV.IDTransaccion = T.ID
			Join Operacion O on O.ID = T.IDOperacion
			join Sucursal S on DV.IDSucursal = S.ID
			Where year(DV.Fecha)=@vAño
			and MONTH(DV.fecha)=@vMes
			and O.Descripcion = 'TICKET DE BASCULA'
			And DV.CuentaContableDeposito = @vCodigo 
			and DV.IDTransaccion not in (select distinct IDTransaccion from #TablaTemporal)
			group by 
			S.ID,
			S.Codigo,
			O.Descripcion,
			DV.TipoComprobante,
			DV.IDTipoComprobante,
			DV.NroComprobante,
			DV.Numero,
			DV.IDTransaccion) as c
			group by 
			IDSucursal,
			Operacion,
			IDTransaccion,
			Suc,
			Comprobante,
			IDTipoComprobante,
			TipoComprobante
		end
		




		if @SoloMostrarDiferencias = 'True' begin
			Select *,'Diferencia Entrada'= Entradas-Debito, 'Diferencia Salida'=Salidas-Credito from #TablaTemporal where Round(Entradas,0) <> Round(Debito,0) or ROUND(Salidas,0)<>Round(Credito,0)
		end 
		else begin
			Select *,'Diferencia Entrada'= Entradas-Debito, 'Diferencia Salida'=Salidas-Credito from #TablaTemporal 
		end
	end

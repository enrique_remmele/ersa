﻿
CREATE Procedure [dbo].[SpLimiteDescuentoUsuario]

	--Entrada
	@IDUsuarioDescuento int,
	@IDListaPrecio int,
	@Porcentaje decimal(18,2),
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if not exists(Select * From Usuario Where ID=@IDUsuarioDescuento) begin
			set @Mensaje = 'El usuario no existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if not exists(Select * From ListaPrecio Where ID=@IDListaPrecio) begin
			set @Mensaje = 'La lista de precios no existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la configuracion ya existe
		if exists(Select * From LimiteDescuentoUsuario Where IDUsuario=@IDUsuarioDescuento And IDListaPrecio=@IDListaPrecio) begin
			set @Mensaje = 'La configuracion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Insertamos
		Insert Into LimiteDescuentoUsuario(IDUsuario, IDListaPrecio, Porcentaje)
		Values(@IDUsuarioDescuento, @IDListaPrecio, @Porcentaje)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='LIMITEDESCUENTOUSUARIO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From LimiteDescuentoUsuario Where IDUsuario=@IDUsuarioDescuento And IDListaPrecio=@IDListaPrecio) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update LimiteDescuentoUsuario Set Porcentaje=@Porcentaje
										Where IDUsuario=@IDUsuarioDescuento 
										And IDListaPrecio = @IDListaPrecio
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='LIMITEDESCUENTOUSUARIO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From LimiteDescuentoUsuario Where IDUsuario=@IDUsuarioDescuento And IDListaPrecio=@IDListaPrecio) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		Delete From LimiteDescuentoUsuario 
		Where IDUsuario=@IDUsuarioDescuento And IDListaPrecio=@IDListaPrecio
		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='LIMITEDESCUENTOUSUARIO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿CREATE Procedure [dbo].[SpViewChequeras]
	--Entrada
	@ID Int,
	@IDCuentaBancaria int,
	@EstadoWhere varchar(16)
As

Begin
  
	--variables
	Declare @vID tinyint
	Declare @NroOp varchar(32)
	Declare @CuentaBancaria varchar(50)
	Declare @Banco varchar(50)
	Declare @NroCheque varchar(32)
	Declare @Fecha Date
	Declare @Importe money
	Declare @Moneda varchar(6)
	
	declare @NroDesde int
	declare @NroHasta int
	Declare @AUtilizar int
	Declare @i int 
	declare @estado varchar(16)
	
	set @NroDesde = (Select Nrodesde from Chequera where id = @ID)
	set @NroHasta = (Select NroHasta from Chequera where id = @ID)
	set @AUtilizar = (Select AUtilizar from Chequera where id = @ID)
	set @i = @NroDesde
	Set @Banco = (Select B.Descripcion from Banco B join CuentaBancaria CB on Cb.IDBanco = B.Id where CB.Id = @IDCuentaBancaria)
	Set @Moneda = (Select Mon from VCuentaBancaria where ID = @IDCuentaBancaria)
	Set @CuentaBancaria = (Select CuentaBancaria from CuentaBancaria where id = @IDCuentaBancaria)

	Begin
		
	--Crear la tabla temporal
    Create table #TablaChequera(IDChequera int,
								IdCuentaBancaria int,
								CuentaBancaria varchar(50),
								Banco varchar(50),
								Moneda varchar(6),
								NroCheque varchar (32),
								Importe money,
								Fecha date,
								Estado Varchar (16),
								NroOP int
								)
										
	WHILE @i <= @NroHasta BEGIN
		
		set @estado = null
		set @Importe = 0
		set @Fecha = SYSDATETIME()
		set @NroOp = ''
		Select top 1 @estado = Anulado, @Importe = ISNULL(ImporteMoneda,0), @Fecha = Fecha, @NroOp = Numero from Cheque where IDCuentaBancaria = @IDCuentaBancaria and NroCheque = cast(@i as varchar(100)) order by IDTransaccion desc
		if @estado = 1 begin
			set @estado = 'Anulado'
		end else if @estado = 0 begin
			set @estado = 'Utilizado'
		end else if @estado is null begin
			set @estado = 'En blanco'
		end

				Insert Into #TablaChequera(IDChequera, IdCuentaBancaria,CuentaBancaria,Banco,Moneda,NroCheque,Importe, Fecha, Estado, NroOP)
				Values(@Id,@IDCuentaBancaria,@CuentaBancaria, @Banco,@Moneda,@i,@Importe, @Fecha,@estado,@NroOp )
				set @i = @i +1 
		END
	
		if @EstadoWhere = 'Todos'
		begin
			Select * From #TablaChequera
		end
		else
		begin
			Select * From #TablaChequera where estado = @EstadoWhere order by NroCheque
		end
			
	End
End
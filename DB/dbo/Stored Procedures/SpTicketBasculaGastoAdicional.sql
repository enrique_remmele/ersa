﻿CREATE Procedure [dbo].[SpTicketBasculaGastoAdicional]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDGastoAdicional numeric(18,0) = NULL,
	@ID int = NULL,
	@IDMoneda tinyint = NULL,
	@Importe money = null,
	@ImporteGS money = null,
	@ImporteUS money = null,
	@Cotizacion money = NULL,
	@CotizacionUS money = NULL,
	@IDImpuesto tinyint = 1,
	@IDProveedor int = null,
	@Operacion varchar(50),		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin		
	--INSERTAR
	If @Operacion = 'INS' Begin
		If not Exists(Select * From TicketBascula Where IDTransaccion = @IDTransaccion) Begin
			set @Mensaje = 'El Gasto no tiene Ticket de bascula Asociado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		--Importe sin IVA
		Declare @ImporteSinIVA money = @ImporteGS
		IF @IDImpuesto = 1 Begin
		  set @ImporteSinIVA = @ImporteSinIVA / 1.1
		End 
		IF @IDImpuesto = 2 Begin
		  set @ImporteSinIVA = @ImporteSinIVA / 1.05
		End 

		--select * from TicketBasculaGastoAdicional
		Insert Into TicketBasculaGastoAdicional
		Values(@IDTransaccion, @ID, @IDGastoAdicional, @IDMoneda, @Cotizacion, 
		@Importe, @ImporteGS,@ImporteUS, @CotizacionUS, @IDImpuesto,@ImporteSinIVA,@IDProveedor)
				
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'DEL' Begin
		If not Exists(Select * From TicketBascula Where IDTransaccion = @IDTransaccion) Begin
			set @Mensaje = 'El Gasto no tiene Ticket de bascula Asociado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		Delete from TicketBasculaGastoAdicional where IDTransaccion = @IDTransaccion
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	return @@rowcount
		
End


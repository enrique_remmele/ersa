﻿--select * from DetalleOrdenDePedido
CREATE Procedure [dbo].[SpDetalleOrdenDePedido]

	--Entrada
	@IDTransaccion numeric(18,0),
	@IDProducto int = NULL,
	@ID tinyint = NULL,
	@IDDeposito tinyint = NULL,
	@Observacion varchar(50) = NULL,
	@Existencia decimal(10,2) = NULL,
	@Pedido decimal(10,2) = NULL,
	@Extra decimal(10,2) = NULL,
	@FechaEntrega date =  null,
	@Operacion varchar(20),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin
	
	--Variables Kardex
	Declare @vFecha datetime
	Declare @vIDSucursal integer
	Declare @vNroComprobante varchar(50)
	Set @vFecha = (Select Fecha From OrdenDePedido Where IDTransaccion = @IDTransaccion)
	Set @vNroComprobante = (Select NroComprobante From OrdenDePedido Where IDTransaccion = @IDTransaccion)


	--Validar
	--Transaccion
	If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--Transaccion
	If Not Exists(Select IDTransaccion From OrdenDePedido Where IDTransaccion=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
		
	--BLOQUES
	if @Operacion='INS' Begin
		--Insertar el detalle
		Insert Into DetalleOrdenDePedido(IDTransaccion, IDProducto, ID, IDDeposito, Observacion, Existencia,Pedido,Extra, Anulado, FechaEntrega, CantidadAEntregar)
		Values(@IDTransaccion, @IDProducto, @ID, @IDDeposito, @Observacion,@Existencia,@Pedido,@Extra,'False', @FechaEntrega, @Extra)
		
		Set @Procesado = 'False'
		return @@rowcount					
			
	End
	
	if @Operacion='DEL' Begin
		
		--Validar
		--Transaccion
		If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra la transaccion!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--Transaccion
		If Not Exists(Select IDTransaccion From OrdenDePedido Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra la transaccion!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	if @Operacion='ANULAR' Begin
		
		--Verificar que exista
		If Not Exists(Select * From DetalleOrdenDePedido Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El Registro ya esta anulado!'
			Set @Procesado = 'True'
			return @@rowcount
		End
		
		--Si todo es correcto, anulamos con el cursor
		Begin		
			--Poner en Anulado el registro
			Update DetalleOrdenDePedido Set Anulado='True'
			Where @IDTransaccion=@IDTransaccion
		End
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	return @@rowcount
	
End
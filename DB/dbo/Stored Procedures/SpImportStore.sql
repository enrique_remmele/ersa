﻿CREATE procedure [dbo].[SpImportStore]

	@IDSucursal int
as
Begin


	Select
	'cdStore'=Referencia,
	'cdRegionStatus'=(Case When IDEStado=2 Then 'ACT'Else 'DEACT'End),
	'cdStoreBrand'='LATAM',
	'dsName'=NombreFantasia,
	'dsCorporateName'=RazonSocial,
	'cdClass1'=TipoCliente,
	'cdClass2'='',
	'cdCity'=IDCiudad,
	'cdRegion'=IDZonaVenta,
	'nmAddress'=Direccion,
	'nrAddress'='',
	'dsAddressComplement'='',
	'nrZipCode'='',
	'nmNeighborhood'=Barrio
	From VCliente Where IDSucursal=@IDSucursal
	
	
End


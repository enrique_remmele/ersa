﻿CREATE Procedure [dbo].[SpUsuarioOperacionFueraRangoFecha]

	--Entrada
	@IDUsuario smallint,
	@IDOperacion varchar(10),
	@Desde date,
	@Hasta date,
	@Operacion char(3),
	
	--Auditoria
	@IDTerminal smallint,
	@IDUsuarioOperacion int			
	
As

Begin

	--BLOQUES
	Declare @vMensaje varchar(100) = 'No se proceso!'
	Declare @vProcesado bit = 'False'
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From UsuarioOperacionFueraRangoFecha Where IDUsuario=@IDUsuario And IDOperacion=@IDOperacion) begin
			set @vMensaje = 'El usuario ya cuenta con permiso para la operación seleccionada!'
			set @vProcesado = 'False'
			Goto Salir
		end
		
		--Insertamos
		Insert Into UsuarioOperacionFueraRangoFecha(IDUsuario, IDOperacion, Desde, Hasta)
		Values(@IDUsuario, @IDOperacion, @Desde, @Hasta)
	
		Insert Into aUsuarioOperacionFueraRangoFecha(IDUsuario, IDOperacion, Desde, Hasta, Operacion, IDUsuarioOperacion, FechaOperacion, IDTerminalOperacion)
		Values(@IDUsuario, @IDOperacion, @Desde, @Hasta, @Operacion, @IDUsuarioOperacion, GetDate(), @IDTerminal)
				
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='UsuarioOperacionFueraRangoFecha', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @vMensaje = 'Registro guardado!'
		set @vProcesado = 'True'
		GoTo Salir
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From UsuarioOperacionFueraRangoFecha Where IDUsuario=@IDUsuario And IDOperacion=@IDOperacion) begin
			set @vMensaje = 'El sistema no encuentra el registro solicitado!'
			set @vProcesado = 'False'
			GoTo Salir
		end
		Insert Into aUsuarioOperacionFueraRangoFecha(IDUsuario, IDOperacion, Desde, Hasta, Operacion, IDUsuarioOperacion, FechaOperacion, IDTerminalOperacion)
		Select IDUsuario, IDOperacion, Desde, Hasta, @Operacion, @IDUsuarioOperacion, GetDate(), @IDTerminal from UsuarioOperacionFueraRangoFecha Where IDUsuario=@IDUsuario And IDOperacion=@IDOperacion
		
		
		--Eliminamos
		Delete From UsuarioOperacionFueraRangoFecha 
		Where IDUsuario=@IDUsuario And IDOperacion=@IDOperacion
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='UsuarioOperacionFueraRangoFecha', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @vMensaje = 'Registro guardado!'
		set @vProcesado = 'True'
		GoTo Salir
			
	end
	
	GoTo Salir
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


﻿CREATE Procedure [dbo].[SpImportarCuentaContableProducto]

	--Entrada
	@Referencia varchar(100) = NULL,
	@Codigo varchar(50) = NULL,
	@Proveedor varchar(50) = NULL,
	
	@Actualizar bit
	
As

Begin

	Declare @vIDProducto int
	Declare @vOperacion varchar(50)
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	
	Set @vMensaje = 'No se proceso'
	Set @vProcesado = 'False'
	Set @vOperacion = 'INS'
	
	--Obtener codigos
	Begin
		
		
		--INSERTAR
		If @vOperacion = 'INS' Begin
			
			Set @vMensaje = 'Registro insertado'
			Set @vProcesado = 'True'
			
		End
			
		--ACTUALIZAR
		If @vOperacion = 'UPD' Begin	
				
			If @Actualizar = 'False' Begin
				Goto Salir
			End
			
			Set @vMensaje = 'Registro actualizado'
			Set @vProcesado = 'True'
			
		End	
		
	End	

Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	
End


﻿CREATE Procedure [dbo].[SpTransporte]

	--Entrada
	@ID tinyint = NULL,
	@Descripcion varchar(50) = NULL,
	@IDZonaVenta tinyint = NULL,
	@IDDistribuidor tinyint = NULL,
	@IDCamion tinyint = NULL,
	@IDChofer tinyint = NULL,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Transporte Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDTransporte tinyint
		set @vIDTransporte = (Select IsNull((Max(ID)+1), 1) From Transporte)

		--Insertamos
		Insert Into Transporte(ID, Descripcion, IDZonaVenta, IDDistribuidor, IDCamion, IDChofer, FechaModificacion)
		Values(@vIDTransporte, @Descripcion, @IDZonaVenta, @IDDistribuidor, @IDCamion, @IDChofer, GETDATE())		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TRANSPORTE', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
				
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From Transporte Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Transporte Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Transporte Set	Descripcion=@Descripcion, 
								IDZonaVenta=@IDZonaVenta,
								IDDistribuidor=@IDDistribuidor,
								IDCamion = @IDCamion,
								IDChofer = @IDChofer,
								FechaModificacion=GETDATE()
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TRANSPORTE', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
			
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Transporte Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end					
		
		--Eliminamos
		Delete From Transporte 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TRANSPORTE', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
			
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


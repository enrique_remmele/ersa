﻿
CREATE Procedure [dbo].[SpUsuarioUnidadNegocio]

	--Entrada
	@ID tinyint,
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From UsuarioUnidadNegocio Where ID = @ID and Estado = 'True') begin
			set @Mensaje = 'El Usuario ya esta habilitado!'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Si es que la descripcion ya existe
		if exists(Select * From UsuarioUnidadNegocio Where ID = @ID and Estado = 'False') begin
			set @Mensaje = 'El Usuario ya esta habilitado, pero con Estado Desactivado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Insertamos
		Insert Into UsuarioUnidadNegocio(ID, Estado)
		Values(@ID, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='USUARIOUNIDADNEGOCIO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
				 		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From UsuarioUnidadNegocio Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update UsuarioUnidadNegocio Set Estado =@Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='USUARIOUNIDADNEGOCIO', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From UsuarioUnidadNegocio Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From UsuarioUnidadNegocio 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='USUARIOUNIDADNEGOCIO', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


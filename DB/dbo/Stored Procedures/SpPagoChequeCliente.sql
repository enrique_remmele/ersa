﻿
CREATE Procedure [dbo].[SpPagoChequeCliente]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDTipoComprobante smallint = NULL,
	@Numero int = null,
	@NroComprobante int = NULL,
	@Fecha date = NULL,
	@Observacion varchar(100) = NULL,
	@Total money = NULL,
	@Anulado bit = 'False',
		
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
	
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from PagoChequeCliente where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		If Exists(Select * From PagoChequeCliente   Where Numero=@Numero And IDSucursal=@IDSucursalOperacion) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Si es que la descripcion ya existe
		If Exists(Select * From PagoChequeCliente Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante and Numero=@Numero And IDSucursal=@IDSucursalOperacion) begin
			set @Mensaje = 'El Pago ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--Insertar en DepositoBancario
		Insert Into PagoChequeCliente (IDTransaccion, IDSucursal , Numero, IDTipoComprobante, NroComprobante, Fecha ,     Observacion ,  Total,Anulado )
		Values(@IDTransaccionSalida , @IDSucursalOperacion, @Numero, @IDTipoComprobante , @NroComprobante , @Fecha,    @Observacion,  @Total, @Anulado   )
								
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From PagoChequeCliente     Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Reestablecer el efectivo
		Exec SpPagoChequeClienteProcesar
		     @IDTransaccion = @IDTransaccion,
		     @Operacion = @Operacion,
		     @Procesado = @Procesado,
		     @Mensaje = @Mensaje
		
		
		--Eliminamos el detalle
		Delete DetallePagoChequeCliente   where IDTransaccionChequeCliente  = @IDTransaccion				
				
		--Eliminamos el registro
		Delete PagoChequeCliente   Where IDTransaccion = @IDTransaccion
						
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion				
				
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From PagoChequeCliente Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
	
		If Exists(Select * From PagoChequeCliente Where IDTransaccion=@IDTransaccion And Anulado='True') Begin
			set @Mensaje = 'Esta transacción ya se encuentra anulada.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Reestablecer el efectivo
		Exec SpPagoChequeClienteProcesar
		     @IDTransaccion = @IDTransaccion,
		     @Operacion = @Operacion,
		     @Procesado = @Procesado,
		     @Mensaje = @Mensaje
		     
		       --Insertamos el registro de anulacion
		Exec SpDocumentoAnulado
			 @IDTransaccion=@IDTransaccion,
			 @IDUsuario=@IDUsuario, 
			 @IDSucursal=@IDSucursal,
		     @IDTerminal=@IDTerminal
		
		
		--Actualizar el registro
		Update PagoChequeCliente Set Anulado='True' 
		Where IDTransaccion = @IDTransaccion
						
		--Eliminamos los asientos
		
		Delete From Efectivo Where IDTransaccion=@IDTransaccion 
		
		Delete From FormaPagoDocumento Where IDTransaccion=@IDTransaccion 
		
		Delete From DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete From Asiento Where IDTransaccion = @IDTransaccion
				
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = 0
	return @@rowcount
		
End





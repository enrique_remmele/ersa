﻿
create Procedure [dbo].[SpFondoFijoCuentaContable]

	--Entrada
	@IDFondoFijo int,
	@IDCuentaContable int,
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From FondoFijoCuentaContable Where IDFondoFijo = @IDFondoFijo and IDCuentaContable = @IDCuentaContable) begin
			set @Mensaje = 'La relacion Fondo Fijo y Cuenta Contable ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end


		--Insertamos
		Insert Into FondoFijoCuentaContable(IDFondoFijo, IDCuentaContable, Estado)
		Values(@IDFondoFijo,@IDCuentaContable,@Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='FONDOFIJOCUENTACONTABLE', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
				 		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From FondoFijoCuentaContable Where IDFondoFijo = @IDFondoFijo and IDCuentaContable = @IDCuentaContable) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		----Si es que la descripcion ya existe
		--if exists(Select * From FondoFijoCuentaContable Where IDFondoFijo = @IDFondoFijo and IDCuentaContable = @IDCuentaContable) begin
		--	set @Mensaje = 'La relacion Fondo Fijo y Cuenta Contable ya existe!'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--end
		
		--Actualizamos
		Update FondoFijoCuentaContable Set	Estado =@Estado
		Where  IdFondoFijo = @IdFondoFijo
			and IdCuentaContable = @IDCuentaContable
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='FONDOFIJOCUENTACONTABLE', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		If not exists(Select * From FondoFijoCuentaContable Where IDFondoFijo = @IDFondoFijo and IDCuentaContable = @IDCuentaContable) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From FondoFijoCuentaContable 
		Where IdFondoFijo = @IdFondoFijo
			and IdCuentaContable = @IDCuentaContable
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='FONDOFIJOCUENTACONTABLE', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


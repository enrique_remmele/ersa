﻿CREATE Procedure [dbo].[SpImportarUNCC]

	--Entrada
	--Identificadores
	@Tipo  varchar(6),
	@Descripcion varchar(200) = '',
	@Actualizar bit



As

Begin

	Declare @vOperacion varchar(50)
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	declare @vID int

	Set @vOperacion = 'INS'
	Set @vMensaje = 'Sin proceso'
	Set @vProcesado = 'False'

	
	IF @Tipo = 'UN' and not exists(Select * from UnidadNegocio where Descripcion = @Descripcion) Begin
		set @vID = (Select Isnull(max(ID),0) from UnidadNegocio)		
		IF @vID = 0 Begin
		 insert into UnidadNegocio (ID,Codigo,Descripcion,Estado) Values(1,'',@Descripcion,'True')
		End

		IF @vID >= 1 Begin
		 set @vID = @vID +1
		 insert into UnidadNegocio (ID,Codigo,Descripcion,Estado) Values(@vID,'',@Descripcion,'True')
		End
		set @vMensaje = concat('UN ',@Descripcion,' Procesada')
		set @vProcesado = 'True'
		GoTo Salir
	End

	--Centro de Costo
	IF @Tipo = 'CC' and not exists(Select * from CentroCosto where Descripcion = @Descripcion) Begin
		set @vID = (Select Isnull(max(ID),0) from CentroCosto)
		IF @vID = 0 Begin
		 insert into CentroCosto(ID,Codigo,Descripcion,Estado) Values(1,'',@Descripcion,'True')
		End
		IF @vID >= 1 Begin
		 set @vID = @vID +1
		 insert into CentroCosto (ID,Codigo,Descripcion,Estado) Values(@vID,'',@Descripcion,'True')
		End
		set @vMensaje = concat('CC ',@Descripcion,' Procesado')
		set @vProcesado = 'True'
		GoTo Salir
	End


	salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	

End

















﻿CREATE Procedure [dbo].[SpActualizarImpresoOrdenPago]
	
	--Entrada
	@IDTransaccion numeric(18,0),
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	

As

Begin
	
	Set @Mensaje = 'No se proceso'
	Set @Procesado = 'False'
	
	--Validar
	--Existe
	If Not Exists(Select * From OrdenPago Where IDTransaccion = @IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra el registro'
		Set @Procesado = 'False'	
		Return @@rowcount
	End
			
	--Actualizar
	Update OrdenPago Set Impreso = 'True'
	Where IDTransaccion = @IDTransaccion
	
	Set @Mensaje = 'Registro procesado!'
	Set @Procesado = 'True'	
	Return @@rowcount
	
End


﻿CREATE Procedure [dbo].[SpDescuentoChequeProcesar]

	--Entrada
	@IDTransaccion numeric(18),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

    ---Variables para CALCULAR SALDO DEPOSITADO
	declare @Saldo money
	declare @Depositado money
	declare @Cancelado bit

	--Declarar variables Variables
	declare @vIDTransaccionCheque numeric (18,0)
	
	--27/07/2021 SM Para resguardar datos de transaccion a eliminar
		Declare @vvIDAuditoria int
	    Declare @vvIDSucursal tinyint
		Declare @vvNumero int
		Declare @vvIDCliente int
		Declare @vvIDBanco tinyint
		Declare @vvIDCuentaBancaria int
		Declare @vvFecha date
		Declare @vvNroCheque varchar (50)
		Declare @vvIDMoneda tinyint
		Declare @vvCotizacion money
		Declare @vvImporteMoneda money
		Declare @vvImporte money
		Declare @vvDiferido bit
		Declare @vvFechaVencimiento date
		Declare @vvChequeTercero bit
		Declare @vvLibrador varchar (50)
		Declare @vvSaldo money
		Declare @vvCancelado bit
		Declare @vvCartera bit
		Declare @vvDepositado bit
		Declare @vvRechazado bit
		Declare @vvConciliado bit
		Declare @vvIDMotivoRechazo tinyint
		Declare @vvSaldoACuenta money
		Declare @vvTitular varchar (150)
		Declare @vvFechaCobranza date
		Declare @vvDescontado bit
		Declare @vvIDUsuario int
		Declare @vvAccion varchar(3)

		Declare @IDUsuario int = (select IDUsuario from Transaccion where ID=@IDTransaccion)
		Declare @IDTerminal int  = (select IDTerminal from Transaccion where ID=@IDTransaccion)
		Declare @IDOperacion tinyint  = (select IDOperacion from Transaccion where ID=@IDTransaccion)				
	
		--INICIO SM 27072021 - Auditoria Informática
		Declare @vObservacion varchar(100)='' 
		Declare @vvID int 

	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'

	If @Operacion = 'INS' Begin
		
		
		--Procesar los Cheques
		Begin
		
			Declare db_cursor cursor for
			Select D.IDTransaccionChequeCliente
			From DetalleDescuentoCheque D 
			Join ChequeCliente C On D.IDTransaccionChequeCliente=C.IDTransaccion 
			Where IDTransaccionDescuentoCheque=@IDTransaccion
			
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionCheque
			While @@FETCH_STATUS = 0 Begin  
				
				Set @Cancelado  = 'True'
				
				--ACTUALIZAR LA TABLA CHEQUE			
				Update ChequeCliente Set Depositado = 'True',
										Cartera = 'False',
										Rechazado = 'False',
										Cancelado=@Cancelado,
										Descontado = 'True'   
				where IDTransaccion= @vIDTransaccionCheque 

				set @vObservacion = concat ('Se actualiza la tabla ChequeCliente. IdTransaccion:',@IDTransaccion)
				
				Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		        Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@vIDTransaccionCheque,'CHEQUE CLIENTE' )
		        				
				set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @vIDTransaccionCheque)
				
				set @vvNumero = (select Numero from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvIDBanco = (select IDBanco from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvIDCuentaBancaria = (select IDCuentaBancaria from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvNroCheque = (select NroCheque from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvIDMoneda = (select IDMoneda from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvCotizacion = (select Cotizacion from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvImporteMoneda = (select ImporteMoneda from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvDiferido = (select Diferido from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvFechaVencimiento = (select FechaVencimiento from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvChequeTercero = (select ChequeTercero from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvLibrador = (select Librador from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvSaldo = (select Saldo from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvCancelado = (select Cancelado from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvCartera = (select Cartera from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvDepositado = (select Depositado from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvRechazado = (select Rechazado from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvConciliado = (select Conciliado from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvIDMotivoRechazo = (select IDMotivoRechazo from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvSaldoACuenta = (select SaldoACuenta from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvTitular = (select Titular from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvFechaCobranza = (select FechaCobranza from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvDescontado = (select Descontado from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)	
				set @vvIDSucursal = (select IDSucursal from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvIDCliente = (select IDCliente from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvImporte = (select Importe from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)

				Insert Into aChequeCliente(IdAuditoria,IDTransaccion, IDSucursal, Numero, IDCliente, IDBanco, IDCuentaBancaria, Fecha, NroCheque, IDMoneda, Cotizacion, Importe, ImporteMoneda, Diferido, FechaVencimiento, ChequeTercero, Librador, Saldo, SaldoACuenta, Cancelado, Cartera, Depositado, Rechazado,Titular,FechaCobranza, Descontado,IDUsuario,Accion)
		        values(@vvID,@IDTransaccion, @vvIDSucursal, @vvNumero, @vvIDCliente, @vvIDBanco, @vvIDCuentaBancaria, getdate(), @vvNroCheque, @vvIDMoneda, @vvCotizacion, @vvImporte, @vvImporteMoneda, @vvDiferido, @vvFechaVencimiento, @vvChequeTercero, @vvLibrador, @vvSaldo, @vvImporte, @vvCancelado, @vvCartera, @vvDepositado, @vvRechazado,@vvTitular,@vvFechaCobranza, @vvDescontado ,@IDUsuario,'UPD')
	
				
				Fetch Next From db_cursor Into @vIDTransaccionCheque
				
				
			End
			
			set @vObservacion = concat ('Se actualiza la tabla DescuentoCheque. IdTransaccion:',@IDTransaccion, 'Campo: Procesado=TRUE')
				--print @Cancelado
			Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		    Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'DESCUENTO CHEQUE' )
		
			Update DescuentoCheque Set Procesado = 'True' 
			Where IDTransaccion=@IDTransaccion
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		End	
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
			
	End	
	
	If @Operacion = 'DEL' Begin	 
		
		--VALIDAR
		--Que ningun cheque este rechazado
		If Exists(Select * From DetalleDescuentoCheque  D join ChequeCliente C on D.IDTransaccionDescuentoCheque=C.IDTransaccion where D.IDTransaccionDescuentoCheque =@IDTransaccion And  C.Rechazado='True') Begin 
			set @Mensaje = 'El Cheque Esta Rechazado!'
			set @Procesado = 'False'
			return @@rowcount		
		end
		
		--Que ningun cheque este conciliado
		If Exists(Select * From DetalleDescuentoCheque  D join ChequeCliente C on D.IDTransaccionDescuentoCheque=C.IDTransaccion where D.IDTransaccionDescuentoCheque =@IDTransaccion And  C.Conciliado='True') Begin 
			set @Mensaje = 'El Cheque Esta Conciliado!'
			set @Procesado = 'False'
			return @@rowcount
		end
				
		
		--Procesar los Cheques
		Begin
			Declare db_cursor cursor for
			Select D.IDTransaccionChequeCliente 
			From DetalleDescuentoCheque D 
			Join ChequeCliente C On D.IDTransaccionChequeCliente=C.IDTransaccion 
			Where IDTransaccionDescuentoCheque=@IDTransaccion
			
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionCheque
			While @@FETCH_STATUS = 0 Begin
			
				--Descancelar directo
				Set @Cancelado = 'False'  
												
				----ACTUALIZAR LA TABLA CHEQUE
					
				Update ChequeCliente Set Depositado = 'False',
										Cartera = 'True',
										Cancelado=@Cancelado,
										Descontado = 'False'   
				where IDTransaccion= @vIDTransaccionCheque 
				
				set @vObservacion = concat ('Se actualiza la tabla ChequeCliente. IdTransaccion:',@IDTransaccion)
				--print @Cancelado
				Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		        Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@vIDTransaccionCheque,'CHEQUE CLIENTE' )
		
		        set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @vIDTransaccionCheque)
				
				set @vvNumero = (select Numero from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvIDBanco = (select IDBanco from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvIDCuentaBancaria = (select IDCuentaBancaria from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvNroCheque = (select NroCheque from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvIDMoneda = (select IDMoneda from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvCotizacion = (select Cotizacion from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvImporteMoneda = (select ImporteMoneda from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvDiferido = (select Diferido from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvFechaVencimiento = (select FechaVencimiento from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvChequeTercero = (select ChequeTercero from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvLibrador = (select Librador from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvSaldo = (select Saldo from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvCancelado = (select Cancelado from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvCartera = (select Cartera from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvDepositado = (select Depositado from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvRechazado = (select Rechazado from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvConciliado = (select Conciliado from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvIDMotivoRechazo = (select IDMotivoRechazo from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvSaldoACuenta = (select SaldoACuenta from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvTitular = (select Titular from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvFechaCobranza = (select FechaCobranza from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvDescontado = (select Descontado from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)	
				set @vvIDSucursal = (select IDSucursal from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvIDCliente = (select IDCliente from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)
				set @vvImporte = (select Importe from ChequeCliente where IDTransaccion = @vIDTransaccionCheque)

				Insert Into aChequeCliente(IdAuditoria,IDTransaccion, IDSucursal, Numero, IDCliente, IDBanco, IDCuentaBancaria, Fecha, NroCheque, IDMoneda, Cotizacion, Importe, ImporteMoneda, Diferido, FechaVencimiento, ChequeTercero, Librador, Saldo, SaldoACuenta, Cancelado, Cartera, Depositado, Rechazado,Titular,FechaCobranza, Descontado,IDUsuario,Accion)
		        values(@vvID,@IDTransaccion, @vvIDSucursal, @vvNumero, @vvIDCliente, @vvIDBanco, @vvIDCuentaBancaria, getdate(), @vvNroCheque, @vvIDMoneda, @vvCotizacion, @vvImporte, @vvImporteMoneda, @vvDiferido, @vvFechaVencimiento, @vvChequeTercero, @vvLibrador, @vvSaldo, @vvImporte, @vvCancelado, @vvCartera, @vvDepositado, @vvRechazado,@vvTitular,@vvFechaCobranza, @vvDescontado ,@IDUsuario,'UPD')
	
				
				Fetch Next From db_cursor Into @vIDTransaccionCheque
								
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		End	
					
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
	
	
	
	End
	
End
	










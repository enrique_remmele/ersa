﻿CREATE Procedure [dbo].[SpCFNotaDebito]

	--Cuenta Fija
	@IDOperacion tinyint,
	@ID tinyint,
	@IDSucursal tinyint,
	@IDMoneda tinyint,
	@CuentaContable varchar(50),
	@Debe bit,
	@Haber bit,
	@IDTipoComprobante smallint,
	@Descripcion varchar(50),
	@Orden tinyint,
			
	--Cuenta Fija Venta
	@Credito bit,
	@Contado bit,
	@BuscarEnCliente bit,
	@BuscarEnProducto bit,
	@IDTipoCuentaFija tinyint,
	@IDTipoDescuento tinyint = NULL,
	
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
	
As

Begin

	--BLOQUES
	
	Declare	@vID tinyint
	
	--INSERTAR
	if @Operacion='INS' begin
			
		EXEC SpCF @ID = 0,
				@IDOperacion = @IDOperacion,
				@IDSucursal = @IDSucursal,
				@IDMoneda = @IDMoneda,
				@CuentaContable = @CuentaContable,
				@Debe = @Debe,
				@Haber = @Haber,
				@IDTipoComprobante = @IDTipoComprobante,
				@Descripcion = @Descripcion,
				@Orden = @Orden,
				@Operacion = @Operacion,
				@vID = @vID OUTPUT
			
		Insert Into CFNotaDebito(IDOperacion, ID, Credito, Contado, BuscarEnCliente, BuscarEnProducto, IDTipoCuentaFija, IDTipoDescuento)
		Values(@IDOperacion, @vID, @Credito, @Contado, @BuscarEnCliente, @BuscarEnProducto, @IDTipoCuentaFija, @IDTipoDescuento)
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		EXEC SpCF @ID = @ID,
				@IDOperacion = @IDOperacion,
				@IDSucursal = @IDSucursal,
				@IDMoneda = @IDMoneda,
				@CuentaContable = @CuentaContable,
				@Debe = @Debe,
				@Haber = @Haber,
				@IDTipoComprobante = @IDTipoComprobante,
				@Descripcion = @Descripcion,
				@Orden = @Orden,
				@Operacion = @Operacion,
				@vID = @vID OUTPUT
				
		Update CFNotaDebito Set	Credito=@Credito, 
							Contado=@Contado, 
							BuscarEnCliente=@BuscarEnCliente, 
							BuscarEnProducto=@BuscarEnProducto, 
							IDTipoCuentaFija=@IDTipoCuentaFija,
							IDTipoDescuento=@IDTipoDescuento
		Where IDOperacion=@IDOperacion And ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		Delete From CFNotaDebito Where IDOperacion=@IDOperacion And ID=@ID
		Delete From CF Where IDOperacion=@IDOperacion And ID=@ID
		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount
			
End


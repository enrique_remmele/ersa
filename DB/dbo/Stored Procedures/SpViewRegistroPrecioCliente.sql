﻿CREATE Procedure [dbo].[SpViewRegistroPrecioCliente]

	@Año smallint,
	@IDTipoProducto int = 0,
	@IDProducto int = 0,
	@IDTipoCliente int = 0,
	@IDCliente int = 0
	
	
As

Begin
	
		--Crear la tabla temporal
		create table #TablaTemporal(IDCliente int,
									ReferenciaCliente varchar(50),
									Cliente varchar(300),
									IDTipoCliente int,
									IDProducto int,
									ReferenciaProducto varchar(50),
									Producto varchar(300),
									IDTipoProducto int,
									Mes tinyint,
									PrecioUnitario money,
									PrecioUnitarioDiscriminado money,
									Cantidad int,
									CanditadDevolucion int)
					
			Select 
			a.IDCliente,
			a.CodigoCliente,
			a.Cliente,
			a.IDTipoCliente,
			a.IDProducto,
			a.CodigoProducto,
			a.Producto,
			a.IDTipoProducto,
			a.Mes,
			a.PrecioUnitario,
			a.PrecioUnitarioDiscriminado,
			'Cantidad'=Sum(a.Cantidad),
			'Devoluciones'=Sum(a.Devoluciones)
			from (												
			Select 
			V.IDCliente,
			'CodigoCliente'=C.Referencia,
			'Cliente'=C.RazonSocial,
			C.IDTipoCliente,
			dv.IDProducto,
			'CodigoProducto'=P.Referencia,
			'Producto'=P.Descripcion,
			P.IDTipoProducto,
			'Mes'=MONTH(V.FechaEmision),
			'PrecioUnitario'=dv.PrecioUnitario - dv.DescuentoUnitario,
			'PrecioUnitarioDiscriminado'=(dv.PrecioUnitario - dv.DescuentoUnitario)/Imp.FactorDiscriminado,
			'Cantidad'=dv.Cantidad,
			'Devoluciones'=0
			From detalleventa dv
			JOin Venta V on dv.IDTransaccion = V.IDTransaccion
			Join Producto P on dv.IDProducto = P.ID
			join Cliente C on v.IDCliente  = C.ID
			join Impuesto Imp on dv.IDImpuesto = Imp.ID
			Where  year(V.FechaEmision) =@Año
			and V.Anulado = 0 and V.Procesado = 1
			and (Case when @IDProducto = 0 then 0 else P.ID end)=(Case when @IDProducto = 0 then 0 else @IDProducto end)
			and (Case when @IDCliente = 0 then 0 else C.ID end)=(Case when @IDCliente = 0 then 0 else @IDCliente end)
			and (Case when @IDTipoProducto = 0 then 0 else P.IDTipoProducto end)=(Case when @IDTipoProducto = 0 then 0 else @IDTipoProducto end)
			and (Case when @IDTipoCliente = 0 then 0 else C.IDTipoCliente end)=(Case when @IDTipoCliente = 0 then 0 else @IDTipoCliente end)
					
			Union all

			Select 
			NC.IDCliente,
			'CodigoCliente'=C.Referencia,
			'Cliente'=C.RazonSocial,
			C.IDTipoCliente,
			DC.IDProducto,
			'CodigoProducto'=P.Referencia,
			'Producto'=P.Descripcion,
			P.IDTipoProducto,
			'Mes'=MONTH(NC.Fecha),
			'PrecioUnitario'=dC.PrecioUnitario,
			'PrecioUnitarioDiscriminado'=dC.PrecioUnitario/Imp.FactorDiscriminado,
			'Cantidad'=0,
			'Devoluciones'=DC.Cantidad
			From detallenotacredito DC
			JOin NotaCredito NC on DC.IDTransaccion = NC.IDTransaccion
			Join Producto P on DC.IDProducto = P.ID
			join Cliente C on NC.IDCliente  = C.ID
			join Impuesto Imp on dC.IDImpuesto = Imp.ID
			Where  year(NC.Fecha) =@Año
			and NC.Anulado = 0 and NC.Procesado = 1
			and P.ControlarExistencia = 1
			and (Case when @IDProducto = 0 then 0 else P.ID end)=(Case when @IDProducto = 0 then 0 else @IDProducto end)
			and (Case when @IDCliente = 0 then 0 else C.ID end)=(Case when @IDCliente = 0 then 0 else @IDCliente end)
			and (Case when @IDTipoProducto = 0 then 0 else P.IDTipoProducto end)=(Case when @IDTipoProducto = 0 then 0 else @IDTipoProducto end)
			and (Case when @IDTipoCliente = 0 then 0 else C.IDTipoCliente end)=(Case when @IDTipoCliente = 0 then 0 else @IDTipoCliente end)
			) a

			Group by 
			a.IDCliente,
			a.CodigoCliente,
			a.Cliente,
			a.IDTipoCliente,
			a.IDProducto,
			a.CodigoProducto,
			a.Producto,
			a.IDTipoProducto,
			a.Mes,
			a.PrecioUnitario,
			a.PrecioUnitarioDiscriminado

			Order By a.IDCliente, a.IDProducto, a.mes
		
	
End

﻿CREATE Procedure [dbo].[SpAsientoMovimientoConsumoCombustible]

	@IDTransaccion numeric(18,0)
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	
	--Movimiento
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoOperacion smallint
	Declare @vIDTipoComprobante smallint
	Declare @vIDDepositoEntrada smallint
	Declare @vIDDepositoSalida smallint
	Declare @vEntrada bit
	Declare @vSalida bit
	Declare @vComprobante varchar(50)
	Declare @vFecha varchar(50)
	Declare @vObservacion varchar(100)
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money

	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	Declare @vCodigoDepositoEntrada varchar(50)
	Declare @vCodigoDepositoSalida varchar(50)
	Declare @vCodigoProducto varchar(50)

	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money
	Declare @vBuscarEnProducto bit
	Declare @vIDMotivo int
	

	Declare @MovimientoStock bit
	Declare @DescargaStock bit
	
	--Obtener valores
	Begin
	
		Set @vIDMoneda = 1
		Set @vCotizacion = 1
		
		Select	@vIDSucursal=IDSucursal,
				@vIDTipoOperacion=IDTipoOperacion,
				@vTipoComprobante=TipoComprobante,
				@vIDTipoComprobante=IDTipoComprobante,
				@vIDDepositoEntrada=IDDepositoEntrada,
				@vIDDepositoSalida=IDDepositoSalida,
				@vEntrada=Entrada,
				@vSalida=Salida,
				@vComprobante=Comprobante,
				@vFecha=Fecha,
				@vObservacion=Observacion,
				@vIDMotivo = IDMotivo

		From VMovimiento Where IDTransaccion=@IDTransaccion and (MovimientoCombustible = 'True') --and Entrada = 1 and Salida = 1
		print 'Entra Seccion 1'
	End
					
	--Verificar que el asiento se pueda modificar
	Begin
	
		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta anulado
		If (Select Anulado From Movimiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El registro esta anulado'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		print 'Pasa filtros'
	End

	--Eliminar primero el asiento
	Begin
	--Cuentas para Transferencias y Salidas
		If @vSalida=1 Begin
		
			Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
			Delete From Asiento Where IDTransaccion=@IDTransaccion
			
			print 'Asientos Eliminados'	
		end 
		else begin
			goto Salir
		end
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = @vObservacion
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		print 'Inserta Cabecera'
	End
	
	--Cuentas para Transferencias
	If @vEntrada=1 And @vSalida=1 Begin
		
		Set @vImporte = isnull((Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion),0)
		--Debe
		if @vFecha < '20191127' begin
			Set @vCodigo = '11122101'
		end
		else begin 
			Set @vCodigo = (Select isnull(CuentaContableCombustible,'') from Deposito where ID = @vIDDepositoSalida)
		end
		print Concat('cuentacontable depsalida ',@vCodigo)
		
		Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

		Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
		Values(@IDTransaccion, 0, @vIDCuentaContable, @vCodigo, @vImporte, 0, @vImporte, '')
	
		--Haber
		if @vFecha < '20191127' begin
			Set @vCodigo = '11122101'
		end
		else begin 
			Set @vCodigo = (Select isnull(CuentaContableCombustible,'') from Deposito where ID = @vIDDepositoEntrada)
		end
		print Concat('cuentacontable depentrada ',@vCodigo)
		Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

		Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
		Values(@IDTransaccion, 1, @vIDCuentaContable, @vCodigo, 0, @vImporte, @vImporte, '')

	End
	
	--CUentas para Salidas
	If @vEntrada=0 And @vSalida=1 Begin
		
		Set @vImporte = isnull((Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion),0)
		
		--Debe
		
		Set @vCodigo = (Select CodigoCuentaContable from MotivoMovimiento where ID = @vIDMotivo)
		Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

		Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
		Values(@IDTransaccion, 0, @vIDCuentaContable, @vCodigo, 0, @vImporte, 0, '')
	
		--Haber
		Set @vCodigo = (Select  (Case when isnull(CuentaContableCombustible,'') = '' then CuentaContableMercaderia else CuentaContableCombustible end) from Deposito where ID = @vIDDepositoSalida)
		print Concat('Codigo, IDDepositoSalida ', @vCodigo,', ', @vIDDepositoSalida)
		Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

		Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
		Values(@IDTransaccion, 1, @vIDCuentaContable, @vCodigo, @vImporte, 0, @vImporte, '')

	End

	--Actualizamos la cabecera, el total
	Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
	Set @vImporteHaber = ROUND(@vImporteHaber, 0)
	Set @vImporteDebe = ROUND(@vImporteDebe, 0)
	
	Update Asiento Set Total = @vImporteHaber,
						Credito = @vImporteHaber,
						Debito = @vImporteDebe,
						Saldo = @vImporteHaber - @vImporteDebe
	Where IDTransaccion=@IDTransaccion
	
	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
	
End


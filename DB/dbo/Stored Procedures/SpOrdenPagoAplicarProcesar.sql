﻿CREATE Procedure [dbo].[SpOrdenPagoAplicarProcesar]

	--Entrada
	@IDTransaccion numeric(18),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

    ---Variables
    Declare @vIDTransaccionEgreso numeric(18,0)
    Declare @vIDTransaccionEfectivo numeric(18,0)
    Declare @vIDSucursal int
	Declare @vIDEfectivo tinyint
    Declare @vImporte money
    Declare @vSaldo money
    Declare @vCancelado bit
    Declare @vTotal money
	Declare @vRetencionIVA money
    Declare @vTotalOP money
    Declare @vEsCuota bit
	Declare @vCuota smallint

	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'

	Set @vTotal = 0
	
	If @Operacion = 'UPD' Begin
		
		--Saldar COMPRAS
		If Exists(Select * From OrdenPagoEgreso OP JOin Compra C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion And Procesado='False') Begin
			Begin
				Declare db_cursor cursor for
				Select OP.IDTransaccionEgreso, OP.Importe, C.Saldo, C.Cancelado, C.Total From OrdenPagoEgreso OP JOin Compra C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion
				Open db_cursor   
				Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte, @vSaldo, @vCancelado, @vTotal
				While @@FETCH_STATUS = 0 Begin  
					
					--Calculamos el Saldo
					Set @vSaldo = @vSaldo - @vImporte
					If @vSaldo < 0 Begin
						Set @vSaldo = @vSaldo * -1
					End
					
					--Cancelar si saldo = 0
					If @vSaldo = 0 Begin
						Set @vCancelado = 'True'
					End
					
					--Actualizar
					Update Compra Set Saldo = @vSaldo, Cancelado = @vCancelado
					Where IDTransaccion = @vIDTransaccionEgreso
				
					--Establecemos procesado el egreso
					Update OrdenPagoEgreso Set Procesado = 'True',  ProcesaRetencion = 'False'
					Where IDTransaccionOrdenPago=@IDTransaccion and IDTransaccionEgreso=@vIDTransaccionEgreso
					
					Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte, @vSaldo, @vCancelado, @vTotal
									
				End
				
				--Cierra el cursor
				Close db_cursor   
				Deallocate db_cursor		   			
					
				set @Mensaje = 'Registro guardado!'
				set @Procesado = 'True'
				
			End
		End
		
		--Saldar Gastos
		If Exists(Select * From OrdenPagoEgreso OP Join Gasto C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion And Procesado='False') Begin
			Begin
				Declare db_cursor cursor for
				Select OP.IDTransaccionEgreso, OP.Importe, C.Saldo, C.Cancelado, C.Total, 'EsCuota'=Case When (IsNull(C.Cuota, 1))>1 Then 'True' Else 'False' End, OP.Cuota, C.RetencionIVA, C.IDSucursal From OrdenPagoEgreso OP JOin Gasto C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion
				Open db_cursor   
				Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte, @vSaldo, @vCancelado, @vTotal, @vEsCuota, @vCuota, @vRetencionIVA, @vIDSucursal
				While @@FETCH_STATUS = 0 Begin  
					
					--Calculamos el Saldo
					--NO SE INCLUYEN RETENCIONES!!!!
					--If @vTotal >= (Select Top(1) CompraImporteMinimoRetencion From Configuraciones Where IDSucursal=@vIDSucursal) Begin
					--	Set @vSaldo = @vSaldo - (@vImporte + @vRetencionIVA)
					--End Else Begin
					--	Set @vSaldo = @vSaldo - @vImporte
					--End

					Set @vSaldo = @vSaldo - @vImporte

					If @vSaldo < 0 Begin
						Set @vSaldo = @vSaldo * -1
					End
					
					--Cancelar si saldo = 0
					If @vSaldo < 1 Begin
						Set @vCancelado = 'True'
					End
					
					--Actualizar
					Update Gasto Set Saldo = @vSaldo, 
									Cancelado = @vCancelado,
									Pagar = 'False'
					Where IDTransaccion = @vIDTransaccionEgreso
					
					--Actualizar Detalle Fondo Fijo Gasto Fondo Fijo
					Update DetalleFondoFijo Set Cancelado= 'True'
					Where IDTransaccionGasto=@vIDTransaccionEgreso 
				
					--Establecemos procesado el egreso
					Update OrdenPagoEgreso Set Procesado = 'True', ProcesaRetencion = 'False'
					Where IDTransaccionOrdenPago=@IDTransaccion and IDTransaccionEgreso=@vIDTransaccionEgreso
					
					--Si es un pago de cuota
					If @vEsCuota = 'True' Begin
						Update Cuota Set Saldo=Saldo-@vImporte,
										 Cancelado=Case When(Saldo-@vImporte) < 1 Then 'True' Else 'False' End,
										 Pagar='False'
						Where IDTransaccion=@vIDTransaccionEgreso And ID=@vCuota  
					End
						
					Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte, @vSaldo, @vCancelado, @vTotal, @vEsCuota, @vCuota, @vRetencionIVA, @vIDSucursal
									
				End
				
				--Cierra el cursor
				Close db_cursor   
				Deallocate db_cursor		   			
					
				set @Mensaje = 'Registro guardado!'
				set @Procesado = 'True'
				
			End
		End	
		
		--Cambiar Estado Vale
		If Exists(Select * From OrdenPagoEgreso OP Join Vale C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion And Procesado='False') Begin
			Begin
				Declare db_cursor cursor for
				Select OP.IDTransaccionEgreso From OrdenPagoEgreso OP JOin Vale C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion
				Open db_cursor   
				Fetch Next From db_cursor Into @vIDTransaccionEgreso
				While @@FETCH_STATUS = 0 Begin  
				
					--Actualizar Vale
					Update Vale Set Pagado = 'True',Cancelado='True', Pagar='False'
					Where IDTransaccion = @vIDTransaccionEgreso
					
					--Actualizar Detalle Fondo Fijo Vale
					Update DetalleFondoFijo Set Cancelado= 'True'
					Where IDTransaccionVale=@vIDTransaccionEgreso 
					
								
					--Establecemos procesado el egreso
					Update OrdenPagoEgreso Set Procesado = 'True', ProcesaRetencion = 'False'
					Where IDTransaccionOrdenPago=@IDTransaccion and IDTransaccionEgreso=@vIDTransaccionEgreso
						
					Fetch Next From db_cursor Into @vIDTransaccionEgreso
									
				End
				
				--Cierra el cursor
				Close db_cursor   
				Deallocate db_cursor		   			
					
				set @Mensaje = 'Registro guardado!'
				set @Procesado = 'True'
				
			End
		End	
		
	End
	
	
End
	





﻿
CREATE Procedure [dbo].[SpAutorizacionCancelarAutomatico]
	--Entrada
	@IDTransaccionPedido as numeric(18,0),
	@Valor varchar(20),
	@IDUsuario int
As
Begin
  if @IDTransaccionPedido <> 0 and @valor = 'APROBAR' begin		
		----Actualizar
		Update Pedido 
		Set Aprobado= 1,
		IDUsuarioAprobacion = @IDUsuario,
		FechaAprobacion = getdate()
		Where IDTransaccion = @IDTransaccionPedido
		and Facturado = 0
			
  end

  if @IDTransaccionPedido <> 0 and @valor = 'RECHAZAR' begin	
		Update Pedido 
		Set Aprobado= 0,
		IDUsuarioAprobacion = @IDUsuario,
		FechaAprobacion = getdate()
		Where IDTransaccion = @IDTransaccionPedido
		and Facturado = 0
  end

  if @IDTransaccionPedido <> 0 and @valor = 'RECUPERAR' begin	
		Update Pedido 
		Set Aprobado= null,
		IDUsuarioAprobacion = null,
		FechaAprobacion = null
		Where IDTransaccion = @IDTransaccionPedido
		and Facturado = 0
  end
	
End



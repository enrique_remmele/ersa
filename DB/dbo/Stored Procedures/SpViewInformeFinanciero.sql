﻿CREATE Procedure [dbo].[SpViewInformeFinanciero]

	--Entrada
	@Desde date,
	@Hasta date,
	@IDSucursal as int
	
As

Begin

	--variables
	Declare @vIDSucursal int
	Declare @vIDMoneda int
	Declare @Fecha date = @desde
	
	--Crear la tabla
	Create table #InformeFinanciero(IDSucursal int,
						IDMoneda int,
						Fecha date,
						ChequesDiferidosVencidosHoy money,
						DocumentosCobradosCredito money,
						DocumentosCobradosContado money,
						VentasDelDia money,
						NCDelDia money,
						ChequesDiferidosDelDia money,
						VentasDelDiaSinIVA money,
						NCDelDiaSinIVA money
								)
								
IF @IDSucursal = 0 Begin
	Declare db_cursor1 cursor for
		Select S.ID, M.ID as IDMoneda
		from Sucursal S, Moneda M 
		order by S.ID
	Open db_cursor1   
	Fetch Next From db_cursor1 Into	@vIDSucursal, @vIDMoneda
	While @@FETCH_STATUS = 0 Begin 
	    set @fecha = @Desde
		while  (@Fecha <= @Hasta) begin
			--Cheques Diferidos Vencidos del dia 
			declare @vVencidosDiferidosDelDia as money
			set @vVencidosDiferidosDelDia = Isnull((Select Sum(ImporteMoneda) from vchequeCliente 
			where IdSucursal = @vIDSucursal and IDMoneda = @vIDMoneda and FechaVencimiento = @Fecha and Diferido = 1),0)

			--Documentos Cobrados Credito del dia
			declare @vDocumentosCredito as money
			set @vDocumentosCredito = Isnull((select sum(Cobrado) from VDetalleDocumentosCobrados 
			where idsucursalOperacion = @vIDSucursal and Fecha = @Fecha and IdMoneda = @vIDMoneda and Credito = 1),0)

			--Documentos Cobrados Contado del dia
			declare @vDocumentosContado as money
			set @vDocumentosContado = Isnull((select sum(Cobrado) from VDetalleDocumentosCobrados 
			where idsucursalOperacion = @vIDSucursal and Fecha = @Fecha and IdMoneda = @vIDMoneda and Credito = 0),0)

			--Ventas del dia
			declare @VentasDelDia as money
			set @VentasDelDia = Isnull((select sum(VD.total) from VDetalleVentaConDevolucion VD
			Join VVenta V on V.IdTransaccion = VD.IDTransaccion
			where V.IDSucursal = @vIDSucursal 
			and V.IDMoneda = @vIDMoneda 
			and V.FechaEmision=@Fecha 
			and V.Anulado = 0 
			and V.Credito = 0
			and V.CancelarAutomatico = 'False'
			and V.IDFormaPagoFactura in (1,6)),0)

			--Ventas del dia SIN IVA
			declare @VentasDelDiaSinIVA as money
			set @VentasDelDiaSinIVA = Isnull((select sum(VD.totalDiscriminado) from VDetalleVentaConDevolucion VD
			Join VVenta V on V.IDTransaccion = VD.IDTransaccion 
			where V.IDSucursal = @vIDSucursal 
			and V.IDMoneda = @vIDMoneda 
			and V.FechaEmision=@Fecha 
			and V.Anulado = 0
			and V.CancelarAutomatico = 'False'),0)
			--and IDFormaPagoFactura in (1,6)),0)

			--NC del dia
			declare @NotaCreditoDelDia as money
			set @NotaCreditoDelDia = Isnull((select sum(total) from NotaCredito where IDSucursal = @vIDSucursal and IDMoneda = @vIDMoneda and Fecha=@Fecha and Anulado = 0),0)

			--NC del dia SIN IVA
			declare @NotaCreditoDelDiaSinIva as money
			set @NotaCreditoDelDiaSinIva = Isnull((select sum(totalDiscriminado) from NotaCredito where IDSucursal = @vIDSucursal and IDMoneda = @vIDMoneda and Fecha=@Fecha and Anulado = 0),0)

			--Cheques Diferidos Vencidos del dia 
			declare @vDiferidosDelDia as money
			set @vDiferidosDelDia = Isnull((Select Sum(ImporteMoneda) from vchequeCliente where IdSucursal = @vIDSucursal and IDMoneda = @vIDMoneda and fecha = @Fecha and Diferido = 1),0)

			Insert Into #InformeFinanciero	
			Values(@vIDSucursal, @vIDMoneda, @Fecha,@vVencidosDiferidosDelDia,@vDocumentosCredito,@vDocumentosContado,@VentasDelDia,@NotaCreditoDelDia,@vDiferidosDelDia, @VentasDelDiaSinIVA, @NotaCreditoDelDiaSinIva)
			set @Fecha = DATEADD(day, 1, @Fecha)
		End --Fin de fechas
		
Siguiente:
		
		Fetch Next From db_cursor1 Into	@vIDSucursal, @vIDMoneda
		
	End

	--Cierra el cursor
	Close db_cursor1   
	Deallocate db_cursor1	
End	

IF @IDSucursal <> 0 Begin
	Declare db_cursor1 cursor for
		Select S.ID, M.ID as IDMoneda
		from Sucursal S, Moneda M 
		Where S.ID = @IDSucursal
		order by S.ID
	Open db_cursor1   
	Fetch Next From db_cursor1 Into	@vIDSucursal, @vIDMoneda
	While @@FETCH_STATUS = 0 Begin 
	    set @fecha = @Desde
		while  (@Fecha <= @Hasta) begin
			--Cheques Diferidos Vencidos del dia 
			set @vVencidosDiferidosDelDia = 0
			set @vVencidosDiferidosDelDia = Isnull((Select Sum(ImporteMoneda) from vchequeCliente 
			where IdSucursal = @vIDSucursal and IDMoneda = @vIDMoneda and FechaVencimiento = @Fecha and Diferido = 1),0)

			--Documentos Cobrados Credito del dia
			set @vDocumentosCredito = 0
			set @vDocumentosCredito = Isnull((select sum(Cobrado) from VDetalleDocumentosCobrados 
			where idsucursalOperacion = @vIDSucursal and Fecha = @Fecha and IdMoneda = @vIDMoneda and Credito = 1),0)

			--Documentos Cobrados Contado del dia
			set @vDocumentosContado = 0
			set @vDocumentosContado = Isnull((select sum(Cobrado) from VDetalleDocumentosCobrados 
			where idsucursalOperacion = @vIDSucursal and Fecha = @Fecha and IdMoneda = @vIDMoneda and Credito = 0),0)
			print concat('Sucursal ',@vIDSucursal, ' Fecha ', @Fecha, ' MOneda ')

			--Ventas del dia
			set @VentasDelDia = 0
			set @VentasDelDia = Isnull((select sum(VD.total) from VDetalleVentaConDevolucion VD 
			Join VVenta V on V.IDTransaccion = VD.IDTransaccion
			where V.IDSucursal = @vIDSucursal 
			and V.IDMoneda = @vIDMoneda 
			and V.FechaEmision=@Fecha 
			and V.Anulado = 0 
			and V.Credito = 0
			and V.CancelarAutomatico = 'False'
			and V.IDFormaPagoFactura in (1,6)),0)

			--Ventas del dia SIN IVA
			set @VentasDelDiaSinIVA = 0
			set @VentasDelDiaSinIVA = Isnull((select sum(VD.totalDiscriminado) from VDetalleVentaConDevolucion VD
			Join VVenta V on V.IDTransaccion = VD.IDTransaccion 
			where V.IDSucursal = @vIDSucursal 
			and V.IDMoneda = @vIDMoneda 
			and V.FechaEmision=@Fecha 
			and V.Anulado = 0
			and V.CancelarAutomatico = 'False'),0)
			--and IDFormaPagoFactura in (1,6)),0)

			--NC del dia
			set @NotaCreditoDelDia = 0
			set @NotaCreditoDelDia = Isnull((select sum(total) from NotaCredito where IDSucursal = @vIDSucursal and IDMoneda = @vIDMoneda and Fecha=@Fecha and Anulado = 0),0)

			--NC del dia SIN IVA
			set @NotaCreditoDelDiaSinIva = 0
			set @NotaCreditoDelDiaSinIva = Isnull((select sum(totalDiscriminado) from NotaCredito where IDSucursal = @vIDSucursal and IDMoneda = @vIDMoneda and Fecha=@Fecha and Anulado = 0),0)

			--Cheques Diferidos Vencidos del dia 
			set @vDiferidosDelDia = 0
			set @vDiferidosDelDia = Isnull((Select Sum(ImporteMoneda) from vchequeCliente where IdSucursal = @vIDSucursal and IDMoneda = @vIDMoneda and fecha = @Fecha and Diferido = 1),0)

			Insert Into #InformeFinanciero	
			Values(@vIDSucursal, @vIDMoneda, @Fecha,@vVencidosDiferidosDelDia,@vDocumentosCredito,@vDocumentosContado,@VentasDelDia,@NotaCreditoDelDia,@vDiferidosDelDia, @VentasDelDiaSinIVA, @NotaCreditoDelDiaSinIva)
			set @Fecha = DATEADD(day, 1, @Fecha)
		End --Fin de fechas
		
Siguiente1:
		
		Fetch Next From db_cursor1 Into	@vIDSucursal, @vIDMoneda
		
	End

	--Cierra el cursor
	Close db_cursor1   
	Deallocate db_cursor1	
End	   		
	
	
	Select S.Codigo, S.Descripcion, M.Referencia, #InformeFinanciero.* From #InformeFinanciero 
	Join Sucursal S on S.ID = #InformeFinanciero.IDSucursal
	Join Moneda M on M.ID = #InformeFinanciero.IDMoneda
	where (ChequesDiferidosVencidosHoy + DocumentosCobradosContado + 
	DocumentosCobradosCredito +	VentasDelDia + NCDelDia + ChequesDiferidosDelDia)<>0
	Order by IDSucursal, IDMoneda, Fecha
End





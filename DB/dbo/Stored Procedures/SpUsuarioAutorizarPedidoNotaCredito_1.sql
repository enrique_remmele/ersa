﻿CREATE Procedure [dbo].[SpUsuarioAutorizarPedidoNotaCredito]

	--Entrada
	@ID tinyint,
	@IDUsuarioAutorizar varchar(50),
	@IDSubMotivo varchar(5),
	@MontoLimite decimal(18,2) = 0,
	@PorcentajeLimite decimal(18,2) = 0,
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--INSERTAR
	if @Operacion='INS' begin
		
		if exists(Select * from UsuarioAutorizarPedidoNotaCredito where idusuario = @IDUsuarioAutorizar and IDSubMotivo = @IDSubMotivo) begin
			 set @Mensaje = 'La configuracion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Obtenemos el nuevo ID
		declare @vID tinyint
		set @vID = (Select IsNull((Max(ID)+1), 1) From UsuarioAutorizarPedidoNotaCredito)

		--Insertamos
		Insert Into UsuarioAutorizarPedidoNotaCredito(ID, IDUsuario, IDSubMotivo, MontoLimite, PorcentajeLimite, Estado)
		Values(@vID, @IDUsuarioAutorizar, @IDSubMotivo,@MontoLimite, @PorcentajeLimite, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='UsuarioAutorizarPedidoNotaCredito', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
				 		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From UsuarioAutorizarPedidoNotaCredito Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		if exists(Select * from UsuarioAutorizarPedidoNotaCredito where idusuario = @IDUsuarioAutorizar and IDSubMotivo = @IDSubMotivo and ID != @ID) begin
			 set @Mensaje = 'La configuracion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update UsuarioAutorizarPedidoNotaCredito Set IDUsuario=@IDUsuarioAutorizar,
						IDSubMotivo =@IDSubMotivo,
						MontoLimite = @MontoLimite,
						PorcentajeLimite = @PorcentajeLimite,
						 Estado =@Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='UsuarioAutorizarPedidoNotaCredito', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From UsuarioAutorizarPedidoNotaCredito Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
				
		--Eliminamos
		Delete From UsuarioAutorizarPedidoNotaCredito 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='UsuarioAutorizarPedidoNotaCredito', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


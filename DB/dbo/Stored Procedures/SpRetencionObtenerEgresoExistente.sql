﻿CREATE Procedure [dbo].[SpRetencionObtenerEgresoExistente]

	--Entrada
	@Comprobante varchar(15),
	@IDProveedor int,
	@Fecha date

As

Begin
	
	Declare @vMensaje varchar(100) = 'Sin procesar'
	Declare @vProcesado bit = 'False'
	Declare @vIDTransaccion numeric(18, 0)

	Set @vIDTransaccion = IsNull((Select Top(1) IDTransaccion 
									From VEgresos 
									Where IDProveedor=@IDProveedor
									And Comprobante=@Comprobante
									And Fecha=@Fecha), 0)
									
	If @vIDTransaccion = 0 Begin
		Set @vMensaje = 'El sistema no encontro el comprobante del proveedor! El Proveedor/Comprobante/Fecha deben coincidir con un comprobante existente.'
		Set @vProcesado = 'False'
		GoTo Salir
	End

	Set @vMensaje = 'Registro procesado'
	Set @vProcesado = 'True'

Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado, 'IDTransaccion'=@vIDTransaccion
End
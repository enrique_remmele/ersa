﻿CREATE Procedure [dbo].[SpDetalleRendicionLoteCobranzaExtra]

	--Entrada
	@IDTransaccion numeric(18,0),
	@ID tinyint = NULL,
	@Cliente varchar(50) = NULL,
	@Comprobante varchar(50) = NULL,
	@Importe money = NULL,

	--Operacion
	@Operacion varchar(10),
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
As

Begin
	
	--Procesar Saldos de Ventas
	If @Operacion = 'INS' Begin
	
		--Validar
		--Asegurarse que exista la transaccion
		if Not Exists(Select * From RendicionLote Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra el registro!'
			Set @Procesado = 'False'
			Return @@rowcount
		End
				
		--Insertar
		Insert Into DetalleRendicionLoteCobranzaExtra(IDTransaccion, ID, Cliente, Comprobante, Importe)
		Values(@IDTransaccion, @ID, @Cliente, @Comprobante, @Importe)
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		
		Return @@rowcount
		
	End
	
	If @Operacion = 'ANULAR' Begin
		
		--Validar
		--Que exista el registro
		if Not Exists(Select * From RendicionLote Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra el registro!'
			Set @Procesado = 'False'
			Return @@rowcount
		End
	
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		Return @@rowcount
		
	End
	
	If @Operacion = 'DEL' Begin
		
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'false'
		Return @@rowcount
		
	End
		
	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'
	
End




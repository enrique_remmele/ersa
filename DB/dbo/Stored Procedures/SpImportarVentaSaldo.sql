﻿CREATE Procedure [dbo].[SpImportarVentaSaldo]

	--Entrada
	--Punto de Expedicion
	@TipoComprobante varchar(10),
	@ReferenciaSucursal varchar(5),
	@ReferenciaPuntoExpedicion varchar(5),
	@NroComprobante varchar(50),
	
	--Cliente
	@Cliente varchar(50),
	@RUC varchar(50),
	
	--Totales
	@Total money,
	@Saldo money,
	
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
		
	@Actualizar bit
	
As

Begin
		
	--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		
		Declare @vOperacion varchar(50)
		Declare @vIDTransaccion int
		Declare @vComprobante varchar(50)
		Declare @vCobrado money
		
	End
	
	--Establecer valores predefinidos
	Begin
		
		Set @vOperacion = 'INS'
		Set @vIDTransaccion = 0
		Set @vCobrado = @Total - @Saldo
		
		--Comprobante
		Set @ReferenciaSucursal = '0' + dbo.FFormatoDosDigitos(CONVERT(tinyint, @ReferenciaSucursal))
		Set @ReferenciaPuntoExpedicion = '0' + dbo.FFormatoDosDigitos(CONVERT(tinyint, @ReferenciaPuntoExpedicion))
		Set @vComprobante = @ReferenciaSucursal + '-' + @ReferenciaPuntoExpedicion + '-' + @NroComprobante
				
	End
	
	--Verificar si ya existe
	If Exists(Select * From VentaImportada VI  Join Venta V On VI.IDTransaccion= V.IDTransaccion Where V.Comprobante=@vComprobante) Begin
		set @vIDTransaccion = (Select VI.IDTransaccion  From VentaImportada VI  Join Venta V On VI.IDTransaccion= V.IDTransaccion Where V.Comprobante=@vComprobante)
		Set @vOperacion = 'UPD'
	End
	
	--ACTUALIZAR
	If @vOperacion = 'UPD' Begin
	
		If @Actualizar = 'True' Begin

			Set @vCobrado = @Total - @Saldo
			
			Update VentaImportada Set	Importe=@Total,
										Cobrado=@vCobrado,
										Saldo=@Saldo
			Where IDTransaccion=@vIDTransaccion

			Set @vMensaje = 'Actualizado!'
			Set @vProcesado = 'True'
			GoTo Salir

		End
		
		Set @vMensaje = 'Sin Act.!'
		Set @vProcesado = 'True'
		GoTo Salir
		
	End
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End

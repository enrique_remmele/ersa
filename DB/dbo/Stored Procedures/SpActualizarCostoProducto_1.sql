﻿
CREATE Procedure [dbo].[SpActualizarCostoProducto]

	--Entrada
	@ID int,
	@Costo money,
	@IDImpuesto tinyint = 0, 
	@IDTransaccion numeric(18,0),
	
	@Operacion varchar(10)
	
As

Begin

	Declare @vUltimoCosto money = 0
	Declare @vUltimoCostoSinIVA money = 0
	Declare @vCostoCG money = 0
	Declare @vCostoPromedio money = 0
	Declare @vImpuesto decimal(10,2)
	Declare @vUltimaEntrada datetime
	Declare @vCantidad decimal(10,2)
	Declare @vFecha datetime
	Declare @Cotizacion int
	
	Set @vImpuesto = (Select FactorDiscriminado From Impuesto Where ID=@IDImpuesto)


	--Bloques de Compra
	if @Operacion = 'INS' Begin
		set @Cotizacion = (select ISNULL(Cotizacion,1) from Compra where IDTransaccion = @IDTransaccion)
		Set @vFecha = (Select Top(1) FechaEntrada From VDetalleCompra Where IDTransaccion=@IDTransaccion)
		Set @vCantidad = (Select SUM(Cantidad) From VDetalleCompra Where IDProducto=@ID and IDTransaccion=@IDTransaccion)
		Set @vUltimoCosto = @Costo * @Cotizacion
		Set @vUltimoCostoSinIVA = Round(@Costo / @vImpuesto, 0) * @Cotizacion
		--Set @vCostoPromedio =(Select Top(1) ISNULL(CostoPromedio,0) from Kardex Where IDProducto=@ID and Indice = (select max(indice) from kardex where idproducto = @id))
		Set @vCostoPromedio = (Select top(1) ISNULL(CostoPromedio,0) from kardex where IDProducto = @ID order by indice desc)
		
		if @vCostoPromedio = 0 begin
			Set @vCostoPromedio = (Select AVG(((PrecioUnitario / @vImpuesto) * ISnull(C.Cotizacion,1))) From DetalleCompra join compra C on C.IDTransaccion = DetalleCompra.IDTransaccion Where IDProducto=@ID)
		end
		
		Set @vCostoCG = @vCostoPromedio
				
		Update Producto Set UltimaEntrada=@vFecha,
							CantidadEntrada=@vCantidad,
							UltimoCosto=@vUltimoCosto,
							CostoSinIVA=@vUltimoCostoSinIVA,
							CostoCG=@vCostoCG,
							CostoPromedio=@vCostoPromedio							
		Where ID=@ID
		
	End
	

	if @Operacion = 'DEL' Begin
	
		--Seleccionar el ultimo producto comprado
		Declare @vIDTransaccion numeric(18,0)

		Set @vFecha = (Select Max(FechaEntrada) From VDetalleCompra Where IDProducto=@ID)
		Set @vIDTransaccion = (Select MAX(IDTransaccion) From VDetalleCompra Where IDProducto=@ID And FechaEntrada Between DATEADD(DAY,-1, @vFecha) And GETDATE())
		set @Cotizacion = (select ISNULL(Cotizacion,1) from Compra where IDTransaccion = @vIDTransaccion)
		Set @vCantidad = (Select SUM(Cantidad) From VDetalleCompra Where IDProducto=@ID and IDTransaccion=@vIDTransaccion)
				
		Set @Costo = (Select IsNull((Select Top(1) PrecioUnitario From VDetalleCompra Where IDProducto=@ID And FechaEntrada=@vFecha Order By 1 Desc),0))
		
		Set @vUltimoCosto = @Costo * @Cotizacion
		If @Costo != 0 Begin
			Set @vUltimoCostoSinIVA = Round(@Costo / @vImpuesto, 0) * @Cotizacion
		End
		
		Set @vCostoPromedio = (Select AVG(((PrecioUnitario / @vImpuesto) * ISNUll(C.Cotizacion,1))) From DetalleCompra Join Compra C on C.IDTransaccion = DetalleCompra.IDTransaccion Where IDProducto=@ID)
		Set @vCostoCG = @vCostoPromedio

		print concat('Costo: ',@Costo)
		print concat('Cotizacion: ',@Cotizacion)
		print concat('Ultimo: ',@vUltimoCosto)
		
		Update Producto Set UltimaEntrada=@vFecha,
							CantidadEntrada=@vCantidad,
							UltimoCosto=@vUltimoCosto,
							CostoSinIVA=@vUltimoCostoSinIVA,
							CostoCG=@vCostoCG,
							CostoPromedio=@vCostoPromedio
		Where ID=@ID
		
	End

	--Bloques de Ticket de Bascula
	if @Operacion = 'INSTICKET' Begin
		set @Cotizacion = (select ISNULL(Cotizacion,1) from TicketBascula where IDTransaccion = @IDTransaccion)
		Set @vFecha = (Select Top(1) Fecha From TicketBascula Where IDTransaccion=@IDTransaccion)
		Set @vCantidad = (Select SUM(PesoBascula) From TicketBascula Where IDProducto=@ID and IDTransaccion=@IDTransaccion)
		Set @vUltimoCosto = @Costo* @Cotizacion
		Set @vUltimoCostoSinIVA = Round(@Costo / @vImpuesto, 0)* @Cotizacion
		Declare @vCostoAdicional money = (Select Isnull(sum(ImporteGs/@vCantidad),0) from TicketBasculaGastoAdicional where IDTransaccion = @IDTransaccion) 
		--Set @vCostoPromedio = (Select AVG(((PrecioUnitario + @vCostoAdicional) / @vImpuesto)) From TicketBascula Where IDProducto=@ID and Anulado = 0)
		Set @vCostoPromedio = (Select top(1) CostoPromedio from kardex where IDProducto = @ID order by indice desc)
		Set @vCostoCG = @vCostoPromedio
		
		Update Producto Set UltimaEntrada=@vFecha,
							CantidadEntrada=@vCantidad,
							UltimoCosto=@vUltimoCosto,
							CostoSinIVA=@vUltimoCostoSinIVA,
							CostoCG=@vCostoCG,
							CostoPromedio=@vCostoPromedio							
		Where ID=@ID
		
	End
	

	if @Operacion = 'DELTICKET' Begin
	
		Set @vFecha = (Select Max(Fecha) From TicketBascula Where IDProducto=@ID)
		Set @vIDTransaccion = (Select MAX(IDTransaccion) From TicketBascula Where IDProducto=@ID And Fecha Between DATEADD(DAY,-1, @vFecha) And GETDATE())
		Set @vCantidad = (Select SUM(PesoBascula) From TicketBascula Where IDProducto=@ID and IDTransaccion=@vIDTransaccion)
		Set @Costo = (Select IsNull((Select Top(1) PrecioUnitario From TicketBascula Where IDProducto=@ID And Fecha=@vFecha Order By 1 Desc),0))
		
		Set @vUltimoCosto = @Costo
		If @Costo != 0 Begin
			Set @vUltimoCostoSinIVA = Round(@Costo / @vImpuesto, 0)
		End
		set @vCostoAdicional = (Select Isnull(sum(ImporteGs)/@vCantidad,0) from TicketBasculaGastoAdicional where IDTransaccion = @IDTransaccion) 
		Set @vCostoPromedio = (Select AVG(((PrecioUnitario + @vCostoAdicional) / @vImpuesto)) From TicketBascula Where IDProducto=@ID)
		Set @vCostoCG = @vCostoPromedio

		print concat('Costo: ',@Costo)
		print concat('Cotizacion: ',@Cotizacion)
		print concat('Ultimo: ',@vUltimoCosto)
		
		Update Producto Set UltimaEntrada=@vFecha,
							CantidadEntrada=@vCantidad,
							UltimoCosto=@vUltimoCosto,
							CostoSinIVA=@vUltimoCostoSinIVA,
							CostoCG=@vCostoCG,
							CostoPromedio=@vCostoPromedio
		Where ID=@ID
		
	End
	
	--Bloques Produccion
	if @Operacion = 'PRODUCCION' begin
		
		Set @vFecha = (Select Top(1) Fecha From Movimiento Where IDTransaccion=@IDTransaccion)
		Set @vCantidad = (Select SUM(Cantidad) From DetalleMovimiento Where IDProducto=@ID and IDTransaccion=@IDTransaccion)
		Set @vCostoPromedio = (Select top(1) ISNULL(CostoPromedio,0) from kardex where IDProducto = @ID order by indice desc)
		if @vCostoPromedio = 0 begin
			Set @vCostoPromedio = @Costo
		end
		
		Update Producto Set UltimaEntrada=@vFecha,
							CantidadEntrada=@vCantidad,
							UltimoCosto=@Costo,
							CostoSinIVA=0,
							CostoCG=@vCostoPromedio,
							CostoPromedio=@vCostoPromedio							
		Where ID=@ID
		

	end

End


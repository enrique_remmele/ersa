﻿CREATE Procedure [dbo].[SpNotaDebitoActualizarStock]

	--Entrada
	@IDTransaccion numeric(18),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	--Variable
	Declare @vIDDeposito tinyint
	Declare @vIDProducto int
	Declare @vCantidad decimal(10,2)
	
	--Variables Kardex
	Declare @vFecha datetime
	Declare @vIDSucursal integer
	Declare @vNroComprobante varchar(50)
	Declare @vCosto decimal(10,2)
	Declare @vTotalDiscriminado money
	Declare @vID int

	Set @vFecha = (Select Fecha From NotaDebito Where IDTransaccion = @IDTransaccion)
	Set @vIDSucursal = (Select IDSucursal From Deposito Where ID = @vIDDeposito)
	Set @vNroComprobante = (Select NroComprobante From NotaDebito Where IDTransaccion = @IDTransaccion)


	Set @Mensaje = ''
	Set @Procesado = 'False'
	
	If @Operacion = 'INS' Begin
				
		Declare db_cursor cursor for
		Select DNC.IDProducto, DNC.IDDeposito, DNC.ID, DNC.Cantidad From DetalleNotaDebito DNC Join Producto P On DNC.IDProducto=P.ID Where DNC.IDTransaccion=@IDTransaccion And P.ControlarExistencia='True'			
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vID, @vCantidad 
		While @@FETCH_STATUS = 0 Begin  
		
			--Actualizar Stock
			Exec SpActualizarProductoDeposito @IDDeposito=@vIDDeposito, @IDProducto = @vIDProducto, @Cantidad= @vCantidad, @Signo='-', @Mensaje= @Mensaje, @Procesado = @Procesado
			
			--Actualiza el Kardex
			Set @vCosto = IsNull((Select CostoPromedio From Producto Where ID = @vIDProducto),0)		
			EXEC SpKardex
				@Fecha = @vFecha,
				@IDTransaccion = @IDTransaccion,
				@IDProducto = @vIDProducto,
				@ID = @vID,
				@Orden = 15,
				@CantidadEntrada = 0,
				@CantidadSalida = @vCantidad,
				@CostoEntrada = 0,
				@CostoSalida = @vCosto,
				@IDSucursal = @vIDSucursal,
				@Comprobante = @vNroComprobante

			Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vID, @vCantidad
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
	End	
		
	If @Operacion = 'ANULAR' Begin
			
		Declare db_cursor cursor for
		Select DNC.IDProducto, DNC.IDDeposito, DNC.ID, DNC.Cantidad From DetalleNotaDebito DNC Join Producto P On DNC.IDProducto=P.ID Where DNC.IDTransaccion=@IDTransaccion And P.ControlarExistencia='True'			
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vID, @vCantidad 
		While @@FETCH_STATUS = 0 Begin  
		
			--Actualizar Stock
			Exec SpActualizarProductoDeposito @IDDeposito=@vIDDeposito, @IDProducto = @vIDProducto, @Cantidad= @vCantidad, @Signo='+', @Mensaje= @Mensaje, @Procesado = @Procesado
			
			--Actualiza el Kardex
			Set @vCosto = IsNull((Select CostoPromedio From Producto Where ID = @vIDProducto),0)		
			Set @vCantidad = @vCantidad*-1
			EXEC SpKardex
				@Fecha = @vFecha,
				@IDTransaccion = @IDTransaccion,
				@IDProducto = @vIDProducto,
				@ID = @vID,
				@Orden = 15,
				@CantidadEntrada = 0,
				@CantidadSalida = @vCantidad,
				@CostoEntrada = 0,
				@CostoSalida = @vCosto,
				@IDSucursal = @vIDSucursal,
				@Comprobante = @vNroComprobante	

			If @Procesado = 'False' Begin
				Close db_cursor   
				Deallocate db_cursor
				return @@rowcount
			End
		
			Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vID, @vCantidad
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
	End
	
	If @Operacion = 'ELIMINAR' Begin
	
		--Solo si no esta anulado
		If (Select Anulado From NotaDebito Where IDTransaccion=@IDTransaccion) = 'True' Begin
			Set @Mensaje = 'Registro guardado'
			Set @Procesado = 'True'
			return @@rowcount
		End
		
		--Eliminar del Kardex
		Delete Kardex where IDTransaccion = @IDTransaccion

		Declare db_cursor cursor for
		Select DNC.IDProducto, DNC.IDDeposito, DNC.Cantidad From DetalleNotaDebito DNC Join Producto P On DNC.IDProducto=P.ID Where DNC.IDTransaccion=@IDTransaccion And P.ControlarExistencia='True'			
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vCantidad 
		While @@FETCH_STATUS = 0 Begin  
		
			--Actualizar Stock
			Exec SpActualizarProductoDeposito @IDDeposito=@vIDDeposito, @IDProducto = @vIDProducto, @Cantidad= @vCantidad, @Signo='+', @Mensaje= @Mensaje, @Procesado = @Procesado
			
			Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vCantidad
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
	End
	
End
	





﻿
CREATE Procedure [dbo].[SpCFDepositoBancario]

	--Cuenta Fija
	@IDOperacion tinyint,
	@ID tinyint,
	@IDSucursal tinyint,
	@IDMoneda tinyint,
	@CuentaContable varchar(50),
	@Debe bit,
	@Haber bit,
	@Descripcion varchar(50),
	@Orden tinyint,
	
	--Cuenta Fija
	@BuscarEnCuentaBancaria bit = 'False',
	@Efectivo bit = 'False',
	@ChequeAlDia bit = 'False',
	@ChequeDiferido bit = 'False',
	@ChequeRechazado bit = 'False',
	@TipoCuenta bit = 'False',
	@DiferenciaCambio bit = 'False',
	@Documento bit = 'False',
	
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
	
As

Begin

	--BLOQUES
	
	Declare	@vID tinyint
	
	--INSERTAR
	if @Operacion='INS' begin
			
		EXEC SpCF @ID = 0,
				@IDOperacion = @IDOperacion,
				@IDSucursal = @IDSucursal,
				@IDMoneda = @IDMoneda,
				@CuentaContable = @CuentaContable,
				@Debe = @Debe,
				@Haber = @Haber,
				@Descripcion = @Descripcion,
				@Orden = @Orden,
				@Operacion = @Operacion,
				@vID = @vID OUTPUT
			
		Insert Into CFDepositoBancario(IDOperacion, ID, BuscarEncuentaBancaria, Efectivo, ChequeAlDia, ChequeDiferido, ChequeRechazado, TipoCuenta, DiferenciaCambio,Documento)
		Values(@IDOperacion, @vID, @BuscarEncuentaBancaria, @Efectivo, @ChequeAlDia, @ChequeDiferido, @ChequeRechazado, @TipoCuenta,@DiferenciaCambio, @Documento)
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		EXEC SpCF @ID = @ID,
				@IDOperacion = @IDOperacion,
				@IDSucursal = @IDSucursal,
				@IDMoneda = @IDMoneda,
				@CuentaContable = @CuentaContable,
				@Debe = @Debe,
				@Haber = @Haber,
				@Descripcion = @Descripcion,
				@Orden = @Orden,
				@Operacion = @Operacion,
				@vID = @vID OUTPUT
		
		Update CFDepositoBancario Set BuscarEncuentaBancaria=@BuscarEncuentaBancaria,
									Efectivo=@Efectivo,
									ChequeAlDia=@ChequeAlDia,
									ChequeDiferido=@ChequeDiferido,
									ChequeRechazado=@ChequeRechazado,
									TipoCuenta=@TipoCuenta,
									DiferenciaCambio = @DiferenciaCambio,
									Documento = @Documento
		Where ID=@ID And IDOperacion=@IDOperacion
				
		set @Mensaje = 'Registro guardado...!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		Delete From CFDepositoBancario Where IDOperacion=@IDOperacion And ID=@ID
		Delete From CF Where IDOperacion=@IDOperacion And ID=@ID
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount
			
End


﻿CREATE Procedure [dbo].[SpListaPrecio]

	--Entrada
	@ID int,
	@IDSucursal tinyint,
	@Descripcion varchar(50),
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From ListaPrecio Where Descripcion=@Descripcion And IDSucursal=@IDSucursal) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDListaPrecio tinyint
		set @vIDListaPrecio = (Select IsNull((Max(ID)+1), 1) From ListaPrecio)

		--Insertamos
		Insert Into ListaPrecio(ID, IDSucursal, Descripcion, Estado)
		Values(@vIDListaPrecio, @IDSucursal, @Descripcion, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='LISTA DE PRECIO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From ListaPrecio Where ID=@ID ) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From ListaPrecio Where Descripcion=@Descripcion And ID!=@ID And IDSucursal=@IDSucursal) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update ListaPrecio Set Descripcion=@Descripcion,
								IDSucursal=@IDSucursal,
						 Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='LISTA DE PRECIO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From ListaPrecio Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Productos
		--if exists(Select * From ProductoListaPrecio Where IDListaPrecio=@ID) begin
		--	set @Mensaje = 'El registro tiene productos asociados! No se puede eliminar.'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--end
		
		--Eliminar desde Clientes
		Update Cliente Set IDListaPrecio=NULL Where IDListaPrecio=@ID
		
		--Eliminar relaciones con productos
		Delete From ProductoListaPrecio Where IDListaPrecio=@ID
		
		--Eliminamos
		Delete From ListaPrecio 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='LISTA DE PRECIO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


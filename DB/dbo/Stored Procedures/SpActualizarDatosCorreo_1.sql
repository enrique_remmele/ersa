﻿CREATE Procedure [dbo].[SpActualizarDatosCorreo]

	--Entrada
	@Correo varchar(500),
	@Contraseña varchar(500)
As

Begin
	
	if exists(SELECT * FROM msdb.dbo.sysmail_account where email_address = @Correo) begin
		
		Declare @vid as integer
		Set @vid = (SELECT account_id FROM msdb.dbo.sysmail_account where email_address = @Correo)

		EXECUTE msdb.dbo.sysmail_update_account_sp  
		 @account_id = @vid, 
		 @account_name = @Correo,  
		 @email_address =  @Correo,   
		 @display_name =  @Correo,   
		 @mailserver_name = 'smtp.office365.com',
		 @port =  587 ,   
		 @username=	@Correo,
		 @password=@Contraseña,  
		 --@password='##',  
		 @enable_ssl = 1
	end
	else begin
	
		EXECUTE msdb.dbo.sysmail_add_account_sp
		@account_name = @Correo,
		@description = 'Used for sending email from SQL Express',
		--Name of sending email address
		@email_address = @Correo,
		--Display name for the sending email address
		@display_name = @Correo,
		--Username to login to the mail server
		@username=@Correo,
		--Password to login to the mail server
		@password=@Contraseña,
		@enable_ssl=1,
		@port = 587,
		--Name or IP of the mail server 
		@mailserver_name = 'smtp.office365.com';

		EXECUTE msdb.dbo.sysmail_add_profile_sp
		@profile_name = @Correo,
		@description = 'Main profile used to send email';

		/* Add the account to the profile */
		EXECUTE msdb.dbo.sysmail_add_profileaccount_sp
		@profile_name = @Correo,
		@account_name = @Correo,
		--The sequence number determines the order in which Database Mail uses accounts in the profile.
		--Going from lowest to highest until one works.
		@sequence_number = 1;

		/* Grant permission to public to use the profile
		or choose a more restrictive group if desired */
		EXECUTE msdb.dbo.sysmail_add_principalprofile_sp
		@profile_name = @Correo,
		@principal_name = 'public',
		@is_default = 1;

	end
	
			
End


﻿CREATE Procedure [dbo].[SpViewLibroMayorSucursal]

	--Entrada
	@CuentaInicial varchar(50),
	@CuentaFinal varchar(50),
	@Fecha1 date,
	@Fecha2 date,
	
	--Tipo
	@Pantalla bit = 'False',
	@IDSucursal int	
	
As

Begin

	--Variables
	Begin
		declare @vID tinyint
		declare @vIDTransaccion int
		declare @vCod varchar(50)
		declare @vCodigo varchar(50)
		declare @vDescripcion varchar (100)
		declare @vFecha date
		declare @vAsiento int
		declare @vComprobante varchar(50)
		declare @vSuc varchar(50)
		declare @vConcepto varchar(50)
		declare @vDebito money
		declare @vCredito money
		declare @vIDSucursal int
		declare @vIDTipoComprobante int
		declare @vPrimero bit
		Declare @vCodigoActual varchar(100)
		Declare @vCuentaTipo varchar(50)
		Declare @vPrefijo varchar(50)
		Declare @vUsuario varchar(32)
		Declare @vFechaTransaccion Date	
		Declare @vCotizacion money
		declare @IDUnidadNegocio int
		Declare @IDCentroCosto int
		
				
		--Variables para calcular saldo
		declare @vSaldo money
		declare @vTotalDebito money
		declare @vTotalCredito money
		declare @vSaldoAnterior money
		declare @vSaldoFinal money
		declare @vSaldoInicial MONEY

		declare @IDMoneda int
		Declare @Moneda varchar(16)
		Declare @vAño varchar(4)
		
	END

	If @Pantalla = 'False' Begin
	
		--Crear la tabla temporal
		create table #TablaTemporal(ID int,
									IDTransaccion int,
									Codigo varchar(100),
									Descripcion varchar(300),
									Fecha date,
									Asiento int,
									Comprobante varchar(100),
									Suc varchar(100),
									Concepto varchar(100),
									Debito money,
									Credito money,
									Saldo money,
									SaldoInicial money,
									SaldoAnterior money,
									IDSucursal int,
									IDTipoComprobante int,
									Usuario varchar(32),
									FechaTransaccion Date,
									Cotizacion money,
									IDUnidadNegocio int,
									IDCentroCosto int,
									IDMoneda int,
									Moneda varchar(16))
																	
		set @vID = (Select IsNull(MAX(ID)+1,1) From #TablaTemporal)
		Set @vPrimero = 'True'
		
		--Insertar datos	
		Begin
				
			Declare db_cursor cursor for
			
			Select 
			IDTransaccion,
			Codigo,
			'Cod'=substring(Codigo,1,1),
			Descripcion,
			Fecha,
			Asiento,
			Comprobante,
			CodSucursal,
			Concepto,
			Debito,
			Credito,
			IDSucursal,
			IDTipoComprobante,
			Usuario,
			FechaTransaccion,
			Cotizacion,
			IDCentroCosto,
			IDUnidadNegocio,
			IDMoneda,
			Moneda
			From VLibroMayor
			Where Codigo Between @CuentaInicial and  @CuentaFinal and Fecha between @Fecha1 and @Fecha2 And IDSucursal=@IDSucursal
			Order By Codigo, Fecha		
			Open db_cursor   
			Fetch Next From db_cursor Into	@vIDTransaccion,@vCodigo,@vCod,@vDescripcion,@vFecha,@vAsiento,@vComprobante,@vSuc,@vConcepto,@vDebito,@vCredito,@vIDSucursal,@vIDTipoComprobante, @vUsuario, @vFechaTransaccion, @vCotizacion, @IDCentroCosto, @IDUnidadNegocio, @IDMoneda, @Moneda
			While @@FETCH_STATUS = 0 Begin 
			
				If @vCodigoActual != @vCodigo Begin
					Set @vPrimero = 'True'
				End

				If @vPrimero = 'True' Begin	
					
					--Hallar Totales para Saldo Anterior
					Set @vTotalDebito  = IsNull((Select Sum(Debito) From VLibroMayor Where Codigo = @vCodigo And Fecha < @Fecha1),0)
					Set @vTotalCredito = IsNull((Select Sum(Credito) From VLibroMayor Where Codigo = @vCodigo And Fecha < @Fecha1),0)
							
					--Hallar Saldo Anterior
					Set @vSaldoAnterior = @vTotalDebito - @vTotalCredito
					
					Set @vPrimero = 'False'
					
				End
				
				Set @vPrefijo = SubString(@vCodigo, 0, 2)
				
				--Cuentas del Debe
				If @vPrefijo = '1' Or @vPrefijo = '5' Begin
					set @vSaldo = (@vSaldoAnterior + @vDebito) - @vCredito
				End
				
				--Cuentas del Haber
				If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
					set @vSaldo = (@vSaldoAnterior + @vCredito) - @vDebito
				End
								
				Insert Into  #TablaTemporal(ID,IDTransaccion,Codigo,Descripcion,Fecha,Asiento,Comprobante,Suc,Concepto,Debito,Credito,Saldo,SaldoInicial,SaldoAnterior,IDSucursal,IDTipoComprobante, Usuario, FechaTransaccion, cotizacion, IDCentroCosto, IDUnidadNegocio, IDMoneda, Moneda) 
									Values (@vID,@vIDTransaccion,@vCodigo,@vDescripcion,@vFecha,@vAsiento,@vComprobante,@vSuc,@vConcepto,@vDebito,@vCredito,@vSaldo,@vSaldoInicial,@vSaldoAnterior,@vIDSucursal,@vIDTipoComprobante,@vUsuario,@vFechaTransaccion,@vCotizacion, @IDCentroCosto, @IDUnidadNegocio, @IDMoneda, @Moneda)
								
				--Actualizar Saldo
				set @vSaldoAnterior = @vSaldo 
				Set @vCodigoActual = @vCodigo		
								
				Fetch Next From db_cursor Into	@vIDTransaccion,@vCodigo,@vCod,@vDescripcion,@vFecha,@vAsiento,@vComprobante,@vSuc,@vConcepto,@vDebito,@vCredito,@vIDSucursal,@vIDTipoComprobante, @vUsuario, @vFechaTransaccion, @vCotizacion, @IDCentroCosto, @IDUnidadNegocio, @IDMoneda, @Moneda
				
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
			
		End	
		
		--Select * From #TablaTemporal Where ID=@vID Order By Codigo
		Select T.* 
		, CASE TC.Resumen
		WHEN '' THEN T.Concepto
		--ELSE  CAST(T.Asiento AS VARCHAR(50)) + '  ' + TC.Resumen  + ' del ' + CAST(T.Fecha AS VARCHAR(50)) 
		ELSE  TC.Resumen  + ' del ' + CAST(T.Fecha AS VARCHAR(50)) 
		END AS Resumen
		From #TablaTemporal T
		LEFT JOIN dbo.TipoComprobante TC ON T.IDTipoComprobante = TC.ID
		Where T.ID=@vID Order By T.Codigo, T.Fecha		
		
		Select * From #TablaTemporal Where ID=@vID 
	
	End
	
	If @Pantalla = 'True' Begin
		
		--Crear la tabla
		Create Table #TablaTemporalPantalla(ID tinyint,
									Codigo varchar(50),
									Denominacion varchar(200),
									[Saldo Anterior] money,
									Debitos money,
									Creditos money,
									[Saldo Final] money)
		
		Set @vID = (Select IsNull(MAX(ID)+1,1) From #TablaTemporalPantalla)
		
		Declare db_CursorPantalla cursor for
		
		Select Codigo, Descripcion, 'Debito'=Sum(Debito), 'Credito'=Sum(Credito) 
		From VLibroMayor 
		Where Codigo Between @CuentaInicial and  @CuentaFinal and Fecha between @Fecha1 and @Fecha2 
		Group By Codigo, Descripcion, Fecha
		Order By Codigo, Fecha
		Open db_CursorPantalla   
		Fetch Next From db_CursorPantalla Into @vCodigo, @vDescripcion, @vDebito, @vCredito
		While @@FETCH_STATUS = 0 Begin 
		
			Set @vPrefijo = SubString(@vCodigo, 0, 2)
			
			--Cuentas del Debe
			If @vPrefijo = '1' Or @vPrefijo = '5' Begin
				Set @vSaldoAnterior = IsNull((Select SUM(Debito) - SUM(Credito) From VLibroMayor Where Codigo=@vCodigo And Fecha<@Fecha1), 0)
				Set @vSaldoFinal = (@vSaldoAnterior + @vDebito) - @vCredito
			End
			
			--Cuentas del Haber
			If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
				Set @vSaldoAnterior = IsNull((Select SUM(Credito) - SUM(Debito) From VLibroMayor Where Codigo=@vCodigo And Fecha<@Fecha1), 0)
				Set @vSaldoFinal = (@vSaldoAnterior + @vCredito) - @vDebito
			End
			
			Insert Into  #TablaTemporalPantalla(ID, Codigo, Denominacion, [Saldo Anterior], Debitos, Creditos,[Saldo Final]) 
			Values (@vID, @vCodigo, @vDescripcion, @vSaldoAnterior, @vDebito, @vCredito, @vSaldoFinal)
			
			Fetch Next From db_CursorPantalla Into @vCodigo, @vDescripcion, @vDebito, @vCredito
			
		End
		
		--Cierra el cursor
		Close db_CursorPantalla   
		Deallocate db_CursorPantalla	
		
		Select * From #TablaTemporalPantalla Where ID=@vID 
		
	END

End
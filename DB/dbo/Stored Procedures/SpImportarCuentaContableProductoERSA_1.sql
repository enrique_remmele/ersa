﻿create Procedure [dbo].[SpImportarCuentaContableProductoERSA]

	--Entrada
	@Referencia varchar(50) = 0,
	@CCVenta as Varchar(50)='',
	@CCCompra as varchar(50)='',
	@CCDeudor as varchar(51)='',
	@CCCosto as Varchar(50)='',
	
	--Opciones
	@Actualizar Bit = 'False'
	
As

Begin

	--ID's	
	Declare @vID int
	Declare @vOperacion varchar(10) 
	Declare @Mensaje varchar(200)
	Declare @Procesado bit
		
	Begin
	
	set @vID = @Referencia
	
	If @vID > 0 Begin
		Update Producto
		Set CuentaContableVenta = @CCVenta,
		CuentaContableCompra = @CCCompra,
		CuentaContableCosto = @CCCosto,
		CuentaContableDeudor = @CCDeudor
		where ID = @vID

		set @Mensaje = 'Registro Procesado'
		set @Procesado = 'True'
	End



Salir:
	Select 'Mensaje'=@Mensaje + ' ' + @vOperacion, 'Procesado'=@Procesado, 'Cantidad'=@@ROWCOUNT
End
End



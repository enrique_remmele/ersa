﻿CREATE Procedure [dbo].[SpCierreAperturaStock]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Numero int = NULL,
	@Fecha date = NULL,
	@Observacion varchar(200) = NULL,
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion int,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,

	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--BLOQUES
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		If Exists(Select * From VCierreReaperturaStock Where Numero=@Numero) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Fecha
		
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--Insertar
		Insert Into CierreAperturaStock(IDTransaccion, Numero, Fecha, Observacion, Procesado, Anulado)
		Values(@IDTransaccionSalida, @Numero, @Fecha, @Observacion, 'False', 'False')
				
		--Insertar Detalle
		EXEC SpDetalleCierreAperturaStock
		@Numero = @Numero,
		@IDTransaccion = @IDTransaccionSalida,
		@Fecha = @Fecha

		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From CierreAperturaStock Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Anulamos el registro
		Update CierreAperturaStock Set Anulado='True', IDUsuarioAnulado=@IDUsuario, FechaAnulado=GETDATE()
		Where IDTransaccion = @IDTransaccion

		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From CierreAperturaStock Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Eliminar del detalle
		Delete DetalleCierreAperturaStock Where IDTransaccion = @IDTransaccion
		
		--Eliminar del Kardex
		Delete Kardex Where IDTransaccion = @IDTransaccion
		
		--Eliminar Cabecera
		Delete CierreAperturaStock Where IDTransaccion = @IDTransaccion
		
		--Verificar que no este anulado
					
		--Actualizamos el Stock
			
		--Insertamos el registro de anulacion
		
		--Anulamos el registro
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End

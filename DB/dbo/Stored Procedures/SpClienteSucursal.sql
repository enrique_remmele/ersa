﻿CREATE Procedure [dbo].[SpClienteSucursal]

	--Entrada
	@IDCliente int,
	@ID tinyint = NULL,
	@Sucursal varchar(50) = NULL,
	@Direccion varchar(50) = NULL,
	@Contacto varchar(50) = NULL,
	@Telefono varchar(50) = NULL,
	
	@IDPais tinyint = NULL,
	@IDDepartamento tinyint = NULL,
	@IDCiudad smallint = NULL,
	@IDBarrio smallint = NULL,
	@IDVendedor tinyint = NULL,
	@IDZonaVenta tinyint = NULL,
	@IDSucursal tinyint = NULL,
	@IDListaPrecio tinyint = NULL,
	@Contado bit = NULL,
	@Credito bit = NULL,
	@IDCobrador tinyint = NULL,
	@IDEstado tinyint = NULL,
	@PlazoCredito int = 0,
	@IDPromotor tinyint = NULL,
	--SM 22-07-2021 Nuevo campo
	@Email varchar(50) = NULL,

	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
	
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From ClienteSucursal Where Sucursal=@Sucursal And IDCliente=@IDCliente) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end

		if (@Credito = 1 and @PlazoCredito =0) Begin
			set @Mensaje = 'Ingrese un Plazo de Credito Valido'
			set @Procesado = 'False'
			return @@rowcount
		End

		if (@Credito = 0 and @PlazoCredito > 0) Begin
			set @Mensaje = 'Ingrese un Plazo de Credito Valido'
			set @Procesado = 'False'
			return @@rowcount
		End

		
		--Obtenemos el nuevo ID
		declare @vID tinyint
		set @vID = (Select IsNull((Max(ID)+1), 1) From ClienteSucursal Where IDCliente=@IDCliente)

		--Insertamos
		Insert Into ClienteSucursal(IDCliente,  ID,   Sucursal,  Direccion,  Contacto,  Telefono,  Estado, IDPais,  IDDepartamento,  IDCiudad,  IDBarrio,  IDZonaVenta,  IDVendedor, Latitud, Longitud, IDSucursal, IDListaPrecio, Contado, Credito, IDCobrador, IDEstado, PlazoCredito,IDPromotor,Email)
		                    Values (@IDCliente, @vID, @Sucursal, @Direccion, @Contacto, @Telefono, 'True', @IDPais, @IDDepartamento, @IDCiudad, @IDBarrio, @IDZonaVenta, @IDVendedor, '0', '0', @IDSucursal, @IDListaPrecio, @Contado , @Credito, @IDCobrador, @IDEstado, @PlazoCredito, @IDPromotor,@Email)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CLIENTESUCURSAL', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		--Auditoria de cambios de clientes	
		
		IF ((Select ISnull(PlazoCredito,0) from ClienteSucursal where IDCliente = @IdCliente and ID=@ID) <> @PlazoCredito) Begin
		 
		 Declare @Referencia as varchar(50) = (select Referencia from cliente where ID = @IDCliente)
		 Declare @RazonSocial as varchar(100) = (select RazonSocial from cliente where ID = @IDCliente)
		 Declare @CI as varchar(32) = (select CI from cliente where ID = @IDCliente)
		 Declare @RUC as varchar(32) = (select RUC from cliente where ID = @IDCliente)
		 Declare @NombreFantasia as varchar(100) = (select NombreFantasia from cliente where ID = @IDCliente)
		 Declare @IDTipoCliente as int = (select IDTipoCliente from cliente where ID = @IDCliente)
		 Declare @LimiteCredito as int = (select LimiteCredito from cliente where ID = @IDCliente)
		 Declare @vPlazo bit = 'False'
		 Declare @vCondicion bit = 'False'
		 
		 IF ((Select ISnull(PlazoCredito,0) from ClienteSucursal where IDCliente=@IDCliente and ID = @ID) <> @PlazoCredito) begin set @vPlazo = 'True' End
		 --IF ((Select ISnull(Contado,0) from ClienteSucursal where IDCliente=@IDCliente and ID = @ID) <> @Contado) begin set @vCondicion = 'True' End
		 --IF ((Select ISnull(Credito,0) from ClienteSucursal where IDCliente=@IDCliente and ID = @ID) <> @Credito) begin set @vCondicion = 'True' End


			Insert into ACliente(ID, Referencia, RazonSocial,CI,RUC,Nombrefantasia,Direccion,
			Telefono,IDListaPrecio, IDTipoCliente, Contado, Credito, IdVendedor, LimiteCredito,
			FechaModificacion, PlazoCredito,IDUsuario, IdEstado, IdPais, IDDepartamento, IdCiudad, 
			IDSucursal,Accion, ModiLimiteCredito, ModiListaPrecio,ModiVendedor,ModiTipoCliente,ModiPlazo,ModiCondicion,IDPromotor)
			Values(@ID, @Referencia,@RazonSocial,@CI,@RUC,@NombreFantasia,@Direccion,
			@Telefono,@IDListaPrecio, @IDTipoCliente,@Contado, @Credito, @IDVendedor, @LimiteCredito,
			getdate(),@PlazoCredito, @IDUsuario, @IDEstado, @IDPais, @IDDepartamento, @IDCiudad, 
			@IDSucursal,'INS', @LimiteCredito, '','','',@vPlazo,@vCondicion, @IDPromotor)
			
		End
		


		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From ClienteSucursal Where IDCliente=@IDCliente And ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From ClienteSucursal Where Sucursal=@Sucursal And IDCliente=@IDCliente And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		if (@Credito = 1 and @PlazoCredito =0) Begin
			set @Mensaje = 'Ingrese un Plazo de Credito Valido'
			set @Procesado = 'False'
			return @@rowcount
		End

		if (@Credito = 0 and @PlazoCredito > 0) Begin
			set @Mensaje = 'Ingrese un Plazo de Credito Valido'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Auditoria de cambios de clientes	
		
		IF ((Select ISnull(PlazoCredito,0) from ClienteSucursal where IDCliente = @IdCliente and ID=@ID) <> @PlazoCredito) Begin
		 
		 set @Referencia = (select Referencia from cliente where ID = @IDCliente)
		 set @RazonSocial  = (select RazonSocial from cliente where ID = @IDCliente)
		 set @CI  = (select CI from cliente where ID = @IDCliente)
		 set @RUC  = (select RUC from cliente where ID = @IDCliente)
		 set @NombreFantasia  = (select NombreFantasia from cliente where ID = @IDCliente)
		 set @IDTipoCliente  = (select IDTipoCliente from cliente where ID = @IDCliente)
		 set @LimiteCredito = (select LimiteCredito from cliente where ID = @IDCliente)
		 set @vPlazo = 'False'
		 set @vCondicion  = 'False'
		 
		 IF ((Select ISnull(PlazoCredito,0) from ClienteSucursal where IDCliente=@IDCliente and ID = @ID) <> @PlazoCredito) begin set @vPlazo = 'True' End
		 --IF ((Select ISnull(Contado,0) from ClienteSucursal where IDCliente=@IDCliente and ID = @ID) <> @Contado) begin set @vCondicion = 'True' End
		 --IF ((Select ISnull(Credito,0) from ClienteSucursal where IDCliente=@IDCliente and ID = @ID) <> @Credito) begin set @vCondicion = 'True' End


			Insert into ACliente(ID, Referencia, RazonSocial,CI,RUC,Nombrefantasia,Direccion,
			Telefono,IDListaPrecio, IDTipoCliente, Contado, Credito, IdVendedor, LimiteCredito,
			FechaModificacion, PlazoCredito,IDUsuario, IdEstado, IdPais, IDDepartamento, IdCiudad, 
			IDSucursal,Accion, ModiLimiteCredito, ModiListaPrecio,ModiVendedor,ModiTipoCliente,ModiPlazo,ModiCondicion,IDPromotor)
			Values(@ID, @Referencia,@RazonSocial,@CI,@RUC,@NombreFantasia,@Direccion,
			@Telefono,@IDListaPrecio, @IDTipoCliente,@Contado, @Credito, @IDVendedor, @LimiteCredito,
			getdate(),@PlazoCredito, @IDUsuario, @IDEstado, @IDPais, @IDDepartamento, @IDCiudad, 
			@IDSucursal,'MOD', @LimiteCredito, '','','',@vPlazo,@vCondicion,@IDPromotor)
			
		End
		
		--Actualizamos
		Update ClienteSucursal Set	Sucursal=@Sucursal, 
									Direccion=@Direccion,
									Contacto = @Contacto,
									Telefono = @Telefono,
									IDDepartamento = @IDDepartamento,
									IDCiudad = @IDCiudad,
									IDBarrio = @IDBarrio,
									IDZonaVenta = @IDZonaVenta,
									IDVendedor = @IDVendedor,
									IDSucursal=@IDSucursal,
									IDListaPrecio= @IDListaPrecio,
									Contado= @Contado,
									Credito= @Credito,
									IDCobrador= @IDCobrador,
									IDEstado= @IDEstado,
									PlazoCredito = @PlazoCredito,
									IDPromotor = @IDPromotor,
									--SM 22-07-2021 Nuevo campo
									Email=@Email
		Where ID=@ID And IDCliente=@IDCliente 
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CLIENTESUCURSAL', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From ClienteSucursal Where ID=@ID And IDCliente=@IDCliente ) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una transaccion
		if exists(Select * From Venta Where IDSucursalCliente=@ID And IDCliente=@IDCliente) begin
			set @Mensaje = 'El registro tiene transacciones asociadas! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end

		Insert into ACliente(ID, Referencia, RazonSocial,CI,RUC,Nombrefantasia,Direccion,
			Telefono,IDListaPrecio, IDTipoCliente, Contado, Credito, IdVendedor, LimiteCredito,
			FechaModificacion, PlazoCredito,IDUsuario, IdEstado, IdPais, IDDepartamento, IdCiudad, 
			IDSucursal,Accion, ModiLimiteCredito, ModiListaPrecio,ModiVendedor,ModiTipoCliente,ModiPlazo,ModiCondicion,IDPromotor)
			Values(@ID, @Referencia,@RazonSocial,@CI,@RUC,@NombreFantasia,@Direccion,
			@Telefono,@IDListaPrecio, @IDTipoCliente,@Contado, @Credito, @IDVendedor, @LimiteCredito,
			getdate(),@PlazoCredito, @IDUsuario, @IDEstado, @IDPais, @IDDepartamento, @IDCiudad, 
			@IDSucursal,'DEL', @LimiteCredito, '','','',@vPlazo,@vCondicion,@IDPromotor)
		
		--Eliminamos
		Delete From ClienteSucursal 
		Where ID=@ID And IDCliente=@IDCliente 
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--Auditoria
	Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CLIENTESUCURSAL', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


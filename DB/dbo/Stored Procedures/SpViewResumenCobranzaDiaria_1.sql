﻿CREATE Procedure [dbo].[SpViewResumenCobranzaDiaria]

	--Entrada
	@Fecha1 Date,
	@Fecha2 Date
	
		
As

	
Begin
	
		--Crear la tabla temporal
		create table #TablaTemporal(ID tinyint,
								Fec date,
								Fecha date,
								IDCliente int,
								Cod varchar(50),
								CantidadComprobantes decimal(18,0),
								IDCobrador int,
								IDVendedor int,
								IDSucursal int,
								IDDeposito int,
								IDTipoComprobante int,
								NumeroCobranza int,
								NumeroLote int,
								FormaPago varchar(50),
								TipoComprobante varchar(50),
								Tipo varchar(50),
								CantidadFormaPago decimal(18,0),
								TotalCobrado money,
								Importe money,
								CancelarCheque bit,
								Credito bit)
								
 
	
		--Declarar variables 
		declare @vID tinyint
		declare @vFecha date
		declare @vFec date
		declare @vIDCliente int
		declare @vCod varchar(50)
		declare @vCantidadComprobantes decimal(18,0)
		declare @vIDCobrador int
		declare @vIDVendedor int
		declare @vIDSucursal int
		declare @vIDDeposito int
		declare @vIDTipoComprobante int
		declare @vNumeroCobranza int
		declare @vNumeroLote int
		declare @vFormaPago varchar(50)
		declare @vTipoComprobante varchar(50)
		declare @vTipo varchar(50)
		declare @vCantidadFormaPago decimal(18,0)
		declare @vTotalCobrado money
		declare @vImporte money
		declare @vCancelarCheque bit
		declare @vCredito bit	
			
	
		Set @vID=(Select isnull(MAX(ID)+ 1,1) From #TablaTemporal)
		
		Declare db_cursor cursor for
		
		Select
		Fec,
		Fecha,
		IDCliente,
		[Cod.],
		IDCobrador,
		IDVendedor,
		IDSucursal,
		IDDeposito,
		IDTipoComprobante,
		NumeroCobranza,
		Lote,
		FormaPago,
		TipoComprobante,
		Tipo,
		CancelarCheque,
		Cant,
		CantComprobantes,
		TotalCobrado,
		Importe,
		Credito
		From VResumenCobranzaDiaria
		Where Fecha Between @Fecha1 And @Fecha2 And Anulado='False'
								
		Open db_cursor   
		
		Fetch Next From db_cursor Into @vFec,@vFecha,@vIDCliente,@vCod,@vIDCobrador,@vIDVendedor,@vIDSucursal,@vIDDeposito,@vIDTipoComprobante,@vNumeroCobranza,@vNumeroLote,@vFormaPago,@vTipoComprobante,@vTipo,@vCancelarCheque,@vCantidadFormaPago,@vCantidadComprobantes,@vTotalCobrado,@vImporte,@vCredito
		
		While @@FETCH_STATUS = 0 Begin										
																	
				Insert into #TablaTemporal(ID,Fec,Fecha,IDCliente,Cod,CantidadComprobantes,IDCobrador,IDVendedor,IDSucursal,IDDeposito,IDTipoComprobante,NumeroCobranza,NumeroLote,FormaPago,TipoComprobante,Tipo,CantidadFormaPago,TotalCobrado,Importe,CancelarCheque,Credito) 
				Values (@vID,@vFec,@vFecha,@vIDCliente,@vCod,@vCantidadComprobantes,@vIDCobrador,@vIDVendedor,@vIDSucursal,@vIDDeposito,@vIDTipoComprobante,@vNumeroCobranza,@vNumeroLote,@vFormaPago,@vTipoComprobante,@vTipo,@vCantidadFormaPago,@vTotalCobrado,@vImporte,@vCancelarCheque,@vCredito) 													 
										  
				
			Fetch Next From db_cursor Into @vFec,@vFecha,@vIDCliente,@vCod,@vIDCobrador,@vIDVendedor,@vIDSucursal,@vIDDeposito,@vIDTipoComprobante,@vNumeroCobranza,@vNumeroLote,@vFormaPago,@vTipoComprobante,@vTipo,@vCancelarCheque,@vCantidadFormaPago,@vCantidadComprobantes,@vTotalCobrado,@vImporte,@vCredito
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		

		Select Fec,Cod,CantidadComprobantes,Tipo,CantidadFormaPago,TotalCobrado,Importe,IDTipoComprobante,Credito   From #TablaTemporal Where ID=@vID
		Group By Fec,Cod,CantidadComprobantes,Tipo,CantidadFormaPago,TotalCobrado,Importe,IDTipoComprobante,Credito
	
End









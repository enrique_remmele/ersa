﻿
CREATE Procedure [dbo].[SpViewLibroMayorCG]

	--Entrada
	@CuentaInicial varchar(50),
	@CuentaFinal varchar(50),
	@Fecha1 date,
	@Fecha2 date,
	
	--Tipo
	@Pantalla bit = 'False'	

As

Begin
	--SV 20140821 Agregando campo resumen de la tabla comprobante para mostrar en Libro Mayor
	
	--Variables
	Begin
		declare @vID tinyint
		declare @vNumero int
		declare @vCod varchar(50)
		declare @vCodigo varchar(50)
		declare @vDescripcion varchar (100)
		declare @vFecha date
		declare @vAsiento int
		declare @vComprobante varchar(50)
		declare @vSuc varchar(50)
		declare @vConcepto varchar(50)
		declare @vDebito money
		declare @vCredito money
		declare @vIDSucursal int
		declare @vIDTipoComprobante int
		declare @vPrimero bit
		Declare @vCuentaTipo varchar(50)
		Declare @vPrefijo varchar(50)
		Declare @vCodigoActual varchar(50)
			
		
		--Variables para calcular saldo
		declare @vSaldo money
		declare @vTotalDebito money
		declare @vTotalCredito money
		declare @vSaldoAnterior money
		declare @vSaldoFinal money
		declare @vSaldoInicial MONEY
		
	End
	
	If @Pantalla = 'False' Begin
	
		--Crear la tabla temporal
		create table #TablaTemporal(ID tinyint,
									Numero int,
									Codigo varchar(50),
									Descripcion varchar(50),
									Fecha date,
									Asiento int,
									Comprobante varchar(50),
									Suc varchar(50),
									Concepto varchar(50),
									Debito money,
									Credito money,
									Saldo money,
									SaldoInicial money,
									SaldoAnterior money,
									IDSucursal int,
									IDTipoComprobante int)
																	
		set @vID = (Select IsNull(MAX(ID)+1,1) From #TablaTemporal)
		Set @vPrimero = 'True'
		Set @vCodigoActual = '0'
		
		--Insertar Saldo Anterior	
		
			Begin
			
			Declare db_cursorSaldoAnterior cursor for
			
			Select 
			Codigo,
			Descripcion,
			'Debito'=Sum(Debito),
			'Credito'=Sum(Credito)
			From VLibroMayorCG
			Where Codigo Between @CuentaInicial and  @CuentaFinal and Fecha < @Fecha1
			Group By Codigo, Descripcion
			Order By Codigo
			
			Open db_cursorSaldoAnterior   
			Fetch Next From db_cursorSaldoAnterior Into @vCodigo, @vDescripcion, @vDebito, @vCredito
			While @@FETCH_STATUS = 0 Begin 
			
				--Reiniciar	el Saldo Inciial si se cambia el codigo de la cuenta
				--Esto es necesario cuando se listan varias cuentas en el informe.
				
				Set @vPrefijo = SubString(@vCodigo, 0, 2)
					
				
					--Hallar Saldo Anterior
					If @vPrefijo = '1' Or @vPrefijo = '5' Begin
						Set @vSaldoAnterior = @vDebito - @vCredito
					End
					
					If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
						Set @vSaldoAnterior = @vCredito - @vDebito
					End
					
					Set @vSaldoInicial = @vSaldoAnterior
					
								
				Insert Into  #TablaTemporal(ID, Numero,Codigo,Descripcion,Fecha,Asiento,Comprobante,Suc,Concepto,Debito,Credito,Saldo,SaldoInicial,SaldoAnterior,IDSucursal,IDTipoComprobante) 
									Values (@vID, 0,@vCodigo,@vDescripcion,@Fecha1,0,'','','=== Saldo Anterior ===',0,0,@vSaldoAnterior,0,@vSaldoAnterior,0,0)
								
				
				Fetch Next From db_cursorSaldoAnterior Into @vCodigo, @vDescripcion, @vDebito, @vCredito
				
			End
			
			--Cierra el cursor
			Close db_cursorSaldoAnterior   
			Deallocate db_cursorSaldoAnterior
			
		End	

		--Insertar Datos
		Begin
							
			Declare db_cursor cursor for
			
			Select 
			Numero,
			Codigo,
			'Cod'=substring(Codigo,1,1),
			Descripcion,
			Fecha,
			Asiento,
			Comprobante,
			CodSucursal,
			Concepto,
			Debito,
			Credito,
			IDSucursal,
			IDTipoComprobante
			From VLibroMayorCG
			Where Codigo Between @CuentaInicial and  @CuentaFinal and Fecha between @Fecha1 and @Fecha2
			Order By Codigo, Fecha	
			Open db_cursor   
			Fetch Next From db_cursor Into	@vNumero, @vCodigo, @vCod, @vDescripcion, @vFecha, @vAsiento, @vComprobante, @vSuc, @vConcepto, @vDebito, @vCredito, @vIDSucursal, @vIDTipoComprobante
			While @@FETCH_STATUS = 0 Begin 
			
				--Reiniciar	el Saldo Inciial si se cambia el codigo de la cuenta
				--Esto es necesario cuando se listan varias cuentas en el informe.
				If @vCodigoActual != @vCodigo Begin
					Set @vPrimero = 'True'
				End
				
				Set @vPrefijo = SubString(@vCodigo, 0, 2)
					
				If @vPrimero = 'True' Begin	
					
					--Hallar Totales para Saldo Anterior
					Set @vTotalDebito  = IsNull((Select Sum(Debito) From VLibroMayorCG Where Codigo = @vCodigo And Fecha < @Fecha1),0)
					Set @vTotalCredito = IsNull((Select Sum(Credito) From VLibroMayorCG Where Codigo = @vCodigo And Fecha < @Fecha1),0)
							
					--Hallar Saldo Anterior
					If @vPrefijo = '1' Or @vPrefijo = '5' Begin
						Set @vSaldoAnterior = @vTotalDebito - @vTotalCredito
					End
					
					If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
						Set @vSaldoAnterior = @vTotalCredito - @vTotalDebito
					End
					
					Set @vSaldoInicial = @vSaldoAnterior
					
					Set @vPrimero = 'False'
					
				End
			
				
				--Cuentas del Debe
				If @vPrefijo = '1' Or @vPrefijo = '5' Begin
					set @vSaldo = (@vSaldoAnterior + @vDebito) - @vCredito
				End
				
				--Cuentas del Haber
				If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
					set @vSaldo = (@vSaldoAnterior + @vCredito) - @vDebito
				End
								
				Insert Into  #TablaTemporal(ID, Numero,Codigo,Descripcion,Fecha,Asiento,Comprobante,Suc,Concepto,Debito,Credito,Saldo,SaldoInicial,SaldoAnterior,IDSucursal,IDTipoComprobante) 
									Values (@vID, @vNumero,@vCodigo,@vDescripcion,@vFecha,@vAsiento,@vComprobante,@vSuc,@vConcepto,@vDebito,@vCredito,@vSaldo,@vSaldoInicial,@vSaldoAnterior,@vIDSucursal,@vIDTipoComprobante)
								
				--Actualizar Saldo
				set @vSaldoAnterior = @vSaldo 
				Set @vCodigoActual = @vCodigo					
				
				Fetch Next From db_cursor Into	@vNumero, @vCodigo, @vCod, @vDescripcion, @vFecha, @vAsiento, @vComprobante, @vSuc, @vConcepto, @vDebito, @vCredito, @vIDSucursal, @vIDTipoComprobante
				
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
			
		End	

		
		--Select * From #TablaTemporal Where ID=@vID Order By Codigo
		Select T.* 
		,'Resumen'=(CASE TC.Resumen WHEN '' THEN T.Concepto ELSE  TC.Resumen  + ' del ' + CAST(T.Fecha AS VARCHAR(50)) End) 
		From #TablaTemporal T
		LEFT JOIN dbo.TipoComprobante TC ON T.IDTipoComprobante = TC.ID
		Where T.ID=@vID Order By T.Codigo, T.Fecha, T.Numero
		

	END
	
	If @Pantalla = 'True' Begin
		
		--Crear la tabla
		Create Table #TablaTemporalPantalla(ID tinyint,
									Codigo varchar(50),
									Denominacion varchar(200),
									[Saldo Anterior] money,
									Debitos money,
									Creditos money,
									[Saldo Final] money)
		
		Set @vID = (Select IsNull(MAX(ID)+1,1) From #TablaTemporalPantalla)
		
		Declare db_CursorPantalla cursor for
		
		Select Codigo, Descripcion, 'Debito'=Sum(Debito), 'Credito'=Sum(Credito) 
		From VLibroMayorCG 
		Where Codigo Between @CuentaInicial and  @CuentaFinal and Fecha between @Fecha1 and @Fecha2 
		Group By Codigo, Descripcion
		Order By Codigo
		Open db_CursorPantalla   
		Fetch Next From db_CursorPantalla Into @vCodigo, @vDescripcion, @vDebito, @vCredito
		While @@FETCH_STATUS = 0 Begin 
		
			Set @vPrefijo = SubString(@vCodigo, 0, 2)
			
			--Cuentas del Debe
			If @vPrefijo = '1' Or @vPrefijo = '5' Begin
				Set @vSaldoAnterior = IsNull((Select SUM(Debito) - SUM(Credito) From VLibroMayorCG Where Codigo=@vCodigo And Fecha<@Fecha1), 0)
				Set @vSaldoFinal = (@vSaldoAnterior + @vDebito) - @vCredito
			End
			
			--Cuentas del Haber
			If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
				Set @vSaldoAnterior = IsNull((Select SUM(Credito) - SUM(Debito) From VLibroMayorCG Where Codigo=@vCodigo And Fecha<@Fecha1), 0)
				Set @vSaldoFinal = (@vSaldoAnterior + @vCredito) - @vDebito
			End
			
			Insert Into  #TablaTemporalPantalla(ID, Codigo, Denominacion, [Saldo Anterior], Debitos, Creditos,[Saldo Final]) 
			Values (@vID, @vCodigo, @vDescripcion, @vSaldoAnterior, @vDebito, @vCredito, @vSaldoFinal)
			
			Fetch Next From db_CursorPantalla Into @vCodigo, @vDescripcion, @vDebito, @vCredito
			
		End
		
		--Cierra el cursor
		Close db_CursorPantalla   
		Deallocate db_CursorPantalla		   			
		
		Select * From #TablaTemporalPantalla Where ID=@vID Order By CONVERT(INT, Codigo)
				
	End

End
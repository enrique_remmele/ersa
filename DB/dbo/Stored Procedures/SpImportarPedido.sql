﻿CREATE Procedure [dbo].[SpImportarPedido]

	--Entrada
	--Punto de Expedicion
	@TipoComprobante varchar(10),
	@ReferenciaSucursal varchar(5),
	@ReferenciaPuntoExpedicion varchar(5),
	@NroComprobante varchar(50),
	
	--Cliente
	@Cliente varchar(50),
	@Referencia varchar(50),
	@RazonSocial varchar(50),
	@Direccion varchar(200),
	
	--
	@FechaEmision varchar(50),
	@Sucursal varchar(50),
	@Deposito varchar(50),
	@Vendedor varchar(100),
	@Observacion varchar(200),
	
	--Datos Extras
	@NroPedido int, 
	@Operador varchar(10),
	@NroFactura varchar(50),
	@FechaFactura varchar(50),
	
	--Configuraciones
	@Anulado bit,
	
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	
	@Actualizar bit
	
As

Begin
		
	--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		
		Declare @vOperacion varchar(50)
		Declare @vIDTransaccion int
		
		Declare @vNumero int
		Declare @vTelefono varchar(100)
		Declare @vEsPedidoSucursal bit
		
		--Codigos
		Declare @vIDCliente int
		Declare @vIDSucursalCliente int
		Declare @vIDSucursal int
		Declare @vIDDeposito int
		Declare @vIDListaPrecio int
		Declare @vIDVendedor int
		
	End
	
	--Establecer valores predefinidos
	Begin
		
		Set @vOperacion = 'INS'
		Set @vTelefono = ''
		Set @vEsPedidoSucursal = 'False'
		
	End
	
	--Verificar si ya existe
	If @NroPedido = 0 Begin
		Set @NroPedido = @NroFactura
	End
	
	If Exists(Select * From PedidoImportado Where NroPedido=@NroPedido) Begin
		set @vIDTransaccion = (Select IDTransaccion From PedidoImportado Where NroPedido=@NroPedido)
		Set @vOperacion = 'UPD'
	End
	
	--ACTUALIZAR
	If @vOperacion = 'UPD' Begin
	
		If @Actualizar = 'True' Begin
			
			--Cliente
			Set @vIDCliente=(IsNull((Select Top(1) ID From Cliente Where Referencia=@Referencia And RUC=@Cliente),0))
			If @vIDCliente = 0 Begin
				Set @vMensaje = 'Cliente incorrecto!'
				Set @vProcesado = 'False'
				GoTo Salir
			End
			
			Update PedidoImportado Set NroFactura=@NroFactura,
										FEchaFactura=@FechaFactura
			Where IDTransaccion = @vIDTransaccion
			
			If (Select Facturado From Pedido Where IDTransaccion=@vIDTransaccion) = 'True' Begin
				Set @vMensaje = 'Ya se facturo!'
				Set @vProcesado = 'False'
				GoTo Salir
			End
			
			--Pedido
			Update Pedido Set Anulado=@Anulado, IDCliente=@vIDCliente
			Where IDTransaccion = @vIDTransaccion
			
			--Eliminar su detalle
			Delete From DescuentoPedido Where IDTransaccion=@vIDTransaccion
			Delete From DetalleImpuesto Where IDTransaccion=@vIDTransaccion
			Delete From DetallePedido Where IDTransaccion=@vIDTransaccion
			
			Set @vMensaje = 'Actualizado!!!'
			Set @vProcesado = 'True'
			GoTo Salir
			
		End
		
		Set @vMensaje = 'Sin Act.!'
		Set @vProcesado = 'True'
		GoTo Salir
		
	End
	
	--Hayar Valores
	Begin
		
		Set @vMensaje = 'No se proceso!'
		Set @vProcesado = 'False'
		
		--Punto de Expedicion
		Set @ReferenciaSucursal = '0' + dbo.FFormatoDosDigitos(CONVERT(tinyint, @ReferenciaSucursal))
		Set @ReferenciaPuntoExpedicion = '0' + dbo.FFormatoDosDigitos(CONVERT(tinyint, @ReferenciaPuntoExpedicion))
				
		--Cliente
		Set @vIDCliente=(IsNull((Select Top(1) ID From Cliente Where Referencia=@Referencia And RUC=@Cliente),0))
		If @vIDCliente = 0 Begin
			Set @vMensaje = 'Cliente incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Sucursal Cliente
		Set @vIDSucursalCliente = IsNull((Select Top(1) ID From ClienteSucursal Where IDCliente=@vIDCliente And Direccion=@Direccion),0)
		If @vIDSucursalCliente > 0 Begin
			Set @vEsPedidoSucursal = 'True'
			Set @vTelefono = IsNull((Select Top(1) Telefono From ClienteSucursal Where IDCliente=@vIDCliente And ID=@vIDSucursalCliente),'')
		End
		
		If @vIDSucursalCliente = 0 Begin
			Set @vTelefono = IsNull((Select Top(1) Telefono From Cliente Where ID=@vIDCliente),'')
		End		
		
		--Sucursal
		Set @vIDSucursal = (IsNull((Select Top(1) ID From Sucursal Where Descripcion=@Sucursal),0))
		If @vIDSucursal = 0 Begin
			Set @vMensaje = 'Sucursal incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Deposito
		Set @vIDDeposito = (IsNull((Select Top(1) ID From Deposito Where IDSucursal=@vIDSucursal And Descripcion=@Deposito),0))
		If @vIDDeposito = 0 Begin
			Set @vMensaje = 'Deposito incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Lista de Precio
		Set @vIDListaPrecio = (IsNull((Select IDListaPrecio  From Cliente Where ID=@vIDCliente),0))
		If @vIDListaPrecio = 0 Begin
			Set @vMensaje = 'Lista de precio incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Vendedor
		Set @vIDVendedor = (IsNull((Select Top(1) ID  From Vendedor Where Nombres=@Vendedor),0))
		If @vIDVendedor = 0 Begin
			Set @vMensaje = 'Vendedor incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Actualizamos el Cliente
		If (Select IDSucursal From Cliente Where ID=@vIDCliente) Is Null Begin
			Update Cliente Set IDSucursal=@vIDSucursal
			Where ID=@vIDCliente
		End
		
	End
	
	--INSERTAR
	If @vOperacion = 'INS' Begin
		
		--Validar Informacion
		Begin
		
			--Numero
			If Exists(Select * From PedidoImportado Where NroPedido=@NroPedido) Begin
				set @vMensaje = 'El sistema detecto que el numero de comprobante ya esta registrado! Obtenga un numero siguiente.'
				set @vProcesado = 'False'
				GoTo Salir
			End			
			
		End
		
		----Insertar la transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@MostrarTransaccion = 'False',
			@Mensaje = @vMensaje OUTPUT,
			@Procesado = @vProcesado OUTPUT,
			@IDTransaccion = @vIDTransaccion OUTPUT
		
		If @vProcesado = 'False' Begin
			GoTo Salir
		End
		
		--Obtener Pedido
		Set @vNumero = ISNULL((Select MAX(Numero)+1 From Pedido Where IDSucursal=@vIDSucursal),'1')
				
		--Insertar en Pedido
		Insert Into Pedido(IDTransaccion, Numero, IDCliente, IDSucursalCliente, Direccion, Telefono, Fecha, FechaFacturar, IDSucursal, IDDeposito, IDListaPrecio,IDVendedor, IDMoneda, Cotizacion, Observacion, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, EsVentaSucursal, Anulado, FechaAnulado, IDUsuarioAnulado, Procesado)
		Values(@vIDTransaccion, @vNumero, @vIDCliente, @vIDSucursalCliente, @Direccion, @vTelefono, @FechaEmision, @FechaEmision, @vIDSucursal, @vIDDeposito, @vIDListaPrecio,@vIDVendedor, 1, 1, @Observacion, 0, 0, 0, 0, @vEsPedidoSucursal, @Anulado, NULL, NULL, 'True')
		
		--Insertar en PedidoImportacion
		Insert Into PedidoImportado(IDTransaccion, NroPedido, Operador, NroFactura, FechaFactura)
		Values(@vIDTransaccion, @NroPedido, @Operador, @NroFactura, @FechaFactura)
		
		Set @vMensaje = 'Registro guardado!'
		Set @vProcesado = 'True'
		GoTo Salir
				
	End
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


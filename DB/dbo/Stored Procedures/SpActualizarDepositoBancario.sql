﻿CREATE Procedure [dbo].[SpActualizarDepositoBancario]

	--Entrada
	@IDTransaccion int,
	@Comprobante varchar(50),
	@Fecha date,
	@Observacion varchar(150),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin
			
			
	--Actualizar 
	Update DepositoBancario  Set NroComprobante=@Comprobante,
								 Fecha=@Fecha,
								Observacion =@Observacion 
	Where IDTransaccion=@IDTransaccion 
	
	
	Update Asiento Set Fecha=@Fecha
	Where IDTransaccion=@IDTransaccion
			
	set @Mensaje = 'Registro guardado'
	set @Procesado = 'True'
	return @@rowcount
		

End
	
	


﻿CREATE Procedure [dbo].[SpViewExtractoMovimientoProveedorFacturaCCSaldo]

	--Identificadores
	@FechaPago Date,
	@CuentaContable varchar(20) = '',
	@IDProveedor int = 0
	
As

Begin

Select 
C.IDTransaccion, 
C.Operacion,
C.Codigo,
'Proveedor'=Concat(P.Ruc , '-', P.RazonSocial),
C.Fecha,
C.Documento,
C.[Detalle/Concepto],
'Debito'=Sum(Isnull(D.Debito,0)),
'Credito'=Isnull(C.Credito,0),
'Saldo'=isnull(C.Credito,0)-Sum(Isnull(D.Debito,0)),
C.Cotizacion,
'CreditoGs'=Isnull(C.Credito,0)* C.Cotizacion,
'SaldoGs'=(C.Credito-Sum(Isnull(D.Debito,0))) *C.Cotizacion,
C.IDMoneda,
C.Movimiento,
C.ComprobanteAsociado,
--'FechaPago'=D.Fecha,
C.CodigoCC
from VExtractoMovimientoProveedorFacturaCCCredito C
join Proveedor P on C.Codigo = P.ID
left outer join VExtractoMovimientoProveedorFacturaDebito D ON C.IDTransaccion = D.ComprobanteAsociado and Isnull(D.Fecha,GetDate()) <=@FechaPago
--where Isnull(D.Fecha,C.Fecha) <=@FechaPago
where Isnull(C.Fecha,GetDate()) <=@FechaPago
and (Case when @CuentaContable = '' then '' else C.CodigoCC end) = (Case when @CuentaContable = '' then '' else @CuentaContable end)
--and (Case when @CuentaContable = '' then '' else D.Codigo end) = (Case when @CuentaContable = '' then '' else @CuentaContable end)
and (Case when @IDProveedor = 0 then 0 else C.Codigo end) = (Case when @IDProveedor = 0 then 0 else @IDProveedor end)
Group by C.IDTransaccion, 
C.Operacion,
C.Codigo,
C.Fecha,
C.Documento,
C.[Detalle/Concepto],
C.Debito,
C.Credito,
C.IDMoneda,
C.Movimiento,
C.ComprobanteAsociado,
C.CodigoCC,
P.Ruc,
P.RazonSocial,
C.Cotizacion
having isnull(C.Credito,0)-Sum(Isnull(D.Debito,0)) > 0
order by C.Fecha
		
	
End




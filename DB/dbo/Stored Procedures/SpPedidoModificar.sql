﻿
CREATE Procedure [dbo].[SpPedidoModificar]

	--Entrada
	@IDTransaccion as numeric(18,0),
	@Fecha as date,
	@EntregaCliente as bit = 'False',
	@RestriccionHorariaEntrega as bit = 0,
	@HoraDesde as time = null,
	@HoraHasta as time = null,
	--Operacion
	@Operacion varchar(50),
	@IDUsuario int=NULL,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric output
	
	
As

Begin

	If @Operacion = 'UPD' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Pedido Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			Set @IDTransaccionSalida = 0
			return @@rowcount
		End
		If Exists(select * from vPedidoVenta where IDTransaccionPedido=@IDTransaccion) Begin
			set @Mensaje = 'El pedido ya esta facturado.'
			set @Procesado = 'False'
			Set @IDTransaccionSalida = 0
			return @@rowcount
		End

		If Exists(Select * From Pedido Where IDTransaccion=@IDTransaccion and cast(Fecha as date) > cast(FechaFacturar as date)) Begin
			set @Mensaje = 'La fecha de carga no puede ser mayor a la de facturacion.'
			set @Procesado = 'False'
			Set @IDTransaccionSalida = 0
			return @@rowcount
		End

		Insert into aPedidoModificado(IDTransaccion, FechaFacturar,EntregaCliente,IDUsuarioModificacion,FechaModificacion)
		(Select IDTransaccion, FechaFacturar, EntregaCliente, @IDUsuario, Getdate() from Pedido where IDTransaccion = @IDTransaccion)
		----Anular
		Update Pedido Set FechaFacturar = @Fecha,
		EntregaCliente = @EntregaCliente,
		RestriccionHorariaEntrega=@RestriccionHorariaEntrega,
		HoraDesde=@HoraDesde,
		HoraHasta=@HoraHasta
		Where IDTransaccion = @IDTransaccion
		and Anulado =  0
	
						
		Set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
			
	End
	
	Set @Mensaje = 'No se proceso ningun registro!'
	Set @IDTransaccionSalida = 0
	set @Procesado = 'False'
	Return @@rowcount
	
End


﻿
CREATE Procedure [dbo].[SpAsientoMovimientoDescargaStock]

	@IDTransaccion numeric(18,0)
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint = 0
	
	--Movimiento
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoOperacion smallint
	Declare @vIDTipoComprobante smallint
	Declare @vIDDepositoEntrada smallint
	Declare @vIDDepositoSalida smallint
	Declare @vEntrada bit
	Declare @vSalida bit
	Declare @vComprobante varchar(50)
	Declare @vFecha varchar(50)
	Declare @vObservacion varchar(100)
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money

	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	Declare @vCodigoDepositoEntrada varchar(50)
	Declare @vCodigoDepositoSalida varchar(50)
	Declare @vCodigoProducto varchar(50)

	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money
	Declare @vBuscarEnProducto bit
	

	Declare @MovimientoStock bit
	Declare @DescargaStock bit
	
	--Obtener valores
	Begin
	
		Set @vIDMoneda = 1
		Set @vCotizacion = 1
		
		Select	@vIDSucursal=IDSucursal,
				@vIDTipoOperacion=IDTipoOperacion,
				@vTipoComprobante=TipoComprobante,
				@vIDTipoComprobante=IDTipoComprobante,
				@vIDDepositoEntrada=IDDepositoEntrada,
				@vIDDepositoSalida=IDDepositoSalida,
				@vEntrada=Entrada,
				@vSalida=Salida,
				@vComprobante=Comprobante,
				@vFecha=Fecha,
				@vObservacion=Observacion,
				@MovimientoStock = MovimientoStock,
				@DescargaStock = DescargaStock

		From VMovimiento Where IDTransaccion=@IDTransaccion and (DescargaStock = 'True') 
		
	End
					
	--Verificar que el asiento se pueda modificar
	Begin
	
		--Si no es un movimiento por descarga de stock
		If not exists(Select * From Movimiento Where IDTransaccion=@IDTransaccion and DescargaStock = 1) Begin
			print 'La operacion no es un movimiento por descarga de stock'
			GoTo salir
		End 

		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta anulado
		If (Select Anulado From Movimiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El registro esta anulado'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 
		
		Set @vCodigoDepositoEntrada = IsNull((Select CuentaContableMercaderia From Deposito Where ID=@vIDDepositoEntrada), '')
		Set @vCodigoDepositoSalida = IsNull((Select CuentaContableMercaderia From Deposito Where ID=@vIDDepositoSalida), '')
	
		
	End
				
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = @vObservacion
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	
	--Cuentas para Transferencias
	If @vEntrada=1 And @vSalida=1 Begin
	
		Declare cCFTranferencia cursor for
		Select Codigo, Debe, Haber, BuscarEnProducto From VCFMovimiento 
		Where IDTipoOperacion=@vIDTipoOperacion 
		And IDTipoComprobante=@vIDTipoComprobante
		Open cCFTranferencia
		fetch next from cCFTranferencia into @vCodigo, @vDebe, @vHaber, @vBuscarEnProducto
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporte = (Select SUM(Total) From vDetalleMovimiento Where IDTransaccion=@IDTransaccion)
					
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			Set @vID = (Select ISNULL(Max(ID)+1,1) From vDetalleAsiento Where IDTransaccion=@IDTransaccion)
			
			if @vDebe = 'True' begin
				
				Declare cCxDetalleMovimiento cursor for
				Select --P.CuentaContableCompra, 
				D.CuentaContableEntrada,
				Sum(D.Total) 
				From vDetalleMovimiento D 
				--Join Producto P On D.IDProducto=P.ID 
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, D.CuentaContableEntrada 
				--,P.CuentaContableCompra
				Open cCxDetalleMovimiento 
				fetch next from cCxDetalleMovimiento into @vCodigoProducto, @vImporte
				While @@FETCH_STATUS = 0 Begin  
		
					If @vCodigoProducto != '' Begin
						Set @vCodigo = @vCodigoProducto 
					End

					Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
					
					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, Round(@vImporte,@vRedondeo), 0, '')
					
					End
			
					fetch next from cCxDetalleMovimiento into @vCodigoProducto, @vImporte

				End
		
				close cCxDetalleMovimiento
				deallocate cCxDetalleMovimiento

			end
			else begin
				
				--if @vIDTipoComprobante = 90 begin --Dañado Interno almacen
					Declare cCxDetalleMovimiento cursor for
					Select --P.CuentaContableCompra, 
					D.CuentaContableSalida,
					Sum(D.Total) 
					From vDetalleMovimiento D 
					--Join Producto P On D.IDProducto=P.ID 
					Where D.IDTransaccion=@IDTransaccion
					Group By D.IDTransaccion, D.CuentaContableSalida --, P.CuentaContableCompra
					Open cCxDetalleMovimiento 
					fetch next from cCxDetalleMovimiento into @vCodigoProducto, @vImporte
					While @@FETCH_STATUS = 0 Begin  
		
						If @vCodigoProducto != '' Begin
							Set @vCodigo = @vCodigoProducto 
						End

						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
					
						If @vImporte > 0 Begin		
						
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporte,@vRedondeo), 0, 0, '')
					
						End
			
						fetch next from cCxDetalleMovimiento into @vCodigoProducto, @vImporte

					End
		
					close cCxDetalleMovimiento
					deallocate cCxDetalleMovimiento
				--end 
				--else begin
					
				--	if @vImporte > 0 begin
				--		Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				--		Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporte,@vRedondeo), 0, 0, '')
				--	end

				--end
			end
			
			fetch next from cCFTranferencia into @vCodigo, @vDebe, @vHaber, @vBuscarEnProducto
			
		End
		
		close cCFTranferencia
		deallocate cCFTranferencia

	End

	--Cuentas para Entrada
	If @vEntrada=1 And @vSalida=0 Begin
	
		Declare cCFEntrada cursor for
		Select Codigo, Debe, Haber, BuscarEnProducto From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFEntrada 
		fetch next from cCFEntrada into @vCodigo, @vDebe, @vHaber, @vBuscarEnProducto
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			print concat('CODIGO ', @vCodigo)
			print concat('TIPO ', @vDebe, ' ', @vHaber )
			--Cuenta del debe
			If @vDebe = 1 Begin
				print concat('ENTRO A DEBE ', @vCodigo)
				Declare cCFDetalleMovimiento cursor for
				Select P.CuentaContableCompra, CC.IDCuentaContable, Sum(D.Total) 
				From vDetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				--Join TipoProducto TP On P.IDTipoProducto=TP.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCompra
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCompra, CC.IDCuentaContable
				Open cCFDetalleMovimiento 
				fetch next from cCFDetalleMovimiento into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
		
					If @vCodigoProducto != '' Begin
						Set @vCodigo = @vCodigoProducto 
					End

					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0,  Round(@vImporte,@vRedondeo),Round(@vImporte,@vRedondeo), '')
					
					End
			
					fetch next from cCFDetalleMovimiento into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimiento
				deallocate cCFDetalleMovimiento

			End
			
			If @vHaber = 1  and @vBuscarEnProducto = 0 Begin
				print concat('ENTRO A HABER ', @vCodigo)
				If @vCodigoDepositoEntrada != '' Begin
					Set @vCodigo = @vCodigoDepositoEntrada
				End

				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				print concat('IDCUENTACONTABLE ', @vIDCuentaContable)
				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From vDetalleMovimiento Where IDTransaccion=@IDTransaccion)
				print concat('Importe ', @vImporte)

				If @vImporte > 0 Begin		
					print concat('Insert ', @vImporte)
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo,  Round(@vImporte,@vRedondeo), 0,Round(@vImporte,@vRedondeo), '')
					print concat('TRansaccion ', @IDTransaccion)
				End

			End			
			
			If @vHaber = 1  and @vBuscarEnProducto = 1 Begin
				Declare cCFDetalleMovimiento cursor for
				Select P.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
				From vDetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				--Join TipoProducto TP On P.IDTipoProducto=TP.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCompra
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCosto, CC.IDCuentaContable
				Open cCFDetalleMovimiento 
				fetch next from cCFDetalleMovimiento into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
		
					If @vCodigoProducto != '' Begin
						Set @vCodigo = @vCodigoProducto 
					End

					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporte,@vRedondeo),  0,Round(@vImporte,@vRedondeo), '')
					
					End
			
					fetch next from cCFDetalleMovimiento into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimiento
				deallocate cCFDetalleMovimiento


			End				
			
			fetch next from cCFEntrada into @vCodigo, @vDebe, @vHaber, @vBuscarEnProducto
			
		End
		
		close cCFEntrada
		deallocate cCFEntrada

	End

	--Cuentas para Salida
	If @vEntrada=0 And @vSalida=1 Begin
	
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber, BuscarEnProducto From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber, @vBuscarEnProducto
		
		While @@FETCH_STATUS = 0 Begin  
			print 'aqui'
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			--Cuenta del debe
			If @vDebe = 1 and @vIDTipoComprobante <> 89 and @vIDTipoComprobante <> 71 and @vBuscarEnProducto <>1 Begin -- QUe no sea MOVINT
				print 'primero'
				print @vCodigoDepositoSalida

				
				 
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				
				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From vDetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				Set @vImporteHaber = @vImporte

				If @vImporteHaber > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')
				
				End

			End

			If @vDebe = 1 and @vIDTipoComprobante <> 89 and @vIDTipoComprobante <> 71 and @vBuscarEnProducto = 1 begin
				print 'segundo'
				Declare cCFDetalleMovimientoSalida cursor for
					Select P.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
					From vDetalleMovimiento D Join Producto P On D.IDProducto=P.ID 
					--Join TipoProducto TP On P.IDTipoProducto=TP.ID 
					Join VCuentaContable CC On CC.Codigo=P.CuentaContableCosto 
					Where D.IDTransaccion=@IDTransaccion
					Group By D.IDTransaccion, P.CuentaContableCosto, CC.IDCuentaContable
					Open cCFDetalleMovimientoSalida 
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
					While @@FETCH_STATUS = 0 Begin  
		
						If @vCodigoProducto != '' Begin
							Set @vCodigo = @vCodigoProducto 
						End

						If @vImporte > 0 Begin		
						
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')
					
						End
			
						fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

					End
		
					close cCFDetalleMovimientoSalida
					deallocate cCFDetalleMovimientoSalida
			end
			
			If @vDebe = 1 and @vIDTipoComprobante = 89 Begin -- Solo para MOVINT				 
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				
				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From vDetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				Set @vImporteHaber = @vImporte

				If @vImporteHaber > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')
				
				End

			End

			If @vHaber = 1 and @vIDTipoComprobante = 71  Begin -- Solo para REPRO				 
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				
				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From vDetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				Set @vImporteHaber = @vImporte

				If @vImporteHaber > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')
				
				End

			End
			
			--If @vHaber = 1  and @DescargaStock = 'False' and @vIDTipoComprobante <> 89 and @vIDTipoComprobante <> 71 Begin -- que no sea MOVINT

			--	Declare cCFDetalleMovimientoSalida cursor for
			--	Select TP.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
			--	From vDetalleMovimiento D Join Producto P On D.IDProducto=P.ID Join TipoProducto TP On P.IDTipoProducto=TP.ID Join VCuentaContable CC On CC.Codigo=TP.CuentaContableCosto 
			--	Where D.IDTransaccion=@IDTransaccion
			--	Group By D.IDTransaccion, TP.CuentaContableCosto, CC.IDCuentaContable
			--	Open cCFDetalleMovimientoSalida 
			--	fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
			--	While @@FETCH_STATUS = 0 Begin  
		
			--		IF @vIDTipoComprobante <> 72 begin --Si es distinto a Destruccion
			--			If @vCodigoProducto != '' Begin
			--				Set @vCodigo = @vCodigoProducto 
			--			End
			--		end

			--		If @vImporte > 0 Begin		
						
			--			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

			--			Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
			--			Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporte, 0, @vImporte, '')
					
			--		End
			
			--		fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

			--	End
		
			--	close cCFDetalleMovimientoSalida
			--	deallocate cCFDetalleMovimientoSalida

			--End
			--If @vHaber = 1  and @DescargaStock = 'False' and @vIDTipoComprobante = 71 Begin -- que no sea REPRO

			--	Declare cCFDetalleMovimientoSalida cursor for
			--	Select TP.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
			--	From vDetalleMovimiento D Join Producto P On D.IDProducto=P.ID Join TipoProducto TP On P.IDTipoProducto=TP.ID Join VCuentaContable CC On CC.Codigo=TP.CuentaContableCosto 
			--	Where D.IDTransaccion=@IDTransaccion
			--	Group By D.IDTransaccion, TP.CuentaContableCosto, CC.IDCuentaContable
			--	Open cCFDetalleMovimientoSalida 
			--	fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
			--	While @@FETCH_STATUS = 0 Begin  
		
			--		IF @vIDTipoComprobante <> 72 begin --Si es distinto a Destruccion
			--			If @vCodigoProducto != '' Begin
			--				Set @vCodigo = @vCodigoProducto 
			--			End
			--		end

			--		If @vImporte > 0 Begin		
						
			--			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

			--			Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
			--			Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo,0, @vImporte, @vImporte, '')
					
			--		End
			
			--		fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

			--	End
		
			--	close cCFDetalleMovimientoSalida
			--	deallocate cCFDetalleMovimientoSalida

			--End
			--If @vHaber = 1  and @DescargaStock = 'False' and @vIDTipoComprobante = 89 Begin -- que sea MOVINT

			--	Declare cCFDetalleMovimientoSalida cursor for
			--	Select TP.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
			--	From vDetalleMovimiento D Join Producto P On D.IDProducto=P.ID Join TipoProducto TP On P.IDTipoProducto=TP.ID Join VCuentaContable CC On CC.Codigo=TP.CuentaContableCosto 
			--	Where D.IDTransaccion=@IDTransaccion
			--	Group By D.IDTransaccion, TP.CuentaContableCosto, CC.IDCuentaContable
			--	Open cCFDetalleMovimientoSalida 
			--	fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
			--	While @@FETCH_STATUS = 0 Begin  
			--		If @vImporte > 0 Begin		
						
			--			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

			--			Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
			--			Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporte, 0, @vImporte, '')
					
			--		End
			
			--		fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

			--	End
		
			--	close cCFDetalleMovimientoSalida
			--	deallocate cCFDetalleMovimientoSalida

			--End	

			If @vHaber = 1 and @vIDTipoComprobante<>71  and @DescargaStock = 'True' and @vBuscarEnProducto <> 1 Begin

				--Declare cCFDetalleMovimientoSalida cursor for
				--Select P.CuentaContableCompra, CC.IDCuentaContable, Sum(D.Total) 
				--From vDetalleMovimiento D Join Producto P On D.IDProducto=P.ID 
				----Join TipoProducto TP On P.IDTipoProducto=TP.ID 
				--Join VCuentaContable CC On CC.Codigo=P.CuentaContableCosto 
				--Where D.IDTransaccion=@IDTransaccion
				--Group By D.IDTransaccion, P.CuentaContableCompra, CC.IDCuentaContable
				--Open cCFDetalleMovimientoSalida 
				--fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				--While @@FETCH_STATUS = 0 Begin  
		
				--	If @vCodigoProducto != '' Begin
				--		Set @vCodigo = @vCodigoProducto 
				--	End

				If @vCodigoDepositoSalida != '' Begin
					Set @vCodigo = @vCodigoDepositoSalida
				End
				
				Set @vImporte = (Select SUM(Total) From vDetalleMovimiento Where IDTransaccion=@IDTransaccion)
				
				If @vImporte > 0 Begin		
						
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporte,@vRedondeo), 0, Round(@vImporte,@vRedondeo), '')
					
				End
			
				--	fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				--End
		
				--close cCFDetalleMovimientoSalida
				--deallocate cCFDetalleMovimientoSalida

			End				
			
			If @vHaber = 1 and @vIDTipoComprobante <> 71 and @vBuscarEnProducto = 1 begin
				print 'segundo'
				Declare cCFDetalleMovimientoSalida cursor for
					Select P.CuentaContableCompra, CC.IDCuentaContable, Sum(D.Total) 
					From vDetalleMovimiento D Join Producto P On D.IDProducto=P.ID 
					Join VCuentaContable CC On CC.Codigo=P.CuentaContableCosto 
					Where D.IDTransaccion=@IDTransaccion
					Group By D.IDTransaccion, P.CuentaContableCompra, CC.IDCuentaContable
					Open cCFDetalleMovimientoSalida 
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
					While @@FETCH_STATUS = 0 Begin  
		
						If @vCodigoProducto != '' Begin
							Set @vCodigo = @vCodigoProducto 
						End

						If @vImporte > 0 Begin		
						
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporte,@vRedondeo), 0, Round(@vImporte,@vRedondeo), '')
					
						End
			
						fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

					End
		
					close cCFDetalleMovimientoSalida
					deallocate cCFDetalleMovimientoSalida
			end

			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber, @vBuscarEnProducto
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End

	--Actualizamos la cabecera, el total
	Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
	Set @vImporteHaber = ROUND(@vImporteHaber, @vRedondeo)
	Set @vImporteDebe = ROUND(@vImporteDebe, @vRedondeo)
	
	Update Asiento Set Total = @vImporteHaber,
						Credito = @vImporteHaber,
						Debito = @vImporteDebe,
						Saldo = @vImporteHaber - @vImporteDebe
	Where IDTransaccion=@IDTransaccion
	
	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
	
End


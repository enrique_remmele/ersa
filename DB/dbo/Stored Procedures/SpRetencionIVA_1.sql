﻿CREATE Procedure [dbo].[SpRetencionIVA]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDPuntoExpedicion int = Null,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante int = NULL,			
	@IDProveedor int = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@Fecha date = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money= NULL,
	@Observacion varchar(100) = NULL,
	@TotalIVA money = NULL,
	@TotalRenta money = NULL,
	@Total money=NULL,
	@IDTransaccionOrdenPago numeric(18,0)=NULL,
	@IDTransaccionEgreso numeric(18,0)=NULL,
	@OP bit=NULL,
			
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
   	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		


As

Begin

	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from RetencionIVA where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
	--Varios
   --04-06-2021 SMC Nueva linea de abajo
   --declare @IDTransaccionEgreso numeric(18,0)
   declare @vObservacionATI varchar(200)		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Validar Fecha Timbrado 
		declare @fechaTimbrado as datetime
		select @fechaTimbrado = Vencimiento from PuntoExpedicion where Id = @IdPuntoExpedicion
		if @fechaTimbrado < @Fecha begin
			Set @Mensaje  = 'La fecha del timbrado ha vencido. Por favor seleccione otro numero de timbrado.'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		end

		--Comprobante
		if Exists(Select * From RetencionIVA  Where IDPuntoExpedicion=@IDPuntoExpedicion And NroComprobante=@NroComprobante) Begin
			set @Mensaje = 'El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		
		--Validar que la OP del Proveedor no haya sido usada
		--if Exists(Select * From RetencionIVA  Where IDProveedor=@IDProveedor And IDTransaccionOrdenPago=@IDTransaccionOrdenPago And Anulado='False' And OP='True') Begin
		--	set @Mensaje = 'Esta OP pertenece a otra Retención'
		--	set @Procesado = 'False'
		--	set @IDTransaccionSalida  = 0
		--	return @@rowcount
		--End
				
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--Calcular Saldo....
		--Si es Sin Comprobantes, SALDO = @TOTAL
		--Si no, SALDO = 0
		--If @ConComprobantes = 'False' Begin
		--	Set @Saldo = @Total 		
		--End Else Begin
		--	Set @Saldo = 0
		--End
		
		--04-06-2021 SMC Nueva linea de abajo
		--set @IDTransaccionEgreso = (select VOPE.IDTransaccion from VOrdenPagoEgreso VOPE where VOPE.IDTransaccionOrdenPago = @IDTransaccionOrdenPago and VOPE.RetencionIVA =@TotalIVA)
		--hasta aqui	
		
		set  @vObservacionATI = concat('RetencionIVA:', @NroComprobante, ' - Monto:',@TotalIVA, @Observacion)
				
		--INICIO SM 24062021 - Auditoria Informática
		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,@Operacion,(Select Descripcion from Operacion where id=@IDOperacion),@vObservacionATI,@IDTransaccionSalida,'RETENCIONIVA' )
		--FIN SM 24062021 - Auditoria Informática
			
		--Insertar en Nota de Credito
		Insert Into RetencionIVA (IDTransaccion, IDPuntoExpedicion, IDTipoComprobante, NroComprobante, IDProveedor, IDSucursal,  Fecha, IDMoneda, Cotizacion, Observacion,TotalIVA,TotalRenta,  Anulado,FechaAnulado, IDUsuarioAnulado, Procesado,IDTransaccionOrdenPago,Total,OP )
		Values(@IDTransaccionSalida, @IDPuntoExpedicion, @IDTipoComprobante, @NroComprobante, @IDProveedor,  @IDSucursalOperacion, @Fecha, @IDMoneda, @Cotizacion, @Observacion, @TotalIVA,@TotalRenta,'False', NULL, NULL, 'True',@IDTransaccionOrdenPago,@Total,@OP )

		set  @vObservacionATI = concat('RetencionIVA:', @NroComprobante, ' - Monto:',@TotalIVA, ' PuntoExpedicion: ',@IDPuntoExpedicion, ' UltimoNumero:',@NroComprobante)
				
		--INICIO SM 24062021 - Auditoria Informática
		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacionATI,@IDPuntoExpedicion,'PUNTOEXPEDICION' )
		--FIN SM 24062021 - Auditoria Informática

		--Actualizar Punto de Expedicion
		Update PuntoExpedicion Set UltimoNumero=@NroComprobante
		Where ID=@IDPuntoExpedicion


		set  @vObservacionATI = concat('RetencionIVA:', @NroComprobante, ' - Monto:',@TotalIVA, ' OrdenPagoEgreso - IdTransaccionEgreso: ',@IdTransaccionEgreso, ' - IdTransaccionOP:',@IDTransaccionOrdenPago, ' - ProcesaRetencion:','TRUE')
				
		--INICIO SM 24062021 - Auditoria Informática
		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacionATI,@IDTransaccionEgreso,'ORDENPAGOEGRESO' )
		--FIN SM 24062021 - Auditoria Informática

		--04-06-2021 SMC Nueva linea de abajo
		--Actualizar OrdenPagoEgreso
		Update OrdenPagoEgreso
		Set ProcesaRetencion='True'
		Where IDTransaccionEgreso =@IDTransaccionEgreso
		  and IDTransaccionOrdenPago = @IDTransaccionOrdenPago
		--hasta aqui
										
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
				
				
		----Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
						
		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from RetencionIVA where IDTransaccion = @IDTransaccion)
		set @IDProveedor = (select IDProveedor from RetencionIVA where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from RetencionIVA where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from RetencionIVA where IDTransaccion = @IDTransaccion)
		declare @Comprobante varchar(16)= (select Comprobante from vRetencionIVA where IDTransaccion = @IDTransaccion)
		declare @VTipoComprobante varchar(16) = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from RetencionIVA where IDTransaccion = @IDTransaccion)
		declare @Condicion varchar(16) = 'RETENCION'
		--sm -24062021
		set @NroComprobante = (select NroComprobante from vRetencionIVA where IDTransaccion = @IDTransaccion)
		set @TotalIVA = (select TotalIVA from RetencionIVA where IDTransaccion = @IDTransaccion)
		
		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,0,@IDProveedor,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ANU',@VTipoComprobante, @IDSucursal)
		
		--04-06-2021 SMC Nueva linea de abajo				
		set @IDTransaccionOrdenPago = (select vri.IDTransaccionOrdenPago from vRetencionIVA vri where vri.IDTransaccion = @IDTransaccion)
		set @IDTransaccionEgreso = (select vri.IDTransaccionEgreso from vRetencionIVA vri where vri.IDTransaccion = @IDTransaccion)
		--hasta aqui
		 
		set  @vObservacionATI = concat('RetencionIVA:', @NroComprobante, ' - Monto:',@TotalIVA, ' *Anula Comprobante*')
				
		--INICIO SM 24062021 - Auditoria Informática
		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,@Operacion,(Select Descripcion from Operacion where id=@IDOperacion),@vObservacionATI,@IDTransaccion,'RETENCIONIVA' )
		--FIN SM 24062021 - Auditoria Informática
	   						
		Update RetencionIVA  Set Anulado='True'
		Where IDTransaccion = @IDTransaccion
		
		set  @vObservacionATI = concat('RetencionIVA:', @NroComprobante, ' - Monto:',@TotalIVA, ' *OrdenPagoEgreso - IdTransaccionEgreso: ',@IdTransaccionEgreso, ' - IdTransaccionOP:',@IDTransaccionOrdenPago, ' - ProcesaRetencion:','FALSE')
				
		--INICIO SM 24062021 - Auditoria Informática
		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacionATI,@IDTransaccionEgreso,'ORDENPAGOEGRESO' )
		--FIN SM 24062021 - Auditoria Informática

		--04-06-2021 SMC Nueva linea de abajo
		--Actualizar OrdenPagoEgreso
		Update OrdenPagoEgreso
		Set ProcesaRetencion='False'
		Where IDTransaccionEgreso =@IDTransaccionEgreso
		  and IDTransaccionOrdenPago = @IDTransaccionOrdenPago
        --hasta aqui

		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
		
	End
	
	If @Operacion = 'DEL' Begin
		
		--Validar
		--Comprobante
		If Not Exists(Select * From RetencionIVA  Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encuentra el registro!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from RetencionIVA where IDTransaccion = @IDTransaccion)
		set @IDProveedor = (select IDProveedor from RetencionIVA where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from RetencionIVA where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from RetencionIVA where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select Comprobante from vRetencionIVA where IDTransaccion = @IDTransaccion)
		set @VTipoComprobante = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from RetencionIVA where IDTransaccion = @IDTransaccion)
		set @Condicion = 'RETENCION'
			--sm -24062021
		set @NroComprobante = (select NroComprobante from vRetencionIVA where IDTransaccion = @IDTransaccion)
		set @TotalIVA = (select TotalIVA from RetencionIVA where IDTransaccion = @IDTransaccion)

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,0,@IDProveedor,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ELI',@VTipoComprobante, @IDSucursal)
		
		--04-06-2021 SMC Nueva linea de abajo				
		set @IDTransaccionOrdenPago = (select vri.IDTransaccionOrdenPago from vRetencionIVA vri where vri.IDTransaccion = @IDTransaccion)
		set @IDTransaccionEgreso = (select vri.IDTransaccionEgreso from vRetencionIVA vri where vri.IDTransaccion = @IDTransaccion)
		--hasta aqui

		set  @vObservacionATI = concat('RetencionIVA:', @NroComprobante, ' - Monto:',@TotalIVA, ' *Elimina Comprobante*')
				
		--INICIO SM 24062021 - Auditoria Informática
		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,@Operacion,(Select Descripcion from Operacion where id=@IDOperacion),@vObservacionATI,@IDTransaccion,'RETENCIONIVA' )
		--FIN SM 24062021 - Auditoria Informática
	   	
		--Eliminar Retencion
		Delete From RetencionIVA Where IDTransaccion=@IDTransaccion
		
		set  @vObservacionATI = concat('RetencionIVA:', @NroComprobante, ' - Monto:',@TotalIVA, ' *OrdenPagoEgreso - IdTransaccionEgreso: ',@IdTransaccionEgreso, ' - IdTransaccionOP:',@IDTransaccionOrdenPago, ' - ProcesaRetencion:','FALSE')
				
		--INICIO SM 24062021 - Auditoria Informática
		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacionATI,@IDTransaccionEgreso,'ORDENPAGOEGRESO' )
		--FIN SM 24062021 - Auditoria Informática
						
		--04-06-2021 SMC Nueva linea de abajo
		--Actualizar OrdenPagoEgreso
		Update OrdenPagoEgreso
		Set ProcesaRetencion='False'
		Where IDTransaccionEgreso =@IDTransaccionEgreso
		  and IDTransaccionOrdenPago = @IDTransaccionOrdenPago
        --hasta aqui  

		set @Mensaje = 'Registro eliminado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
		
	End
			
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End






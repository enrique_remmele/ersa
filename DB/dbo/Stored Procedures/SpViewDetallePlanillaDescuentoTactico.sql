﻿CREATE Procedure [dbo].[SpViewDetallePlanillaDescuentoTactico]
	
	--Identificadores
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int
	
As

Begin

	Select 
	*
	From VDetallePlanillaDescuentoTactico D
	Join VPlanillaDescuentoTactico P On D.IDTransaccion=P.IDTransaccion
	Where D.IDSucursal=@IDSucursal 
	And (GETDATE() Between P.Desde And P.Hasta)
		
End




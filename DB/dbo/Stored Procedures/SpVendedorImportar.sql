﻿CREATE Procedure [dbo].[SpVendedorImportar]

	--Entrada
	@Referencia varchar(100),
	@Nombres varchar(100),
	@Resumen varchar(50),
	@IDSucursal int,
	
	@Actualizar bit = 'True'
	
As

Begin

	Declare @vID int
	
	--Si existe
	If Exists(Select * From Vendedor Where Nombres=@Nombres And IDSucursal=@IDSucursal) Begin
		Update Vendedor Set Nombres=@Nombres, Referencia=@Referencia, Resumen=@Resumen
		Where Nombres=@Nombres And IDSucursal=@IDSucursal
	End
	
	--Si existe
	If Not Exists(Select * From Vendedor Where Nombres=@Nombres And IDSucursal=@IDSucursal) Begin
		If @Actualizar = 'True' Begin
			Set @vID = (Select IsNull(Max(ID)+1,1) From Vendedor)
			Insert Into Vendedor(ID, Nombres,  Estado, Referencia, Resumen, IDSucursal)
			Values(@vID, @Nombres,  'True', @Referencia, @Resumen, @IDSucursal)
		End
	End
	
End



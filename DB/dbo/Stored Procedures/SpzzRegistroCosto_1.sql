﻿CREATE Procedure [dbo].[SpzzRegistroCosto]

	--Entrada
	@IDProducto int,
	@Cantidad decimal(10,2),
	@Costo money,
	@PrecioUnitario money,
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	--INSERTAR
	If @Operacion='INS' Begin
		
		--Obtenemos el registro
		Declare @vID numeric(18, 0)
		Declare @vNuevo bit
		
		--Verificamos si existe el producto
		If Exists(Select * From zzRegistroCosto Where IDProducto=@IDProducto) Begin
		
			PRINT 'El producto existe.'
			--Si existe verificamos si es que esta registrado
			If Exists(Select * From zzRegistroCosto Where IDProducto=@IDProducto And Cerrado = 'False') Begin
				PRINT 'El producto tiene registros no cerrado.'
				Set @vID = (Select Max(ID) From zzRegistroCosto Where IDProducto=@IDProducto And Cerrado='False')
				Set @vNuevo = 'False'
			End Else Begin
				PRINT 'El producto tiene registros anteriores pero registrado'
				Set @vID = (Select Max(ID) + 1 From zzRegistroCosto)
				Set @vNuevo = 'True'
			End
		End Else Begin
			PRINT 'El producto no existe.'
			Set @vID = (Select IsNull((Select Max(ID) + 1 From zzRegistroCosto), 1))
			Set @vNuevo = 'True'
		End
		
		print @vID
		
		--Registro anterior
		Declare @vIDAnterior numeric(18, 0)
		Declare @vAnteriorCantidad decimal(10,2)
		Declare @vAnteriorTotalCosto money
		Declare @vAnteriorCostoPromedio money
				
		--Obtenemos los valores anteriores
		Begin
			
			Set @vIDAnterior = IsNull((Select Max(ID) From zzRegistroCosto Where IDProducto=@IDProducto And Cerrado='True'), 0)
			
			If @vIDAnterior = 0 Begin
				Set @vAnteriorCantidad = 0
				Set @vAnteriorTotalCosto = 0
				Set @vAnteriorCostoPromedio = 0
			End Else Begin
				Set @vAnteriorCantidad = (Select IsNull((Select ActualCantidad From zzRegistroCosto Where ID=@vIDAnterior), 0))
				Set @vAnteriorTotalCosto = (Select IsNull((Select ActualTotalCosto From zzRegistroCosto Where ID=@vIDAnterior), 0))
				Set @vAnteriorCostoPromedio = (Select IsNull((Select ActualCostoPromedio From zzRegistroCosto Where ID=@vIDAnterior), 0))
			End
							
		End
					
		--Si no existe, agregamos	
		If @vNuevo = 'True' Begin
						
			--Insertamos el registro
			Insert Into zzRegistroCosto(ID, IDProducto, FechaInicio, UltimaActualizacion, UltimoCosto, UltimoCostoTemp, ActualCantidad, ActualTotalCosto, ActualCostoPromedio, AnteriorCantidad, AnteriorTotalCosto, AnteriorCostoPromedio, GeneralCantidad, GeneralTotalCosto, GeneralCostoPromedio, Cerrado, FechaCerrado, IDUsuarioCierre)
			Values(@vID, @IDProducto, GETDATE(), GetDate(), @PrecioUnitario, @PrecioUnitario, 0, 0, 0, @vAnteriorCantidad, @vAnteriorTotalCosto, @vAnteriorCostoPromedio, 0, 0, 0, 'False', NULL, NULL)			
			
		End
				
		--Realizamos los Calculos
		Declare @vActualCantidad decimal(10,2)
		Declare @vActualTotalCosto money
		Declare @vActualCostoPromedio money

		Declare @vGeneralCantidad decimal(10,2)
		Declare @vGeneralTotalCosto money
		Declare @vGeneralCostoPromedio money

		Set @vActualCantidad = (Select IsNull((Select ActualCantidad From zzRegistroCosto Where ID=@vID), 0))
		Set @vActualTotalCosto = (Select IsNull((Select ActualTotalCosto From zzRegistroCosto Where ID=@vID), 0))
		
		Set @vActualCantidad = @vActualCantidad + @Cantidad
		Set @vActualTotalCosto = @vActualTotalCosto + @Costo
		Set @vActualCostoPromedio = @vActualTotalCosto / @vActualCantidad
		
		Set @vGeneralCantidad = @vActualCantidad + @vAnteriorCantidad
		Set @vGeneralTotalCosto = @vActualTotalCosto + @vAnteriorTotalCosto
		Set @vGeneralCostoPromedio = @vGeneralTotalCosto / @vGeneralCantidad
						  										
		Update zzRegistroCosto Set ActualCantidad = @vActualCantidad,
								 ActualTotalCosto = @vActualTotalCosto,
								 ActualCostoPromedio = @vActualCostoPromedio,
								 GeneralCantidad = @vGeneralCantidad,
								 GeneralTotalCosto = @vGeneralTotalCosto,
								 GeneralCostoPromedio = @vGeneralCostoPromedio,
								 UltimaActualizacion=GETDATE(),
								 UltimoCosto=@PrecioUnitario
		Where ID=@vID
								
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		Return @@rowcount
		
	End
	
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	Return @@rowcount
	
End


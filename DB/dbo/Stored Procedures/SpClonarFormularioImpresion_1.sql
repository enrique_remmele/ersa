﻿CREATE Procedure [dbo].[SpClonarFormularioImpresion]

	--Entrada
	@IDFormularioOrigen int,
	@IDFormularioDestino int
	
As

Begin
	Declare @vMensaje as varchar(100)
	Declare @vProcesado as bit = 'False'

	Declare @IDDetalleFormularioImpresion as int

	Declare cCFTranferencia cursor for
	Select 
	IDDetalleFormularioIMpresion 
	From DetalleFormularioImpresion 
	Where IDFormularioImpresion = @IDFormularioOrigen
	Open cCFTranferencia
	fetch next from cCFTranferencia into @IDDetalleFormularioImpresion
		
	While @@FETCH_STATUS = 0 Begin  
		insert into DetalleFOrmularioImpresion 
		select @IDFormularioDestino,IDDetalleFormularioImpresion,NombreCampo,Campo,PosicionX,PosicionY,TipoLetra,TamañoLetra,Formato,Valor,Detalle,Numerico,Alineacion,Largo,PuedeCrecer,Lineas,MargenPrimeraLinea,Interlineado 
		FROM DetalleFormularioImpresion where IDFormularioImpresion = @IDFormularioOrigen and IDDetalleFormularioImpresion = @IDDetalleFormularioImpresion
		
		fetch next from cCFTranferencia into @IDDetalleFormularioImpresion
			
	End
		
	close cCFTranferencia
	deallocate cCFTranferencia


	Set @vMensaje = 'Registro Clonado!'
	Set @vProcesado = 'True'
	GoTo Salir

	
			
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	
End


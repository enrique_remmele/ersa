﻿CREATE Procedure [dbo].[SpConciliarAsiento]
	--Entrada
	@IDTransaccion numeric,
	@Conciliado bit,
	@Operacion varchar(20),
	
	--Transaccion	
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
			
As

Begin

	--BLOQUES
	Declare @vFechaConciliado Date
	Declare @vIDUsuarioConciliado int
	Declare @vFechaAsiento Date = (Select Fecha from asiento where idtransaccion = @IDTransaccion)
	Declare @vIDOperacion int = (Select ID from Transaccion where ID = @IDTransaccion)	
	
	--ACTUALIZAR
	if @Operacion='UPD' begin

			if isnull((select conciliado from asiento where IDTransaccion = @IDTransaccion),'False') = 'True' and @Conciliado = 'True'  begin
				set @Mensaje = 'El asiento ya se encuentra conciliado!'
				set @Procesado = 'False'
				return @@rowcount
			end
		
			--Actualizamos
			Update Asiento  Set Conciliado=@Conciliado 
								,IDUsuarioConciliado=@IDUsuario
								,FechaConciliado = GetDate()
			Where IDTransaccion =@IDTransaccion 	
		
			set @Mensaje = 'Registro procesado!'
			set @Procesado = 'True'
			return @@rowcount
	   
	End
	
	if @Operacion = 'DESCONCILIAR' begin
		Set @vFechaConciliado = (Select FechaConciliado from Asiento where IDTransaccion = @IDTransaccion)
		Set @vIDUsuarioConciliado = (Select IDUsuarioConciliado from Asiento where IDTransaccion = @IDTransaccion)
		
		print 'Verifica permisos de usuario'
		if (Cast(@vFechaConciliado as date) <> Cast(GetDate() as date)) or (@vIDUsuarioConciliado <> @IDUsuario) begin
			--Verifica permisos de usuario
			if exists(select * from UsuarioDesconciliarAsiento where IDUsuario = @IDUsuario) begin
				
				If dbo.FValidarFechaOperacion(@IDSucursal, @vFechaAsiento, @IDUsuario, @vIDOperacion) = 'False' Begin
					Set @Mensaje  = 'Fecha fuera de rango permitido!!'
					Set @Procesado = 'False'
					Return @@rowcount
				End
			end
			else begin
				if @vIDUsuarioConciliado <> @IDUsuario begin
					set @Mensaje = 'No tiene permisos para realizar esta operacion, No es el usuario conciliador'
					set @Procesado = 'False'
					return @@rowcount
				end
				else begin
					set @Mensaje = 'No tiene permisos para realizar esta operacion.'
					set @Procesado = 'False'
					return @@rowcount
				end
			end
		end
		
		Insert into aConciliacionAsiento(IDTransaccion,Conciliado,FechaConciliado,IDUsuarioConciliado,FechaDesconciliado,IDUsuarioDesconciliado)
								Select IDTransaccion,Conciliado,FechaConciliado,IDUsuarioConciliado,GetDate(),@IDUsuario from Asiento where IDTransaccion = @IDTransaccion
			
		--Actualizamos
			Update Asiento  Set Conciliado=null 
								,IDUsuarioConciliado=null
								,FechaConciliado = null
			Where IDTransaccion =@IDTransaccion 	
		
			set @Mensaje = 'Registro procesado!'
			set @Procesado = 'True'
			return @@rowcount
	end
		
		
	
	set @Mensaje = 'No se realizo ninguna operacion! '
	set @Procesado = 'False'
	return @@rowcount

End


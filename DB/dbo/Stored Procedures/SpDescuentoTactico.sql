﻿CREATE Procedure [dbo].[SpDescuentoTactico]

	--Entrada
	@IDProducto int,
	@IDDeposito tinyint,
	@Importe money,
	@Cantidad decimal(10,2),
		
	@Operacion varchar(20),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
As

Begin

	--Variables
	Declare @vIDSucursal tinyint
	Declare @vIDActividad int
	Declare @vIDTransaccionPlanilla numeric(18,0)
	Declare @vCantidad decimal(18,0)
	Declare @vTotal money
	
	--Hayar la sucursal
	Set @vIDSucursal = (Select IDSucursal From Deposito Where ID=@IDDeposito)
	
	--Insertar
	if @Operacion= 'INS' Begin
	
		Set @vIDActividad = (Select A.ID From PlanillaDescuentoTactico P Join DetallePlanillaDescuentoTactico D On P.IDTransaccion=D.IDTransaccion Join Actividad A On D.IDActividad=A.ID Join DetalleActividad DA On A.ID=DA.IDActividad Where DA.IDProducto=@IDProducto And P.IDSucursal=@vIDSucursal)
				
		--Verificar Si Existen Actividades para este producto en esta sucursal
		If @vIDActividad Is Null Begin
			--Salimos
			Set @Mensaje = 'No hay actividad para este producto'
			Set @Procesado = 'True'
			return @@rowcount
		End
		
		--Actualizamos el Detalle
		Set @vCantidad = (Select IsNull((Select SUM(Cantidad) From DetalleActividad Where IDActividad=@vIDActividad And IDProducto=@IDProducto), 0))
		Set @vTotal = (Select IsNull((Select SUM(Total) From DetalleActividad Where IDActividad=@vIDActividad And IDProducto=@IDProducto), 0))
		
		Set @vCantidad = @vCantidad + @Cantidad
		Set @vTotal = @vTotal + @Importe
		
		Update DetalleActividad Set Cantidad=@vCantidad,
									Total=@vTotal
		Where IDActividad = @vIDActividad And IDProducto=@IDProducto
		
		--Actualizamos la Actividad
		
		--Actualizamos
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		
	End
	

	Set @Mensaje = 'No se proceso'
	Set @Procesado = 'False'
	return @@rowcount	
		
End


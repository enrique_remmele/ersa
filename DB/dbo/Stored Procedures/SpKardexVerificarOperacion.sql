﻿CREATE Procedure [dbo].[SpKardexVerificarOperacion]

@IDTransaccion int 

As

Begin

	SET NOCOUNT ON

	Declare @vIDKardex numeric(18,0)
	Declare @vOrden tinyint
	Declare @vAño smallint
	Declare @vMes tinyint
	Declare @vFecha datetime
	Declare @vIDTransaccion numeric(18,0)
	Declare @vIDProducto int
	Declare @vID tinyint
	Declare @vCantidadEntrada decimal(10,2)
	Declare @vCantidadSalida decimal(10,2)
	Declare @vCantidadSaldo decimal(10,2)
	Declare @vCostoEntrada money
	Declare @vCostoSalida money
	Declare @vCostoEntradaAnterior money
	Declare @vCostoSalidaAnterior money
	Declare @vCostoPromedio money
	Declare @vTotalSaldo money
	Declare @vIDSucursal int
	Declare @vComprobante varchar(50)
	Declare @vTipo varchar(15)
	Declare @vIDKardexAnterior int
	Declare @vCantidadSaldoAnterior decimal(10,2)
	Declare @vCostoPromedioAnterior money
	Declare @vTotalSaldoAnterior money
	Declare @vCostoPromedioOperacion money
	Declare @vFechaInicio Date
	Declare @vIDProductoActual int = 0
	Declare @vIndice int = 0
	


	Declare @vCantidadDetalleOperacion int = 0
	Declare @vCantidadDetalleKardex int = 0

	Begin
		
		if Exists (select * from DocumentoAnulado Where IDTransaccion = @IDTransaccion) begin
			print 'eliminados'
			Delete from kardex where idtransaccion = @IDTransaccion
			goto salir
		end

		Set @vCantidadDetalleOperacion = (select count(*) from VCargaKardex where IDTransaccion = @IDTransaccion)
		Set @vCantidadDetalleKardex = (select count(*) from Kardex where IDTransaccion = @IDTransaccion)
		if @vCantidadDetalleOperacion = @vCantidadDetalleKardex begin
			print 'Son iguales'
			Goto Salir
		end

		if (select sum(isnull(CantidadEntrada,0))-sum(isnull(CantidadSalida,0)) from VCargaKardex where IDTransaccion = @IDTransaccion) = isnull((select sum(CantidadEntrada)-sum(CantidadSalida) from VKardex where IDTransaccion = @IDTransaccion),0) begin
			print 'Las cantidades son iguales'
			Goto Salir
		end

	end
	
	Begin

	print 'IDTransaccion'
	print @IDTransaccion
					
		Declare db_cursor cursor for
		Select	
			CK.FechaEntrada, 
			CK.IDProducto, 
			CK.ID, 
			CK.Orden, 
			CK.IDSucursal, 
			CK.Comprobante, 
			CK.CantidadEntrada, 
			CK.CantidadSalida, 
			CK.CostoEntrada, 
			CK.CostoSalida, 
			CK.Tipo 
		From VCargaKardex CK
		Join Producto P On CK.IDProducto=P.ID 
		Where CK.IDTransaccion = @IDTransaccion
		--and CK.IDProducto not in (select IDProducto from Kardex where IDTransaccion = @IDTransaccion)
		and (select sum(isnull(CantidadEntrada,0))-sum(isnull(CantidadSalida,0)) from VCargaKardex where IDProducto = P.ID and IDTransaccion = @IDTransaccion) <> isnull((select sum(CantidadEntrada)-sum(CantidadSalida) from VKardex where IDProducto = P.ID and IDTransaccion = @IDTransaccion),0)
		And P.ControlarExistencia='True'		
		Order by CK.ID
		Open db_cursor   

		Fetch Next From db_cursor Into @vFecha, @vIDProducto, @vID, @vOrden, @vIDSucursal, @vComprobante, @vCantidadEntrada, @vCantidadSalida, @vCostoEntrada, @vCostoSalida, @vTipo
		While @@FETCH_STATUS = 0 Begin 
			print 'ok'
			Set @vCantidadEntrada = (select sum(CantidadEntrada) from VCargaKardex where IDProducto = @vIDProducto and IDTransaccion = @IDTransaccion)
			Set @vCantidadSalida = (select sum(CantidadSalida) from VCargaKardex where IDProducto = @vIDProducto and IDTransaccion = @IDTransaccion)
			If Isnull(@vCantidadEntrada,0) = 0 and Isnull(@vCantidadSalida,0) <> 0 begin	
			print @vIDProducto
				Delete from Kardex where IDProducto = @vIDProducto and IDTransaccion = @IDTransaccion
				print 'entro'
				EXEC SpKardex
					@Fecha= @vFecha,
					@IDTransaccion = @IDTransaccion,
					@IDProducto = @vIDProducto,
					@ID = @vID,
					@Orden = @vOrden, 
					@CantidadEntrada = 0,
					@CantidadSalida = @vCantidadSalida,
					@CostoEntrada = 0,
					@CostoSalida = @vCostoSalida,
					@IDSucursal = @vIDSucursal,
					@Comprobante = @vComprobante	
			End

			If Isnull(@vCantidadSalida,0) = 0 and Isnull(@vCantidadEntrada,0) <> 0 begin	
			print @vIDProducto
					Delete from Kardex where IDProducto = @vIDProducto and IDTransaccion = @IDTransaccion
					print 'entro'
					EXEC SpKardex
					@Fecha= @vFecha,
					@IDTransaccion = @IDTransaccion,
					@IDProducto = @vIDProducto,
					@ID = @vID,
					@Orden = @vOrden,
					@CantidadEntrada = @vCantidadEntrada,
					@CantidadSalida = 0,
					@CostoEntrada = @vCostoEntrada,
					@CostoSalida = 0,
					@IDSucursal = @vIDSucursal,
					@Comprobante = @vComprobante	

					if @vTipo in ('COMPRA','PRODUCCION') begin
						EXEC SpActualizarCostoProducto @ID = @vIDProducto, @Costo = @vCostoEntrada, @IDTransaccion = @IDTransaccion, @Operacion = 'PRODUCCION'
					end
			End
			
			Fetch Next From db_cursor Into @vFecha, @vIDProducto, @vID, @vOrden, @vIDSucursal, @vComprobante, @vCantidadEntrada, @vCantidadSalida, @vCostoEntrada, @vCostoSalida, @vTipo

		End

		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor

	End

	Salir:

End

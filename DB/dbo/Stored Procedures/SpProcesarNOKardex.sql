﻿

CREATE Procedure [dbo].[SpProcesarNOKardex]
	
	----Entrada
	@IDProducto int,
	@FechaInicio date = null
As

Begin

	Set NOCOUNT ON

	Declare @vIDKardex numeric(18,0)
	Declare @vOrden tinyint
	Declare @vAño smallint
	Declare @vMes tinyint
	Declare @vFecha datetime
	Declare @vIDTransaccion numeric(18,0)
	Declare @vIDProducto int
	Declare @vID tinyint
	Declare @vCantidadEntrada decimal(10,2)
	Declare @vCantidadSalida decimal(10,2)
	Declare @vCantidadSaldo decimal(10,2)
	Declare @vCostoEntrada money
	Declare @vCostoSalida money
	Declare @vCostoPromedio money
	Declare @vTotalEntrada money
	Declare @vTotalSalida money
	Declare @vTotalSaldo money
	Declare @vIDSucursal int
	Declare @vComprobante varchar(50)
	Declare @vIDKardexAnterior int
	Declare @vCantidadSaldoAnterior decimal(10,2)
	Declare @vCostoPromedioAnterior money
	Declare @vTotalSaldoAnterior money
	Declare @vCostoPromedioOperacion money
	Declare @vIDProductoAnterior int = 0
	Declare @vIndice integer = 1 
	Declare @vFactorImpuesto decimal(18,3)
	Declare @vFactorDiscriminado decimal(18,3)
	Declare @vIDTipoComprobante int

	Declare @vFechaInicio date
	Declare @vOperacion varchar(30)

	--Set @vAño = @Año
	--Set @vMes = @Mes

	Begin
		print concat('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$', @IDProducto, '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
		if @FechaInicio is null begin 
		Set @vFechaInicio = (Select Max(Fecha) From CierreAperturaStock Where Anulado='False')
		end 
		else begin
			Set @vFechaInicio = @FechaInicio
		end

		If @vFechaInicio Is Null Begin
			Set @vFechaInicio = '20190101'
		End

		Declare db_cursorProcesarKardex cursor for
		Select	IDTransaccion, Fecha, IDProducto, ID, CantidadEntrada, CantidadSalida
		from VDetalleMovimiento
		Where IDProducto = @IDProducto and Fecha>=@vFechaInicio
		And Operacion = 'TRANSFERENCIA'
		Open db_cursorProcesarKardex   
		
		Fetch Next From db_cursorProcesarKardex Into @vIDTransaccion, @vFecha, @vIDProducto, @vID, @vCantidadEntrada, @vCantidadSalida
		While @@FETCH_STATUS = 0 Begin 
				
			If @vCantidadSalida<0 Begin
				Set @vCantidadSalida = @vCantidadSalida*-1
			End

			If @vCantidadEntrada<0 Begin
				Set @vCantidadEntrada = @vCantidadEntrada*-1
			End


			Set @vCostoPromedio = dbo.FCostoProductoFechaKardex(@vIDProducto,@vFecha) 

			Update DetalleMovimiento
			Set PrecioUnitario = @vCostoPromedio,
				Total = IsNull((@vCantidadEntrada) * @vCostoPromedio,0)
			Where IDTransaccion = @vIDTransaccion  And IDProducto=@vIDProducto And ID=@VID

			Update Movimiento
			Set Total = (Select sum(Total) from detalleMovimiento where IDTransaccion = @vIDTransaccion)
			Where IDTransaccion = @vIDTransaccion
				
			----Generar Asiento
			if @vOperacion = 'MOVIMIENTO MATERIA PRIMA' begin
				Exec SpAsientoMovimientoMateriaPrimaBalanceado @IDTransaccion = @vIDTransaccion
			end else begin
				Exec SpAsientoMovimiento @IDTransaccion=@vIDTransaccion
			end

Seguir:
			
			Fetch Next From db_cursorProcesarKardex Into @vIDTransaccion, @vFecha, @vIDProducto, @vID, @vCantidadEntrada, @vCantidadSalida
		
		End
		
		--Cierra el cursorv xj 
		Close db_cursorProcesarKardex   
		Deallocate db_cursorProcesarKardex
		
	End

	

End

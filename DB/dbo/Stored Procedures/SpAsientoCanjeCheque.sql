﻿CREATE Procedure [dbo].[SpAsientoCanjeCheque]

	@IDTransaccion numeric(18,0)

As

Begin

	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint = 0

	--
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vFecha date
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money


	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vCodigo varchar(50)
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money
	Declare @vImporte money
	declare @vCliente varchar(200)

	--Obtener valores
	Begin


		Select	@vIDSucursal=IDSucursal,
				@vFecha=Fecha,
				@vIDTipoComprobante=IDTipoComprobante,
				@vComprobante=Comprobante,
				@vIDMoneda=1,
				@vCotizacion=1,
				@vImporte = Total
		From VPagoChequeCliente Where IDTransaccion=@IDTransaccion

	End

	--Verificar que el asiento se pueda modificar
	Begin

		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	

		--Si esta procesado
		If (Select Procesado From CobranzaCredito Where IDTransaccion=@IDTransaccion) = 'False' Begin
			print 'La cobranza no es valida'
			GoTo salir
		End 	

		--Si esta anulado
		If (Select Anulado From CobranzaCredito Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'La cobranza esta anulada'
			GoTo salir
		End 	

		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	

		print 'Los datos son correctos!'

	End

	--Eliminar primero el asiento
	Begin

		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion

		print 'Registros de Asiento y Detalle eliminados!'

	End

	--Cargamos la Cabecera
	Begin

		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)

		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = 'Canje de Cheque del ' + CONVERT(varchar(50), @vFecha)

		print 'Tipo de Comprobante:' + convert(varchar(50), @vTipoComprobante)
		print 'Comprobante:' + convert(varchar(50), @vComprobante)


		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')

		print 'Cabecera cargada!'

	End

	--Ventas
	Begin

		--Variables

		Declare cCFVenta cursor for
		Select Codigo, Debe, Haber From VCFCanjeCheque
		Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda 
		Open cCFVenta   
		fetch next from cCFVenta into @vCodigo, @vDebe, @vHaber

		While @@FETCH_STATUS = 0 Begin  

			Set @vImporteDebe = 0
			Set @vImporteHaber = 0

			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
				Set @vImporteHaber = 0
			End

			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
				Set @vImporteDebe = 0
			End				
			
			--Obtener la cuenta
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
			End

			fetch next from cCFVenta into @vCodigo, @vDebe, @vHaber

		End

		close cCFVenta 
		deallocate cCFVenta

	End

	--Actualizamos la cabecera, el total
	Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)

	Set @vImporteHaber = ROUND(@vImporteHaber, @vRedondeo)
	Set @vImporteDebe = ROUND(@vImporteDebe, @vRedondeo)
	
	Update Asiento Set Total = @vImporteHaber,
						Credito = @vImporteHaber,
						Debito = @vImporteDebe,
						Saldo = @vImporteHaber - @vImporteDebe
	Where IDTransaccion=@IDTransaccion

	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:

End


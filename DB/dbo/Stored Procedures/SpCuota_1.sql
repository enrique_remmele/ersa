﻿CREATE Procedure [dbo].[SpCuota]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@ID int,
	@Importe money,
	@Saldo money,
	@Vencimiento date,
	@Cancelado bit,

	--Operacion
	@Operacion varchar(50),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin

	--Variable
	
	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Verificar no existe en transaccion
		If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro ningun transaccion para el registro actual! No se puede cargar las cuotas.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Verificar si ya existe la transaccion en cuotas
		If Exists(Select ID From Cuota Where IDTransaccion=@IDTransaccion And ID=@ID) Begin
			set @Mensaje = 'El sistema encontro registros anteriores para este documento! No se puede generar cuotas.'
			set @Procesado = 'False'
			return @@rowcount
		End
	
		--Insertar en cuotas
		Insert into Cuota(IDTransaccion, ID, Vencimiento, Importe, Saldo, Cancelado)
		Values(@IDTransaccion, @ID, @Vencimiento, @Importe, @Importe, 'False')

		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'DEL' Begin
	
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	If @Operacion = 'UPD' Begin
	
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
			
	End
	
	set @Mensaje = 'No se proceso ninguna transacción!'
	set @Procesado = 'False'
	return @@rowcount
		
End







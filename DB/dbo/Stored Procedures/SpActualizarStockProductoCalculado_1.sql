﻿CREATE Procedure [dbo].[SpActualizarStockProductoCalculado]
	
	--Entrada
	@IDProducto int,
	@IDDeposito int,
	@IDTransaccion int,
		
	--Transaccion
	@IDUsuarioOperacion smallint,
	@IDSucursalOperacion tinyint,
	@IDDepositoOperacion tinyint,
	@IDTerminalOperacion tinyint,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	

As

Begin
	Declare @vExistencia decimal(18,2) = 0
	
	
	Begin

		Set @vExistencia = isnull((Select Calculado From VProductosConDiferencias Where (Calculado-Existencia)<>0 and IDProducto = @IDProducto and IDDeposito = @IDDeposito),0)
		
		--Actualizar Efectivo 
		if @vExistencia <> 0 begin
			
			insert into aExistenciaDepositoCalculado
			select IDDeposito, IDProducto, Existencia, @vExistencia, @IDUsuarioOperacion, @IDSucursalOperacion, @IDDepositoOperacion, @IDTerminalOperacion, @IDTransaccion from ExistenciaDeposito Where IDProducto=@IDProducto And IDDeposito=@IDDeposito
		
			Update ExistenciaDeposito 
             Set Existencia=@vExistencia
             Where IDProducto=@IDProducto
            And IDDeposito=@IDDeposito

		Set @Mensaje = 'Registro procesado!'
		Set @Procesado = 'True'	
		Return @@rowcount
	
	End
	
End

End
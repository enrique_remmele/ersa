﻿CREATE Procedure [dbo].[SpTipoDeposito]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Estado bit,
	@Transferir bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From TipoDeposito Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDTipoDeposito tinyint
		set @vIDTipoDeposito = (Select IsNull((Max(ID)+1), 1) From TipoDeposito)

		--Insertamos
		Insert Into TipoDeposito(ID, Descripcion,Transferir, Estado)
		Values(@vIDTipoDeposito, @Descripcion,@Transferir, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TIPO DE DEPOSITO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From TipoDeposito Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From TipoDeposito Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update TipoDeposito 
		Set Descripcion=@Descripcion,
			Transferir = @Transferir,		
			Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TIPO DE DEPOSITO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From TipoDeposito Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con deposito
		if exists(Select * From Deposito Where IDTipoDeposito=@ID) begin
			set @Mensaje = 'El registro tiene depositos asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From TipoDeposito 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TIPO DE DEPOSITO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


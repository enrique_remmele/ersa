﻿
CREATE Procedure [dbo].[SpAsientoDepositoBancario]

	@IDTransaccion numeric(18,0)
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint =0

	--Banco
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vObservacion varchar(100)
	Declare @vFecha date
	Declare @vIDCuentaBancaria int
	Declare @vCuentaBancaria varchar(50)
	Declare @vTipoCuenta bit
	Declare @vIDMoneda int
	Declare @vCotizacion money

	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	
	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vIDTipoProducto int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money=0
	Declare @vImporteHaber money=0

	Declare @vCliente as varchar(32)
	Declare @vIDCliente as int
	
	--Obtener valores
	Begin
		
		Select	@vIDSucursal=IDSucursal,
				@vFecha=Fecha,
				@vIDTipoComprobante =IDTipoComprobante,
				@vComprobante = NroComprobante,
				@vObservacion = Observacion,
				@vTipoComprobante = TipoComprobante,
				@vIDCuentaBancaria = IDCuentaBancaria,
				@vCuentaBancaria = Cuenta,
				@vTipoCuenta =TipoCuenta,
				@vIDMoneda = IDMoneda,
				@vCotizacion = Cotizacion
		From vDepositoBancario
		Where IDTransaccion=@IDTransaccion
		

	End
					
	--Verificar que el asiento se pueda modificar
	Begin
	
		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
			
		--Si esta anulado
		If (Select Anulado From vMacheoFacturaTicket Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'La venta esta anulada'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		
	End
				
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = @vObservacion
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	
	--Banco
	Begin
		
		Declare cCFProvision cursor for
		Select Codigo, Debe, Haber From VCFDepositoBancario 
		Where IDSucursal = @vIDSucursal 
		and IDMoneda = @vIDMoneda 
		and TipoCuenta = @vTipoCuenta
		and Debe = 1
		
		Open cCFProvision   
		fetch next from cCFProvision into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
				Set @vImporteDebe = 0
				Set @vImporteHaber = 0
			
				--Obtener la cuenta
				Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
				Set @vCodigo = (select CodigoCuentaContable from CuentaBancaria where id = @vIDCuentaBancaria)
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			
				Set @vImporte = (select Total from depositobancario where idtransaccion = @IDTransaccion) * @vCotizacion
			
				
				If @vImporte > 0  Begin				
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, Round(@vImporte,@vRedondeo), 0, '')
				End
			
			
			fetch next from cCFProvision into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFProvision 
		deallocate cCFProvision
	End
	--pagos
	begin
		Declare @TipoPago as varchar(50)
		Declare @ImportePago as money
		Declare @CotizacionFormaPago as money
		Declare cCFFormaPago cursor for
		Select Tipo, Importe, Cotizacion From VDetalleDepositoBancario
		Where IDTransaccionDepositoBancario = @IDTransaccion
		Open cCFFormaPago   
		fetch next from cCFFormaPago into @TipoPago, @ImportePago, @CotizacionFormaPago
		
		While @@FETCH_STATUS = 0 Begin  

				If @TipoPago = 'EFECTIVO' begin
					print concat('Efectivo ', @vIDSucursal, ' ', @vIDMoneda, ' ', @vTipoCuenta)
					Set @vCodigo = (select cuentacontable from VCFDepositoBancario where Efectivo = 1 and IDSucursal= @vIDSucursal and IDMoneda=@vIDMoneda and TipoCuenta = @vTipoCuenta and Haber = 1)
                End

                If @TipoPago = 'CHEQUE AL DIA' Begin
					Set @vCodigo = (select cuentacontable from VCFDepositoBancario where ChequeALDia = 1 and IDSucursal= @vIDSucursal and IDMoneda=@vIDMoneda and TipoCuenta = @vTipoCuenta and Haber = 1)
                End 

                If @TipoPago = 'CHEQUE DIFERIDO' Begin
					Set @vCodigo = (select cuentacontable from VCFDepositoBancario where ChequeDiferido = 1 and IDSucursal= @vIDSucursal and IDMoneda=@vIDMoneda and TipoCuenta = @vTipoCuenta and Haber = 1)
                End 

                If @TipoPago = 'CHEQUE RECHAZADO' Begin
					Set @vCodigo = (select cuentacontable from VCFDepositoBancario where ChequeRechazado = 1 and IDSucursal= @vIDSucursal and IDMoneda=@vIDMoneda and TipoCuenta = @vTipoCuenta and Haber = 1)
                End 

                If @TipoPago = 'DIFERENCIACAMBIO' Begin
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
					Set @vCodigo = (select cuentacontable from VCFDepositoBancario where DiferenciaCambio = 1 and IDSucursal= @vIDSucursal and IDMoneda=@vIDMoneda )
					Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
					print @vCodigo
						print @vIDCuentaContable
					if @ImportePago > 0 begin
						Set @vImporteDebe = @ImportePago --* @CotizacionFormaPago
					end

					if @ImportePago < 0 begin
						Set @vImporteHaber = @ImportePago * -1
						print @vImporteHaber
					end

					If (@vImporteDebe > 0)  or  (@vImporteHaber > 0) Begin
						
						If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
							print 'insert diferencia cambio'
						print @vCodigo
						print @vIDCuentaContable	
							Update DetalleAsiento Set Debito=Debito+Round(@vImporteDebe,@vRedondeo),
														Credito=Credito+Round(@vImporteHaber,@vRedondeo)
							Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
						End Else Begin				
						
							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo),  @vImporteDebe, 0, '')
						End
					end
					
					goto seguir
                End 
				If @TipoPago = 'DOCUMENTO' Begin
					Set @vCodigo = (select cuentacontable from VCFDepositoBancario where Documento = 1 and IDSucursal= @vIDSucursal and IDMoneda=@vIDMoneda and TipoCuenta = @vTipoCuenta and Haber = 1)
                End 
				
				Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				Set @vImporte = @ImportePago * @CotizacionFormaPago
				print @TipoPago

				If @vImporte > 0 Begin
					If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
						Update DetalleAsiento Set Credito=Credito+Round(@vImporte,@vRedondeo),
													Importe=Importe+Round(@vImporte,@vRedondeo)
						Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
					End Else Begin				
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporte,@vRedondeo),  0, 0, '')
					End
				end
				seguir:
				
			fetch next from cCFFormaPago into @TipoPago, @ImportePago, @CotizacionFormaPago
			
		End
		
		close cCFFormaPago 
		deallocate cCFFormaPago
	End
		
	--Actualizamos la cabecera, el total
	Begin
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
		Set @vImporteHaber = ROUND(@vImporteHaber, @vRedondeo)
		Set @vImporteDebe = ROUND(@vImporteDebe, @vRedondeo)
	
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vImporteHaber - @vImporteDebe
		Where IDTransaccion=@IDTransaccion
	End

	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
	
End



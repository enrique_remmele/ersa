﻿
CREATE Procedure [dbo].[SpImportListaPrecio]

	@Producto varchar(50),
	@ListaPrecio varchar(50),
	@Precio decimal(18,2)
	
As

Begin
	
	Declare @vIDProducto int = 0
	Declare @vIDListaPrecio int = 0
	Declare @Mensaje varchar(200)
	Declare @Procesado bit
	Declare @vOperacion varchar(10) = 'UPD'

	Set @vIDProducto = (select top(1) isnull(id,0) from Producto where referencia = @Producto)
	Set @vIDListaPrecio = (Select top(1) isnull(id,0) from ListaPrecio where Descripcion = @ListaPrecio)

	if @vIDProducto < 1 begin
		print concat('producto no encontrado', @Producto)
		goto Salir
	end

	if @vIDListaPrecio < 1 begin
		print concat('producto no encontrado', @ListaPrecio)
		goto Salir
	end

	if not exists(select * from ProductoListaPrecio where IDProducto = @vIDProducto and IDListaPrecio = @vIDListaPrecio) begin
		Set @vOperacion = 'INS'
	end


	Exec SpProductoListaPrecio
		@IDProducto=@vIDProducto
		,@IDListaPrecio=@vIDListaPrecio
		,@IDMoneda='1'
		,@Precio=@Precio
		,@TPRPorcentaje='0'
		,@Operacion=@vOperacion
		,@IDUsuario='1'
		,@IDSucursal='1'
		,@IDDeposito='1'
		,@IDTerminal='1'
		,@Mensaje=@Mensaje output
		,@Procesado=@Procesado output

		print @Mensaje
		print concat('actualizacion completa', @Producto, ' ', @ListaPrecio )
	
	
Salir:	

End


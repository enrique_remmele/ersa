﻿CREATE Procedure [dbo].[SpViewPedidoAnticipado]
	--Entrada
	@FechaDesde date,
	@FechaHasta date,
	@IDSucursal tinyint
As

Begin
  
	--variables
	Declare @vIDTransaccion decimal(18,0)
	Declare @vIDSucursal tinyint
	Declare @vSuc varchar(6)
	Declare @vNumero integer
	Declare @vFechaPedido date
	Declare @vFechaCarga date
	Declare @vIDProducto integer
	Declare @vRefPRoducto varchar(16)
	Declare @vPRoducto varchar(128)
	Declare @vCantidad money
	Declare @vPeso money
	Declare @vCantidadPedidos integer
	Declare @vDias integer
	Declare @vIDTipoProducto integer
	Begin
		
	--Crear la tabla temporal
    Create table #TablaPedido(	IDSucursal tinyint,
								Suc varchar(6),
								FechaPedido date,
								FechaCarga date,
								Cantidad money,
								Peso money,
								CantidadPedidos integer,
								Dias integer,
								Observacion varchar(32),
								IDTipoProducto integer
								)
 IF @IDSucursal > 0 Begin										
		Declare db_cursor1 cursor for
		Select IDSucursal,
		Suc,
		FechaFacturar,
		'Fecha'=Cast(Fecha as date),
		Sum(Cantidad) as Cantidad,
		Sum(Cantidad*Peso) as Peso,
		count(*), IDTipoProducto
		from vDetallePedido 
		where FechaFacturar between @FechaDesde and @FechaHasta 
		and IDSucursal = @IDSucursal
		and Anulado = 'False'
		Group by IDSucursal, Suc,cast(Fecha as date), FechaFacturar, IDTipoProducto
	Open db_cursor1   
	Fetch Next From db_cursor1 Into	@vIDSucursal,@vSuc,@vFechaPedido, @vFechaCarga,@vCantidad,@vPeso,@vCantidadPedidos, @vIDTipoProducto
	While @@FETCH_STATUS = 0 Begin 
	        set @vDias = datediff(day,@vFechaPedido,@vFechaCarga)
			set @vCantidadPedidos = (Select count(distinct P.IDTransaccion)from VPedido P Join DetallePedido DP on DP.IDTransaccion = P.IDTransaccion where P.IDSucursal = @vIDSucursal and IDTipoProducto = @vIDTipoProducto and datediff(day,cast(Fecha as date),cast(FechaFacturar as date)) = (@vDias * -1) and Anulado = 'False' and cast(FechaFacturar as date) between @FechaDesde and @FechaHasta )
			Insert Into #TablaPedido
			Values(@vIDSucursal,@vSuc,@vFechaPedido,@vFechaCarga,@vCantidad,@vPeso,@vCantidadPedidos,@vDias,'', @vIDTipoProducto)
		
Siguiente:
		
		Fetch Next From db_cursor1 Into	@vIDSucursal,@vSuc,@vFechaPedido, @vFechaCarga,@vCantidad,@vPeso,@vCantidadPedidos,@vIDTipoProducto
		
	End

	--Cierra el cursor
	Close db_cursor1   
	Deallocate db_cursor1

	End
	
	--Todas las Sucursales
	IF @IDSucursal = 0 Begin
	Declare db_cursor cursor for
		Select IDSucursal,
		Suc,
		FechaFacturar,
		'Fecha'=Cast(Fecha as date),
		Sum(Cantidad) as Cantidad,
		Sum(Cantidad*Peso) as Peso,
		count(*),
		IDTipoProducto
		from vDetallePedido 
		where FechaFacturar between @FechaDesde and @FechaHasta 
		and Anulado = 'False'
		Group by IDSucursal, Suc,cast(Fecha as date), FechaFacturar, IDTipoProducto
	Open db_cursor   
	Fetch Next From db_cursor Into	@vIDSucursal,@vSuc,@vFechaPedido, @vFechaCarga,@vCantidad,@vPeso,@vCantidadPedidos, @vIDTipoProducto
	While @@FETCH_STATUS = 0 Begin 
	        set @vDias = datediff(day,@vFechaPedido,@vFechaCarga)
			set @vCantidadPedidos = (Select count(distinct P.IDTransaccion)from VPedido P Join DetallePedido DP on DP.IDTransaccion = P.IDTransaccion where P.IDSucursal = @vIDSucursal and IDTipoProducto = @vIDTipoProducto and datediff(day,cast(Fecha as date),cast(FechaFacturar as date)) = (@vDias * -1) and Anulado = 'False' and cast(FechaFacturar as date) between @FechaDesde and @FechaHasta )
			Insert Into #TablaPedido
			Values(@vIDSucursal,@vSuc,@vFechaPedido,@vFechaCarga,@vCantidad,@vPeso,@vCantidadPedidos,@vDias,'', @vIDTipoProducto)
		
Siguiente1:
		
		Fetch Next From db_cursor Into	@vIDSucursal,@vSuc,@vFechaPedido, @vFechaCarga,@vCantidad,@vPeso,@vCantidadPedidos, @vIDTipoProducto
		
	End

	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor
	End
	update #TablaPedido
	set Observacion = (Case when Dias = 0  then '0 DIAS DE ANTICIPACION'
					        when Dias = -1 then '1 DIA DE ANTICIPACION'
							when Dias <= -2 then '2 Y MAS DIAS DE ANTICIPACION' end)


    Select IDSucursal,Suc,cast(FechaPedido as date) as FechaPedido,
	Sum(Cantidad) as Cantidad,Sum(Peso) as Peso,
	--(select sum(CantidadPedidos) from #TablaPedido where IDSucursal = IDSucursal and IDTipoProducto = IDTipoProducto and Observacion = Observacion and Suc = Suc and FechaPedido = FechaPedido group by IDSucursal,IDTipoProducto )CantidadPedidos
	CantidadPedidos,
	Observacion, IDTipoProducto
	From #TablaPedido 
	Group By IDSucursal, Suc, cast(FechaPedido as date),Observacion, IDTipoProducto,CantidadPedidos


			
	End
End
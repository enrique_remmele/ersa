﻿CREATE Procedure [dbo].[SpEfectivo]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalDocumento tinyint = NULL,
	@Numero int = NULL,
	@Fecha date = NULL,
	@IDTipoComprobante smallint = NULL,
	@Comprobante varchar(50) = NULL,
	
	@IDMotivo tinyint = NULL,
	@Observacion varchar(200) = NULL,
	
	@IDMoneda tinyint = NULL,
	@Cotizacion money = NULL,
	@ImporteMoneda money = NULL,
	@Total money = NULL,
	
	@Habilitado bit = 'False',

	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
	
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from Efectivo where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--Variable
	
	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		Declare @vNumero int
		Declare @vID int
		Set @vNumero = IsNull((Select MAX(Numero)+1 From Efectivo Where IDSucursal=@IDSucursalDocumento),1)
		Set @vID = 0
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--Insertar el Registro de Gasto		
		Insert Into Efectivo(IDTransaccion, ID, IDSucursal, Numero, IDTipoComprobante, Comprobante, Fecha, IDMoneda, ImporteMoneda, Cotizacion, IDMotivo, Depositado, Cancelado, Total, Saldo, ImporteHabilitado, Habilitado, Observacion)
		Values(@IDTransaccionSalida, @vID, @IDSucursalDocumento, @vNumero, @IDTipoComprobante, @Comprobante, @Fecha, @IDMoneda, @ImporteMoneda, @Cotizacion, @IDMotivo, 0, 'False', @Total, @Total, 0, 'False', @Observacion)
								
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'DEL' Begin
	
		--Solo si esta anulado
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Efectivo  Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que el documento no tenga otros registros asociados
		If (Select Depositado From Efectivo  Where IDTransaccion=@IDTransaccion) > 0 Begin
			set @Mensaje = 'El registro ya esta depositado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		If Exists(Select * From DetalleDepositoBancario Where IDTransaccionEfectivo=@IDTransaccion) Begin
			set @Mensaje = 'El registro ya esta depositado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Eliminamos el registro
		Delete Efectivo Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion
				
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	If @Operacion = 'UPD' Begin
		
		--Si esta anulado
		If (Select Anulado From Efectivo  Where IDTransaccion=@IDTransaccion) = 'True' Begin
			set @Mensaje = 'El registro esta anulado! No se puede modificar.'
			set @Procesado = 'False'
			return @@rowcount
		End
				
		--Actualizar Datos
		Update Efectivo Set Fecha=@Fecha,	
							IDTipoComprobante=@IDTipoComprobante,
							Comprobante=@Comprobante,     
							IDMotivo=@IDMotivo,
							Observacion=@Observacion						
		Where IDTransaccion=@IDTransaccion     

		If Not Exists(Select * From DetalleDepositoBancario Where IDTransaccionEfectivo=@IDTransaccion) Begin
			Update Efectivo Set IDMoneda=@IDMoneda,
								Cotizacion=@Cotizacion,
								ImporteMoneda=@ImporteMoneda,
								Total=@Total
			Where IDTransaccion=@IDTransaccion     
		End
									 	
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		print @Mensaje
		return @@rowcount
			
	End
	
	--ANULAR
	If @Operacion = 'ANULAR' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Efectivo  Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Si esta anulado
		If (Select Anulado From Efectivo  Where IDTransaccion=@IDTransaccion) = 'True' Begin
			set @Mensaje = 'El registro esta anulado! No se puede modificar.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Verificar que el documento no tenga otros registros asociados
		If (Select Depositado From Efectivo  Where IDTransaccion=@IDTransaccion) > 0 Begin
			set @Mensaje = 'El registro ya esta depositado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		If Exists(Select * From DetalleDepositoBancario Where IDTransaccionEfectivo=@IDTransaccion) Begin
			set @Mensaje = 'El registro ya esta depositado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Eliminamos el registro
		Update Efectivo Set Anulado='True'
		Where IDTransaccion = @IDTransaccion
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal		
				
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
		
	End
	
	set @Mensaje = 'No se proceso ninguna transacción!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End





﻿CREATE Procedure [dbo].[SpOperacionAutomatica]

	--Entrada
	@ID int,
	@DB varchar(50)=null,
	@Titulo varchar(50)=null,
	@Descripcion varchar(400)=null,
	@IDTipoOperacion int=null,
	@HoraOperacion time(7)=null,
	@Recurrencia varchar(50)=null,
	@FechaOperacion date=null,
	@DiaSemana tinyint=null,
	@DiaMes tinyint=null,
	@IDTipoRecalculoKardex int=null,
	@IDTipoProducto int=null,
	@IDProducto int=null,
	@FechaInicioKardex date=null,
	@SQL varchar(MAX)=null,
	
	@Operacion varchar(50),
	--Salida
	@Mensaje varchar(200) =null output,
	@Procesado bit =null output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Obtenemos el nuevo ID
		declare @vID tinyint
		set @vID = (Select IsNull((Max(ID)+1), 1) From OperacionAutomatica)

		
		--Insertamos
		INSERT INTO OperacionAutomatica(ID,DB,Titulo,Descripcion,IDTipoOperacion,HoraOperacion,Recurrencia,FechaOperacion,DiaSemana,DiaMes,IDTipoRecalculoKardex,IDTipoProducto,IDProducto,FechaInicioKardex,[SQL])
        VALUES (@vID,@DB,@Titulo,@Descripcion,@IDTipoOperacion,@HoraOperacion,@Recurrencia,@FechaOperacion,@DiaSemana,@DiaMes,@IDTipoRecalculoKardex,@IDTipoProducto,@IDProducto,@FechaInicioKardex,@SQL)

		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Insertamos
		update OperacionAutomatica
				set DB=@DB,
				Titulo=@Titulo,
				Descripcion=@Descripcion,
				IDTipoOperacion=@IDTipoOperacion,
				HoraOperacion=@HoraOperacion,
				Recurrencia=@Recurrencia,
				FechaOperacion=@FechaOperacion,
				DiaSemana=@DiaSemana,
				DiaMes=@DiaMes,
				IDTipoRecalculoKardex=@IDTipoRecalculoKardex,
				IDTipoProducto=@IDTipoProducto,
				IDProducto=@IDProducto,
				FechaInicioKardex=@FechaInicioKardex,
				[SQL]=@SQL
		Where ID = @ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		
		--Auditoria
					
		--Eliminamos
		Delete From OperacionAutomatica 
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end


	if @Operacion='INICIO' begin
		
		--Update OperacionAutomatica set ejecutando = 1 where id = @ID
		delete from OperacionAutomaticaRealizada where id = @ID
		INSERT INTO OperacionAutomaticaRealizada(ID,DB,Titulo,Descripcion,IDTipoOperacion,HoraOperacion,Recurrencia,FechaOperacion,DiaSemana,DiaMes,IDTipoRecalculoKardex,IDTipoProducto,IDProducto,FechaInicioKardex,[SQL],FechaInicio,HoraInicio)
        select ID,DB,Titulo,Descripcion,IDTipoOperacion,HoraOperacion,Recurrencia,FechaOperacion,DiaSemana,DiaMes,IDTipoRecalculoKardex,IDTipoProducto,IDProducto,FechaInicioKardex,[SQL],CONVERT(date,GetDate()),Convert(time(7),GetDate())
		from OperacionAutomatica where ID = @ID
		--VALUES (@ID,@DB,@Titulo,@Descripcion,@IDTipoOperacion,@HoraOperacion,@Recurrencia,@FechaOperacion,@DiaSemana,@DiaMes,@IDTipoRecalculoKardex,@IDTipoProducto,@IDProducto,@FechaInicioKardex,@SQL,CONVERT(date,GetDate()),Convert(time(7),GetDate()))
          

	end

	if @Operacion='FIN' begin
		
		Update OperacionAutomatica set ejecutando = 0 where id = @ID

		update OperacionAutomaticaRealizada
		set FechaFin =CONVERT(date,GetDate()),
		HoraFin=Convert(time(7),GetDate())
		where ID = @ID

	end

	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿CREATE Procedure [dbo].[SpAsientoNotaCredito]

	@IDTransaccion numeric(18,0)
As

Begin
	--CARLOS 12-03-2014
	
	SET NOCOUNT ON
	
	--********INVERTIR LA CUENTA DE LA VENTA******************
	-- CREDITO = CONTADO Y CONTADO = CREDITO
	
	--Variables
	Declare @vIDSucursal tinyint
		
	--Venta
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vContado bit
	Declare @vCredito bit
	Declare @vCondicion varchar(50)
	Declare @vFechaEmision date
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money
	Declare @vCuentaContableVenta varchar(50)
	Declare @vCuentaContableCosto varchar(50)
	Declare @vTotalDiscriminado money
	Declare @vTotalDescuentoDiscriminado money
	Declare @vTotalCosto money
	Declare @vConComprobantes bit
	
	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	
	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money
	--Declare @vIDFormaPagoFactura tinyint = 1
	Declare @CotizacionNC as money
	set @CotizacionNC = (Select Cotizacion from NotaCredito where IDTransaccion = @IDTransaccion)

	--Obtener valores
	Begin
	
		Set @vContado='False'
		
		Select	@vIDSucursal=IDSucursal,
				@vTipoComprobante=TipoComprobante,
				@vIDTipoComprobante=IDTipoComprobante,
				@vComprobante=Comprobante,
				@vCredito=Credito,
				@vCondicion=@vCondicion,
				@vConComprobantes=ConComprobantes,
				@vFechaEmision=Fecha,
				@vIDMoneda=IDMoneda,
				@vCotizacion=Cotizacion,
				@vCondicion=Condicion
		From VNotaCredito Where IDTransaccion=@IDTransaccion
	
		If @vCredito=0 Begin
			Set @vContado='True'
		End
		
	End
					
	--Verificar que el asiento se pueda modificar
	Begin
	
		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta procesado
		If (Select Procesado From NotaCredito Where IDTransaccion=@IDTransaccion) = 'False' Begin
			print 'El documentos no es valido'
			GoTo salir
		End 	
		
		--Si esta anulado
		If (Select Anulado From NotaCredito Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El documento esta anulado'
			GoTo salir
		End
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End
		
		--Solo si es por devolucion
		If (Select Devolucion From NotaCredito Where IDTransaccion=@IDTransaccion) = 'False' Begin
			print 'Solo se pueden generar asientos para Notas de Credito por devolucion.'
			Exec SpAsientoNotaCreditoDescuento @IDTransaccion=@IDTransaccion
			GoTo salir

		End
		
		print 'Los datos son correctos!'
		
		--Solo si NO es comprobante interno genera asiento JGR 20140816
		If @vTipoComprobante = 'NCIC' Begin--Nota credito interno cliente
			print 'NCIC'
			GoTo salir
--			print 'NCIC'
		End
	End

	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = @vTipoComprobante + ' ' + @vComprobante + ' ' + @vCondicion
		print @idtransaccion
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFechaEmision, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	
	--Cuentas Fijas de Clientes
	Begin
	
		--Variables
		Declare @vVentaCredito bit
		Declare @vVentaContado bit
		Declare @vCodigoCuentaCliente varchar(50)
		
		Declare cCFVentaCliente cursor for
		Select Codigo, Debe, Haber, Credito, Contado From VCFVentaCliente Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFVentaCliente   
		fetch next from cCFVentaCliente into @vCodigo, @vDebe, @vHaber, @vVentaCredito, @vVentaContado
		
		While @@FETCH_STATUS = 0 Begin 
		
		   
			If @vIDSucursal <> 5 Begin -- Si no es MErcury
				Set @vCodigoCuentaCliente = dbo.FCuentaContableClienteNC(@IDTransaccion, @vIDSucursal)
			End

			--IF Exists(Select * from CLiente where ID = @vIDCliente and CodigoCuentaContable <> '') Begin
			--	Set @vCodigoCuentaCliente = (Select CodigoCuentaContable from Cliente where id = @vIDCliente)
			--End

			If @vCodigoCuentaCliente != '' Begin
				Set @vCodigo = @vCodigoCuentaCliente
			End 
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Credito
			If @vConComprobantes = 'True' Begin
			
				--Si Credito y Contado son iguales
				If @vVentaCredito = 'True' And @vVentaContado = 'True' Begin	
					
					Set @vImporte = (Select 
									Round((SUM(Importe) * @CotizacionNC),0)
									From NotaCreditoVenta NCV
									Join VVenta V On NCV.IDTransaccionVentaGenerada=V.IDTransaccion
									Where NCV.IDTransaccionNotaCredito=@IDTransaccion)
										
				End Else Begin
					If @vVentaCredito = 'True' Begin	
						Set @vImporte = (Select 
										Round((SUM(Importe) * @CotizacionNC),0)
										From NotaCreditoVenta NCV
										Join VVenta V On NCV.IDTransaccionVentaGenerada=V.IDTransaccion
										Where NCV.IDTransaccionNotaCredito=@IDTransaccion And V.Credito='True')
					End Else Begin
					
						--Contado
					
						Set @vImporte = (Select 
										Round((SUM(Importe) * @CotizacionNC),0)
										From NotaCreditoVenta NCV
										Join VVenta V On NCV.IDTransaccionVentaGenerada=V.IDTransaccion
										Where NCV.IDTransaccionNotaCredito=@IDTransaccion And V.Credito='False')
					End
				End
				--set @vIDFormaPagoFactura = (Select top(1) V.IDFormaPagoFactura
				--					From NotaCreditoVenta NCV
				--					Join VVenta V On NCV.IDTransaccionVentaGenerada=V.IDTransaccion
				--					Where NCV.IDTransaccionNotaCredito=@IDTransaccion)
			End
			
			If @vConComprobantes = 'False' Begin
				Set @vImporte = (Select Round((Total * @cotizacionNC),0) From NotaCredito Where IDTransaccion=@IDTransaccion)				
			End
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				

			If @vCredito = 'True' Begin
				If @vVentaCredito = 'False' Begin
					Set @vImporteHaber = 0
					Set @vImporteDebe = 0
				End
			End Else Begin
				If @vVentaContado = 'False' Begin
					Set @vImporteHaber = 0
					Set @vImporteDebe = 0
				End
			End
				
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin				
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteDebe, @vImporteHaber, 0, '')
			End
			
			fetch next from cCFVentaCliente into @vCodigo, @vDebe, @vHaber, @vVentaCredito, @vVentaContado
			
		End
		
		close cCFVentaCliente 
		deallocate cCFVentaCliente
	End
	
	--Cuentas Fijas Descuentos
	Begin
		
		--Variables
		Declare @vIDTipoDescuento tinyint
		Declare @vIDProveedorDescuento int
		
		Declare cCFVentaDescuento cursor for
		Select Codigo, IDTipoDescuento, Debe, Haber, IDProveedor From VCFVentaDescuento Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFVentaDescuento
		fetch next from cCFVentaDescuento into @vCodigo, @vIDTipoDescuento, @vDebe, @vHaber, @vIDProveedorDescuento
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			If @vIDTipoDescuento = 4 Begin
				Set @vIDTipoDescuento = 0
			End
			
			If @vIDProveedorDescuento Is Null Begin		
				Set @vImporte = (Select Round((SUM(TotalDescuentoDiscriminado) * @CotizacionNC),0) From VDescuento Where IDTransaccion=@IDTransaccion And IDTipoDescuento=@vIDTipoDescuento)
			End Else Begin
				Set @vImporte = (Select Round((SUM(TotalDescuentoDiscriminado) * @CotizacionNC),0) From VDescuento Where IDTransaccion=@IDTransaccion And IDTipoDescuento=@vIDTipoDescuento And IDProveedor=@vIDProveedorDescuento)
			End
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteDebe, @vImporteHaber, 0, '')
			End
			
			fetch next from cCFVentaDescuento into @vCodigo, @vIDTipoDescuento, @vDebe, @vHaber, @vIDProveedorDescuento
			
		End
		
		close cCFVentaDescuento
		deallocate cCFVentaDescuento
	End
					
	--Cuentas Fijas de Venta 
	Begin
	
		--Cuenta Fija Venta
		Set @vIDCuentaContable = NULL
		Set @vCuentaContableVenta = ''
		Set @vTotalDiscriminado = 0
		Set @vTotalDescuentoDiscriminado = 0
		
		Declare cDetalleVenta cursor for
		Select 
		--'CuentaContableVenta'=IsNull(dbo.FCuentaContableVenta(D.IDProducto, @vIDSucursal,@vIDFormaPagoFactura),''), 
		'CuentaContableVenta'=IsNull(dbo.FCuentaContableVenta(D.IDProducto, @vIDSucursal,1),''), 
		'TotalDiscriminado'=Round((SUM(D.TotalDiscriminado) * @CotizacionNC),0),
		'TotalDescuentoDiscriminado'=IsNull((Select Round((Sum(DE.DescuentoDiscriminado) * @CotizacionNC),0) From Descuento DE Where DE.IDTransaccion=D.IDTransaccion And DE.IDProducto=D.IDProducto And DE.ID=D.ID),0) * D.Cantidad
		From VDetalleNotaCredito D 
		Where D.IDTransaccion=@IDTransaccion 
		Group By D.IDTransaccion, D.IDProducto, D.ID, D.Cantidad
		Open cDetalleVenta 
		fetch next from cDetalleVenta into @vCuentaContableVenta, @vTotalDiscriminado, @vTotalDescuentoDiscriminado
		
		While @@FETCH_STATUS = 0 Begin  
			
			set @vImporte = 0
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vCodigo = @vCuentaContableVenta
			
			--Si no existe, poner el predeterminado
			If @vCuentaContableVenta Is Null or @vCuentaContableVenta = '' Begin
				Set @vCodigo = (Select IsNull((Select Top(1) Codigo From VCFVentaVenta Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda),''))				
			End
			
			--Validar codigo
			If @vCodigo = '' Begin
				fetch next from cDetalleVenta into @vCuentaContableVenta, @vTotalDiscriminado, @vTotalDescuentoDiscriminado
			End
						
			--Hayar el importe	
			Set @vImporte = @vTotalDiscriminado + @vTotalDescuentoDiscriminado
			Set @vDebe = (Select Top(1) Debe From VCFVentaVenta Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda)
			Set @vHaber = (Select Top(1) Haber From VCFVentaVenta Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
																
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			--Cargamod
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin
			
				--Si existe la cuenta, actualizar
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+@vImporteDebe,
												Debito=Debito+@vImporteHaber,
												Importe=Importe+@vImporte
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteDebe, @vImporteHaber, 0, '')
				End
			End
				
			fetch next from cDetalleVenta into @vCuentaContableVenta, @vTotalDiscriminado, @vTotalDescuentoDiscriminado
			
		End
		
		close cDetalleVenta 
		deallocate cDetalleVenta
		
	End
			
	--Cuentas Fijas Impuesto
	Begin
		
		--Variables
		Declare @vIDImpuesto tinyint
		
		Declare cCFVentaImpuesto cursor for
		Select Codigo, IDImpuesto, Debe, Haber From VCFVentaImpuesto Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFVentaImpuesto   
		fetch next from cCFVentaImpuesto into @vCodigo, @vIDImpuesto, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			Set @vImporte = (Select Round((SUM(TotalImpuesto) * @CotizacionNC),0) From DetalleImpuesto Where IDTransaccion=@IDTransaccion And IDImpuesto=@vIDImpuesto)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteDebe, @vImporteHaber, 0, '')
			End
			
			fetch next from cCFVentaImpuesto into @vCodigo, @vIDImpuesto, @vDebe, @vHaber
			
		End
		
		close cCFVentaImpuesto 
		deallocate cCFVentaImpuesto
	End					

	--Cuentas Fijas de Mercaderia
	Begin
	
		--Variables
		
		Declare cCFVentaMercaderia cursor for
		Select Codigo, Debe, Haber From VCFVentaMercaderia Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFVentaMercaderia   
		fetch next from cCFVentaMercaderia into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			Set @vImporte = (Select Round((SUM(TotalCosto) * @CotizacionNC),0) From VDetalleNotaCredito Where IDTransaccion=@IDTransaccion)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteDebe, @vImporteHaber, 0, '')
			End
			
			fetch next from cCFVentaMercaderia into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFVentaMercaderia 
		deallocate cCFVentaMercaderia
	End
	
	--Cuentas Fijas de Costo de Mercaderia
	Begin
	
		--Cuenta Fija Venta
		Set @vCuentaContableVenta = ''
		Set @vTotalCosto = 0
			
		Declare cDetalleCosto cursor for
		Select 
		'CuentaContableCosto'=IsNull(dbo.FCuentaContableCosto(IDProducto, @vIDSucursal),''), 
		'TotalCosto'=Round((Sum(TotalCosto) * @cotizacionNC),0)
		From VDetalleNotaCredito
		Where IDTransaccion=@IDTransaccion Group By IDProducto
		Open cDetalleCosto
		fetch next from cDetalleCosto into @vCuentaContableCosto, @vTotalCosto
		
		While @@FETCH_STATUS = 0 Begin  
			
			set @vImporte = 0
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vCodigo = @vCuentaContableCosto
			
			--Si no existe, poner el predeterminado
			If @vCuentaContableCosto = '' Begin
				Set @vCodigo = (Select IsNull((Select Top(1) Codigo From VCFVentaCostoMercaderia Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda),''))								
			End
			
			--Validar codigo
			If @vCodigo = '' Begin
				fetch next from cDetalleCosto into @vCuentaContableCosto, @vTotalCosto
			End
			
			--Hayar el importe	
			Set @vImporte = @vTotalCosto
			Set @vDebe = (Select Top(1) Debe From VCFVentaCostoMercaderia Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda)
			Set @vHaber = (Select Top(1) Haber From VCFVentaCostoMercaderia Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')			
										
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			--Cargamod
			If @vImporteHaber > 0 Or @vImporteDebe > 0 Begin
			
				--Si existe la cuenta, actualizar
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+@vImporteDebe,
												Debito=Debito+@vImporteHaber,
												Importe=Importe+@vImporte
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteDebe, @vImporteHaber, 0, '')
				End
			End
							
			fetch next from cDetalleCosto into @vCuentaContableCosto, @vTotalCosto
			
		End
		
		close cDetalleCosto 
		deallocate cDetalleCosto
		
	End 

	Execute SpRedondearAsientoNotaCreditoERSA @IDTransaccion
	
	--Actualizamos la cabecera, el total
	Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
	Set @vImporteHaber = ROUND(@vImporteHaber, 0)
	Set @vImporteDebe = ROUND(@vImporteDebe, 0)
	
	Update Asiento Set Total = @vImporteHaber,
						Credito = @vImporteHaber,
						Debito = @vImporteDebe,
						Saldo = @vImporteHaber - @vImporteDebe
	Where IDTransaccion=@IDTransaccion
	
	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
		
End


﻿CREATE Procedure [dbo].[SpPedidoDiferenciaPrecio]

	--Entrada
	@IDTransaccionPedido numeric(18,0) = 0,
	@IDTransaccionPedidoNotaCredito numeric(18,0) = 0,
	@IDTransaccionVenta numeric(18,0) = 0,
	@IDTransaccionNotaCredito numeric(18,0) = 0,
	@Operacion varchar(50)

		
As

Begin
	Declare @Retorno as bit = 0
	Declare @IDCliente as int
	Declare @IDTipoDescuento as int
	Declare @IDListaPrecio as int
	Declare @PrecioNuevo as decimal
	
	Declare @vIDProducto as int
	Declare @vPrecioUnitario as decimal
	Declare @vCantidad as decimal

	Declare @vIDUsuario as int
	Declare @vFecha as date
	
	Set @IDTipoDescuento = (Select ID from TipoDescuento where descripcion = 'DIFERENCIA PRECIO')
	
	

	if @Operacion = 'INSPEDIDOVENTA' BEGIN
	
		if not exists(Select * from pedido where idtransaccion = @IDTransaccionPedido) begin
			print('no existe')
			Set @Retorno = 0
			goto Salir
		end

		Select 
			@IDCliente = IDCliente,
			@IDListaPrecio=IDListaPrecio,
			@vFecha = FechaFacturar
		From Pedido where IDTransaccion = @IDTransaccionPedido

		Declare cCFVenta cursor for
				Select IDProducto, 'PrecioUnitario'=(PrecioUnitario - DescuentoUnitario), Cantidad From DetallePedido
				Where IDTransaccion =@IDTransaccionPedido
				Open cCFVenta   
			fetch next from cCFVenta into @vIDProducto, @vPrecioUnitario, @vCantidad

			While @@FETCH_STATUS = 0 Begin  
				print @vPrecioUnitario
				Set @PrecioNuevo = @vPrecioUnitario - (select Top(1)ISNULL(Descuento,0) 
														from vProductoListaPrecioExcepciones 
														where IDProducto = @vIDProducto 
														and ListaPrecio = (select top(1) descripcion from listaprecio where id =@IDListaPrecio)
														and IDCliente = @IDCliente
														and IDTipoDescuento = @IDTipoDescuento
														and @vFecha between Desde and Hasta
														and (Case When CantidadLimite > 0 then CantidadLimite else 0 end) >= (Case When CantidadLimite > 0 then CantidadLimiteSaldo+@vCantidad else 0 end))
				
				print concat('precionuevo ', @PrecioNuevo) 
				if @PrecioNuevo <@vPrecioUnitario begin
					insert into DetalleNotaCreditoDiferenciaPrecio(IDProducto, IDCliente, IDListaPrecio, Cantidad, PrecioOriginal, NuevoPrecio, IDTransaccionPedidoVenta, IDTransaccionPedidoNotaCredito, IDTransaccionVenta, IDTransaccionNotaCredito,AutomaticoPorExcepcion)
					 Values (@vIDProducto, @IDCliente, @IDListaPrecio, @vCantidad, @vPrecioUnitario, @PrecioNuevo,@IDTransaccionPedido,0,0,0,1)
					print concat('precionuevo ', @PrecioNuevo) 
					Set @Retorno = 1
				end
			
				fetch next from cCFVenta into @vIDProducto, @vPrecioUnitario, @vCantidad

			End

			close cCFVenta 
		deallocate cCFVenta

	END

	if @Operacion = 'UPDPEDIDONOTACREDITO' BEGIN
		
		Select @vIDUsuario= IDUsuario, @vFecha = Fecha from transaccion where ID = @IDTransaccionPedidoNotaCredito

		if not exists(Select * from Pedido where IDTransaccion = @IDTransaccionPedido) begin
			Set @Retorno = 0
			goto Salir
		end

		if not exists(Select * from PedidoNotaCredito where IDTransaccion = @IDTransaccionPedidoNotaCredito) begin
			Set @Retorno = 0
			goto Salir
		end
						
		Update DetalleNotaCreditoDiferenciaPrecio set IDTransaccionPedidoNotaCredito = @IDTransaccionPedidoNotaCredito
		where IDTransaccionPedidoVenta = @IDTransaccionPedido

		Update PedidoNotaCredito
			Set EstadoNC  = 1,
			IDUsuarioAprobado = @vIDUsuario,
			FechaAprobado = @vFecha
			Where IDTransaccion = @IDTransaccionPedidoNotaCredito
		
		Set @Retorno = 1
	END

Salir:
	Select @Retorno
		
End






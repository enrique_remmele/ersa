﻿CREATE Procedure [dbo].[SpMoneda]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Referencia varchar(10),
	@Decimales  bit,
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	Declare @vMultiplica bit = 'False'
	Declare @vDivide bit = 'False'

	--Si la moneda es la principal ID = 1, entonces la conversion es Divide
	--de lo contrario, multiplica

	If @ID = 1 Begin
		Set @vMultiplica = 'False'
		Set @vDivide = 'True'
	End
	
	If @ID <> 1 Begin
		Set @vMultiplica = 'True'
		Set @vDivide = 'False'
	End

	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Moneda Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDMoneda tinyint
		set @vIDMoneda = (Select IsNull((Max(ID)+1), 1) From Moneda)

		--Insertamos
		Insert Into Moneda(ID, Descripcion, Referencia, Decimales, Estado, Multiplica, Divide)
		Values(@vIDMoneda, @Descripcion, @Referencia, @Decimales, @Estado, @vMultiplica, @vDivide)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='MONEDA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From Moneda Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Moneda Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Moneda Set	Descripcion=@Descripcion,
							Referencia=@Referencia,
							Decimales=@Decimales,
							Estado = @Estado,
							Multiplica = @vMultiplica,
							Divide = @vDivide
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='MONEDA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Moneda Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con CLIENTES
		if exists(Select * From Cliente Where IDMoneda=@ID) begin
			set @Mensaje = 'El registro tiene clientes asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con DetalleDistribucion
		if exists(Select * From DetalleDistribucion Where IDMoneda=@ID) begin
			set @Mensaje = 'El registro tiene Distribuciones asociadas! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Producto
		if exists(Select * From Producto Where IDMonedaUltimoCosto=@ID) begin
			set @Mensaje = 'El registro tiene productos asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Moneda 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='MONEDA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


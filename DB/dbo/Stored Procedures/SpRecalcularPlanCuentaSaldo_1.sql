﻿CREATE Procedure [dbo].[SpRecalcularPlanCuentaSaldo]

	--Entrada
	@Mes int,
	@Año int
As

Begin
	
	--Primero eliminamos los registros
	Delete From PlanCuentaSaldo Where Año = @Año And Mes = @Mes
	
	--Variables
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	Declare @vRegistros int
	Declare @AñoInicial smallint
	
	Declare @vCuentaTipo varchar(6)
	Declare @vCuenta varchar(10)
	Declare @vIDSucursal tinyint
	Declare @vIDCentroCosto tinyint
	Declare @vDebito money
	Declare @vCredito money
	Declare @vSaldo money
	Declare @vDebitoAcumulado money
	Declare @vCreditoAcumulado money
	Declare @vSaldoAcumulado money
	
	Declare @vPrefijo varchar(50)

	--Set @vRegistros=0
	--Goto Salir
	
	Set @AñoInicial = 2000
	Set @vRegistros = 0
	
	Declare db_cursor cursor for
	Select
	'Cuenta'=DA.CuentaContable,
	'IDSucursal'=A.IDSucursal,
	'IDCentroCosto'=IsNull(DA.IDCentroCosto, IsNull(A.IDCentroCosto, 1)),
	'Debito'=SUM(DA.Debito),
	'Credito'=SUM(DA.Credito),
	'Saldo'=0,
	'DebitoAcumulado'=0,
	'CreditoAcumulado'=0,
	'SaldoAcumulado'=0

	From DetalleAsiento	DA
	Join Asiento A On DA.IDTransaccion=A.IDTransaccion
	Join VCuentaContable CC On DA.CuentaContable=CC.Codigo
	Where YEAR(A.Fecha) = @Año
	And Month(A.Fecha) = @Mes
	
	Group By YEAR(A.Fecha), Month(A.Fecha), DA.CuentaContable, A.IDSucursal, IsNull(DA.IDCentroCosto, IsNull(A.IDCentroCosto, 1))
	
	Open db_cursor   
	Fetch Next From db_cursor Into @vCuenta, @vIDSucursal, @vIDCentroCosto, @vDebito, @vCredito, @vSaldo, @vDebitoAcumulado, @vCreditoAcumulado, @vSaldoAcumulado
	While @@FETCH_STATUS = 0 Begin 
		
		--Obtener Credito/Debito acumulados
		
		--Años anteriores
		Set @vDebitoAcumulado = IsNull((Select SUM(Debito) From PlanCuentaSaldo Where Año<@Año And Año>=@AñoInicial And IDSucursal=@vIDSucursal And Cuenta=@vCuenta And IDCentroCosto=@vIDCentroCosto), 0 )
		
		--Año actual
		Set @vDebitoAcumulado = @vDebitoAcumulado + IsNull((Select SUM(Debito) From PlanCuentaSaldo Where Año=@Año And Año>=@AñoInicial And Mes<@Mes And IDSucursal=@vIDSucursal And Cuenta=@vCuenta And IDCentroCosto=@vIDCentroCosto), 0 )
		Set @vDebitoAcumulado = @vDebitoAcumulado + @vDebito
		
		--Años anteriores
		Set @vCreditoAcumulado = IsNull((Select SUM(Credito) From PlanCuentaSaldo Where Año<@Año And Año>=@AñoInicial And IDSucursal=@vIDSucursal And Cuenta=@vCuenta And IDCentroCosto=@vIDCentroCosto), 0 )
		
		--Año actual
		Set @vCreditoAcumulado = @vCreditoAcumulado + IsNull((Select SUM(Credito) From PlanCuentaSaldo Where Año=@Año And Año>=@AñoInicial And Mes<@Mes And IDSucursal=@vIDSucursal And Cuenta=@vCuenta And IDCentroCosto=@vIDCentroCosto), 0 )
		Set @vCreditoAcumulado = @vCreditoAcumulado + @vCredito
		
		Set @vPrefijo = SubString(@vCuenta, 0, 2)

		--Cuentas del Debe
		If @vPrefijo = '1' Or @vPrefijo = '5' Begin
			Set @vCuentaTipo = 'DEBE'
		End
		
		--Cuentas del Haber
		If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
			Set @vCuentaTipo = 'HABER'
		End
	
		If @vCuentaTipo = 'DEBE' Begin
			Set @vSaldo = @vDebito - @vCredito
			Set @vSaldoAcumulado = @vDebitoAcumulado  - @vCreditoAcumulado
		End
		
		If @vCuentaTipo = 'HABER' Begin
			Set @vSaldo = @vCredito - @vDebito
			Set @vSaldoAcumulado = @vCreditoAcumulado - @vDebitoAcumulado
		End
		
		--Si existe, actualizamos
		If Exists(Select * From PlanCuentaSaldo Where Año=@Año And Mes=@Mes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal And IDCentroCosto=@vIDCentroCosto) Begin
			
			Update PlanCuentaSaldo Set Debito=@vDebito,
										Credito=@vCredito,
										Saldo=@vSaldo,
										DebitoAcumulado=@vDebitoAcumulado,
										CreditoAcumulado=@vCreditoAcumulado,
										SaldoAcumulado=@vSaldoAcumulado
			Where Año=@Año And Mes=@Mes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal And IDCentroCosto=@vIDCentroCosto
			
		End Else Begin
			
			Insert Into PlanCuentaSaldo(Año, Mes, Cuenta, IDSucursal, IDCentroCosto, Debito, Credito, Saldo, DebitoAcumulado, CreditoAcumulado, SaldoAcumulado)
			Values(@Año, @Mes, @vCuenta, @vIDSucursal, @vIDCentroCosto, @vDebito, @vCredito, @vSaldo, @vDebitoAcumulado, @vCreditoAcumulado, @vSaldoAcumulado)
		End
		 
		Set @vRegistros = @vRegistros + 1
		 	
		Fetch Next From db_cursor Into @vCuenta, @vIDSucursal, @vIDCentroCosto, @vDebito, @vCredito, @vSaldo, @vDebitoAcumulado, @vCreditoAcumulado, @vSaldoAcumulado
									
	End
	
	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor
	
	--Salir:
	Set @vMensaje = 'Registros procesados!'
	Set @vProcesado = 'True'
	
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado, 'Registros'=@vRegistros
	
End

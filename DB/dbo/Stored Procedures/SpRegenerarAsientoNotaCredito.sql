﻿CREATE Procedure [dbo].[SpRegenerarAsientoNotaCredito]

	@Desde date,
	@Hasta date,
	@Todos bit = 'False',
	@SoloConDiferencias bit = 'False',
	@IDSucursal tinyint
	
As

Begin
	
	--Variables
	Begin
		Declare @vIDTransaccion numeric(18,0)		
	End
	
	If Not Exists (Select * From sys.objects Where object_id = OBJECT_ID(N'[dbo].[GeneracionAsientoNotaCredito]') And type in (N'U')) Begin
		Create Table GeneracionAsientoNotaCredito(ID int, Cantidad int, RegistrosProcesados int, Porcentaje int)
	End
	
	TRUNCATE TABLE GeneracionAsientoNotaCredito
	
	Declare @vCantidad int
	Declare @vRegistrosProcesados int
	Declare @vPorcentaje int
	
	Set @vCantidad = (Select Count(*) From VNotaCredito NC Where NC.Fecha Between @Desde And @Hasta And NC.IDSucursal=@IDSucursal And (Case When @SoloConDiferencias = 0 Then 1 Else (Case When (Select A.Saldo From Asiento A Where A.IDTransaccion=NC.IDTransaccion)<>0 Then 1 Else 0 End) End) = 1)
	Set @vRegistrosProcesados = 0
	Set @vPorcentaje = 0
	
	print @vCantidad

	Insert Into GeneracionAsientoNotaCredito(ID, Cantidad, RegistrosProcesados, Porcentaje)
	Values(1, @vCantidad, 0, 0)
	
	Declare cNotaCredito cursor for
	Select IDTransaccion From VNotaCredito NC 
	Where NC.Fecha Between @Desde And @Hasta 
	And NC.IDSucursal=@IDSucursal 
	And (Case When @SoloConDiferencias = 0 Then 1 Else (Case When (Select A.Saldo From Asiento A Where A.IDTransaccion=NC.IDTransaccion)<>0 Then 1 Else 0 End) End) = 1

	Open cNotaCredito   
	fetch next from cNotaCredito into @vIDTransaccion

	While @@FETCH_STATUS = 0 Begin  			  

		--Generar
		Exec SpAsientoNotaCredito @IDTransaccion=@vIDTransaccion		
		
		--Redondear
		Exec SpRedondearAsientoNotaCredito @IDTransaccion=@vIDTransaccion, @Credito='False'
		
		--Contador
		Set @vRegistrosProcesados = @vRegistrosProcesados + 1
		Set @vPorcentaje = Round((@vRegistrosProcesados / @vCantidad) * 100, 0)
		
		Update GeneracionAsientoNotaCredito Set RegistrosProcesados=@vRegistrosProcesados,
													Porcentaje=@vPorcentaje
		Where ID=1
					
		--Siguiente
		fetch next from cNotaCredito into @vIDTransaccion
		
	End   
	
	close cNotaCredito   
	deallocate cNotaCredito
	
	--Eliminar la tabla
	If Exists (Select * From sys.objects Where object_id = OBJECT_ID(N'[dbo].[GeneracionAsientoNotaCredito]') And type in (N'U')) Begin
		Drop Table GeneracionAsientoNotaCredito
	End
	
	Select 'Mensaje'='Registros procesados', 'Procesado'='True'
End



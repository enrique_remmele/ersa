﻿CREATE Procedure [dbo].[SpDetalleVenta2]

	--Entrada
	@IDTransaccion numeric(18,0),
	@IDProducto int = NULL,
	@ID tinyint = NULL,
	@IDDeposito tinyint = NULL,
	@Observacion varchar(50) = NULL,
	@IDImpuesto tinyint = NULL,
	@Cantidad decimal(10,2) = NULL,
	@PrecioUnitario money = NULL,
	@DescuentoUnitario money = NULL,
	@PorcentajeDescuento numeric(2,0) = NULL,
	@Total money = NULL,
	@TotalImpuesto money = NULL,
	@TotalDescuento money = NULL,
	@TotalDiscriminado money = NULL,
	@Caja bit = NULL,
	@CantidadCaja smallint = NULL,
	
	--Costo
	@CostoUnitario money = NULL,
	@TotalCosto money = NULL,
	@TotalCostoImpuesto money = NULL,
	@TotalCostoDiscriminado money = NULL,
	
	@Operacion varchar(20),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin

	--Validar
	
	--BLOQUES
	if @Operacion='INS' Begin
		 
		--Insertar el detalle
		Insert Into DetalleVenta(IDTransaccion, IDProducto, ID, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, DescuentoUnitario, PorcentajeDescuento, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado, Caja, CantidadCaja, CostoUnitario, TotalCosto, TotalCostoImpuesto, TotalCostoDiscriminado)
		Values(@IDTransaccion, @IDProducto, @ID, @IDDeposito, @Observacion, @IDImpuesto, @Cantidad, @PrecioUnitario, @DescuentoUnitario, @PorcentajeDescuento, @Total, @TotalImpuesto, @TotalDescuento, @TotalDiscriminado, @Caja, @CantidadCaja, @CostoUnitario, @TotalCosto, @TotalCostoImpuesto, @TotalCostoDiscriminado)
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount	
		
			
	End
	
	
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	return @@rowcount
	
End



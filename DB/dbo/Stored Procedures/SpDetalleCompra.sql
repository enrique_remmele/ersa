﻿CREATE Procedure [dbo].[SpDetalleCompra]

	--CARLOS 12-02-2015
	
	--Entrada
	@IDTransaccion numeric(18,0),
	@IDProducto int = NULL,
	@ID tinyint = NULL,
	@IDDeposito tinyint = NULL,
	@Observacion varchar(50) = NULL,
	@IDImpuesto tinyint = NULL,
	@Cantidad decimal(10,2) = NULL,
	@PrecioUnitario money = NULL,
	@Total money = NULL,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@Caja bit = NULL,
	@CantidadCaja int = NULL,
	@Operacion varchar(20),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin
	--Variable
	Declare @RetencionIVA money
	Declare @PorcentajeRetencion tinyint
	Declare @Cotizacion int
	set @Cotizacion = (select Top(1)Isnull(Cotizacion,1) from Compra where IDTransaccion = @IDTransaccion)
	
	--Variables Kardex
	Declare @vFecha datetime
	Declare @vIDSucursal integer
	Declare @vNroComprobante varchar(50)
	Declare @vCostoEntrada decimal(10,2)

	Set @vFecha = (Select Fecha From Compra Where IDTransaccion = @IDTransaccion)
	Set @vIDSucursal = (Select IDSucursal From Deposito Where ID = @IDDeposito)
	Set @vNroComprobante = (Select NroComprobante From Compra Where IDTransaccion = @IDTransaccion)
	Set @vCostoEntrada = IsNull(@TotalDiscriminado/@Cantidad,0) * @Cotizacion

	--Validar
	--Transaccion
	If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--Transaccion
	If Not Exists(Select * From Compra Where IDTransaccion=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--BLOQUES
	if @Operacion='INS' Begin
	
		--Cantidad
		If @Cantidad <= 0 Begin
			Set @Mensaje = 'La cantidad no puede ser igual ni menor a 0!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		Set @IDDeposito = (Select Top(1) IDDeposito From Compra Where IDTransaccion=@IDTransaccion)
		
		--Retencion
		set @PorcentajeRetencion = (Select Top 1 CompraPorcentajeRetencion  from Configuraciones )
		set @RetencionIVA  = (@TotalImpuesto  * @PorcentajeRetencion) / 100
		set @RetencionIVA = ROUND(@RetencionIVA,0)
		
		--Redondear!!!!!!
		
		--Insertar el detalle
		Insert Into DetalleCompra(IDTransaccion, IDProducto, ID, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, Total, TotalImpuesto, TotalDiscriminado, Caja, CantidadCaja, RetencionIVA)
		Values(@IDTransaccion, @IDProducto, @ID, @IDDeposito, @Observacion, @IDImpuesto, @Cantidad, @PrecioUnitario, @Total, @TotalImpuesto, @TotalDiscriminado, @Caja, @CantidadCaja, @RetencionIVA)
		
		EXEC SpActualizarProductoDeposito
			@IDDeposito = @IDDeposito,
			@IDProducto = @IDProducto,
			@Cantidad = @Cantidad,
			@Signo = N'+',
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT
		
		EXEC SpRegistroCosto @IDProducto = @IDProducto, @Cantidad = @Cantidad, @Costo = @Total, @PrecioUnitario=@PrecioUnitario, @Operacion = 'INS', @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Actualiza el Kardex			
		EXEC SpKardex
			@Fecha = @vFecha,
			@IDTransaccion = @IDTransaccion,
			@IDProducto = @IDProducto,
			@ID = @ID,
			@Orden = 1,
			@CantidadEntrada = @Cantidad,
			@CantidadSalida = 0,
			@CostoEntrada = @vCostoEntrada,
			@CostoSalida = 0,
			@IDSucursal = @vIDSucursal,
			@Comprobante = @vNroComprobante	
		--Actualizar el Costo del Producto
		EXEC SpActualizarCostoProducto @ID = @IDProducto, @Costo = @PrecioUnitario, @IDImpuesto = @IDImpuesto, @IDTransaccion = @IDTransaccion, @Operacion = @Operacion
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'False'
		return @@rowcount	
			
	End
	
	if @Operacion='DEL' Begin
		
		
		Begin
			Declare @vID tinyint
			Declare @vIDDeposito tinyint
			Declare @vIDProducto int
			Declare @vCantidad decimal(10,2)
			Declare @vPrecioUnitario money
			Declare @vCosto money
			Declare @vIDImpuesto tinyint
			Declare @vControlarExistencia bit
			
			Declare db_cursorEliminar cursor for
			Select DC.ID, DC.IDProducto, DC.IDDeposito, DC.Cantidad, DC.PrecioUnitario, DC.Total, DC.IDImpuesto, P.ControlarExistencia
			From Compra C Join DetalleCompra DC On C.IDTransaccion=DC.IDTransaccion Join Producto P On DC.IDProducto=P.ID
			Where C.IDTransaccion=@IDTransaccion 
			Open db_cursorEliminar   
			fetch next from db_cursorEliminar into @vID, @vIDProducto, @vIDDeposito, @vCantidad, @vPrecioUnitario, @vCosto, @vIDImpuesto, @vControlarExistencia

			While @@FETCH_STATUS = 0 Begin  			  

				If @vControlarExistencia = 1 Begin
					EXEC SpActualizarProductoDeposito
						@IDDeposito = @vIDDeposito,
						@IDProducto = @vIDProducto,
						@Cantidad = @vCantidad,
						@Signo = N'-',
						@ControlStock  = 'False',
						@Mensaje = @Mensaje OUTPUT,
						@Procesado = @Procesado OUTPUT
					End
						
					--Eliminar el registro del kardex
					Delete From Kardex Where IDTransaccion=@IDTransaccion And IDProducto=@vIDProducto

					--Recalcular kardex
					EXEC SpRecalcularKardexProducto @IDProducto = @vIDProducto, @FechaInicio = @vFecha

					--Eliminar el Registro
					Delete From DetalleCompra Where IDTransaccion=@IDTransaccion And IDProducto=@vIDProducto And ID=@vID
					
					--Actualizamos el Registro de Costos
					EXEC SpRegistroCosto @IDProducto = @vIDProducto, @Cantidad = @vCantidad, @Costo = @vCosto, @PrecioUnitario=@vPrecioUnitario, @Operacion = 'DEL', @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
					
					--Actualizar el Costo del Producto
					EXEC SpActualizarCostoProducto @ID = @vIDProducto, @Costo = @vPrecioUnitario, @IDImpuesto = @vIDImpuesto, @IDTransaccion = @IDTransaccion, @Operacion = @Operacion
		
					--Siguiente
				   fetch next from db_cursorEliminar into @vID, @vIDProducto, @vIDDeposito, @vCantidad, @vPrecioUnitario, @vCosto, @vIDImpuesto, @vControlarExistencia
			End   
			
			Close db_cursorEliminar   
			deallocate db_cursorEliminar
			
		End
		
		Set @Mensaje = 'Detalle procesado!'
		Set @Procesado = 'True'
		return @@rowcount	
			
	End
	
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	return @@rowcount
	
End




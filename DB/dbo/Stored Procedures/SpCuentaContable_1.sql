﻿CREATE Procedure [dbo].[SpCuentaContable]

	--Entrada
	@ID smallint = NULL,
	@IDPlanCuenta tinyint = NULL,
	@Codigo varchar(50) = NULL,
	@Descripcion varchar(100) = NULL,
	@Alias varchar(50) = NULL,
	@Categoria tinyint = NULL,
	@IDPadre smallint = NULL,
	@Estado bit = NULL,
	@Sucursal bit = NULL,
	@IDSucursal tinyint = NULL,
	@IDUnidadNegocio tinyint= NULL,
	@IDCentroCosto tinyint = NULL,
	@Operacion varchar(10),
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
As

Begin

	--VARIABLES
	Declare @vCodigoPadre varchar(50)
	Declare @vCodigoTemp varchar(50)
	Declare @vImputable bit
	
	--BLOQUES
	
	--INSERTAR
	If @Operacion='INS' Begin
		
		--Si el codigo ya existe
		If Exists(Select * From CuentaContable Where Codigo=@Codigo And IDPlanCuenta=@IDPlanCuenta) Begin
			set @Mensaje = 'El codigo ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si la descripcion ya existe <- SI SE PERMITE!!!!
		--If Exists(Select * From CuentaContable Where Descripcion=@Descripcion And IDPlanCuenta=@IDPlanCuenta) Begin
		--	set @Mensaje = 'La descripcion ya existe!'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		--Si el Alias ya existe
		If Exists(Select * From CuentaContable Where Alias=@Alias And Alias!='' And IDPlanCuenta=@IDPlanCuenta) Begin
			set @Mensaje = 'El alias ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si el codigo no corresponde al padre
		--If @Categoria>1 And @IDPadre Is Not Null Begin
						
		--	Set @vCodigoPadre = (Select Codigo From CuentaContable Where ID=@IDPadre)
		--	Set @vCodigoTemp = (Select SUBSTRING(@Codigo, 1, Len(@vCodigoPadre)))
			
		--	If @vCodigoPadre != @vCodigoTemp Begin
		--		set @Mensaje = 'El codigo no es correcto!'
		--		set @Procesado = 'False'
		--		return @@rowcount
		--	End				
			
		--End
		
		--Si el padre ya esta asociado a un asiento
		If Exists(Select * From DetalleAsiento Where IDCuentaContable=@IDPadre) Begin
			set @Mensaje = 'El totalizador ya tiene asientos asociados! No se puede insertar.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Insertar
		Declare @vID smallint
		Set @vID = (Select ISNULL(MAX(ID)+1, 1) From CuentaContable)
				
		Insert Into CuentaContable(ID, IDPlanCuenta, Codigo, Descripcion, Alias, Categoria, Imputable, IDPadre, Estado, Sucursal, IDSucursal, IDUnidadNegocio, IDCentroCosto)
		Values(@vID, @IDPlanCuenta, @Codigo, @Descripcion, @Alias, @Categoria, 'True', @IDPadre, @Estado, @Sucursal, @IDSucursal, @IDUnidadNegocio, @IDCentroCosto)
		
		--Poner al padre automaticamente como NO Imputable
		Update CuentaContable Set Imputable = 'False'
		Where ID=@IDPadre
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
				
	End
	
	--ACTUALIZAR
	If @Operacion='UPD' Begin
	
		--Si el registro no existe
		If Not Exists(Select * From CuentaContable Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si el codigo ya existe
		If Exists(Select * From CuentaContable Where Codigo=@Codigo And ID!=@ID And IDPlanCuenta=@IDPlanCuenta) Begin
			set @Mensaje = 'El codigo ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si la descripcion ya existe <- SI SE PERMITE!!!!
		--If Exists(Select * From CuentaContable Where Descripcion=@Descripcion And ID!=@ID And IDPlanCuenta=@IDPlanCuenta) Begin
		--	set @Mensaje = 'La descripcion ya existe!'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		--Si el Alias ya existe
		If Exists(Select * From CuentaContable Where Alias=@Alias And Alias!='' And ID!=@ID And IDPlanCuenta=@IDPlanCuenta) Begin
			set @Mensaje = 'El alias ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si el codigo no corresponde al padre
		--If @Categoria>1 And @IDPadre Is Not Null Begin
			
		--	Set @vCodigoPadre = (Select Codigo From CuentaContable Where ID=@IDPadre)
		--	Set @vCodigoTemp = (Select SUBSTRING(@Codigo, 1, Len(@vCodigoPadre)))
			
		--	If @vCodigoPadre != @vCodigoTemp Begin
		--		set @Mensaje = 'El codigo no es correcto!'
		--		set @Procesado = 'False'
		--		return @@rowcount
		--	End				
			
		--End
		
		Update CuentaContable Set	Codigo=@Codigo, 
									Descripcion=@Descripcion, 
									Alias=@Alias,
									Estado=@Estado,
									IDPadre=@IDPadre,
									Sucursal=@Sucursal,
									IDSucursal=@IDSucursal,
									Categoria=@Categoria,
									IDUnidadNegocio=@IDUnidadNegocio,
									IDCentroCosto=@IDCentroCosto
		Where ID=@ID
													
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	--ELIMINAR
	If @Operacion='DEL' Begin
	
		--Si el registro no existe
		If Not Exists(Select * From CuentaContable Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si es que no esta asociado a ningun asiento
		If Exists(Select * From DetalleAsiento Where IDCuentaContable=@ID) Begin
			set @Mensaje = 'El registro tiene asientos asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si es que no esta asociado a ningun cliente
		If Exists(Select * From Cliente Where IDCuentaContable=@ID) Begin
			set @Mensaje = 'El registro tiene clientes asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si es que no esta asociado a ningun proveedor
		If Exists(Select * From Proveedor Where IDCuentaContableCompra=@ID Or IDCuentaContableVenta=@ID) Begin
			set @Mensaje = 'El registro tiene proveedores asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--*** Si es padre, no se puede eliminar hasta que sus hijos sean eliminados *** Esto puede cambiar, pero no se recomienda.
		If Exists(Select * From CuentaContable Where IDPadre=@ID) Begin
			set @Mensaje = 'No se puede eliminar una cuenta cuando este tenga cuentas dependientes! Elimine primeramente las cuentas hijas.'
			set @Procesado = 'False'
			return @@rowcount
		End 
		
		--Si es que es padre y ninguno de sus hijos esta asociaso
		If Exists(Select * From CuentaContable Where IDPadre=@ID) Begin
			If Exists(Select * From CuentaContable CC Join DetalleAsiento DA On CC.ID=DA.IDCuentaContable Where IDPadre=@ID) Begin
				set @Mensaje = 'No se puede eliminar porque uno o mas de sus hijos ya tienen asientos asociados.'
				set @Procesado = 'False'
				return @@rowcount
			End
		End 
		
		--Si es el ultimo hijo, poner al padre como IMPUTABLE
		If (Select COUNT(*) From CuentaContable Where IDPadre=(Select IDPadre From CuentaContable Where ID=@ID)) = 1 Begin
			Update CuentaContable Set Imputable = 'True'
			Where ID = (Select IDPadre From CuentaContable Where ID=@ID)
		End
				
		--Eliminamos la cuenta
		Delete From CuentaContable Where ID=@ID
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
End



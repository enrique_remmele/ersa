﻿CREATE Procedure [dbo].[SpBanco]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Referencia varchar(5),
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Banco Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDBanco tinyint
		set @vIDBanco = (Select IsNull((Max(ID)+1), 1) From Banco)

		--Insertamos
		Insert Into Banco(ID, Descripcion, Referencia , Estado)
		Values(@vIDBanco, @Descripcion, @Referencia ,@Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='BANCO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
				 		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From Banco Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Banco Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Banco Set Descripcion=@Descripcion,
						Referencia =@referencia,
						 Estado =@Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='BANCO', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Banco Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con ChequeCliente
		if exists(Select * From ChequeCliente Where IDBanco=@ID) begin
			set @Mensaje = 'El registro tiene asociaciones con Cheque de Clientes! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con ClienteCuentaBancaria
		if exists(Select * From ClienteCuentaBancaria Where IDBanco=@ID) begin
			set @Mensaje = 'El registro esta asociado! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Banco 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='BANCO', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


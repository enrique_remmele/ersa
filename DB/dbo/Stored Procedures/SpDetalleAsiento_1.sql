﻿CREATE Procedure [dbo].[SpDetalleAsiento]

	--Entrada
	@Operacion varchar(50),
	@IDTransaccion numeric(18,0) = NULL,
	@IDCuentaContable smallint = NULL,
	@ID smallint = NULL,
	@Debito	money	= NULL,
	@Credito	money	= NULL,
	@Importe	money	= NULL,
	@Observacion	varchar(200)	= NULL,
	@IDSucursal tinyint = NULL,
	@TipoComprobante varchar(50) = NULL,
	@NroComprobante varchar(50) = NULL,
	@IDUnidadNegocio tinyint = NULL,
	@IDCentroCosto tinyint = NULL,

	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
			
As

Begin

	--BLOQUES
	declare @vID smallint
	declare @vCuentaContable varchar(50)

	--VERIFICAR QUE TENGA UNIDAD DE NEGOCIO (Asientos Automaticos y cuentas fijas)
	If @IDUnidadNegocio = '' Begin
		Set @IDUnidadNegocio = (Select IDUnidadNegocio From CuentaContable Where ID = @IDCuentaContable)
	End

	--VERIFICAR QUE TENGA CENTRO DE COSTO (Asientos Automaticos y cuentas fijas)
	If @IDCentroCosto = '' Begin
		Set @IDCentroCosto = (Select IDCentroCosto From CuentaContable Where ID = @IDCuentaContable)
	End

	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Transaccion
		If Not Exists(Select * From Asiento Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no entontro el registro asociado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		Set @vID = (Select IsNull((Select Max(ID) + 1 From DetalleAsiento Where IDTransaccion=@IDTransaccion),0))
		Set @vCuentaContable = (Select Top(1) Codigo From CuentaContable Where ID=@IDCuentaContable)
		
		If @TipoComprobante Is Null Or @TipoComprobante = ''  Begin
			Set @TipoComprobante = (Select Top(1) TipoComprobante From VAsiento Where IDTransaccion=@IDTransaccion)
		End
		
		If @NroComprobante Is Null Or @NroComprobante = '' Begin
			Set @NroComprobante = (Select Top(1) NroComprobante From VAsiento Where IDTransaccion=@IDTransaccion)
		End
		
		If @IDSucursal Is Null Or @IDSucursal = 0 Begin
			Set @IDSucursal = (Select Top(1) IDSucursal From VAsiento Where IDTransaccion=@IDTransaccion)
		End
		
		If @IDCentroCosto is null Begin
			Set @IDCentroCosto = (Select Top(1) IDCentroCosto From VAsiento Where IDTransaccion=@IDTransaccion)
		End

		Insert Into DetalleAsiento(IDTransaccion, IDCuentaContable, CuentaContable, ID, Credito, Debito, Importe, Observacion, IDSucursal, TipoComprobante, NroComprobante, IDCentroCosto, IDUnidadNegocio)
		Values(@IDTransaccion, @IDCuentaContable, @vCuentaContable, @vID, @Credito, @Debito, @Importe, @Observacion, @IDSucursal, @TipoComprobante, @NroComprobante, @IDCentroCosto, @IDUnidadNegocio)
											
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR, Igual que insertar
	If @Operacion = 'UPD' Begin
	
		--Validar
		--Transaccion
		If Not Exists(Select * From Asiento Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no entontro el registro asociado!'
			set @Procesado = 'False'
			return @@rowcount
		End
				
		Set @vID = (Select IsNull((Select Max(ID) + 1 From DetalleAsiento Where IDTransaccion=@IDTransaccion),0))
		
		If @TipoComprobante Is Null Or @TipoComprobante = ''  Begin
			Set @TipoComprobante = (Select Top(1) TipoComprobante From VAsiento Where IDTransaccion=@IDTransaccion)
		End
		
		If @NroComprobante Is Null Or @NroComprobante = '' Begin
			Set @NroComprobante = (Select Top(1) NroComprobante From VAsiento Where IDTransaccion=@IDTransaccion)
		End
		
		If @IDSucursal Is Null Or @IDSucursal = 0 Begin
			Set @IDSucursal = (Select Top(1) IDSucursal From VAsiento Where IDTransaccion=@IDTransaccion)
		End
		
		--If @IDCentroCosto is null Begin
		--	Set @IDCentroCosto = (Select Top(1) IDCentroCosto From VAsiento Where IDTransaccion=@IDTransaccion)
		--End

		Insert Into DetalleAsiento(IDTransaccion, IDCuentaContable, CuentaContable, ID, Credito, Debito, Importe, Observacion, IDSucursal, TipoComprobante, NroComprobante, IDCentroCosto, IDUnidadNegocio)
		Values(@IDTransaccion, @IDCuentaContable, @vCuentaContable, @vID, @Credito, @Debito, @Importe, @Observacion, @IDSucursal, @TipoComprobante, @NroComprobante, @IDCentroCosto, @IDUnidadNegocio)
										
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
				
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	return @@rowcount
		
End





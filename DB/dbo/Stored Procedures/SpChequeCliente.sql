﻿CREATE Procedure [dbo].[SpChequeCliente]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalDocumento tinyint = NULL,
	@Numero int = NULL,
	@IDCliente int = NULL,
	@IDBanco tinyint = NULL,
	@CuentaBancaria varchar(50) = NULL,
	@Fecha date = NULL,
	@NroCheque varchar(50) = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money = NULL,
	@Importe money = NULL,
	@ImporteMoneda money = NULL,
	@Diferido bit = NULL,
	@FechaVencimiento date = NULL,
	@ChequeTercero bit = NULL,
	@Librador varchar(50) = NULL,
	@Operacion varchar(50),
	@Titular varchar(50)=NULL,
	@FechaCobranza date=NULL,
				
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
		Declare @vvIDAuditoria int
	    Declare @vvIDSucursal tinyint
		Declare @vvNumero int
		Declare @vvIDCliente int
		Declare @vvIDBanco tinyint
		Declare @vvIDCuentaBancaria int
		Declare @vvFecha date
		Declare @vvNroCheque varchar (50)
		Declare @vvIDMoneda tinyint
		Declare @vvCotizacion money
		Declare @vvImporteMoneda money
		Declare @vvImporte money
		Declare @vvDiferido bit
		Declare @vvFechaVencimiento date
		Declare @vvChequeTercero bit
		Declare @vvLibrador varchar (50)
		Declare @vvSaldo money
		Declare @vvCancelado bit
		Declare @vvCartera bit
		Declare @vvDepositado bit
		Declare @vvRechazado bit
		Declare @vvConciliado bit
		Declare @vvIDMotivoRechazo tinyint
		Declare @vvSaldoACuenta money
		Declare @vvTitular varchar (150)
		Declare @vvFechaCobranza date
		Declare @vvDescontado bit
		Declare @vvIDUsuario int
		Declare @vvAccion varchar(3)

		--INICIO SM 14072021 - Auditoria Informática
		Declare @vObservacion varchar(100)='' 
		Declare @vvID int 

	--Validar Feha Operacion
	--IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
	--	If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
	--		Set @Mensaje  = 'Fecha fuera de rango permitido!!'
	--		Set @Procesado = 'False'
	--		set @IDTransaccionSalida  = 0
	--		Return @@rowcount
	--	End
	--End 

	--IF @Operacion = 'DEL' Begin
	--	set @Fecha = (Select Fecha from ChequeCliente where IDTransaccion = @IDTransaccion)
	--	If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
	--		Set @Mensaje  = 'Fecha fuera de rango permitido!!'
	--		Set @Procesado = 'False'
	--		set @IDTransaccionSalida  = 0
	--		Return @@rowcount
	--	End
	--End 

	--BLOQUES
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		Declare @vNumero int
		Set @vNumero = ISNULL((Select MAX(Numero)+1 From ChequeCliente Where IDSucursal=@IDSucursalDocumento),1)
		
		--Cuenta Bancaria
		If @CuentaBancaria = '' Begin
			set @Mensaje = 'Falta la cuenta bancaria'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
			
		--La Fecha de Entrada no puede ser menor a la del comprobante
		if @Diferido = 'True' Begin
			If DATEDIFF(dd, @Fecha, @FechaVencimiento) < 0 Begin
				set @Mensaje = 'La fecha de vencimiento no puede ser menor a la del documento!'
				set @Procesado = 'False'
				return @@rowcount
			End
		End
		
		--Cheque Duplicado
		If Exists(Select * From ChequeCliente Where IDCliente=@IDCliente And NroCheque=@NroCheque And IDBanco=@IDBanco) Begin
			set @Mensaje = 'Ya existe el cheque para este cliente'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
				
		--Insertar la transaccion			
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccion OUTPUT
		
		--------Insertar
		Declare @vSaldo money
		Declare @vCancelado bit
		Declare @vIDCuentaBancaria int
		
		Set @vSaldo = @ImporteMoneda
		Set @vCancelado = 'False'
				
		--Cuenta Bancaria
		Begin
		
			If Exists(Select ID From ClienteCuentaBancaria Where IDCliente=@IDCliente And IDBanco=@IDBanco And CuentaBancaria=@CuentaBancaria) Begin
				Set @vIDCuentaBancaria = (Select ID From ClienteCuentaBancaria Where IDCliente=@IDCliente And IDBanco=@IDBanco And CuentaBancaria=@CuentaBancaria)
			End Else Begin
				Set @vIDCuentaBancaria = (Select ISNULL(Max(ID) + 1, 1) From ClienteCuentaBancaria)
				Insert Into ClienteCuentaBancaria(ID, IDCliente, IDBanco, CuentaBancaria, IDMoneda)
				Values(@vIDCuentaBancaria, @IDCliente, @IDBanco, @CuentaBancaria, @IDMoneda)
			End
			
		End
		
		--INICIO SM 14072021 - Auditoria Informática
		set @vObservacion = 'Inserta un cheque de Cliente'

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'INSERTAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'CHEQUE CLIENTE' )
		
		set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion)
		
		Insert Into aChequeCliente(IdAuditoria,IDTransaccion, IDSucursal, Numero, IDCliente, IDBanco, IDCuentaBancaria, Fecha, NroCheque, IDMoneda, Cotizacion, Importe, ImporteMoneda, Diferido, FechaVencimiento, ChequeTercero, Librador, Saldo, SaldoACuenta, Cancelado, Cartera, Depositado, Rechazado,Titular,FechaCobranza, Descontado,IDUsuario,Accion)
		values(@vvID,@IDTransaccion, @IDSucursalDocumento, @vNumero, @IDCliente, @IDBanco, @vIDCuentaBancaria, @Fecha, @NroCheque, @IDMoneda, @Cotizacion, @Importe, @ImporteMoneda, @Diferido, @FechaVencimiento, @ChequeTercero, @Librador, @vSaldo, @Importe, @vCancelado, 'True', 'False', 'False',@Titular,@FechaCobranza, 'False',@IDUsuario,'INS')
	
		
		--FIN SM 14072021 - Auditoria Informática

		----Insertar el Registro de Cheque
		Insert Into ChequeCliente(IDTransaccion, IDSucursal, Numero, IDCliente, IDBanco, IDCuentaBancaria, Fecha, NroCheque, IDMoneda, Cotizacion, Importe, ImporteMoneda, Diferido, FechaVencimiento, ChequeTercero, Librador, Saldo, SaldoACuenta, Cancelado, Cartera, Depositado, Rechazado,Titular,FechaCobranza, Descontado)
		values(@IDTransaccion, @IDSucursalDocumento, @vNumero, @IDCliente, @IDBanco, @vIDCuentaBancaria, @Fecha, @NroCheque, @IDMoneda, @Cotizacion, @Importe, @ImporteMoneda, @Diferido, @FechaVencimiento, @ChequeTercero, @Librador, @vSaldo, @Importe, @vCancelado, 'True', 'False', 'False',@Titular,@FechaCobranza, 'False')
															
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		set @IDTransaccionSalida  = '0'
		return @@rowcount
			
	End
	
	--ELIMINAR
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		--Se comenta esta seccion porque hay cheque que existen en ChequeCliente pero no en Transaccion 
		--If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
		--	set @Mensaje = 'El sistema no encontro el registro solicitado.'
		--	set @Procesado = 'False'
		--	set @IDTransaccionSalida  = 0
		--	return @@rowcount
		--End
		
		--Cobranza Credito
		If Not Exists(Select * From ChequeCliente Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que el documento no tenga otros registros asociados
		--Forma de Pago
		If Exists(Select * From FormaPago FP Where FP.IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El registro ya esta usado en otros documentos! No se puede eliminar.'
			Set @Procesado = 'False'
			return @@rowcount			
		End
		
		--Forma de Pago  - Cobranza Credito
		If Exists(Select * From FormaPago FP Join CobranzaCredito CC On FP.IDTransaccion=CC.IDTransaccion Where FP.IDTransaccionCheque =@IDTransaccion) Begin
			Set @Mensaje = 'El registro ya esta usado en otros documentos! No se puede eliminar.'
			Set @Procesado = 'False'
			return @@rowcount			
		End
		
		--Forma de Pago  - Cobranza Contado
		If Exists(Select * From FormaPago FP Join CobranzaContado CC On FP.IDTransaccion=CC.IDTransaccion Where FP.IDTransaccionCheque =@IDTransaccion) Begin
			Set @Mensaje = 'El registro ya esta usado en otros documentos! No se puede eliminar.'
			Set @Procesado = 'False'
			return @@rowcount			
		End
		
		--Detalle Deposito Bancario
		If Exists(Select * From DetalleDepositoBancario D Where D.IDTransaccionCheque =@IDTransaccion) Begin
			Set @Mensaje = 'El registro ya esta usado en otros documentos! No se puede eliminar.'
			Set @Procesado = 'False'
			return @@rowcount			
		End
								
		--Eliminamos
		Delete from ChequeClienteImportar where IDTransaccion = @IDTransaccion

		--Auditoria de documentos
		declare @IDTipoComprobante as int = 0
		set @IDCliente = (select IDCliente from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from ChequeCliente where IDTransaccion = @IDTransaccion)
		declare @Comprobante as varchar(16) = (select NroCheque from ChequeCliente where IDTransaccion = @IDTransaccion)
		declare @VTipoComprobante varchar(16) = 'CHQCLI'
		set @Importe = (select Importe from ChequeCliente where IDTransaccion = @IDTransaccion)
		declare @Condicion varchar(16) = (select (Case when Diferido = 'True' then 'DIFERIDO' else 'AL DIA' end) from ChequeCliente where IDTransaccion = @IDTransaccion)

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,@IDCliente,0,@Fecha,@Comprobante,@Importe,@Condicion, getdate(),@IDUsuario,'ELI',@VTipoComprobante, @IDSucursal)

		--INICIO SM 14072021 - Auditoria Informática
		set @vObservacion = 'Elimina un cheque de Cliente'
		set @vvNumero = (select Numero from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvIDBanco = (select IDBanco from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvIDCuentaBancaria = (select IDCuentaBancaria from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvNroCheque = (select NroCheque from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvIDMoneda = (select IDMoneda from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvCotizacion = (select Cotizacion from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvImporteMoneda = (select ImporteMoneda from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvDiferido = (select Diferido from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvFechaVencimiento = (select FechaVencimiento from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvChequeTercero = (select ChequeTercero from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvLibrador = (select Librador from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvSaldo = (select Saldo from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvCancelado = (select Cancelado from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvCartera = (select Cartera from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvDepositado = (select Depositado from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvRechazado = (select Rechazado from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvConciliado = (select Conciliado from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvIDMotivoRechazo = (select IDMotivoRechazo from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvSaldoACuenta = (select SaldoACuenta from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvTitular = (select Titular from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvFechaCobranza = (select FechaCobranza from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvDescontado = (select Descontado from ChequeCliente where IDTransaccion = @IDTransaccion)
			
		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'CHEQUE CLIENTE' )
		
		set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion)
		
		Insert Into aChequeCliente(IdAuditoria,IDTransaccion, IDSucursal, Numero, IDCliente, IDBanco, IDCuentaBancaria, Fecha, NroCheque, IDMoneda, Cotizacion, Importe, ImporteMoneda, Diferido, FechaVencimiento, ChequeTercero, Librador, Saldo, SaldoACuenta, Cancelado, Cartera, Depositado, Rechazado,Titular,FechaCobranza, Descontado,IDUsuario,Accion)
		values(@vvID,@IDTransaccion, @IDSucursal, @vvNumero, @IDCliente, @vvIDBanco, @vvIDCuentaBancaria, @Fecha, @vvNroCheque, @vvIDMoneda, @vvCotizacion, @Importe, @vvImporteMoneda, @vvDiferido, @vvFechaVencimiento, @vvChequeTercero, @vvLibrador, @vvSaldo, @Importe, @vvCancelado, @vvCartera, @vvDepositado, @vvRechazado,@vvTitular,@vvFechaCobranza, @vvDescontado ,@IDUsuario,'ELI')
	
		--FIN SM 14072021 - Auditoria Informática

		--Eliminamos el registro
		Delete ChequeCliente Where IDTransaccion = @IDTransaccion
				
		--Eliminamos Forma de Pago - Basura
		Delete FormaPago Where IDTransaccionCheque = @IDTransaccion
				
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
			
	End
		
	--ACTUALIZAR
	If @Operacion='UPD' Begin
	
		--INICIO SM - 17072021 - Implementacion de Auditoria
		Declare @vIDTerminal int = (Select IDTerminal from Transaccion where ID = @IDTransaccion)
		Declare @vIDUsuario int = (Select IDUsuario from Transaccion where ID = @IDTransaccion)
		Declare @vIDOperacion int = (Select IDOperacion from Transaccion where ID = @IDTransaccion)
		--FIN SM - 17072021 - Implementacion de Auditoria
		set @vvNumero = (select Numero from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvIDMoneda = (select IDMoneda from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvCotizacion = (select Cotizacion from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvImporteMoneda = (select ImporteMoneda from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvSaldo = (select Saldo from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvCancelado = (select Cancelado from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvCartera = (select Cartera from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvDepositado = (select Depositado from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvRechazado = (select Rechazado from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvConciliado = (select Conciliado from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvIDMotivoRechazo = (select IDMotivoRechazo from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvSaldoACuenta = (select SaldoACuenta from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vvDescontado = (select Descontado from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @vObservacion = 'Modificacion de Cheque Cliente'

		--Validar
		--Numero de Cheque
		If Exists(Select * From ChequeCliente Where IDCliente=@IDCliente And IDBanco=@IDBanco And NroCheque=@NroCheque And IDTransaccion!=@IDTransaccion And IDSucursal=@IDSucursal) Begin
			set @Mensaje = 'El sistema detecto que el numero de cheque ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
	
		--Cuenta Bancaria
		Begin
		
			If Exists(Select ID From ClienteCuentaBancaria Where IDCliente=@IDCliente And IDBanco=@IDBanco And CuentaBancaria=@CuentaBancaria) Begin
				Set @vIDCuentaBancaria = (Select ID From ClienteCuentaBancaria Where IDCliente=@IDCliente And IDBanco=@IDBanco And CuentaBancaria=@CuentaBancaria)
			End Else Begin
				Set @vIDCuentaBancaria = (Select ISNULL(Max(ID) + 1, 1) From ClienteCuentaBancaria)
				Insert Into ClienteCuentaBancaria(ID, IDCliente, IDBanco, CuentaBancaria, IDMoneda)
				Values(@vIDCuentaBancaria, @IDCliente, @IDBanco, @CuentaBancaria, @IDMoneda)
			End
			
		End
		
		Declare @vEstado varchar(50) 
		Declare @vTotalCheque money

		Select Top(1)	@vEstado=Estado,
						@vTotalCheque=ImporteMoneda,
						@vSaldo=Saldo
		From VChequeCliente 
		Where IDTransaccion=@IDTransaccion

		--Auditoria
		Declare @vTabla varchar(50)
		Set @vTabla = (Select Descripcion From Operacion Where ID=@IDOperacion)
	
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=@vTabla, @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @IDTransaccion=@IDTransaccion
	
		--Si ya esta cobrado, actualizar ciertos campos
		If @vSaldo <> @vTotalCheque Begin
		
		--Auditoria de documentos
		set @IDTipoComprobante = 0
		set @IDCliente = (select IDCliente from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select NroCheque from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @VTipoComprobante = 'CHQCLI'
		set @Importe = (select Importe from ChequeCliente where IDTransaccion = @IDTransaccion)
		set @Condicion = (select (Case when Diferido = 'True' then 'DIFERIDO' else 'AL DIA' end) from ChequeCliente where IDTransaccion = @IDTransaccion)
		
		
		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,@IDCliente,0,@Fecha,@Comprobante,@Importe,@Condicion, getdate(),@IDUsuario,'ANU',@VTipoComprobante, @IDSucursal)
		
		 --INICIO SM 15072021 - Auditoria Informática		
    	 Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		 Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@IDTransaccion,'CHEQUE CLIENTE' )
		 
		 set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion)

		 Insert Into aChequeCliente(IdAuditoria,IDTransaccion, IDSucursal, Numero, IDCliente, IDBanco, IDCuentaBancaria, Fecha, NroCheque, IDMoneda, Cotizacion, Importe, ImporteMoneda, Diferido, FechaVencimiento, ChequeTercero, Librador, Saldo, SaldoACuenta, Cancelado, Cartera, Depositado, Rechazado,Titular,FechaCobranza, Descontado,IDUsuario,Accion)
		 values(@vvID,@IDTransaccion, @IDSucursal, @vvNumero, @IDCliente, @IDBanco, @vIDCuentaBancaria, @Fecha, @NroCheque, @vvIDMoneda, @vvCotizacion, @Importe, @vvImporteMoneda, @vvDiferido, @vvFechaVencimiento, @vvChequeTercero, @vvLibrador, @vvSaldo, @Importe, @vvCancelado, @vvCartera, @vvDepositado, @vvRechazado,@Titular,@FechaCobranza, @vvDescontado ,@IDUsuario,'UPD')

		 --FIN SM 15072021 - Auditoria Informática
			   	
			Update ChequeCliente Set IDBanco=@IDBanco,
										IDCuentaBancaria=@vIDCuentaBancaria, 
										Fecha=@Fecha, 
										NroCheque=@NroCheque, 
										Diferido=@Diferido, 
										FechaVencimiento=@FechaVencimiento, 
										ChequeTercero=@ChequeTercero, 
										Librador=@Librador,
										Titular=@Titular,
										FechaCobranza=@FechaCobranza
		   Where IDTransaccion=@IDTransaccion 
		
			set @Mensaje = 'El cheque ya esta usado en alguna cobranza! Solo se pueden modificar ciertos campos.'
			set @Procesado = 'False'
		
			return @@rowcount

		End 

		--Si ya esta depositado, actualizar ciertos campos
		If @vEstado='DEPOSITADO' Or @vEstado='RECHAZADO' Begin
			
		--INICIO SM 15072021 - Auditoria Informática		
    	 Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		 Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@IDTransaccion,'CHEQUE CLIENTE' )
		 
		 set @vvID = (select id from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion)

		 Insert Into aChequeCliente(IdAuditoria,IDTransaccion, IDSucursal, Numero, IDCliente, IDBanco, IDCuentaBancaria, Fecha, NroCheque, IDMoneda, Cotizacion, Importe, ImporteMoneda, Diferido, FechaVencimiento, ChequeTercero, Librador, Saldo, SaldoACuenta, Cancelado, Cartera, Depositado, Rechazado,Titular,FechaCobranza, Descontado,IDUsuario,Accion)
		 values(@vvID,@IDTransaccion, @IDSucursal, @vvNumero, @IDCliente, @IDBanco, @vIDCuentaBancaria, @Fecha, @NroCheque, @vvIDMoneda, @vvCotizacion, @Importe, @vvImporteMoneda, @vvDiferido, @vvFechaVencimiento, @vvChequeTercero, @vvLibrador, @vvSaldo, @Importe, @vvCancelado, @vvCartera, @vvDepositado, @vvRechazado,@Titular,@FechaCobranza, @vvDescontado ,@IDUsuario,'UPD')
		 --FIN SM 15072021 - Auditoria Informática

			Update ChequeCliente Set IDBanco=@IDBanco,
										IDCuentaBancaria=@vIDCuentaBancaria, 
										Fecha=@Fecha, 
										NroCheque=@NroCheque, 
										ChequeTercero=@ChequeTercero, 
										Librador=@Librador,
										Titular=@Titular,
										FechaCobranza=@FechaCobranza
		   	
			Where IDTransaccion=@IDTransaccion 
			
			set @Mensaje = 'El cheque ya esta depositado! Solo se pueden modificar ciertos campos.'
			set @Procesado = 'False'
		
			return @@rowcount
		

		End 
		
		--INICIO SM 15072021 - Auditoria Informática		
    	 Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		 Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@IDTransaccion,'CHEQUE CLIENTE' )
		 
		 set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion)

		 Insert Into aChequeCliente(IdAuditoria,IDTransaccion, IDSucursal, Numero, IDCliente, IDBanco, IDCuentaBancaria, Fecha, NroCheque, IDMoneda, Cotizacion, Importe, ImporteMoneda, Diferido, FechaVencimiento, ChequeTercero, Librador, Saldo, SaldoACuenta, Cancelado, Cartera, Depositado, Rechazado,Titular,FechaCobranza, Descontado,IDUsuario,Accion)
		 values(@vvID,@IDTransaccion, @IDSucursal, @vvNumero, @IDCliente, @IDBanco, @vIDCuentaBancaria, @Fecha, @NroCheque, @vvIDMoneda, @vvCotizacion, @Importe, @vvImporteMoneda, @vvDiferido, @vvFechaVencimiento, @vvChequeTercero, @vvLibrador, @vvSaldo, @Importe, @vvCancelado, @vvCartera, @vvDepositado, @vvRechazado,@Titular,@FechaCobranza, @vvDescontado ,@IDUsuario,'UPD')

		 --FIN SM 15072021 - Auditoria Informática

		--Actualizamos Cheque Cliente
		Update ChequeCliente Set IDSucursal=@IDSucursalDocumento,								
								 IDCliente=@IDCliente, 
								 IDBanco=@IDBanco,
								 IDCuentaBancaria=@vIDCuentaBancaria, 
								 Fecha=@Fecha, 
								 NroCheque=@NroCheque, 
								 IDMoneda=@IDMoneda, 
								 Cotizacion=@Cotizacion, 
								 Importe=@Importe, 
								 ImporteMoneda=@ImporteMoneda,
								 Diferido=@Diferido, 
								 FechaVencimiento=@FechaVencimiento, 
								 ChequeTercero=@ChequeTercero, 
								 Librador=@Librador,
								 Titular=@Titular,
								 FechaCobranza=@FechaCobranza
		   	
		Where IDTransaccion=@IDTransaccion 

		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		
		return @@rowcount
		
	End

End



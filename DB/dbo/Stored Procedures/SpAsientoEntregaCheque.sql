﻿CREATE Procedure [dbo].[SpAsientoEntregaCheque]

	@IDTransaccion int
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint = 0

	--
	Declare @vIDTransaccionOP int
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vFecha date
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money
	Declare @vCuentaContableCheque varchar(50)
	Declare @vObservacion varchar(100)
	Declare @vDiferido bit
	Declare @vAnticipoProveedor bit
	Declare @vEgresoRendir bit
	
	--Variables
	Declare @vCodigoCuentaProveedor varchar(50)
	Declare @vTotal money

	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	
	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money

	Declare @vProveedor as varchar(32)
	Declare @vIDProveedor as int
	
	--Obtener valores
	Begin
	
		Select	@vIDTransaccionOP= EOP.IDTransaccionOP,
				@vIDSucursal=EOP.IDSucursal,
				@vTipoComprobante=OP.TipoComprobante,
				@vIDTipoComprobante=OP.IDTipoComprobante,
				@vComprobante=OP.Comprobante,
				@vFecha=EOP.FechaEntrega,
				@vIDMoneda = OP.IDMoneda,
				@vCotizacion = OP.Cotizacion,
				@vProveedor = OP.Proveedor,
				@vIDProveedor = OP.IDProveedor,
				@vObservacion = OP.Observacion,
				@vAnticipoProveedor = OP.AnticipoProveedor,
				@vEgresoRendir = OP.EgresoRendir,
				@vDiferido = OP.Diferido
		From VEntregaChequeOP EOP 
		join vOrdenPago OP on EOP.IDTransaccionOP = OP.IDTransaccion
		Where EOP.IDTransaccion=@IDTransaccion
	End
					
	--Verificar que el asiento se pueda modificar
	Begin
	
		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	

	End
				
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
		--egresos a rendir no deben tener asientos contable
		if @vEgresoRendir = 'True' begin
			Goto salir
		end
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = Concat(@vTipoComprobante,' ',@vComprobante)
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	
	--Cuentas Fijas de Proveedors
	Begin
	
			Declare cCFCompraProveedor cursor for
			
			Select 
			DA.Codigo,
			'Total'=(select dbo.FImporteProporcional(DA.Credito,((OPE.Importe * OPE.Cotizacion)),(OPE.Total * OPE.Cotizacion)))
			from vdetalleasientoAgrupado DA
			join VOrdenPagoEgreso OPE on DA.IDTransaccion = OPE.IDTransaccion
			Where OPE.IDTransaccionOrdenPago=@vIDTransaccionOP
			and Da.Credito > 1
			and (Da.Descripcion like 'Proveedo%' or Da.Descripcion like 'Anticipo%')
			and OPE.RRHH = 'False'
			And @vAnticipoProveedor = 'False'

			union all

			Select 
			DA.Codigo,
			'Total'=(select dbo.FImporteProporcional(DA.Credito,((OPE.Importe * OPE.Cotizacion)),(OPE.Total * OPE.Cotizacion)))
			from vdetalleasientoAgrupado DA
			join VOrdenPagoEgreso OPE on DA.IDTransaccion = OPE.IDTransaccion
			Where OPE.IDTransaccionOrdenPago=@vIDTransaccionOP
			and Da.Credito > 1
			and Da.Descripcion like 'Proveedo%'
			and OPE.RRHH = 'True'
			And @vAnticipoProveedor = 'False'

			union all


			Select 
			P.CuentaContableAnticipo,
			'Total'= (select dbo.FImporteProporcional(DA.Credito,((OPE.Importe * OPE.Cotizacion)),(OPE.Total * OPE.Cotizacion)))
			from vOrdenPago OP 
			Join vProveedor P on OP.IDProveedor = P.ID
			left outer join VOrdenPagoEgreso OPE on OP.IDTransaccion = OPE.IDTransaccionOrdenPago
			left outer join vdetalleasientoAgrupado DA on OPE.IDTransaccion = DA.IDTransaccion and Da.Credito > 1 and (Da.Descripcion like 'Proveedo%' or Da.Descripcion like 'Anticipo%')
			Where OP.IDTransaccion=@vIDTransaccionOP
			And @vAnticipoProveedor = 'True'
			
			union all
			
			Select 
			P.CuentaContableAnticipo,
			'Total'=(Case when OP.IDMoneda <> 1 then OP.ImporteMoneda * OP.Cotizacion else OP.ImporteMoneda end)
			from vOrdenPago OP 
			Join vProveedor P on OP.IDProveedor = P.ID
			Where OP.IDTransaccion=@vIDTransaccionOP
			And @vAnticipoProveedor = 'True'
			and OP.IDTransaccion not in (select idtransaccionOrdenPago from OrdenPagoEgreso)
			

			Open cCFCompraProveedor   
			fetch next from cCFCompraProveedor into @vCodigoCuentaProveedor, @vTotal
		
			While @@FETCH_STATUS = 0 Begin  
				print @vCodigoCuentaProveedor
				print @vTotal
				
				Set @vCodigo = @vCodigoCuentaProveedor
				if (@vCodigo = '') begin
					Goto Seguir
				end

				Set @vImporteDebe = 0
				Set @vImporteHaber = 0

				--Obtener la cuenta
				Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			
				Set @vImporteDebe = @vTotal
				Set @vImporteHaber = 0
			
				print @vCodigo
				print @vImporteDebe
				If @vImporteDebe>0 Begin				
					If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
						Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
													Debito=Debito+Round(@vImporteDebe,@vRedondeo),
													Importe=Importe+Round(@vImporte,@vRedondeo)
						Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
					End Else Begin
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
					End
				End
	Seguir:
				fetch next from cCFCompraProveedor into @vCodigoCuentaProveedor, @vTotal
			
			End
		
			close cCFCompraProveedor 
			deallocate cCFCompraProveedor

		--end
		--else begin
			
		--	Declare cCFEntregaCheque cursor for
		--	Select Codigo, Debe, Haber from VCFEntregaChequeOP 
		--	Where Tipo = 'PROVEEDOR' 
		--	And IDSucursal=@vIDSucursal 
		--	And IDMoneda=@vIDMoneda
		--	and ChequeDiferido = 'True'
		
		--	Open cCFEntregaCheque   
		--	fetch next from cCFEntregaCheque into @vCodigo, @vDebe, @vHaber
		
		--	While @@FETCH_STATUS = 0 Begin  
			
		--		Set @vImporteDebe = 0
		--		Set @vImporteHaber = 0
			
		--		Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
		--		Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

		--		Set @vImporte = (Select Sum(DA.Credito)	from vdetalleasiento DA 
		--							join OrdenPagoEgreso OPE on DA.IDTransaccion = OPE.IDTransaccionEgreso
		--							Where IDTransaccionOrdenPago=@vIDTransaccionOP)
			
		--		If @vDebe = 1 Begin
		--			Set @vImporteDebe = @vImporte
		--			Set @vImporteHaber = 0
		--		End
			
		--		If @vHaber = 1 Begin
		--			Set @vImporteHaber = @vImporte
		--			Set @vImporteDebe = 0
		--		End				
			
		--		If @vImporteHaber > 0 Or @vImporteDebe>0 Begin
		--			If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
		--				Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
		--											Debito=Debito+Round(@vImporteDebe,@vRedondeo),
		--											Importe=Importe+Round(@vImporte,@vRedondeo)
		--				Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
		--			End Else Begin
		--				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
		--									Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
		--			End
		--		End
			
		--		fetch next from cCFEntregaCheque into @vCodigo, @vDebe, @vHaber
			
		--	End
		
		--	close cCFEntregaCheque 
		--	deallocate cCFEntregaCheque


		--end
	End
					
	--Cuentas Fijas Cheque
	Begin
		
	
		Declare cCFCompraImpuesto cursor for
		Select Codigo, Debe, Haber from VCFEntregaChequeOP 
		Where Tipo = 'CHEQUE' 
		And IDSucursal=@vIDSucursal 
		And IDMoneda=@vIDMoneda
		and ChequeDiferido = @vDiferido
		
		Open cCFCompraImpuesto   
		fetch next from cCFCompraImpuesto into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Set @vImporte = (Select Sum(DA.Credito)	from vdetalleasiento DA 
			--					join VOrdenPagoEgreso OPE on DA.IDTransaccion = OPE.IDTransaccion
			--					Where IDTransaccionOrdenPago=@vIDTransaccionOP)
			if isnull(@vAnticipoProveedor,'False') = 'True' begin
				Set @vImporte = (Select ImporteMoneda * Cotizacion from vOrdenPago
					Where IDTransaccion=@vIDTransaccionOP)
			end
			else begin
				Set @vImporte = (Select Sum(Importe * Cotizacion) from vOrdenPagoEgreso 
					Where IDTransaccionOrdenPago=@vIDTransaccionOP)
			end
		
			
			Set @vImporteHaber = @vImporte
			Set @vImporteDebe = 0
			
			If @vImporteHaber > 0 Begin
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
										Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End
			End
			
			fetch next from cCFCompraImpuesto into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFCompraImpuesto 
		deallocate cCFCompraImpuesto
	End					


	--Actualizamos la cabecera, el total
	Begin
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
		Set @vImporteHaber = ROUND(@vImporteHaber, @vRedondeo)
		Set @vImporteDebe = ROUND(@vImporteDebe, @vRedondeo)
	
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vImporteHaber - @vImporteDebe
		Where IDTransaccion=@IDTransaccion
	End

	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
End

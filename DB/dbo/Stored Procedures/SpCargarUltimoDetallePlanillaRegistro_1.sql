﻿CREATE procedure [dbo].[SpCargarUltimoDetallePlanillaRegistro]

	@IDProveedor int,
	@IDSucursal tinyint 

As

Begin

	--Variables
	Declare @vIDTransacionMayorPlanilla numeric(18,0)
	Declare @vIDTransacionMayorPlanillaNuevo numeric(18,0)
	Declare @vIDActividad numeric(18,0)
	Declare @vIDActividadNuevo numeric(18,0)
	Declare @vIDActividadPrimero numeric(18,0)
	Declare @vIDTransacionPlanillaDetalle numeric(18,0)
	
	Set @vIDActividadPrimero = 0
	Set @vIDTransacionMayorPlanilla  = (Select MAX(IDTransaccion ) From VDetallePlanillaDescuentoTactico  Where IDProveedor=@IDProveedor And IDSucursal=@IDSucursal And Anulado='False')
	Set @vIDTransacionMayorPlanillaNuevo  = (Select MAX(IDTransaccion ) From VPlanillaDescuentoTactico  Where IDProveedor=@IDProveedor And IDSucursal=@IDSucursal And Anulado='False')
	
	Declare db_cursor cursor for
	Select ID, IDTransaccion From VDetallePlanillaDescuentoTactico 
	Where IDTransaccion=@vIDTransacionMayorPlanilla
	Open db_cursor   
	Fetch Next From db_cursor Into @vIDActividad, @vIDTransacionPlanillaDetalle
	While @@FETCH_STATUS = 0 Begin 
		
		Set @vIDActividadNuevo = (Select ISNULL(MAX(ID)+1,1) From Actividad)
	
		If @vIDActividadPrimero = 0 Begin
			Set @vIDActividadPrimero = @vIDActividadNuevo
		End					
		
		print @vIDActividadPrimero
		print @vIDActividadNuevo
		
		--Insertar la Actividad	
		Insert Into Actividad(ID, IDSucursal, IDProveedor, Codigo, Mecanica, ImporteAsignado, Entrada, Salida, DescuentoMaximo, CarteraMaxima, Tactico, Acuerdo, Total, PorcentajeUtilizado, Saldo, Excedente)
		Select 'ID'=@vIDActividadNuevo, IDSucursal, IDProveedor, Codigo, Mecanica, ImporteAsignado, 0, 0, DescuentoMaximo, 0, 0, 0, 0, 0, ImporteAsignado, 0 
		From Actividad Where ID= @vIDActividad
	
		--Cargar los productos de la actividad
		Insert Into DetalleActividad(IDActividad , ID, IDProducto , Cantidad , Total)
		Select  'IDActividad'= @vIDActividadNuevo, ID, IDProducto , Cantidad , Total 
		From DetalleActividad Where IDActividad=@vIDActividad
		
		--Relacionar con la Planilla
		Insert Into DetallePlanillaDescuentoTactico (IDTransaccion, IDActividad )
		Values (@vIDTransacionMayorPlanillaNuevo, @vIDActividadNuevo) 
		
		Fetch Next From db_cursor Into @vIDActividad, @vIDTransacionPlanillaDetalle
											
	End
		
	Close db_cursor   
	Deallocate db_cursor
			
End
	


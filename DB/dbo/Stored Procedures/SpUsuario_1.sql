﻿
CREATE Procedure [dbo].[SpUsuario]

	--Entrada
	@ID int,
	@Nombre varchar(50),
	@Usuario varchar(50),
	@Password varchar(50) = '12345',
	@IDPerfil tinyint,
	@Identificador varchar(3),
	@Estado bit,
	@EsVendedor bit = 'False',
	@IDVendedor int = NULL,
	@EsChofer bit = 'False',
	@IDChofer int = NULL,
	@Email varchar(100) = '',
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la Nombre ya existe
		if exists(Select * From Usuario Where Nombre=@Nombre) begin
			set @Mensaje = 'El Nombre ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		if exists(Select * From Usuario Where Usuario=@Usuario) begin
			set @Mensaje = 'El Usuario ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que el Identi ya existe
		if exists(Select * From Usuario Where Identificador=@Identificador) begin
			set @Mensaje = 'El identificador ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDUsuario int
		set @vIDUsuario = (Select IsNull((Max(ID)+1), 1) From Usuario)

		Set @Password = Convert(varchar(300), HASHBYTES('Sha1', '12345'))
						
		--Insertamos
		Insert Into Usuario(ID, Nombre, Usuario, [Password], Identificador, IDPerfil, Estado, EsVendedor, IDVendedor, Email, EsChofer, IDChofer)
		Values(@vIDUsuario, @Nombre, @Usuario, @Password, @Identificador, @IDPerfil, @Estado, @EsVendedor, @IDVendedor, @Email, @EsChofer, @IDChofer)		
		
		If exists(SELECT * FROM VAccesoPerfil Where TAG = 'Frmpedido' AND IDPerfil = @IDPerfil) begin
		 	INSERT INTO FormaPagoFacturaUsuario(IDFormaPagoFactura,IDUsuario) 
			VALUES (1, @vIDUsuario)
			INSERT INTO FormaPagoFacturaUsuario(IDFormaPagoFactura,IDUsuario) 
			VALUES (2, @vIDUsuario)
			INSERT INTO FormaPagoFacturaUsuario(IDFormaPagoFactura,IDUsuario) 
			VALUES (3, @vIDUsuario)
			INSERT INTO FormaPagoFacturaUsuario(IDFormaPagoFactura,IDUsuario) 
			VALUES (4, @vIDUsuario)
			INSERT INTO FormaPagoFacturaUsuario(IDFormaPagoFactura,IDUsuario) 
			VALUES (5, @vIDUsuario)
			INSERT INTO FormaPagoFacturaUsuario(IDFormaPagoFactura,IDUsuario) 
			VALUES (6, @vIDUsuario)
			INSERT INTO FormaPagoFacturaUsuario(IDFormaPagoFactura,IDUsuario) 
			VALUES (7, @vIDUsuario)
			INSERT INTO FormaPagoFacturaUsuario(IDFormaPagoFactura,IDUsuario) 
			VALUES (8, @vIDUsuario)
			INSERT INTO FormaPagoFacturaUsuario(IDFormaPagoFactura,IDUsuario) 
			VALUES (9, @vIDUsuario)
			
		end

		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From Usuario Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la Nombre ya existe
		if exists(Select * From Usuario Where Nombre=@Nombre And ID!=@ID) begin
			set @Mensaje = 'El Nombre ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		if exists(Select * From Usuario Where Usuario=@Usuario And ID!=@ID) begin
			set @Mensaje = 'El Usuario ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la Nombre ya existe
		if exists(Select * From Usuario Where Identificador=@Identificador And ID!=@ID) begin
			set @Mensaje = 'El identificador ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Usuario Set Nombre=@Nombre,
						 Estado = @Estado,
						 Usuario=@Usuario, 
						 IDPerfil=@IDPerfil,
						 Identificador=@Identificador,
						 EsVendedor=@EsVendedor,
						 IDVendedor=@IDVendedor,
						 Eschofer=@EsChofer,
						 IDChofer=@IDChofer,
						 Email=@Email
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Usuario Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Productos
		if exists(Select * From Transaccion Where IDUsuario=@ID) begin
			set @Mensaje = 'El registro tiene operaciones realizadas! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Usuario 
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End



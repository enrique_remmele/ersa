﻿CREATE Procedure [dbo].[SpRepararEgreso]

	--Entrada
	@IDTransaccion numeric(18,0)
	
As

Begin
	
	--Validar
	Declare @vMensaje varchar(200)
	Declare @vProcesado bit
	
	Set @vMensaje = 'No se proceso'
	Set @vProcesado = 'False'
	
	--Validar
	--Si existe
	If  Not Exists(Select * From VEgreso Where IDTransaccion=@IDTransaccion) Begin
		Set @vMensaje = 'El registro no existe!'
		Set @vProcesado = 'False'
		GoTo Salir
	End

	
	--Variables
	Declare @vSaldo money
	Declare @vCancelado bit
	Declare @vTipo varchar(50)

	Set @vTipo = (Select Operacion From VEgresos where idtransaccion = @IDTransaccion)
	Set @vSaldo =(Select Sum(Credito) - Sum(debito) From VExtractoMovimientoProveedorFactura Where ComprobanteAsociado= @IDTransaccion)
	
	If @vSaldo < 1 and @vSaldo >-10 Begin
		Set @vCancelado = 'True'
		Set @vSaldo = 0
	End Else Begin 
		Set @vCancelado = 'False'	
	End


Actualizar:
	
	if @vTipo = 'GASTO' begin
		Update Gasto 
		Set Saldo=@vSaldo
		,Cancelado=@vCancelado
		Where IDTransaccion=@IDTransaccion
	end 
	
	if @vTipo = 'COMPRA DE MERCADERIA' begin
		Update Compra 
		Set Saldo=@vSaldo
		,Cancelado=@vCancelado
		Where IDTransaccion=@IDTransaccion
	end
	
	if @vTipo = 'FONDO FIJO' begin
		Update Gasto 
		Set Saldo=@vSaldo
		,Cancelado=@vCancelado
		Where IDTransaccion=@IDTransaccion
	end

	Set @vMensaje = 'Registro procesado'
	Set @vProcesado = 'True'
		
Salir:
	Select 'Procesado'=@vProcesado
End


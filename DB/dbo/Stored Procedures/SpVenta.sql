﻿CREATE Procedure [dbo].[SpVenta]
	
	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDPuntoExpedicion int = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante numeric(18, 0) = NULL,
	@Comprobante varchar(50) = NULL,
	@IDCliente int = NULL,
	@IDSucursalCliente tinyint = NULL,
	@Direccion varchar(100) = NULL,
	@Telefono varchar(50) = NULL,
	@IDVendedor tinyint = NULL,	
	@Fecha date = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDDepositoOperacion tinyint = NULL,
	@Credito bit = NULL,
	@FechaVencimiento date = NULL,
	@IDListaPrecio tinyint = NULL,
	@Descuento numeric(2,0) = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money = NULL,
	@Observacion varchar(200) = NULL,
	@NroComprobanteRemision varchar(50) = NULL,
	@Total money = 0,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@TotalDescuento money = NULL,
	
	--Motivo Anulacion
	@IDMotivo integer =NULL,
	@Motivo varchar(50)=NULL,
	@Autoriza varchar(20)=NULL,
	@Nuevo bit=NULL,
	@FechaAnulado date = NULL,
	@IDUsuarioAnulado int=NULL,
	@SolicitudAprobada bit = 0,
	
	
	@EsVentaSucursal bit = 'False',
	@EsMovil bit = 'False',	
	@Operacion varchar(50),
	
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	@IdFormaPagoFactura tinyint = NULL,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

		--Validar Feha Operacion
		IF @Operacion = 'DEL' or @Operacion = 'UPD' Begin
		Declare @IDOperacion int = (select IDOperacion from Transaccion where ID = @IDTransaccion)
		Declare @FechaFactura date= (Select FechaEmision from Venta where IDTransaccion = @IDTransaccion)
			If dbo.FValidarFechaOperacion(@IDSucursal, @FechaFactura, @IDUsuario, @IDOperacion) = 'False' Begin
				Set @Mensaje  = 'Fecha fuera de rango permitido!!'
				Set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				Return @@rowcount
			End
		End 

	--BLOQUES
	Declare @vIDCliente int 
	Declare @vMaxIDMotivo int
	Declare @CancelarAutomatico bit 
	Declare @MantenerPedidoVigente bit = 0
	Declare @vIDTransaccionPedido int
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		If Exists(Select * From Venta Where Comprobante=@Comprobante and Procesado = 1) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Comprobante
		if Exists(Select * From Venta Where IDPuntoExpedicion=@IDPuntoExpedicion And NroComprobante=@NroComprobante) Begin
			set @Mensaje = 'El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Comprobante fuera del Rango
		if @NroComprobante < (Select NumeracionDesde From PuntoExpedicion Where ID=@IDPuntoExpedicion) Or @NroComprobante > (Select NumeracionHasta From PuntoExpedicion Where ID=@IDPuntoExpedicion) Begin
			set @Mensaje = 'El numero de comprobante no esta dentro del rango correcto! Cambie el punto de expedicion o actualicelo.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Credito
		If @Credito='True' Begin
			
			--Fecha
			If DATEDIFF(dd, @Fecha, @FechaVencimiento) < 0 Begin
				set @Mensaje = 'La fecha de de vencimiento no puede ser menor al del comprobante!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
		
			--Verificar que el cliente tenga saldo suficiente.
			If @Total > (Select SaldoCredito From VCliente Where ID=@IDCliente) Begin
				set @Mensaje = 'El cliente no tiene credito suficiente! Para continuar, incremente el credito al cliente o disminuya el total de la factura.'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
			
			
			--Plazo credito
			Declare @vPlazoCredito smallint
			Set @vPlazoCredito = (Select ISNULL((Select PlazoCredito From Cliente Where ID=@IDCliente),0))
			
			If DATEDIFF(dd, @Fecha, @FechaVencimiento) > @vPlazoCredito Begin
				set @Mensaje = 'El plazo de credito es mayor al del que tiene asignado el cliente!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
			
		End
				
		----Insertar la transaccion
		Declare @vIDOperacion tinyint
		Set @vIDOperacion = 1
		
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @vIDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		------Insertar
		Declare @vSaldo money
		Declare @vCancelado bit
		Set @vSaldo = @Total
		Set @vCancelado = 'False'
		
		-- Si forma de pago es Cancelar Automarico
		if (Select CancelarAutomatico From FormaPagoFactura Where ID=@IDFormaPagoFactura)='True' begin
		  Set @vSaldo = 0
		  Set @vCancelado = 'True'
		end
		
		--Insertar el Registro de Venta		
		Insert Into Venta(IDTransaccion, IDPuntoExpedicion, IDTipoComprobante, NroComprobante, Comprobante, IDCliente, IDSucursalCliente, Direccion, Telefono, FechaEmision, IDSucursal, IDDeposito, Credito, FechaVencimiento, IDListaPrecio, Descuento, IDMoneda, Cotizacion, Observacion, NroComprobanteRemision, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, Cobrado, Descontado, Saldo, Cancelado, Despachado, EsVentaSucursal, Anulado, FechaAnulado, IDUsuarioAnulado, IDVendedor)
		Values(@IDTransaccionSalida, @IDPuntoExpedicion, @IDTipoComprobante, @NroComprobante, @Comprobante, @IDCliente, @IDSucursalCliente, @Direccion, @Telefono, @Fecha, @IDSucursalOperacion, @IDDepositoOperacion, @Credito, @FechaVencimiento, @IDListaPrecio, @Descuento, @IDMoneda, @Cotizacion, @Observacion, @NroComprobanteRemision, @Total, @TotalImpuesto, @TotalDiscriminado, @TotalDescuento, 0, 0, @vSaldo, @vCancelado, 'False', @EsVentaSucursal, 'False', NULL, NULL, @IDVendedor)
				
		--Actualizar el Cliente (Saldo Total, Saldo Credito, Ultima Compra)
		EXEC SpAcualizarClienteVenta
			@IDCliente = @IDCliente,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT
			
		--Actualizar Punto de Expedicion
		Update PuntoExpedicion Set UltimoNumero=@NroComprobante
		Where ID=@IDPuntoExpedicion
		
		
								
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	
		--print 'entro a anulacion'
		
		--No existe el motivo
		If not Exists(Select * From MotivoAnulacionVenta where ID = @IDMotivo) Begin
			set @Mensaje = 'Motivo seleccionado invalido.'
			set @Procesado = 'False'
			return @@rowcount
		End

		Set @MantenerPedidoVigente = (select isnull(MantenerPedidoVigente,0) from MotivoAnulacionVenta where id = @IDMotivo)
		
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Venta Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End			
		
		--Verificar que el documento ya este anulado
		If Exists(Select * From Venta Where IDTransaccion=@IDTransaccion And Anulado='True') Begin
			set @Mensaje = 'El Documento ya esta anulado'
			set @Procesado = 'False'
			return @@rowcount
		End
	

		--Verificar que el documento no tenga otros registros asociados
		--Recibos
		If Exists(Select * From VVentaDetalleCobranza Where IDTransaccion=@IDTransaccion And AnuladoCobranza='False') Begin
			set @Mensaje = 'El documento tiene recibos asociados! Elimine los recibos para continuar.'
			set @Procesado = 'False'
			return @@rowcount
		End
	
		--Notas de Credito/Debito
		If Exists(Select * From NotaCreditoVenta A Join NotaCredito on NOtaCredito.IdTransaccion =  A.IdTransaccionNotaCredito Where A.IDTransaccionVentaGenerada=@IDTransaccion and NotaCredito.Anulado = 'False') Begin
			set @Mensaje = 'La Venta tiene Nota de Credito Aplicada! No se puede anular.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Asientos Cerrados
		If Exists(Select * From Asiento A Where A.IDTransaccion=@IDTransaccion And A.Conciliado='True') Begin
			set @Mensaje = 'El asiento de este documento ya esta conciliado! No se puede anular.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		
		--Recibo Virtual
		If Exists(Select * From ReciboVenta A Where A.IDTransaccionVenta=@IDTransaccion And A.Anulado='False') Begin
			set @Mensaje = 'La factura tiene un recibo 020 vigente. Anulelo antes de continuar.'
			set @Procesado = 'False'
			return @@rowcount
		End


		if @MantenerPedidoVigente = 0 begin
			
			set @vIDTransaccionPedido = IsNull((Select IDTransaccionPedido From PedidoVenta Where IDTransaccionVenta = @IDTransaccion),0)
		if @vIDTransaccionPedido > 0 begin
			if exists(select * from AsignacionCamion AC join DetalleAsignacionCamion DAC on AC.IDTransaccion = DAC.IDTransaccion where AC.Anulado = 0 and DAC.IDTransaccionPedido=@vIDTransaccionPedido) begin
				set @Mensaje = 'El Pedido asociado a esta factura, esta asignado a una carga, se debe anular la carga antes de anular el pedido!'
				set @Procesado = 'False'
				--print @Mensaje
				return @@rowcount
			end
			end
		end
		--Asignacion a Camion
		

		-----Validar
		--If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
		--	Set @Mensaje  = 'Fecha fuera de rango permitido!!'
		--	Set @Procesado = 'False'
		--	set @IDTransaccionSalida  = 0
		--	Return @@rowcount
		--End
		if @SolicitudAprobada <> 1 begin
			----No anular Venta con fecha distint a hoy
			If Isnull((select VentaAnularFechaDistintaHoy from Configuraciones where IDSucursal = @IDSucursal),'False') = 'False' Begin
				If (Select CAST(fechaEmision as date) From Venta Where IDTransaccion=@IDTransaccion) <> cast(getdate() as date) Begin
					--No Cambiar el mensaje
					set @Mensaje = 'No se puede anular con fecha distinta a hoy'
					set @Procesado = 'False'
					return @@rowcount
				End
			End
		end
				
		--Remisiones
		
		----Verificar dias de bloqueo
		If (Select Top(1) VentaBloquearAnulacion From Configuraciones where IDSucursal = @IDSucursal) = 'True' Begin
			
			Declare @vDias tinyint
			Set @vDias = (Select Top(1) VentaDiasBloqueo From Configuraciones where IDSucursal = @IDSucursal)

			If (Select DATEDIFF(dd,(Select V.FechaEmision From Venta V Where IDTransaccion=@IDTransaccion), GETDATE())) > @vDias Begin
			 			
				set @Mensaje = 'El sistema no puede anular este registro ya que la configuracion lo restringe por la antigüedad del documento. Cambie la configuracion de bloqueo para anular!'
				set @Procesado = 'False'
				return @@rowcount
				
			End
			
		End
		
		----Pedido de Notas de Credito
		--If Exists(Select * From PedidoNotaCreditoVenta A Join PedidoNotaCredito on PedidoNotaCredito.IdTransaccion =  A.IdTransaccionPedidoNotaCredito Where A.IDTransaccionVentaGenerada =@IDTransaccion and PedidoNotaCredito.Anulado = 'False') Begin
		--	set @Mensaje = 'La Venta tiene Pedido de Nota de Credito Aplicada! No se puede anular.'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--End

		--Anular Pedidos de nc relacionados a la venta
		Declare @vIDTransaccionPedidoNotaCredito as integer
		Declare cCFProvision cursor for
		Select 
		IDTransaccionPedidoNotaCredito
		From PedidoNotaCreditoVenta A 
		Join PedidoNotaCredito on PedidoNotaCredito.IdTransaccion = A.IdTransaccionPedidoNotaCredito 
		Where A.IDTransaccionVentaGenerada = @IDTransaccion 
		and PedidoNotaCredito.Anulado = 'False'
		and isnull(PedidoNotaCredito.ProcesadoNC,0) = 0 
		
		Open cCFProvision   
		fetch next from cCFProvision into @vIDTransaccionPedidoNotaCredito
		
		While @@FETCH_STATUS = 0 Begin  

			Exec SpPedidoNotaCredito
				@IDTransaccion = @vIDTransaccionPedidoNotaCredito
				,@Operacion = 'ANULAR'
				,@IDUsuario = @IDUsuario
				,@IDSucursal = @IDSucursal
				,@IDDeposito = @IDDeposito
				,@IDTerminal = @IDTerminal
				,@IDOperacion = 0
				,@Mensaje = '' 
				,@Procesado = 0 
				,@IDTransaccionSalida = 0 
			
			fetch next from cCFProvision into @vIDTransaccionPedidoNotaCredito
			
		End
		
		close cCFProvision 
		deallocate cCFProvision

		--print 'entra a anular'
		--Actualizamos el Stock
		Exec SpDetalleVenta @IDTransaccion=@IDTransaccion, @Operacion='ANULAR', @Mensaje=@Mensaje output, @Procesado=@Procesado output
		
		--Actualizar los descuentos tacticos
		If @EsMovil = 'False' Begin
			EXEC SpActualizarActividad @Operacion = 'ANULAR', @IDTransaccion = @IDTransaccion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		End
		--print concat('insertar registro de anulacion: ',@IDTransaccion)
		
		---Insertamos el registro de anulacion
		--Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		
		Insert Into DocumentoAnulado(IDTransaccion, Fecha, IDUsuario, IDSucursal, IDTerminal)
			values(@IDTransaccion, GETDATE(), @IDUsuario, @IDSucursal, @IDTerminal)	
			
					
			
				
		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from Venta where IDTransaccion = @IDTransaccion)
		set @IDCliente = (select IDCliente from Venta where IDTransaccion = @IDTransaccion)
		set @Fecha = (select FechaEmision from Venta where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from Venta where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select Comprobante from Venta where IDTransaccion = @IDTransaccion)
		declare @VTipoComprobante varchar(16) = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from Venta where IDTransaccion = @IDTransaccion)
		declare @Condicion varchar(16) = (select (Case when Credito = 'True' then 'CREDITO' else 'CONTADO' end) from Venta where IDTransaccion = @IDTransaccion)

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,@IDCliente,0,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ANU',@VTipoComprobante, @IDSucursal)
		
		--Anulamos el registro
		Update Venta Set Anulado = 'True',
						IDMotivo = @IDMotivo,
						Autoriza = @Autoriza,
						FecAnulado = @FechaAnulado 
		Where IDTransaccion = @IDTransaccion
		
		--Actualizar el Cliente (Saldo Total, Saldo Credito, Ultima Compra)
		Set @vIDCliente = (Select IDCliente From Venta Where IDTransaccion=@IDTransaccion)
			
		--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
		
		--Liberar Pedido
		Delete From PedidoVenta 
		Where IDTransaccionVenta = @IDTransaccion
			
		--Actualiza Pedido
		
		If @vIDTransaccionPedido > 0 Begin
			Update Pedido Set Facturado=0, AnuladoAprobado=1 Where IDTransaccion = @vIDTransaccionPedido
			
			if @MantenerPedidoVigente = 0 begin

				EXEC SpPedido
				   @IDTransaccion= @vIDTransaccionPedido
				  ,@AnularPedidoNotaCredito = 1
				  ,@Operacion = 'ANULAR'
				  ,@IDOperacion = 0
				  ,@IDUsuario = @IDUsuario
				  ,@IDSucursal = @IDSucursal
				  ,@IDDeposito = @IDDeposito
				  ,@IDTerminal = @IDTerminal
				  ,@Mensaje = '' 
				  ,@Procesado = 0 
				
			end

		End
		
		--Declare @vIDTransaccionPedidoNotaCredito int
		--set @vIDTransaccionPedidoNotaCredito = (Select top(1) IsNull(IDTransaccionPedidoNotaCredito,0) From PedidoNotaCreditoVenta Where IDTransaccionVentaGenerada = @IDTransaccion)
		
		--print concat('idtransaccionpedidonc',@vIDTransaccionPedidoNotaCredito)

		--If @vIDTransaccionPedidoNotaCredito > 0 Begin
		--	print 'entro'
		--	Update DetalleNotaCreditoDiferenciaPrecio 
		--			set IDTransaccionVenta =  null
		--	Where IDTransaccionPedidoNotaCredito = @vIDTransaccionPedidoNotaCredito

		--	Update DetalleNotaCreditoAcuerdo 
		--			set IDTransaccionVenta = null
		--	Where IDTransaccionPedidoNotaCredito = @vIDTransaccionPedidoNotaCredito
			
		--	Delete from PedidoNotaCreditoVenta where IDTransaccionVentaGenerada = @IDTransaccion
			
		--End

		--Actualizar el Cliente (Saldo Total, Saldo Credito, Ultima Compra)
		If @EsMovil = 'False' Begin
			EXEC SpAcualizarClienteVenta
				@IDCliente = @vIDCliente,
				@Mensaje = @Mensaje OUTPUT,
				@Procesado = @Procesado OUTPUT
				
			--Cargar Comisiones
			EXEC SpComision
				@IDTransaccion=@IDTransaccion,
				@Venta='True',
				@Devolucion='False',
				@Operacion= 'Anular' 	
		End	
			
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		Set @IDTransaccionSalida = 0
		--print @Mensaje
		return @@rowcount
			
	End
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Venta Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
       
				
		--Solo se puede eliminar si esta aulado
		If Not Exists(Select * From Venta Where IDTransaccion=@IDTransaccion And Anulado='True') Begin
			set @Mensaje = 'El comprobante tiene que estar anulado para poder eliminar!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si tiene una rendicion asociada y no esta anulada, no se elimina
		If Exists(Select * From DetalleRendicionLoteCheque Where IDTransaccionVenta=@IDTransaccion) Begin
			Declare @vIDTransaccionRendicion as int
			Set @vIDTransaccionRendicion = (Select IDTransaccion From DetalleRendicionLoteCheque Where IDTransaccionVenta=@IDTransaccion)
			
			if (Select Anulado From RendicionLote Where IDTransaccion=@vIDTransaccionRendicion) = 0 begin
				
				Declare @OPRendicion as varchar(20)= (Select numero From RendicionLote Where IDTransaccion=@vIDTransaccionRendicion)

				set @Mensaje = 'El comprobante tiene una rendicion vigente! OPERACION ' + @OPRendicion 
				set @Procesado = 'False'
				return @@rowcount
			end
			
		End
		
		--Actualizar el Cliente (Saldo Total, Saldo Credito, Ultima Compra)
		Set @vIDCliente = (Select IDCliente From Venta Where IDTransaccion=@IDTransaccion)
		
		EXEC SpAcualizarClienteVenta
			@IDCliente = @vIDCliente,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT
			
		
		
		--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
		
		--Los detalle
		Delete From DetalleImpuesto Where IDTransaccion=@IDTransaccion
		Delete From Descuento Where IDTransaccion=@IDTransaccion
		Delete From DetalleVenta Where IDTransaccion=@IDTransaccion
		--Eliminar venta Cobanza
		Delete From VentaCobranza Where IDTransaccionVenta = @IDTransaccion 			
		--Eliminar la venta
		Delete From VentaLoteDistribucion Where IDTransaccionVenta=@IDTransaccion 

		--Eliminamos el detalle de la rendicion 
		Delete From DetalleRendicionLoteCheque Where IDTransaccionVenta=@IDTransaccion
		
		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from Venta where IDTransaccion = @IDTransaccion)
		set @IDCliente = (select IDCliente from Venta where IDTransaccion = @IDTransaccion)
		set @Fecha = (select FechaEmision from Venta where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from Venta where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select Comprobante from Venta where IDTransaccion = @IDTransaccion)
		set @VTipoComprobante = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from Venta where IDTransaccion = @IDTransaccion)
		set @Condicion = (select (Case when Credito = 'True' then 'CREDITO' else 'CONTADO' end) from Venta where IDTransaccion = @IDTransaccion)

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,@IDCliente,0,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ELI',@VTipoComprobante, @IDSucursal)
		
		
		Delete From Venta Where IDTransaccion=@IDTransaccion
		
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		Set @IDTransaccionSalida = 0
		--print @Mensaje
		return @@rowcount
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		--print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End

﻿CREATE Procedure [dbo].[SpDetalleFondoFijo]

	--Entrada
	@IDTransaccionRendicionFondoFijo numeric(18,0),
	@IDSucursal tinyint,
	@IDGrupo tinyint,
	@Operacion varchar(50),
	@IDTransaccionGasto numeric(18,0),
	@IDTransaccionVale numeric(18,0),
	@Tipo varchar(50),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
As

Begin
	
	--BLOQUES
	
	--ACTUALIZAR
	if @Operacion='INS' begin
		
		--Si es un Gasto
		if @Tipo = 'GASTO' begin
			Update DetalleFondoFijo Set
			IDTransaccionRendicionFondoFijo=@IDTransaccionRendicionFondoFijo,
			Cancelado='True'
			Where IDSucursal=@IDSucursal and IDGrupo=@IDGrupo 
			and IDTransaccionGasto=@IDTransaccionGasto 
		
			set @Mensaje = 'Registro guardado!'
			set @Procesado = 'True'
			return @@rowcount
		end
		
		--Si es un Vale
		if @Tipo = 'VALE' begin
			Update DetalleFondoFijo Set
			IDTransaccionRendicionFondoFijo=@IDTransaccionRendicionFondoFijo,
			Cancelado='True'
			Where IDSucursal=@IDSucursal and IDGrupo=@IDGrupo 
			and IDTransaccionVale=@IDTransaccionVale
		
			set @Mensaje = 'Registro guardado!'
			set @Procesado = 'True'
			return @@rowcount
		end
			
	end
	
	--ANULAR
	if @Operacion='DEL'begin
	
		--Si es un Gasto
		if @Tipo = 'GASTO' begin
			
			--Actualizar el Detalle Fondo Fijo
			Update DetalleFondoFijo Set
			IDTransaccionRendicionFondoFijo=NULL,
			Cancelado='False'
			Where IDSucursal=@IDSucursal and IDGrupo=@IDGrupo 
			and IDTransaccionGasto=@IDTransaccionGasto 
			and IDTransaccionRendicionFondoFijo=@IDTransaccionRendicionFondoFijo 
			
			--Actualizar la Rendicion Fondo Fijo
			Update RendicionFondoFijo Set
			IDTransaccionOrdenPago = NULL
			Where IDSucursal=@IDSucursal and IDGrupo=@IDGrupo 
			and IDTransaccion=@IDTransaccionRendicionFondoFijo 
			
			set @Mensaje = 'Registro guardado!'
			set @Procesado = 'True'
			return @@rowcount
		end
		
		--Si es un Vale
		If @Tipo = 'VAlE' begin
			
			--Actualizar el Detalle Fondo Fijo
			Update DetalleFondoFijo Set
			IDTransaccionRendicionFondoFijo=NULL,
			Cancelado='False'
			Where IDSucursal=@IDSucursal and IDGrupo=@IDGrupo 
			and IDTransaccionRendicionFondoFijo=@IDTransaccionRendicionFondoFijo 
			and IDTransaccionVale=@IDTransaccionVale
			
			--Actualizar la Rendicion Fondo Fijo
			Update RendicionFondoFijo Set
			IDTransaccionOrdenPago = NULL
			Where IDSucursal=@IDSucursal and IDGrupo=@IDGrupo 
			and IDTransaccion=@IDTransaccionRendicionFondoFijo 
		
			set @Mensaje = 'Registro guardado!'
			set @Procesado = 'True'
			return @@rowcount
		end
			
	end
	
	
	
	

End






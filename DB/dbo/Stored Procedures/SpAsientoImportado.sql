﻿CREATE Procedure [dbo].[SpAsientoImportado]

	--Entrada
	@Desde date,
	@Hasta date
	
As

Begin

	Declare @vIDTransaccion numeric(18,0)
	Declare @vMensaje varchar(200)
	Declare @vProcesado bit
	Declare @vRegistros int
	
	Set @vProcesado  = 'False'
	Set @vMensaje = 'No se proceso'
	set @vRegistros = 0
	
	Declare db_cursor cursor for
	Select IDTransaccion From AsientoImportado Where Fecha Between @Desde And @Hasta
	Open db_cursor   
	Fetch Next From db_cursor Into @vIDTransaccion
	While @@FETCH_STATUS = 0 Begin 
		
		Delete From DetalleAsiento Where IDTransaccion=@vIDTransaccion
		Delete From Asiento Where IDTransaccion=@vIDTransaccion
		Delete From AsientoImportado Where IDTransaccion=@vIDTransaccion
		Delete From Transaccion Where ID=@vIDTransaccion
		
		Set @vProcesado  = 'True'
		Set @vMensaje = 'Registro eliminado'
		set @vRegistros = @vRegistros + 1
		
		Fetch Next From db_cursor Into @vIDTransaccion
									
	End
	
	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado, 'Registros'=@vRegistros	
End


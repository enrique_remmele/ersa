﻿CREATE Procedure [dbo].[SpLlamada]
	
	--Entrada
	@ID integer,
	@IDCliente integer,
	@IDTipoLlamada integer,
	@IDVendedor integer,
	@IDResultado integer,
	@LlamarFecha Date,
	@LlamarHora Time,
	@Atendido Varchar(50),
	@Comentario varchar(254),
	@Estado bit,
	@Realizado bit = NULL,
	@Entrante bit = NULL,
	@Saliente bit = NULL,

	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
	    If @ID = 0 Begin
	     set @ID = (select Max(Id)+1 from Llamada) 
	    end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Llamada Where ID = @ID) begin
		   set @ID = (select max(id)+1 from Llamada) 
			--set @Mensaje = 'La llamada ya existe!'
			--set @Procesado = 'False'
			--return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDLlamada integer
		set @vIDLlamada = (Select IsNull((Max(ID)+1), 1) From Llamada)

		--Insertamos
		Insert Into Llamada(ID, IDcliente, Fecha, IDTipoLlamada,IDVendedor, IDresultado, LlamarFecha, LlamarHora, Atendido, Comentario, Estado, Realizado, Entrante, Saliente)
		Values(@ID, @IDcliente, Getdate(), @IDTipoLlamada,@IDVendedor, @IDresultado, @LlamarFecha, @LlamarHora, @Atendido, @Comentario, @Estado, @Realizado, @Entrante, @Saliente)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='LLAMADA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
				 		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From Llamada Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Auditoria
		Insert into ALlamada Select *, @IDUsuario, 'MOD', Getdate() from Llamada where ID = @ID

		--Actualizamos
		Update Llamada 
		Set IDcliente = @IDcliente, 
			Fecha = Getdate(), 
			IDTipoLlamada = @IDTipoLlamada,
			IDVendedor = @IDVendedor, 
			IDresultado = @IDresultado, 
			LlamarFecha = @LlamarFecha,
			LlamarHora = @LlamarHora, 
			Atendido = @Atendido, 
			Comentario = @Comentario,
			Estado = @Estado,		
			Realizado = @Realizado,
			Entrante = @Entrante,
			Saliente = @Saliente
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='LLAMADA', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Llamada Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		--Auditoria
		Insert into ALlamada Select *, @IDUsuario, 'DEL', Getdate() from Llamada where ID = @ID
		
		--Eliminamos
		Update Llamada 
		set Estado = 'False'
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='LLAMADA', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


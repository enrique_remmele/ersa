﻿CREATE procedure [dbo].[SpHechaukaVentaDetalle] 
	@Año smallint,
	@Mes tinyint
	
As
Begin
	
	--Venta
		Select
	'TipoRegistro'= 2,
	'Ruc'= V.Ruc,
	'CI'= (Case when V.Ruc <> C.RUC then 'False' else C.CI end),
	'RUCProveedor'= (Case When(CharIndex('-', V.RUC,0)) = 0 Then V.RUC Else (SubString(V.RUC,0, CharIndex('-', V.RUC,0))) End),
	'DVProveedor'=SubString(V.RUC, CharIndex('-', V.RUC,0)+1,1),
	'NombreCliente'= V.Cliente,
	'TipoDocumento'= 1,
	'NumeroDocumento'= Comprobante,
	'FechaDocumento'= dbo.FFormatoFecha(Fecha),
	'MontoVenta10%'=Round(Isnull ((Select convert(decimal(18,2),[Gravado10%]) from VDetalleImpuestoDesglosadoGravado I where V.IDTransaccion= I.IDTransaccion ) - (Select convert(decimal(18,2),[IVA10%])  from VDetalleImpuestoDesglosado I where V.IDTransaccion= I.IDTransaccion ),0) * V.Cotizacion,0),
	'IVADebito10%'= Round((Select convert(decimal(18,2),[IVA10%])  from VDetalleImpuestoDesglosado I where V.IDTransaccion= I.IDTransaccion ) * V.Cotizacion,0),
	'MontoVenta5%'= Round(Isnull ((Select convert(decimal(18,2),[Gravado5%])   from VDetalleImpuestoDesglosadoGravado I where V.IDTransaccion= I.IDTransaccion )- (Select convert(decimal(18,2),[IVA5%])   from VDetalleImpuestoDesglosado I where V.IDTransaccion= I.IDTransaccion ), 0) * V.Cotizacion,0), 
	'IVADebito5%'= Round((Select convert(decimal(18,2),[IVA5%]) from VDetalleImpuestoDesglosado I where V.IDTransaccion= I.IDTransaccion ) * V.Cotizacion,0), 
	'MontoVentaNoGrabada'= Round((Isnull((Select convert(decimal(18,2),EXENTO)  from VDetalleImpuestoDesglosadoGravado I where V.IDTransaccion= I.IDTransaccion ) * V.Cotizacion,0)),0),
	'MontodelIngreso'= Round((IsNull((Select Sum(DI.Total) From DetalleImpuesto DI Where DI.IDTransaccion=V.IDTransaccion) *  V.Cotizacion, 0)), 0),
	'CondicionVenta'=(Case When V.Credito='True' Then 2 Else 1 End),
	'CantidadCuota'=(Case When V.Credito='True' Then 1 Else 0 End),
	'NumeroTimbrado'=V.Timbrado
	
	From VVenta  V 
	join Cliente C On V.IDCliente = ID 
	join TipoComprobante TC on V.IDTipoComprobante = TC.ID
	Where YEAR(Fecha)=@Año 
	And MONTH(Fecha)=@Mes 
	And V.Procesado='True'
	And V.Anulado = 'False'
	And TC.HechaukaTipoDocumento <> 0
	
	Union all

	--NotaDebito
	Select
	'TipoRegistro'= 2,
	'Ruc'= C.Ruc,
	'CI'= C.CI,
	'RUCProveedor'= (Case When(CharIndex('-', ND.RUC,0)) = 0 Then ND.RUC Else (SubString(ND.RUC,0, CharIndex('-', ND.RUC,0))) End),
	'DVProveedor'=SubString(ND.RUC, CharIndex('-', ND.RUC,0)+1,1),
	'NombreCliente'= ND.Cliente,
	'TipoDocumento'= 2,
	'NumeroDocumento'= Comprobante,
	'FechaDocumento'= dbo.FFormatoFecha(Fecha),
	'MontoVenta10%'= (Isnull((Select CONVERT (bigint,[Gravado10%])  from VDetalleImpuestoDesglosadoGravado  I where ND.IDTransaccion= I.IDTransaccion ) - (Select CONVERT  (Integer,[IVA10%])  from VDetalleImpuestoDesglosado I where ND.IDTransaccion= I.IDTransaccion ),0) * ND.Cotizacion),
	'IVACredito10%'= ((Select CONVERT  (bigint,[IVA10%])  from VDetalleImpuestoDesglosado I where ND.IDTransaccion= I.IDTransaccion ) * ND.Cotizacion),
	'MontoVenta5%'= (Isnull((Select CONVERT (bigint,[Gravado5%])  from VDetalleImpuestoDesglosadoGravado  I where ND.IDTransaccion= I.IDTransaccion )- (Select CONVERT (Integer,[IVA5%])   from VDetalleImpuestoDesglosado I where ND.IDTransaccion= I.IDTransaccion ),0) * ND.Cotizacion),
	'IVACredito5%'= ((Select CONVERT (bigint,[IVA5%])   from VDetalleImpuestoDesglosado I where ND.IDTransaccion= I.IDTransaccion ) * ND.Cotizacion), 
	'MontoVentaNoGrabada'= (Isnull((Select CONVERT (bigint,EXENTO )   from VDetalleImpuestoDesglosadoGravado I where ND.IDTransaccion= I.IDTransaccion ),0) *  ND.Cotizacion),
	'MontodelIngreso'=(Isnull (CONVERT (bigint,Total),0) *  ND.Cotizacion),
	'CondicionVenta'=1,
	'CantidadCuota'=0,
	'NumeroTimbrado'=PE.Timbrado
	
	From VNotaDebito ND 
	join Cliente C On ND.IDCliente = ID 
	join PuntoExpedicion PE On ND.IDPuntoExpedicion = PE.ID
	join TipoComprobante TC on ND.IDTipoComprobante = TC.ID
	Where YEAR(Fecha)=@Año  
	And MONTH(Fecha)=@Mes 
	And ND.Procesado='True'
	And ND.Anulado = 'False'
	And TC.HechaukaTipoDocumento <> 0

	Union All

	--NotaCredito
	Select
	'TipoRegistro'= 2,
	'Ruc'= C.Ruc,
	'CI'= 'False',
	'RUCProveedor'= (Case When(CharIndex('-', NC.RUC,0)) = 0 Then NC.RUC Else (SubString(NC.RUC,0, CharIndex('-', NC.RUC,0))) End),
	'DVProveedor'=SubString(NC.RUC, CharIndex('-', NC.RUC,0)+1,1),
	'NombreCliente'= NC.Proveedor,
	'TipoDocumento'= 3,
	'NumeroDocumento'= Comprobante,
	'FechaDocumento'= dbo.FFormatoFecha(Fecha),
	'MontoVenta10%'=(CONVERT (bigint,Round((Isnull ((Select [Gravado10%]  from VDetalleImpuestoDesglosadoGravado  I where NC.IDTransaccion= I.IDTransaccion),0)- isnull((Select [IVA10%]  from VDetalleImpuestoDesglosado I where NC.IDTransaccion= I.IDTransaccion ),0)) * NC.Cotizacion,0))),
	'IVACredito10%'= (CONVERT (bigint,Round(Isnull ((Select [IVA10%]  from VDetalleImpuestoDesglosado I where NC.IDTransaccion= I.IDTransaccion ),0) * NC.Cotizacion,0))),
	'MontoVenta5%'=(CONVERT (bigint, Round((Isnull ((Select [Gravado5%]  from VDetalleImpuestoDesglosadoGravado  I where NC.IDTransaccion= I.IDTransaccion),0)- isnull((Select [IVA5%] from VDetalleImpuestoDesglosado I where NC.IDTransaccion= I.IDTransaccion ),0)) * NC.Cotizacion,0))), 
	'IVACredito5%'= (CONVERT (bigint,Round((Select [IVA5%] from VDetalleImpuestoDesglosado I where NC.IDTransaccion= I.IDTransaccion ) * NC.Cotizacion,0))), 
	'MontoVentaNoGrabada'= (CONVERT (bigint, Round(Isnull((Select EXENTO from VDetalleImpuestoDesglosadoGravado I where NC.IDTransaccion= I.IDTransaccion),0) * NC.Cotizacion,0))),
	'MontodelIngreso'=CONVERT (bigint,Round((Isnull (Total,0)* NC.Cotizacion),0)),
	'CondicionVenta'=1,
	'CantidadCuota'=0,
	'NumeroTimbrado'=NC.NroTimbrado
	From VNotaCreditoProveedor NC
	join VProveedor C On NC.IDProveedor= ID
	join TipoComprobante TC ON NC.IDTipoComprobante = TC.ID
	Where YEAR(Fecha)=@Año  
	And MONTH(Fecha)=@Mes  
	And NC.Anulado = 'False'
	And TC.HechaukaTipoDocumento <> 0
	--select [Gravado10%] from VDetalleImpuestoDesglosadoGravado where IDTransaccion = 694313
	--select (Isnull (CONVERT (bigint, (Select [Gravado10%]  from VDetalleImpuestoDesglosadoGravado  I where I.IDTransaccion = 694313 )- (Select [IVA10%]  from VDetalleImpuestoDesglosado I where I.IDTransaccion = 694313)),0) * 565)

	union all

	Select
	'TipoRegistro'= 2,
	'Ruc'= C.Ruc,
	'CI'= C.CI,
	'RUCProveedor'= (Case When(CharIndex('-', V.RUC,0)) = 0 Then V.RUC Else (SubString(V.RUC,0, CharIndex('-', V.RUC,0))) End),
	'DVProveedor'=SubString(V.RUC, CharIndex('-', V.RUC,0)+1,1),
	'NombreCliente'= V.RazonSocial,
	'TipoDocumento'= 1,
	'NumeroDocumento'= NroComprobante,
	'FechaDocumento'= dbo.FFormatoFecha(Fecha),
	'MontoVenta10%'=(Isnull ((Select CONVERT (bigint,[Gravado10%])   from VDetalleImpuestoDesglosadoGravado I where V.IDTransaccion= I.IDTransaccion ) - (Select CONVERT (Integer, [IVA10%])  from VDetalleImpuestoDesglosado I where V.IDTransaccion= I.IDTransaccion ),0)) * V.Cotizacion,
	'IVADebito10%'= ((Select CONVERT (bigint, [IVA10%])  from VDetalleImpuestoDesglosado I where V.IDTransaccion= I.IDTransaccion ) * V.Cotizacion),
	'MontoVenta5%'= (Isnull ((Select CONVERT (bigint,[Gravado5%])   from VDetalleImpuestoDesglosadoGravado I where V.IDTransaccion= I.IDTransaccion )- (Select CONVERT (Integer, [IVA5%])   from VDetalleImpuestoDesglosado I where V.IDTransaccion= I.IDTransaccion ), 0) * V.Cotizacion), 
	'IVADebito5%'= ((Select CONVERT (bigint, [IVA5%])   from VDetalleImpuestoDesglosado I where V.IDTransaccion= I.IDTransaccion ) * V.Cotizacion), 
	'MontoVentaNoGrabada'= (Isnull((Select CONVERT (bigint,EXENTO )   from VDetalleImpuestoDesglosadoGravado I where V.IDTransaccion= I.IDTransaccion ),0) * V.Cotizacion),
	--'MontodelIngreso'=Isnull (CONVERT (Integer,Total),0),
	'MontodelIngreso'= (IsNull((Select Sum(DI.Total) From DetalleImpuesto DI Where DI.IDTransaccion=V.IDTransaccion), 0) *  V.Cotizacion),
	--'MontodelIngreso'=IsNull((Select CEILING(Sum(DI.Total)) From DetalleImpuesto DI Where DI.IDTransaccion=V.IDTransaccion), 0),
	'CondicionVenta'=(Case When V.Credito='True' Then 2 Else 1 End),
	'CantidadCuota'=(Case When V.Credito='True' Then 1 Else 0 End),
	'NumeroTimbrado'=V.NroTimbrado
	
	From vComprobanteLibroIVA  V 
	join Cliente C On V.IDCliente = ID 
	join TipoComprobante TC on V.IDTipoComprobante = TC.ID
	Where YEAR(Fecha)=@Año 
	And MONTH(Fecha)=@Mes 
	And TC.HechaukaTipoDocumento <> 0
	ANd TC.LibroVenta = 1

End







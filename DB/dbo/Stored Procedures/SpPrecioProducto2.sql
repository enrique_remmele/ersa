﻿CREATE Procedure [dbo].[SpPrecioProducto2]

	--Entrada
	@Fecha Date = null,
	@IDProducto int,
	@IDCliente int,
	@IDListaPrecio smallint = 0,
	@IDSucursal tinyint,
	@IDClienteSucursal tinyint=0,
	@IDDeposito tinyint = 0,
	@ControlarReserva bit = 'False',
	@IDMoneda Tinyint,
	@Cotizacion decimal(18,2) 
	
As

Begin

	--Variables
	Declare @vIDListaPrecio int
	Declare @vExistencia decimal(10,2)
	Declare @vListaPrecio varchar(100)
	
	Declare @vFecha date
	
	if @Fecha is null begin
		Set @Fecha = convert(Date,getdate())
	end
	
	Set @vFecha = convert(Date,@Fecha)
	
	--Si la lista de precio no se envia por parametros
	If @IDListaPrecio = 0 Begin	
		--Primero Hayamos el Precio			
		--Obtener la lista de precio del cliente
		--Si no se selecciono una sucursal
		If @IDClienteSucursal = 0 Begin
			Set @vIDListaPrecio = (Select IsNull((Select IDListaPrecio From Cliente Where ID=@IDCliente), 0))
		End
	
		--Si es por sucursal del cliente
		If @IDClienteSucursal > 0 Begin
			Set @vIDListaPrecio = (Select IsNull((Select IDListaPrecio From ClienteSucursal Where IDCliente=@IDCliente And ID=@IDClienteSucursal), 0))
		End
		Set @vListaPrecio = (select Descripcion from ListaPrecio where id = @vIDListaPrecio)
	End Else Begin
		Set @vIDListaPrecio = @IDListaPrecio
		Set @vListaPrecio = (select Descripcion from ListaPrecio where id = @vIDListaPrecio)
	End
	----------------------------------------------------------------

	--Aprovechamos y sacamos la existencia, para la venta y pedido
	--y asi no consultamos tantas veces a la BD desde la aplicacion
	Set @vExistencia = dbo.FExistenciaProducto(@IDProducto, @IDDeposito)
	
	If @ControlarReserva = 'True' Begin
		Set @vExistencia = @vExistencia - ISNULL((dbo.FExistenciaProductoReservado(@IDProducto, @IDDeposito)), 0)
	End
	--ATENCION!!! - Saque la sucursal para poder vender a clientes de otra sucursal	
	--Este es para Precio Normal	
	-- Cuando el precio esta en la misma moneda que la operacion
	IF (Select Top(1) IDMoneda from vProductoListaPrecio where IDProducto = @IDProducto and ListaPrecio = @vListaPrecio) = @IDMoneda Begin
		print @vFecha
		Select top(1) 'IDTipo'=-1, 'Tipo'='Precio', 'TipoDescuento'='', 'IDActividad'=NULL, 'Importe'=P.Precio, 'Porcentaje'=0, 'Existencia'=@vExistencia, 'Tactico'='False',IDMoneda
		From vProductoListaPrecio P
		Where P.IDProducto=@IDProducto
		and ListaPrecio = @vListaPrecio
	
		Union All
	
		--Este es para TPR	
		Select top(1) 'IDTipoDescuento'=0, 'Tipo'='TPR', 'T. Desc.'='TPR', 'IDActividad'=NULL, 'Importe'=IsNull(P.TPR,0), 'Porcentaje'=IsNull(P.TPRPorcentaje,0), 'Existencia'=@vExistencia, 'Tactico'='False', IDMoneda
		From vProductoListaPrecio P
		Where P.IDProducto=@IDProducto 
		--And P.ListaPrecio=@vListaPrecio
		And P.ListaPrecio=''
		And (@vFecha Between IsNull(P.TPRDesde, DateAdd(d,-1, @vFecha)) And IsNull(P.TPRHasta, DateAdd(d,1, @vFecha)))
	
		Union all
	
		--Descuentos normales
		Select top(1)PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, PLPE.Descuento, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='False' , IDMoneda
		From vProductoListaPrecioExcepciones PLPE 
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto 
		--And ListaPrecio=@vListaPrecio 
		And ListaPrecio=''
		And IDCliente=@IDCliente  
		And TD.Descripcion <> 'TACTICO'
		And (@vFecha Between convert(Date,Desde) And convert(Date,Hasta))
	
		Union All
	
		--Excepciones TACTICO
		Select top(1) PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, 
		PLPE.Descuento, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='True' , IDMoneda
		From vProductoListaPrecioExcepciones PLPE 
		--Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto 
		----Prueba excepciones por LISTA DE PRECIO----------------------!!!!!!!!!!!!!!!!!!!!!!!!!!!
		--And PLPE.ListaPrecio=(Case when PLPE.IDTipoProducto = 1 then '' else @vListaPrecio end)
		And PLPE.ListaPrecio=@vListaPrecio
		And PLPE.IDCliente=@IDCliente  
		And @vFecha between convert(Date,PLPE.Desde) And convert(Date,PLPE.Hasta)
		And TD.Descripcion = 'TACTICO'
		And (Case When isnull(PLPE.CantidadLimite,0)=0 then 1 else PLPE.CantidadLimite end)>(Case When Isnull(PLPE.CantidadLimite,0)=0 then 0 else PLPE.CantidadLimiteSaldo end) 
		And ISnull(PLPE.IDTransaccionPedido,0) = 0			
    End -- Fin de la misma moneda
-- precio GS a FACT otras monedas
--ATENCION!!! - Saque la sucursal para poder vender a clientes de otra sucursal	

	IF (@IDMoneda <> 1) and 
	(Select top(1) IDMoneda from vProductoListaPrecio where IDProducto = @IDProducto and ListaPrecio = @vListaPrecio) = 1 Begin
		--Este es para Precio Normal	
		Select 'IDTipo'=-1, 'Tipo'='Precio', 'TipoDescuento'='', 'IDActividad'=NULL, 
			   'Importe'=P.Precio / @Cotizacion, 'Porcentaje'=0, 'Existencia'=@vExistencia, 'Tactico'='False', IDMoneda
		From vProductoListaPrecio P
		--Join ListaPrecio LP On P.IDListaPrecio=LP.ID
		Where P.IDProducto=@IDProducto 
		And P.ListaPrecio=@vListaPrecio
		
		--And LP.IDSucursal=@IDSucursal 
	
		Union All
	
		--Este es para TPR	
		Select top(1) 'IDTipoDescuento'=0, 'Tipo'='TPR', 'T. Desc.'='TPR', 'IDActividad'=NULL, 'Importe'=IsNull(P.TPR / @Cotizacion,0), 'Porcentaje'=IsNull(P.TPRPorcentaje,0), 'Existencia'=@vExistencia, 'Tactico'='False' , IDMoneda
		From vProductoListaPrecio P
		--Join ListaPrecio LP On P.IDListaPrecio=LP.ID
		Where P.IDProducto=@IDProducto 
		And P.ListaPrecio=''--@vListaPrecio 
		And (convert(Date,@vFecha) Between IsNull(convert(Date,P.TPRDesde), DateAdd(d,-1, @vFecha)) And IsNull(convert(Date,P.TPRHasta), DateAdd(d,1, @vFecha)))
	
		Union all
	
		--Descuentos normales 
		Select top(1) PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, PLPE.Descuento / @Cotizacion, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='False' , IDMoneda
		From vProductoListaPrecioExcepciones PLPE 
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto 
		And ListaPrecio=''--@vListaPrecio 
		And IDCliente=@IDCliente  
		--And IDSucursal = @IDSucursal 
		And TD.PlanillaTactico = 'False'
		And (@vFecha Between convert(Date,Desde) And convert(Date,Hasta))
	
		Union All
	
		--Excepciones TACTICO
		Select top(1) PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, PLPE.Descuento / @Cotizacion, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='True' , IDMoneda
		From vProductoListaPrecioExcepciones PLPE 
		--Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto 
		and (Case when PLPE.IDTipoProducto = 1 then '' else PLPE.ListaPrecio end)=(Case when PLPE.IDTipoProducto = 1 then '' else @vListaPrecio end)
		And PLPE.IDCliente=@IDCliente  
		--And LP.IDSucursal = @IDSucursal 
		And @vFecha between convert(Date,PLPE.Desde) And convert(Date,PLPE.Hasta)
		And TD.PlanillaTactico = 'True'
		And ISnull(PLPE.IDTransaccionPedido,0) = 0
		And (Case When Exists(Select * From VDetalleActividad D 
											Join VActividad A On D.IDActividad=A.ID 
											Join DetallePlanillaDescuentoTactico DP On A.ID=DP.IDActividad 
											Join PlanillaDescuentoTactico P On DP.IDTransaccion=P.IDTransaccion 
											Where D.ID=@IDProducto And (@vFecha between convert(Date,P.Desde) And convert(Date,P.Hasta)) And P.IDSucursal=@IDSucursal) Then 'True' Else 'False' End)='True'

    ENd -- de Gs a otras monedas
	print 'k'
	-- Precio otras monedas a Fact GS
--ATENCION!!! - Saque la sucursal para poder vender a clientes de otra sucursal	
	IF (@IDMoneda = 1) and 
	(Select top(1) IDMoneda from vProductoListaPrecio where IDProducto = @IDProducto and ListaPrecio = @vListaPrecio) <> 1 Begin
		--Este es para Precio Normal	
		declare @IDMonedaPrecio1 tinyint = (Select top(1) IDMoneda from vProductoListaPrecio where IDProducto = @IDProducto and ListaPrecio = @vListaPrecio)
		declare @vCotizacionMonedaPrecio1 Decimal = (Select top(1) Cotizacion from Cotizacion where IDMoneda = @IDMonedaPrecio1 order by ID desc )

		Select 'IDTipo'=-1, 'Tipo'='Precio', 'TipoDescuento'='', 'IDActividad'=NULL, 'Importe'=P.Precio * @vCotizacionMonedaPrecio1, 'Porcentaje'=0, 'Existencia'=@vExistencia, 'Tactico'='False', IDMoneda
		From vProductoListaPrecio P
		--Join ListaPrecio LP On P.IDListaPrecio=LP.ID
		Where P.IDProducto=@IDProducto 
		And P.ListaPrecio=@vListaPrecio 
		--And LP.IDSucursal=@IDSucursal 
	
		Union All
	
		--Este es para TPR	
		Select top(1) 'IDTipoDescuento'=0, 'Tipo'='TPR', 'T. Desc.'='TPR', 'IDActividad'=NULL, 'Importe'=IsNull(P.TPR * @vCotizacionMonedaPrecio1,0), 'Porcentaje'=IsNull(P.TPRPorcentaje,0), 'Existencia'=@vExistencia, 'Tactico'='False'  , IDMoneda
		From vProductoListaPrecio P
		--Join ListaPrecio LP On P.IDListaPrecio=LP.ID
		Where P.IDProducto=@IDProducto 
		And P.ListaPrecio=''--@vListaPrecio 
		--And LP.IDSucursal=@IDSucursal 
		And (@vFecha Between IsNull(P.TPRDesde, DateAdd(d,-1, @vFecha)) And IsNull(P.TPRHasta, DateAdd(d,1, @vFecha)))
	
		Union all
	
		--Descuentos normales
		Select top(1) PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, PLPE.Descuento * @vCotizacionMonedaPrecio1, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='False' , IDMoneda
		From vProductoListaPrecioExcepciones PLPE 
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto 
		And ListaPrecio=''--@vListaPrecio 
		And IDCliente=@IDCliente  
		--And IDSucursal = @IDSucursal 
		And TD.PlanillaTactico = 'False'
		And (@vFecha Between Desde And Hasta)
	
		Union All
	
		--Excepciones TACTICO
		Select top(1) PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, PLPE.Descuento * @vCotizacionMonedaPrecio1, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='True' , IDMoneda
		From vProductoListaPrecioExcepciones PLPE 
		--Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto
		and (Case when PLPE.IDTipoProducto = 1 then '' else PLPE.ListaPrecio end)=(Case when PLPE.IDTipoProducto = 1 then '' else @vListaPrecio end)
		 And PLPE.IDCliente=@IDCliente  
		--And LP.IDSucursal = @IDSucursal 
		And @vFecha between PLPE.Desde And PLPE.Hasta
		And TD.PlanillaTactico = 'True'
		And ISnull(PLPE.IDTransaccionPedido,0) = 0			
		And (Case When Exists(Select * From VDetalleActividad D 
											Join VActividad A On D.IDActividad=A.ID 
											Join DetallePlanillaDescuentoTactico DP On A.ID=DP.IDActividad 
											Join PlanillaDescuentoTactico P On DP.IDTransaccion=P.IDTransaccion 
											Where D.ID=@IDProducto And (@vFecha between P.Desde And P.Hasta) And P.IDSucursal=@IDSucursal) Then 'True' Else 'False' End)='True'

    ENd -- de otras monedas a GS

	
	-- Precio otras monedas a otras monedas (US - RS)
--ATENCION!!! - Saque la sucursal para poder vender a clientes de otra sucursal	
	IF (@IDMoneda <> 1) and 
	(Select top(1) IDMoneda from vProductoListaPrecio where IDProducto = @IDProducto and ListaPrecio = @vListaPrecio) <> 1 Begin
		--Este es para Precio Normal	
		declare @IDMonedaPrecio tinyint = (Select top(1) IDMoneda from vProductoListaPrecio where IDProducto = @IDProducto and ListaPrecio = @vListaPrecio)
		declare @vCotizacionMonedaPrecio Decimal = (Select top(1) Cotizacion from Cotizacion where IDMoneda = @IDMonedaPrecio order by ID desc )

		Select top(1) 'IDTipo'=-1, 'Tipo'='Precio', 'TipoDescuento'='', 'IDActividad'=NULL, 'Importe'=(P.Precio * @vCotizacionMonedaPrecio)/ @Cotizacion, 'Porcentaje'=0, 'Existencia'=@vExistencia, 'Tactico'='False', IDMoneda
		From vProductoListaPrecio P
		--Join ListaPrecio LP On P.IDListaPrecio=LP.ID
		Where P.IDProducto=@IDProducto 
		And P.ListaPrecio=@vListaPrecio 
		--And LP.IDSucursal=@IDSucursal 
	
		Union All
	
		--Este es para TPR	
		Select top(1) 'IDTipoDescuento'=0, 'Tipo'='TPR', 'T. Desc.'='TPR', 'IDActividad'=NULL, 'Importe'=IsNull((P.TPR * @vCotizacionMonedaPrecio)/@Cotizacion,0), 'Porcentaje'=IsNull(P.TPRPorcentaje,0), 'Existencia'=@vExistencia, 'Tactico'='False', IDMoneda
		From vProductoListaPrecio P
		--Join ListaPrecio LP On P.IDListaPrecio=LP.ID
		Where P.IDProducto=@IDProducto 
		And P.ListaPrecio=''--@vListaPrecio 
		--And LP.IDSucursal=@IDSucursal 
		And (@vFecha Between IsNull(P.TPRDesde, DateAdd(d,-1, @vFecha)) And IsNull(P.TPRHasta, DateAdd(d,1, @vFecha)))
	
		Union all
	
		--Descuentos normales
		Select top(1) PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, (PLPE.Descuento * @vCotizacionMonedaPrecio)/@Cotizacion, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='False' , IDMoneda
		From vProductoListaPrecioExcepciones PLPE 
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto 
		And ListaPrecio=''--@vListaPrecio 
		And IDCliente=@IDCliente  
		--And IDSucursal = @IDSucursal 
		And TD.PlanillaTactico = 'False'
		And (@vFecha Between Desde And Hasta)
	
		Union All
	
		--Excepciones TACTICO
		Select top(1) PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, (PLPE.Descuento * @vCotizacionMonedaPrecio)/@Cotizacion, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='True' , IDMoneda
		From vProductoListaPrecioExcepciones PLPE 
		--Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto 
		and (Case when PLPE.IDTipoProducto = 1 then '' else PLPE.ListaPrecio end)=(Case when PLPE.IDTipoProducto = 1 then '' else @vListaPrecio end)
		And PLPE.IDCliente=@IDCliente  
		--And LP.IDSucursal = @IDSucursal 
		And @vFecha between PLPE.Desde And PLPE.Hasta
		And TD.PlanillaTactico = 'True'
		And ISnull(PLPE.IDTransaccionPedido,0) = 0			
		And (Case When Exists(Select * From VDetalleActividad D 
											Join VActividad A On D.IDActividad=A.ID 
											Join DetallePlanillaDescuentoTactico DP On A.ID=DP.IDActividad 
											Join PlanillaDescuentoTactico P On DP.IDTransaccion=P.IDTransaccion 
											Where D.ID=@IDProducto And (@vFecha between P.Desde And P.Hasta) And P.IDSucursal=@IDSucursal) Then 'True' Else 'False' End)='True'

    ENd -- de otras monedas a GS
End




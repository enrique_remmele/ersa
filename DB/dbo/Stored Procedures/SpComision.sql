﻿CREATE Procedure  [dbo].[SpComision]

	--Entrada
	@IDTransaccion numeric (18,0)= NULL,
	@Venta bit= 'False',
	@Devolucion bit = 'False',
	@Operacion varchar(10)

As

Begin

	--CARLOS 26-02-2014
	
	-- Variables
	Declare @vIDTransaccion numeric (18,0)
	Declare @vIDProducto int 
	Declare @vIDVendedor int 
	Declare @vFecha date
	Declare @vIDSucursal int 
	Declare @vID tinyint
	Declare @vPorcentaje decimal(5,3)
	Declare @vTotal money
	Declare @vMonto money
	 
	 --Insertar Notas de Credito
	If @Operacion = 'INS' And @Devolucion  ='True' Begin
		
		Declare db_cursorNC cursor for
		Select D.IDTransaccion, D.IDProducto, D.ID, C.IDVendedor, V.IDSucursal, V.Fecha
		From DetalleNotaCredito D 
		Join VNotaCredito V On D.IDTransaccion=V.IDTransaccion Join VCliente C On V.IDCliente=C.ID
		Where D.IDTransaccion=@IDTransaccion 
		Open db_cursorNC   
		Fetch Next From db_cursorNC Into @vIDTransaccion, @vIDProducto, @vID, @vIDVendedor, @vIDSucursal, @vFecha  
		While @@FETCH_STATUS = 0 Begin 
		
			If Not Exists (Select * from ComisionProducto Where IDProducto=@vIDProducto And IDVendedor=@vIDVendedor And IDSucursal=@vIDSucursal) Begin
				print 'No tiene comision'
				GoTo Siguiente
			End
			
			Set @vTotal= (Select Total From DetalleNotaCredito  Where IDTransaccion=@vIDTransaccion And IDProducto=@vIDProducto And ID=@vID)
			
			--Buscar primero el porcentaje del producto
			Set @vPorcentaje = IsNull((Select PorcentajeEsteProducto From ComisionProducto Where IDProducto=@vIDProducto And IDVendedor=@vIDVendedor And IDSucursal=@vIDSucursal),0)
			
			--Si no se encuenta, buscar por el tipo de producto
			if @vPorcentaje = 0 Begin
				Set @vPorcentaje = IsNull((Select PorcentajeBase From ComisionProducto Where IDProducto=@vIDProducto And IDVendedor=@vIDVendedor And IDSucursal=@vIDSucursal),0)
			End
			
			Set @vMonto = ROUND(@vTotal * (@vPorcentaje / 100),0)
			
			--Si la comision es 0
			If @vPorcentaje=0 Begin
				--print 'No tiene comision'
				GoTo Siguiente
			End
			
			--Si existe, Actualizamos
			If Exists(Select * From Comision Where IDTransaccionDetalleVenta=@vIDTransaccion And IDProducto=@vIDProducto And ID=@vID And Devolucion='True') Begin
				Update Comision Set Porcentaje=@vPorcentaje,
									Monto=@vMonto
				Where IDTransaccionDetalleVenta=@vIDTransaccion And IDProducto=@vIDProducto And ID=@vID				
			End
			
			If Not Exists(Select * From Comision Where IDTransaccionDetalleVenta=@vIDTransaccion And IDProducto=@vIDProducto And ID=@vID And Devolucion='True') Begin
				Insert Into Comision(IDTransaccionDetalleVenta, IDProducto, ID, Fecha,  Porcentaje, Monto, Venta, Devolucion)
				Values (@vIDTransaccion, @vIDProducto, @vID, @vFecha, @vPorcentaje, @vMonto, @Venta, @Devolucion)
			End
		
		Siguiente:
			Fetch Next From db_cursorNC Into @vIDTransaccion, @vIDProducto, @vID, @vIDVendedor, @vIDSucursal, @vFecha  
		End	
			
		Close db_cursorNC   
		Deallocate db_cursorNC
		
	End	
	
	--Insertar Ventas
	If @Operacion = 'INS' And @Venta='True' Begin
		
		Declare db_cursorVenta cursor for
		Select D.IDTransaccion, D.IDProducto, D.ID, V.IDVendedor, V.IDSucursal, V.Fecha
		From VDetalleVenta D Join VVenta V On D.IDTransaccion=V.IDTransaccion
		Where D.IDTransaccion=@IDTransaccion 
		Open db_cursorVenta   
		Fetch Next From db_cursorVenta Into @vIDTransaccion, @vIDProducto, @vID, @vIDVendedor, @vIDSucursal, @vFecha  
		While @@FETCH_STATUS = 0 Begin 
		
			--Si no tiene comision
			If Not Exists (Select * from ComisionProducto Where IDProducto=@vIDProducto And IDVendedor=@vIDVendedor And IDSucursal=@vIDSucursal) Begin
				--print 'No tiene comision'
				GoTo Siguiente1
			End
			
			Set @vTotal= (Select Total From DetalleVenta Where IDTransaccion=@vIDTransaccion And IDProducto=@vIDProducto And ID=@vID)
			
			--Buscar primero el porcentaje del producto
			Set @vPorcentaje = IsNull((Select PorcentajeEsteProducto From ComisionProducto Where IDProducto=@vIDProducto And IDVendedor=@vIDVendedor And IDSucursal=@vIDSucursal),0)
			
			--Si no se encuenta, buscar por el tipo de producto
			if @vPorcentaje = 0 Begin
				Set @vPorcentaje = IsNull((Select PorcentajeBase From ComisionProducto Where IDProducto=@vIDProducto And IDVendedor=@vIDVendedor And IDSucursal=@vIDSucursal),0)
			End
			
			Set @vMonto = ROUND(@vTotal * (@vPorcentaje / 100),0)
			
			--Si la comision es 0
			If @vPorcentaje=0 Begin
				--print 'No tiene comision'
				GoTo Siguiente1
			End
			
			print 'Total: ' + Convert(varchar(50), @vTotal) + ' - Porcentaje: ' + Convert(varchar(50), @vPorcentaje) + ' - Monto: ' + Convert(varchar(50), @vMonto)
			
			--Si existe, Actualizamos
			If Exists(Select * From Comision Where IDTransaccionDetalleVenta=@vIDTransaccion And IDProducto=@vIDProducto And ID=@vID) Begin
				
				print 'Actualizamos'
				Update Comision Set Porcentaje=@vPorcentaje,
									Monto=@vMonto
				Where IDTransaccionDetalleVenta=@vIDTransaccion And IDProducto=@vIDProducto And ID=@vID				
				
			End
			
			If Not Exists(Select * From Comision Where IDTransaccionDetalleVenta=@vIDTransaccion And IDProducto=@vIDProducto And ID=@vID) Begin
			
				print 'Insertamos'
				Insert Into Comision(IDTransaccionDetalleVenta, IDProducto, ID, Fecha,  Porcentaje, Monto, Venta, Devolucion)
				Values (@vIDTransaccion, @vIDProducto, @vID, @vFecha, @vPorcentaje, @vMonto, @Venta, @Devolucion)
				
			End
			
		Siguiente1:

			Fetch Next From db_cursorVenta Into @vIDTransaccion, @vIDProducto, @vID, @vIDVendedor, @vIDSucursal, @vFecha  
				
		End	
			
		Close db_cursorVenta   
		Deallocate db_cursorVenta
		
	End	
	
	If @Operacion = 'ANULAR' Begin
	
		Delete Comision Where IDTransaccionDetalleVenta= @IDTransaccion 
		
	End	
	
	If @Operacion = 'ELIMINAR' Begin
	
		Delete Comision Where IDTransaccionDetalleVenta= @IDTransaccion 
		
	End	
		
		
End


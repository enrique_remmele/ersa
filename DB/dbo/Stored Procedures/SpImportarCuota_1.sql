﻿CREATE Procedure [dbo].[SpImportarCuota]

	--Cabecera
	@TipoComprobante varchar(10),
	@ReferenciaSucursal varchar(50) = 1,
	@ReferenciaTerminal varchar(50) = 1,
	@Comprobante varchar(50),
	@NroTimbrado varchar(50)=NULL,
	
	--Proveedor
	@Referencia varchar(50),
	@Proveedor varchar(100),
	@RUC varchar(50),

	--Cuota
	@NroCuota smallint = 1,
	@Cuota smallint = 1,
	@Vencimiento date = NULL,
	@Importe money = 0,
	@Saldo money = 0,

	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,

	@Actualizar bit
	
		
As

Begin
		
	--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		Declare @vOperacion varchar(50)
		Declare @vIDTransaccion int
		Declare @vComprobante varchar(50)
		Declare @vCancelado bit 

		--Codigos
		Declare @vIDTipoComprobante int
		Declare @vIDProveedor int
					
	End
	
	--Establecer valores predefinidos
	Begin
		
		Set @vOperacion = 'INS'
			
		
		Set @vCancelado = 'False'
		If @Saldo = 0 Begin
			Set @vCancelado = 'True'
		End	
			
	End
	
	
	--Hayar Valores
	Begin
		
		Set @vMensaje = 'No se procesó!'
		Set @vProcesado = 'False'
		
		--validar
		--Tipo de Comprobante
		Set @vIDTipoComprobante = (IsNull((Select Top(1) ID From TipoComprobante Where Codigo=@TipoComprobante And IDOperacion=@IDOperacion), 0))
		If @vIDTipoComprobante = 0 Begin
			Set @vMensaje = 'No se encontro el tipo de comprobante!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Proveedor
		Set @vIDProveedor=(IsNull((Select Top(1) ID From Proveedor Where Referencia=@Referencia),0))
		If @vIDProveedor = 0 Begin
			Set @vIDProveedor=(IsNull((Select Top(1) ID From Proveedor Where RUC=@RUC),0))
		End 

		If @vIDProveedor = 0 Begin
			Set @vMensaje = 'No se encontro el proveedor!'
			Set @vProcesado = 'False'
			GoTo Salir
		End 
		
		--Actualizamos la referencia del proveedor
		Update Proveedor Set Referencia=@Referencia
		Where ID=@vIDProveedor

		Set @vComprobante = '0' + dbo.FFormatoDosDigitos(@ReferenciaSucursal) + '-' + '0' + dbo.FFormatoDosDigitos(@ReferenciaTerminal) + '-' + @Comprobante 		
	
	End
	
	--Verificar si ya existe
	If Exists(Select * From GastoImportado GI Join Gasto G On GI.IDTransaccion=G.IDTransaccion Join TipoComprobante TC On G.IDTipoComprobante=TC.ID Where G.NroComprobante=@vComprobante And G.IDProveedor=@vIDProveedor And G.NroTimbrado=@NroTimbrado And TC.Codigo=@TipoComprobante) Begin
		set @vIDTransaccion = IsNull((Select Top(1) GI.IDTransaccion From GastoImportado GI Join Gasto G On GI.IDTransaccion=G.IDTransaccion Join TipoComprobante TC On G.IDTipoComprobante=TC.ID Where G.NroComprobante=@vComprobante And G.IDProveedor=@vIDProveedor And G.NroTimbrado=@NroTimbrado And TC.Codigo=@TipoComprobante), 0) 
		Set @vOperacion = 'UPD'
	End

	--Validar
	If @vIDTransaccion = 0 Begin
		Set @vMensaje = 'No se encontro el documento! ' + @vComprobante
		Set @vProcesado = 'False'
		GoTo Salir
	End
		
	--Registrar
	--Verificar si ya existe la cuota
	If Not Exists(Select * From Cuota Where IDTransaccion=@vIDTransaccion And Cuota.ID=@NroCuota) Begin

		Insert into Cuota(IDTransaccion, ID, Importe, Saldo, Vencimiento, Cancelado, Pagar, ImporteAPagar, IDCuentaBancaria, ObservacionPago)
		Values(@vIDTransaccion, @NroCuota, @Importe, @Saldo, @Vencimiento, @vCancelado, 'False', 0, NULL, '')

		--Actualizamos el Gasto
		Update Gasto Set Cuota=@Cuota
		Where IDTransaccion=@vIDTransaccion

		Set @vMensaje = 'Registro guardado!'
		Set @vProcesado = 'True'
		GoTo Salir

	End Else Begin
		
		--Si existe, actualizamos
		If @Actualizar = 'True' Begin

			Update Cuota Set Importe=@Importe,
								Saldo=@Saldo,
								Cancelado=@vCancelado,
								Vencimiento=@Vencimiento
			Where IDTransaccion=@vIDTransaccion And ID=@NroCuota

			Set @vMensaje = 'Registro actualizado!'
			Set @vProcesado = 'True'
			GoTo Salir
		End
	End

Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


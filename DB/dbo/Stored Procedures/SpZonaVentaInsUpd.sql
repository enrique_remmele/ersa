﻿CREATE Procedure [dbo].[SpZonaVentaInsUpd]

	--Entrada
	@Descripcion varchar(100)
	
As

Begin

	Declare @vID int
	
	If @Descripcion = '' Begin
		Set @Descripcion = 'CENTRAL'
	End
	
	--Si existe
	If Exists(Select * From ZonaVenta Where Descripcion = @Descripcion) Begin
		Set @vID = (Select Top(1) ID From ZonaVenta Where Descripcion = @Descripcion)
		
	End Else Begin
		Set @vID = (Select IsNull(Max(ID)+1,1) From ZonaVenta)
		Insert Into ZonaVenta(ID, Descripcion, Orden, Estado)
		Values(@vID, @Descripcion, 0, 'True')
	End
	
	return @vID
	
End


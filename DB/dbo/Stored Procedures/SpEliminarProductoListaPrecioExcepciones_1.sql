﻿CREATE Procedure [dbo].[SpEliminarProductoListaPrecioExcepciones]

	--Entrada
	@IDProducto int = 0,
	@IDCliente int = 0,
	
	--Transaccion
	@IDUsuario int,
	@IDSucursal tinyint,
	@IDDeposito tinyint = 0,
	@IDTerminal int = 0,
	
	--Salida
	@Mensaje varchar(200) = '' output ,
	@Procesado bit = 0 output 
As

Begin

	DECLARE @IDListaPrecio tinyint
	DECLARE @IDTipoDescuento tinyint
	DECLARE @ImporteDescuento money
	DECLARE @Desde date
	DECLARE @Hasta date
	DECLARE @Cantidad int
	DECLARE @Operacion varchar(10)='DEL'

	-- TODO: Set parameter values here.
	Declare cCFT cursor for
		select 
		IDProducto,
		IDListaPrecio,
		IDCliente,
		IDTipoDescuento,
		Descuento,
		Desde,
		Hasta,
		CantidadLimite
		from
		productolistaprecioexcepciones 
		where idtransaccionpedido = 0 
		and (Case when @IDCliente = 0 then 0 else IDCliente end) = (Case when @IDCliente = 0 then 0 else @IDCliente end)
		and (Case when @IDProducto = 0 then 0 else IDProducto end) = (Case when @IDProducto = 0 then 0 else @IDProducto end)
		
		Open cCFT
		fetch next from cCFT into @IDProducto,@IDListaPrecio,@IDCliente,@IDTipoDescuento,@ImporteDescuento,@Desde,@Hasta,@Cantidad
		
		While @@FETCH_STATUS = 0 Begin  
			EXECUTE [SpProductoListaPrecioExcepciones] 
				@IDProducto
				,@IDListaPrecio
				,@IDCliente
				,@IDTipoDescuento
				,@ImporteDescuento
				,@Desde
				,@Hasta
				,@Cantidad
				,@Operacion
				,@IDUsuario
				,@IDSucursal
				,@IDDeposito
				,@IDTerminal

		fetch next from cCFT into @IDProducto,@IDListaPrecio,@IDCliente,@IDTipoDescuento,@ImporteDescuento,@Desde,@Hasta,@Cantidad
			
		End
		
		close cCFT
		deallocate cCFT
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
	
End


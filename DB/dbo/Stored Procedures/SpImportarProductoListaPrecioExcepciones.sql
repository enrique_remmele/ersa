﻿CREATE Procedure [dbo].[SpImportarProductoListaPrecioExcepciones]

	--Entrada
	@Producto varchar(50) = NULL,
	@ListaPrecio varchar(100) = NULL,
	@Cliente varchar(50) = NULL,
	@Tipo varchar(50) = '---',
	@Porcentaje varchar(50),
	@Desde date = NULL, 
	@Hasta date = NULL,	
	
	@IDMoneda tinyint = 1,
	@IDSucursal tinyint = 1,
	
	@Actualizar bit
	
As

Begin

	Declare @vOperacion varchar(50)
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	Declare @vIDListaPrecio int
	Declare @vIDProducto int
	Declare @vIDCliente int
	Declare @vIDTipoDescuento tinyint	
	Declare @vIDDeposito tinyint
	
	Set @vOperacion = 'INS'
	
	Set @Porcentaje = '0' + @Porcentaje
	Set @Porcentaje = REPLACE(@Porcentaje, ',','.')
	
	--Obtener codigos
	Begin
		Set @vIDProducto = (Select IsNull((Select Top(1) ID From Producto Where Referencia=@Producto), 0))
		Set @vIDCliente = (Select IsNull((Select Top(1) ID From Cliente Where Referencia=@Cliente), 0))
		Exec @vIDListaPrecio = SpListaPrecioInsUpd @ListaPrecio, @IDSucursal
	End
		
	--Si no existe el producto, salir
	If @vIDProducto = 0 Begin
		Return @@rowcount
	End
	
	--Si no existe el cliente, salir
	If @vIDCliente = 0 Begin
		Return @@rowcount
	End
	
	If Exists(Select * From ProductoListaPrecioExcepciones Where IDProducto=@vIDProducto And IDListaPrecio=@vIDListaPrecio And IDCliente=@vIDCliente And IDSucursal=@IDSucursal And IDMoneda=@IDMoneda) And @Actualizar='True' Begin
		Set @vOperacion = 'UPD'
	End 
	
	Set @vIDTipoDescuento = (Select IsNull((Select ID From TipoDescuento Where Codigo=@Tipo), 0))
	
	--Si no se encuentra el descuento
	If @vIDTipoDescuento = 0 Begin
		Return @@rowcount
	End

	Set @vIDDeposito = (Select Top(1) ID From Deposito Where IDSucursal=@IDSucursal)
	
	--Verificar que exista en la lista de precio la configuracion
	If Not Exists(Select * From ProductoListaPrecio Where IDListaPrecio=@vIDListaPrecio And IDProducto=@vIDProducto and IDMoneda=@IDMoneda And IDSucursal=@IDSucursal) Begin
		Return @@rowcount
	End
	
	
	EXEC SpProductoListaPrecioExcepciones
		@IDProducto = @vIDProducto,
		@IDListaPrecio = @vIDListaPrecio,
		@IDCliente = @vIDCliente,
		@IDTipoDescuento = @vIDTipoDescuento,
		@Porcentaje = @Porcentaje,
		@Desde = @Desde,
		@Hasta = @Hasta,
		@Operacion = @vOperacion,
		@IDUsuario = 1,
		@IDSucursal = @IDSucursal,
		@IDDeposito=@vIDDeposito,
		@IDTerminal=1,
		@Mensaje = @vMensaje OUTPUT,
		@Procesado = @vProcesado OUTPUT	

End


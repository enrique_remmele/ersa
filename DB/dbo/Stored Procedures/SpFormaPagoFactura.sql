﻿CREATE Procedure [dbo].[SpFormaPagoFactura]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Referencia varchar(16),
	@Contado bit = 'False',
	@Credito bit = 'False',
	@CancelarAutomatico bit,
	@CuentaContable varchar(50),
	@Estado bit,
	@Aprobacion bit = 'True',
	@FacturacionSinPedido bit = 'False',
	@Condicion varchar(20) = '',
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	Declare @IDCuentaContable Int
	
	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
	  
		set @IDCuentaContable = (select ID from  VCuentaContable where Codigo = @CuentaContable)
		
		--Si es que la descripcion ya existe
		if exists(Select * From FormaPagoFactura Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDFormaPagoFactura tinyint
		set @vIDFormaPagoFactura = (Select IsNull((Max(ID)+1), 1) From FormaPagoFactura)

		--Insertamos
		Insert Into FormaPagoFactura(ID, Descripcion, Referencia, Contado,Credito,CancelarAutomatico,IDCuentaContable, Estado, Aprobacion, FacturacionSinPedido, Condicion)
		Values(@vIDFormaPagoFactura, @Descripcion, @Referencia, @Contado,@Credito,@CancelarAutomatico,@IDCuentaContable, @Estado, @Aprobacion, @FacturacionSinPedido, @Condicion)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='FORMAPAGOFACTURA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		 set @IDCuentaContable = (select Id from  CuentaContable where Codigo = @CuentaContable)
		--Si el ID existe
		if not exists(Select * From FormaPagoFactura Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From FormaPagoFactura Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update FormaPagoFactura Set	Descripcion=@Descripcion,
							Referencia=@Referencia,
							Contado = @Contado,
							Credito = @Credito,
							CancelarAutomatico = @CancelarAutomatico,
							IDCuentaContable = @IDCuentaContable,
							Estado = @Estado,
							Aprobacion=@Aprobacion,
							FacturacionSinPedido=@FacturacionSinPedido,
							Condicion = @Condicion
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='FORMAPAGOFACTURA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From FormaPagoFactura Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene alguna relacion con Pedidos
		if exists(Select * From Pedido Where IDFormaPagoFactura=@ID) begin
			set @Mensaje = 'El registro tiene pedidos asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene alguna relacion con VENTAS
		if exists(Select * From Venta Where IDFormaPagoFactura=@ID) begin
			set @Mensaje = 'El registro tiene ventas asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From FormaPagoFactura 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='FORMAPAGOFACTURA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


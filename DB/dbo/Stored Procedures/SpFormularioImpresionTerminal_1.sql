﻿CREATE Procedure [dbo].[SpFormularioImpresionTerminal]

	--Entrada
	@IDTerminalConfiguracion int,
	@IDFormularioImpresion smallint,
	@Impresora varchar(50),
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
						
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From FormularioImpresionTerminal Where IDTerminal=@IDTerminalConfiguracion And IDFormularioImpresion=@IDFormularioImpresion) begin
			set @Mensaje = 'La configuracion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Insertamos
		Insert Into FormularioImpresionTerminal(IDTerminal, IDFormularioImpresion, Impresora)
		Values(@IDTerminalConfiguracion, @IDFormularioImpresion, @Impresora)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CONFIGURACION DE IMPRESORA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@IDTerminalConfiguracion
	
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From FormularioImpresionTerminal Where IDTerminal=@IDTerminalConfiguracion) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Actualizamos
		Update FormularioImpresionTerminal Set Impresora=@Impresora
		Where IDTerminal=@IDTerminalConfiguracion
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CONFIGURACION DE IMPRESORA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@IDTerminalConfiguracion
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From FormularioImpresionTerminal Where IDTerminal=@IDTerminalConfiguracion And IDFormularioImpresion=@IDFormularioImpresion) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From FormularioImpresionTerminal 
		Where IDTerminal=@IDTerminalConfiguracion And IDFormularioImpresion=@IDFormularioImpresion
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CONFIGURACION DE IMPRESORA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@IDTerminalConfiguracion
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿CREATE Procedure [dbo].[SpCFOrdenPago]

	--Cuenta Fija
	@IDOperacion tinyint,
	@ID tinyint,
	@IDSucursal tinyint,
	@IDMoneda tinyint,
	@CuentaContable varchar(50),
	@Debe bit,
	@Haber bit,
	@Descripcion varchar(50),
	@Orden tinyint,
	@IDTipoComprobante int = NULL,
	
	--Cuenta Fija
	@Proveedor bit,
	@Efectivo bit,
	@Cheque bit,
	@Retencion bit,
	@DiferenciaCambio bit,
	@Documento bit,
	@ChequeDiferido bit,
	@SoloDiferido bit = False,
	@BuscarEnCuentaBancaria bit,
	@BuscarEnProveedores bit,
		
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
	
As

Begin

	--BLOQUES
	
	Declare	@vID tinyint
	
	--INSERTAR
	if @Operacion='INS' begin
			
		EXEC SpCF @ID = 0,
				@IDOperacion = @IDOperacion,
				@IDSucursal = @IDSucursal,
				@IDMoneda = @IDMoneda,
				@CuentaContable = @CuentaContable,
				@Debe = @Debe,
				@Haber = @Haber,
				@Descripcion = @Descripcion,
				@Orden = @Orden,
				@Operacion = @Operacion,
				@IDTipoComprobante = @IDTipoComprobante,
				@vID = @vID OUTPUT
			
		Insert Into CFOrdenPago(IDOperacion, ID, BuscarEncuentaBancaria, Efectivo, Cheque, Proveedor, Retencion, DiferenciaCambio, Documento, BuscarEnProveedores, ChequeDiferido, SoloDiferido)
		Values(@IDOperacion, @vID, @BuscarEncuentaBancaria, @Efectivo, @Cheque, @Proveedor, @Retencion, @DiferenciaCambio, @Documento, @BuscarEnProveedores, @ChequeDiferido, @SoloDiferido)
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		EXEC SpCF @ID = @ID,
				@IDOperacion = @IDOperacion,
				@IDSucursal = @IDSucursal,
				@IDMoneda = @IDMoneda,
				@CuentaContable = @CuentaContable,
				@Debe = @Debe,
				@Haber = @Haber,
				@Descripcion = @Descripcion,
				@Orden = @Orden,
				@Operacion = @Operacion,
				@IDTipoComprobante = @IDTipoComprobante,
				@vID = @vID OUTPUT
		
		Update CFOrdenPago Set BuscarEncuentaBancaria=@BuscarEncuentaBancaria,
								BuscarEnProveedores=@BuscarEnProveedores,
								Proveedor=@Proveedor,
								Efectivo=@Efectivo,
								Cheque=@Cheque,
								Retencion=@Retencion,
								DiferenciaCambio=@DiferenciaCambio,
								Documento=@Documento,
								ChequeDiferido = @ChequeDiferido,
								SoloDiferido = @SoloDiferido
		Where ID=@ID And IDOperacion=@IDOperacion
				
		set @Mensaje = 'Registro guardado...!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		Delete From CFOrdenPago Where IDOperacion=@IDOperacion And ID=@ID
		Delete From CF Where IDOperacion=@IDOperacion And ID=@ID
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount
			
End


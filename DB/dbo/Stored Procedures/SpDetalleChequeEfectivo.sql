﻿CREATE Procedure [dbo].[SpDetalleChequeEfectivo]

	--Entrada
	@IDTransaccionCheque numeric(18,0) = NULL,
	@IDTransaccionEfectivo numeric(18,0) = NULL,
	@Importe money = NULL,
	
	--Operacion
	@Operacion varchar(50),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
		
		--Insertar en 
		Insert Into DetalleChequeEfectivo(IDTransaccionCheque,IDTransaccionEfectivo,Importe)
		Values(@IDTransaccionCheque,@IDTransaccionEfectivo,@Importe)
		
										
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
		
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End





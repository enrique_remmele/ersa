﻿
CREATE Procedure [dbo].[SpPuntoExpedicion]

	--Entrada
	@ID smallint,
	@IDTipoComprobante smallint,
	@IDSucursal tinyint,
	@Referencia varchar(5),
	@Descripcion varchar(50),
	@Timbrado varchar(50),
	@Vencimiento date,
	@NumeracionDesde numeric(18,0),
	@NumeracionHasta numeric(18,0),
	@CantidadPorTalonario smallint,
	@VentaDirecta bit= 'False',
	@IDDepositoVentaDirecta tinyint=0,
	@IDDeposito tinyint=0,
	@Estado bit = 'True',
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	if @VentaDirecta = 'False' begin
		set @IDDepositoVentaDirecta = 0
	end
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Obtenemos el nuevo ID
		declare @vID int
		set @vID = (Select IsNull((Max(ID)+1), 1) From PuntoExpedicion)
		
		--Insertamos
		Insert Into PuntoExpedicion(ID, IDTipoComprobante, IDSucursal, Referencia, Descripcion, Timbrado, Vencimiento, NumeracionDesde, NumeracionHasta, CantidadPorTalonario, Estado, VentaDirecta, IDDepositoVentaDirecta)
		Values(@vID, @IDTipoComprobante, @IDSucursal, @Referencia, @Descripcion, @Timbrado, @Vencimiento, @NumeracionDesde, @NumeracionHasta, @CantidadPorTalonario, @Estado, @VentaDirecta, @IDDepositoVentaDirecta)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PUNTO DE EXPEDICION', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From PuntoExpedicion Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update PuntoExpedicion Set	IDTipoComprobante=@IDTipoComprobante,
									IDSucursal=@IDSucursal,
									Referencia=@Referencia,
									Descripcion=@Descripcion,
									Timbrado=@Timbrado,
									Vencimiento=@Vencimiento,
									NumeracionDesde=@NumeracionDesde,
									NumeracionHasta=@NumeracionHasta,
									CantidadPorTalonario=@CantidadPorTalonario,
									Estado=@Estado,
									IDDepositoVentaDirecta = @IDDepositoVentaDirecta,
									VentaDirecta = @VentaDirecta

									
		Where ID=@ID
		
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PUNTO DE EXPEDICION', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From PuntoExpedicion Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Relacion con terminal transaccion
		if exists(Select * From TerminalPuntoExpedicion Where IDPuntoExpedicion=@ID) begin
			set @Mensaje = 'El registro tiene terminales relacionadas! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From PuntoExpedicion
		Where ID=@ID
		
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PUNTO DE EXPEDICION', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End





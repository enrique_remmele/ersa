﻿
CREATE Procedure [dbo].[SpLinea]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Estado bit,
	@Operacion varchar(10),

	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Linea Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDLinea tinyint
		set @vIDLinea = (Select IsNull((Max(ID)+1), 1) From Linea)

		--Insertamos
		Insert Into Linea(ID, Descripcion, Estado)
		Values(@vIDLinea, @Descripcion, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='LINEA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From Linea Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Linea Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Linea Set Descripcion=@Descripcion, 
						Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='LINEA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Linea Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Producto
		if exists(Select * From Producto Where IDLinea=@ID) begin
			set @Mensaje = 'El registro tiene productos asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Si tiene una relacion con tipoproducto
		if exists(Select * from TipoProductoLinea where IDLinea = @ID) begin
			set @Mensaje = 'El registro tiene clasificaciones padre asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Linea 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='LINEA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿	CREATE Procedure  [dbo].[SpMovimientosOperativosContraContables]
			
		@vCodigo as varchar(50)
		,@vMes as int
		,@vAño as int

	as 
	begin
	
		--Crear la tabla temporal
    create table #TablaTemporal(Origen varchar(50),
								Operacion varchar(50),
								IDTipoComprobante int,
								TipoComprobante varchar(50),
								Entradas money,
								Salidas money,
								Orden int)

		Declare @vTipoComprobante varchar(50)
		Declare @vIDTipoComprobante int
		Declare @vDescripcionTipoComprobante varchar(50)
		Declare @vOperacion varchar(50)
		Declare @vCredito money
		Declare @vDebito money
		
		begin -- Movimientos
		Declare cMovimientos cursor for
		select 
		DA.Operacion,
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante,
		'Credito'=Sum(DA.Credito),
		'Debito'=Sum(DA.Debito)
		from VDetalleAsiento DA
		Join vMovimiento M on DA.IDTransaccion = M.IDTransaccion
		where Year(DA.fecha)=@vAño
		and month(DA.fecha)=@vMes
		and DA.Operacion in('MOVIMIENTOS','DESCARGASTOCK','MOVIMIENTO MATERIA PRIMA','MOVIMIENTO DESCARGA DE COMPRA','CONSUMO COMBUSTIBLE')
		--and DA.Codigo = @vCodigo
		Group by
		DA.Operacion,
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante
		
		Open cMovimientos   
		fetch next from cMovimientos into @vOperacion,@vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
		
		While @@FETCH_STATUS = 0 Begin  
		
			Insert Into #TablaTemporal(Origen,Operacion,IDTipoComprobante,TipoComprobante,Entradas,Salidas, Orden)
			select 
			'Origen'='CONTABILIDAD',
			'Operacion'=@vOperacion,
			M.IDTipoComprobante,
			M.DescripcionTipoComprobante,
			'Debito'=Sum(DA.Debito),
			'Credito'=Sum(DA.Credito),
			1
			
			from VDetalleAsiento DA
			Join vMovimiento M on DA.IDTransaccion = M.IDTransaccion
			where Year(DA.fecha)=@vAño
			and month(DA.fecha)=@vMes
			and DA.Codigo = @vCodigo
			and M.IDTipoComprobante=@vIDTipoComprobante
			Group by
			M.TipoComprobante,
			M.IDTipoComprobante,
			M.DescripcionTipoComprobante

			Insert Into #TablaTemporal(Origen,Operacion,IDTipoComprobante,TipoComprobante,Entradas,Salidas, Orden)
			Select 
			'Origen'='OPERATIVO', 
			'Operacion'=@vOperacion,
			IDTipoComprobante,
			@vDescripcionTipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			1
			from
			(Select 
			'TotalSalida' = sum(Case when CuentaContableSalida = @vCodigo then TotalDetalleSalida else 0 end), 
			'TotalEntrada' = sum(Case when CuentaContableEntrada = @vCodigo then TotalDetalleEntrada else 0 end),
			IDTipoComprobante
			From VDetalleMovimiento 
			Where year(Fecha)=@vAño
			and MONTH(fecha)=@vMes
			And (CuentaContableEntrada = @vCodigo or CuentaContableSalida = @vCodigo)
			And (IDTipoComprobante=@vIDTipoComprobante)  
			And Anulado = 0
			group by 
			DepositoSalida, 
			DepositoEntrada,
			IDTipoComprobante) as c
			group by 
			IDTipoComprobante

		fetch next from cMovimientos into @vOperacion,@vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
			
		End
		
		close cMovimientos 
		deallocate cMovimientos
		end

		begin -- Ventas
		Declare cVenta cursor for
		select 
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante,
		'Credito'=Sum(DA.Credito),
		'Debito'=Sum(DA.Debito)
		from VDetalleAsiento DA
		Join VVenta M on DA.IDTransaccion = M.IDTransaccion
		where Year(DA.fecha)=@vAño
		and month(DA.fecha)=@vMes
		and DA.Operacion = 'VENTAS CLIENTES'
		--and DA.Codigo = @vCodigo
		Group by
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante

		Open cVenta   
		fetch next from cVenta into @vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
		
		While @@FETCH_STATUS = 0 Begin  
		
			Insert Into #TablaTemporal(Origen,Operacion,IDTipoComprobante,TipoComprobante,Entradas,Salidas, Orden)
			select 
			'Origen'='CONTABILIDAD',
			'Operacion'='VENTAS CLIENTES',
			M.IDTipoComprobante,
			M.DescripcionTipoComprobante,
			'Debito'=Sum(DA.Debito),
			'Credito'=Sum(DA.Credito),
			2
			
			from VDetalleAsiento DA
			Join VVenta M on DA.IDTransaccion = M.IDTransaccion
			where Year(DA.fecha)=@vAño
			and month(DA.fecha)=@vMes
			and DA.Codigo = @vCodigo
			and M.IDTipoComprobante=@vIDTipoComprobante
			Group by
			M.TipoComprobante,
			M.IDTipoComprobante,
			M.DescripcionTipoComprobante

			Insert Into #TablaTemporal(Origen,Operacion,IDTipoComprobante,TipoComprobante,Entradas,Salidas, Orden)
			Select 
			'Origen'='OPERATIVO', 
			'Operacion'='VENTAS CLIENTES',
			IDTipoComprobante,
			@vDescripcionTipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			2
			from
			(Select 
			'TotalSalida' = sum(Case when DV.CuentaContableDeposito = @vCodigo then DV.TotalCosto else 0 end), 
			'TotalEntrada' =0,
			DV.IDTipoComprobante
			From VDetalleVenta DV
			Where year(DV.Fecha)=@vAño
			and MONTH(DV.fecha)=@vMes
			and DV.ControlarExistencia = 'True'
			And DV.CuentaContableDeposito = @vCodigo 
			And DV.IDTipoComprobante=@vIDTipoComprobante
			And DV.Anulado = 0
			group by 
			DV.IDTipoComprobante) as c
			group by 
			IDTipoComprobante


		fetch next from cVenta into @vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
			
		End
		
		close cVenta 
		deallocate cVenta
		end

		begin -- nota credito
		Declare cNotaCredito cursor for
		select 
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante,
		'Credito'=Sum(DA.Credito),
		'Debito'=Sum(DA.Debito)
		from VDetalleAsiento DA
		Join VNotaCredito M on DA.IDTransaccion = M.IDTransaccion
		where Year(DA.fecha)=@vAño
		and month(DA.fecha)=@vMes
		and DA.Operacion = 'NOTA CREDITO'
		--and DA.Codigo = @vCodigo
		Group by
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante

		Open cNotaCredito   
		fetch next from cNotaCredito into @vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
		
		While @@FETCH_STATUS = 0 Begin  
		
			Insert Into #TablaTemporal(Origen,Operacion,IDTipoComprobante,TipoComprobante,Entradas,Salidas, Orden)
			select 
			'Origen'='CONTABILIDAD',
			'Operacion'='NOTA CREDITO',
			M.IDTipoComprobante,
			M.DescripcionTipoComprobante,
			'Debito'=Sum(DA.Debito),
			'Credito'=Sum(DA.Credito),
			3
			
			from VDetalleAsiento DA
			Join VNotaCredito M on DA.IDTransaccion = M.IDTransaccion
			where Year(DA.fecha)=@vAño
			and month(DA.fecha)=@vMes
			and DA.Codigo = @vCodigo
			and M.IDTipoComprobante=@vIDTipoComprobante
			Group by
			M.TipoComprobante,
			M.IDTipoComprobante,
			M.DescripcionTipoComprobante

			Insert Into #TablaTemporal(Origen,Operacion,IDTipoComprobante,TipoComprobante,Entradas,Salidas, Orden)
			Select 
			'Origen'='OPERATIVO', 
			'Operacion'='NOTA CREDITO',
			IDTipoComprobante,
			@vDescripcionTipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			3
			from
			(Select 
			'TotalSalida' = 0,
			'TotalEntrada' =sum(Case when DV.CuentaContableDeposito = @vCodigo then DV.TotalCosto else 0 end),
			DV.IDTipoComprobante
			From VDetalleNotaCredito DV
			Where year(DV.Fecha)=@vAño
			and MONTH(DV.fecha)=@vMes
			and DV.ControlarExistencia = 'True'
			--And IDTipoProducto In (0, 2, 1, 6) 
			And DV.CuentaContableDeposito = @vCodigo 
			And DV.IDTipoComprobante=@vIDTipoComprobante
			And DV.Anulado = 0
			group by 
			DV.IDTipoComprobante) as c
			group by 
			IDTipoComprobante


		fetch next from cNotaCredito into @vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
			
		End
		
		close cNotaCredito 
		deallocate cNotaCredito
		end

		begin -- compras

		Declare cCompras cursor for
		select 
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante,
		'Credito'=Sum(DA.Credito),
		'Debito'=Sum(DA.Debito)
		from VDetalleAsiento DA
		Join VCompra M on DA.IDTransaccion = M.IDTransaccion
		where Year(DA.fecha)=@vAño
		and month(DA.fecha)=@vMes
		and DA.Operacion = 'COMPRA DE MERCADERIA'
		--and DA.Codigo = @vCodigo
		Group by
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante

		Open cCompras   
		fetch next from cCompras into @vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
		
		While @@FETCH_STATUS = 0 Begin  
		
			Insert Into #TablaTemporal(Origen,Operacion,IDTipoComprobante,TipoComprobante,Entradas,Salidas, Orden)
			select 
			'Origen'='CONTABILIDAD',
			'Operacion'='COMPRA DE MERCADERIA',
			M.IDTipoComprobante,
			M.DescripcionTipoComprobante,
			'Debito'=Sum(DA.Debito),
			'Credito'=Sum(DA.Credito),
			4
			
			from VDetalleAsiento DA
			Join VCompra M on DA.IDTransaccion = M.IDTransaccion
			where Year(DA.fecha)=@vAño
			and month(DA.fecha)=@vMes
			and DA.Codigo = @vCodigo
			and M.IDTipoComprobante=@vIDTipoComprobante
			Group by
			M.TipoComprobante,
			M.IDTipoComprobante,
			M.DescripcionTipoComprobante

			Insert Into #TablaTemporal(Origen,Operacion,IDTipoComprobante,TipoComprobante,Entradas,Salidas, Orden)
			Select 
			'Origen'='OPERATIVO', 
			'Operacion'='COMPRA DE MERCADERIA',
			IDTipoComprobante,
			@vDescripcionTipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			4
			from
			(Select 
			'TotalSalida' = 0, 
			'TotalEntrada' =sum(Case when DV.CuentaContableDeposito = @vCodigo then DV.TotalDiscriminado * isnull(DV.Cotizacion,1) else 0 end),
			DV.IDTipoComprobante
			From VDetalleCompra DV
			Where year(DV.Fecha)=@vAño
			and MONTH(DV.fecha)=@vMes
			and DV.ControlarExistencia = 'True'
			--And IDTipoProducto In (0, 2, 1, 6) 
			And DV.CuentaContableDeposito = @vCodigo 
			And DV.IDTipoComprobante=@vIDTipoComprobante
			group by 
			DV.IDTipoComprobante) as c
			group by 
			IDTipoComprobante


		fetch next from cCompras into @vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
			
		End
		
		close cCompras 
		deallocate cCompras
		end

		begin -- nota credito proveedor
		Declare cNotaCreditoProveedor cursor for
		select 
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante,
		'Credito'=Sum(DA.Credito),
		'Debito'=Sum(DA.Debito)
		from VDetalleAsiento DA
		Join VNotaCreditoProveedor M on DA.IDTransaccion = M.IDTransaccion
		where Year(DA.fecha)=@vAño
		and month(DA.fecha)=@vMes
		and DA.Operacion = 'NOTA CREDITO PROVEEDOR'
		--and DA.Codigo = @vCodigo
		Group by
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.DescripcionTipoComprobante

		Open cNotaCreditoProveedor   
		fetch next from cNotaCreditoProveedor into @vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
		
		While @@FETCH_STATUS = 0 Begin  
		
			Insert Into #TablaTemporal(Origen,Operacion,IDTipoComprobante,TipoComprobante,Entradas,Salidas, Orden)
			select 
			'Origen'='CONTABILIDAD',
			'Operacion'='NOTA CREDITO PROVEEDOR',
			M.IDTipoComprobante,
			M.DescripcionTipoComprobante,
			'Debito'=Sum(DA.Debito),
			'Credito'=Sum(DA.Credito),
			5
			from VDetalleAsiento DA
			Join VNotaCreditoProveedor M on DA.IDTransaccion = M.IDTransaccion
			where Year(DA.fecha)=@vAño
			and month(DA.fecha)=@vMes
			and DA.Codigo = @vCodigo
			and M.IDTipoComprobante=@vIDTipoComprobante
			Group by
			M.TipoComprobante,
			M.IDTipoComprobante,
			M.DescripcionTipoComprobante

			Insert Into #TablaTemporal(Origen,Operacion,IDTipoComprobante,TipoComprobante,Entradas,Salidas, Orden)
			Select 
			'Origen'='OPERATIVO', 
			'Operacion'='NOTA CREDITO PROVEEDOR',
			IDTipoComprobante,
			@vDescripcionTipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			5
			from
			(Select 
			'TotalSalida' = sum(Case when DV.CuentaContableDeposito = @vCodigo then DV.TotalDiscriminado else 0 end),
			'TotalEntrada' =0,
			DV.IDTipoComprobante
			From VDetalleNotaCreditoProveedor DV
			Where year(DV.Fecha)=@vAño
			and MONTH(DV.fecha)=@vMes
			and DV.ControlarExistencia = 'True'
			--And IDTipoProducto In (0, 2, 1, 6) 
			And DV.CuentaContableDeposito = @vCodigo 
			And DV.IDTipoComprobante=@vIDTipoComprobante
			And DV.Anulado = 0
			group by 
			DV.IDTipoComprobante) as c
			group by 
			IDTipoComprobante


		fetch next from cNotaCreditoProveedor into @vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
			
		End
		
		close cNotaCreditoProveedor 
		deallocate cNotaCreditoProveedor
		end


		begin --ticket bascula
		Declare cTicket cursor for
		select 
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.TipoComprobante,
		'Credito'=Sum(DA.Credito),
		'Debito'=Sum(DA.Debito)
		from VDetalleAsiento DA
		Join VTicketBascula M on DA.IDTransaccion = M.IDTransaccion
		where Year(DA.fecha)=@vAño
		and month(DA.fecha)=@vMes
		and DA.Operacion = 'TICKET DE BASCULA'
		--and DA.Codigo = @vCodigo
		Group by
		M.TipoComprobante,
		M.IDTipoComprobante,
		M.TipoComprobante

		Open cTicket   
		fetch next from cTicket into @vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
		
		While @@FETCH_STATUS = 0 Begin  
		
			Insert Into #TablaTemporal(Origen,Operacion,IDTipoComprobante,TipoComprobante,Entradas,Salidas, Orden)
			select 
			'Origen'='CONTABILIDAD',
			'Operacion'='TICKET DE BASCULA',
			M.IDTipoComprobante,
			M.TipoComprobante,
			'Debito'=Sum(DA.Debito),
			'Credito'=Sum(DA.Credito),
			6
			
			from VDetalleAsiento DA
			Join VTicketBascula M on DA.IDTransaccion = M.IDTransaccion
			where Year(DA.fecha)=@vAño
			and month(DA.fecha)=@vMes
			and DA.Codigo = @vCodigo
			and M.IDTipoComprobante=@vIDTipoComprobante
			Group by
			M.TipoComprobante,
			M.IDTipoComprobante,
			M.TipoComprobante

			Insert Into #TablaTemporal(Origen,Operacion,IDTipoComprobante,TipoComprobante,Entradas,Salidas, Orden)
			Select 
			'Origen'='OPERATIVO', 
			'Operacion'='TICKET DE BASCULA',
			IDTipoComprobante,
			@vDescripcionTipoComprobante,
			Sum(TotalEntrada),
			Sum(TotalSalida),
			6
			from
			(Select 
			'TotalSalida' = 0, 
			'TotalEntrada' =sum(Case when DV.CuentaContableDeposito = @vCodigo then DV.TotalDiscriminado else 0 end),
			DV.IDTipoComprobante
			From VTicketBascula DV
			Where year(DV.Fecha)=@vAño
			and MONTH(DV.fecha)=@vMes
			--and DV.ControlarExistencia = 'True'
			--And IDTipoProducto In (0, 2, 1, 6) 
			And DV.CuentaContableDeposito = @vCodigo 
			And DV.IDTipoComprobante=@vIDTipoComprobante
			group by 
			DV.IDTipoComprobante) as c
			group by 
			IDTipoComprobante


		fetch next from cTicket into @vTipoComprobante, @vIDTipoComprobante, @vDescripcionTipoComprobante,@vCredito, @vDebito
			
		End
		
		close cTicket
		deallocate cTicket
		end

		----Otros

		Declare cOtros cursor for
		
		Select distinct
		Operacion 
		from vAsiento
		Where year(fecha)=@vAño
		and month(fecha)=@vMes
		and Operacion not in('MOVIMIENTOS','DESCARGASTOCK','MOVIMIENTO MATERIA PRIMA','MOVIMIENTO DESCARGA DE COMPRA','CONSUMO COMBUSTIBLE','VENTAS CLIENTES','NOTA CREDITO','COMPRA DE MERCADERIA','NOTA CREDITO PROVEEDOR','TICKET DE BASCULA')
		
		Open cOtros   
		fetch next from cOtros into @vOperacion
		
		While @@FETCH_STATUS = 0 Begin  
		
			Insert Into #TablaTemporal(Origen,Operacion,IDTipoComprobante,TipoComprobante,Entradas,Salidas, Orden)
			select 
			'Origen'=@vOperacion,
			'Operacion'='- OTROS',
			0,
			'OTROS',
			'Debito'=ISNULL((select Sum(DA.Debito) from VDetalleAsiento DA 
										where Year(DA.fecha)=@vAño
										and month(DA.fecha)=@vMes
										and DA.Codigo = @vCodigo
										and DA.Operacion=@vOperacion),0),
			'Credito'=ISNULL((Select Sum(DA.Credito) from VDetalleAsiento DA 
										where Year(DA.fecha)=@vAño
										and month(DA.fecha)=@vMes
										and DA.Codigo = @vCodigo
										and DA.Operacion=@vOperacion),0),
			7
			
			from  Operacion O
			where O.Descripcion=@vOperacion

		fetch next from cOtros into @vOperacion
			
		End
		
		close cOtros 
		deallocate cOtros





		Select * from #TablaTemporal ORDER BY Orden ASC

	end

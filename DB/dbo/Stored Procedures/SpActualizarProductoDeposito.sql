﻿
CREATE Procedure [dbo].[SpActualizarProductoDeposito]

	--Entrada
	@IDDeposito tinyint,
	@IDProducto int,
	@Cantidad decimal(10,2),
	@Signo char(1),
	@ControlStock bit = 'False',
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output	
	
As

Begin

	Declare @vVentaPermitirCantidadNegativa bit
	Declare @vIDSucursal tinyint

	Set @vIDSucursal = (Select IDSucursal From Deposito Where ID=@IDDeposito)
	Set @vVentaPermitirCantidadNegativa = ISNull((Select Top(1) VentaPermitirCantidadNegativa From Configuraciones Where IDSucursal=@vIDSucursal), 'False')

	--Si no existe el registro, crearlo.
	If Not Exists(Select * From ExistenciaDeposito Where IDDeposito=@IDDeposito And IDProducto=@IDProducto) Begin
		
		Insert Into ExistenciaDeposito(IDDeposito, IDProducto, Existencia, ExistenciaMinima, ExistenciaCritica, PlazoReposicion)
		Values(@IDDeposito, @IDProducto, 0, 0, 0, 0)
		
	End
	
	--Obtenemos la cantidad nueva
	Declare @vCantidadNueva decimal(10,2)
	Declare @vCantidadActual decimal (10,2)
	
	Set @vCantidadActual = dbo.FExistenciaProductoReal(@IDProducto, @IDDeposito)
	--print 'actualiza producto'
	--Verificar la Cantidad
	--If @Signo='-' Begin
	If @Signo like '%-%' Begin
	
		Set @vCantidadNueva = @vCantidadActual - @Cantidad
		
		If @vVentaPermitirCantidadNegativa = 0 Begin

			If @vCantidadNueva < 0 And @ControlStock= 'True' Begin
				Set @Mensaje = 'Stock insuficiente en deposito para el producto: ' + CONVERT(varchar(50), @IDProducto) + ' '
				Set @Procesado = 'False'
				--print '[SpActualizarProductoDeposito] Stock insuficiente en deposito para el producto: ' + CONVERT(varchar(50), @IDProducto) + ' '
				return @@rowcount
			End

		End

	End Else Begin
		
		Set @vCantidadNueva = @Cantidad + @vCantidadActual
		
	End
	
	--Actualizar el Deposito
	Update ExistenciaDeposito Set Existencia = @vCantidadNueva
	Where IDDeposito = @IDDeposito And IDProducto = @IDProducto
	
	--Insertar historial
	Declare @vCantidadGuardada decimal(10,2)
	Set @vCantidadGuardada = dbo.FExistenciaProductoReal(@IDProducto, @IDDeposito)
	Insert into ExistenciaDepositoHistorial(IDDeposito, IDProducto,ExistenciaActual,Signo,Cantidad,ExistenciaNueva, CantidadGuardada, Fecha)
	Values (@IDDeposito, @IDProducto, @vCantidadActual, @Signo, @Cantidad, @vCantidadNueva, @vCantidadGuardada, GETDATE())
		
	--Actualizar el Producto
	Declare @vExistenciaGeneral decimal(10,2)
	Set @vExistenciaGeneral = (Select IsNull((Select SUM(Existencia) From ExistenciaDeposito Where IDProducto = @IDProducto), 0))
	
	Update Producto Set ExistenciaGeneral=@vExistenciaGeneral
	Where ID=@IDProducto
		
	Set @Mensaje = 'Registro cargado!'
	Set @Procesado = 'True'
	return @@rowcount

End


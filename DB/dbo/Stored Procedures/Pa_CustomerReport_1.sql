﻿--select * from producto where id =1
--Execute Pa_CustomerReport '1','41001','HARINA 000 PAQ. 1 KILO'
CREATE PROCEDURE Pa_CustomerReport
 @ID varchar(10)=NULL,
 @Referencia varchar(10)=NULL,
 @Descripcion varchar(50)=NULL
As

DECLARE @Seleccion varchar(2000)
DECLARE @Origen varchar(2000)
DECLARE @Condiciones varchar(2000)
DECLARE @Consulta varchar(6000)

Set @Seleccion = 'SELECT ID, Referencia, Descripcion ' + Char(10)
Set @Origen = 'FROM producto ' + Char(10)
Set @Condiciones='WHERE 1=1 ' + Char(10)
IF @ID IS NOT NULL
SET @Condiciones = @Condiciones + ' AND ID = ' + Quotename(@ID,'''')
IF @Referencia IS NOT NULL
SET @Condiciones = @Condiciones +' AND Referencia = ' + Quotename(@Referencia,'''') 
IF @Descripcion IS NOT NULL
SET @Condiciones = @Condiciones +' AND Descripcion = ' + Quotename(@Descripcion,'''') 
SET @Consulta = @Seleccion + @Origen + @Condiciones 

EXEC sp_executesql  @Consulta,
N'@P_ID AS varchar(10),@P_Referencia varchar(10),@P_Descripcion varchar(50)',@ID,@Referencia,@Descripcion;


﻿CREATE Procedure [dbo].[pa_WsPrecioProducto]

	--Entrada
	@IDProducto int,
	@IDCliente int,
	@IDSucursal tinyint,
	@IDDeposito tinyint = 0
	
As

Begin

	--Variables
	Declare @vIDListaPrecio int
	Declare @vExistencia decimal(10,2)
	
	Declare @vFecha date
	
	Set @vFecha = (Select Top(1) VentaFechaFacturacion From Configuraciones Where IDSucursal=@IDSucursal)
		
	--Primero hallamos el Precio			
	--Obtener la lista de precio del cliente
	Set @vIDListaPrecio = (Select IsNull((Select TOP 1 IDListaPrecio From Cliente Where ID=@IDCliente), 0))
	
	--Aprovechamos y sacamos la existencia, para la venta y pedido
	--y asi no consultamos tantas veces a la BD desde la aplicacion
	Set @vExistencia = dbo.FExistenciaProducto(@IDProducto, @IDDeposito)
	
	--ATENCION!!! - Saque la sucursal para poder vender a clientes de otra sucursal	
	--Este es para Precio Normal	
	SELECT LP.Descripcion, ISNULL(PLP2.Precio, ISNULL(PLPE.Descuento,P.Precio)) AS Importe, ISNULL(PLP2.TPRPorcentaje,ISNULL(PLPE.Porcentaje, P.TPRPorcentaje)) AS Porcentaje, @vExistencia AS Existencia
	From ProductoListaPrecio P
	Join ListaPrecio LP On P.IDListaPrecio=LP.ID
	LEFT OUTER JOIN ProductoListaPrecio PLP2 ON LP.ID=PLP2.IDListaPrecio AND @vFecha >= PLP2.TPRDesde OR @vFecha <= PLP2.TPRHasta
	LEFT OUTER JOIN ProductoListaPrecioExcepciones PLPE ON LP.ID=PLPE.IDListaPrecio And PLPE.IDCliente=@IDCliente  AND (@vFecha Between Desde And Hasta) 
	Where P.IDProducto=@IDProducto 
	And P.IDListaPrecio=@vIDListaPrecio

	

	
	--Excepciones TACTICO
	Select PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, PLPE.Descuento, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='True' 
	From ProductoListaPrecioExcepciones PLPE 
	Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
	Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
	Where PLPE.IDProducto=@IDProducto And PLPE.IDListaPrecio=@vIDListaPrecio And PLPE.IDCliente=@IDCliente  
	--And LP.IDSucursal = @IDSucursal 
	And @vFecha between PLPE.Desde And PLPE.Hasta
	And TD.PlanillaTactico = 'True'
	And (Case When Exists(Select * From VDetalleActividad D 
										Join VActividad A On D.IDActividad=A.ID 
										Join DetallePlanillaDescuentoTactico DP On A.ID=DP.IDActividad 
										Join PlanillaDescuentoTactico P On DP.IDTransaccion=P.IDTransaccion 
										Where D.ID=@IDProducto And (@vFecha between P.Desde And P.Hasta) And P.IDSucursal=@IDSucursal) Then 'True' Else 'False' End)='True'


End




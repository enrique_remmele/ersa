﻿CREATE Procedure [dbo].[SpCuentaFijaFormaPagoDocumento]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Orden tinyint = NULL,
	@IDTipoComprobante smallint = NULL,
	@IDMoneda tinyint = NULL,
	@IDCuentaContable smallint = NULL,
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	If @Operacion='INS' Begin
		
		--Si es que la Nombres ya existe
		If Exists(Select * From CuentaFijaFormaPagoDocumento Where Descripcion=@Descripcion) Begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si existe la combinacion
		If Exists(Select * From CuentaFijaFormaPagoDocumento Where IDTipoComprobante=@IDTipoComprobante And IDMoneda=@IDMoneda) Begin
			set @Mensaje = 'La combinacion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Obtenemos el nuevo ID
		declare @vID tinyint
		set @vID = (Select IsNull((Max(ID)+1), 1) From CuentaFijaFormaPagoDocumento)

		--Insertamos
		Insert Into CuentaFijaFormaPagoDocumento(ID, Descripcion, Orden, IDTipoComprobante, IDMoneda, IDCuentaContable)
		Values(@vID, @Descripcion, @Orden, @IDTipoComprobante, @IDMoneda, @IDCuentaContable)		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si es que no existe el registro
		If Not Exists(Select * From CuentaFijaFormaPagoDocumento Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro seleccionado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si es que la Nombres ya existe
		If Exists(Select * From CuentaFijaFormaPagoDocumento Where Descripcion=@Descripcion And ID!=@ID) Begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si existe la combinacion
		If Exists(Select * From CuentaFijaFormaPagoDocumento Where IDTipoComprobante=@IDTipoComprobante And IDMoneda=@IDMoneda And ID!=@ID) Begin
			set @Mensaje = 'La combinacion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
				
		Update CuentaFijaFormaPagoDocumento Set Descripcion=@Descripcion,
												Orden=@Orden,
												IDTipoComprobante=@IDTipoComprobante,
												IDMoneda=@IDMoneda,
												IDCuentaContable=@IDCuentaContable
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si es que no existe el registro
		If Not Exists(Select * From CuentaFijaFormaPagoDocumento Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro seleccionado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		Delete From CuentaFijaFormaPagoDocumento Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End





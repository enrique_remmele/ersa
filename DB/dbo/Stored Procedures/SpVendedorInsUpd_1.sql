﻿CREATE Procedure [dbo].[SpVendedorInsUpd]

	--Entrada
	@Descripcion varchar(100),
	@IDSucursal tinyint = NULL
	
As

Begin

	Declare @vID int
	
	If @Descripcion = '' Begin
		Set @Descripcion = ''
	End
	
	--Si existe
	If Exists(Select * From Vendedor Where Nombres = @Descripcion) Begin
		Set @vID = (Select Top(1) ID From Vendedor Where Nombres = @Descripcion)
		Update Vendedor Set IDSucursal = @IDSucursal
		Where ID=@vID
	End Else Begin
		Set @vID = (Select IsNull(Max(ID)+1,1) From Vendedor)
		Insert Into Vendedor(ID, Nombres, Estado, IDSucursal)
		Values(@vID, @Descripcion, 'True', @IDSucursal)
	End
	
	return @vID
	
End

﻿CREATE Procedure [dbo].[SpBarrioImportar]

	--Entrada
	@Referencia varchar(100),
	@Descripcion varchar(100),
	
	@Actualizar bit = 'True'
	
As

Begin

	Declare @vID int
	
	--Si existe
	If Exists(Select * From Barrio Where Descripcion=@Descripcion) Begin
		If @Actualizar = 'True' Begin
			Update Barrio Set Descripcion=@Descripcion, Referencia=@Referencia
			Where Descripcion=@Descripcion
		End
	End
	
	--Si existe
	If Not Exists(Select * From Barrio Where Descripcion=@Descripcion) Begin
		Set @vID = (Select IsNull(Max(ID)+1,1) From Barrio)
		Insert Into Barrio(ID, Descripcion, Orden, Estado, Referencia)
		Values(@vID, @Descripcion,  0, 'True', @Referencia)
	End
	
End

Select * From Barrio Where Referencia='033'


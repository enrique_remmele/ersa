﻿CREATE Procedure [dbo].[SpFormularioImpresion]

	--Entrada
	@IDFormularioImpresion int = NULL,
	@IDImpresion int = 0,
	@Descripcion varchar(20) = '',
	@Detalle bit = 'False',
	@CantidadDetalle tinyint = 1,
	@Copias tinyint = 1,

	--Papel
	@TamañoPapel varchar(50) = 'A4', 
	@TamañoPapelX smallint = 827, 
	@TamañoPapelY smallint = 1169,
	@Horizontal bit = 'False',
	@MargenIzquierdo tinyint = 0,
	@MargenDerecho tinyint = 0,
	@MargenArriba tinyint = 0,
	@MargenAbajo tinyint = 0,

	@Operacion varchar(10)

As

Begin

	Declare @vIDFormularioImpresion int
	Declare @vMensaje varchar(150)
	Declare @vProcesado bit
	
	Set @vMensaje = ''
	Set @vProcesado = 'False'
	Set @vIDFormularioImpresion = 0
	
	--Insertar
	If @Operacion='INS' Begin
		
		--Validar
		If Exists(Select * From FormularioImpresion Where Descripcion=@Descripcion) Begin
			Set @vMensaje = 'El FormularioImpresion ya existe!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		If Not Exists(Select * From Impresion Where IDImpresion=@IDImpresion) Begin
			Set @vMensaje = 'El sistema no encuentra el tipo de FormularioImpresion! Comunique a Sistemas.'
			Set @vProcesado = 'False'
			GoTo Salir
		End

		Set @vIDFormularioImpresion = (Select IsNull(Max(IDFormularioImpresion)+1, 1) From FormularioImpresion)
		Declare @vDetalle bit = (Select Detalle From Impresion Where IDImpresion=@IDImpresion)

		Insert Into FormularioImpresion(IDFormularioImpresion, IDImpresion, Descripcion, Detalle, CantidadDetalle, Copias, Fondo, TamañoFondoX, TamañoFondoY)
		Values(@vIDFormularioImpresion, @IDImpresion, @Descripcion, @vDetalle, @CantidadDetalle, @Copias, NULL, 0, 0)
		
		Set @vMensaje = 'Registro insertado!'
		Set @vProcesado = 'True'

	End
	
	--Actualizar
	If @Operacion='UPD' Begin
		
		--Validar
		If Exists(Select * From FormularioImpresion Where Descripcion=@Descripcion And IDFormularioImpresion!=@IDFormularioImpresion) Begin
			Set @vMensaje = 'El FormularioImpresion ya existe!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		Update FormularioImpresion Set Descripcion=@Descripcion, 
							Detalle=@Detalle, 
							CantidadDetalle=@CantidadDetalle, 
							Copias=@Copias
		Where IDFormularioImpresion=@IDFormularioImpresion
		
		Set @vMensaje = 'Registro actualizado!'
		Set @vProcesado = 'True'
		
	End
	
	--Eliminar
	If @Operacion='DEL' Begin
		
		--Validar
		Delete From FormularioImpresionCopias 
		Where IDFormularioImpresion=@IDFormularioImpresion
		
		Delete From DetalleFormularioImpresion 
		Where IDFormularioImpresion=@IDFormularioImpresion
		
		Delete From FormularioImpresion 
		Where IDFormularioImpresion=@IDFormularioImpresion
		
		Set @vMensaje = 'Registro eliminado!'
		Set @vProcesado = 'True'

	End
	
	--Actualizar Papel
	If @Operacion='PAPEL' Begin
		
		--Validar
		If Exists(Select * From FormularioImpresion Where Descripcion=@Descripcion And IDFormularioImpresion!=@IDFormularioImpresion) Begin
			Set @vMensaje = 'El FormularioImpresion ya existe!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		Update FormularioImpresion Set	TamañoPapel=@TamañoPapel, 
								TamañoPapelX=@TamañoPapelX, 
								TamañoPapelY=@TamañoPapelY, 
								Horizontal=@Horizontal,
								MargenIzquierdo=@MargenIzquierdo,
								MargenDerecho=@MargenDerecho,
								MargenArriba=@MargenArriba,
								MargenAbajo=@MargenAbajo
		Where IDFormularioImpresion=@IDFormularioImpresion
		
		print 'PAPEL'
		Set @vMensaje = 'Registro actualizado!'
		Set @vProcesado = 'True'
		
	End
	
	If @Operacion='OPCIONES' Begin
		
		--Validar
		If Not Exists(Select * From FormularioImpresion Where IDFormularioImpresion=@IDFormularioImpresion) Begin
			Set @vMensaje = 'El sistema no encuentra el registro!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		Update FormularioImpresion Set	Detalle=@Detalle, 
								CantidadDetalle=@CantidadDetalle, 
								Copias=@Copias								
		Where IDFormularioImpresion=@IDFormularioImpresion
		
		--Generar las copias
		If @Copias>0 Begin
			Declare @vIDCopia int = 1
			While @vIDCopia<=@Copias Begin
				
				--Verificar si ya existe la copia
				If Not Exists(Select * From FormularioImpresionCopia Where IDFormularioImpresion=@IDFormularioImpresion And ID=@vIDCopia) Begin
					Insert into FormularioImpresionCopia(IDFormularioImpresion, ID, PosicionX, PosicionY)
					values(@IDFormularioImpresion, @vIDCopia, 0, 0)
				End
				  
				Set @vIDCopia = @vIDCopia + 1

			End

			--Eliminamos los posteriores
			Delete From FormularioImpresionCopia
			Where IDFormularioImpresion=@IDFormularioImpresion And ID>@Copias

		End Else Begin
			
			--Eliminamos los posteriores
			Delete From FormularioImpresionCopia
			Where IDFormularioImpresion=@IDFormularioImpresion
		
		End

		print 'OPCIONES'
		Set @vMensaje = 'Registro actualizado!'
		Set @vProcesado = 'True'

	End
			
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	
End


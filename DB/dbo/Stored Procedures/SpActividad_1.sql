﻿CREATE Procedure [dbo].[SpActividad]

	--Entrada
	@ID int,
	@IDSucursal tinyint = NULL,
	@IDProveedor int = NULL,
	@Codigo varchar(50) = NULL,
	@Mecanica varchar(500) = NULL,
	@ImporteAsignado money = NULL,
	@DescuentoMaximo decimal (5,2) = NULL,
	
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
	
		Declare @vID int
		
		
		--Validar
		--Si existe el codigo
		if exists(Select * From Actividad Where Codigo=@Codigo And IDSucursal=@IDSucursal) Begin
			set @Mensaje = 'El codigo de la actividad ya existe!'
			set @Procesado = 'false'
			return @@rowcount
		End
		
		--Importe
		--if @ImporteAsignado=0 Begin
		--	set @Mensaje = 'El importe no es correcto!'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		--Insertamos
		Set @vID = IsNull((Select MAX(ID) + 1 From Actividad),1)
		
		Insert Into Actividad(ID, IDSucursal, IDProveedor, Codigo, Mecanica, ImporteAsignado, Entrada, Salida, DescuentoMaximo, CarteraMaxima, Tactico, Acuerdo, Total, PorcentajeUtilizado, Saldo, Excedente)
		Values(@ID, @IDSucursal, @IDProveedor, @Codigo, @Mecanica, @ImporteAsignado, 0, 0, @DescuentoMaximo, 0, 0, 0, 0, 0, @ImporteAsignado, 0)
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si es que el ID no existe
		If Not Exists(Select * From Actividad Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro!'
			set @Procesado = 'false'
			return @@rowcount
		End
		
		--Si existe el codigo
		--if exists(Select * From Actividad Where Codigo=@Codigo And IDSucursal=@IDSucursal And ID!=@ID) Begin
		--	set @Mensaje = 'El codigo de la actividad ya existe!'
		--	set @Procesado = 'false'
		--	return @@rowcount
		--End
		
		--Importe
		--if @ImporteAsignado=0 Begin
		--	set @Mensaje = 'El importe no es correcto!'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		--Que el importe no sea menor al utilizado
		If (Select Total From Actividad Where ID=@ID) > @ImporteAsignado Begin
			set @Mensaje = 'El total utilizado es mayor al importe asignado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--si es que ya se utilizo, no podemos eliminar
		--If (Select Total From Actividad Where ID=@ID) > 0 Begin
		--	set @Mensaje = 'No se puede editar el registro porque ya esta usado!'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		--Actualizamos
		Update Actividad Set Codigo=@Codigo,
							 Mecanica=@Mecanica,
							 ImporteAsignado=@ImporteAsignado,
							 Saldo=@ImporteAsignado,
							 DescuentoMaximo=@DescuentoMaximo							 
							 
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si es que el ID no existe
		If Not Exists(Select * From Actividad Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro!'
			set @Procesado = 'false'
			return @@rowcount
		End
		
		--si es que ya se utilizo, no podemos eliminar
		If (Select Total From Actividad Where ID=@ID) > 0 Begin
			set @Mensaje = 'No se puede eliminar el registro porque ya esta usado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Eliminamos
		Delete From DetalleActividad Where IDActividad=@ID
		Delete From Actividad Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


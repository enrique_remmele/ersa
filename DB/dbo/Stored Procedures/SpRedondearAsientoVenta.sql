﻿
CREATE Procedure [dbo].[SpRedondearAsientoVenta]

	@IDTransaccion numeric(18,0),
	@Credito bit = 'True'
	
		
As

Begin
	
	SET NOCOUNT ON
	
	--Redondear a 0
	Begin
	
		Declare @vID tinyint 
		Declare @vCuentaContable varchar(50) 
		Declare @vCredito money 
		Declare @vDebito money
		Declare @vImporte money
		
		Declare db_cursor cursor for
		Select ID, CuentaContable, Credito, Debito, Importe From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vID, @vCuentaContable, @vCredito, @vDebito, @vImporte 
		While @@FETCH_STATUS = 0 Begin 
		
			If @vCredito > 0 Begin
				Set @vCredito = ROUND(@vCredito, 0)
			End
			
			If @vDebito > 0 Begin
				Set @vDebito = ROUND(@vDebito, 0)
			End
			
			Set @vImporte = @vDebito + @vCredito
			
			--Actualizar El detalle
			Update DetalleAsiento Set Credito = @vCredito, 
									  Debito = @vDebito,
									  Importe = @vImporte
			
			Where IDTransaccion=@IDTransaccion And ID=@vID And CuentaContable=@vCuentaContable
		
			Fetch Next From db_cursor Into @vID, @vCuentaContable, @vCredito, @vDebito, @vImporte 
			print('Entro')							
		End	
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
		--Actualizamos la cabecera, el total
		Declare @vImporteDebe money
		Declare @vImporteHaber money
		Declare @vSaldo money
		
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vSaldo = @vImporteHaber - @vImporteDebe
		
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vSaldo
		Where IDTransaccion=@IDTransaccion
		
		declare @vSaldoMinimo money = -20
		declare @vSaldoMaximo money = 20

		--Reparar
		If @vSaldo != 0 Begin
		
			If @vSaldo > @vSaldoMinimo or @vSaldo < @vSaldoMaximo Begin
				
				Declare @vCuenta varchar(50)
			
				--Hayamos la cuenta a afectar
				Set @vCuenta = (Select Top(1) CC.CuentaContable 
								From VCFVentaVenta CC
								Join DetalleAsiento DA On CC.Codigo=DA.CuentaContable
								Where DA.IDTransaccion=@IDTransaccion
								Order By 1 Desc)
			
				If @vCuenta is NULL Begin
					Set @vCuenta = (Select Top(1) Codigo from VDetalleAsiento Where IDTransaccion=@IDTransaccion And Descripcion Like '%Ventas%')
				End
			
				--Si es mayor a 0, Restar el Credito en Ventas
				If @vSaldo > 0 Begin
					If @Credito='True' Begin
						Update DetalleAsiento Set Credito = Credito - @vSaldo								
						Where IDTransaccion=@IDTransaccion And CuentaContable = @vCuenta
					End
				
					If @Credito='False' Begin
						Update DetalleAsiento Set Debito = Debito - @vSaldo								
						Where IDTransaccion=@IDTransaccion And CuentaContable = @vCuenta
					End
				
				End
			
				--Si es menor a 0, Aumentar el Credito en Ventas
				If @vSaldo < 0 Begin
					If @Credito = 'True' Begin
						Update DetalleAsiento Set Credito = Credito + (@vSaldo * -1)
						Where IDTransaccion=@IDTransaccion And CuentaContable = @vCuenta
					End
				
					If @Credito = 'False' Begin
						Update DetalleAsiento Set Debito = Debito + (@vSaldo * -1)
						Where IDTransaccion=@IDTransaccion And CuentaContable = @vCuenta
					End
				
				End
			
			End
		
		End

	End
	
	
Salir:
	
End


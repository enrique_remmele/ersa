﻿CREATE Procedure [dbo].[SpRecalcularSaldoTactico]
	
	--Entrada
	@IDTransaccion numeric(18,0)
	
As

Begin
	
	--Variables
	Begin
		Declare @vMensaje varchar(50)
		Declare @vProcesado bit
		
		Declare @vIDTipoDescuento int
		Declare @vIDTipoDescuentoTactico int
		Declare @vIDTipoDescuentoAcuerdo int
		Declare @vFechaDesde date
		Declare @vFechaHasta date
		Declare @vIDProducto int
		Declare @vProducto varchar(100)
		Declare @vDescuento money
		Declare @vIDActividad int
		Declare @vActividad varchar(50)
	End
	
	--Establecer a 0 todas las actividades
	Begin
		
		Declare db_cursorActividad cursor for
		Select A.ID From Actividad A Join DetallePlanillaDescuentoTactico DP On A.ID=DP.IDActividad
		Where DP.IDTransaccion=@IDTransaccion
		Open db_cursorActividad   
		Fetch Next From db_cursorActividad Into @vIDActividad
		While @@FETCH_STATUS = 0 Begin 
		
			Update Actividad Set Tactico=0,
								Acuerdo=0,
								Total=0,
								PorcentajeUtilizado=0,
								Saldo=0,
								Excedente=0
			Where ID=@vIDActividad	 	 		
			Fetch Next From db_cursorActividad Into @vIDActividad
										
		End
		
		--Cierra el cursor
		Close db_cursorActividad   
		Deallocate db_cursorActividad
		
	End
	
	--Obtener valores
	Begin
	
		Set @vMensaje='No se proceso!'
		Set @vProcesado='False'
		
		Set @vIDTipoDescuentoTactico = 1
		Set @vIDTipoDescuentoAcuerdo = 3
		Set @vFechaDesde = (Select Desde From PlanillaDescuentoTactico where IDTransaccion=@IDTransaccion)
		Set @vFechaHasta = (Select Hasta From PlanillaDescuentoTactico where IDTransaccion=@IDTransaccion)
	End
	
	--Calcular
	Begin
		Declare db_cursor cursor for
		Select IDProducto, Descuento, Producto, IDTipoDescuento, IDActividad From VDescuento D 
		Where (D.Fecha Between @vFechaDesde And @vFechaHasta) And (D.IDTipoDescuento=@vIDTipoDescuentoTactico Or D.IDTipoDescuento=@vIDTipoDescuentoAcuerdo)
		And Anulado='False'
		Order By 1
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDProducto, @vDescuento, @vProducto, @vIDTipoDescuento, @vIDActividad
		While @@FETCH_STATUS = 0 Begin 
			
			--Buscamos la Actividad
			--Set @vIDActividad = IsNull((Select A.ID From PlanillaDescuentoTactico P 
			--					Join DetallePlanillaDescuentoTactico DP On	P.IDTransaccion=DP.IDTransaccion
			--					Join Actividad A On DP.IDActividad=A.ID
			--					Join DetalleActividad DA On A.ID=DA.IDActividad
			--					Where DA.IDProducto=@vIDProducto And P.IDTransaccion=@IDTransaccion),0)
			
			Set @vActividad = IsNull((Select Top 1 Codigo From Actividad Where ID=@vIDActividad), '---')
			
			--Tacticos											
			If @vIDTipoDescuento = @vIDTipoDescuentoTactico Begin
				Update Actividad Set Tactico=Tactico + @vDescuento
				Where ID=@vIDActividad
			End
			 
			--Acuerdos
			If @vIDTipoDescuento = @vIDTipoDescuentoAcuerdo Begin
				Update Actividad Set Acuerdo=Acuerdo + @vDescuento
				Where ID=@vIDActividad
			End	 		
			
			Fetch Next From db_cursor Into @vIDProducto, @vDescuento, @vProducto, @vIDTipoDescuento, @vIDActividad
										
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
		
		Set @vMensaje='Registros procesados!'
		Set @vProcesado='True'
		
	End
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado, 'Registros'=@@rowcount
End





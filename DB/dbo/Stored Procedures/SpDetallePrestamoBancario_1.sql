﻿CREATE Procedure [dbo].[SpDetallePrestamoBancario]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@NroCuota smallint = NULL,
	@FechaVencimiento date = NULL,
	@Amortizacion money = NULL,
	@Interes money = NULL,
	@Impuesto money = NULL,
	@ImporteCuota money = NULL,
	@Pagar bit = NULL,
	@FechaPago date = NULL,
	@ImporteAPagar money = NULL,
	@PagosVarios money = NULL,
	@ObservacionPago varchar(100) = NULL,
	@Cancelado bit = 'False',
	@Operacion varchar(50),
	
--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin

	--Validar
	--Transaccion
	If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--Transaccion
	If Not Exists(Select IDTransaccion From PrestamoBancario Where IDTransaccion=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--BLOQUES
	if @Operacion='INS' Begin
	
		--Nro cuota
		If @NroCuota <= 0 Begin
			Set @Mensaje = 'La cuota no puede ser igual ni menor a 0!'
			Set @Procesado = 'False'
			return @@rowcount
		End
	
		--Amortizacion
		If @Amortizacion < 0 Begin
			Set @Mensaje = 'La amortizacion no puede ser menor a 0!'
			Set @Procesado = 'False'
			return @@rowcount
		End

		--Nro cuota
		If @ImporteCuota <= 0 Begin
			Set @Mensaje = 'El importe de la cuota no puede ser igual ni menor a 0!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--Nro cuota
		If Exists(Select NroCuota From DetallePrestamoBancario Where IDTransaccion=@IDTransaccion And NroCuota=@NroCuota) Begin
			Set @Mensaje = 'Numero de cuota ya asignado. Por favor verifique y vuelva a asignar!'
			Set @Procesado = 'False'
			return @@rowcount
		End

		--Insertar el detalle
		Insert Into DetallePrestamoBancario(IDTransaccion, NroCuota, FechaVencimiento, Amortizacion, Interes, Impuesto, ImporteCuota, Pagar, FechaPago, ImporteAPagar, PagosVarios, ObservacionPago, Cancelado)
		Values(@IDTransaccion, @NroCuota, @FechaVencimiento, @Amortizacion, @Interes, @Impuesto, @ImporteCuota, @Pagar, @FechaPago, @ImporteAPagar, @PagosVarios, @ObservacionPago, 'False')
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount					
			
	End
	
	if @Operacion='DEL' Begin
		
		--Validar
		--Transaccion
		If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra la transaccion!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--Transaccion
		If Not Exists(Select IDTransaccion From DetallePrestamoBancario Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra la transaccion!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	if @Operacion='ANULAR' Begin
		
		Set @Mensaje = 'No se proceso!'
		Set @Procesado = 'False'
		return @@rowcount
	
	End
	
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	return @@rowcount
	
		
End

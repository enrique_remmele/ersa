﻿CREATE Procedure [dbo].[SpAcualizarClienteVenta]

	--Entrada
	@IDCliente int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin

	--Variables
	Declare @vFechaVenta date
	
	--Obtener valores	
	Set @vFechaVenta = (Select Max(FechaEmision) From Venta Where IDCliente=@IDCliente And Anulado='False' And Procesado='True')
		
	--Actualizar la ultima compra
	Update Cliente Set FechaUltimaCompra=@vFechaVenta
	Where ID=@IDCliente
	
	--Actualizar la Deuda	
	Exec SpAcualizarSaldoCliente @IDCliente
			
	set @Mensaje = 'Registro guardado'
	set @Procesado = 'True'
	return @@rowcount
		

End
	
	


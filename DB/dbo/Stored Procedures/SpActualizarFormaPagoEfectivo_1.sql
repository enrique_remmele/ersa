﻿CREATE Procedure [dbo].[SpActualizarFormaPagoEfectivo]

	--Entrada
	@IDTransaccion int,
	@Comprobante varchar(50),
	@Observacion varchar(150),
	@ID int,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin

			
	--Actualizar 
	Update Efectivo Set Comprobante=@Comprobante,
								 Observacion =@Observacion 
								  
	Where IDTransaccion=@IDTransaccion And ID=@ID
	
			
	set @Mensaje = 'Registro guardado'
	set @Procesado = 'True'
	return @@rowcount
		

End
	
	


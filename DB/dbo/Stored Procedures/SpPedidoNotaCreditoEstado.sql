﻿CREATE Procedure [dbo].[SpPedidoNotaCreditoEstado]

	--Entrada
	@IDPedido int,
	@IDSucursal tinyint,
	@IDTransaccionNotaCredito numeric(18,0)
	
	
As

Begin

	Declare @vProcesado bit
	Declare @vIDTransaccionPedido numeric(18,0)
	Declare @vIDTransaccionPedidoVenta numeric(18,0)
	Declare @vIDTransaccionVenta numeric(18,0)
	Declare @vMensaje varchar(50)
	Declare @vComprobante varchar(50)
	Declare @vTotalNotaCredito money

	Declare @vIDProducto int
	Declare @vIDCliente int
	Declare @vIDListaPrecio int
	Declare @vCantidad decimal(18,3)
	Declare @vFecha date
	
	Set @vProcesado = 'False'
	Set @vMensaje = 'No procesado'
	Set @vComprobante = '---'

	--Si no se guardo en la BD	
	If Not Exists(Select * From NotaCredito Where IDTransaccion=@IDTransaccionNotaCredito) Begin
		Set @vProcesado = 'False'
		Set @vMensaje = 'La NotaCredito no se guardo!'
		GoTo Salir
	End

	If (Select Procesado From NotaCredito Where IDTransaccion=@IDTransaccionNotaCredito) = 0 Begin
		Set @vProcesado = 'False'
		Set @vMensaje = 'La NotaCredito no se proceso!'
		GoTo Salir
	End

	
	Set @vIDTransaccionPedido = (Select Max(IDTransaccion) From PedidoNotaCredito Where Numero=@IDPedido And IDSucursal=@IDSucursal And Procesado='True')
	Set @vIDTransaccionPedidoVenta = (select Max(IDTransaccionPedidoVenta) from PedidoNotaCreditoPedidoVenta where idtransaccionPedidoNotaCredito = @vIDTransaccionPedido)
	Set @vIDTransaccionVenta = (Select Max(IDTransaccionVenta) From PedidoVenta Where IDTransaccionPedido = @vIDTransaccionPedidoVenta)
	--Select * From PedidoNotaCredito Where Procesado='True'
	

	--Si el pedido ya corresponde a otra NotaCredito sin anular, no es correcta
	If Exists(Select Top(1) V.IDTransaccion From NotaCredito V 
											Join PedidoNCNotaCredito PV On V.IDTransaccion=PV.IDTransaccionNotaCredito 
											Where V.IDTransaccion<>@IDTransaccionNotaCredito 
											And V.Anulado='False' 
											And PV.IDTransaccionPedido=@vIDTransaccionPedido) Begin
		Set @vProcesado = 'False'
		Set @vMensaje = 'La NotaCredito no se proceso!'
		GoTo Salir
	End

	if Exists(select * from PedidoNCNotaCredito where IDTransaccionPedido = @vIDTransaccionPedido and IDTransaccionNotaCredito = @IDTransaccionNotaCredito) begin
		GoTo OK
	end

	Update PedidoNotaCredito Set ProcesadoNC='True'	
	Where IDTransaccion = @vIDTransaccionPedido
	
	Insert Into PedidoNCNotaCredito(IDTransaccionPedido, IDTransaccionNotaCredito)
	Values(@vIDTransaccionPedido, @IDTransaccionNotaCredito)
	
	Update DetalleNotaCreditoDiferenciaPrecio 
			set IDTransaccionNotaCredito = @IDTransaccionNotaCredito,
				IDTransaccionVenta = @vIDTransaccionVenta
	Where IDTransaccionPedidoNotaCredito = @vIDTransaccionPedido

	Update DetalleNotaCreditoAcuerdo 
			set IDTransaccionNotaCredito = @IDTransaccionNotaCredito,
			IDTransaccionVenta = @vIDTransaccionVenta
	Where IDTransaccionPedidoNotaCredito = @vIDTransaccionPedido
	
OK:
	
	Set @vProcesado = 'True'
	Set @vMensaje = 'OK'
	Set @vComprobante = (Select Comprobante From VNotaCredito Where IDTransaccion=@IDTransaccionNotaCredito)
	
Salir:

	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado, 'Comprobante'=@vComprobante
	
End



﻿CREATE Procedure [dbo].[SpCobranzaCredito]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@Numero int = 0,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante varchar(50) = NULL,
	@Comprobante varchar(50) = NULL,	
	@IDCliente int = NULL,
	@FechaEmision date = NULL,
	@IDCobrador tinyint = NULL, 
	@Total money = NULL,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@TotalDescuento money = NULL,
	@Observacion varchar(200) = NULL,
	@Operacion varchar(50) = '',
	@NroPlanilla varchar(10) = '',
	@EsMovil bit = 'False',
	@IDMoneda tinyint = Null,
	@Cotizacion money = null,
	@DiferenciaCambio money = 0,
	@AnticipoCliente bit = 'False',
	@IDTipoAnticipo tinyint = null,
	@ReciboInterno bit = 'False',
	@IDMotivoAnulacion varchar(50) = NULL,
		
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	@IDTransaccionCaja numeric(18,0) = 0,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
	
	Declare @vIDRecibo as int = 0
	Declare @NroReciboInterno as int =0
	If (@ReciboInterno = 'True') Begin
		set @vIDRecibo = (Select ID from Recibo where Referencia = '030' and Estado = 1)
	End 
--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @FechaEmision, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @FechaEmision = (Select FechaEmision from CobranzaCredito where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @FechaEmision, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		Declare @vNumero int
		Set @vNumero = ISNull((Select MAX(Numero)+1 From CobranzaCredito Where IDSucursal=@IDSucursalOperacion),1)
		
		if isnull(@IDCliente,0) = 0 begin
			set @Mensaje = 'Debe seleccionar un cliente!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		end

		--controlar comprobante recibos a rendir
		if @IDTipoComprobante = 128 and @AnticipoCliente = 'False' begin
			set @Mensaje = 'El Comprobante Recibo a rendir solo puede utilizarse con anticipos!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		end

		--Controlar NroRecibo
		If @IDTipoComprobante = 33  and @Comprobante !=''  begin -- 33 IDTipoComprobante = "Recibo"
			If exists(Select * from Configuraciones Where CobranzaControlarNroRecibo = 1) begin
				If exists(Select * from CobranzaCredito Where Comprobante = @Comprobante and @ReciboInterno = 'True') begin
				--If exists(Select * from CobranzaCredito Where IDSucursal = @IDSucursal And Comprobante = @Comprobante) begin
					set @Mensaje = 'El numero de recibo ya existe para esta sucursal!'
					set @Procesado = 'False'
					set @IDTransaccionSalida  = 0
					return @@rowcount
				End
			End
		End

		--08/07/2021 SM: Controlar NroRecibo con  @ReciboInterno = 'False'
		--Controlar NroRecibo
		If @IDTipoComprobante = 33  and @Comprobante !=''  begin -- 33 IDTipoComprobante = "Recibo"
			If exists(Select * from Configuraciones Where CobranzaControlarNroRecibo = 1) begin
				If exists(Select * from CobranzaCredito Where IDTipoComprobante = 33 and Comprobante = @Comprobante and IDSucursal = @IDSucursalOperacion and @ReciboInterno = 'False') begin
						set @Mensaje = 'El numero de recibo ya existe para esta sucursal!'
					set @Procesado = 'False'
					set @IDTransaccionSalida  = 0
					return @@rowcount
				End
			End
		End
		
		--Si ya existe el documento
		--If (@NroComprobante!='') and (@ReciboInterno = 'False') Begin
		--	If Exists(Select * From CobranzaCredito Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante And Anulado='False' and Procesado = 'True') Begin
		--	--If Exists(Select * From CobranzaCredito Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante And IDSucursal=@IDSucursalOperacion And Anulado='False' and Procesado = 'True') Begin
		--		set @Mensaje = 'El numero de comprobante ya existe!'
		--		set @Procesado = 'False'
		--		set @IDTransaccionSalida  = 0
		--		return @@rowcount
		--	End
		--End

		If (@ReciboInterno = 'True') Begin
			set @NroReciboInterno = (Select UltimoNumero+1 from Recibo where ID = @vIDRecibo)
			set @NroComprobante = concat('030-',(Select UltimoNumero+1 from Recibo where ID = @vIDRecibo))
		end
		else begin
			
			if @NroComprobante = '' begin
				set @Mensaje = 'Debe ingresar un numerode recibo!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
			end

		End
		
		--Total
		If @Total <= 0 Begin
			set @Mensaje = 'El importe debe ser mayor a 0!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Insertar Transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@IDTransaccionCaja=@IDTransaccionCaja,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
			
		--Insertar en Cobranza Credito
		Insert Into CobranzaCredito(IDTransaccion, IDSucursal, Numero, IDTipoComprobante, NroComprobante, Comprobante,NroPlanilla ,IDCliente, FechaEmision, IDCobrador, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, Anulado, FechaAnulado, IDUsuarioAnulado, Observacion,IDMoneda,Cotizacion,DiferenciaCambio,AnticipoCliente,IDTipoAnticipo, IDRecibo, ReciboInterno)
		Values(@IDTransaccionSalida, @IDSucursalOperacion, @vNumero, @IDTipoComprobante, @NroComprobante, @Comprobante, @NroPlanilla,   @IDCliente, @FechaEmision, @IDCobrador, @Total, @TotalImpuesto, @TotalDiscriminado, @TotalDescuento, 'False', NULL, NULL, @Observacion,@IDMoneda,@Cotizacion,@DiferenciaCambio,@AnticipoCliente,@IDTipoAnticipo, @vIDRecibo, @ReciboInterno)
				
		--Actualizar el ultimo numero utilizado del recibo interno
		If (@ReciboInterno = 'True') Begin
			IF (@vIDRecibo>0) Begin					
				Update Recibo 
				Set  UltimoNumero =@NroReciboInterno
				Where ID=@vIDRecibo
			End
		End

		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	--ANULAR
	If @Operacion = 'ANULAR' Begin
	--La cobranza ya esta anulada
			If Exists(select * from CobranzaCredito where IDTransaccion = @IDTransaccion and Anulado = 'True') Begin
				set @Mensaje = 'La Cobranza ya esta anulada.'
				set @Procesado = 'False'
				return @@rowcount
			End
		--Validar
		If @EsMovil = 'False' Begin
			--Si el efectivo ya se usó en una orden de pago
			If Exists(Select * From VOrdenPagoEfectivo OPE Join Efectivo E on OPE.IDTransaccion=E.IDTransaccion Where E.IDTransaccion= @IDTransaccion) Begin
				set @Mensaje = 'Este efectivo ya fué usado en una orden de pago.'
				set @Procesado = 'False'
				return @@rowcount
			End
		End
				
		--Si el efectivo esta depositado, esto no pasa ya que no se depositan los efectivos
		If (Select Depositado From Efectivo Where IDTransaccion=@IDTransaccion) > 0 Begin
			set @Mensaje = 'Parte o el parcial del importe pagado en efectivo ya fue depositado! No se puede anular el registro.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
--Si los cheques estan depositados
		If Exists (Select CC.Depositado From FormaPago FP Join ChequeCliente CC on FP.IDTransaccionCheque=CC.IDTransaccion Where FP.IDTransaccion=@IDTransaccion And (CC.Depositado='True' or CC.Rechazado = 'True')) Begin
			set @Mensaje = 'El documento se pago con un Cheque que ya esta Depositado o Rechazado! No se puede anular el registro.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Si los Documentos ya estan depositados
		IF Exists(select * from FormaPagoDocumento FPD JOin DetalleDepositoBancario DDB on DDB.IDTransaccionDocumento =  FPD.IDTransaccion 
		Join TipoComprobante TC on TC.ID = FPD.IDTipoComprobante and FPD.ID = DDB.IDDetalleDocumento
		where FPD.IDTransaccion = @IDTransaccion and DDB.IDTransaccionDocumento is not null and TC.Deposito = 'True') Begin
			set @Mensaje = 'La cobranza tiene un documento que ya fue depositado! No se puede anular el registro.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Si las tarjetas ya estan depositados
		IF Exists(select * from FormaPagoTarjeta FPD JOin DetalleDepositoBancario DDB on DDB.IDTransaccionTarjeta =  FPD.IDTransaccion 
		Join TipoComprobante TC on TC.ID = FPD.IDTipoComprobante and FPD.ID = DDB.IDDetalleDocumento
		where FPD.IDTransaccion = @IDTransaccion and DDB.IDTransaccionTarjeta is not null) Begin
			set @Mensaje = 'La cobranza tiene un pago con tarjeta que ya fue depositado! No se puede anular el registro.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Asientos Cerrados
		If Exists(Select * From Asiento A Where A.IDTransaccion=@IDTransaccion And A.Conciliado='True') Begin
			set @Mensaje = 'El asiento de este documento ya esta conciliado! No se puede anular.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Verificar dias de bloqueo
		If (Select Top(1) CobranzaCreditoBloquearAnulacion From Configuraciones) = 'True' Begin
			
			Declare @vDias tinyint
			Set @vDias = (Select Top(1) CobranzaCreditoDiasBloqueo From Configuraciones)

			If (Select DATEDIFF(dd,(Select V.FechaEmision From CobranzaCredito V Where IDTransaccion=@IDTransaccion), GETDATE())) > @vDias Begin
			 			
				set @Mensaje = 'El sistema no puede anular este registro ya que la configuracion lo restringe por la antigüedad del documento. Cambie la configuracion de bloqueo para anular!'
				set @Procesado = 'False'
				return @@rowcount
				
			End
			
		End
		
		--Procesar
		--Reestablecemos las Ventas y Saldo del Cliente
		EXEC SpVentaCobranza @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Reestablecemos el cliente
		If @EsMovil = 'False' Begin
			EXEC SpChequeClienteSaldar @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		End
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la forma de pago		
		Delete From Efectivo Where IDTransaccion = @IDTransaccion
		Delete From FormaPagoDocumento Where IDTransaccion = @IDTransaccion
		Delete From FormaPago Where IDTransaccion = @IDTransaccion
		Delete From FormaPagoTarjeta Where IDTransaccion = @IDTransaccion
		Delete From FormaPagoDocumentoRetencion Where IDTransaccion = @IDTransaccion

		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from CobranzaCredito where IDTransaccion = @IDTransaccion)
		set @IDCliente = (select IDCliente from CobranzaCredito where IDTransaccion = @IDTransaccion)
		set @FechaEmision = (select FechaEmision from CobranzaCredito where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from CobranzaCredito where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select Comprobante from CobranzaCredito where IDTransaccion = @IDTransaccion)
		declare @VTipoComprobante varchar(16) = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from CobranzaCredito where IDTransaccion = @IDTransaccion)
		declare @Condicion varchar(16) = 'CREDITO'

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,@IDCliente,0,@FechaEmision,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ANU',@VTipoComprobante, @IDSucursal)

				
		--Anulamos el registro
		Update CobranzaCredito Set Anulado='True', IDUsuarioAnulado=@IDUsuario, FechaAnulado=GETDATE(), idMotivoAnulacion =@IDMotivoAnulacion
		Where IDTransaccion = @IDTransaccion
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal		
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		GoTo Salir
		
	End
	
	--ELIMINAR
	If @Operacion = 'DEL' Begin
	
		--Validar
		--Si el efectivo esta depositado
		
		--Si los cheques estan depositados
		
		--Controlar los asientos asociados
		
		--Procesar
		--Eliminar la Forma de Pago (Eliminar el efectivo y actualizarlo - Reestablecer el saldo del cheque)

		--Eliminar relacion de las ventas asociadas (Actualizar saldos de la venta y cliente)
		
		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from CobranzaCredito where IDTransaccion = @IDTransaccion)
		set @IDCliente = (select IDCliente from CobranzaCredito where IDTransaccion = @IDTransaccion)
		set @FechaEmision = (select FechaEmision from CobranzaCredito where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from CobranzaCredito where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select Comprobante from CobranzaCredito where IDTransaccion = @IDTransaccion)
		set @VTipoComprobante = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from CobranzaCredito where IDTransaccion = @IDTransaccion)
		set @Condicion = 'CREDITO'

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,@IDCliente,0,@FechaEmision,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ELI',@VTipoComprobante, @IDSucursal)


		--Eliminamos el registro
		Delete CobranzaCredito Where IDTransaccion = @IDTransaccion
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		GoTo Salir
		
	End

Salir:
	
	--Actualizar saldos de clientes
	Begin
		Declare @vIDTransaccionVenta numeric(18,0)
		Declare @vIDCliente int
		
		Declare db_cursor cursor for
		Select IDTransaccionVenta From VentaCobranza Where IDTransaccionCobranza=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVenta
		While @@FETCH_STATUS = 0 Begin 
			
			Set @vIDCliente = (Select IDCliente From Venta Where IDTransaccion=@vIDTransaccionVenta)
			
			Exec SpAcualizarSaldoCliente @IDCliente=@vIDCliente
			
			Fetch Next From db_cursor Into @vIDTransaccionVenta
										
		End
		
		Close db_cursor   
		Deallocate db_cursor
		
	End
		
End


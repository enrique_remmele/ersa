﻿CREATE Procedure [dbo].[SpNotaDebitoProveedor]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDTipoComprobante smallint = NULL,
	@Numero int = NULL,
	@NroComprobante Varchar(50) = NULL,			
	@NroTimbrado int = NULL,			
	@IDProveedor int = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDDepositoOperacion tinyint = NULL,
	@Fecha date = NULL,
	@VencimientoTimbrado date = NULL, 
	@IDMoneda tinyint = NULL,
	@Cotizacion money= NULL,
	@Observacion varchar(100) = NULL,
	@Total money = NULL,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@TotalDescuento money = NULL,
	@Aplicar bit = 'False',
	@Devolucion bit = 'False',
	@Descuento bit = 'False',	
	@ConComprobantes bit = 'True',
	@Saldo money  = NULL,
			
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
	
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from NotaDebitoProveedor where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Comprobante
		if Exists(Select * From NotaDebitoProveedor   Where IDSucursal =@IDSucursal  And NroComprobante=@NroComprobante) Begin
			set @Mensaje = 'El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--Calcular Saldo....
		--Si es Sin Comprobantes, SALDO = @TOTAL
		--Si no, SALDO = 0
		If @ConComprobantes = 'False' Begin
			Set @Saldo = @Total 		
		End Else Begin
			Set @Saldo = 0
		End
		
		--Insertar en Nota de Debito Proveedor
		Insert Into NotaDebitoProveedor  (IDTransaccion,IDTipoComprobante, Numero , NroComprobante, NroTimbrado, IDProveedor, IDSucursal, IDDeposito,  Fecha, IDMoneda, Cotizacion, Observacion, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, Saldo, Anulado,FechaAnulado, FechaVencimientoTimbrado, IDUsuarioAnulado, Procesado, Devolucion, Descuento)
		Values(@IDTransaccionSalida, @IDTipoComprobante, @Numero, @NroComprobante, @NroTimbrado, @IDProveedor,  @IDSucursalOperacion, @IDDeposito, @Fecha, @IDMoneda, @Cotizacion, @Observacion, @Total, @TotalImpuesto, @TotalDiscriminado, @TotalDescuento, @Saldo, 'False',@VencimientoTimbrado, NULL, NULL, 'False',@Devolucion, @Descuento)

		
										
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From NotaDebitoProveedor      Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Actualizamos el Stock
		Exec SpNotaDebitoProveedorActualizarStock @IDTransaccion = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje output, @Procesado=@Procesado output
		print @Procesado
		
		If @Procesado = 0 Begin
			set @Mensaje = 'No se puede ELIMINAR STOCK'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Actualizamos de saldos de las Compras
		Exec SpNotaDebitoProveedorActualizarCompra  @IDTransaccion = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje output, @Procesado=@Procesado output
		
		If @Procesado = 0 Begin
			set @Mensaje = 'No se actualizaron las Compras :'
			return @@rowcount
		End

		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from NotaDebitoProveedor where IDTransaccion = @IDTransaccion)
		set @IDProveedor = (select IDProveedor from NotaDebitoProveedor where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from NotaDebitoProveedor where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from NotaDebitoProveedor where IDTransaccion = @IDTransaccion)
		declare @Comprobante varchar(16)= (select Comprobante from VNotaDebitoProveedor where IDTransaccion = @IDTransaccion)
		declare @VTipoComprobante varchar(16) = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from NotaDebitoProveedor where IDTransaccion = @IDTransaccion)
		declare @Condicion varchar(16) = 'DEBITO'

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,0,@IDProveedor,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ELI',@VTipoComprobante, @IDSucursal)


		--Eliminamos notadebitoproveedorcompra
		Delete NotaDebitoProveedorCompra   Where IDTransaccionNotaDebitoProveedor = @IDTransaccion 
		
		--Eliminamos impuesto
		Delete DetalleImpuesto where IDTransaccion= @IDTransaccion 
		
		--Eliminamos el detalle
		Delete DetalleNotaDebitoProveedor    where IDTransaccion = @IDTransaccion				
				
		--Eliminamos el registro
		Delete NotaDebitoProveedor    Where IDTransaccion = @IDTransaccion
						
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion				
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End



﻿
CREATE Procedure [dbo].[SpVentaLoteDistribucion]

	--Entrada
	@IDTransaccionVenta numeric(18,0),
	@IDTransaccionLote numeric(18,0),
	@ID smallint = 0,
	@Importe money = NULL,
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin
	
	--Procesar Saldos de Ventas
	If @Operacion = 'INS' Begin
	
		--Verificar la venta
		If Not Exists(Select * From Venta Where IDTransaccion=@IDTransaccionVenta) Begin
			Set @Mensaje = 'La venta no existe!'
			Set @Procesado = 'false'
			Return @@rowcount
		End
		
		--Verificar el lote
		If Not Exists(Select * From LoteDistribucion Where IDTransaccion=@IDTransaccionLote) Begin
			Set @Mensaje = 'El lote no existe!'
			Set @Procesado = 'false'
			Return @@rowcount
		End
		
		--Insertar
		Insert Into VentaLoteDistribucion(IDTransaccionVenta, IDTransaccionLote, ID, Importe)
		Values(@IDTransaccionVenta, @IDTransaccionLote, @ID, @Importe)
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		Return @@rowcount
		
	End
	
	If @Operacion = 'ANULAR' Begin
		
		--Verificar que exista el registro
		If Not Exists(Select * From VentaLoteDistribucion Where IDTransaccionVenta=@IDTransaccionVenta And IDTransaccionLote=@IDTransaccionLote) Begin
			Set @Mensaje = 'El registro no existe!'
			Set @Procesado = 'false'
			Return @@rowcount
		End
		
		Delete From VentaLoteDistribucion 
		Where IDTransaccionVenta=@IDTransaccionVenta And IDTransaccionLote=@IDTransaccionLote 		
						
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		Return @@rowcount
		
	End
	
	If @Operacion = 'DEL' BEGIN		
		--Verificar que exista el registro
		If Not Exists(Select * From VentaLoteDistribucion Where IDTransaccionVenta=@IDTransaccionVenta And IDTransaccionLote=@IDTransaccionLote) Begin
			Set @Mensaje = 'El registro no existe!'
			Set @Procesado = 'false'
			Return @@rowcount
		End
		
		Delete From VentaLoteDistribucion 
		Where IDTransaccionVenta=@IDTransaccionVenta And IDTransaccionLote=@IDTransaccionLote 
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'false'
		Return @@rowcount
		
	END
	--JGR 20140820 Elimino todo lo que sea del lote, sin importar la transaccion de venta.
	--Ver frmLoteDistribucion. Evento Guardar.
	If @Operacion = 'DEL1' BEGIN 
		--Verificar que exista el registro
		--If Not Exists(Select * From VentaLoteDistribucion Where IDTransaccionVenta=@IDTransaccionVenta And IDTransaccionLote=@IDTransaccionLote) Begin
		If Not Exists(Select * From VentaLoteDistribucion Where IDTransaccionLote=@IDTransaccionLote) Begin
			Set @Mensaje = 'El registro no existe!'
			Set @Procesado = 'false'
			Return @@rowcount
		End
		
		--Elimina todos los registros de este lote
		Delete From VentaLoteDistribucion 
		--Where IDTransaccionVenta=@IDTransaccionVenta And IDTransaccionLote=@IDTransaccionLote 
		Where IDTransaccionLote=@IDTransaccionLote 	
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		
		--Eliminamos y Volvemos a cargar el Detalle
		EXEC SpDetalleLoteDistribucion @IDTransaccion = @IDTransaccionLote, @Operacion = 'INS',	@Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
				

		Return @@rowcount
		
	End
		
	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'
	
End
	



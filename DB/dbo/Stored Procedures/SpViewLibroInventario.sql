﻿CREATE Procedure [dbo].[SpViewLibroInventario]

	--Entrada
	@Año smallint,
	@Mes tinyint,
	@IDSucursal int = 0,
	@IDUnidadNegocio int = 0,
	@Factor int
	
As

Begin

	--variables
	Declare @vID tinyint
	Declare @vM varchar(50)
	Declare @vCodigo varchar(50)
	Declare @vDenominacion varchar(200)
	Declare @vCategoria tinyint
	Declare @vImputable bit
	Declare @vDebito money
	Declare @vCredito money
	Declare @vSaldo money
	Declare @vDebitoAnterior money
	Declare @vCreditoAnterior money
	Declare @vSaldoAnterior money
	Declare @vDebitoActual money
	Declare @vMovimientoMes money
	Declare @vCreditoActual money
	Declare @vSaldoActual money
	Declare @vIDUnidadNegocio int

	Declare @vMovimientoCierre money
	Declare @vCreditoCierre money
	Declare @vDebitoCierre money
	Declare @vSeccion varchar(50)

	--Crear la tabla
	Create table #BalanceGeneral(ID tinyint,
								Año smallint,
								Mes tinyint,
								IDSucursal int,
								M varchar(50),
								Codigo varchar(50),
								Denominacion varchar(200),
								Categoria tinyint,
								Imputable bit,
								MostrarCodigo varchar(50),

								IDUnidadNegocio int,
								SaldoAnterior money,
								MovimientoMes money,
								MovimientoCierre money,
								SaldoActual money,
								Seccion varchar(50)
								)
								
	set @vID = (Select IsNull(MAX(ID)+1,1) From #BalanceGeneral)
	
	If @Factor = 0 Begin
		Set @Factor = 1
	End
	
	Declare db_cursor cursor for
	Select	CC.Codigo, CC.Descripcion, CC.Categoria, CC.Imputable, isnull(CC.IDUnidadNegocio,0)	
	From VCuentaContable CC	
	Where (SUBSTRING(CC.Codigo, 0, 2) Between '1' And '5')
	And (Case When @IDUnidadNegocio = 0 then 0 else IDUnidadNegocio end) = (Case When @IDUnidadNegocio = 0 then 0 else @IDUnidadNegocio end)
	--Where (SUBSTRING(CC.Codigo, 0, 5) Between '1114' And '1114')
	Open db_cursor   
	Fetch Next From db_cursor Into	@vCodigo, @vDenominacion, @vCategoria, @vImputable, @vIDUnidadNegocio
	While @@FETCH_STATUS = 0 Begin 
		
		--Tipo de Cuenta
		Declare @vCuentaTipo varchar(50)
		Declare @vPrefijo varchar(50)

		Set @vPrefijo = SubString(@vCodigo, 0, 2)
		if @vPrefijo =1 begin
			Set @vSeccion = 'A C T I V O'
		end 
		if @vPrefijo =2 begin
			Set @vSeccion = 'P A S I V O'
		end 
		if @vPrefijo =3 begin
			Set @vSeccion = 'P A T R I M O N I O  N E T O'
		end 
		if @vPrefijo =4 begin
			Set @vSeccion = 'I N G R E G O S'
		end 
		if @vPrefijo =5 begin
			Set @vSeccion = 'E G R E S O S'
		end 
		--Cuentas del Debe
		If @vPrefijo = '1' Or @vPrefijo = '5' Begin
			Set @vCuentaTipo = 'DEBE'
		End

		--Cuentas del Haber
		If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
			Set @vCuentaTipo = 'HABER'
		End
					
		Set @vDebitoAnterior = 0
		Set @vCreditoAnterior = 0
		
		--Saldo Anterior
		If @Mes > 1 Begin
			--Toda la Empresa
			If @IDSucursal = 0 Begin
				Set @vDebitoAnterior = Round(IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes<@Mes And Cuenta=@vCodigo),0),0)
				Set @vCreditoAnterior = round(IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes<@Mes And Cuenta=@vCodigo),0),0)
			End
			
			--Por Sucursal
			If @IDSucursal > 0 Begin
				Set @vDebitoAnterior = Round(IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes<@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0),0)
				Set @vCreditoAnterior = Round(IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes<@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0),0)
			End
		End
		
		--Saldo Actual
		--Toda la Empresa
		If @IDSucursal = 0 Begin
			Set @vDebito = Round(IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes=@Mes And Cuenta=@vCodigo),0),0)
			Set @vCredito = Round(IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes=@Mes And Cuenta=@vCodigo),0),0)
			
		End 
		
		--Por Sucursal
		If @IDSucursal > 0 Begin
			Set @vDebito = Round(IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes=@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0),0)
			Set @vCredito = Round(IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes=@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0),0)
			
		End
		
		--Saldo de CierreContable
		if @IdSucursal = 0 begin
			Set @vDebitoCierre = Round(Isnull((select DA.Debito from Asiento A
								join DetalleAsiento DA on DA.IDTransaccion = A.IDTransaccion
								join CierreReapertura CR on CR.IDtransaccion = A.Idtransaccion
								where CR.Comprobante = 'CIERRE'
								and DA.CuentaContable = @vCodigo
								and Anho = @Año),0),0)
			Set @vCreditoCierre = Round(Isnull((select DA.Credito from Asiento A
								join DetalleAsiento DA on DA.IDTransaccion = A.IDTransaccion
								join CierreReapertura CR on CR.IDtransaccion = A.Idtransaccion
								where CR.Comprobante = 'CIERRE'
								and DA.CuentaContable = @vCodigo
								and Anho = @Año),0),0)
	   End
	   If @IdSucursal > 0 begin
	   Set @vDebitoCierre = Round(Isnull((select DA.Debito from Asiento A
								join DetalleAsiento DA on DA.IDTransaccion = A.IDTransaccion
								join CierreReapertura CR on CR.IDtransaccion = A.Idtransaccion
								where CR.Comprobante = 'CIERRE'
								and DA.CuentaContable = @vCodigo
								AND DA.IDCuentaContable =  @vID
								and CR.IDSucursal =@IdSucursal
								and Anho = @Año),0),0)
				Set @vCreditoCierre = Round(Isnull((select DA.Credito from Asiento A
								join DetalleAsiento DA on DA.IDTransaccion = A.IDTransaccion
								join CierreReapertura CR on CR.IDtransaccion = A.Idtransaccion
								where CR.Comprobante = 'CIERRE'
								and DA.CuentaContable = @vCodigo
								AND DA.IDCuentaContable =  @vID
								and CR.IDSucursal =@IdSucursal
								and Anho = @Año),0),0)
	   end
	   	
		If @vCuentaTipo = 'DEBE' Begin
			Set @vSaldoAnterior = IsNUll(@vDebitoAnterior  - @vCreditoAnterior,0)
			Set @vSaldoActual = @vSaldoAnterior + (@vDebito - @vCredito) + (@vCreditoCierre - @vDebitoCierre)
			Set @vMovimientoMes = round(@vDebito,0) - round(@vCredito,0)
			Set @vMovimientoCierre = @vCreditoCierre - @vDebitoCierre
		End
		
		If @vCuentaTipo = 'HABER' Begin
			Set @vSaldoAnterior = IsNUll(@vCreditoAnterior-@vDebitoAnterior,0)
			Set @vSaldoActual = @vSaldoAnterior + (@vCredito - @vDebito) + (@vDebitoCierre - @vCreditoCierre)
			Set @vMovimientoMes = round(@vCredito,0) - round(@vDebito,0) 
			set @vMovimientoCierre = @vDebitoCierre - @vCreditoCierre
		End
		
		Set @vSaldoAnterior = @vSaldoAnterior / @Factor
		Set @vSaldoActual = @vSaldoActual / @Factor
		
		Set @vDebito = @vDebito / @Factor
		Set @vCredito = @vCredito / @Factor
		Set @vMovimientoMes = @vMovimientoMes / @Factor
		set @vMovimientoCierre = @vMovimientoCierre / @Factor
			
		--Si ya existe, actualizar
		If @IDSucursal = 0 Begin
			If Exists(Select * From #BalanceGeneral Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo) Begin
			
				Update #BalanceGeneral Set 
				SaldoAnterior=SaldoAnterior+@vSaldoAnterior,
				MovimientoMes=MovimientoMes+@vMovimientoMes,
				SaldoActual=SaldoActual+@vSaldoActual,
				MovimientoCierre = MovimientoCierre+@vMovimientoCierre
				Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo
				
			End Else Begin
				Insert Into #BalanceGeneral(ID, Año, Mes,  M, Codigo, Denominacion, Categoria, Imputable, SaldoAnterior, MovimientoMes, MovimientoCierre ,SaldoActual, MostrarCodigo, IDUnidadNegocio, Seccion)
				Values(@vID, @Año, @Mes, dbo.FMes(@Mes), @vCodigo, @vDenominacion, @vCategoria, @vImputable, @vSaldoAnterior, @vMovimientoMes,@vMovimientoCierre, @vSaldoActual, '1', @vIDUnidadNegocio, @vSeccion)
			End
		End
		
		--Por Sucursal
		If @IDSucursal > 0 Begin
			If Exists(Select * From #BalanceGeneral Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo And IDSucursal=@IDSucursal) Begin
			
				Update #BalanceGeneral Set 
				SaldoAnterior=SaldoAnterior+@vSaldoAnterior,
				MovimientoMes=MovimientoMes+@vMovimientoMes,
				SaldoActual=SaldoActual+@vSaldoActual,
				MovimientoCierre = MovimientoCierre+@vMovimientoCierre
				Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo
				
			End Else Begin
				Insert Into #BalanceGeneral(ID, Año, Mes, IDSucursal, M, Codigo, Denominacion, Categoria, Imputable, SaldoAnterior, MovimientoMes,MovimientoCierre, SaldoActual, MostrarCodigo, IDUnidadNegocio, Seccion)
				Values(@vID, @Año, @Mes, @IDSucursal, dbo.FMes(@Mes), @vCodigo, @vDenominacion, @vCategoria, @vImputable, @vSaldoAnterior, @vMovimientoMes,@vMovimientoCierre, @vSaldoActual, '1', @vIDUnidadNegocio, @vSeccion)
			End
		End
		
Siguiente:
		
		Fetch Next From db_cursor Into	@vCodigo, @vDenominacion, @vCategoria, @vImputable, @vIDUnidadNegocio
		
	End

	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor		   		
	
	--RESULTADO DEL EJERCICIO
	Begin
		Declare @vIDSucursal int
		Declare @vCuentaContableSucursal varchar(50)
		
		Declare Sucursal_Cursor cursor for
		Select ID, CuentaContable From Sucursal
		Open Sucursal_Cursor   
		Fetch Next From Sucursal_Cursor Into @vIDSucursal, @vCuentaContableSucursal
		While @@FETCH_STATUS = 0 Begin
		
			Set @vDenominacion = (Select Descripcion From VCuentaContable Where Codigo=@vCuentaContableSucursal)
			Set @vCategoria = (Select Categoria From VCuentaContable Where Codigo=@vCuentaContableSucursal)
			Set @vImputable = (Select Imputable From VCuentaContable Where Codigo=@vCuentaContableSucursal)
			Set @vIDUnidadNegocio = (Select isnull(IDUnidadNegocio,0) From VCuentaContable Where Codigo=@vCuentaContableSucursal)
			--Inicializar
			Set @vSaldoAnterior = 0
			
			--Obtener Saldos Anteriores
			If @Mes > 1 Begin
				Set @vSaldoAnterior = IsNull((Select Sum(Credito) - Sum(Debito) From PlanCuentaSaldo Where IDSucursal=@vIDSucursal And Año=@Año And Mes<@Mes And SubString(Cuenta, 0, 2)='4'),0) 
							         -IsNull((Select Sum(Debito) - Sum(Credito) From PlanCuentaSaldo Where IDSucursal=@vIDSucursal And Año=@Año And Mes<@Mes And SubString(Cuenta, 0, 2)='5'),0)
			End

			Set @vMovimientoMes = IsNull((Select Sum(Credito) - Sum(Debito) From PlanCuentaSaldo Where IDSucursal=@vIDSucursal And Año=@Año And Mes=@Mes And SubString(Cuenta, 0, 2)='4'),0) 
								 -IsNull((Select Sum(Debito) - Sum(Credito) From PlanCuentaSaldo Where IDSucursal=@vIDSucursal And Año=@Año And Mes=@Mes And SubString(Cuenta, 0, 2)='5'),0)
								 
			Set @vMovimientoCierre = Isnull((Select sum(DCR.Debito)-Sum(DCR.Credito) from CierreReapertura CR
									Join DetalleCierreReapertura DCR on CR.IDTransaccion = DCR.IdTransaccion
									Join CuentaContable CC on CC.Id = DCR.IDCuentaContable
									where CR.IDSucursal=@vIDSucursal And CR.Anho=@Año And SubString(CC.Codigo, 0, 2) = '4'),0)
								- Isnull((Select sum(DCR.Credito)-Sum(DCR.Debito) from CierreReapertura CR
									Join DetalleCierreReapertura DCR on CR.IDTransaccion = DCR.IdTransaccion
									Join CuentaContable CC on CC.Id = DCR.IDCuentaContable
									where CR.IDSucursal=@vIDSucursal And CR.Anho=@Año And SubString(CC.Codigo, 0, 2) = '5'),0)				 
			
			Set @vSaldoActual = @vSaldoAnterior + @vMovimientoMes + @vMovimientoCierre
			 
			-- Se modifico para que inserte de nuevo si ya existe la cuenta en #BalanceGeneral dbordon 11-11-2015
			If Exists(Select * From #BalanceGeneral Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCuentaContableSucursal) Begin 							         			
				Update #BalanceGeneral Set 
				SaldoAnterior=SaldoAnterior+@vSaldoAnterior,
				MovimientoMes=MovimientoMes+@vMovimientoMes,
				SaldoActual=SaldoActual+@vSaldoActual,
				MovimientoCierre = MovimientoCierre+@vMovimientoCierre
				Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCuentaContableSucursal
			end else
			begin
				Insert Into #BalanceGeneral(ID, Año, Mes, M, Codigo, Denominacion, Categoria, Imputable, SaldoAnterior, SaldoActual, MovimientoMes, MovimientoCierre, MostrarCodigo, IDUnidadNegocio, Seccion)
				Values(@vID, @Año, @Mes, dbo.FMes(@Mes), @vCuentaContableSucursal, @vDenominacion, @vCategoria, @vImputable, @vSaldoAnterior, @vSaldoActual, @vMovimientoMes, @vMovimientoCierre, '1',@vIDUnidadNegocio,'P A T R I M O N I O  N E T O')
			end
			
			Fetch Next From Sucursal_Cursor Into @vIDSucursal, @vCuentaContableSucursal
			
		End

		--Cierra el cursor
		Close Sucursal_Cursor   
		Deallocate Sucursal_Cursor	
	 
	End
	
	Select * From #BalanceGeneral 
	Where ID=@vID 
	And (Case When @IDUnidadNegocio = 0 then 0 else IDUnidadNegocio end) = (Case When @IDUnidadNegocio = 0 then 0 else @IDUnidadNegocio end)
	order by codigo, SaldoActual

End




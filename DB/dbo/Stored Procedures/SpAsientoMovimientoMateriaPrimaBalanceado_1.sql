﻿CREATE Procedure [dbo].[SpAsientoMovimientoMateriaPrimaBalanceado]

	@IDTransaccion numeric(18,0)
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint = 0

	--Movimiento
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoOperacion smallint
	Declare @vIDTipoComprobante smallint
	Declare @vIDDepositoEntrada smallint
	Declare @vIDDepositoSalida smallint
	Declare @vEntrada bit
	Declare @vSalida bit
	Declare @vComprobante varchar(50)
	Declare @vFecha varchar(50)
	Declare @vObservacion varchar(100)
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money

	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	Declare @vCodigoDepositoEntrada varchar(50)
	Declare @vCodigoDepositoSalida varchar(50)
	Declare @vCodigoProducto varchar(50)

	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money

	Declare @MovimientoStock bit
	Declare @DescargaStock bit
	
	--Obtener valores
	Begin
	
		Set @vIDMoneda = 1
		Set @vCotizacion = 1
		
		Select	@vIDSucursal=IDSucursal,
				@vIDTipoOperacion=IDTipoOperacion,
				@vTipoComprobante=TipoComprobante,
				@vIDTipoComprobante=IDTipoComprobante,
				@vIDDepositoEntrada=IDDepositoEntrada,
				@vIDDepositoSalida=IDDepositoSalida,
				@vEntrada=Entrada,
				@vSalida=Salida,
				@vComprobante=Comprobante,
				@vFecha=Fecha,
				@vObservacion=Observacion,
				@MovimientoStock = MovimientoStock,
				@DescargaStock = DescargaStock
		From VMovimiento 
		Where IDTransaccion=@IDTransaccion 
		and (MovimientoMateriaPrima = 'True') 

	End
					
	--Verificar que el asiento se pueda modificar
	Begin

		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta anulado
		If (Select Anulado From Movimiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El registro esta anulado'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		
	End

	Set @vCodigoDepositoEntrada = IsNull((Select CuentaContableMercaderia From Deposito Where ID=@vIDDepositoEntrada), '')
	Set @vCodigoDepositoSalida = IsNull((Select CuentaContableMercaderia From Deposito Where ID=@vIDDepositoSalida), '')

				
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = @vObservacion
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	
	If @vEntrada=0 And @vSalida=1 and @vIDTipoComprobante = 117 begin --AF30H0050 117
		
		Set @vCodigo = @vCodigoDepositoSalida
		
		Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
		
		Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
		Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

		Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
		Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')
		
		
		--Variables
		Declare cCFTranferencia cursor for
		Select Codigo From VCFMovimiento 
		Where IDTipoOperacion=@vIDTipoOperacion 
		And IDTipoComprobante=@vIDTipoComprobante
		And isnull(Debe,'False') = 'True'
		Open cCFTranferencia
		fetch next from cCFTranferencia into @vCodigo
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			--Entrada
			--Cuenta contable Entrada
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			
			Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
			Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, Round(@vImporte,@vRedondeo), 0, '')
			
			
			fetch next from cCFTranferencia into @vCodigo
			
		End
		
		close cCFTranferencia
		deallocate cCFTranferencia
	end

	If @vEntrada=1 And @vSalida=0 and @vIDTipoComprobante = 132 begin --AF30H0050 Devolucion
		
		Set @vCodigo = @vCodigoDepositoEntrada
		
		Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
		
		Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
		Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

		Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
		Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo,0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')
		
		
		--Variables
		Declare cCFTranferencia cursor for
		Select Codigo From VCFMovimiento 
		Where IDTipoOperacion=9 
		And IDTipoComprobante=117
		And isnull(Debe,'False') = 'True'
		Open cCFTranferencia
		fetch next from cCFTranferencia into @vCodigo
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			--Entrada
			--Cuenta contable Entrada
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			
			Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
			Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo,  Round(@vImporte,@vRedondeo),0, 0, '')
			
			
			fetch next from cCFTranferencia into @vCodigo
			
		End
		
		close cCFTranferencia
		deallocate cCFTranferencia
	end
	--Salidas
	If @vEntrada=0 And @vSalida=1 and @vIDTipoComprobante = 78 Begin
	
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			If @vHaber = 1 Begin

				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCompra, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCompra
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCompra, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
						
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto, @vImporte, 0, @vImporte, '')

						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11140214' And PlanCuentaTitular='True')

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, '11140214', 0,Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')					

					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End			
			
			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End -- CONSB Consumo Balanceado

	If @vEntrada=0 And @vSalida=1 and @vIDTipoComprobante = 79 Begin -- SALBA salida balanceado asu a mol
	 print '@vEntrada=0 And @vSalida=1 and @vIDTipoComprobante = 79'
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			If @vHaber = 1 Begin

				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCompra, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCompra
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCompra, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  

					print @vImporte
					If @vImporte > 0 Begin								
					    --Si es insumo Balanceado
						IF substring(@vCodigoProducto,1,7) = '1112180' Begin 
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto, Round(@vImporte,@vRedondeo), 0, Round(@vImporte,@vRedondeo), '')

						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11121803' And PlanCuentaTitular='True')
							
							if exists(select * from detalleasiento where idtransaccion = @IDTransaccion and IDCuentaContable = @vIDCuentaContable) begin
								Update DetalleAsiento
									Set Debito = Round(@vImporte,@vRedondeo)
									Where IDTransaccion = @IDTransaccion 
									and IDCuentaContable = @vIDCuentaContable
									and CuentaContable = '11121803'
							end
							else begin
								Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
								Values(@IDTransaccion, @vID, @vIDCuentaContable, '11121803', 0,Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')					
							end
							
						End
						--Si es Complemento y aditivo
						IF substring(@vCodigoProducto,1,7) = '1112150' Begin 
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto, Round(@vImporte,@vRedondeo), 0, Round(@vImporte,@vRedondeo), '')

						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11121503' And PlanCuentaTitular='True')

							if exists(select * from detalleasiento where idtransaccion = @IDTransaccion and IDCuentaContable = @vIDCuentaContable) begin
								Update DetalleAsiento
									Set Debito = Round(@vImporte,@vRedondeo)
								Where IDTransaccion = @IDTransaccion 
								and IDCuentaContable = @vIDCuentaContable
								and CuentaContable = '11121503'
							end
							else begin
								Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
								Values(@IDTransaccion, @vID, @vIDCuentaContable, '11121503', 0,Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')
							end



							
						End
					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End			
			
			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End
	
	If @vEntrada=0 And @vSalida=1 and @vIDTipoComprobante = 81 Begin
	
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			If @vHaber = 1 Begin

				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCompra, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCompra
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCompra, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					If @vImporte > 0 Begin		
						IF @vCodigoProducto = '11121502' begin
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='5330703' And PlanCuentaTitular='True')
							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, '5330703',0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')

							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11121602' And PlanCuentaTitular='True')

							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, '11121602', Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')					
						ENd
						IF @vCodigoProducto = '11121802' begin
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='5330703' And PlanCuentaTitular='True')
							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, '5330703',0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')

							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11121902' And PlanCuentaTitular='True')

							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, '11121902', Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')					
						ENd
					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End			
			
			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End -- DESTB Destruccion de Balanceado
	
	If @vEntrada=0 And @vSalida=1 and @vIDTipoComprobante = 95 Begin 
	
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			If @vHaber = 1 Begin

				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCompra, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCompra
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCompra, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
				print(@vImporte)
					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto, Round(@vImporte,@vRedondeo), 0, Round(@vImporte,@vRedondeo), '')

						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						
						--Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11121202' And PlanCuentaTitular='True')
						Set @vIDCuentaContable = (select IDCuentaContable from VCFMovimiento where IDTipoComprobante = 95 and [Debe/Haber] = 'DEBE')

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, '11140101', 0,Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')					

					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End			
			
			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End -- CONTR Consumo Trigo

	If @vEntrada=0 And @vSalida=1 and @vIDTipoComprobante = 97 Begin -- DEST DESECHO TRIGO
	
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			If @vHaber = 1 Begin

				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCosto
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCosto, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto, Round(@vImporte,@vRedondeo), 0, Round(@vImporte,@vRedondeo), '')

						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11121208' And PlanCuentaTitular='True')

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, '11121208', 0,Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')					

					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End			
			
			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End -- DEST DESECHO TRIGO

	If @vEntrada=0 And @vSalida=1 and @vIDTipoComprobante = 99 Begin -- CONBA CONSUMO BALANCEADO
	
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			If @vHaber = 1 Begin

				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCompra, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCompra
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCompra, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto, Round(@vImporte,@vRedondeo), 0, Round(@vImporte,@vRedondeo), '')

						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11140201' And PlanCuentaTitular='True')

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, '11140201', 0,Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')					

					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End			
			
			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End -- CONBA CONSUMO BALANCEADO

	--Entradas

	If @vEntrada=1 And @vSalida=0 and @vIDTipoComprobante = 98 Begin -- PROD PRODUCCION TRIGO
	
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			If @vHaber = 1 Begin

				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCosto
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCosto, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto,0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')

						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11140101' And PlanCuentaTitular='True')
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, '11140101', Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')	
									

					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End			
			
			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End -- PROD PRODUCCION TRIGO

	If @vEntrada=1 And @vSalida=0 and @vIDTipoComprobante = 101 Begin -- PRODBA PRODUCCION BALANCEADO
	
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			If @vHaber = 1 Begin

				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCosto
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCosto, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto,0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')

						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11140101' And PlanCuentaTitular='True')
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, '11140101', Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')	
									

					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End			
			
			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End -- PRODBA PRODUCCION BALANCEADO

	If @vEntrada=1 And @vSalida=0 and @vIDTipoComprobante = 102 Begin -- RECUP RECUPERO BALANCEADO
	
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			If @vHaber = 1 Begin

				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCosto
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCosto, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto,0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')

						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='5330705' And PlanCuentaTitular='True')
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, '5330705', Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')									
					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End			
			
			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End -- RECUP RECUPERO BALANCEADO

	If @vEntrada=1 And @vSalida=0 and @vIDTipoComprobante = 83 Begin
	
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			If @vHaber = 1 Begin

				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCompra, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCompra
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCompra, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto,0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')

						IF @vCodigoProducto = '11121502' Begin
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11121503' And PlanCuentaTitular='True')
							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, '11121503', Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')	
						End	
						IF @vCodigoProducto = '11121802' Begin
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11121803' And PlanCuentaTitular='True')
							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, '11121803', Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')	
						End				

					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End			
			
			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End -- ENTRADA al molino

	--Transferencias

	If @vEntrada=1 And @vSalida=1 and @vIDTipoComprobante = 80 Begin -- DINT dañado interno
	
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			If @vHaber = 1 Begin

				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCompra, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCompra
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCompra, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					If @vImporte > 0 Begin		
						IF @vCodigoProducto = '11121502' begin
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto, Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')

							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11121602' And PlanCuentaTitular='True')

							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, '11121602',0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')					
						ENd
						IF @vCodigoProducto = '11121802' begin
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto, Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')

							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11121902' And PlanCuentaTitular='True')

							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, '11121902',0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')					
						ENd
					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End			
			
			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End -- DESTB Destruccion de Balanceado
	
	If @vEntrada=1 And @vSalida=1 and @vIDTipoComprobante = 82 Begin -- RECUB Recupero de Balanceado
	
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			If @vHaber = 1 Begin

				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCompra, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCompra
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCompra, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					If @vImporte > 0 Begin		
						IF @vCodigoProducto = '11121502' begin
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto, 0,Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')

							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11121602' And PlanCuentaTitular='True')

							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, '11121602',Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')					
						ENd
						IF @vCodigoProducto = '11121802' begin
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto,0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')

							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11121902' And PlanCuentaTitular='True')

							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, '11121902',Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')					
						ENd
					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End			
			
			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End -- DESTB Destruccion de Balanceado
	
	If @vEntrada=1 And @vSalida=1 and @vIDTipoComprobante = 96 Begin -- TRANS TRANSFERENCIA TRIGO
	
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			If @vHaber = 1 Begin

				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCompra, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCompra
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCompra, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					If @vImporte > 0 Begin		
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto,0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')

							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11121203' And PlanCuentaTitular='True')

							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, '11121203',Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')					
					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End			
			
			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End -- TRANS TRANSFERENCIA TRIGO

	If @vEntrada=1 And @vSalida=1 and @vIDTipoComprobante = 100 Begin -- TRANSBA TRANSFERENCIA BALANCEADO
	
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			If @vHaber = 1 Begin

				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCompra, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCompra
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCompra, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					If @vImporte > 0 Begin		
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigoProducto And PlanCuentaTitular='True')
							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigoProducto,0, Round(@vImporte,@vRedondeo), Round(@vImporte,@vRedondeo), '')

							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
						
							Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo='11121202' And PlanCuentaTitular='True')

							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, '11121202',Round(@vImporte,@vRedondeo),0, Round(@vImporte,@vRedondeo), '')					
					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End			
			
			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End -- TRANSBA TRANSFERENCIA BALANCEADO
	
	--TODAS LAS DEMAS
	If @vIDTipoComprobante not in(102,101,100,99,98,97,96,95,83,82,81,80,79,78,117) Begin
	
		--Variables
		Declare cCFTranferencia cursor for
		Select Codigo, Debe, Haber From VCFMovimiento 
		Where IDTipoOperacion=@vIDTipoOperacion 
		And IDTipoComprobante=@vIDTipoComprobante
		Open cCFTranferencia
		fetch next from cCFTranferencia into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			--Entrada
			--Cuenta contable Entrada
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			if @vDebe = 'True' begin
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, Round(@vImporte,@vRedondeo), 0, '')
			end 
			else begin
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporte,@vRedondeo), 0, 0, '')
			end
			
			fetch next from cCFTranferencia into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFTranferencia
		deallocate cCFTranferencia

	End
	
	--Actualizamos la cabecera, el total
	Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
	Set @vImporteHaber = ROUND(@vImporteHaber, @vRedondeo)
	Set @vImporteDebe = ROUND(@vImporteDebe, @vRedondeo)
	
	Update Asiento Set Total = @vImporteHaber,
						Credito = @vImporteHaber,
						Debito = @vImporteDebe,
						Saldo = @vImporteHaber - @vImporteDebe
	Where IDTransaccion=@IDTransaccion
	
	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
	
End


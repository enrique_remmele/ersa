﻿CREATE Procedure [dbo].[SpPerfil]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Estado bit,
	@Visualizar bit,
	@Agregar bit,
	@Modificar bit,
	@Eliminar bit,
	@Anular bit,
	@Imprimir bit,
	@VerCosto bit = 'False',
	@IDDepartamentoEmpresa int=0,
	
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Perfil Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDPerfil tinyint
		set @vIDPerfil = (Select IsNull((Max(ID)+1), 1) From Perfil)

		--Insertamos
		Insert Into Perfil(ID, Descripcion, Estado, Visualizar, Agregar, Modificar, Eliminar, Anular, Imprimir,VerCosto,IDDepartamentoEmpresa)
		Values(@vIDPerfil, @Descripcion, @Estado, @Visualizar, @Agregar, @Modificar, @Eliminar, @Anular, @Imprimir,@VerCosto,@IDDepartamentoEmpresa)		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From Perfil Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Perfil Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Perfil Set Descripcion=@Descripcion,
						 Estado = @Estado,
						 Visualizar=@Visualizar, 
						 Agregar=@Agregar, 
						 Modificar=@Modificar, 
						 Eliminar=@Eliminar, 
						 Anular=@Anular, 
						 Imprimir=@Imprimir,
						 VerCosto = @VerCosto,
						 IDDepartamentoEmpresa=@IDDepartamentoEmpresa
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Perfil Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Productos
		if exists(Select * From Usuario Where IDPerfil=@ID) begin
			set @Mensaje = 'El registro tiene usuarios asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Perfil 
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


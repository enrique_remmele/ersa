﻿CREATE Procedure [dbo].[SpDetalleFormularioImpresion]

	--Entrada
	@IDFormularioImpresion int = NULL,
	@IDDetalleFormularioImpresion tinyint = NULL,
	@NombreCampo varchar(50) = '',
	@Campo varchar(50) = '',
	@PosicionX smallint = 0,
	@PosicionY smallint = 0,
	@TipoLetra varchar(15) = 'Arial',
	@TamañoLetra tinyint = 7,
	@Formato varchar(50) = 'Normal',
	@Valor varchar(100) = '',
	@Detalle bit = 'False',

	@Numerico bit = 'False',
	@Alineacion varchar(15) = 'Izquierda', --Izquierda, Derecha, Centrada 
	@Largo tinyint = 0,
	@PuedeCrecer bit = 'False',
	@Lineas tinyint = 0,
	@MargenPrimeraLinea tinyint = 0,
	@Interlineado tinyint = 0,

	@Operacion varchar(10)

As

Begin

	Declare @vIDDetalleFormularioImpresion int
	Declare @vMensaje varchar(150)
	Declare @vProcesado bit
	
	Set @vMensaje = ''
	Set @vProcesado = 'False'
	Set @vIDDetalleFormularioImpresion = 0
	
	--Insertar
	If @Operacion='INS' Begin
		
		--Validar
		If Exists(Select * From DetalleFormularioImpresion Where IDFormularioImpresion=@IDFormularioImpresion And NombreCampo=@NombreCampo) Begin
			Set @vMensaje = 'El el campo ya existe!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		Set @vIDDetalleFormularioImpresion = (Select IsNull(Max(IDDetalleFormularioImpresion)+1, 1) From DetalleFormularioImpresion Where IDFormularioImpresion=@IDFormularioImpresion)
		
		Insert Into DetalleFormularioImpresion(IDFormularioImpresion, IDDetalleFormularioImpresion, NombreCampo, Campo, PosicionX, PosicionY, TipoLetra, TamañoLetra, Formato, Valor, Detalle)
		Values(@IDFormularioImpresion, @vIDDetalleFormularioImpresion, @NombreCampo, @Campo, @PosicionX, @PosicionY, @TipoLetra, @TamañoLetra, @Formato, @Valor, @Detalle)
		
		Set @vMensaje = 'Registro insertado!'
		Set @vProcesado = 'True'
		GoTo Salir

	End
	
	--Actualizar
	If @Operacion='UPD' Begin
		
		--Validar
		If Not Exists(Select * From DetalleFormularioImpresion Where IDFormularioImpresion=@IDFormularioImpresion And IDDetalleFormularioImpresion=@IDDetalleFormularioImpresion) Begin
			Set @vMensaje = 'El sistema no encuentra el registro solicitado!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		Update DetalleFormularioImpresion Set	PosicionX=@PosicionX,
										PosicionY=@PosicionY,
										TipoLetra=@TipoLetra,
										TamañoLetra=@TamañoLetra,
										Formato=@Formato,
										Numerico=@Numerico,
										Alineacion=@Alineacion,
										Largo=@Largo,
										PuedeCrecer=@PuedeCrecer,
										Lineas=@Lineas,
										MargenPrimeraLinea=@MargenPrimeraLinea,
										Interlineado=@Interlineado
		Where IDFormularioImpresion=@IDFormularioImpresion And IDDetalleFormularioImpresion=@IDDetalleFormularioImpresion
						
		Set @vMensaje = 'Registro actualizado!'
		Set @vProcesado = 'True'
		GoTo Salir

	End
	
	--Eliminar
	If @Operacion='DEL' Begin
		
		--Validar
		If Not Exists(Select * From DetalleFormularioImpresion Where IDFormularioImpresion=@IDFormularioImpresion And IDDetalleFormularioImpresion=@IDDetalleFormularioImpresion) Begin
			Set @vMensaje = 'El sistema no encuentra el registro solicitado!'
			Set @vProcesado = 'False'
			GoTo Salir
		End

		Delete From DetalleFormularioImpresion
		Where IDFormularioImpresion=@IDFormularioImpresion And IDDetalleFormularioImpresion=@IDDetalleFormularioImpresion
				
		Set @vMensaje = 'Registro eliminado!'
		Set @vProcesado = 'True'

	End
			
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	
End


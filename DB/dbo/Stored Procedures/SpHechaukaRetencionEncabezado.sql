﻿CREATE Procedure [dbo].[SpHechaukaRetencionEncabezado]

	@Año smallint,
	@Mes tinyint,
	@TipoReporte tinyint
	
	
As
Begin

	

	Select
	'TipoRegistro'=1,
	'Periodo'= convert( varchar(20), @Año) + CONVERT(varchar(20), dbo.FFormatoDosDigitos(@Mes)),
	'TipoReporte'= @TipoReporte ,
	'CodigoObligacion'= 931,
	'CodigoFormulario'= 231,
	'RUCAgente'=(Select RUC From DatoEmpresa),
	'DVAgente'= (Select DVRUC From DatoEmpresa),
	'NombreAgente'= (Select RazonSocial From DatoEmpresa),
	'RUCRepresentante'= (Select RucPropietario From DatoEmpresa),
	'DVRepresentante'=(Select DVRUCPropietario From DatoEmpresa),
	'NombreRepresentante'=(Select Propietario From DatoEmpresa),
	'CantidadRegistros'=(Select count (*)From VRetencionIVA RI Where YEAR(RI.fecha) = @Año And MONTH(RI.Fecha) = @Mes And RI.Anulado= 'False') ,
	'MontoTotal'= (Select SUM(Total * Cotizacion) 
					From VRetencionIVA Where YEAR(fecha) = @Año And MONTH(Fecha) = @Mes And Anulado='False')
	
	
End






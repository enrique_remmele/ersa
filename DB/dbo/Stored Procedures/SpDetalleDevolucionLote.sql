﻿CREATE Procedure [dbo].[SpDetalleDevolucionLote]

	--Entrada
	@IDTransaccion numeric(18,0),
	@IDProducto int = NULL,
	@Cantidad decimal(10,2) = NULL,
	@CantidadCaja smallint = NULL,
	@Operacion varchar(20),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin

	--Validar
	--Transaccion
	If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--BLOQUES
	if @Operacion='INS' Begin
	
		--Insertar el detalle
		Insert Into DetalleDevolucionLote(IDTransaccion, IDProducto, Cantidad, CantidadCaja)
		Values(@IDTransaccion, @IDProducto, @Cantidad, @CantidadCaja)
		
		Set @Procesado = 'False'
		return @@rowcount					
			
	End
	
	if @Operacion='DEL' Begin
		
		--Validar
		--Transaccion
		If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra la transaccion!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		Delete From DetalleDevolucionLote Where IDTransaccion=@IDTransaccion
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	if @Operacion='ANULAR' Begin
		
		--Verificar que exista
		If Not Exists(Select * From DetalleMovimiento Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El Registro ya esta anulado!'
			Set @Procesado = 'True'
			return @@rowcount
		End
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	return @@rowcount
	
End



﻿CREATE Procedure [dbo].[SpClonarAccesoMenu]

	--Entrada
	@TagOriginal varchar(50),
	@TagNuevo varchar(50),
	
	--Auditoria
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDTerminal int
	
As

Begin

	--Primero Eliminamos todos los accesos
	--Delete From AccesoPerfil Where IDMenu=(Select Codigo from menu where Tag = @TagNuevo)
	
	--Variables
	declare @vIDPerfil int
	declare @vVisualizar bit
	declare @vAgregar bit
	declare @vModificar bit
	declare @vEliminar bit
	declare @vAnular bit
	declare @vImprimir bit
	declare @vIDMenuClonar int = (Select Codigo from menu where Tag = @TagNuevo)
	declare @vNombreControl varchar(200) = (select NombreControl from Menu where Tag = @TagNuevo)
	declare @vNombreControlOriginal varchar(200)= (select NombreControl from Menu where tag = @TagOriginal)
	
	--Ahora recorremos el cursor del perfil matriz e insertamo
	--con el nuevo a clonar
	Declare db_cursor cursor for
	Select 
	IDPerfil, 
	Visualizar, 
	Agregar, 
	Modificar, 
	Eliminar, 
	Anular, 
	Imprimir 
	From AccesoPerfil 
	Where  NombreControl = @vNombreControlOriginal
	Open db_cursor   
	Fetch Next From db_cursor Into @vIDPerfil, @vVisualizar, @vAgregar, @vModificar, @vEliminar, @vAnular, @vImprimir
	While @@FETCH_STATUS = 0 Begin  
	
		if exists (select * from AccesoPerfil where IDPerfil = @vIDPerfil and NombreControl = @vNombreControl) begin
			goto seguir
		end

		Insert Into AccesoPerfil(IDMenu, IDPerfil, Visualizar, Agregar, Modificar, Eliminar, Anular, Imprimir,NombreControl)
		Values(@vIDMenuClonar, @vIDPerfil, @vVisualizar, @vAgregar, @vModificar, @vEliminar, @vAnular, @vImprimir,@vNombreControl)
		
		print concat('IDMenu= ',@vIDMenuClonar, ' IDPerfil=',@vIDPerfil, ' NombreControl=',@vNombreControl)
		
		Fetch Next From db_cursor Into @vIDPerfil, @vVisualizar, @vAgregar, @vModificar, @vEliminar, @vAnular, @vImprimir
Seguir:

	End
	
	Close db_cursor   
	Deallocate db_cursor		   			
	
End


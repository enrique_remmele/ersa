﻿create Procedure [dbo].[SpAplicacionAnticipoAProveedores]

	@IDTransaccion numeric(18,0) = NULL,
	@Numero int = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante varchar(50) = NULL,
	@IDSucursalOperacion	tinyint	= NULL,
	@Fecha date = NULL,
	@IDProveedor int = NULL,
	@IDMoneda tinyint = 1,
	@Observacion varchar(500) = NULL,
	@TotalEgreso money = 0,
	@TotalOrdenPago money = 0,
		
	--Operacion
	@Operacion varchar(50),
	@IDMotivoAnulacion integer = 0,
	@IDUsuarioAnulado smallint = NULL,
	@FechaAnulado date =  NULL,

	@vIdTransaccion numeric(18,0) = NULL,

	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output,
	
	--Variables de recuperacion de Datos
	@IdTransaccionOrdenPago numeric(18,0) = NULL
	--@IdTransaccionProveedores numeric(18,0) = NULL
As

Begin
	--Variables
	 Declare @vCancelado bit

	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from AplicacionAnticipoProveedores where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'ANULAR' Begin
		set @Fecha = (Select Fecha from AplicacionAnticipoProveedores where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 


	If @Operacion = 'INS' Begin	

	--Numero
	Set @Numero = ISNULL((Select MAX(Numero+1) From AplicacionAnticipoProveedores Where IDSucursal=@IDSucursalOperacion),1)
			
		--Insertar Transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
			
		--Insertar en Orden de Pago
		Insert Into AplicacionAnticipoProveedores(IDTransaccion,Numero,IDTipoComprobante, NroComprobante,IDSucursal,Fecha,IDProveedor,IDMoneda,Observacion,TotalEgreso,TotalOrdenPago,Procesado,Anulado)
		Values(@IDTransaccionSalida, @Numero, @IDTipoComprobante, @NroComprobante, @IDSucursalOperacion,@Fecha,@IDProveedor,@IDMoneda,@Observacion,@TotalEgreso,@TotalOrdenPago,'True','False')
		
		set @Mensaje = 'Registro guardado y Procesado'
	    set @Procesado = 'True'
		return @@rowcount	
	
	End

	If @Operacion = 'ANULAR' Begin
	
		--Validar
		--Asientos Cerrados
		If Exists(Select * From Asiento A Where A.IDTransaccion=@IDTransaccion And A.Conciliado='True') Begin
			set @Mensaje = 'El asiento de este documento ya esta conciliado! No se puede anular.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Verificar dias de bloqueo
		If (Select Top(1) OrdenPagoBloquearAnulacion  From Configuraciones where IDSucursal = @IDSucursal) = 'True' Begin
			
			Declare @vDias tinyint
			Set @vDias = (Select Top(1) OrdenPagoDiasBloqueo From Configuraciones where IDSucursal = @IDSucursal)

			If (Select DATEDIFF(dd,(Select V.Fecha From AnticipoProveedores V Where IDTransaccion=@IDTransaccion), GETDATE())) > @vDias Begin
			 			
				set @Mensaje = 'El sistema no puede anular este registro ya que la configuracion lo restringe por la antigüedad del documento. Cambie la configuracion de bloqueo para anular!'
				set @Procesado = 'False'
				return @@rowcount
				
			End
			
		End		
		
		--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion

		--set @IdTransaccionProveedores =(select d.IdTransaccionProveedores from AplicacionAnticipoProveedoresDetalle d where d.IDTransaccion=@IDTransaccion)
	    set @IdTransaccionOrdenPago =(select distinct d.IdTransaccionOrdenPago from AplicacionAnticipoProveedoresDetalle d where d.IDTransaccion=@IDTransaccion)
	
		--Procesar
		--Reestablecer los egresos
		--Exec SpOrdenPagoProcesar @IDTransaccion = @IdTransaccionOrdenPago, @Operacion = @Operacion, @Procesado = @Procesado, @Mensaje = @Mensaje

		--Eliminamos las Relaciones
		--Delete From OrdenPagoEgreso Where IDTransaccionOrdenPago = @IdTransaccionOrdenPago
	
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		set @FechaAnulado = (select fecha from DocumentoAnulado where IDTransaccion = @IDTransaccion )
		set @IDUsuarioAnulado = (select IDUsuario from DocumentoAnulado where IDTransaccion = @IDTransaccion )

		--Anulamos el Registro
		Update AplicacionAnticipoProveedores 
		Set Anulado = 'True', 
		    IDMotivoAnulacion = @IDMotivoAnulacion,
			IDUsuarioAnulado = @IDUsuarioAnulado,
	        FechaAnulado = @FechaAnulado
		Where IDTransaccion = @IDTransaccion

		--Actualiza el estado de la Orden de Pago para que ya no habilite en las proximas busquedas
		update OrdenPago
		set Cancelado =  'False'
		where IDTransaccion = @IdTransaccionOrdenPago
		
		
		set @Mensaje = 'Registro Anulado y Guardado'
		set @Procesado = 'True'
		return @@rowcount	
		
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
	
	
End

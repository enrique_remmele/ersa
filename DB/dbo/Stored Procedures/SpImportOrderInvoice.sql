﻿CREATE procedure [dbo].[SpImportOrderInvoice]

@IDSucursal int,
@Desde date,
@Hasta date

As

begin



	Select 

	'cdOrder'='P'+''+convert(Varchar(50),ReferenciaSucursal)+'-'+convert(varchar(50),ReferenciaPunto)+'-'+convert(varchar(50),NroComprobante),
	'cdCreationUser'=IDUsuario,
	'cdStore'=IDCliente,
	'cdRegion'=IDZonaVenta,
	'dtOrder'=CONVERT(varchar(5),Year(Fecha)) + '-' + CONVERT(varchar(5), dbo.FFormatoDosDigitos( Month(Fecha))) + '-' + CONVERT(varchar(5), dbo.FFormatoDosDigitos( Day(Fecha))),
	'vlDiscountTotal'=TotalDescuento,
	'vlTotalOrder'=Total,
	'vlTotalUnit'=(Select COUNT(*) From DetalleVenta DV Where DV.IDTransaccion=V.IDTransaccion),
	'cdRoute'='RUTA_VENDEDOR_'+''+convert(Varchar(50),IDVendedor),
	'cdStatusOrder'=(Case When Procesado=1 Then 'APPRO'Else'CANC'End),
	'cdInvoice'=[Cod.]+''+convert(Varchar(50),ReferenciaSucursal)+'-'+convert(varchar(50),ReferenciaPunto)+'-'+convert(varchar(50),NroComprobante),
	'dsDisplayText'='-----',
	'dtInvoice'=CONVERT(varchar(5),Year(Fecha)) + '-' + CONVERT(varchar(5), dbo.FFormatoDosDigitos( Month(Fecha))) + '-' + CONVERT(varchar(5), dbo.FFormatoDosDigitos( Day(Fecha))),
	'vlTotalInvoice'=TotalDiscriminado - IsNull((Select Sum(TotalDiscriminado) From VVentaDevoluciones V2 Where V2.IDTransaccion=V.IDTransaccion),0),
	'nrTotalQuantity'=(Select Sum(Cantidad) From DetalleVenta DV Where DV.IDTransaccion=V.IDTransaccion),
	'cdStatusInvoice'=(Case When Cancelado=1 Then 'LIBER'Else'CANC'End)

	From VVenta V 
	Where IDSucursal=@IDSucursal 
	And Fecha Between @Desde And @Hasta
	And V.Anulado='False'
	
End


﻿CREATE Procedure [dbo].[SpOrdenPagoProcesar]

	--Entrada
	@IDTransaccion numeric(18),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

    ---Variables
    Declare @vIDTransaccionEgreso numeric(18,0)
    Declare @vIDTransaccionEfectivo numeric(18,0)
    Declare @vIDEfectivo tinyint
    Declare @vImporte money
    Declare @vSaldo money
    Declare @vCancelado bit
    Declare @vTotal money
    Declare @vTotalOP money
    Declare @vPagado bit
    Declare @vEsCuota bit
	Declare @vCuota smallint

	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'

	Set @vTotalOP = 0
	
	If @Operacion = 'INS' Begin
					
		--Saldar COMPRAS
		If Exists(Select * From OrdenPagoEgreso OP JOin Compra C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion And Procesado='False') Begin
			Begin
				Declare db_cursor cursor for
				Select OP.IDTransaccionEgreso, OP.Importe, C.Saldo, C.Cancelado, C.Total From OrdenPagoEgreso OP JOin Compra C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion
				Open db_cursor   
				Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte, @vSaldo, @vCancelado, @vTotal
				While @@FETCH_STATUS = 0 Begin  
					
					--Calculamos el Saldo
					Set @vSaldo = @vSaldo - @vImporte
					If @vSaldo < 0 Begin
						Set @vSaldo = @vSaldo * -1
					End
					
					--Cancelar si saldo = 0
					If @vSaldo = 0 Begin
						Set @vCancelado = 'True'
					End
					
					--Actualizar
					Update Compra Set Saldo = @vSaldo, Cancelado = @vCancelado
					Where IDTransaccion = @vIDTransaccionEgreso
				
					--Establecemos procesado el egreso
					Update OrdenPagoEgreso Set Procesado = 'True', ProcesaRetencion = 'False'
					Where IDTransaccionOrdenPago=@IDTransaccion and IDTransaccionEgreso=@vIDTransaccionEgreso
					
					Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte, @vSaldo, @vCancelado, @vTotal
									
				End
				
				--Cierra el cursor
				Close db_cursor   
				Deallocate db_cursor		   			
					
				set @Mensaje = 'Registro guardado!'
				set @Procesado = 'True'
				
			End
		End
		
		--Saldar Gastos
		If Exists(Select * From OrdenPagoEgreso OP Join Gasto C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion And Procesado='False') Begin
			Begin
				Declare db_cursor cursor for
				Select OP.IDTransaccionEgreso, OP.Importe, C.Saldo, C.Cancelado, C.Total, 'EsCuota'=Case When (IsNull(C.Cuota, 1))>1 Then 'True' Else 'False' End, OP.Cuota From OrdenPagoEgreso OP JOin Gasto C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion
				Open db_cursor   
				Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte, @vSaldo, @vCancelado, @vTotal, @vEsCuota, @vCuota
				While @@FETCH_STATUS = 0 Begin  
					
					--Calculamos el Saldo
					Set @vSaldo = @vSaldo - @vImporte
					If @vSaldo < 0 Begin
						Set @vSaldo = @vSaldo * -1
					End
					
					--Cancelar si saldo = 0
					If @vSaldo = 0 Begin
						Set @vCancelado = 'True'
					End
					
					--Actualizar
					Update Gasto Set Saldo = @vSaldo, 
									Cancelado = @vCancelado,
									Pagar = 'False'
					Where IDTransaccion = @vIDTransaccionEgreso
					
					--Actualizar Detalle Fondo Fijo Gasto Fondo Fijo
					Update DetalleFondoFijo Set Cancelado= 'True'
					Where IDTransaccionGasto=@vIDTransaccionEgreso 
				
					--Establecemos procesado el egreso
					Update OrdenPagoEgreso Set Procesado = 'True', ProcesaRetencion = 'False'
					Where IDTransaccionOrdenPago=@IDTransaccion and IDTransaccionEgreso=@vIDTransaccionEgreso
					
					--Si es un pago de cuota
					If @vEsCuota = 'True' Begin
						Update Cuota Set Saldo=Saldo-@vImporte,
										 Cancelado=Case When(Saldo-@vImporte) < 1 Then 'True' Else 'False' End,
										 Pagar='False'
						Where IDTransaccion=@vIDTransaccionEgreso And ID=@vCuota  
					End
						
					Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte, @vSaldo, @vCancelado, @vTotal, @vEsCuota, @vCuota
									
				End
				
				--Cierra el cursor
				Close db_cursor   
				Deallocate db_cursor		   			
					
				set @Mensaje = 'Registro guardado!'
				set @Procesado = 'True'
				
			End
		End	
		
		--Cambiar Estado Vale
		If Exists(Select * From OrdenPagoEgreso OP Join Vale C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion And Procesado='False') Begin
			Begin
				Declare db_cursor cursor for
				Select OP.IDTransaccionEgreso From OrdenPagoEgreso OP JOin Vale C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion
				Open db_cursor   
				Fetch Next From db_cursor Into @vIDTransaccionEgreso
				While @@FETCH_STATUS = 0 Begin  
				
					--Actualizar Vale
					Update Vale Set Pagado = 'True',Cancelado='True', Pagar='False'
					Where IDTransaccion = @vIDTransaccionEgreso
					
					--Actualizar Detalle Fondo Fijo Vale
					Update DetalleFondoFijo Set Cancelado= 'True'
					Where IDTransaccionVale=@vIDTransaccionEgreso 
					
								
					--Establecemos procesado el egreso
					Update OrdenPagoEgreso Set Procesado = 'True', ProcesaRetencion = 'False'
					Where IDTransaccionOrdenPago=@IDTransaccion and IDTransaccionEgreso=@vIDTransaccionEgreso
						
					Fetch Next From db_cursor Into @vIDTransaccionEgreso
									
				End
				
				--Cierra el cursor
				Close db_cursor   
				Deallocate db_cursor		   			
					
				set @Mensaje = 'Registro guardado!'
				set @Procesado = 'True'
				
			End
		End	
		
		--Saldar Efectivo
		If Exists(Select * From OrdenPagoEfectivo OP Where OP.IDTransaccionOrdenPago=@IDTransaccion) Begin
			Begin
				Declare db_cursor cursor for
				Select OPE.IDTransaccionEfectivo, OPE.IDEfectivo, OPE.Importe From OrdenPagoEfectivo OPE Where OPE.IDTransaccionOrdenPago=@IDTransaccion
				Open db_cursor   
				Fetch Next From db_cursor Into @vIDTransaccionEfectivo, @vIDEfectivo, @vImporte
				While @@FETCH_STATUS = 0 Begin  
				
					Set @vCancelado = 'False'
					
					--Obtenemos el saldo actual del Efectivo
					Set @vSaldo = (Select IsNull(Saldo, ImporteMoneda) from Efectivo where IDTransaccion=@vIDTransaccionEfectivo And ID=@vIDEfectivo)  
					
					--Calculamos el Saldo
					Set @vSaldo = @vSaldo - @vImporte
					If @vSaldo < 0 Begin
						Set @vSaldo = @vSaldo * -1
					End
					
					--Cancelar si saldo = 0
					If @vSaldo = 0 Begin
						Set @vCancelado = 'True'
					End
					
					--Actualizar
					Update Efectivo Set Saldo = @vSaldo, Cancelado = @vCancelado, Habilitado='False',ImporteHabilitado=@vSaldo
					Where IDTransaccion = @vIDTransaccionEfectivo And ID=@vIDEfectivo
					
					Set @vTotalOP = @vTotalOP + @vImporte
					
					Fetch Next From db_cursor Into @vIDTransaccionEfectivo, @vIDEfectivo, @vImporte
									
				End
				
				--Cierra el cursor
				Close db_cursor   
				Deallocate db_cursor		   			
					
				set @Mensaje = 'Registro guardado!'
				set @Procesado = 'True'
				
			End
		End	
		
		Set @vTotalOP = @vTotalOP + IsNull((Select Importe From Cheque Where IDTransaccion=@IDTransaccion),0) + isnull((select ImporteMoneda from FormaPagoDocumento where IdTransaccion = @IDTransaccion),0)
		
		Update OrdenPago Set Total = @vTotalOP 
		Where IDTransaccion=@IDTransaccion
		
		--Asiento solo si no es Egreso a Rendir
		if (Select EgresoRendir from OrdenPago Where IDTransaccion=@IDTransaccion)=0 begin
			Exec spAsientoOrdenPago @IDTransaccion
		end
				
	End	
	
	If @Operacion = 'ANULAR' Begin	
	
						
		--Saldar COMPRAS
		If Exists(Select * From OrdenPagoEgreso OP JOin Compra C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion) Begin
			Begin
				Declare db_cursor cursor for
				Select OP.IDTransaccionEgreso, OP.Importe, C.Saldo, C.Cancelado, C.Total From OrdenPagoEgreso OP JOin Compra C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion
				Open db_cursor   
				Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte, @vSaldo, @vCancelado, @vTotal
				While @@FETCH_STATUS = 0 Begin  
					
					--Calculamos el Saldo
					Set @vSaldo = @vSaldo + @vImporte
					Set @vCancelado = 'False'				
					
					
					--Actualizar
					Update Compra Set Saldo = @vSaldo, Cancelado = @vCancelado, Pagar='True'
					Where IDTransaccion = @vIDTransaccionEgreso
					
					Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte, @vSaldo, @vCancelado, @vTotal
									
				End
				
				--Cierra el cursor
				Close db_cursor   
				Deallocate db_cursor		   			
					
				set @Mensaje = 'Registro guardado!'
				set @Procesado = 'True'
				
			End
		End
		
		--Saldar Gastos
		If Exists(Select * From OrdenPagoEgreso OP JOin Gasto C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion) Begin
			Begin
				Declare db_cursor cursor for
				Select OP.IDTransaccionEgreso, OP.Importe, C.Saldo, C.Cancelado, C.Total, 'EsCuota'=Case When (IsNull(C.Cuota, 1))>1 Then 'True' Else 'False' End, OP.Cuota From OrdenPagoEgreso OP JOin Gasto C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion
				Open db_cursor   
				Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte, @vSaldo, @vCancelado, @vTotal, @vEsCuota, @vCuota
				While @@FETCH_STATUS = 0 Begin  
					
					--Calculamos el Saldo
					Set @vSaldo = @vSaldo + @vImporte
					Set @vCancelado = 'False'				
					
					--Actualizar
					Update Gasto Set Saldo = @vSaldo, Cancelado = @vCancelado
					Where IDTransaccion = @vIDTransaccionEgreso
					
					--Actualizar Detalle Fondo Fijo Gasto Fondo Fijo
					Update DetalleFondoFijo Set Cancelado= 'False'
					Where IDTransaccionGasto=@vIDTransaccionEgreso 
					
					--Si es un pago de cuota
					If @vEsCuota = 'True' Begin
						if (select importe from cuota where IDTransaccion = @IDTransaccion and id = @vCuota) < (Select Saldo+@vImporte from cuota where IDTransaccion = @IDTransaccion and id = @vCuota) begin
							Update Cuota Set Saldo=Importe,
								Cancelado='False',
								Pagar='True'
								Where IDTransaccion=@vIDTransaccionEgreso And ID=@vCuota	
						end else begin
							Update Cuota Set Saldo=Saldo+@vImporte,
								Cancelado='False',
								Pagar='True'
								Where IDTransaccion=@vIDTransaccionEgreso And ID=@vCuota
						end
						
					End

					Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte, @vSaldo, @vCancelado, @vTotal, @vEsCuota, @vCuota
									
				End
				
				--Cierra el cursor
				Close db_cursor   
				Deallocate db_cursor		   			
					
				set @Mensaje = 'Registro guardado!'
				set @Procesado = 'True'
				
			End
		End	
				
		--Cambiar Estado Vale
		If Exists(Select * From OrdenPagoEgreso OP Join Vale C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion) Begin
			Begin
				Declare db_cursor cursor for
				Select OP.IDTransaccionEgreso From OrdenPagoEgreso OP JOin Vale C On OP.IDTransaccionEgreso=C.IDTransaccion Where OP.IDTransaccionOrdenPago=@IDTransaccion
				Open db_cursor   
				Fetch Next From db_cursor Into @vIDTransaccionEgreso
				While @@FETCH_STATUS = 0 Begin  
				
					--Actualizar Vale
					Update Vale Set Pagado = 'False',Cancelado='False'
					Where IDTransaccion = @vIDTransaccionEgreso
					
					--Actualizar Detalle Fondo Fijo
					Update DetalleFondoFijo Set Cancelado= 'False'
					Where IDTransaccionVale=@vIDTransaccionEgreso 
					
											
					Fetch Next From db_cursor Into @vIDTransaccionEgreso
									
				End
				
				--Cierra el cursor
				Close db_cursor   
				Deallocate db_cursor		   			
					
				set @Mensaje = 'Registro guardado!'
				set @Procesado = 'True'
				
			End
		End	
				
		--Saldar Efectivo
		If Exists(Select * From OrdenPagoEfectivo OP Where OP.IDTransaccionOrdenPago=@IDTransaccion) Begin
			Begin
				Declare db_cursor cursor for
				Select OPE.IDTransaccionEfectivo, OPE.IDEfectivo, OPE.Importe From OrdenPagoEfectivo OPE Where OPE.IDTransaccionOrdenPago=@IDTransaccion
				Open db_cursor   
				Fetch Next From db_cursor Into @vIDTransaccionEfectivo, @vIDEfectivo, @vImporte
				While @@FETCH_STATUS = 0 Begin  
				
					--Obtenemos el saldo actual del Efectivo
					Set @vSaldo = (select Saldo from Efectivo where IDTransaccion=@vIDTransaccionEfectivo And ID=@vIDEfectivo)  
					
					--Calculamos el Saldo
					Set @vSaldo = @vSaldo + @vImporte
					
					--Cancelar
					Set @vCancelado = 'False'
										
					--Actualizar
					Update Efectivo Set Saldo = @vSaldo, Cancelado = @vCancelado, Habilitado='True',ImporteHabilitado=@vSaldo
					Where IDTransaccion = @vIDTransaccionEfectivo And ID=@vIDEfectivo
					
					Fetch Next From db_cursor Into @vIDTransaccionEfectivo, @vIDEfectivo, @vImporte
									
				End
				
				--Cierra el cursor
				Close db_cursor   
				Deallocate db_cursor		   			
				
				Delete From OrdenPagoEfectivo Where IDTransaccionOrdenPago=@IDTransaccion
					
				set @Mensaje = 'Registro guardado!'
				set @Procesado = 'True'
				
			End
		End	
		
	End			
	
		
End
	





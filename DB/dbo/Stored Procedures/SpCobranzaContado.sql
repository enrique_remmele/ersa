﻿CREATE Procedure [dbo].[SpCobranzaContado]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@Numero int = NULL,
	@IDTransaccionLote numeric(18,0) = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante varchar(50) = NULL,
	@Comprobante varchar(50) = NULL,	
	@Fecha date = NULL,
	@IDMotivoAnulacion varchar(50) = NULL,
	
	--Totales
	@Total money = NULL,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@TotalDescuento money = NULL,
	@Observacion varchar(200) = NULL,
	@Operacion varchar(50),
	@IDMoneda tinyint = 1,
	@Cotizacion money = 1,
	@DiferenciaCambio money = 0,

		
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
	
--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from CobranzaContado where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		If Exists(Select * From CobranzaContado Where Numero=@Numero AND IDSucursal=@IDSucursalOperacion) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Si ya existe el documento
		If Exists(Select * From CobranzaContado Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante And IDSucursal=@IDSucursalOperacion And Anulado='False') Begin
			set @Mensaje = 'El numero de comprobante ya existe!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		----Si es que existe el lote
		If Not Exists(Select * From LoteDistribucion Where IDTransaccion=@IDTransaccionLote) Begin
			set @Mensaje = 'El número de Lote no existe!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Total
		If @Total <= 0 Begin
			set @Mensaje = 'El importe debe ser mayor a 0!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Insertar Transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
			
		--Insertar en Cobranza Contado
		Insert Into CobranzaContado(IDTransaccion, IDSucursal, Numero, IDTransaccionLote, IDTipoComprobante, NroComprobante, Comprobante, Fecha, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, Anulado, FechaAnulado, IDUsuarioAnulado, Observacion, IDMoneda, Cotizacion)
		Values(@IDTransaccionSalida, @IDSucursalOperacion, @Numero, @IDTransaccionLote, @IDTipoComprobante, @NroComprobante, @Comprobante, @Fecha, @Total, @TotalImpuesto, @TotalDiscriminado, @TotalDescuento, 'False', NULL, NULL, @Observacion,1,1)
					
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	--ANULAR
	If @Operacion = 'ANULAR' Begin
	
		--Validar
		--Si el efectivo esta depositado
		--If (Select Depositado From Efectivo Where IDTransaccion=@IDTransaccion) > 0 Begin
		--	set @Mensaje = 'Parte o el parcial del importe pagado en efectivo ya fue depositado! No se puede anular el registro.'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		--Si los cheques estan depositados
		If Exists (Select CC.Depositado From FormaPago FP Join ChequeCliente CC on FP.IDTransaccionCheque=CC.IDTransaccion Where FP.IDTransaccion=@IDTransaccion And CC.Depositado='True') Begin
			set @Mensaje = 'El documento se pago con un cheque que ya fue depositado! No se puede anular el registro.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si los efectivos estan depositados
		If Exists (Select FP.Efectivo From FormaPago FP Join Efectivo e on FP.IDTransaccion =e.IDTransaccion 
		Where FP.IDTransaccion=@IDTransaccion and FP.efectivo = 'True' And E.Depositado > 0) Begin
			set @Mensaje = 'El documento se pago con efectivo que ya fue depositado! No se puede anular el registro.'
			set @Procesado = 'False'
			return @@rowcount
		End		
				
		--Asientos Cerrados
		If Exists(Select * From Asiento A Where A.IDTransaccion=@IDTransaccion And A.Conciliado='True') Begin
			set @Mensaje = 'El asiento de este documento ya esta conciliado! No se puede anular.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Verificar dias de bloqueo
		If (Select Top(1) CobranzaContadoBloquearAnulacion From Configuraciones) = 'True' Begin
			
			Declare @vDias tinyint
			Set @vDias = (Select Top(1) CobranzaContadoDiasBloqueo From Configuraciones)

			If (Select DATEDIFF(dd,(Select V.Fecha From CobranzaContado V Where IDTransaccion=@IDTransaccion), GETDATE())) > @vDias Begin
			 			
				set @Mensaje = 'El sistema no puede anular este registro ya que la configuracion lo restringe por la antigüedad del documento. Cambie la configuracion de bloqueo para anular!'
				set @Procesado = 'False'
				return @@rowcount
				
			End
			
		End
		
		--Procesar
		--Reestablecemos las Ventas y Saldo del Cliente
		EXEC SpVentaCobranza @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Reestablecemos el cheque
		EXEC SpChequeClienteSaldar @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la forma de pago		
		Delete From Efectivo Where IDTransaccion = @IDTransaccion
		Delete From FormaPagoDocumento Where IDTransaccion = @IDTransaccion
		Delete From FormaPago Where IDTransaccion = @IDTransaccion

		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from CobranzaContado where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from CobranzaContado where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from CobranzaContado where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select Comprobante from CobranzaContado where IDTransaccion = @IDTransaccion)
		declare @VTipoComprobante varchar(16) = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from CobranzaContado where IDTransaccion = @IDTransaccion)
		declare @Condicion varchar(16) = 'CONTADO'

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,0,0,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ANU',@VTipoComprobante, @IDSucursal)

				
		--Anulamos el registro
		Update CobranzaContado Set Anulado='True', IDUsuarioAnulado=@IDUsuario, FechaAnulado=GETDATE(), IDMotivoAnulacion=@IDMotivoAnulacion
		Where IDTransaccion = @IDTransaccion
		
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal		
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		Goto Salir
		
	End
	
	--ELIMINAR
	If @Operacion = 'DEL' Begin
	
		--Validar
		--Si el efectivo esta depositado
		
		--Si los cheques estan depositados
		
		--Controlar los asientos asociados
		
		--Procesar
		--Eliminar la Forma de Pago (Eliminar el efectivo y actualizarlo - Reestablecer el saldo del cheque)

		--Eliminar relacion de las ventas asociadas (Actualizar saldos de la venta y cliente)
		
		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from CobranzaContado where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from CobranzaContado where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from CobranzaContado where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select Comprobante from CobranzaContado where IDTransaccion = @IDTransaccion)
		set @VTipoComprobante = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from CobranzaContado where IDTransaccion = @IDTransaccion)
		set @Condicion = 'CONTADO'

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,0,0,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ELI',@VTipoComprobante, @IDSucursal)


		--Eliminamos el registro
		Delete CobranzaContado Where IDTransaccion = @IDTransaccion
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		Goto Salir
		
	End

Salir:
	
	--Actualizar saldos de clientes
	Begin
		Declare @vIDTransaccionVenta numeric(18,0)
		Declare @vIDCliente int
		
		Declare db_cursor cursor for
		Select IDTransaccionVenta From VentaCobranza Where IDTransaccionCobranza=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVenta
		While @@FETCH_STATUS = 0 Begin 
			
			Set @vIDCliente = (Select IDCliente From Venta Where IDTransaccion=@vIDTransaccionVenta)
			
			Exec SpAcualizarSaldoCliente @IDCliente=@vIDCliente
			
			Fetch Next From db_cursor Into @vIDTransaccionVenta
										
		End
		
		Close db_cursor   
		Deallocate db_cursor
		
	End
		
End

	
	


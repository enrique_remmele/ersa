﻿CREATE Procedure [dbo].[SpAsientoCobranzaCredito]

	@IDTransaccion numeric(18,0)

As

Begin

	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint = 0

	--Cobranzas
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vFecha date
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money

	--Venta
	Declare @vCredito bit
	Declare @vContado bit
	Declare @vDiferenciaCambio bit

	--Efectivo
	Declare @vIDTipoComprobanteEfectivo TINYINT
	Declare @vIDTipoComprobanteTarjeta tinyint

	--Cheque
	Declare @vDiferido bit
	Declare @vIDCuentaBancaria int
	Declare @vIDTipoAnticipo int

	--Documento
	Declare @vIDTipoComprobanteDocumento tinyint

	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vCodigo varchar(50)
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money
	Declare @vImporte money
	Declare @TotalDiferenciaCambio money
	declare @vCliente varchar(200)

	Declare @vIDMonedaFormaPago as integer
	--Obtener valores
	Begin


		Select	@vIDSucursal=IDSucursal,
				@vFecha=FechaEmision,
				@vIDTipoComprobante=IDTipoComprobante,
				@vComprobante=Comprobante,
				@vIDMoneda=IDMoneda,
				@vCotizacion=Cotizacion,
				@TotalDiferenciaCambio = DiferenciaCambio,
				@vIDTipoAnticipo = IDTipoAnticipo
		From VCobranzaCredito Where IDTransaccion=@IDTransaccion


	End

	--Verificar que el asiento se pueda modificar
	Begin

		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	

		----Si esta procesado
		--If (Select Procesado From CobranzaCredito Where IDTransaccion=@IDTransaccion) = 'False' Begin
		--	print 'La cobranza no es valida'
		--	GoTo salir
		--End 	

		--Si esta anulado
		If (Select Anulado From CobranzaCredito Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'La cobranza esta anulada'
			GoTo salir
		End 	

		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	

		print 'Los datos son correctos!'

	End

	--Eliminar primero el asiento
	Begin

		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion

		print 'Registros de Asiento y Detalle eliminados!'

	End

	--Cargamos la Cabecera
	Begin

		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)

		set @vCliente = (select cliente from VCobranzaCredito where IDTransaccion = @IDTransaccion)
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = 'Cobranzas del ' + CONVERT(varchar(50), @vFecha) + ' de ' + @vCliente

		print 'Tipo de Comprobante:' + convert(varchar(50), @vTipoComprobante)
		print 'Comprobante:' + convert(varchar(50), @vComprobante)


		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')

		print 'Cabecera cargada!'

	End

	--Ventas
	Begin

		--Variables

		Declare cCFVenta cursor for
		Select Codigo, Debe, Haber, Credito, Credito From VCFCobranzaVenta 
		Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda and DiferenciaCambio = 0 and AnticipoCliente = 0
		Open cCFVenta   
		fetch next from cCFVenta into @vCodigo, @vDebe, @vHaber, @vCredito, @vContado

		While @@FETCH_STATUS = 0 Begin  

			Set @vImporteDebe = 0
			Set @vImporteHaber = 0

			If @vIdSucursal <> 5 Begin -- Si la Sucursal es <> a Mercury

				--Busca la cuenta contable deudor del producto
				Declare @IDProductoVenta as integer
				Declare @vCuentaContableProducto varchar(32)
				set @IDProductoVenta = (select top (1)isnull(IDProducto,0) from vDetalleVenta
				Join VentaCobranza on VentaCobranza.IDTransaccionVenta = VDetalleVenta.IDTransaccion
				where VentaCobranza.IDTransaccionCobranza = @IDTransaccion)


				--BUsca la cuenta contable deudor del tipo de producto
				declare @vIDTipoProducto as integer
				Declare @vCuentaContableTipoProducto varchar(32)
				set @vIDTipoProducto =(select top (1)isnull(IDTipoProducto,0) from vDetalleVenta
				Join VentaCobranza on VentaCobranza.IDTransaccionVenta = VDetalleVenta.IDTransaccion
				where VentaCobranza.IDTransaccionCobranza = @IDTransaccion)

				set @vCuentaContableProducto = (select CuentaContableDeudor from Producto where ID = @IDProductoVenta)
				set @vCuentaContableTipoProducto = (select isnull(CuentaContableCliente,'') from TipoProducto where ID = @vIDTipoProducto)

				--Tipo producto
				If @vCuentaContableTipoProducto <> '' Begin
					set @vCodigo = @vCuentaContableTipoProducto
				end
			End 

			--Producto
			IF @vCuentaContableProducto <> '' begin
				set @vCodigo = @vCuentaContableProducto
			end



			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--declare @vCotizacionVenta as money = (select Cotizacion from Venta where idtransaccion = @IDTransaccion)
			Set @vImporte = (Select SUM(Isnull(Importe,0)) From vVentaCobranzaCreditoAsiento Where IDTransaccionCobranza=@IDTransaccion)
			print concat('venta : ', @vImporte)
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End

			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				

			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
				
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End
				
				
			End

			fetch next from cCFVenta into @vCodigo, @vDebe, @vHaber, @vCredito, @vContado

		End

		close cCFVenta 
		deallocate cCFVenta

	End

	--DiferenciaCambio
	begin
		Declare cCFDiferencia cursor for
		Select Codigo, Debe, Haber, Credito, Credito, DiferenciaCambio From VCFCobranzaVenta 
		Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda and DiferenciaCambio = 1 
		Open cCFDiferencia 
		fetch next from cCFDiferencia into @vCodigo, @vDebe, @vHaber, @vCredito, @vContado, @vDiferenciaCambio

		While @@FETCH_STATUS = 0 Begin  

			If @TotalDiferenciaCambio = 0  Begin
				set @vImporte = 0
			End

			set @vImporte = @TotalDiferenciaCambio
			If @TotalDiferenciaCambio < 0 Begin
				set @vImporte = @TotalDiferenciaCambio * -1
			End 
			print concat('DIF CAMBIO : ', @vImporte)

			If @TotalDiferenciaCambio < 0 Begin
				Set @vImporteDebe = @TotalDiferenciaCambio
				Set @vImporteHaber = 0
				set @vDebe = 1
				set @vHaber = 0
			End 

			If @TotalDiferenciaCambio > 0 Begin
				Set @vImporteDebe = 0
				Set @vImporteHaber = @TotalDiferenciaCambio
				set @vDebe = 0
				set @vHaber = 1
			End 
		  	
			
				Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

				--Set @vImporte = (Select SUM(Isnull(ImporteGS,0)) From VentaCobranza Where IDTransaccionCobranza=@IDTransaccion)
				If @vDebe = 1 Begin
					Set @vImporteDebe = @vImporte
				End

				If @vHaber = 1 Begin
					Set @vImporteHaber = @vImporte
				End				

				If (@vImporteHaber > 0 Or @vImporteDebe>0) and (@vCodigo <> '') Begin		
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End

			--Diferencia de cambio cuando las formas de pagos tiene cotizaciones diferentes a la cobranza
			--declare @DiferenciaCambioFP decimal
			--set @DiferenciaCambioFP =  ISnull((select ((Total*Cotizacion) - (select sum(importe) from FormaPago where idtransaccion = VCobranzaCredito.IDTransaccion)) 
			--from VCobranzaCredito where IDTransaccion = @IDTransaccion),0)
			
			--If @DiferenciaCambioFP = 0  Begin
			--	set @vImporte = 0
			--End

			--set @vImporte = @DiferenciaCambioFP
			--If @DiferenciaCambioFP < 0 Begin
			--	set @vImporte = @DiferenciaCambioFP * -1
			--End 

			--If @DiferenciaCambioFP > 0 Begin
			--	Set @vImporteDebe = @DiferenciaCambioFP
			--	Set @vImporteHaber = 0
			--	set @vDebe = 1
			--	set @vHaber = 0
			--End 

			--If @DiferenciaCambioFP < 0 Begin
			--	Set @vImporteDebe = 0
			--	Set @vImporteHaber = @DiferenciaCambioFP
			--	set @vDebe = 0
			--	set @vHaber = 1
			--End 
		  	
			
			--	Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			--	Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--	--Set @vImporte = (Select SUM(Isnull(ImporteGS,0)) From VentaCobranza Where IDTransaccionCobranza=@IDTransaccion)
			--	If @vDebe = 1 Begin
			--		Set @vImporteDebe = @vImporte
			--	End

			--	If @vHaber = 1 Begin
			--		Set @vImporteHaber = @vImporte
			--	End				

			--	If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
			--		Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
			--		Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, 'DIFERENCIA FORMA DE PAGO')
			--	End

				fetch next from cCFDiferencia into @vCodigo, @vDebe, @vHaber, @vCredito, @vContado, @vDiferenciaCambio

		End

		close cCFDiferencia
		deallocate cCFDiferencia

	End

	--Anticipo de clientes
	begin
		Declare cCFDiferencia cursor for
		Select Codigo, Debe, Haber, Credito, Credito, DiferenciaCambio From VCFCobranzaVenta 
		Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda and AnticipoCliente = 1 
		Open cCFDiferencia 
		fetch next from cCFDiferencia into @vCodigo, @vDebe, @vHaber, @vCredito, @vContado, @vDiferenciaCambio

		While @@FETCH_STATUS = 0 Begin  
			set @vImporte = (Select  round(Total,0)* Cotizacion from CobranzaCredito where IDTransaccion = @IDTransaccion)
			print concat('ANTICIPO : ', @vImporte)
			If @TotalDiferenciaCambio < 0 Begin
				set @vImporte = @TotalDiferenciaCambio * -1
			End 

			If @vImporte < 0 Begin
				Set @vImporteDebe = @vImporte
				Set @vImporteHaber = 0
				set @vDebe = 1
				set @vHaber = 0
			End 

			If @vImporte > 0 Begin
				Set @vImporteDebe = 0
				Set @vImporteHaber = @vImporte
				set @vDebe = 0
				set @vHaber = 1
			End 
		  	
			    set @vCodigo = (select Codigo from VTipoAnticipo where ID = @vIDTipoAnticipo)
				Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

				--Set @vImporte = (Select SUM(Isnull(ImporteGS,0)) From VentaCobranza Where IDTransaccionCobranza=@IDTransaccion)
				If @vDebe = 1 Begin
					Set @vImporteDebe = @vImporte
				End

				If @vHaber = 1 Begin
					Set @vImporteHaber = @vImporte
				End				

				If (@vImporteHaber > 0 Or @vImporteDebe>0) and (@vCodigo <> '') Begin		
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End

				fetch next from cCFDiferencia into @vCodigo, @vDebe, @vHaber, @vCredito, @vContado, @vDiferenciaCambio

		End

		close cCFDiferencia
		deallocate cCFDiferencia

	End
	--Efectivo
	Begin
		print 'efectivo'
		--Variables
		Declare cCFEfectivo cursor for
		Select Codigo, Debe, Haber, IDTipoComprobante, IDMoneda From VCFCobranzaEfectivo
		Where IDSucursal=@vIDSucursal-- And IDMoneda=@vIDMoneda
		Open cCFEfectivo   
		fetch next from cCFEfectivo into @vCodigo, @vDebe, @vHaber, @vIDTipoComprobanteEfectivo, @vIDMoneda

		While @@FETCH_STATUS = 0 Begin  

			Set @vImporteDebe = 0
			Set @vImporteHaber = 0

			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Hayar el importe
			Set @vImporte = (Select SUM(FP.Importe) 
			From FormaPago FP 
			Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID 
			Where FP.IDTransaccion=@IDTransaccion 
			And E.IDTipoComprobante=@vIDTipoComprobanteEfectivo and IDMoneda = @vIDMoneda)
			print @vImporte
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End

			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				

			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin	
				if exists(select * from DetalleAsiento where IDTransaccion = @IDTransaccion and IDCuentaContable = @vIDCuentaContable) begin
					Update DetalleAsiento
					Set Credito = Credito +Round(@vImporteHaber,@vRedondeo),
					Debito = Debito + Round(@vImporteDebe,@vRedondeo)
					where IDTransaccion = @IDTransaccion 
					and IDCuentaContable = @vIDCuentaContable
				end
				else begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				end
			End

			fetch next from cCFEfectivo into @vCodigo, @vDebe, @vHaber, @vIDTipoComprobanteEfectivo,@vIDMoneda

		End

		close cCFEfectivo 
		deallocate cCFEfectivo

	END

	--Cheque
	Begin

		--Variables
		Declare cCFCheque cursor for
		Select Codigo, Debe, Haber, Diferido, IDCuentaBancaria, IDMoneda From VCFCobranzaCheque
		Where IDSucursal=@vIDSucursal --And IDMoneda=@vIDMoneda
		Open cCFCheque 
		fetch next from cCFCheque into @vCodigo, @vDebe, @vHaber, @vDiferido, @vIDCuentaBancaria, @vIDMonedaFormaPago

		While @@FETCH_STATUS = 0 Begin  

			Set @vImporteDebe = 0
			Set @vImporteHaber = 0

			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Hayar el importe
			Set @vImporte = IsNull((Select SUM(FP.Importe) 
											From vFormaPago FP 
											Join ChequeCliente CQH On FP.IDTransaccionCheque=CQH.IDTransaccion 
									Where FP.IDTransaccion=@IDTransaccion 
									And CQH.Diferido=@vDiferido 
									and FP.IDMoneda = @vIDMonedaFormaPago),0)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End

			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				

			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin	
				if exists(select * from DetalleAsiento where IDTransaccion = @IDTransaccion and IDCuentaContable = @vIDCuentaContable) begin
					Update DetalleAsiento
					Set Credito = Credito +Round(@vImporteHaber,@vRedondeo),
					Debito = Debito + Round(@vImporteDebe,@vRedondeo)
					where IDTransaccion = @IDTransaccion 
					and IDCuentaContable = @vIDCuentaContable
				end
				else begin	
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				end
			End

			fetch next from cCFCheque into @vCodigo, @vDebe, @vHaber, @vDiferido, @vIDCuentaBancaria, @vIdMOnedaFormaPAgo

		End

		close cCFCheque 
		deallocate cCFCheque

	End

	--Documentos
	Begin
		print 'Documentos'
		--Anular por sucursal
		--Variables
		Declare cCFDocumento cursor for
		Select Codigo, Debe, Haber, IDTipoComprobante, IDMoneda From VCFCobranzaDocumento
		Where IDSucursal=@vIDSucursal --And IDMoneda=@vIDMoneda
		--Where IDMoneda=@vIDMoneda
		Open cCFDocumento 
		fetch next from cCFDocumento into @vCodigo, @vDebe, @vHaber, @vIDTipoComprobanteDocumento,@vIDMoneda

		While @@FETCH_STATUS = 0 Begin  

			Set @vImporteDebe = 0
			Set @vImporteHaber = 0

			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Hayar el importe
			Set @vImporte = IsNull((Select SUM(FP.Importe) 
			From FormaPago FP 
			Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion 
			And FP.ID=FPD.ID 
			Where FP.IDTransaccion=@IDTransaccion 
			And FPD.IDTipoComprobante=@vIDTipoComprobanteDocumento 
			and IDMoneda=@vIDMoneda),0)
			print @vImporte
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End

			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				

			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End

				
			End

			fetch next from cCFDocumento into @vCodigo, @vDebe, @vHaber, @vIDTipoComprobanteDocumento,@vIDMoneda

		End

		close cCFDocumento 
		deallocate cCFDocumento

	END

	--Tarjeta
	Begin

		print 'Tarjeta'

		--Variables
		Declare cCFTarjeta cursor for
		Select Codigo, Debe, Haber, IDTipoComprobante From VCFCobranzaTarjeta
		Where IDSucursal=@vIDSucursal
		Open cCFTarjeta   
		fetch next from cCFTarjeta into @vCodigo, @vDebe, @vHaber, @vIDTipoComprobanteTarjeta

		While @@FETCH_STATUS = 0 Begin  

			Set @vImporteDebe = 0
			Set @vImporteHaber = 0

			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Hayar el importe
			Set @vImporte = IsNull((Select SUM(FP.Importe) From FormaPago FP Join FormaPagoTarjeta E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID Where FP.IDTransaccion=@IDTransaccion And E.IDTipoComprobante=@vIDTipoComprobanteTarjeta), 0)

			print @vImporte
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End

			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				

			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
			End

			fetch next from cCFTarjeta into @vCodigo, @vDebe, @vHaber, @vIDTipoComprobanteTarjeta

		End

		close cCFTarjeta 
		deallocate cCFTarjeta

	END

	--Actualizamos la cabecera, el total
	Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)

	Set @vImporteHaber = ROUND(@vImporteHaber, @vRedondeo)
	Set @vImporteDebe = ROUND(@vImporteDebe, @vRedondeo)
	
	Update Asiento Set Total = @vImporteHaber,
						Credito = @vImporteHaber,
						Debito = @vImporteDebe,
						Saldo = @vImporteHaber - @vImporteDebe
	Where IDTransaccion=@IDTransaccion

	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:

End


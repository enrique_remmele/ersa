﻿CREATE Procedure [dbo].[SpAsientoCompra]

	@IDTransaccion numeric(18,0)
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint = 0

	--Compra
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vContado bit
	Declare @vCredito bit
	Declare @vCondicion varchar(50)
	Declare @vFecha date
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money
	Declare @vCuentaContableCompra varchar(50)
	Declare @vCuentaContableCosto varchar(50)
	Declare @vTotalDiscriminado money
	Declare @vTotalDescuentoDiscriminado money
	Declare @vTotalCosto money
	Declare @vFactorimpuesto decimal(18,7)
	Declare @vFactorDiscriminado decimal(18,7)
	Declare @vIDProducto int
	Declare @vIDTipoProducto int
	
	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	
	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money

	Declare @vProveedor as varchar(32)
	Declare @vIDProveedor as int
	
	--Obtener valores
	Begin
	
		Set @vContado='False'
		Set @vCondicion = 'CREDITO'

		Select	@vIDSucursal=IDSucursal,
				@vTipoComprobante=TipoComprobante,
				@vIDTipoComprobante=IDTipoComprobante,
				@vComprobante=Comprobante,
				@vCredito=Credito,
				@vFecha=Fecha,
				@vIDMoneda=IDMoneda,
				@vCotizacion=Cotizacion,
				@vProveedor = Proveedor,
				@vIDProveedor = IDProveedor
		From VCompra Where IDTransaccion=@IDTransaccion
	
		


		If @vCredito=0 Begin
			Set @vContado='True'
			Set @vCondicion = 'CONTADO'
		End
				
	End
					
	--Verificar que el asiento se pueda modificar
	Begin
		
		If not exists(Select * From Compra Where IDTransaccion=@IDTransaccion) Begin
			print 'La operacion no es una Compra'
			GoTo salir
		End 	

		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		
	End
				
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = @vTipoComprobante + ' ' + @vComprobante + ' ' + @vCondicion + ' - ' + @vProveedor
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	
	--Cuentas Fijas de Proveedors
	Begin
	
		--Variables
		Declare @vCompraCredito bit
		Declare @vCompraContado bit
		Declare @vCodigoCuentaProveedor varchar(50)
		Declare @vTotalProducto money

		Declare cCFCompraProveedor cursor for
		Select CuentaContableProveedorTipoProducto,
		'Total'=Total 
		From vDetalleCompra 
		Where IDTransaccion=@IDTransaccion
		Open cCFCompraProveedor   
		fetch next from cCFCompraProveedor into @vCodigoCuentaProveedor, @vTotalProducto
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vCodigo = @vCodigoCuentaProveedor
			if (@vCodigo = '') begin
				Set @vCodigo = '21010101'
			end

			Set @vImporteDebe = 0
			Set @vImporteHaber = 0

			--Obtener la cuenta
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			Set @vTotalProducto = @vTotalProducto * @vCotizacion
			Set @vImporteHaber = @vTotalProducto
				
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin				
				--Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				--Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo) , 0, '')
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End
			End
			
			fetch next from cCFCompraProveedor into @vCodigoCuentaProveedor, @vTotalProducto
			
		End
		
		close cCFCompraProveedor 
		deallocate cCFCompraProveedor
	End
					
	--Cuentas Fijas Impuesto
	Begin
		
		--Variables
		Declare @vIDImpuesto tinyint
		
		Declare cCFCompraImpuesto cursor for
		Select Codigo, IDImpuesto, Debe, Haber from VCFCompra 
		Where TipoCuentaFija = 'IMPUESTO' 
		And IDSucursal=@vIDSucursal 
		And IDMoneda=@vIDMoneda
		
		Open cCFCompraImpuesto   
		fetch next from cCFCompraImpuesto into @vCodigo, @vIDImpuesto, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Set @vImporte = (Select SUM(TotalImpuesto * @vCotizacion) From DetalleImpuesto Where IDTransaccion=@IDTransaccion And IDImpuesto=@vIDImpuesto)
			Set @vImporte = (select sum(TotalImpuesto * @vCotizacion) from DetalleCompra where IDTransaccion = @IDTransaccion and IDImpuesto = @vIDImpuesto)
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
										Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End
			End
			
			fetch next from cCFCompraImpuesto into @vCodigo, @vIDImpuesto, @vDebe, @vHaber
			
		End
		
		close cCFCompraImpuesto 
		deallocate cCFCompraImpuesto
	End					

	--Cuentas Fijas de Costo de Mercaderia
	Begin
	
		--Cuenta Fija Compra
		Set @vCuentaContableCompra = ''
		Set @vTotalCosto = 0
			
		Declare cDetalleCosto cursor for
		Select CuentaContableCompra,
				'Total'=Total,
				IDProducto
			    From VDetalleCompra 
				Where IDTransaccion=@IDTransaccion 
		Open cDetalleCosto
		fetch next from cDetalleCosto into @vCuentaContableCosto, @vTotalCosto, @vIDProducto
		
		While @@FETCH_STATUS = 0 Begin  
					
			set @vIDTipoProducto = (select idtipoProducto from Producto where ID = @vIDProducto)
			
			if @vIDTipoProducto = 27 begin
				if @vIDSucursal = 4 and @vFecha >= '20191127' begin
					set @vCuentaContableCosto = '11122102'
				end
			end
			
			set @vImporte = 0
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			--set @vFactorimpuesto = (select FactorImpuesto from Impuesto where id = (select idimpuesto from producto where id = @vIDProducto))
			
			set @vFactorDiscriminado = (select FactorDiscriminado from Impuesto where id = (select idimpuesto from producto where id = @vIDProducto))
			if @vFactorDiscriminado > 1 begin
				Set @vTotalCosto = Round((@vTotalCosto/@vFactorDiscriminado),2)
			end 
			
			PRINT concat('factor discriminado ', @vTotalCosto, ' ', @vFactorDiscriminado)
			Set @vTotalCosto = @vTotalCosto * @vCotizacion
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vCodigo = @vCuentaContableCosto
			--select * from detallecompra where idtransaccion = 673477
			--Si no existe, poner el predeterminado
			If @vCuentaContableCosto = '' Begin
				Set @vCodigo = (Select IsNull((Select Top(1) Codigo From VCFCompra Where TipoCuentaFija = 'PRODUCTO' and IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda),''))								
			End
			
			--Validar codigo
			If @vCodigo = '' Begin
				fetch next from cDetalleCosto into @vCuentaContableCosto, @vTotalCosto
			End
			
			--Hayar el importe	
			Set @vImporte = @vTotalCosto
			
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')			
										
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			--Cargamod
			If @vImporteHaber > 0 Or @vImporteDebe > 0 Begin
			
				--Si existe la cuenta, actualizar
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
										Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End
			End
							
			fetch next from cDetalleCosto into @vCuentaContableCosto, @vTotalCosto, @vIDProducto
			
		End
		
		close cDetalleCosto 
		deallocate cDetalleCosto
		
	End

	--Actualizamos la cabecera, el total
	Begin
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
		Set @vImporteHaber = ROUND(@vImporteHaber, @vRedondeo)
		Set @vImporteDebe = ROUND(@vImporteDebe, @vRedondeo)
	
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vImporteHaber - @vImporteDebe
		Where IDTransaccion=@IDTransaccion
	End

	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
	
End

﻿CREATE Procedure [dbo].[SpImportarDetalleVenta]

	--Entrada
	--Punto de Expedicion
	@TipoComprobante varchar(10),
	@ReferenciaSucursal varchar(5),
	@ReferenciaPuntoExpedicion varchar(5),
	@NroComprobante varchar(50),
	
	--Detalle Vienen sin IVA
	@CodigoProducto varchar(50),
	@IVA int,
	@Cantidad decimal(10,2),
	@Importe money = 0,
	
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	
	@Actualizar bit
	
As

Begin
		
	--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		
		Declare @vOperacion varchar(50)
		Declare @vIDTransaccion int
		Declare @vIDTipoComprobante smallint
		Declare @vComprobante varchar(15)

		--Totales
		Declare @vTotal money
		Declare @vTotalImpuesto money
		Declare @vTotalDiscriminado money
		Declare @vTotalDescuento money
		Declare @vIDSucursal int
		Declare @vIDDepositoOperacion int
							
	End
	
	--Establecer valores predefinidos
	Begin
		
		--Obtener el tipo de comprobante
		Set @vIDTipoComprobante = IsNull((Select Top(1) ID From TipoComprobante Where IDOperacion=@IDOperacion And Codigo=@TipoComprobante), 0)
		If @vIDTipoComprobante = 0 Begin
			set @vMensaje = 'No se encontro el tipo de comprobante!'
			set @vProcesado = 'False'
			GoTo Salir
		End
		
		Set @ReferenciaSucursal = '0' + dbo.FFormatoDosDigitos(CONVERT(tinyint, @ReferenciaSucursal))
		Set @ReferenciaPuntoExpedicion = '0' + dbo.FFormatoDosDigitos(CONVERT(tinyint, @ReferenciaPuntoExpedicion))
		Set @vComprobante = @ReferenciaSucursal + '-' + @ReferenciaPuntoExpedicion + '-' + @NroComprobante
						
	End
	
	--Verificar si ya existe
	set @vIDTransaccion = IsNull((Select Top(1) IDTransaccion From VentaImportada Where TipoComprobante=@TipoComprobante And Comprobante=@vComprobante), 0)
	
	If @vIDTransaccion = 0 Begin
		
		Set @vMensaje = 'El registro no corresponde a ninguna venta!'
		Set @vProcesado = 'True'
		GoTo Salir
		
	End Else Begin
		Set @vOperacion = 'UPD'
	End
	
Detalle:
	
	--DETALLE
	Begin
	
		--Hayar Valores
		--Variables
		Begin
			
			Declare @vIDImpuesto int
			Declare @vIDProducto int
			Declare @vID int
			Declare @vFactorImpuesto decimal(10,7)
			Declare @vFactorDiscriminado decimal(10,7)
			
			Declare @vPrecioUnitario money

		End
		
		--Hayar valores
		Begin
		
			--Producto
			Set @vIDProducto = IsNull((Select Top(1) ID From Producto Where Referencia=@CodigoProducto), 0)
			
			--Deposito
			Set @vIDDepositoOperacion = (Select Top(1) IDDeposito From Venta Where IDTransaccion=@vIDTransaccion)

			--Impuesto
			If @IVA=10 Begin
				Set @vIDImpuesto = 1
			End
			
			If @IVA=5 Begin
				Set @vIDImpuesto = 2
			End
			
			If @IVA=0 Begin
				Set @vIDImpuesto = 3
			End
			
			Set @vFactorDiscriminado = IsNull((Select Top(1) FactorDiscriminado From  Impuesto Where ID=@vIDImpuesto),0)
			Set @vFactorImpuesto = IsNull((Select Top(1) FactorImpuesto From  Impuesto Where ID=@vIDImpuesto),0)
			
			--Totales
			--El importe esta sin IVA, calcular su iva y obtener los demas valores
			Set @vTotalDiscriminado = @Importe
			Set @vTotal = @Importe * @vFactorDiscriminado
			Set @vPrecioUnitario = @vTotal / @Cantidad
			Set @vTotalImpuesto = @vTotal / @vFactorImpuesto	
									
		End
				
		--Validar
		Begin
		
			--Producto
			If @vIDProducto=0 Begin
				Set @vMensaje = 'No se encontro el producto!'
				Set @vProcesado = 'False'
				GoTo Salir
			End
			
			--Deposito
			If @vIDDepositoOperacion = 0 Begin
				Set @vMensaje = 'Deposito incorrecto!'
				Set @vProcesado = 'False'
				GoTo Salir
			End
			
		End
		
		--Insertar si no existe
		If Not Exists(Select * from DetalleVenta Where IDTransaccion=@vIDTransaccion And IDProducto=@vIDProducto And PrecioUnitario=@vPrecioUnitario) Begin
			
			Set @vID = IsNull((Select MAX(ID) + 1 From DetalleVenta Where IDTransaccion=@vIDTransaccion),0)
			
			Insert Into DetalleVenta(IDTransaccion, IDProducto, ID, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, Total, TotalImpuesto, TotalDiscriminado, PorcentajeDescuento, DescuentoUnitario, DescuentoUnitarioDiscriminado, TotalDescuento, TotalDescuentoDiscriminado, CostoUnitario, TotalCostoImpuesto, TotalCostoDiscriminado, Caja, CantidadCaja)
			Values(@vIDTransaccion, @vIDProducto, @vID, @vIDDepositoOperacion, '', @vIDImpuesto, @Cantidad, @vPrecioUnitario, @vTotal, @vTotalImpuesto, @vTotalDiscriminado, 0, 0, 0, 0, 0, 0, 0, 0, 'False', @Cantidad)			
			
			Set @vMensaje = 'Registro insertado!'
			Set @vProcesado = 'True'
			GoTo Salir

		End Else Begin
			Set @vID = (Select Top(1) ID from DetalleVenta Where IDTransaccion=@vIDTransaccion And IDProducto=@vIDProducto)
			Set @vMensaje = 'Registro actualizado!'
			Set @vProcesado = 'True'
			GoTo Salir
		End
		
	End
	
Salir:

	--Actualizar la venta y su detalle
	If @vProcesado='True' Begin
		
		--Actualizamos el DetalleImpuesto
		Update DetalleImpuesto Set	Total=IsNull((Select Sum(DV.Total) From DetalleVenta DV Where DV.IDTransaccion=@vIDTransaccion And IDImpuesto=@vIDImpuesto), 0),
									TotalImpuesto=IsNull((Select Sum(DV.TotalImpuesto) From DetalleVenta DV Where DV.IDTransaccion=@vIDTransaccion And IDImpuesto=@vIDImpuesto), 0),
									TotalDiscriminado=IsNull((Select Sum(DV.TotalDiscriminado) From DetalleVenta DV Where DV.IDTransaccion=@vIDTransaccion And IDImpuesto=@vIDImpuesto), 0)
		Where IDTransaccion=@vIDTransaccion And IDImpuesto=@vIDImpuesto

		--Actualizamos en Ventas
		Update Venta Set	Total=IsNull((Select Sum(DV.Total) From DetalleImpuesto DV Where DV.IDTransaccion=@vIDTransaccion), 0),
							TotalImpuesto=IsNull((Select Sum(DV.TotalImpuesto) From DetalleImpuesto DV Where DV.IDTransaccion=@vIDTransaccion), 0),
							TotalDiscriminado=IsNull((Select Sum(DV.TotalDiscriminado) From DetalleImpuesto DV Where DV.IDTransaccion=@vIDTransaccion), 0)
		Where IDTransaccion=@vIDTransaccion 

	End

	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End

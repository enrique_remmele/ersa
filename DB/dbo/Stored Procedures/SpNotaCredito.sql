﻿CREATE Procedure [dbo].[SpNotaCredito]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDPuntoExpedicion int = Null,
	@IDSubMotivoNotaCredito int = 0,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante int = NULL,			
	@IDCliente int = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDDepositoOperacion tinyint = NULL,
	@Fecha date = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money= NULL,
	@Observacion varchar(100) = NULL,
	@Total money = NULL,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@TotalDescuento money = NULL,
	@Aplicar bit = 'False',
	@Devolucion bit = 'False',
	@Descuento bit = 'False',	
	@ConComprobantes bit = 'True',
	@Saldo money  = NULL,
	@IDMotivo integer = 0,
	@SolicitudAprobada bit = 0,
	@Direccion varchar(100) = null,
	@Telefono varchar(50) = null,
			
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion int,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	@IDSucursalCliente tinyint = NULL,

	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output	
As

Begin

	Declare @MantenerPedidoVigente bit
	Declare @vIDTransaccionPedido int

	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from NotaCredito where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar Fecha Timbrado 
		declare @fechaTimbrado as datetime
		select @fechaTimbrado = Vencimiento from PuntoExpedicion where Id = @IdPuntoExpedicion
		if @fechaTimbrado < @Fecha begin
			Set @Mensaje  = 'La fecha del timbrado ha vencido. Por favor seleccione otro numero de timbrado.'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		end

	--Comprobante fuera del Rango
		if @NroComprobante < (Select NumeracionDesde From PuntoExpedicion Where ID=@IDPuntoExpedicion) Or @NroComprobante > (Select NumeracionHasta From PuntoExpedicion Where ID=@IDPuntoExpedicion) Begin
			set @Mensaje = 'El numero de comprobante no esta dentro del rango correcto! Cambie el punto de expedicion o actualicelo.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
	
		--Validar
		--Comprobante
		if Exists(Select * From NotaCredito  Where IDPuntoExpedicion=@IDPuntoExpedicion And NroComprobante=@NroComprobante and IDTipoComprobante =@IDTipoComprobante and IDSucursal = @IDSucursal) Begin
			set @Mensaje = 'El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--Calcular Saldo....
		--Si es Sin Comprobantes, SALDO = @TOTAL
		--Si no, SALDO = 0
		--If @ConComprobantes = 'False' Begin
		--	Set @Saldo = @Total 		
		--End Else Begin
		--	Set @Saldo = 0
		--End
		
		if @Aplicar = 'True' Begin
			Set @Saldo = @Total 
		End
		
		--Insertar en Nota de Credito
		Insert Into NotaCredito  (IDTransaccion, IDPuntoExpedicion, IDTipoComprobante, NroComprobante, IDCliente, IDSucursal, IDDeposito,  Fecha, IDMoneda, Cotizacion, Observacion, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, Saldo, Anulado,FechaAnulado, IDUsuarioAnulado, Procesado, Aplicar, Devolucion, Descuento,ConComprobantes, IDSucursalCliente, IDSubMotivoNotaCredito, Direccion, Telefono)
		Values(@IDTransaccionSalida, @IDPuntoExpedicion, @IDTipoComprobante, @NroComprobante, @IDCliente,  @IDSucursalOperacion, @IDDepositoOperacion , @Fecha, @IDMoneda, @Cotizacion, @Observacion, @Total, @TotalImpuesto, @TotalDiscriminado, @TotalDescuento, @Saldo, 'False', NULL, NULL, 'False', @Aplicar ,@Devolucion, @Descuento,@ConComprobantes, @IDSucursalCliente, @IDSubMotivoNotaCredito, @Direccion, @Telefono)

		--Actualizar Punto de Expedicion
		Update PuntoExpedicion Set UltimoNumero=@NroComprobante
		Where ID=@IDPuntoExpedicion
										
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin

		if @SolicitudAprobada <> 1 begin
			----No anular Venta con fecha distint a hoy
			If Isnull((select VentaAnularFechaDistintaHoy from Configuraciones where IDSucursal = @IDSucursal),'False') = 'False' Begin
				If (Select CAST(fecha as date) From NotaCredito Where IDTransaccion=@IDTransaccion) <> cast(getdate() as date) Begin
					--No Cambiar el mensaje 
					set @Mensaje = 'No se puede anular con fecha distinta a hoy'
					set @Procesado = 'False'
					return @@rowcount
				End
			End
		end

		--No existe el motivo
		If not Exists(Select * From MotivoAnulacionNotaCredito where ID = @IDMotivo) Begin
			set @Mensaje = 'Motivo seleccionado invalido.'
			set @Procesado = 'False'
			return @@rowcount
		End

		Set @MantenerPedidoVigente = (select isnull(MantenerPedidoVigente,0) from MotivoAnulacionNotaCredito where id = @IDMotivo)

		declare @vNroOperacion int
		--Validar
		If Exists(Select * From NotaCreditoVentaAplicada NCVA Join NotaCreditoAplicacion NCA On NCVA.IDTransaccionNotaCreditoAplicacion = NCA.IDTransaccion Where NCVA.IDTransaccionNotaCredito=@IDTransaccion And NCA.Anulado='False') Begin
			set @vNroOperacion = (Select NCA.Numero From NotaCreditoVentaAplicada NCVA Join NotaCreditoAplicacion NCA On NCVA.IDTransaccionNotaCreditoAplicacion = NCA.IDTransaccion Where NCVA.IDTransaccionNotaCredito=@IDTransaccion And NCA.Anulado='False')
			set @Mensaje = concat('La Nota de Credito tiene facturas aplicadas! Anule primeramente la aplicacion para continuar. N° de Operacion de Aplicación: ', @vNroOperacion)
			set @Procesado = 'False'
			return @@rowcount
		End

		
		--Actualizamos el Stock
		Exec SpNotaCreditoActualizarStock  @IDTransaccion = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje output, @Procesado=@Procesado output
		print @Procesado
		
		If @Procesado = 1 Begin
			set @Mensaje = 'No se puede anular'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Actualizamos de saldos de las ventas
		Exec SpNotaCreditoActualizarVanta @IDTransaccion = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje output, @Procesado=@Procesado output
		
		If @Procesado = 'False' Begin
			return @@rowcount
		End
				
		--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
		
		----Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		

		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from NotaCredito where IDTransaccion = @IDTransaccion)
		set @IDCliente = (select IDCliente from NotaCredito where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from NotaCredito where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from NotaCredito where IDTransaccion = @IDTransaccion)
		declare @Comprobante as varchar(16) = (select Comprobante from vNotaCredito where IDTransaccion = @IDTransaccion)
		declare @VTipoComprobante varchar(16) = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from NotaCredito where IDTransaccion = @IDTransaccion)
		declare @Condicion varchar(16) = 'CREDITO'

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,@IDCliente,0,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ANU',@VTipoComprobante, @IDSucursal)
		

		----Anular
		Update NotaCredito  
		Set Anulado='True', 
		IDUsuarioAnulado=@IDUsuario, 
		FechaAnulado = GETDATE(),
		IDMotivo = @IDMotivo
		Where IDTransaccion = @IDTransaccion
		----Eliminar relación con Nota credito aplicación
		Delete NotaCreditoVentaAplicada where IDTransaccionNotaCredito = @IDTransaccion

		--Actualiza Pedido Nota Credito
		Declare @vIDTransaccionPedidoNotaCredito int
		set @vIDTransaccionPedidoNotaCredito = (Select top(1) IsNull(IDTransaccionPedido,0) From PedidoNCNotaCredito Where IDTransaccionNotaCredito = @IDTransaccion)
			
			Update DetalleNotaCreditoDiferenciaPrecio 
					set IDTransaccionNotaCredito =  null,
					IDTransaccionVenta = null
			Where IDTransaccionPedidoNotaCredito = @vIDTransaccionPedidoNotaCredito

			Update DetalleNotaCreditoAcuerdo 
					set IDTransaccionNotaCredito = null,
					IDTransaccionVenta = null
			Where IDTransaccionPedidoNotaCredito = @vIDTransaccionPedidoNotaCredito
			
			Update PedidoNotaCredito 
					set ProcesadoNC = null,
					AnuladoAprobado = 1
			where IDTransaccion = @vIDTransaccionPedidoNotaCredito

			
			if @MantenerPedidoVigente = 0 begin
	
				Exec SpPedidoNotaCredito
					@IDTransaccion = @vIDTransaccionPedidoNotaCredito
					,@Operacion = 'ANULAR'
					,@IDUsuario = @IDUsuario
					,@IDSucursal = @IDSucursal
					,@IDDeposito = @IDDeposito
					,@IDTerminal = @IDTerminal
					,@IDOperacion = 0
					,@Mensaje = ''
					,@Procesado = 0
					,@IDTransaccionSalida = 0

			end

	
		Delete From PedidoNCNotaCredito 
		Where IDTransaccionNotaCredito = @IDTransaccion
		
		
		EXEC SpComision
		@IDTransaccion=@IDTransaccion,
		@Venta='False',
		@Devolucion='True',
		@Operacion= 'ANULAR' 	 
		
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
		
	End
	
	If @Operacion = 'DEL' Begin
	
		if not exists (select * from usuario where idperfil = 1 and id = @IDUsuario) begin
				set @Mensaje = 'No tiene permisos para realizar esta operacion.'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
		end

		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From NotaCredito     Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Eliminamos Anulados
		Delete DocumentoAnulado where IDTransaccion = @IDTransaccion	
		
		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from NotaCredito where IDTransaccion = @IDTransaccion)
		set @IDCliente = (select IDCliente from NotaCredito where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from NotaCredito where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from NotaCredito where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select Comprobante from vNotaCredito where IDTransaccion = @IDTransaccion)
		set @VTipoComprobante = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from NotaCredito where IDTransaccion = @IDTransaccion)
		set @Condicion = 'CREDITO'

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,@IDCliente,0,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ELI',@VTipoComprobante, @IDSucursal)
					
		
		--Eliminamos el detalle
		Delete DetalleNotaCredito   where IDTransaccion = @IDTransaccion	
		Delete DetalleImpuesto Where IDTransaccion=@IDTransaccion
		
		--Eliminamo la Venta asociada
		Delete NotaCreditoVenta Where IDTransaccionNotaCredito = @IDTransaccion 	
		
		--Eliminamos la comision
		EXEC SpComision
		@IDTransaccion=@IDTransaccion,
		@Venta='False',
		@Devolucion='True',
		@Operacion= 'ELIMINAR'		
		
		--Transferencia de Saldos
		If Exists(Select * From TransferenciaNotaCredito Where IDTransaccionNotaCredito=@IDTransaccion) Begin
			Declare @vComprobanteNotaCredito varchar(50)
			Set @vComprobanteNotaCredito = (Select Comprobante From VNotaCredito Where IDTransaccion=@IDTransaccion)
			
			Update TransferenciaNotaCredito Set  IDTransaccionNotaCredito=NULL,
												ComprobanteNotaCredito=@vComprobanteNotaCredito
			Where IDTransaccionNotaCredito=IDTransaccion
			
		End
				
		--Eliminamos el registro
		Delete NotaCredito   Where IDTransaccion = @IDTransaccion
						
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		--Delete Transaccion Where ID = @IDTransaccion					 	 			
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End





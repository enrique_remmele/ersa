﻿CREATE Procedure [dbo].[SpEstadoClienteInsUpd]

	--Entrada
	@Descripcion varchar(100)
	
As

Begin

	Declare @vID int
	
	--Si existe
	If Exists(Select * From EstadoCliente Where Descripcion = @Descripcion) Begin
		Set @vID = (Select Top(1) ID From EstadoCliente Where Descripcion = @Descripcion)
		
	End Else Begin
		Set @vID = (Select IsNull(Max(ID)+1,1) From EstadoCliente)
		Insert Into EstadoCliente(ID, Descripcion, Orden, Estado)
		Values(@vID, @Descripcion, 0, 'True')
	End
	
	return @vID
	
End


﻿CREATE Procedure [dbo].[SpViewResumeMovimientosSaldoCobroDetallado]

	--Entrada
	@Fecha1 Date,
	@Fecha2 Date
	


As

Begin
	
	create table #TablaTemporal(ID tinyint,
								IDCliente int,
								Cliente varchar(150),
								Referencia varchar(50),
								TotalVenta money,
								TotalCobranza money,
								TotalDebito money, 
								TotalCredito money,
								SaldoAnterior money, 
								Saldo money,
								UltimaCompra date,
								UltimaCobranza date,
								IDTipoCliente int,
								TipoCliente varchar(50),
								IDMoneda int,
								Condicion varchar(10),
								PlazoCredito tinyint,
								LimiteCredito money,
								Vendedor varchar(50),
								Cobrador varchar(50),
								IDEstado int,
								Estado varchar(15),
								IDCiudad int,
								IDSucursal int,
								IDZonaVenta int)
	
	
	
    
    ---Variables para CALCULAR SALDO 
    --Variables Para Calculo de Saldo Anterior
    declare @vSaldo money
	declare @vSaldoAnterior money
	declare @vTotalVenta money
	declare @vTotalNotaDebito money
	declare @vTotalNotaCredito money
	declare @vTotalCobranza money
	
	--Variables Para Calculo de Saldo Posterior
	declare @vTotalVenta2 money
	declare @vTotalNotaDebito2 money
	declare @vTotalNotaCredito2 money
	declare @vTotalCobranza2 money
	
	--Declarar variables Variables
	
	declare @vID tinyint
	declare @vIDCliente int
	declare @vCliente varchar (100)
	declare @vReferencia varchar (50)
	declare @vUltimaCompra date
	declare @vUltimaCobranza date
	declare @vIDTipoCliente int
	declare @vTipoCliente varchar(50)
	declare @vIDMoneda int
	declare @vCondicion varchar(10)
	declare @vPlazoCredito tinyint
	declare @vLimiteCredito money
	declare @vVendedor varchar(50)
	declare @vCobrador varchar(50)
	declare @vIDEstado int
	declare @vEstado varchar(15)
	declare @vIDCiudad int
	declare @vIDSucursal int
	declare @vIDZonaVenta int

	Set @vID = (Select IsNull(MAX(ID)+1,1) From #TablaTemporal)
	
	--Insertar datos	
	Begin
	
		Declare db_cursor cursor for
		
		Select	
		C.ID,
		C.RazonSocial,
		C.Referencia, 
		C.UltimaCompra,
		C.UltimaCobranza,
		C.IDTipoCliente,
		C.TipoCliente,
		V.IDMoneda,
		V.Condicion,
		C.PlazoCredito,
		C.LimiteCredito,
		C.Vendedor,
		C.Cobrador,
		C.IDEstado,
		C.Estado,
		C.IDCiudad,
		C.IDSucursal,
		C.IDZonaVenta
		From VCliente C
		Join VVenta V On C.ID= V.IDCliente 
		Where V.Saldo > 0
		
		Open db_cursor   
		Fetch Next From db_cursor Into  @vIDCliente,@vCliente,@vReferencia,@vUltimaCompra,@vUltimaCobranza,@vIDTipoCliente,@vTipoCliente,@vIDMoneda,@vCondicion,@vPlazoCredito,@vLimiteCredito,@vVendedor,@vCobrador,@vIDEstado,@vEstado,@vIDCiudad,@vIDSucursal,@vIDZonaVenta
		While @@FETCH_STATUS = 0 Begin 
			--Hallar Saldo Anterior
			set @vTotalVenta  = IsNull((Select Sum(Total) From Venta  Where IDCliente=@vIDCliente And FechaEmision<@Fecha1),0)
			Set @vTotalNotaCredito=IsNull((Select Sum(Importe) From VNotaCreditoVenta Where IDCliente=@vIDCliente And Fecha<@Fecha1),0)
			Set @vTotalNotadebito=IsNull((Select Sum(Importe) From VNotaDebitoVenta Where IDCliente=@vIDCliente And Fecha<@Fecha1),0)
			Set @vTotalCobranza=IsNull((Select  Sum(Importe) From VVentaDetalleCobranza Where IDCliente=@vIDCliente And FechaCobranza<@Fecha1),0)
						
			set @vSaldoAnterior  = (@vTotalVenta + @vTotalNotaDebito) - (@vTotalCobranza + @vTotalNotaCredito) 
			
			--Hallar Saldo
			set @vTotalVenta2  = IsNull((Select Sum(Total) From Venta  Where IDCliente=@vIDCliente And FechaEmision Between @Fecha1  And @Fecha2),0)
			Set @vTotalNotadebito2=IsNull((Select Sum(Importe) From VNotaDebitoVenta Where IDCliente=@vIDCliente And Fecha Between @Fecha1 And @Fecha2),0)
			Set @vTotalCobranza2=IsNull((Select  Sum(Importe) From VVentaDetalleCobranza Where IDCliente=@vIDCliente And FechaCobranza Between @Fecha1 And @Fecha2),0)
			Set @vTotalNotaCredito2=IsNull((Select Sum(Importe) From VNotaCreditoVenta Where IDCliente=@vIDCliente And Fecha Between @Fecha1 And @Fecha2),0)
			
			set @vSaldo= (@vSaldoAnterior + @vTotalVenta2 + @vTotalNotadebito2 )- (@vTotalCobranza2 + @vTotalNotaCredito2)
			
			If Not Exists(Select * From #TablaTemporal Where IDCliente= @vIDCliente)Begin
				Insert Into  #TablaTemporal(ID,IDCliente,Cliente,Referencia,TotalVenta,TotalCobranza,TotalDebito,TotalCredito,SaldoAnterior,Saldo,UltimaCompra,UltimaCobranza,IDTipoCliente,TipoCliente,IDMoneda,Condicion,PlazoCredito,LimiteCredito,Vendedor,Cobrador,IDEstado,Estado,IDCiudad,IDSucursal,IDZonaVenta) 
				Values (@vID,@vIDCliente,@vCliente,@vReferencia,@vTotalVenta2,@vTotalCobranza2,@vTotalNotadebito2,@vTotalNotaCredito2,@vSaldoAnterior,@vSaldo,@vUltimaCompra,@vUltimaCobranza,@vIDTipoCliente,@vTipoCliente,@vIDMoneda,@vCondicion,@vPlazoCredito,@vLimiteCredito,@vVendedor,@vCobrador,@vIDEstado,@vEstado,@vIDCiudad,@vIDSucursal,@vIDZonaVenta)
			End
			
			Fetch Next From db_cursor Into  @vIDCliente,@vCliente,@vReferencia,@vUltimaCompra,@vUltimaCobranza,@vIDTipoCliente,@vTipoCliente,@vIDMoneda,@vCondicion,@vPlazoCredito,@vLimiteCredito,@vVendedor,@vCobrador,@vIDEstado,@vEstado,@vIDCiudad,@vIDSucursal,@vIDZonaVenta
			
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		
	End	
	
	Select * From #TablaTemporal Where ID=@vID
	
End
	




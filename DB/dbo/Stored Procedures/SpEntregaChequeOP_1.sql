﻿CREATE Procedure [dbo].[SpEntregaChequeOP]

	--JET 28-10-2015
	
	--Entrada OP
	@IDTransaccion numeric(18,0) = NULL,
	@IDTransaccionOP numeric(18,0) = NULL,
	@ChequeEntregado bit = NULL,
	@FechaEntrega date = NULL,
	@Recibo varchar(50) = NULL,
	@RetiradoPor varchar(200) = NULL,
	
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,

	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
	
	
As

Begin
	
	----Validar Feha Operacion
	--If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
	--	Set @Mensaje  = 'Fecha fuera de rango permitido!!'
	--	Set @Procesado = 'False'
	--	set @IDTransaccionSalida  = 0
	--	Return @@rowcount
	--End

	If @Operacion = 'INS' Begin
	
		--Si NO existe el registro
		If Not Exists(Select * From OrdenPago Where IDTransaccion=@IDTransaccionOP) Begin
			set @Mensaje = 'El registro no existe!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Si NO existe el registro
		If Exists(Select * From OrdenPago Where IDTransaccion=@IDTransaccionOP and Anulado = 'True') Begin
			set @Mensaje = 'La OP esta anulada!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Si la Orden de pago aun no tiene numero de Cheque o se pago con documentos
		If Exists(Select * From vOrdenPago Where IDTransaccion=@IDTransaccionOP and Anulado = 'False' and NroCheque = '') Begin
			set @Mensaje = 'No tiene numero de cheque asignado o se pago con Documento!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Insertar Transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
			
	
		--Insertar en Orden de Pago
		Insert Into EntregaChequeOP(IDTransaccion, IDTransaccionOP, ChequeEntregado, FechaEntrega, Recibo, RetiradoPor, Anulado)
		Values(@IDTransaccionSalida, @IDTransaccionOP, @ChequeEntregado, @FechaEntrega, @Recibo, @RetiradoPor, 'False')
 
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount	
	
	End

	If @Operacion = 'UPD' Begin
	
		--Si NO existe la OP
		If Not Exists(Select * From OrdenPago Where IDTransaccion=@IDTransaccionOP) Begin
			set @Mensaje = 'El registro no existe!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Si NO existe el registro
		If Not Exists(Select * From EntregaChequeOP Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El registro no existe!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		If Exists(Select * From VencimientoChequeEmitido Where IDTransaccionOP=@IDTransaccionOP) Begin
			set @Mensaje = 'Ya existe el registro del Vencimiento del Cheque Emitido!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End


		--Si la Orden de pago aun no tiene numero de Cheque o se pago con documentos
		If Exists(Select * From vOrdenPago Where IDTransaccion=@IDTransaccionOP and Anulado = 'False' and NroCheque = '') Begin
			set @Mensaje = 'No tiene numero de cheque asignado o se pago con Documento!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		--Actualizar
		Update EntregaChequeOP Set	ChequeEntregado = @ChequeEntregado,
									Recibo = @Recibo,
									RetiradoPor = @RetiradoPor,
									FechaEntrega = @FechaEntrega
		Where IDTransaccion = @IDTransaccion
		
		Set @IDTransaccionSalida = @IDTransaccion
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount	
	
	End
	
	If @Operacion = 'ANULAR' Begin
	
		if isnull(@IDTransaccionOP,0) = 0 begin
			Set @IDTransaccionOP = (select IDTransaccionOP from EntregaChequeOP Where IDTransaccion = @IDTransaccion)
		end
		if exists (select * from VVencimientoChequeEmitido where IDTransaccionOP = @IDTransaccionOP) begin
			set @Mensaje = 'Existe un vencimiento asociado a la OP! No se puede anular.'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Validar
		--Asientos Cerrados
		If Exists(Select * From Asiento A Where A.IDTransaccion=@IDTransaccion And A.Conciliado='True') Begin
			set @Mensaje = 'El asiento de este documento ya esta conciliado! No se puede anular.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
			
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal, @IDTerminal=@IDTerminal
		
		--Anulamos el Registro
		Delete EntregaChequeOP Where IDTransaccion = @IDTransaccion
				
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount	
		
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
	
	
End

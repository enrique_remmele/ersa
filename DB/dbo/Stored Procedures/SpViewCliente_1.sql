﻿CREATE Procedure [dbo].[SpViewCliente]

	--Opciones
		
	--Identificadores
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int
	
As

Begin

	--Declare @vEsVendedor bit
	--Declare @vIDvendedor int
	
	--Set @vEsVendedor = (Select EsVendedor From Usuario Where ID=@IDUsuario)
	--If @vEsVendedor = 'True' Begin
	--	Set @vIDvendedor = (Select IDVendedor From Usuario Where ID=@IDUsuario)
	--End
	
	--If @vEsVendedor = 'False' Begin
		Select 
		ID, 
		Referencia,
		RazonSocial, 
		RUC, 
		RazonSocialReferencia,
		NombreFantasia, 
		Direccion, 
		Telefono, 
		ListaPrecio, 
		Descuento, 
		Deuda, 
		SaldoCredito, 
		Vendedor, 
		Promotor, 
		Cobrador, 
		Ciudad, 
		Barrio, 
		ZonaVenta, 
		TieneSucursales,
		Condicion,
		IDMoneda,
		IDListaPrecio,
		CodigoCuentaContable,
		PlazoCredito,
		IDSucursal,
		LimiteCredito,
		ClienteVario,
		Judicial,
		Abogado,
		CodigoSuCursal
		
		From VCliente
		Where Estado='ACTIVA'
		And (IDSucursal=@IDSucursal Or (Case When (Select Top(1) ID From ClienteSucursal Where IDCliente=VCliente.ID And IDSucursal=@IDSucursal) Is Null Then 'False' Else 'True' End) = 'True')
		Order By RazonSocial
	--End
	
	--If @vEsVendedor = 'True' Begin
	--	Select 
	--	ID, 
	--	Referencia,
	--	RazonSocial, 
	--	RUC, 
	--	NombreFantasia, 
	--	Direccion, 
	--	Telefono, 
	--	ListaPrecio, 
	--	Descuento, 
	--	Deuda, 
	--	SaldoCredito, 
	--	Vendedor, 
	--	Promotor, 
	--	Cobrador, 
	--	Ciudad, 
	--	Barrio, 
	--	ZonaVenta, 
	--	TieneSucursales,
	--	Condicion,
	--	IDMoneda,
	--	IDListaPrecio,
	--	CodigoCuentaContable,
	--	PlazoCredito,
	--	IDSucursal,
	--	ClienteVario,
	--	Judicial,
	--	Abogado,
	--	CodigoSuCursal
		
	--	From VCliente
	--	Where Estado='ACTIVA'
	--	And (IDSucursal=@IDSucursal 
	--	Or (Case When 
	--	(Select Top(1) ID From ClienteSucursal 
	--	Where IDCliente=VCliente.ID And IDSucursal=@IDSucursal) Is Null Then 'False' Else 'True' End) = 'True')
	--	And IDVendedor=@vIDvendedor
	--	--And DateDiff(day, GetDate(), UltimaCompra) < 60 
				
	--	Union All
		
	--	Select 
	--	ID, 
	--	Referencia,
	--	RazonSocial, 
	--	RUC, 
	--	NombreFantasia, 
	--	Direccion, 
	--	Telefono, 
	--	ListaPrecio, 
	--	Descuento, 
	--	Deuda, 
	--	SaldoCredito, 
	--	Vendedor, 
	--	Promotor, 
	--	Cobrador, 
	--	Ciudad, 
	--	Barrio, 
	--	ZonaVenta, 
	--	TieneSucursales,
	--	Condicion,
	--	IDMoneda,
	--	IDListaPrecio,
	--	CodigoCuentaContable,
	--	PlazoCredito,
	--	IDSucursal,
	--	ClienteVario,
	--	Judicial,
	--	Abogado,
	--	CodigoSuCursal
		
	--	From VCliente
	--	Where Estado='ACTIVA'
	--	And  (Case When (Select Top(1) ID From ClienteSucursal CS
	--				   Where CS.IDCliente=VCliente.ID And CS.IDSucursal=@IDSucursal And CS.IDVendedor=@vIDvendedor) 
	--	     Is Null Then 'False' Else 'True' End) = 'True'
		  
		 
	--	--And DateDiff(day, GetDate(), UltimaCompra) < 60 
	--	Order By RazonSocial
		
	--End
		
	
End

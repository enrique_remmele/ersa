﻿CREATE Procedure [dbo].[SpDepartamentoInsUpd]

	--Entrada
	@Descripcion varchar(100),
	@IDPais int
	
As

Begin

	Declare @vID int
	
	If @Descripcion = '' Begin
		Set @Descripcion = 'CENTRAL'
	End
		
	--Si existe
	If Exists(Select * From Departamento Where Descripcion = @Descripcion And IDPais=@IDPais) Begin
		Set @vID = (Select Top(1) ID From Departamento Where Descripcion = @Descripcion And IDPais=@IDPais)
		
	End Else Begin
		Set @vID = (Select IsNull(Max(ID)+1,1) From Departamento)
		Insert Into Departamento(ID, IDPais, Descripcion, Orden, Estado)
		Values(@vID, @IDPais, @Descripcion, 0, 'True')
		
	End
	
	Return @vID
	
End


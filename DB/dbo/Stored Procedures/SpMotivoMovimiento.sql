﻿CREATE Procedure [dbo].[SpMotivoMovimiento]

	--Entrada
	@ID tinyint=NULL,
	@Descripcion varchar(50)=NULL,
	@IDTipoMovimiento tinyint=NULL,
	@Estado bit=NULL,
	@CodigoCuentaContable varchar(50)='',
	
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From MotivoMovimiento Where Descripcion=@Descripcion And IDTipoMovimiento=@IDTipoMovimiento) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDMotivoMovimiento tinyint
		set @vIDMotivoMovimiento = (Select IsNull((Max(ID)+1), 1) From MotivoMovimiento)

		--Insertamos
		Insert Into MotivoMovimiento(ID, Descripcion, IDTipoMovimiento, Activo, CodigoCuentaContable)
		Values(@vIDMotivoMovimiento, @Descripcion, @IDTipoMovimiento, @Estado, @CodigoCuentaContable)	
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='MOTIVOMOVIMIENTO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
					
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From MotivoMovimiento Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From MotivoMovimiento Where Descripcion=@Descripcion And ID!=@ID And IDTipoMovimiento=@IDTipoMovimiento) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update MotivoMovimiento Set Descripcion=@Descripcion, 
						IDTipoMovimiento=@IDTipoMovimiento,
						Activo = @Estado,
						CodigoCuentaContable=@CodigoCuentaContable
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='MOTIVOMOVIMIENTO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
					
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From MotivoMovimiento Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Producto
		if exists(Select * From Movimiento Where IDMotivo=@ID) begin
			set @Mensaje = 'El registro tiene movimientos asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From MotivoMovimiento 
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='MOTIVOMOVIMIENTO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
					
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿CREATE Procedure [dbo].[SpRenumerarAsientoCG]
	
	--Entrada
	@Operacion varchar(10),
	@Asiento int,
	@Numero int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	

As

Begin
	
	If @Operacion = 'UPD' Begin
	
		-- Validar si es que existe
		If Not Exists (Select * from AsientoCG Where Numero= @Numero) Begin
			set @Mensaje = 'El sistema no encuentra el registro!'
			set @Procesado = 'False'
			return @@rowcount
		End	
		
		--Actualizar Numero
		Update AsientoCG Set NumeroAsiento = @Asiento
		Where Numero = @Numero
	
		Set @Mensaje = 'Registro procesado!'
		Set @Procesado = 'True'	
		Return @@rowcount
		
	End 
End
﻿CREATE Procedure [dbo].[SpClasificacionProductoAgrupador]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Estado bit,
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From ClasificacionProductoAgrupador Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDClasificacionProductoAgrupador tinyint
		set @vIDClasificacionProductoAgrupador = (Select IsNull((Max(ID)+1), 1) From ClasificacionProductoAgrupador)

		--Insertamos
		Insert Into ClasificacionProductoAgrupador(ID, Descripcion, Estado)
		Values(@vIDClasificacionProductoAgrupador, @Descripcion, @Estado)		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From ClasificacionProductoAgrupador Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From ClasificacionProductoAgrupador Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update ClasificacionProductoAgrupador Set Descripcion=@Descripcion,
						 Estado = @Estado
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From ClasificacionProductoAgrupador Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con alguna clasificacion
		if exists(Select * From ClasificacionProducto Where IDAgrupador=@ID) begin
			set @Mensaje = 'El registro tiene clasificaciones asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From ClasificacionProductoAgrupador 
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


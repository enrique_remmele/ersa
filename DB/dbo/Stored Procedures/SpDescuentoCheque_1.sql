﻿CREATE Procedure [dbo].[SpDescuentoCheque]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDTipoComprobante smallint = NULL,
	@Numero int = null,
	@NroComprobante int = NULL,
	@Fecha date = NULL,
	@IDCuentaBancaria tinyint = NULL,
	@Cotizacion money= NULL,
	@Observacion varchar(100) = NULL,
	@TotalDescontado money = NULL,
	@TotalAcreditado money = NULL,
	@GastoBancario money = NULL,
	@CuentaBancaria bit = Null,	
	
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
		--INICIO SM 27072021 - Auditoria Informática
		Declare @vObservacion varchar(100)='' 
		Declare @vvID int 

	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from DescuentoCheque where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		If Exists(Select * From DescuentoCheque   Where Numero=@Numero And IDSucursal=@IDSucursalOperacion) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Si es que el Nro de Comprobante ya existe
		if exists(Select * From DescuentoCheque    Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante) begin
			set @Mensaje = 'El Nro de Comprobante ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
			
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--INICIO SM 27072021 - Auditoria Informática
		set @vObservacion = concat('Inserta un Descuento Cheque. IDTransaccion: ',@IDTransaccionSalida)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'INSERTAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccionSalida,'DESCUENTO CHEQUE' )
		
		set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccionSalida)
		
		Insert Into aDescuentoCheque(IdAuditoria,IDTransaccion, IDSucursal , Numero, IDTipoComprobante, NroComprobante, Fecha , IDCuentaBancaria , Cotizacion , Observacion ,  TotalAcreditado, TotalDescontado, GastoBancario, CuentaBancaria, Procesado, IDUsuario,Accion)
		Values(@vvID,@IDTransaccionSalida , @IDSucursalOperacion, @Numero, @IDTipoComprobante , @NroComprobante , @Fecha, @IDCuentaBancaria ,  @Cotizacion, @Observacion, @TotalAcreditado, @TotalDescontado, @GastoBancario, @CuentaBancaria,'False',@IDUsuario,'INS')
	
		--FIN SM 27072021 - Auditoria Informática

		--Insertar en DepositoBancario
		Insert Into DescuentoCheque  (IDTransaccion, IDSucursal , Numero, IDTipoComprobante, NroComprobante, Fecha , IDCuentaBancaria , Cotizacion , Observacion ,  TotalAcreditado, TotalDescontado, GastoBancario, CuentaBancaria)
		Values(@IDTransaccionSalida , @IDSucursalOperacion, @Numero, @IDTipoComprobante , @NroComprobante , @Fecha, @IDCuentaBancaria ,  @Cotizacion, @Observacion, @TotalAcreditado, @TotalDescontado, @GastoBancario, @CuentaBancaria)
								
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'DEL' Begin
	
		--27/07/2021 SM Para resguardar datos de transaccion a eliminar
		set @IDUsuario  = (select IDUsuario from Transaccion where ID=@IDTransaccion)
		set @IDTerminal  = (select IDTerminal from Transaccion where ID=@IDTransaccion)
		set @IDOperacion  = (select IDOperacion from Transaccion where ID=@IDTransaccion)
		
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From DescuentoCheque    Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		----Reestablecer Los Cheques
		Exec SpDescuentoChequeProcesar 
		 @IDTransaccion = @IDTransaccion,
		 @Operacion = @Operacion,
		 @Procesado = @Procesado output,
		 @Mensaje = @Mensaje output
		
		If @Procesado = 0 Begin
			return @@rowcount
		End
		
		--INICIO SM 27072021 - Auditoria Informática
		set @vObservacion = concat('Elimina un Descuento Cheque. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'DESCUENTO CHEQUE' )
		
		set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion)
		set @IDSucursalOperacion = (select IDSucursal from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @Numero = (select Numero from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @IDTipoComprobante = (select IDTipoComprobante from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @NroComprobante = (select NroComprobante from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @IDCuentaBancaria = (select IDCuentaBancaria from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @Cotizacion = (select Cotizacion from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @Observacion = (select Observacion from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @TotalAcreditado = (select TotalAcreditado from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @TotalDescontado = (select TotalDescontado from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @GastoBancario = (select GastoBancario from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @CuentaBancaria = (select CuentaBancaria from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @Procesado = (select Procesado from DescuentoCheque where IDTransaccion = @IDTransaccion)

		Insert Into aDescuentoCheque(IdAuditoria,IDTransaccion, IDSucursal , Numero, IDTipoComprobante, NroComprobante, Fecha , IDCuentaBancaria , Cotizacion , Observacion ,  TotalAcreditado, TotalDescontado, GastoBancario, CuentaBancaria,Procesado, IDUsuario,Accion)
		Values(@vvID,@IDTransaccion, @IDSucursalOperacion, @Numero, @IDTipoComprobante , @NroComprobante , @Fecha, @IDCuentaBancaria ,  @Cotizacion, @Observacion, @TotalAcreditado, @TotalDescontado, @GastoBancario, @CuentaBancaria,@Procesado,@IDUsuario,'ELI')
	
   		set @vObservacion = concat('Elimina un Detalle Descuento Cheque. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'DETALLE DESCUENTO CHEQUE' )
		
		set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion)
		Declare @IDTransaccionChequeCliente numeric(18,0) = (select IDTransaccionDescuentoCheque from DetalleDescuentoCheque where IDTransaccionDescuentoCheque  = @IDTransaccion)
		
		Insert Into aDetalleDescuentoCheque(IdAuditoria,IDTransaccionDescuentoCheque, IDTransaccionChequeCliente, IDUsuario,Accion)
		Values(@vvID, @IDTransaccion, @IDTransaccionChequeCliente,@IDUsuario,'ELI')
		--FIN SM 27072021 - Auditoria Informática

		--Eliminamos el detalle
		Delete DetalleDescuentoCheque  where IDTransaccionDescuentoCheque  = @IDTransaccion				
						
		--Eliminamos el registro
		Delete DescuentoCheque   Where IDTransaccion = @IDTransaccion
		
		--INICIO SM 27072021 - Auditoria Informática
		set @vObservacion = concat('Se Eliminara el Detalle de Asiento: IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'DETALLE ASIENTO' )
		
		set @vObservacion = concat('Se Eliminara el Asiento. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'ASIENTO' )
		
		set @vObservacion = concat('Se Eliminara la Transaccion. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'TRANSACCION' )
		--FIN SM 27072021 - Auditoria Informática
						
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion				
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	If @Operacion = 'UPD' Begin
		--27/07/2021 SM Para resguardar datos de transaccion a eliminar
		set @IDUsuario  = (select IDUsuario from Transaccion where ID=@IDTransaccion)
		set @IDTerminal  = (select IDTerminal from Transaccion where ID=@IDTransaccion)
		set @IDOperacion  = (select IDOperacion from Transaccion where ID=@IDTransaccion)
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From DescuentoCheque    Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--INICIO SM 27072021 - Auditoria Informática
		set @vObservacion = concat('Actualiza un Descuento Cheque. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion, @IDTransaccion,'DESCUENTO CHEQUE' )
		
		Update DescuentoCheque Set IDTipoComprobante=@IDTipoComprobante,
									NroComprobante=@NroComprobante,
									Fecha=@Fecha,
									IDCuentaBancaria=@IDCuentaBancaria,
									Cotizacion=@Cotizacion,
									Observacion=@Observacion,
									GastoBancario=@GastoBancario,
									TotalAcreditado=@TotalAcreditado
		Where IDTransaccion=@IDTransaccion								
		
		set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion)
		set @IDSucursalOperacion = (select IDSucursal from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @Numero = (select Numero from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @IDTipoComprobante = (select IDTipoComprobante from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @NroComprobante = (select NroComprobante from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @IDCuentaBancaria = (select IDCuentaBancaria from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @Cotizacion = (select Cotizacion from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @Observacion = (select Observacion from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @TotalAcreditado = (select TotalAcreditado from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @TotalDescontado = (select TotalDescontado from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @GastoBancario = (select GastoBancario from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @CuentaBancaria = (select CuentaBancaria from DescuentoCheque where IDTransaccion = @IDTransaccion)
		set @Procesado = (select Procesado from DescuentoCheque where IDTransaccion = @IDTransaccion)

		Insert Into aDescuentoCheque(IdAuditoria, IDTransaccion, IDSucursal, Numero, IDTipoComprobante, NroComprobante, Fecha, IDCuentaBancaria, Cotizacion, Observacion, TotalAcreditado, TotalDescontado, GastoBancario, CuentaBancaria, Procesado, IDUsuario, Accion)
		Values(@vvID, @IDTransaccion, @IDSucursalOperacion, @Numero, @IDTipoComprobante, @NroComprobante, @Fecha, @IDCuentaBancaria, @Cotizacion, @Observacion, @TotalAcreditado, @TotalDescontado, @GastoBancario, @CuentaBancaria, @Procesado, @IDUsuario, 'UPD')
	
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End







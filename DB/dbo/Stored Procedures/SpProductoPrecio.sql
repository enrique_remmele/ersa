﻿CREATE Procedure [dbo].[SpProductoPrecio]

	--Entrada
	@IDProducto int,
	@IDCliente int,
	@IDMoneda tinyint = 1,
	@PrecioUnico bit,
	@Precio money = 0,
	@Desde date=null,
	@CantidadMinimaPrecioUnico int = 0,
	
	 --select * from aProductoPrecio
	@Operacion varchar(10),
	
	--Transaccion
	@IDUsuario smallint,
	@IDTerminal int,
			
	--Salida
	@Mensaje varchar(200) = '' output,
	@Procesado bit  = 'False' output
As

Begin

	
	if not exists (select * from PorcentajeProductoPrecio where IDUsuario = @IDUsuario) and @Operacion <> 'DEL' begin
		set @Mensaje = 'Usuario sin acceso!'
		set @Procesado = 'False'
		return @@rowcount
	end


	
		
	--INSERTAR
	If @Operacion='INS' Begin
			
		--Monto mayor a 0
		If @Precio < 0 Begin
			set @Mensaje = 'El precio debe ser mayor a 0!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		if exists (select * from ProductoPrecio Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDMoneda=@IDMoneda and PrecioUnico = @PrecioUnico) begin
			set @Mensaje = 'Ya existe un precio para este Producto, Cliente y Moneda!'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Insertar
		Insert Into ProductoPrecio(IDProducto, IDCliente,IDMoneda, PrecioUnico,Precio,Desde,CantidadMinimaPrecioUnico)
							values(@IDProducto, @IDCliente,@IDMoneda, @PrecioUnico,@Precio,@Desde,@CantidadMinimaPrecioUnico)

		--Auditoria
		Insert Into aProductoPrecio(IDProducto, IDCliente,IDMoneda, PrecioUnico,Precio,Desde,CantidadMinimaPrecioUnico,FechaOperacion,IDUsuarioOperacion,IDTerminalOperacion,Operacion)
							values(@IDProducto, @IDCliente,@IDMoneda, @PrecioUnico,@Precio,@Desde,@CantidadMinimaPrecioUnico,GetDate(),@IDUsuario,@IDTerminal,@Operacion)
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount

	End

	--ELIMINAR
	If @Operacion='DEL' Begin

		--Verificar que los ID´s ya no esten cargados
		If Not Exists(Select * From ProductoPrecio Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDMoneda=@IDMoneda and PrecioUnico  = @PrecioUnico) Begin
			set @Mensaje = 'El sistema no encontro el registro seleccionado! Favor de verificar la informacion y continuar.'
			set @Procesado = 'False'
			return @@rowcount
		End
		--Auditoria
		Insert Into aProductoPrecio(IDProducto, IDCliente,IDMoneda, PrecioUnico,Precio,Desde, IDTransaccionPedidoPrecioUnico,CantidadMinimaPrecioUnico,FechaOperacion,IDUsuarioOperacion,IDTerminalOperacion,Operacion)
							select IDProducto, IDCliente,IDMoneda, PrecioUnico,Precio,Desde, IDTransaccionPedidoPrecioUnico,CantidadMinimaPrecioUnico,GetDate(),@IDUsuario,@IDTerminal,@Operacion from ProductoPrecio Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDMoneda=@IDMoneda and PrecioUnico = @PrecioUnico
		
		--Eliminar
		Delete From ProductoPrecio
		Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDMoneda=@IDMoneda and PrecioUnico = @PrecioUnico
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount	
	End
		

	set @Mensaje = 'No se realizo ninguna operacion!'
	set @Procesado = 'False'
	return @@rowcount
		
End







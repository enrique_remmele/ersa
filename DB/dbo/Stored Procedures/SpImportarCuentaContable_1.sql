﻿CREATE Procedure [dbo].[SpImportarCuentaContable]

	--Entrada
	@PlanCuenta varchar(100) = NULL,
	@Codigo varchar(50) = NULL,
	@Descripcion varchar(200) = NULL,
	@Alias varchar(100) = NULL,
	@Categoria int = NULL,
	@Imputable varchar(10) = NULL,
	@CodigoPadre varchar(50) = NULL,
	@Estado bit = NULL,
	
	@Actualizar bit
	
As

Begin

	Declare @vID int
	Declare @vIDPlanCuenta int
	Declare @vIDPadre int
	
	Declare @vOperacion varchar(50)
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	
	Set @vMensaje = 'No se proceso'
	Set @vProcesado = 'False'
	Set @vOperacion = 'INS'
	
	--Obtener codigos
	Begin
		
		--Plan de Cuenta
		If Not Exists(Select * From PlanCuenta Where Descripcion=@PlanCuenta) Begin
			Set @vIDPlanCuenta = (Select ISNULL(Max(ID)+1,1) From PlanCuenta)
			Insert Into PlanCuenta(ID, Descripcion, Titular, Resolucion173, Estado)
			Values(@vIDPlanCuenta, @PlanCuenta, 'False', 'False', 'True')						
		End Else Begin
			Set @vIDPlanCuenta = (Select Top(1) ID From PlanCuenta Where Descripcion=@PlanCuenta)			
		End
	
		--IDPadre
		Set @vIDPadre = (Select ISNULL((Select Top(1) ID From CuentaContable Where Codigo=@CodigoPadre And IDPlanCuenta=@vIDPlanCuenta),0))
		
		--Obtenemos el IDPadre si es que no se define
		If @vIDPadre = 0 And @Categoria>1 Begin

			Declare @vCodigoPropio varchar(50) = ''
			Declare @vCodigoPadre varchar(50) = ''
			
			--El nivel 2 tiene solo 1 digito el codigo propio
			If @Categoria =  Len(@Codigo) Begin
				Set @vCodigoPropio = SUBSTRING(@Codigo, Len(@Codigo), 1)
				set @vCodigoPadre = SUBSTRING(@Codigo, 1, Len(@Codigo)-1)
			End

			If @Categoria <> Len(@Codigo) Begin
				Set @vCodigoPropio = SUBSTRING(@Codigo, Len(@Codigo)-1, 2)
				set @vCodigoPadre = SUBSTRING(@Codigo, 1, Len(@Codigo)-2)
			End
		
			--IDPadre
			Set @vIDPadre = (Select ISNULL((Select Top(1) ID From CuentaContable Where Codigo=@vCodigoPadre And IDPlanCuenta=@vIDPlanCuenta),0))
			
			print @vCodigoPropio
			print @vCodigoPadre
			print @vIDPadre

		End

		--ID
		If Not Exists(Select * From CuentaContable Where Codigo=@Codigo And IDPlanCuenta=@vIDPlanCuenta) Begin
			Set @vOperacion = 'INS'
		End Else Begin
			Set @vID = (Select Top(1) ID From CuentaContable Where Codigo=@Codigo And IDPlanCuenta=@vIDPlanCuenta)
			Set @vOperacion = 'UPD'
		End
		
		--Imputable
		If @Imputable = 'NO' Or @Imputable = 'No' Begin
			Set @Imputable = 'False'
		End

		If @Imputable = 'Si' Or @Imputable = 'SI' Begin
			Set @Imputable = 'True'
		End

	End
	
	--La categoria empieza con 1
	--Set @Categoria = @Categoria + 1
	
	--Validar
	If @Categoria > 1 And @vIDPadre=0 Begin
		Set @vMensaje = 'La cuenta no tiene Padre correspondiente!'
		Set @vProcesado = 'False'
		Goto Salir
	End
		
	--INSERTAR
	If @vOperacion = 'INS' Begin
		
		Set @vID = (Select ISNULL(Max(ID)+1,1) From CuentaContable)
		
		Insert Into CuentaContable(ID, IDPlanCuenta, Codigo, Descripcion, Alias, Categoria, Imputable, IDPadre, Estado, Sucursal, IDSucursal)
		Values(@vID, @vIDPlanCuenta, @Codigo, @Descripcion, @Alias, @Categoria, @Imputable, @vIDPadre, @Estado, 'False', NULL)
		
		Set @vMensaje = 'Registro insertado'
		Set @vProcesado = 'True'
		
	End
		
	--ACTUALIZAR
	If @vOperacion = 'UPD' Begin	
			
		If @Actualizar = 'False' Begin
			Set @vMensaje = 'Sin Actualizar'
			Set @vProcesado = 'True'
			Goto Salir
		End
		
		Update CuentaContable Set Codigo=@Codigo, 
									Descripcion=@Descripcion, 
									Alias=@Alias,
									Categoria=@Categoria,
									Imputable=@Imputable,
									IDPadre=@vIDPadre, 
									Estado=@Estado, 
									Sucursal='False'									
		Where ID=@vID	
		
		Set @vMensaje = 'Registro actualizado'
		Set @vProcesado = 'True'
		
	End	
		

Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	
End

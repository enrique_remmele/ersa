﻿CREATE Procedure [dbo].[SpChequeClienteModificar]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalDocumento tinyint = NULL,
	@Numero int = NULL,
	@IDCliente int = NULL,
	@IDBanco tinyint = NULL,
	@CuentaBancaria int = NULL,
	@Fecha date = NULL,
	@NroCheque varchar(50) = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money = NULL,
	@Importe money = NULL,
	@ImporteMoneda money = NULL,
	@Diferido bit = NULL,
	@FechaVencimiento date = NULL,
	@ChequeTercero bit = NULL,
	@Librador varchar(50) = NULL,
	@Saldo money = NULL,
	@Operacion varchar(50),
		
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
	
	--Validar Feha Operacion

	Declare @vIDOperacion as integer

	Select	@vIDOperacion=IDOperacion
	From Transaccion Where ID = @IDTransaccion

--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @vIDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from ChequeCliente where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @vIDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
	
		--ACTUALIZAR
	If @Operacion='UPD' Begin
	
		--Validar
	
		--Actualizamos
		Update ChequeCliente Set IDTransaccion=@IDTransaccion ,
								 IDSucursal=@IDSucursal,
								 Numero=@Numero, 
								 IDCliente=IDCliente, 
								 IDBanco=@IDBanco,
								 IDCuentaBancaria=@CuentaBancaria, 
								 Fecha=@Fecha, 
								 NroCheque=@NroCheque, 
								 IDMoneda=@IDMoneda, 
								 Cotizacion=@Cotizacion, 
								 Importe=@Importe, 
								 ImporteMoneda=@ImporteMoneda,
								 Diferido=@Diferido, 
								 FechaVencimiento=@FechaVencimiento, 
								 ChequeTercero=@ChequeTercero, 
								 Librador=@Librador, 
								 Saldo=@Saldo 
		   	
		Where IDTransaccion=@IDTransaccion 
		
		
	
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End


End



﻿CREATE Procedure [dbo].[SpControlInventarioAjusteProvisorio]

	--Entrada
	@IDTransaccion numeric(18,0),
	@IDDepositoAfectado smallint,
	@IDProducto int,
	@TipoDocumento varchar(50),
	@Comprobante varchar(50),
	@Observacion varchar(50),
	@Cantidad decimal(10,2),
	@Operacion varchar(10),
	
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin

	--Actualizar
	If @Operacion = 'INS' Begin
		
		--Validar
		If (Select Procesado From ControlInventario Where IDTransaccion=@IDTransaccion) = 'True' Begin
			set @Mensaje = 'El registro ya esta procesado! No se puede actualizar.'
			set @Procesado = 'True'
			return @@rowcount
		End
		
		Declare @vID tinyint
		Declare @vProvisorio decimal(10,2)
		Declare @vExistencia decimal(10,2)
		
		Set @vID = IsNull((Select MAX(ID)+1 From ControlInventarioAjusteProvisorio), 1)
		
		Insert Into ControlInventarioAjusteProvisorio(IDTransaccion, IDDeposito, IDProducto, ID, TipoDocumento, Comprobante, Observacion, Cantidad, IDUsuario, Fecha)
		Values(@IDTransaccion, @IDDeposito, @IDProducto, @vID, @TipoDocumento, @Comprobante, @Observacion, @Cantidad, @IDUsuario, GETDATE())
		
		Set @vProvisorio = (Select Provisorio From ExistenciaDepositoInventario Where IDTransaccion=@IDTransaccion And IDDeposito=@IDDeposito And IDProducto=@IDProducto)
		Set @vExistencia = (Select CantidadDeposito From ExistenciaDepositoInventario Where IDTransaccion=@IDTransaccion And IDDeposito=@IDDeposito And IDProducto=@IDProducto)
		
		Set @vProvisorio = @vProvisorio + @Cantidad
		Set @vExistencia = @vExistencia + @vProvisorio
		
		Update ExistenciaDepositoInventario Set Provisorio = @vProvisorio, 
												Existencia = @vExistencia
		Where IDTransaccion=@IDTransaccion And IDDeposito=@IDDeposito And IDProducto=@IDProducto
		
		Exec SpActualizarControlInventario @IDTransaccion=@IDTransaccion
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	set @Mensaje = 'No se proceso el registro'
	set @Procesado = 'False'
	return @@rowcount
		
End
	
	


﻿Create Procedure [dbo].[SpCuentaBancariaSaldoDiario]

	--Entrada
	@IDCuentaBancaria smallint,
	@Saldo money,
	@Fecha datetime,
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin
	--VARIABLES
	Declare @vID int

	--Verificar si ya existe
	If exists(select * from CuentaBancariaSaldoDiario where IDCuentaBancaria = @IDCuentaBancaria and cast(fecha as date)= @Fecha) begin

		Update CuentaBancariaSaldoDiario
		set Saldo = @Saldo
		where IDCuentaBancaria = @IDCuentaBancaria
		and Fecha = @Fecha

		--Auditoria
		Exec SpLogSuceso @Operacion='UPD', @Tabla='CUENTABANCARIASALDODIARIO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal

	 end  else if not exists(select * from CuentaBancariaSaldoDiario where IDCuentaBancaria = @IDCuentaBancaria and cast(fecha as date)= @Fecha) begin
		Insert Into CuentaBancariaSaldoDiario(IDCuentaBancaria, Fecha, Saldo)
		Values(@IDCuentaBancaria,@Fecha,@Saldo)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion='INS', @Tabla='CUENTABANCARIASALDODIARIO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
	End	
		
	set @Mensaje = 'Registro guardado!'
	set @Procesado = 'True'
	return @@rowcount
				
End


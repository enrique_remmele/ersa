﻿CREATE Procedure [dbo].[SpControlarActividad]

	--Entrada
	@IDTransaccion numeric(18,0)
	
As

Begin
	
	Declare @vMensaje varchar(100)
	Declare @vProcesado bit
	
	Declare @vIDActividad int
	Declare @vActividad varchar(50)
	Declare @vAsignado money
	Declare @vSaldo money
	Declare @vDescuento money

	Set @vMensaje = 'Sin control'
	Set @vProcesado = 'True'
	
	Declare db_Actividades cursor for
	Select IDActividad, Actividad, Sum(Descuento) From VDescuento 
	Where IDTransaccion=@IDTransaccion
	Group By IDActividad, Actividad
	Open db_Actividades   
	Fetch Next From db_Actividades Into @vIDActividad, @vActividad, @vDescuento
	While @@FETCH_STATUS = 0 Begin 

		Set @vAsignado = IsNull((Select ImporteAsignado From Actividad Where ID=@vIDActividad),0)
		
		--Solo si Asignado es mayor a 0
		If @vAsignado > = 0 Begin
			Set @vMensaje = 'Sin control'
			Set @vProcesado = 'True'
			GoTo Seguir
		End
		
		Set @vSaldo = IsNull((Select Saldo From Actividad Where ID=@vIDActividad),0)
			
		--Calcular
		If  @vSaldo < @vDescuento Begin
			
			Set @vMensaje = 'No hay sufuciente saldo para la actividad ' + @vActividad + ' - Saldo: ' + CONVERT(varchar(50), @vActividad)
			Set @vProcesado = 'False'
			
			--Cierra el cursor
			Close db_Actividades 
			Deallocate db_Actividades
	
			GoTo Salir
		End
			
Seguir:

		Fetch Next From db_Actividades Into @vIDActividad, @vActividad, @vDescuento
												
	End

	--Cierra el cursor
	Close db_Actividades 
	Deallocate db_Actividades

Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


﻿CREATE Procedure [dbo].[SpAnticipoAplicacion]
	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDTransaccionCobranza numeric(18,0) = NULL,
	@Numero int = 0,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante varchar(50) = NULL,
	@Fecha date = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDMoneda tinyint = Null,
	@Cotizacion money = null,
	--@IDCobrador tinyint = NULL, 
	@DiferenciaCambio money = 0,
	@Total money = NULL,
	@Observacion varchar(200) = NULL,
	@Operacion varchar(50) = '',
		
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	@IDTransaccionCaja numeric(18,0) = 0,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @fecha = (Select Fecha from AnticipoAplicacion where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		Declare @vNumero int
		Set @vNumero = ISNull((Select MAX(Numero)+1 From AnticipoAplicacion Where IDSucursal=@IDSucursalOperacion),1)
		
		--Si ya existe el documento
		If @NroComprobante!='' Begin
			If Exists(Select * From AnticipoAplicacion Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante And IDSucursal=@IDSucursalOperacion And Anulado='False' and Procesado = 'True') Begin
				set @Mensaje = 'El numero de comprobante ya existe!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
		End
		
		--Total
		If @Total <= 0 Begin
			set @Mensaje = 'El importe debe ser mayor a 0!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Insertar Transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@IDTransaccionCaja=@IDTransaccionCaja,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
			
		--Insertar en Cobranza Credito
		Insert Into AnticipoAplicacion(IDTransaccion, IDTransaccionCobranza,Numero,IDTipoComprobante,NroComprobante,IDSucursal,
										Fecha,IDMoneda,Cotizacion,Anulado,Observacion,Total,DiferenciaCambio, Procesado)
		Values(@IDTransaccionSalida,@IDTransaccionCobranza,@Numero,@IDTipoComprobante,@NroComprobante,@IDSucursalOperacion,
			  @Fecha,@IDMoneda,@Cotizacion,'False',@Observacion,@Total,@DiferenciaCambio, 'False')
					
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	--ANULAR
	If @Operacion = 'ANULAR' Begin
	--La cobranza ya esta anulada
			If Exists(select * from AnticipoAplicacion where IDTransaccion = @IDTransaccion and Anulado = 'True') Begin
				set @Mensaje = 'La Aplicacion ya esta anulada.'
				set @Procesado = 'False'
				return @@rowcount
			End
		
		--Asientos Cerrados
		If Exists(Select * From Asiento A Where A.IDTransaccion=@IDTransaccion And A.Conciliado='True') Begin
			set @Mensaje = 'El asiento de este documento ya esta conciliado! No se puede anular.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Verificar dias de bloqueo
		If (Select Top(1) CobranzaCreditoBloquearAnulacion From Configuraciones) = 'True' Begin
			
			Declare @vDias tinyint
			Set @vDias = (Select Top(1) CobranzaCreditoDiasBloqueo From Configuraciones)

			If (Select DATEDIFF(dd,(Select V.Fecha From AnticipoAplicacion V Where IDTransaccion=@IDTransaccion), GETDATE())) > @vDias Begin
			 			
				set @Mensaje = 'El sistema no puede anular este registro ya que la configuracion lo restringe por la antigüedad del documento. Cambie la configuracion de bloqueo para anular!'
				set @Procesado = 'False'
				return @@rowcount
				
			End
			
		End
		
		--Procesar
		--Reestablecemos las Ventas y Saldo del Cliente
		EXEC SpVentaAnticipo @IDTransaccionAnticipo = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
				
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Auditoria de documentos
		Declare @IDCliente as integer
		Declare @Comprobante as varchar(16)
		set @IDTipoComprobante = (select IDTipoComprobante from AnticipoAplicacion where IDTransaccion = @IDTransaccion)
		set @IDCliente = (select IDCliente from CobranzaCredito CC Join AnticipoAplicacion AA on AA.IDTransaccionCobranza = CC.IDTransaccion where AA.IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from AnticipoAplicacion where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from AnticipoAplicacion where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select NroComprobante from AnticipoAplicacion where IDTransaccion = @IDTransaccion)
		declare @VTipoComprobante varchar(16) = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from AnticipoAplicacion where IDTransaccion = @IDTransaccion)
		declare @Condicion varchar(16) = 'ANTICIPO.APLIC.'

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,@IDCliente,0,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ANU',@VTipoComprobante, @IDSucursal)

				
		--Anulamos el registro
		Update AnticipoAplicacion Set Anulado='True', IDUsuarioAnulado=@IDUsuario, FechaAnulado=GETDATE()
		Where IDTransaccion = @IDTransaccion
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal		
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		GoTo Salir
		
	End
	
	--ELIMINAR
	If @Operacion = 'DEL' Begin
	
		--Validar		
		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from AnticipoAplicacion where IDTransaccion = @IDTransaccion)
		set @IDCliente = (select IDCliente from CobranzaCredito CC Join AnticipoAplicacion AA on AA.IDTransaccionCobranza = CC.IDTransaccion where AA.IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from AnticipoAplicacion where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from AnticipoAplicacion where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select NroComprobante from AnticipoAplicacion where IDTransaccion = @IDTransaccion)
		set @VTipoComprobante = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from AnticipoAplicacion where IDTransaccion = @IDTransaccion)
		set @Condicion = 'ANTICIPO.APLIC.'

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,@IDCliente,0,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ELI',@VTipoComprobante, @IDSucursal)


		--Eliminamos el registro
		Delete AnticipoAplicacion Where IDTransaccion = @IDTransaccion
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		GoTo Salir
		
	End

Salir:
	
	--Actualizar saldos de clientes
	Begin
		Declare @vIDTransaccionVenta numeric(18,0)
		Declare @vIDCliente int
		
		Declare db_cursor cursor for
		Select IDTransaccionVenta From AnticipoVenta Where IDTransaccionAnticipo=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVenta
		While @@FETCH_STATUS = 0 Begin 
			
			Set @vIDCliente = (Select IDCliente From Venta Where IDTransaccion=@vIDTransaccionVenta)
			
			Exec SpAcualizarSaldoCliente @IDCliente=@vIDCliente
			
			Fetch Next From db_cursor Into @vIDTransaccionVenta
										
		End
		
		Close db_cursor   
		Deallocate db_cursor
		
	End
		
End


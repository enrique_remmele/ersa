﻿CREATE Procedure [dbo].[SpDetalleRendicionLoteDocumento]

	--Entrada
	@IDTransaccion numeric(18,0),
	@ID tinyint = NULL,
	@TipoComprobante varchar(50) = NULL,
	@NroComprobante varchar(50) = NULL,
	@IDTransaccionVenta numeric(18,0) = NULL,
	@Importe money = NULL,
	@Aplicar bit = NULL,
	@Observacion varchar(100) = NULL,
	
	--Operacion
	@Operacion varchar(10),
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
As

Begin
	
	--Procesar Saldos de Ventas
	If @Operacion = 'INS' Begin
	
		--Validar
		--Asegurarse que exista la transaccion
		if Not Exists(Select * From RendicionLote Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra el registro!'
			Set @Procesado = 'False'
			Return @@rowcount
		End
		
		--si es que ya existe el detalle, eliminar primeramente
		--se saco poq no c tiene idea
		--If Exists(Select * From DetalleRendicionLoteDocumento Where IDTransaccion=@IDTransaccion) Begin
		--	Delete From DetalleRendicionLoteDocumento Where IDTransaccion=@IDTransaccion 	
		--End
		
		--Insertar
		Insert Into DetalleRendicionLoteDocumento(IDTransaccion, ID, TipoComprobante, NroComprobante, IDTransaccionVenta, Importe, Aplicar, Observacion )
		Values(@IDTransaccion, @ID, @TipoComprobante, @NroComprobante, @IDTransaccionVenta, @Importe, @Aplicar, @Observacion)
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		
		Return @@rowcount
		
	End
	
	If @Operacion = 'ANULAR' Begin
		
		--Validar
		--Que exista el registro
		if Not Exists(Select * From RendicionLote Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra el registro!'
			Set @Procesado = 'False'
			Return @@rowcount
		End
	
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		Return @@rowcount
		
	End
	
	If @Operacion = 'DEL' Begin
		
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'false'
		Return @@rowcount
		
	End
		
	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'
	
End




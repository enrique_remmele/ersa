﻿--Select * From ComisionProducto
CREATE Procedure [dbo].[SpImportarComision]

	--Entrada
	@Codigo varchar(50),
	@Vendedor varchar(50),
	@IDSucursal int,
	@PorcentajeEsteProducto varchar(50),
	
	@Actualizar bit = 'True'

As

Begin

	--Variables
	Declare @vIDProducto int
	Declare @vIDVendedor int
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	
	--Obtener valores
	Set @vMensaje = 'No se proceso'
	Set @vProcesado = 'False'
	Set @vIDProducto=(Select ID From Producto Where Referencia=@Codigo And Estado='True')
	Set @vIDVendedor=(Select ID From Vendedor Where Referencia=@Vendedor And IDSucursal=@IDSucursal And Estado='True')
	
	--Modificar Porcentaje
	Set @PorcentajeEsteProducto = REPLACE(@PorcentajeEsteProducto, ',','.')
	Set @PorcentajeEsteProducto = CONVERT(varchar(50), convert(decimal(10,2), @PorcentajeEsteProducto))
	
	--Verificar que existan los
	If @vIDProducto Is Null Begin
		Set @vMensaje = 'No se encontro el producto!'
		Set @vProcesado = 'False'
		GoTo Salir
	End
	
	If @vIDVendedor Is Null Begin
		Set @vMensaje = 'No se encontro el vendedor!'
		Set @vProcesado = 'False'
		GoTo Salir
	End
	
	--Si existe, actualizamos
	If Exists(Select * From ComisionProducto Where IDProducto=@vIDProducto And IDVendedor=@vIDVendedor) Begin
		
		If @Actualizar = 'True' Begin
			
			Update ComisionProducto Set PorcentajeEsteProducto=@PorcentajeEsteProducto
			Where IDProducto=@vIDProducto And IDVendedor=@vIDVendedor And IDSucursal=@IDSucursal
			
		End
		
		Set @vMensaje = 'Sin actualizacion!'
		Set @vProcesado = 'True'
		GoTo Salir
			
	End
	
	--Si no existe, insertamos
	If Not Exists(Select * From ComisionProducto Where IDProducto=@vIDProducto And IDVendedor=@vIDVendedor) Begin
		
		Insert Into ComisionProducto(IDProducto, IDVendedor, IDSucursal, PorcentajeBase, PorcentajeEsteProducto, PorcentajeAnterior, FechaUltimoCambio, IDUsuario)
		Values(@vIDProducto, @vIDVendedor, @IDSucursal, 0, @PorcentajeEsteProducto, 0, NULL, 1)
		
		Set @vMensaje = 'Registro insertado!'
		Set @vProcesado = 'True'
		GoTo Salir
		
	End
	
Salir:	
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	
End
	


	


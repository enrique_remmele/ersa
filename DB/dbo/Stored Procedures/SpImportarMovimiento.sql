﻿CREATE Procedure [dbo].[SpImportarMovimiento]

	--Entrada
	@TipoMovimiento varchar(5) = 'E',
	@TipoComprobante varchar(50) = '',
	@Fecha varchar(50),
	@NroComprobante varchar(50) = '',
		
	--Deposito
	@DepositoSalida varchar(50),
	@DepositoEntrada varchar(50),
	
	--Otros
	@Observacion varchar(200) = '',
	
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Detalle
	@CodigoProducto varchar(50),
	@IVA int,
	@Cantidad decimal(10,2),
	@CostoUnitario money,
	
	@Actualizar bit
	
As

Begin
		
	--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		
		Declare @vOperacion varchar(50)
		Declare @vIDTransaccion int
		Declare @vIDTipoOperacion tinyint
		Declare @vIDMotivo tinyint
		Declare @vIDTipoComprobante smallint
		Declare @vTotal money
		Declare @vTotalImpuesto money
		Declare @vTotalDiscriminado money
		Declare @vTotalDescuento money
		Declare @vIDSucursal int
		Declare @vIDDepositoEntrada int
		Declare @vIDDepositoSalida int
		Declare @vIDTipoMovimiento int
					
	End
	
	--Movimiento
	Begin
		
		--Establecer valores predefinidos
		Begin
			
			Set @vMensaje = 'No se proceso!'
			Set @vProcesado = 'False'
			Set @vOperacion = 'INS'
			
			--Tipo de Operacion
			Begin
				
				--ENTRADA
				If @TipoMovimiento='E' Begin 
					
					Set @vIDTipoOperacion = (Select Top(1) ID From TipoOperacion Where IDOperacion=@IDOperacion And Descripcion='ENTRADA') 
					
					--Si no existe el tipo de operacion, insertamos
					If @vIDTipoOperacion Is Null Begin
						
						Set @vIDTipoOperacion = ISNULL((Select MAX(ID)+1 From TipoOperacion),1)
						Insert Into TipoOperacion(ID, IDOperacion, Descripcion, Activo, Entrada, Salida)
						Values(@vIDTipoOperacion, @IDOperacion, 'ENTRADA', 'True', 'True', 'False')
						
						Set @vIDTipoOperacion = (Select Top(1) ID From TipoOperacion Where IDOperacion=@IDOperacion And Descripcion='ENTRADA') 
					End
					 				
				End
				
				--SALIDA
				If @TipoMovimiento='S' Begin 
					
					Set @vIDTipoOperacion = (Select Top(1) ID From TipoOperacion Where IDOperacion=@IDOperacion And Descripcion='SALIDA') 
					
					--Si no existe el tipo de operacion, insertamos
					If @vIDTipoOperacion Is Null Begin
						
						Set @vIDTipoOperacion = ISNULL((Select MAX(ID)+1 From TipoOperacion),1)
						Insert Into TipoOperacion(ID, IDOperacion, Descripcion, Activo, Entrada, Salida)
						Values(@vIDTipoOperacion, @IDOperacion, 'SALIDA', 'True', 'False', 'True')
						
						Set @vIDTipoOperacion = (Select Top(1) ID From TipoOperacion Where IDOperacion=@IDOperacion And Descripcion='SALIDA') 
					End
					 				
				End
				
				--TRANSFERENCIA
				If @TipoMovimiento='T' Begin 
					
					Set @vIDTipoOperacion = (Select Top(1) ID From TipoOperacion Where IDOperacion=@IDOperacion And Descripcion='TRANSFERENCIA') 
					
					--Si no existe el tipo de operacion, insertamos
					If @vIDTipoOperacion Is Null Begin
						
						Set @vIDTipoOperacion = ISNULL((Select MAX(ID)+1 From TipoOperacion),1)
						Insert Into TipoOperacion(ID, IDOperacion, Descripcion, Activo, Entrada, Salida)
						Values(@vIDTipoOperacion, @IDOperacion, 'TRANSFERENCIA', 'True', 'True', 'True')
						
						Set @vIDTipoOperacion = (Select Top(1) ID From TipoOperacion Where IDOperacion=@IDOperacion And Descripcion='TRANSFERENCIA') 
					End
					 				
				End
			End

			--Tipo de Comprobante
			Set @vIDTipoComprobante = (IsNull((Select Top(1) ID From TipoComprobante Where Codigo=@TipoComprobante And IDOperacion=@IDOperacion), 0))
			If @vIDTipoComprobante = 0 Begin
				Set @vIDTipoComprobante = ISNULL((Select MAX(ID)+1 From TipoComprobante),1)
				Insert Into TipoComprobante(ID, Cliente, Proveedor, IDOperacion, Descripcion, Codigo, Resumen, Estado, ComprobanteTimbrado, CalcularIVA, IVAIncluido, LibroVenta, LibroCompra, Signo, Autonumerico, Deposito, Restar, HechaukaTipoDocumento, HechaukaTimbradoReemplazar, HechaukaNumeroTimbrado, EsInterno)
				Values(@vIDTipoComprobante, 'True', 'False', @IDOperacion, @TipoComprobante, @TipoComprobante, @TipoComprobante, 'True', 'False', 'False', 'False', 'False', 'False',			'+', 'False',		'False',  'False', 'False',				'False',					'False',				'False')
			End
			
			--Motivo
			Begin
				
				--ENTRADA
				If @TipoMovimiento='E' Begin 
					Set @vIDTipoMovimiento = (Select Top(1) ID From TipoOperacion Where IDOperacion=@IDOperacion And Entrada='True' And Salida='False')
					Set @vIDMotivo = (Select Top(1) ID From MotivoMovimiento Where IDTipoMovimiento=@vIDTipoMovimiento And Descripcion='AJUSTE POSITIVO') 
					
					--Si no existe el motivo, insertamos
					If @vIDMotivo Is Null Begin
						
						Set @vIDMotivo = ISNULL((Select MAX(ID)+1 From MotivoMovimiento),1)
						Insert Into MotivoMovimiento(ID, IDTipoMovimiento, Descripcion, Activo)
						Values(@vIDMotivo, @vIDTipoOperacion, 'AJUSTE POSITIVO', 'True')
						
						Set @vIDMotivo = (Select Top(1) ID From MotivoMovimiento Where IDTipoMovimiento=@vIDTipoMovimiento And Descripcion='AJUSTE POSITIVO') 
						
					End
					 				
				End
				
				--SALIDA
				If @TipoMovimiento='S' Begin 
					Set @vIDTipoMovimiento = (Select Top(1) ID From TipoOperacion Where IDOperacion=@IDOperacion And Entrada='False' And Salida='True')
					Set @vIDMotivo = (Select Top(1) ID From MotivoMovimiento Where IDTipoMovimiento=@vIDTipoMovimiento And Descripcion='AJUSTE NEGATIVO') 
					
					--Si no existe el motivo, insertamos
					If @vIDMotivo Is Null Begin
						
						Set @vIDMotivo = ISNULL((Select MAX(ID)+1 From MotivoMovimiento),1)
						Insert Into MotivoMovimiento(ID, IDTipoMovimiento, Descripcion, Activo)
						Values(@vIDMotivo, @vIDTipoOperacion, 'AJUSTE NEGATIVO', 'True')
						
						Set @vIDMotivo = (Select Top(1) ID From MotivoMovimiento Where IDTipoMovimiento=@vIDTipoMovimiento And Descripcion='AJUSTE NEGATIVO') 
						
					End
					 				
				End
				
				--TRANSFERENCIA
				If @TipoMovimiento='T' Begin 
					Set @vIDTipoMovimiento = (Select Top(1) ID From TipoOperacion Where IDOperacion=@IDOperacion And Entrada='True' And Salida='True')
					Set @vIDMotivo = (Select Top(1) ID From MotivoMovimiento Where IDTipoMovimiento=@vIDTipoMovimiento And Descripcion='TRANSFERENCIAS') 
					
					--Si no existe el motivo, insertamos
					If @vIDMotivo Is Null Begin
						
						Set @vIDMotivo = ISNULL((Select MAX(ID)+1 From MotivoMovimiento),1)
						Insert Into MotivoMovimiento(ID, IDTipoMovimiento, Descripcion, Activo)
						Values(@vIDMotivo, @vIDTipoOperacion, 'TRANSFERENCIAS', 'True')
						
						Set @vIDMotivo = (Select Top(1) ID From MotivoMovimiento Where IDTipoMovimiento=@vIDTipoMovimiento And Descripcion='TRANSFERENCIAS') 
					
					End
					 				
				End
			End
						
			Set @NroComprobante = CONVERT(int, @NroComprobante)
			
			--Sucursal
			Set @vIDSucursal = @IDSucursal
			
			--Deposito, solo tenemos el codigo del deposito,
			--En este caso, debemos recurrir a la tabla vendedores para saber el IDDeposito
			If ISNUMERIC(@DepositoEntrada) = 1 Begin			
				Set @vIDDepositoEntrada = (Select Top(1) IDDeposito From Vendedor Where IDSucursal=@vIDSucursal And Convert(int, Referencia)=Convert(int, @DepositoEntrada))
			End
			
			If ISNUMERIC(@DepositoSalida) = 1 Begin			
				Set @vIDDepositoSalida = (Select Top(1) IDDeposito From Vendedor Where IDSucursal=@vIDSucursal And Convert(int, Referencia)=Convert(int, @DepositoSalida))
			End													
			
		End
		
		print 'Deposito Entrada: ' + convert(varchar(50), @vIDDepositoEntrada)
		print 'Deposito Salida: ' + convert(varchar(50), @vIDDepositoSalida)
		
		--Verificar si ya existe
		If Exists(Select * From MovimientoImportado MI  Join Movimiento M On MI.IDTransaccion= M.IDTransaccion Where MI.NroOperacion=@NroComprobante And MI.TipoComprobante=@TipoComprobante) Begin
			set @vIDTransaccion = (Select Top(1) MI.IDTransaccion From MovimientoImportado MI  Join Movimiento M On MI.IDTransaccion= M.IDTransaccion Where MI.NroOperacion=@NroComprobante And MI.TipoComprobante=@TipoComprobante)
			Set @vOperacion = 'UPD'
		End
		
		--ACTUALIZAR
		If @vOperacion = 'UPD' Begin
		
			If @Actualizar = 'True' Begin
				
				--Movimiento de Importacion
				--Actualizar en MovimientoImportacion
							
							
				--Movimiento
				Update Movimiento Set Fecha=@Fecha,
										IDTipoComprobante=@vIDTipoComprobante,
										IDMotivo=@vIDMotivo,
										IDDepositoEntrada=@vIDDepositoEntrada,
										IDDepositoSalida=@vIDDepositoSalida										
				Where IDTransaccion = @vIDTransaccion
				
				Set @vMensaje = 'Actualizado!'
				Set @vProcesado = 'True'
				GoTo Detalle
				
			End
			
			Set @vMensaje = 'Sin Act.!'
			Set @vProcesado = 'True'
			GoTo Salir
			
		End
		
		--INSERTAR
		If @vOperacion = 'INS' Begin
			
			--Validar Informacion
			Begin
			
				--Numero
				If Exists(Select * From Movimiento Where NroComprobante=@NroComprobante And IDTipoComprobante=@vIDTipoComprobante) Begin
					set @vMensaje = 'El sistema detecto que el numero de comprobante ya esta registrado! Obtenga un numero siguiente.'
					set @vProcesado = 'False'
					GoTo Salir
				End
				
			End
			
			----Insertar la transaccion
			EXEC SpTransaccion
				@IDUsuario = @IDUsuario,
				@IDSucursal = @IDSucursal,
				@IDDeposito = @IDDeposito,
				@IDTerminal = @IDTerminal,
				@IDOperacion = @IDOperacion,
				@MostrarTransaccion = 'False',
				@Mensaje = @vMensaje OUTPUT,
				@Procesado = @vProcesado OUTPUT,
				@IDTransaccion = @vIDTransaccion OUTPUT
				
			
			If @vProcesado = 'False' Begin
				GoTo Salir
			End
			
			--Numero
			declare @vNumero int
			Set @vNumero = (Select IsNull(MAX(M.Numero)+1,1) From Movimiento M Join Transaccion T On M.IDTransaccion=T.ID Where T.IDSucursal=@vIDSucursal)
						
			--Insertar en Movimiento
			Insert Into Movimiento(IDTransaccion, Numero, Fecha, IDTipoOperacion, IDMotivo, IDTipoComprobante, NroComprobante, IDDepositoSalida, IDDepositoEntrada, Observacion, Autorizacion,  Total, TotalImpuesto, TotalDiscriminado, Anulado)
			Values(@vIDTransaccion, @vNumero, @Fecha, @vIDTipoOperacion, @vIDMotivo, @vIDTipoComprobante, @NroComprobante, @vIDDepositoSalida, @vIDDepositoEntrada, @Observacion, 'ADM',  0, 0, 0, 'False')
			
			--Insertar en CompraImportacion
			Insert Into MovimientoImportado(IDTransaccion, NroOperacion, TipoComprobante)
			Values(@vIDTransaccion, @NroComprobante, @TipoComprobante)
			
			Set @vMensaje = 'Registro guardado!'
			Set @vProcesado = 'True'
			GoTo Detalle
					
		End
		
	End
	
	
Detalle:
	
	--DETALLE
	Begin
	
		--Hayar Valores
		--Variables
		Begin
			
			Declare @vIDImpuesto int
			Declare @vIDProducto int
			Declare @vID int
			Declare @vFactorImpuesto decimal(10,7)
			Declare @vFactorDiscriminado decimal(10,7)
			
		End
		
		--Hayar valores
		Begin
		
			--Producto
			Set @vIDProducto = IsNull((Select Top(1) ID From Producto Where Referencia=@CodigoProducto), 0)
			
			--Impuesto
			If @IVA=10 Begin
				Set @vIDImpuesto = 1
			End
			
			If @IVA=5 Begin
				Set @vIDImpuesto = 2
			End
			
			If @IVA=0 Begin
				Set @vIDImpuesto = 3
			End
			
			Set @vFactorDiscriminado = IsNull((Select Top(1) FactorDiscriminado From  Impuesto Where ID=@vIDImpuesto),0)
			Set @vFactorImpuesto = IsNull((Select Top(1) FactorImpuesto From  Impuesto Where ID=@vIDImpuesto),0)
							
		End
				
		--Validar
		Begin
		
			--Producto
			If @vIDProducto=0 Begin
				Set @vMensaje = 'No se encontro el producto!'
				Set @vProcesado = 'False'
				GoTo Salir
			End
			
			--Deposito
			If @vIDDepositoEntrada = 0 Or @vIDDepositoSalida=0 Begin
				Set @vMensaje = 'Deposito incorrecto!'
				Set @vProcesado = 'False'
				GoTo Salir
			End
			
		End
		
		--Insertar si no existe
		If Not Exists(Select * from DetalleMovimiento Where IDTransaccion=@vIDTransaccion And IDProducto=@vIDProducto) Begin
			
			Set @vID = IsNull((Select MAX(ID) + 1 From DetalleMovimiento Where IDTransaccion=@vIDTransaccion),0)
			
			Insert Into DetalleMovimiento(IDTransaccion, IDProducto, ID, IDDepositoEntrada, IDDepositoSalida, Observacion, Cantidad, PrecioUnitario, Total, IDImpuesto, TotalImpuesto, TotalDiscriminado, TotalDescuento, Caja, CantidadCaja)
			Values(@vIDTransaccion, @vIDProducto, @vID, @vIDDepositoEntrada, @vIDDepositoSalida, @Observacion, @Cantidad, @CostoUnitario, 0, @vIDImpuesto, 0, 0, 0, 'False', 0)			
			
		End Else Begin
			Set @vID = (Select Top(1) ID from DetalleMovimiento Where IDTransaccion=@vIDTransaccion And IDProducto=@vIDProducto)
		End

		--Actualizar los totales del Detalle
		Update DetalleMovimiento Set Total=@Cantidad*@CostoUnitario,
								TotalImpuesto=(@Cantidad*@CostoUnitario) / @vFactorImpuesto,
								TotalDiscriminado=(@Cantidad*@CostoUnitario)-((@Cantidad*@CostoUnitario) / @vFactorImpuesto)
									
								
		Where IDTransaccion=@vIDTransaccion And IDProducto=@vIDProducto And ID=@vID
		
		--Actualizar totales de Compras
		Update Movimiento Set	Total=(Select SUM(DV.Total) From DetalleMovimiento DV Where DV.IDTransaccion=@vIDTransaccion),
								TotalDiscriminado=(Select SUM(DV.TotalDiscriminado) From DetalleMovimiento DV Where DV.IDTransaccion=@vIDTransaccion),
								TotalImpuesto=(Select SUM(DV.TotalImpuesto) From DetalleMovimiento DV Where DV.IDTransaccion=@vIDTransaccion)
							
		Where IDTransaccion=@vIDTransaccion
		
	End
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


﻿CREATE Procedure [dbo].[SpViewSaldoBancarioDiario]
	--Entrada
@IDCuentaBancaria tinyint,
@Fecha Date,
@Todos bit = 'false'
As

Begin

	--Declarar variables 
	declare @vID int
	declare @vBanco varchar(50)
	declare @vCuenta varchar(50)
	declare @vConciliado bit
	declare @vDebito money
	declare @vCredito money
	declare @vMovimiento varchar (50)
	declare @vSaldoContable money
	declare @vDepositoConfirmar money
	declare @vChequesNoPagados money
	declare @vSaldoConciliado money
	declare @vUltimoDebito date
	declare @vUltimoCredito date
	declare @vCuentaAnterior varchar(50) = ''
	declare @Moneda varchar(4)

	set @vSaldoContable = 0
	set @vDepositoConfirmar = 0
	set @vChequesNoPagados = 0
	set @vSaldoConciliado = 0
	Declare @CursorIDCuentaBancaria as int 


	Begin

	--Crear la tabla temporal
    create table #TablaTemporal(ID int,
								Banco varchar(50),
								Cuenta varchar(50),
								Moneda varchar(4),
								Debito money,
								Credito money,
								Movimiento varchar(50),
								UltimoDebito date,
								UltimoCredito date,
								SaldoContable money,
								DepositoConfirmar money,
								ChequesNoPagados money,
								SaldoConciliado money,
								SaldoFecha Money)

	Set @vID = (Select IsNull(MAX(ID)+1,1) From #TablaTemporal )

	IF @Todos = 'False' Begin	


		Declare db_cursor cursor for

		Select 
		Banco,
		Cuenta,
		Conciliado,
		Moneda,
		SUM(Debito),
		SUM(Credito),
		Movimiento,
		'UltimoDebito'=MIN(UltimoDebito),
		'UltimoCredito'=MIN(UltimoCredito) 
		From VSaldoBancoDiario
		where IDCuentaBancaria = @IDCuentaBancaria
		and cast(Fecha as date) <= @Fecha
		Group By Banco, Cuenta, Conciliado, Movimiento,Moneda
		Order By Cuenta

		Open db_cursor   
		Fetch Next From db_cursor Into  @vBanco,@vCuenta,@vConciliado,@Moneda,@vDebito,@vCredito,@vMovimiento,@vUltimoDebito,@vUltimoCredito   
		While @@FETCH_STATUS = 0 Begin  


			If @vCuentaAnterior != @vCuenta Begin
				Set @vChequesNoPagados = 0
				Set @vSaldoConciliado = 0
				Set @vSaldoContable = 0
				Set @vDepositoConfirmar = 0
				Set @vCuentaAnterior = @vCuenta
			End


			--Calcular Saldo Contable
			Set @vSaldoContable = @vSaldoContable + (@vDebito - @vCredito)

			--If @vSaldoContable < 0 Begin
				--Set @vSaldoContable = @vSaldoContable *-1
			--End

			--Calular Saldo Conciliado 
			If @vConciliado= 'True' Begin
				Set @vSaldoConciliado = @vSaldoConciliado + (@vDebito - @vCredito)
			End		

			--Calcular Ordenes de Pago
			If @vMovimiento='OP' And @vConciliado = 'False' Begin
				Set @vChequesNoPagados = @vChequesNoPagados + (@vDebito - @vCredito) 
			End


			----Calcular Deposito a Confirmar
			If @vMovimiento <> 'OP' And @vMovimiento <> 'DEP' and @vMovimiento <> 'CCR' And @vConciliado = 'False' Begin
				Set @vDepositoConfirmar = @vDepositoConfirmar + (@vDebito - @vCredito) 
			End

			--BUscar saldo de la fecha de filtro
			declare @vSaldoFecha money 
			set @vSaldoFecha = ISnull((select max(Saldo) from CuentaBancariaSaldoDiario where IDCuentaBancaria = @IDCuentaBancaria  and Fecha = @Fecha),0)

			--Actualizar si ya existe
			If Exists(Select * From #TablaTemporal Where Cuenta = @vCuenta)Begin

				Update #TablaTemporal Set SaldoContable = @vSaldoContable,
										  DepositoConfirmar = @vDepositoConfirmar,
										  ChequesNoPagados = @vChequesNoPagados,
										  SaldoConciliado = @vSaldoConciliado,
										  UltimoDebito=@vUltimoDebito,
										  UltimoCredito=@vUltimoCredito,
										  SaldoFecha = @vSaldoFecha
										  Where Cuenta = @vCuenta 	 

			End

			--Insertar si aún no existe		
			If Not exists(Select * From #TablaTemporal Where Cuenta = @vCuenta)Begin		
				Insert Into #TablaTemporal(ID,Banco,Cuenta,Debito,Credito,Moneda,Movimiento,UltimoDebito,UltimoCredito,SaldoContable,DepositoConfirmar,ChequesNoPagados,SaldoConciliado, SaldoFecha)  
				Values(@vID,@vBanco,@vCuenta,@vDebito,@vCredito,@Moneda,@vMovimiento,@vUltimoDebito,@vUltimoCredito,@vSaldoContable,@vDepositoConfirmar,@vChequesNoPagados,@vSaldoConciliado, @vSaldoFecha)
			End	

			Fetch Next From db_cursor Into @vBanco,@vCuenta,@vConciliado,@Moneda,@vDebito,@vCredito,@vMovimiento,@vUltimoDebito,@vUltimoCredito   

		End

		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			

		End	

	Else if @Todos = 'True' begin

	   Declare db_cursor cursor for

		Select 
		Banco,
		Cuenta,
		Conciliado,
		Moneda,
		SUM(Debito),
		SUM(Credito),
		Movimiento,
		'UltimoDebito'=MIN(UltimoDebito),
		'UltimoCredito'=MIN(UltimoCredito),
		IDCuentaBancaria
		From VSaldoBancoDiario
		where cast(Fecha as date) <= @Fecha
		Group By Banco, Cuenta, Conciliado, Movimiento,Moneda, IDCuentaBancaria
		Order By Cuenta

		Open db_cursor   
		Fetch Next From db_cursor Into  @vBanco,@vCuenta,@vConciliado,@Moneda,@vDebito,@vCredito,@vMovimiento,@vUltimoDebito,@vUltimoCredito, @CursorIDCuentaBancaria
		While @@FETCH_STATUS = 0 Begin  


			If @vCuentaAnterior != @vCuenta Begin
				Set @vChequesNoPagados = 0
				Set @vSaldoConciliado = 0
				Set @vSaldoContable = 0
				Set @vDepositoConfirmar = 0
				Set @vCuentaAnterior = @vCuenta


			End


			--Calcular Saldo Contable
			Set @vSaldoContable = @vSaldoContable + (@vDebito - @vCredito)

			--If @vSaldoContable < 0 Begin
			--	Set @vSaldoContable = @vSaldoContable *-1
			--End

			--Calular Saldo Conciliado 
			If @vConciliado= 'True' Begin
				Set @vSaldoConciliado = @vSaldoConciliado + (@vDebito - @vCredito)
			End		

			--Calcular Ordenes de Pago
			If @vMovimiento='OP' And @vConciliado = 'False' Begin
				Set @vChequesNoPagados = @vChequesNoPagados + (@vDebito - @vCredito) 
			End


			----Calcular Deposito a Confirmar
			If @vMovimiento <> 'OP' And @vMovimiento <> 'DEP' and @vMovimiento <> 'CCR' And @vConciliado = 'False' Begin
				Set @vDepositoConfirmar = @vDepositoConfirmar + (@vDebito - @vCredito) 
			End

			--BUscar saldo de la fecha de filtro
			--declare @vSaldoFecha money 
			set @vSaldoFecha = ISnull((select max(Saldo) from CuentaBancariaSaldoDiario where IDCuentaBancaria = @CursorIDCuentaBancaria  and Fecha = @Fecha),0)

			--Actualizar si ya existe
			If Exists(Select * From #TablaTemporal Where Cuenta = @vCuenta)Begin

				Update #TablaTemporal Set SaldoContable = @vSaldoContable,
										  DepositoConfirmar = @vDepositoConfirmar,
										  ChequesNoPagados = @vChequesNoPagados,
										  SaldoConciliado = @vSaldoConciliado,
										  UltimoDebito=@vUltimoDebito,
										  UltimoCredito=@vUltimoCredito,
										  SaldoFecha = @vSaldoFecha
										  Where Cuenta = @vCuenta 	 

			End

			--Insertar si aún no existe		
			If Not exists(Select * From #TablaTemporal Where Cuenta = @vCuenta)Begin		
				Insert Into #TablaTemporal(ID,Banco,Cuenta,Debito,Credito,Moneda,Movimiento,UltimoDebito,UltimoCredito,SaldoContable,DepositoConfirmar,ChequesNoPagados,SaldoConciliado, SaldoFecha)  
				Values(@vID,@vBanco,@vCuenta,@vDebito,@vCredito,@Moneda,@vMovimiento,@vUltimoDebito,@vUltimoCredito,@vSaldoContable,@vDepositoConfirmar,@vChequesNoPagados,@vSaldoConciliado, @vSaldoFecha)
			End	

			Fetch Next From db_cursor Into @vBanco,@vCuenta,@vConciliado,@Moneda,@vDebito,@vCredito,@vMovimiento,@vUltimoDebito,@vUltimoCredito, @CursorIDCuentaBancaria

		End

		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			

		End	
	End 
	--Fin todos False

	Select * From #TablaTemporal Where ID=@vID  Order by Banco


End



﻿CREATE Procedure [dbo].[SpVencimientoChequeDescontado]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDTransaccionCheque numeric(18,0) = NULL,
	@Numero int = Null,
	@Fecha date = NULL,
	@Observacion varchar(100) = NULL,
			
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
	--INICIO SM 28072021 - Auditoria Informática
	Declare @vObservacion varchar(100)='' 
	Declare @vvID int 
	Declare @Anulado bit 
	Declare @FechaAnulado datetime 
	Declare @IDUsuarioAnulado int 
	Declare @Conciliado bit
	
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		Set @Numero =ISNULL((Select max(Numero+1) From VencimientoChequeDescontado Where IDSucursal=@IDSucursalOperacion),1)
							
		--Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--INICIO SM 28072021 - Auditoria Informática
		set @vObservacion = concat('Inserta un Vecimiento Cheque Descontado. IDTransaccion: ',@IDTransaccionSalida)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'INSERTAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccionSalida,'VENCIMIENTO CHEQUE DESCONTADO' )
		
		set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccionSalida)
		
		Insert Into aVencimientoChequeDescontado (IdAuditoria,IDTransaccion, IDSucursal,IDTRansaccionCheque,Numero, Fecha, Observacion, Anulado, FechaAnulado, IDUsuarioAnulado, Conciliado,IDUsuario,Accion)
		Values(@vvID,@IDTransaccionSalida, @IDSucursalOperacion, @IDTransaccionCheque, @Numero, @Fecha, @vObservacion, 'False', Null, Null, 'False',@IDUsuario,'INS' )
		--FIN SM 28072021 - Auditoria Informática

		--Insertar
		Insert Into VencimientoChequeDescontado (IDTransaccion, IDSucursal,IDTRansaccionCheque,Numero, Fecha, Observacion, Anulado, FechaAnulado, IDUsuarioAnulado, Conciliado)
		Values(@IDTransaccionSalida, @IDSucursalOperacion, @IDTransaccionCheque, @Numero, @Fecha, @Observacion, 'False', Null, Null, 'False' )
		
		--INICIO SM 28072021 - Auditoria Informática
		set @vObservacion = concat('Actualiza Vecimiento Cheque Descontado. Descontado = False. IDTransaccion: ',@IDTransaccionSalida)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccionSalida,'VENCIMIENTO CHEQUE DESCONTADO' )
		
		--Actualizar el Cheque	
		Update ChequeCliente set Descontado = 'False'									
		Where IDTransaccion=@IDTransaccionCheque
							
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'DEL' Begin
		--28/07/2021 SM Para resguardar datos de transaccion a eliminar
		set @IDUsuario  = (select IDUsuario from Transaccion where ID=@IDTransaccion)
		set @IDTerminal  = (select IDTerminal from Transaccion where ID=@IDTransaccion)
		set @IDOperacion  = (select IDOperacion from Transaccion where ID=@IDTransaccion)

		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From VencimientoChequeDescontado Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--INICIO SM 28072021 - Auditoria Informática
		set @vObservacion = concat('Actualiza Vecimiento Cheque Descontado. Descontado = True. IDTransaccion: ',@IDTransaccionCheque)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccionCheque,'VENCIMIENTO CHEQUE DESCONTADO' )
		
		--Actualizar el Cheque
		Update ChequeCliente set Descontado = 'True'									
		Where IDTransaccion=@IDTransaccionCheque
		
		--INICIO SM 28072021 - Auditoria Informática
		set @vObservacion = concat('Elimina un Vecimiento Cheque Descontado. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'VENCIMIENTO CHEQUE DESCONTADO' )
		
		set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion)

		set @IDSucursalOperacion = (select IDSucursal from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)
		set @IDTransaccionCheque = (select IDTransaccionCheque from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)
		set @Numero = (select Numero from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)
		set @Observacion = (select Numero from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)
		set @Anulado = (select Anulado from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)
		set @FechaAnulado = (select Anulado from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)
		set @IDUsuarioAnulado = (select Anulado from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)
		set @Conciliado = (select Conciliado from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)

		Insert Into aVencimientoChequeDescontado (IdAuditoria,IDTransaccion, IDSucursal,IDTRansaccionCheque,Numero, Fecha, Observacion, Anulado, FechaAnulado, IDUsuarioAnulado, Conciliado,IDUsuario,Accion)
		Values(@vvID,@IDTransaccion, @IDSucursalOperacion, @IDTransaccionCheque, @Numero, GETDATE(), @vObservacion, @Anulado, @FechaAnulado, @IDUsuarioAnulado, @Conciliado, @IDUsuario,'INS' )
		--FIN SM 28072021 - Auditoria Informática
					
		--Eliminamos el registro
		Delete VencimientoChequeDescontado Where IDTransaccion = @IDTransaccion
			
		--INICIO SM 28072021 - Auditoria Informática
		set @vObservacion = concat('Se Eliminara el Detalle de Asiento: IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'DETALLE ASIENTO' )
		
		set @vObservacion = concat('Se Eliminara el Asiento. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'ASIENTO' )
		
		set @vObservacion = concat('Se Eliminara la Transaccion. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'TRANSACCION' )
		--FIN SM 28072021 - Auditoria Informática	
					
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion				
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
		--28/07/2021 SM Para resguardar datos de transaccion a eliminar
		set @IDUsuario  = (select IDUsuario from Transaccion where ID=@IDTransaccion)
		set @IDTerminal  = (select IDTerminal from Transaccion where ID=@IDTransaccion)
		set @IDOperacion  = (select IDOperacion from Transaccion where ID=@IDTransaccion)
		
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From VencimientoChequeDescontado Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--IDTransaccion
		If Exists(Select * From VencimientoChequeDescontado Where IDTransaccion=@IDTransaccion And Anulado = 'True') Begin
			set @Mensaje = 'El registro solicitado ya se encuentra anulado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--INICIO SM 28072021 - Auditoria Informática
		set @vObservacion = concat('Actualiza Vecimiento Cheque Descontado. Descontado = True. IDTransaccion: ',@IDTransaccionCheque)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccionCheque,'VENCIMIENTO CHEQUE DESCONTADO' )
			
		--Actualizar el Cheque
		Update ChequeCliente set Descontado = 'True'									
		Where IDTransaccion=@IDTransaccionCheque
		
		--INICIO SM 28072021 - Auditoria Informática
		set @vObservacion = concat('Anula un Vecimiento Cheque Descontado. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ANULAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'VENCIMIENTO CHEQUE DESCONTADO' )
		
		set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion)

		set @IDSucursalOperacion = (select IDSucursal from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)
		set @IDTransaccionCheque = (select IDTransaccionCheque from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)
		set @Numero = (select Numero from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)
		set @Observacion = (select Numero from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)
		set @Anulado = (select Anulado from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)
		set @FechaAnulado = (select Anulado from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)
		set @IDUsuarioAnulado = (select Anulado from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)
		set @Conciliado = (select Conciliado from VencimientoChequeDescontado where IDTransaccion = @IDTransaccion)

		Insert Into aVencimientoChequeDescontado (IdAuditoria,IDTransaccion, IDSucursal,IDTRansaccionCheque,Numero, Fecha, Observacion, Anulado, FechaAnulado, IDUsuarioAnulado, Conciliado,IDUsuario,Accion)
		Values(@vvID,@IDTransaccion, @IDSucursalOperacion, @IDTransaccionCheque, @Numero, GETDATE(), @vObservacion, @Anulado, getdate(), @IDUsuarioAnulado, @Conciliado, @IDUsuario,'ANU' )
		--FIN SM 28072021 - Auditoria Informática
		
		----Anular
		Update VencimientoChequeDescontado   Set Anulado='True', IDUsuarioAnulado=@IDUsuario, FechaAnulado = GETDATE()
		Where IDTransaccion = @IDTransaccion
				
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal, @IDTerminal=@IDTerminal		
		
		--INICIO SM 28072021 - Auditoria Informática
		set @vObservacion = concat('Se Eliminara el Detalle de Asiento: IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'DETALLE ASIENTO' )
		
		set @vObservacion = concat('Se Eliminara el Asiento. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'ASIENTO' )
		--FIN SM 28072021 - Auditoria Informática
						
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		Set @IDTransaccionSalida = 0
		return @@rowcount
			
	End
	
	
		
End





﻿CREATE Procedure [dbo].[SpCambiarContraseña]

	--Entrada
	@IDUsuario smallint,
	@IDPerfil smallint,
	@IDSucursal tinyint,
	@IDTerminal smallint,
	
	@ContraseñaActual varchar(50),
	@ContraseñaNueva varchar(50),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output

As

Begin

	Declare  @vPasswordActual varchar(300)
	
	Set @vPasswordActual = Convert(varchar(300), (Select [Password] From Usuario Where ID=@IDUsuario))
	Set @Mensaje = '---'
	Set @Procesado = 'False'
		
	--validar
	If @ContraseñaNueva = '' Begin
		Set @Mensaje = 'La contraseña no es segura!'
		Set @Procesado = 'False'
		Return @@rowcount	
	End
	
	Set @ContraseñaActual = Convert(varchar(300), HASHBYTES('Sha1', @ContraseñaActual))
	
	If @vPasswordActual != @ContraseñaActual Begin
		Set @Mensaje = 'La contraseña anterior no es correcta!'
		Set @Procesado = 'False'
		Return @@rowcount	
	End	
		
	Set @ContraseñaNueva = Convert(varchar(300), HASHBYTES('Sha1', @ContraseñaNueva))
	
	Update Usuario Set [Password]=@ContraseñaNueva
	Where ID=@IDUsuario

	Set @Mensaje = 'Registro guardado!'
	Set @Procesado = 'True'	
		
End


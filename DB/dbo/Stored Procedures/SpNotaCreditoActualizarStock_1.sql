﻿CREATE Procedure [dbo].[SpNotaCreditoActualizarStock]

	--CARLITOS 10-03-2014
	
	--Entrada
	@IDTransaccion numeric(18),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	--Variable
	Declare @vIDDeposito tinyint
	Declare @vIDProducto int
	Declare @vCantidad decimal(10,2)
	
	--Variables Kardex
	Declare @vFecha datetime
	Declare @vIDSucursal integer
	Declare @vNroComprobante varchar(50)
	Declare @vCosto decimal(10,2)
	Declare @vTotalDiscriminado money
	Declare @vID int

	Set @vFecha = (Select Fecha From NotaCredito Where IDTransaccion = @IDTransaccion)
	Set @vIDSucursal = (Select IDSucursal From Deposito Where ID = @vIDDeposito)
	Set @vNroComprobante = (Select NroComprobante From NotaCredito Where IDTransaccion = @IDTransaccion)

	Set @Mensaje = ''
	Set @Procesado = 'False'
	
	If @Operacion = 'INS' Begin
				
		Declare db_cursor cursor for
		Select DNC.IDProducto, NC.IDDeposito, Sum(DNC.Cantidad), Sum(DNC.TotalDiscriminado)
		From DetalleNotaCredito DNC 
		Join NotaCredito NC on DNC.IDTransaccion = NC.IDTransaccion
		Join Producto P On DNC.IDProducto=P.ID 
		Where DNC.IDTransaccion=@IDTransaccion And P.ControlarExistencia='True'			
		group by DNC.IDProducto, NC.IDDeposito
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vCantidad, @vTotalDiscriminado 
		While @@FETCH_STATUS = 0 Begin  

			--Actualizar Stock
			Exec SpActualizarProductoDeposito @IDDeposito=@vIDDeposito, @IDProducto = @vIDProducto, @Cantidad= @vCantidad, @Signo='+', @Mensaje= @Mensaje, @Procesado = @Procesado

			--Actualiza el Kardex
			Set @vCosto = IsNull((Select CostoPromedio From Producto Where ID = @vIDProducto),0)			
			Set @vCantidad = @vCantidad*-1
			EXEC SpKardex
				@Fecha = @vFecha,
				@IDTransaccion = @IDTransaccion,
				@IDProducto = @vIDProducto,
				@ID = @vID,
				@Orden = 15,
				@CantidadEntrada = 0,
				@CantidadSalida = @vCantidad,
				@CostoEntrada = 0,
				@CostoSalida = @vCosto,
				@IDSucursal = @vIDSucursal,
				@Comprobante = @vNroComprobante	

			Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vCantidad, @vTotalDiscriminado
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
		Set @Mensaje = 'Registro procesado'
		Set @Procesado = 'True'
	
	End	
		
	If @Operacion = 'ANULAR' Begin
			
		Declare db_cursor cursor for
				Select DNC.IDProducto, DNC.ID, NC.IDDeposito, DNC.Cantidad 
				From DetalleNotaCredito DNC 
				Join NotaCredito NC on DNC.IDTransaccion = NC.IDTransaccion
				Join Producto P On DNC.IDProducto=P.ID 
		Where DNC.IDTransaccion=@IDTransaccion And P.ControlarExistencia='True'			
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDProducto, @vID, @vIDDeposito, @vCantidad 
		While @@FETCH_STATUS = 0 Begin  

			--Actualizar Stock
			Exec SpActualizarProductoDeposito @IDDeposito=@vIDDeposito, @IDProducto = @vIDProducto, @Cantidad= @vCantidad, @Signo='-',@ControlStock  = 'False', @Mensaje= @Mensaje, @Procesado = @Procesado

			--Actualiza el Kardex
			--Tomo el costopromedio del producto en ese momento y no de la factura involucrada en la ncr porque en el Kardex quedaria fuera de lugar
			Set @vCosto = IsNull((Select CostoPromedio From Producto Where ID = @vIDProducto),0)			
			--EXEC SpKardex
			--	@Fecha = @vFecha,
			--	@IDTransaccion = @IDTransaccion,
			--	@IDProducto = @vIDProducto,
			--	@ID = @vID,
			--	@Orden = 1,
			--	@CantidadEntrada = 0,
			--	@CantidadSalida = @vCantidad,
			--	@CostoEntrada = 0,
			--	@CostoSalida = @vCosto,
			--	@IDSucursal = @vIDSucursal,
			--	@Comprobante = @vNroComprobante	
			Delete from Kardex Where IDTransaccion = @IDTransaccion
		--	Exec SPRecalcularKardexProducto @vIDProducto, @vFecha

			Fetch Next From db_cursor Into @vIDProducto, @vID, @vIDDeposito, @vCantidad
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
	End
	
	If @Operacion = 'ELIMINAR' Begin
	
		--Solo si no esta anulado
		If (Select Anulado From NotaCredito Where IDTransaccion=@IDTransaccion) = 'True' Begin
			Set @Mensaje = 'Registro guardado'
			Set @Procesado = 'True'
			return @@rowcount
		End
		
		Declare db_cursor cursor for
		Select DNC.IDProducto, NC.IDDeposito, DNC.Cantidad 
		From DetalleNotaCredito DNC 
		Join NotaCredito NC on DNC.IDTransaccion = NC.IDTransaccion
		Join Producto P On DNC.IDProducto=P.ID Where DNC.IDTransaccion=@IDTransaccion And P.ControlarExistencia='True'			
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vCantidad 
		While @@FETCH_STATUS = 0 Begin  
		
			--Actualizar Stock
			Exec SpActualizarProductoDeposito @IDDeposito=@vIDDeposito, @IDProducto = @vIDProducto, @Cantidad= @vCantidad, @Signo='-',@ControlStock  = 'False', @Mensaje= @Mensaje, @Procesado = @Procesado
			
			Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vCantidad
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
	End
	
End
	





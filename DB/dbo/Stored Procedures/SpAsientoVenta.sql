﻿CREATE Procedure [dbo].[SpAsientoVenta]

	@IDTransaccion numeric(18,0)
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint = 0

	--Venta
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vContado bit
	Declare @vCredito bit
	Declare @vCondicion varchar(50)
	Declare @vIDFormaPagoFactura int
	Declare @vFechaEmision date
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money
	Declare @vCuentaContableVenta varchar(50)
	Declare @vCuentaContableCosto varchar(50)
	Declare @vTotalDiscriminado money
	Declare @vTotalDescuentoDiscriminado money
	Declare @vTotalCosto money
	
	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	
	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money

	Declare @vCliente as varchar(32)
	Declare @vIDCliente as int
	
	--Obtener valores
	Begin
	
		Set @vContado='False'
		
		Select	@vIDSucursal=IDSucursal,
				@vTipoComprobante=TipoComprobante,
				@vIDTipoComprobante=IDTipoComprobante,
				@vComprobante=Comprobante,
				@vCredito=Credito,
				@vCondicion=@vCondicion,
				@vFechaEmision=FechaEmision,
				@vIDMoneda=IDMoneda,
				@vCotizacion=Cotizacion,
				@vCondicion=Condicion,
				@vIDFormaPagoFactura=IDFormaPagoFactura,
				@vCliente = Cliente,
				@vIDCliente = IDCliente
		From VVenta Where IDTransaccion=@IDTransaccion
	
		If @vCredito=0 Begin
			Set @vContado='True'
		End

		if (select CancelarAutomatico from FormaPagoFactura where ID = @vIDFormaPagoFactura) = 'True' Begin
			Execute SpAsientoVentaCancelarAutomatico  @IDTransaccion
			Goto salir
		end
		
	End
					
	--Verificar que el asiento se pueda modificar
	Begin
	
		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta procesado
		If (Select Procesado From Venta Where IDTransaccion=@IDTransaccion) = 'False' Begin
			print 'La venta no es valida'
			GoTo salir
		End 	
		
		--Si esta anulado
		If (Select Anulado From Venta Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'La venta esta anulada'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		
	End
				
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = @vTipoComprobante + ' ' + @vComprobante + ' ' + @vCondicion + ' - ' + @vCliente
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFechaEmision, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	
	--Cuentas Fijas de Clientes
	Begin
	
		--Variables
		Declare @vVentaCredito bit
		Declare @vVentaContado bit
		Declare @vCodigoCuentaCliente varchar(50)

		Declare cCFVentaCliente cursor for
		Select Codigo, Debe, Haber, Credito, Contado From VCFVentaCliente Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFVentaCliente   
		fetch next from cCFVentaCliente into @vCodigo, @vDebe, @vHaber, @vVentaCredito, @vVentaContado
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			If @vIDSucursal <> 5 Begin -- Si no es MErcury
				Set @vCodigoCuentaCliente = dbo.FCuentaContableCliente(@IDTransaccion, @vIDSucursal)
			End

			IF Exists(Select * from CLiente where ID = @vIDCliente and CodigoCuentaContable <> '') Begin
				Set @vCodigoCuentaCliente = (Select CodigoCuentaContable from Cliente where id = @vIDCliente)
			End

			if not exists(select * from cuentacontable where imputable = 1 and Codigo = @vCodigoCuentaCliente) begin
				Set @vCodigoCuentaCliente = ''
			end

			If @vCodigoCuentaCliente != '' Begin
				Set @vCodigo = @vCodigoCuentaCliente
			End

			--Obtener la cuenta
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Set @vImporte = (Select SUM(Total) From DetalleImpuesto Where IDTransaccion=@IDTransaccion)
			Set @vImporte = (Select round(SUM(Total * @vCotizacion),0) From DetalleImpuesto Where IDTransaccion=@IDTransaccion)

			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				

			If @vCredito = 'True' And @vContado = 'False' Begin
				If @vVentaCredito = 'False' Begin
					Set @vImporteHaber = 0
					Set @vImporteDebe = 0
				End
			End 
			
			If @vCredito = 'False' And @vContado = 'True' Begin
				If @vVentaContado = 'False' Begin
					Set @vImporteHaber = 0
					Set @vImporteDebe = 0
				End
			End
				
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin				
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo) , 0, '')
			End
			
			fetch next from cCFVentaCliente into @vCodigo, @vDebe, @vHaber, @vVentaCredito, @vVentaContado
			
		End
		
		close cCFVentaCliente 
		deallocate cCFVentaCliente
	End
	
	--Cuentas Fijas Descuentos
	Begin
		
		--Variables
		Declare @vIDTipoDescuento tinyint
		Declare @vIDProveedorDescuento int
		
		Declare cCFVentaDescuento cursor for
		Select 
		'CuentaContableVenta'=IsNull(dbo.FCuentaContableDescuento(D.IDProducto, @IDTransaccion),''), 
		'TotalDiscriminado'=round(Sum(D.TotalDescuentoDiscriminado *  @vCotizacion),0) --ver que guarde en la moneda de operacion para multiplicar por la cotizacion
		--From VDescuento D 
		From DetalleVenta D 
		Where D.IDTransaccion=@IDTransaccion 
		--and idtipodescuento <> 3
		and D.TotalDescuentoDiscriminado > 0
		Group By D.IDTransaccion, D.IDProducto
		Open cCFVentaDescuento
		fetch next from cCFVentaDescuento into @vCodigo, @vImporte
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			If @vImporte > 0 And @vIDCuentaContable Is Not Null Begin
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set	Debito=Debito+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, Round(@vImporte,@vRedondeo), 0, '')
				End
			End
			If @vImporte < 0 And @vIDCuentaContable Is Not Null Begin --Si se cobra MAS del precio normal
				Set @vImporte = @vImporte * -1
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set	Credito=Credito+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo,  Round(@vImporte,@vRedondeo), 0, 0, '')
				End
			End
			
			fetch next from cCFVentaDescuento into @vCodigo, @vImporte
			
		End
		
		close cCFVentaDescuento
		deallocate cCFVentaDescuento
	End
					
	--Cuentas Fijas de Venta 
	Begin
	
		--Cuenta Fija Venta
		Set @vIDCuentaContable = NULL
		Set @vCuentaContableVenta = ''
		Set @vTotalDiscriminado = 0
		Set @vTotalDescuentoDiscriminado = 0
		
		Declare cDetalleVenta cursor for
		Select 
		'CuentaContableVenta'=IsNull(dbo.FCuentaContableVenta(D.IDProducto, @vIDSucursal, @vIDFormaPagoFactura),''), 
		'TotalDiscriminado'=round(Sum(D.TotalDiscriminado * @vCotizacion),0), 
		--'TotalDescuentoDiscriminado'=IsNull((Select round(Sum(DE.DescuentoDiscriminado * @vCotizacion),0) From Descuento DE Where DE.IDTransaccion=D.IDTransaccion and DE.IDTipoDescuento<>3 And DE.IDProducto=D.IDProducto And DE.ID=D.ID),0) * D.Cantidad
		'TotalDescuentoDiscriminado' = IsNull(Sum(D.TotalDescuentoDiscriminado * @vCotizacion),0)
		From VDetalleVenta D 
		Where D.IDTransaccion=@IDTransaccion 
		Group By D.IDTransaccion, D.IDProducto, D.ID, D.Cantidad
		Open cDetalleVenta 
		fetch next from cDetalleVenta into @vCuentaContableVenta, @vTotalDiscriminado, @vTotalDescuentoDiscriminado

		
		While @@FETCH_STATUS = 0 Begin  
			
			set @vImporte = 0
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vCodigo = @vCuentaContableVenta
			
			Print concat('CC Venta ',@vcodigo)
			declare @vDesc varchar(100) = (select descripcion from cuentacontable where codigo = @vcodigo)
			Print concat('CC Venta ',@vdesc)

			--Si no existe, poner el predeterminado
			If @vCuentaContableVenta Is Null or @vCuentaContableVenta = '' Begin
				Set @vCodigo = (Select IsNull((Select Top(1) Codigo From VCFVentaVenta Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda),''))				
			End
			
			--Validar codigo
			If @vCodigo = '' Begin
				fetch next from cDetalleVenta into @vCuentaContableVenta, @vTotalDiscriminado, @vTotalDescuentoDiscriminado
			End
						
			--Hayar el importe	
			Set @vImporte = @vTotalDiscriminado + @vTotalDescuentoDiscriminado
			Set @vDebe = (Select Top(1) Debe From VCFVentaVenta Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda)
			Set @vHaber = (Select Top(1) Haber From VCFVentaVenta Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
																
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			--Cargamod
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin
			
				--Si existe la cuenta, actualizar
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End
			End
				
			fetch next from cDetalleVenta into @vCuentaContableVenta, @vTotalDiscriminado, @vTotalDescuentoDiscriminado
			
		End
		
		close cDetalleVenta 
		deallocate cDetalleVenta
		
	End
			
	--Cuentas Fijas Impuesto
	Begin
		
		--Variables
		Declare @vIDImpuesto tinyint
		
		Declare cCFVentaImpuesto cursor for
		Select Codigo, IDImpuesto, Debe, Haber From VCFVentaImpuesto Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFVentaImpuesto   
		fetch next from cCFVentaImpuesto into @vCodigo, @vIDImpuesto, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			Set @vImporte = (Select round(SUM(TotalImpuesto * @vCotizacion),0) From DetalleImpuesto Where IDTransaccion=@IDTransaccion And IDImpuesto=@vIDImpuesto)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
			End
			
			fetch next from cCFVentaImpuesto into @vCodigo, @vIDImpuesto, @vDebe, @vHaber
			
		End
		
		close cCFVentaImpuesto 
		deallocate cCFVentaImpuesto
	End					

	--Cuentas Existencia de Mercaderia
	Begin
	
		--Variables
		
		Declare cCFVentaMercaderia cursor for
		Select P.CuentaContableCompra,
			   'TotalCosto'=round(Sum(DV.TotalCosto),0) 
			    From VDetalleVenta DV
				Join Producto P on DV.IDProducto = P.ID
				Where DV.IDTransaccion=@IDTransaccion 
				Group By P.CuentaContableCompra
		Open cCFVentaMercaderia   
		fetch next from cCFVentaMercaderia into @vCodigo, @vImporteHaber

		
		While @@FETCH_STATUS = 0 Begin  
			
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), 0, 0, '')
			End
			
			fetch next from cCFVentaMercaderia into @vCodigo, @vImporteHaber
			
		End
		
		close cCFVentaMercaderia 
		deallocate cCFVentaMercaderia
	End
	
	--Cuentas Fijas de Costo de Mercaderia
	Begin
	
		--Cuenta Fija Venta
		Set @vCuentaContableVenta = ''
		Set @vTotalCosto = 0
			
		Declare cDetalleCosto cursor for
		Select 'CuentaContableCosto'=IsNull(dbo.FCuentaContableCosto(IDProducto, @vIDSucursal),''), 
			   --'TotalCosto'=round(Sum(TotalCosto * @vCotizacion),0) 
			   'TotalCosto'=round(Sum(TotalCosto),0) 
			    From VDetalleVenta 
				Where IDTransaccion=@IDTransaccion 
				Group By IDProducto
		Open cDetalleCosto
		fetch next from cDetalleCosto into @vCuentaContableCosto, @vTotalCosto
		
		While @@FETCH_STATUS = 0 Begin  
			
			set @vImporte = 0
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vCodigo = @vCuentaContableCosto
			
			--Si no existe, poner el predeterminado
			If @vCuentaContableCosto = '' Begin
				Set @vCodigo = (Select IsNull((Select Top(1) Codigo From VCFVentaCostoMercaderia Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda),''))								
			End
			
			--Validar codigo
			If @vCodigo = '' Begin
				fetch next from cDetalleCosto into @vCuentaContableCosto, @vTotalCosto
			End
			
			--Hayar el importe	
			Set @vImporte = @vTotalCosto
			Set @vDebe = (Select Top(1) Debe From VCFVentaCostoMercaderia Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda)
			Set @vHaber = (Select Top(1) Haber From VCFVentaCostoMercaderia Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')			
										
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			--Cargamod
			If @vImporteHaber > 0 Or @vImporteDebe > 0 Begin
			
				--Si existe la cuenta, actualizar
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End
			End
							
			fetch next from cDetalleCosto into @vCuentaContableCosto, @vTotalCosto
			
		End
		
		close cDetalleCosto 
		deallocate cDetalleCosto
		
	End

	----Reparar
		Execute SpRedondearAsientoVentaERSA @IDTransaccion=@IDTransaccion
	
	--Actualizamos la cabecera, el total
	Begin
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
		Set @vImporteHaber = ROUND(@vImporteHaber, @vRedondeo)
		Set @vImporteDebe = ROUND(@vImporteDebe, @vRedondeo)
	
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vImporteHaber - @vImporteDebe
		Where IDTransaccion=@IDTransaccion
	End

	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
	
End



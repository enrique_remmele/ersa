﻿CREATE Procedure [dbo].[SpPedidoVerificarPedidoNCPendiente]

	--Entrada
	@IDTransaccion numeric(18,0)
As

Begin

		Declare @vIDTransaccion integer
		Declare @vNumero integer
		Declare @vIDProducto integer
		Declare @vCantidad Decimal
		Declare @vPrecioUnitario money
		Declare @vIDCliente integer
		Declare @vIDListaPrecio integer
		Declare @vPrecioNuevo money
		Declare @vFecha date
		Declare @vContador integer = 0

		Declare cCFVenta cursor for
		Select	p.IDTransaccion,
				p.Numero,
				dp.IDProducto,
				dp.Cantidad,
				dp.PrecioUnitario,
				p.IDCliente,
				p.IDListaPrecio,
				p.Fecha
				from detallepedido dp
				join vpedido p on dp.IDTransaccion = p.IDTransaccion
				where p.idusuario = 103 
				and p.anulado = 0
				and p.IDTransaccion = @IDTransaccion
				--and p.idsucursal = 4
				--and p.Fecha between '20191101' and '20191105'
				and p.IDTransaccion not in (select distinct IDTransaccionPedidoVenta from PedidoNotaCreditoPedidoVenta)
				and p.IDTransaccion not in (select distinct IDTransaccionPedido from PedidoVenta where IDTransaccionVenta in(select distinct IDTransaccionVentaGenerada from PedidoNotaCreditoVenta))
				and p.IDTransaccion not in (select distinct IDTransaccionPedidoVenta from DetalleNotaCreditoDiferenciaPrecio)
		Open cCFVenta   
		fetch next from cCFVenta into @vIDTransaccion,@vNumero,@vIDProducto,@vCantidad,@vPrecioUnitario,@vIDCliente,@vIDListaPrecio,@vFecha

		While @@FETCH_STATUS = 0 Begin  
			Set @vPrecioNuevo = @vPrecioUnitario - (select ISNULL(Descuento,0) 
													from ProductoListaPrecioExcepciones 
													where IDProducto = @vIDProducto 
													and IDListaPrecio = @vIDListaPrecio 
													and IDCliente = @vIDCliente
													and IDTipoDescuento = 3
													and @vFecha between Desde and Hasta
													and (Case When CantidadLimite > 0 then CantidadLimite else 0 end) >= (Case When CantidadLimite > 0 then CantidadLimiteSaldo+@vCantidad else 0 end))
													
		if @vPrecioNuevo <@vPrecioUnitario begin
			select * from vProductoListaPrecioExcepciones 
													where IDProducto = @vIDProducto 
													and IDListaPrecio = @vIDListaPrecio 
													and IDCliente = @vIDCliente
													and IDTipoDescuento = 3
													and convert(date,@vFecha) between Convert(date,Desde) and convert(date,Hasta)
													and (Case When CantidadLimite > 0 then CantidadLimite else 0 end) >= (Case When CantidadLimite > 0 then CantidadLimiteSaldo+@vCantidad else 0 end)
													
			print concat('select * from vpedido where idtransaccion = ',@vIDTransaccion)
			insert into DetalleNotaCreditoDiferenciaPrecio(IDProducto, IDCliente, IDListaPrecio, Cantidad, PrecioOriginal, NuevoPrecio, IDTransaccionPedidoVenta, IDTransaccionPedidoNotaCredito, IDTransaccionVenta, IDTransaccionNotaCredito,AutomaticoPorExcepcion)
			Values (@vIDProducto, @vIDCliente, @vIDListaPrecio, @vCantidad, @vPrecioUnitario, @vPrecioNuevo,@vIDTransaccion,0,0,0,1)
			
		end

				
			fetch next from cCFVenta into @vIDTransaccion,@vNumero,@vIDProducto,@vCantidad,@vPrecioUnitario,@vIDCliente,@vIDListaPrecio,@vFecha

		End

		close cCFVenta 
		deallocate cCFVenta

		Set @vContador = (Select count(*) from DetalleNotaCreditoDiferenciaPrecio where IDTransaccionPedidoVenta =@IDTransaccion and IDTransaccionPedidoNotaCredito = 0)


		Select @vContador


End


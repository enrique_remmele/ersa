﻿ CREATE Procedure [dbo].[SpNotaCreditoProveedorActualizarStock]

	--Entrada
	@IDTransaccion numeric(18),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	--Variable
	Declare @vIDDeposito tinyint
	Declare @vIDProducto int
	Declare @vCantidad decimal(10,2)
	Declare @vID int

	--Variables Kardex
	Declare @vFecha datetime
	Declare @vIDSucursal integer
	Declare @vNroComprobante varchar(50)
	Declare @vCostoEntrada decimal(10,2)

	Set @vFecha = (Select Fecha From NotaCreditoProveedor Where IDTransaccion = @IDTransaccion)
	Set @vNroComprobante = (Select NroComprobante From NotaCreditoProveedor Where IDTransaccion = @IDTransaccion)


	Set @Mensaje = ''
	Set @Procesado = 'False'
	
	If @Operacion = 'INS' Begin
				
		Declare db_cursor cursor for
		Select DNCP.IDProducto, DNCP.ID ,DNCP.IDDeposito, DNCP.Cantidad From DetalleNotaCreditoProveedor DNCP Join Producto P On DNCP.IDProducto=P.ID Where DNCP.IDTransaccion=@IDTransaccion And P.ControlarExistencia='True'			
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDProducto, @vID, @vIDDeposito, @vCantidad 
		While @@FETCH_STATUS = 0 Begin  

			--Actualizar Stock
			Exec SpActualizarProductoDeposito @IDDeposito=@vIDDeposito, @IDProducto = @vIDProducto, @Cantidad= @vCantidad, @Signo='-', @Mensaje= @Mensaje, @Procesado = @Procesado

			----Actualiza el Kardex			
			Set @vCostoEntrada = (Select CostoPromedio From Producto where ID=@vIDProducto) 
			Set @vCantidad = @vCantidad*-1
			Set @vIDSucursal = (Select IDSucursal From Deposito Where ID = @vIDDeposito)

			EXEC SpKardex
			@Fecha = @vFecha,
			@IDTransaccion = @IDTransaccion,
			@IDProducto = @vIDProducto,
			@ID = @vID,
			@Orden = 1,
			@CantidadEntrada = @vCantidad,
			@CantidadSalida = 0,
			@CostoEntrada = @vCostoEntrada,
			@CostoSalida = 0,
			@IDSucursal = @vIDSucursal,
			@Comprobante = @vNroComprobante	

			Fetch Next From db_cursor Into @vIDProducto, @vID, @vIDDeposito, @vCantidad
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
	End	
		
	
	
	If @Operacion = 'DEL' Begin
			
		Declare db_cursor cursor for
		Select DNCP.IDProducto, DNCP.IDDeposito, DNCP.Cantidad From DetalleNotaCreditoProveedor DNCP Join Producto P On DNCP.IDProducto=P.ID Where DNCP.IDTransaccion=@IDTransaccion And P.ControlarExistencia='True'			
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vCantidad 
		While @@FETCH_STATUS = 0 Begin  
		
			--Actualizar Stock
			Exec SpActualizarProductoDeposito @IDDeposito=@vIDDeposito, @IDProducto = @vIDProducto, @Cantidad= @vCantidad, @Signo='+', @Mensaje= @Mensaje output, @Procesado = @Procesado output
			
			Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vCantidad
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor

		Delete from Kardex Where IDTransaccion = @IDTransaccion
		Exec SPRecalcularKardexProducto @vIDProducto, @vFecha
	End
	
End
	



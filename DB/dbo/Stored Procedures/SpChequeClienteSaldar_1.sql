﻿CREATE Procedure [dbo].[SpChequeClienteSaldar]

	--Entrada
	@IDTransaccionCobranza numeric(18,0),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin Tran Transaccion Begin
Begin Try

	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'
	
	Declare @vIDTransaccionCheque numeric(18,0)	
	Declare @vImporteCheque money
	Declare @vTotalCheque money
	Declare @vSaldoCheque money
	Declare @vUsadoCheque money
	Declare @vCancelar bit
	Declare @vCancelarCheque bit
	
	--dbs
	declare @vSaldoActualCheque money
	
	--Saldar cheque
	If @Operacion = 'INS' Begin
		
		Declare db_cursor cursor for
		Select IDTransaccionCheque, ImporteCheque, CancelarCheque 
		From FormaPago Where IDTransaccion=@IDTransaccionCobranza And Cheque='True'
		
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionCheque, @vImporteCheque, @vCancelarCheque
		While @@FETCH_STATUS = 0 Begin  
			
			----Calcular
			--Set @vTotalCheque = (Select ISNULL((Select Top(1) Importe From ChequeCliente Where IDTransaccion=@vIDTransaccionCheque), 0))
			----Set @vSaldoCheque = (Select ISNULL((Select Top(1) Saldo From ChequeCliente Where IDTransaccion=@vIDTransaccionCheque), 0))
			
			----Importe usado en Cobranzas Credito
			--Set @vUsadoCheque = IsNull((Select SUM(FP.ImporteCheque) From FormaPago FP Join CobranzaCredito CC On FP.IDTransaccion=CC.IDTransaccion Where FP.IDTransaccionCheque=@vIDTransaccionCheque And CC.Procesado='True' And CC.IDTransaccion<>@IDTransaccionCobranza), 0)
			
			----Importe usado en Cobranzas Contado
			--Set @vUsadoCheque = @vUsadoCheque + IsNull((Select SUM(FP.ImporteCheque) From FormaPago FP Join CobranzaContado CC On FP.IDTransaccion=CC.IDTransaccion Where FP.IDTransaccionCheque=@vIDTransaccionCheque And CC.Procesado='True' And CC.IDTransaccion<>@IDTransaccionCobranza), 0)
			
			--Set @vSaldoCheque = (@vTotalCheque - (@vImporteCheque+@vUsadoCheque))
			
			set @vSaldoActualCheque = (Select ISNULL((Select Saldo From ChequeCliente Where IDTransaccion=@vIDTransaccionCheque), 0))
			set @vSaldoCheque = @vSaldoActualCheque - @vImporteCheque	
			
			Set @vCancelar = 'False'
			
			--Cancelar si no tiene saldo
			If @vSaldoCheque < 1 Begin
				Set @vCancelar = 'True'
			End
			
			If @vCancelarCheque = 'True' Begin
				Set @vCancelar = 'True'
			End
			
			--Actualizar				
			Update ChequeCliente Set Saldo=@vSaldoCheque, Cancelado=@vCancelar
			Where IDTransaccion=@vIDTransaccionCheque
			
			Fetch Next From db_cursor Into @vIDTransaccionCheque, @vImporteCheque, @vCancelarCheque
			
		End
		
		Close db_cursor   
		Deallocate db_cursor		   			
		
		Set @Mensaje = 'Registro procesado!'
		Set @Procesado = 'True'
		
		Commit Tran Transaccion
		Return @@RowCount
		
	End
	
	--Reestablecer
	If @Operacion = 'DEL' Begin
		
		Declare db_cursor cursor for
		Select IDTransaccionCheque, ImporteCheque From FormaPago Where IDTransaccion=@IDTransaccionCobranza And Cheque='True'
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionCheque, @vImporteCheque
		While @@FETCH_STATUS = 0 Begin  
			
			--Calcular
			Set @vTotalCheque = (Select ISNULL((Select Importe From ChequeCliente Where IDTransaccion=@vIDTransaccionCheque), 0))
			--Set @vSaldoCheque = (Select ISNULL((Select Top(1) Saldo From ChequeCliente Where IDTransaccion=@vIDTransaccionCheque), 0))
			
			--Importe usado en Cobranzas Credito
			Set @vUsadoCheque = IsNull((Select SUM(FP.ImporteCheque) From FormaPago FP Join CobranzaCredito CC On FP.IDTransaccion=CC.IDTransaccion Where FP.IDTransaccionCheque=@vIDTransaccionCheque And CC.Procesado='True' And CC.IDTransaccion<>@IDTransaccionCobranza), 0)
			
			--Importe usado en Cobranzas Contado
			Set @vUsadoCheque = @vUsadoCheque + IsNull((Select SUM(FP.ImporteCheque) From FormaPago FP Join CobranzaContado CC On FP.IDTransaccion=CC.IDTransaccion Where FP.IDTransaccionCheque=@vIDTransaccionCheque And CC.Procesado='True' And CC.IDTransaccion<>@IDTransaccionCobranza), 0)
			
			Set @vSaldoCheque = (@vTotalCheque - @vUsadoCheque)
			Set @vSaldoCheque = @vSaldoCheque + @vImporteCheque
			
			Set @vCancelar = 'True'
						
			--Actualizar				
			Update ChequeCliente Set Saldo=@vSaldoCheque, Cancelado=@vCancelar
			Where IDTransaccion=@vIDTransaccionCheque
			
			Fetch Next From db_cursor Into @vIDTransaccionCheque, @vImporteCheque
			
		End
		
		Close db_cursor   
		Deallocate db_cursor		   			
		
		Set @Mensaje = 'Registro procesado!'
		Set @Procesado = 'True'
		
		Commit Tran Transaccion
		Return @@RowCount
		
	End
	
	--Reestablecer
	If @Operacion = 'ANULAR' Begin
		
		Declare db_cursor cursor for
		Select IDTransaccionCheque, ImporteCheque From FormaPago Where IDTransaccion=@IDTransaccionCobranza And Cheque='True'
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionCheque, @vImporteCheque
		While @@FETCH_STATUS = 0 Begin 			
			
			set @vSaldoActualCheque = (Select ISNULL((Select Saldo From ChequeCliente Where IDTransaccion=@vIDTransaccionCheque), 0))
			set @vSaldoCheque = @vSaldoActualCheque + @vImporteCheque	
			Set @vCancelar = 'False'
						
			--Actualizar				
			Update ChequeCliente Set Saldo=@vSaldoCheque, Cancelado=@vCancelar
			Where IDTransaccion=@vIDTransaccionCheque
			
			Fetch Next From db_cursor Into @vIDTransaccionCheque, @vImporteCheque
			
		End
		
		Close db_cursor   
		Deallocate db_cursor		   			
		
		Set @Mensaje = 'Registro procesado!'
		Set @Procesado = 'True'
		
		Commit Tran Transaccion
		Return @@RowCount
		
	End
	
	--Si no paso nada	
	RollBack Tran Transaccion
	Set @Procesado = 'False'
	
End Try
Begin Catch
	set @Mensaje = ERROR_MESSAGE()
	Set @Procesado = 'False'
	RollBack Tran Transaccion
	RAISERROR(@Mensaje, 16, 0)		 
End Catch

End


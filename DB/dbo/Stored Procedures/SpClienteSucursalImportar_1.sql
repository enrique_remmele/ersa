﻿CREATE Procedure [dbo].[SpClienteSucursalImportar]

	--Entrada
	@Cliente varchar(100),
	@PuntoVenta varchar(100),
	@Direccion varchar(200),
	@Contacto varchar(50),
	@Telefono varchar(50),
	@Ciudad varchar(50),
	@Vendedor varchar(50),
	@Sucursal varchar(50),
	
	@Actualizar bit = 'True'
	
As

Begin

	--Variables
	Declare @vID int
	Declare @vIDCliente int
	Declare @vIDCiudad int
	Declare @vIDVendedor int
	Declare @vIDSucursal int
	Declare @vOperacion varchar(10) 
	Declare @Mensaje varchar(200)
	Declare @Procesado bit

	Set @Mensaje = 'No se procesó!'
	Set @Procesado = 'False'

	--Hayar Valores
	--Clientes
	Set @vIDCliente = IsNull((Select Top(1) ID From Cliente Where Referencia=@Cliente), 0)
	
	--Verificamos si ya existe
	If @vIDCliente = 0 Begin
		Set @Mensaje = 'No se encontro el cliente'
		Set @Procesado = 'False'
		GoTo Salir
	End		

	Select @vIDSucursal = IDSucursal,
		@vIDVendedor = IDvendedor,
		@vIDCiudad = IDCiudad
	From Cliente Where ID=@vIDCliente

	--ACTUALIZAR
	If Exists(Select * From ClienteSucursal Where IDCliente=@vIDCliente And Sucursal=@PuntoVenta) Begin
		
		Set @vID = (Select Top(1) ID From ClienteSucursal Where IDCliente=@vIDCliente And Sucursal=@PuntoVenta)
			
		Update ClienteSucursal Set Sucursal=@PuntoVenta, 
									Direccion=@Direccion, 
									Contacto=@Contacto, 
									Telefono=@Telefono, 
									IDCiudad=@vIDCiudad, 
									IDVendedor=@vIDVendedor, 
									IDSucursal=@vIDSucursal
		Where IDCliente=@vIDCliente And ID=@vID
		
		Set @Mensaje = 'Registro actualizado!'
		Set @Procesado = 'True'
		GoTo Salir
			
	End
	
	--INSERTAR
	If Not Exists(Select * From ClienteSucursal Where IDCliente=@vIDCliente And Sucursal=@PuntoVenta) Begin
		
		Set @vID = IsNull((Select Max(ID)+1 From ClienteSucursal Where IDCliente=@vIDCliente),1)
		
		Insert Into ClienteSucursal(IDCliente, ID, Sucursal, Direccion, Contacto, Telefono, Estado, IDPais, IDDepartamento, IDCiudad, IDBarrio, IDVendedor, IDZonaVenta, Latitud, Longitud, IDSucursal)
		Values(@vIDCliente, @vID, @PuntoVenta, @Direccion, @Contacto, @Telefono, 'True', 1, NULL, @vIDCiudad, NULL, @vIDVendedor, NULL, NULL, NULL, @vIDSucursal)	

		Set @Mensaje = 'Registro insertado!'
		Set @Procesado = 'True'
		GoTo Salir

	End

Salir:
	Select 'Mensaje'=@Mensaje, 'Procesado'=@Procesado		
End



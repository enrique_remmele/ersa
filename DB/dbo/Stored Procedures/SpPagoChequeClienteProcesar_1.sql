﻿


CREATE Procedure [dbo].[SpPagoChequeClienteProcesar]

	--Entrada
	@IDTransaccion numeric(18),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

    ---Variables para CALCULAR SALDO DEPOSITADO
	declare @SaldoaCuenta money
	declare @Depositado money
	declare @Cancelado bit

	Set @Cancelado  = 'False'
	
	--Declarar variables Variables
	declare @vIDTransaccionCheque numeric (18,0)
	declare @vImporte money
					
	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'

	If @Operacion = 'INS' Begin
		
		
		--Procesamos los Cheques si es que hay
		EXEC SpChequeClienteSaldar @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		
		--Procesar los Cheques
		Begin
		
			Declare db_cursor1 cursor for
			Select D.IDTransaccionChequeCliente, D.Importe
			From DetallePagoChequeCliente D 
			Join ChequeCliente C On D.IDTransaccionChequeCliente=C.IDTransaccion 
			Where IDTransaccionPagoChequeCliente=@IDTransaccion
			
			Open db_cursor1   
			Fetch Next From db_cursor1 Into @vIDTransaccionCheque,@vImporte
			While @@FETCH_STATUS = 0 Begin  
			
			-----CALCULAR SALDO DEPOSITADO
				Set @SaldoaCuenta  = (select SaldoACuenta  from ChequeCliente where IDTransaccion=@vIDTransaccionCheque)  
				
				--
				If @SaldoaCuenta  > @vImporte Begin
					Set @SaldoaCuenta  = @SaldoaCuenta  - @vImporte
				End Else Begin
					set @SaldoaCuenta  = @vImporte - @SaldoaCuenta  
				End 				
								
				--Verificar si se cancela el Efectivo
				If @SaldoaCuenta  = 0 Begin
					Set @Cancelado = 'True'  
				End				
							
				----ACTUALIZAR LA TABLA CHEQUE			
				Update ChequeCliente Set Cancelado= @Cancelado, 
										 SaldoACuenta = @SaldoaCuenta 
				where IDTransaccion= @vIDTransaccionCheque 
				
				Fetch Next From db_cursor1 Into @vIDTransaccionCheque,@vImporte
				
				
			End
			
			Update DescuentoCheque Set Procesado = 'True' 
			Where IDTransaccion=@IDTransaccion

			Exec SpAsientoCanjeCheque @IDTransaccion=@IDTransaccion

			--Cierra el cursor
			Close db_cursor1   
			Deallocate db_cursor1		   			
		End	
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
			
	End	
	
	If @Operacion = 'DEL' Begin	 
		
		--VALIDAR
		--Que ningun cheque este rechazado
		If Exists(Select * From DetalleDescuentoCheque  D join ChequeCliente C on D.IDTransaccionDescuentoCheque  = @IDTransaccion where D.IDTransaccionDescuentoCheque =@IDTransaccion And  C.Rechazado='True') Begin 
			set @Mensaje = 'El Cheque Esta Rechazado!'
			set @Procesado = 'True'
			return @@rowcount		
		end
		
		--Que ningun cheque este conciliado
		If Exists(Select * From DetalleDescuentoCheque  D join ChequeCliente C on D.IDTransaccionDescuentoCheque  = @IDTransaccion where D.IDTransaccionDescuentoCheque =@IDTransaccion And  C.Conciliado='True') Begin 
			set @Mensaje = 'El Cheque Esata Conciliado!'
			set @Procesado = 'True'
			return @@rowcount
		end
				
		--Procesamos los Cheques si es que hay
		EXEC SpChequeClienteSaldar @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Procesar los Cheques
		Begin
			Declare db_cursor cursor for
			Select D.IDTransaccionChequeCliente, D.Importe 
			From DetallePagoChequeCliente D 
			Join ChequeCliente C On D.IDTransaccionChequeCliente=C.IDTransaccion 
			Where IDTransaccionPagoChequeCliente=@IDTransaccion
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionCheque,@vImporte
			While @@FETCH_STATUS = 0 Begin
			
				-----CALCULAR SALDO DEPOSITADO
				Set @SaldoaCuenta  = (select SaldoACuenta  from ChequeCliente where IDTransaccion=@vIDTransaccionCheque)  
				
				--
				If @SaldoaCuenta  > @vImporte Begin
					Set @SaldoaCuenta  = @SaldoaCuenta  + @vImporte
				End Else Begin
					set @SaldoaCuenta  = @vImporte + @SaldoaCuenta  
				End 				
								
				--Descancelar directo
				Set @Cancelado = 'False'  
												
				----ACTUALIZAR LA TABLA CHEQUE
			     Update ChequeCliente Set SaldoACuenta = @SaldoaCuenta ,
										  Cancelado=@Cancelado    
				where IDTransaccion= @vIDTransaccionCheque 
				
				Fetch Next From db_cursor Into @vIDTransaccionCheque,@vImporte
								
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		End	
			
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
	
	
	
	End
	
	If @Operacion = 'ANULAR' Begin	 
		
		--VALIDAR
		--Que ningun cheque este rechazado
		If Exists(Select * From DetalleDescuentoCheque  D join ChequeCliente C on D.IDTransaccionDescuentoCheque  = @IDTransaccion where D.IDTransaccionDescuentoCheque =@IDTransaccion And  C.Rechazado='True') Begin 
			set @Mensaje = 'El Cheque Esta Rechazado!'
			set @Procesado = 'True'
			return @@rowcount		
		end
		
		--Que ningun cheque este conciliado
		If Exists(Select * From DetalleDescuentoCheque  D join ChequeCliente C on D.IDTransaccionDescuentoCheque  = @IDTransaccion where D.IDTransaccionDescuentoCheque =@IDTransaccion And  C.Conciliado='True') Begin 
			set @Mensaje = 'El Cheque Esata Conciliado!'
			set @Procesado = 'True'
			return @@rowcount
		end
				
		
		--Procesar los Cheques
		Begin
			Declare db_cursor cursor for
			Select D.IDTransaccionChequeCliente, D.Importe 
			From DetallePagoChequeCliente D 
			Join ChequeCliente C On D.IDTransaccionChequeCliente=C.IDTransaccion 
			Where IDTransaccionPagoChequeCliente=@IDTransaccion
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionCheque,@vImporte
			While @@FETCH_STATUS = 0 Begin
			
				-----CALCULAR SALDO DEPOSITADO
				Set @SaldoaCuenta  = (select SaldoACuenta  from ChequeCliente where IDTransaccion=@vIDTransaccionCheque)  
				
				--
				If @SaldoaCuenta  > @vImporte Begin
					Set @SaldoaCuenta  = @SaldoaCuenta  + @vImporte
				End Else Begin
					set @SaldoaCuenta  = @vImporte + @SaldoaCuenta  
				End 				
								
				--Descancelar directo
				Set @Cancelado = 'False'  
												
				----ACTUALIZAR LA TABLA CHEQUE
			     Update ChequeCliente Set SaldoACuenta = @SaldoaCuenta ,
										  Cancelado=@Cancelado    
				where IDTransaccion= @vIDTransaccionCheque 
				
				Fetch Next From db_cursor Into @vIDTransaccionCheque,@vImporte
								
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		End	
			
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
	
	
	
	End
	
End
	








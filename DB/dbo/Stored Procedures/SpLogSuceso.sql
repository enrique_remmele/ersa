﻿CREATE Procedure [dbo].[SpLogSuceso]

	--Entrada
	@Operacion varchar(50),
	@Tabla varchar(50),
	@IDUsuario smallint,
	
	--No requerido
	@IDTransaccion numeric(18,0) = NULL,
	@Comprobante varchar(50) = NULL,
	@IDTerminal int = NULL
		
As

Begin

	--Variables
	Declare @vIDTipoOperacionLog tinyint
	Declare @vIDTabla smallint
	Declare @vID numeric(18,0)
	Declare @vUsuario varchar(50)
	
	--IDTipoOperacion
	Begin
		
		If Not Exists(Select * From TipoOperacionLog Where Codigo=@Operacion) Begin
		
			Set @vIDTipoOperacionLog = IsNull((Select MAX(ID) + 1 From TipoOperacionLog),1)
			 
			Insert Into TipoOperacionLog(ID, Descripcion, Codigo)
			Values(@vIDTipoOperacionLog, @Operacion, @Operacion)
		End Else Begin
			Set @vIDTipoOperacionLog = (Select ID From TipoOperacionLog Where Codigo=@Operacion)
		End
		
	End
	
	--Tabla
	Begin
		
		If Not Exists(Select * From TablaLog Where Descripcion=@Tabla) Begin
		
			Set @vIDTabla = IsNull((Select MAX(ID) + 1 From TablaLog),1)
			 
			Insert Into TablaLog(ID, Descripcion)
			Values(@vIDTabla, @Tabla)
			
		End Else Begin
			
			Set @vIDTabla = (Select ID From TablaLog Where Descripcion=@Tabla)
			
		End
		
	End
	
	--Uuario
	Set @vUsuario = (Select Usuario From Usuario Where ID=@IDUsuario)
	Set @vID = IsNull((Select MAX(ID) + 1 From LogSucesos),1)
	
	Insert Into LogSucesos(ID, IDTipoOperacionLog, Fecha, IDTabla, Tabla, IDTransaccion, Comprobante, IDTerminal, IDUsuario, Usuario)
	Values(@vID, @vIDTipoOperacionLog, GETDATE(), @vIDTabla, @Tabla, @IDTransaccion, @Comprobante, @IDTerminal, @IDUsuario, @vUsuario)
	
	Return @@rowcount

End


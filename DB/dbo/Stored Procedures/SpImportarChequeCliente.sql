﻿CREATE Procedure [dbo].[SpImportarChequeCliente]

	--Entrada
	@Sucursal varchar(50),
	@NroOperacion varchar(50),

	--Cliente
	@Cliente varchar(50),
	@RazonSocial varchar(100),
	@RUC varchar(50),

	@Banco varchar(50),
	@Fecha date,
	@NumeroCheque varchar(50),
	@Importe money,
	@Saldo money,
	@Tipo varchar(50),
	@FechaVencimiento date,
	@ChequeTercero bit,
	@Librador varchar(50),
		
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	
	@Actualizar bit
	
As

Begin
		
	--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		
		Declare @vNumero int
		Declare @vIDCliente int
		Declare @vIDSucursal int
		Declare @vIDBanco int
		Declare @vDiferido bit
		
		Declare @vOperacion varchar(50)
		Declare @vIDTransaccion int
		
		
	End
	
	--Establecer valores predefinidos
	Begin
		
		Set @vOperacion = 'INS'
		
		--Sucursal
		Set @vIDSucursal=(IsNull((Select Top(1) ID From Sucursal Where Codigo=@Sucursal),0))
		If @vIDSucursal = 0 Begin
			--Si no se encuentra, poner asuncion
			Set @vIDSucursal=(IsNull((Select Top(1) ID From Sucursal Where Codigo='ASU'),0))

			--Set @vMensaje = 'No existe la sucursal!'
			--Set @vProcesado = 'False'
			--GoTo Salir
		End
		
		--Cliente
		Set @vIDCliente=(IsNull((Select Top(1) ID From Cliente Where Referencia=@Cliente),0))
		If @vIDCliente = 0 Begin

			If @RUC = '' Begin
				Set @vMensaje = 'Cliente incorrecto!'
				Set @vProcesado = 'False'
				GoTo Salir
			End

			--Buscamos por RUC
			Set @vIDCliente=(IsNull((Select Top(1) ID From Cliente Where RUC=@RUC),0))
			If @vIDCliente = 0 Begin
				Set @vMensaje = 'Cliente incorrecto!'
				Set @vProcesado = 'False'
				GoTo Salir
			End

		End
		
		--Banco
		Set @vIDBanco=(IsNull((Select Top(1) ID From Banco Where Referencia=@Banco),0))
		If @vIDBanco = 0 Begin
			Set @vIDBanco = (Select ISNULL(Max(ID) + 1, 0) From Banco)
			Insert Into Banco(ID, Descripcion, Referencia, Estado)
			Values(@vIDBanco, @Banco, @Banco, 'True')
		End		
		
		--Diferido
		If @Tipo='DIF' Begin
			Set @vDiferido = 'True'
		End
		
		If @Tipo='CHQ' Begin
			Set @vDiferido = 'False'
		End
		
	End
	
	--Verificar si ya existe
	If Exists(Select * From ChequeClienteImportar Where NroOperacion=@NroOperacion And NroCheque=@NumeroCheque And IDBanco=@vIDBanco And IDCliente=@vIDCliente) Begin
		set @vIDTransaccion = (Select IDTransaccion From ChequeClienteImportar Where NroOperacion=@NroOperacion And NroCheque=@NumeroCheque And IDBanco=@vIDBanco And IDCliente=@vIDCliente)
		Set @vOperacion = 'UPD'
	End
	
	--ACTUALIZAR
	If @vOperacion = 'UPD' Begin
	
		If @Actualizar = 'True' Begin
			
			If (Select Cancelado From ChequeCliente Where IDTransaccion=@vIDTransaccion) = 'True' Begin
				Set @vMensaje = 'El cheque ya esta cancelado!'
				Set @vProcesado = 'False'
				GoTo Salir
			End
			
			If (Select Cartera From ChequeCliente Where IDTransaccion=@vIDTransaccion) = 'False' Begin
				Set @vMensaje = 'El cheque ya esta usado!'
				Set @vProcesado = 'False'
				GoTo Salir
			End
			
			--Pedido
			Update ChequeCliente Set IDCliente=@vIDCliente,
										IDBanco=@vIDBanco,
										Fecha=@Fecha,
										NroCheque=@NumeroCheque,
										Importe=@Importe,
										Saldo = @Saldo,
										Diferido=@vDiferido,
										FechaVencimiento=@FechaVencimiento,
										ChequeTercero=@ChequeTercero
			Where IDTransaccion = @vIDTransaccion
			
			Set @vMensaje = 'Actualizado!!!'
			Set @vProcesado = 'True'
			GoTo Salir
			
		End
		
		Set @vMensaje = 'Sin Act.!'
		Set @vProcesado = 'True'
		GoTo Salir
		
	End
	
	--INSERTAR
	If @vOperacion = 'INS' Begin
		
		--Validar Informacion
		Begin
		
			--Numero
			If Exists(Select * From ChequeClienteImportar Where NroCheque=@NumeroCheque And IDBanco=@vIDBanco And IDCliente=@vIDCliente) Begin
				set @vMensaje = 'El sistema detecto que el numero de comprobante ya esta registrado! Obtenga un numero siguiente.'
				set @vProcesado = 'False'
				GoTo Salir
			End			
			
		End
		
		----Insertar la transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@MostrarTransaccion = 'False',
			@Mensaje = @vMensaje OUTPUT,
			@Procesado = @vProcesado OUTPUT,
			@IDTransaccion = @vIDTransaccion OUTPUT
		
		If @vProcesado = 'False' Begin
			GoTo Salir
		End
		
		--Obtener Pedido
		Set @vNumero = ISNULL((Select MAX(Numero)+1 From ChequeCliente Where IDSucursal=@vIDSucursal),'1')
				
		--Insertar en Pedido
		Insert Into ChequeCliente(IDTransaccion, IDSucursal, Numero, IDCliente, IDBanco, IDCuentaBancaria, Fecha, NroCheque, IDMoneda, Cotizacion, ImporteMoneda, Importe, Diferido, FechaVencimiento, ChequeTercero, Librador, Saldo, Cancelado, Cartera, Depositado, Rechazado, Conciliado, IDMotivoRechazo, SaldoACuenta)
		Values(@vIDTransaccion, @vIDSucursal, @vNumero, @vIDCliente, @vIDBanco, NULL, @Fecha, @NumeroCheque, 1, 1, @Importe, @Importe, @vDiferido, @FechaVencimiento, @ChequeTercero, @Librador, @Saldo, 'False', 'True', 'False', 'False', 'False', NULL, @Importe)
		
		--Inserttar en Cheque Importado
		Insert Into ChequeClienteImportar(IDTransaccion, NroOperacion, NroCheque, IDBanco, IDCliente)
		Values(@vIDTransaccion, @NroOperacion, @NumeroCheque, @vIDBanco, @vIDCliente)

		Set @vMensaje = 'Registro guardado!'
		Set @vProcesado = 'True'
		GoTo Salir
				
	End
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End

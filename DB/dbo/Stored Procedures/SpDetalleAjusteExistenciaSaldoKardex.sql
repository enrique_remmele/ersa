﻿CREATE Procedure [dbo].[SpDetalleAjusteExistenciaSaldoKardex]

	--Entrada
	@IDTransaccion numeric(18,0),
	@IDProducto int=NULL,
	@ExistenciaValorizada decimal(10,2) =NULL,
	@ExistenciaKardexAnterior decimal(10,2) =NULL,
	@CantidadAjuste decimal(10,2) =NULL,
	@Operacion varchar(20),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin

	--Validar
	--Transaccion
	If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--Transaccion
	If Not Exists(Select * From AjusteExistenciaSaldoKardex Where IDTransaccion=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End

	if @CantidadAjuste = 0 begin
		Set @Procesado = 'True'
		return @@rowcount	
	end

	--Variables Kardex
	Declare @vFecha datetime
	Declare @vIDSucursal integer
	Declare @vNroComprobante varchar(50)
	Declare @vCosto decimal(10,2)
	Declare @vTotalDiscriminado decimal(10,2)
	Declare @vFechaHasta datetime

	Set @vFecha = (Select Fecha From AjusteExistenciaSaldoKardex Where IDTransaccion = @IDTransaccion)
	Set @vIDSucursal = 1
	Set @vNroComprobante = (Select Numero From AjusteExistenciaSaldoKardex Where IDTransaccion = @IDTransaccion)
	
	--BLOQUES
	if @Operacion='INS' Begin
		
		Set @vCosto = dbo.FCostoProductoFechaKardex(@IDProducto, @vFecha)
		Set @CantidadAjuste = @CantidadAjuste *-1 -- Esto es necesario porque la sp que genera la lista de diferencias hace kardex - Operativo y debe ser Operativo - Kardex. Esto no se soluciona en esa sp porque ya es usada en reportes de existencia valorizada 
				--Insertar el detalle
		Insert Into DetalleAjusteExistenciaSaldoKardex(IDTransaccion, IDProducto, ExistenciaValorizada, ExistenciaKardexAnterior, CantidadAjuste, Costo)
												Values(@IDTransaccion, @IDProducto, @ExistenciaValorizada, @ExistenciaKardexAnterior, @CantidadAjuste, @vCosto)
		
		if @CantidadAjuste > 0 begin
			EXEC SpKardex
				@Fecha = @vFecha,
				@IDTransaccion = @IDTransaccion,
				@IDProducto = @IDProducto,
				@ID = 1,
				@Orden = 1,
				@CantidadEntrada = @CantidadAjuste,
				@CantidadSalida = 0,
				@CostoEntrada = @vCosto,
				@CostoSalida = 0,
				@IDSucursal = @vIDSucursal,
				@Comprobante = @vNroComprobante	

				Exec SpRecalcularKardex @IDProducto, @vFecha, 'PRODUCTO'
		end
		else begin
			if @CantidadAjuste < 0 begin
			
				Set @CantidadAjuste = @CantidadAjuste * -1

				EXEC SpKardex
					@Fecha = @vFecha,
					@IDTransaccion = @IDTransaccion,
					@IDProducto = @IDProducto,
					@ID = 1,
					@Orden = 1,
					@CantidadEntrada = 0,
					@CantidadSalida = @CantidadAjuste,
					@CostoEntrada = 0,
					@CostoSalida = @vCosto,
					@IDSucursal = @vIDSucursal,
					@Comprobante = @vNroComprobante	

					Exec SpRecalcularKardex @IDProducto, @vFecha, 'PRODUCTO'
			end
		end
			
		Set @Procesado = 'True'
		return @@rowcount					
			
	End
	
	Salir:

	
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	return @@rowcount
	
End

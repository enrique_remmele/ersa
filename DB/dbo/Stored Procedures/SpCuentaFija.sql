﻿CREATE Procedure [dbo].[SpCuentaFija]

	--Entrada
	@ID tinyint = NULL,
	@Orden tinyint = NULL,
	@IDOperacion tinyint = NULL,
	@IDTipoOperacion tinyint = NULL,
	@IDTipoCuentaFija tinyint = NULL,
	@Debe bit = NULL,
	@Haber bit = NULL,
	@IDCuentaContable smallint = NULL,
	@BuscarEnProducto bit = NULL,
	@BuscarEnClienteProveedor bit = NULL,
	@EsVenta bit = NULL,
	@Contado bit = NULL,
	@Credito bit = NULL,
	@Operacion varchar(10) = NULL,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
As

Begin

	--INSERTAR
	If @Operacion='INS' Begin
	
		--Validar
		--El mismo tipo no debe existir
		--If Exists(Select * From CuentasFijas Where IDOperacion=@IDOperacion And IDTipoCuentaFija=@IDTipoCuentaFija) Begin
		--	set @Mensaje = 'El tipo ya esta cargado en esta configuracion!'
		--	set @Procesado = 'True'
		--	return @@rowcount		
		--End
		
		--Insertar
		Declare @vID tinyint
		Set @vID = (Select ISNULL((Max(ID)+1), 1) From CuentasFijas)
		
		Insert Into CuentasFijas(ID, Orden, IDOperacion, IDTipoOperacion, IDTipoCuentaFija, Debe, Haber, IDCuentaContable, BuscarEnProducto, BuscarEnClienteProveedor, EsVenta, Contado, Credito)
		Values(@vID, @Orden, @IDOperacion, @IDTipoOperacion, @IDTipoCuentaFija, @Debe, @Haber, @IDCuentaContable, @BuscarEnProducto, @BuscarEnClienteProveedor, @EsVenta, @Contado, @Credito)
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
	End	

	--ACTUALIZAR
	If @Operacion='UPD' Begin
	
		--Validar
		If Not Exists(Select * From CuentasFijas Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro!'
			set @Procesado = 'True'
			return @@rowcount		
		End
		
		--El mismo tipo no debe existir
		--If Exists(Select * From CuentasFijas Where ID!=@ID And IDOperacion=@IDOperacion And IDTipoCuentaFija=@IDTipoCuentaFija) Begin
		--	set @Mensaje = 'El tipo ya esta cargado en esta configuracion!'
		--	set @Procesado = 'True'
		--	return @@rowcount		
		--End
		
		--Actualizar
		Update CuentasFijas Set Orden=@Orden,
								IDOperacion=@IDOperacion,
								IDTipoOperacion=@IDTipoOperacion,
								IDTipoCuentaFija=@IDTipoCuentaFija,
								Debe=@Debe,
								Haber=@Haber,
								IDCuentaContable=@IDCuentaContable,
								BuscarEnProducto=@BuscarEnProducto,
								BuscarEnClienteProveedor=@BuscarEnClienteProveedor,
								EsVenta=@EsVenta,
								Contado=@Contado,
								Credito=@Credito								
		Where ID=@ID 
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	--ELIMINAR
	If @Operacion='DEL' Begin
	
		--Validar
		If Not Exists(Select * From CuentasFijas Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro!'
			set @Procesado = 'True'
			return @@rowcount		
		End
		
		Delete From CuentasFijas Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
	End	
	
End


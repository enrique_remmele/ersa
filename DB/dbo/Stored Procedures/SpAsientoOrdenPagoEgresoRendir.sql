﻿CREATE Procedure [dbo].[SpAsientoOrdenPagoEgresoRendir]

	@IDTransaccion int
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint = 0

	--
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vFecha date
	Declare @vIDMoneda tinyint
	Declare @vIDMonedaComprobante tinyint
	Declare @vCotizacion money
	Declare @vCuentaContableCheque varchar(50)
	Declare @vObservacion varchar(100)
	Declare @vDiferido bit
	Declare @vPagoProveedor bit
	Declare @vAnticipoProveedor bit
	Declare @vEgresoRendir bit
	Declare @vIDCuentaBancaria int
	Declare @vCheque bit
	Declare @vImporteMoneda money
	Declare @vImporteMonedaDocumento money
	
	--Variables
	Declare @vCodigoCuentaProveedor varchar(50)
	Declare @vTotal money
	Declare @vTotalDocumento money
	Declare @vDiferenciaCambio money
	Declare @vIDTipoComprobantePago int
	Declare @vAplicarRetencion bit

	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	
	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money

	Declare @vProveedor as varchar(32)
	Declare @vIDProveedor as int
	
	--Obtener valores
	Begin
	
		Select	@vIDSucursal=OP.IDSucursal,
				@vTipoComprobante=OP.TipoComprobante,
				@vIDTipoComprobante=OP.IDTipoComprobante,
				@vComprobante=OP.Comprobante,
				@vFecha=OP.Fecha,
				@vIDMoneda = OP.IDMoneda,
				@vIDMonedaComprobante = OP.IDMonedaComprobante,
				@vCotizacion = OP.Cotizacion,
				@vProveedor = OP.Proveedor,
				@vIDProveedor = OP.IDProveedor,
				@vObservacion = OP.Observacion,
				@vDiferido = OP.Diferido,
				@vPagoProveedor = OP.PagoProveedor,
				@vAnticipoProveedor = OP.AnticipoProveedor,
				@vEgresoRendir = OP.EgresoRendir,
				@vIDCuentaBancaria = OP.IDCuentaBancaria,
				@vTotal = (Case when IDMoneda <> 1 then OP.ImporteMoneda * OP.Cotizacion else OP.ImporteMoneda end),
				@vImporteMoneda = OP.ImporteMoneda,
				@vTotalDocumento = (Case when IDMoneda <> 1 then OP.ImporteMonedaDocumento * OP.Cotizacion else OP.ImporteMonedaDocumento end),
				@vImporteMonedaDocumento = OP.ImporteMonedaDocumento,
				@vDiferenciaCambio = 0,--isnull(OP.DiferenciaCambio,0),
				@vCheque = OP.Cheque,
				@vAplicarRetencion = OP.AplicarRetencion
				From vOrdenPago OP
		Where IDTransaccion=@IDTransaccion
		
	End
	--Verificar que el asiento se pueda modificar
	Begin
	
		If not exists(Select * From OrdenPago Where IDTransaccion=@IDTransaccion) Begin
			print 'La operacion no es una orden de pago'
			GoTo salir
		End 	
		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		
		if @vAnticipoProveedor = 'True' begin
			Goto salir
		end

		if @vPagoProveedor = 'True' begin
			Goto salir
		end

	End
				
	--Eliminar primero el asiento
	Begin
		print 'elimina asiento'
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = Concat(@vTipoComprobante,' ',@vComprobante)
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	--Cuentas Fijas proveedor cheque diferido a vencer/banco
		
		print 'Proveedor'
		Declare @vBuscarEnCuentaBancaria bit = 'False'

		Declare cCFCompraProveedor cursor for
		Select Codigo, BuscarEncuentaBancaria from VCFOrdenPago 
		Where Tipo = 'PROVEEDOR' 
		And IDSucursal=@vIDSucursal 
		And IDMoneda=@vIDMoneda
		And SoloDiferido = @vDiferido
		Open cCFCompraProveedor   
		fetch next from cCFCompraProveedor into @vCodigo, @vBuscarEnCuentaBancaria
		
		While @@FETCH_STATUS = 0 Begin  
			print 'entro a Proveedor'
			if (@vCodigo = '') begin
				Goto Seguir
			end
			if @vBuscarEnCuentaBancaria = 'True' begin
				Set @vCodigo = (select CodigoCuentaContable from CuentaBancaria where id = @vIDCuentaBancaria)
				print 'cont'
			end

			Set @vImporteDebe = 0
			Set @vImporteHaber = 0

			--Obtener la cuenta
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			Set @vTotal = @vTotal
						
			Set @vImporteHaber = @vTotal
			Set @vImporteDebe = 0
			--End
							
			If @vImporteDebe>0 or @vImporteHaber > 0 Begin				
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vTotal,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End
			End
Seguir:
			fetch next from cCFCompraProveedor into @vCodigo,@vBuscarEnCuentaBancaria
			
		End
		
		close cCFCompraProveedor 
		deallocate cCFCompraProveedor


		Declare cCFProveedor cursor for
		Select 
		DA.Codigo,
		'Total'=(select dbo.FImporteProporcional(DA.Credito,((OPE.Importe * OPE.Cotizacion)),(OPE.Total * OPE.Cotizacion)))
		from vdetalleasientoAgrupado DA
		join VOrdenPagoEgreso OPE on DA.IDTransaccion = OPE.IDTransaccion
		Where OPE.IDTransaccionOrdenPago=@IDTransaccion
		and Da.Credito > 1
		and Da.Descripcion like 'Fondo Fijo%'

		Open cCFProveedor   
		fetch next from cCFProveedor into @vCodigoCuentaProveedor, @vTotal
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vCodigo = @vCodigoCuentaProveedor
			if (@vCodigo = '') begin
				Goto Seguir1
			end

			Set @vImporteDebe = 0
			Set @vImporteHaber = 0

			--Obtener la cuenta
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			Set @vTotal = @vTotal
			
			Set @vImporteDebe = @vTotal
			Set @vImporteHaber = 0
			
				
			If @vImporteDebe>0 Begin				
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End
			End
Seguir1:
			fetch next from cCFProveedor into @vCodigoCuentaProveedor, @vTotal
			
		End
		
		close cCFProveedor 
		deallocate cCFProveedor
	

						
	--Actualizamos la cabecera, el total
	Begin
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
		Set @vImporteHaber = ROUND(@vImporteHaber, @vRedondeo)
		Set @vImporteDebe = ROUND(@vImporteDebe, @vRedondeo)
	
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vImporteHaber - @vImporteDebe
		Where IDTransaccion=@IDTransaccion
	End

	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
End

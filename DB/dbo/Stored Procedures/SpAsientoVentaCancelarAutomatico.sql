﻿CREATE Procedure [dbo].[SpAsientoVentaCancelarAutomatico]

	@IDTransaccion numeric(18,0)
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	
	--Venta
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vContado bit
	Declare @vCredito bit
	Declare @vCondicion varchar(50)
	Declare @vIDFormaPagoFactura int
	Declare @vFechaEmision date
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money
	Declare @vCuentaContableVenta varchar(50)
	Declare @vCuentaContableCosto varchar(50)
	Declare @vTotalDiscriminado money
	Declare @vTotalDescuentoDiscriminado money
	Declare @vTotalCosto money
	
	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	
	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money
	
	--Obtener valores
	Begin
	
		Set @vContado='False'
		
		Select	@vIDSucursal=IDSucursal,
				@vTipoComprobante=TipoComprobante,
				@vIDTipoComprobante=IDTipoComprobante,
				@vComprobante=Comprobante,
				@vCredito=Credito,
				@vCondicion=@vCondicion,
				@vFechaEmision=FechaEmision,
				@vIDMoneda=IDMoneda,
				@vCotizacion=Cotizacion,
				@vCondicion=Condicion,
				@vIDFormaPagoFactura=IDFormaPagoFactura
		From VVenta Where IDTransaccion=@IDTransaccion
		and CancelarAutomatico = 'True'
	
		If @vCredito=0 Begin
			Set @vContado='True'
		End
		
	End
					
	--Verificar que el asiento se pueda modificar
	Begin
	
		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta procesado
		If (Select Procesado From Venta Where IDTransaccion=@IDTransaccion) = 'False' Begin
			print 'La venta no es valida'
			GoTo salir
		End 	
		
		--Si esta anulado
		If (Select Anulado From Venta Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'La venta esta anulada'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		
	End
				
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = @vTipoComprobante + ' ' + @vComprobante + ' ' + @vCondicion
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFechaEmision, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	
	--Cuentas Fijas de Clientes
	Begin
	
		--Variables
		Declare @vVentaCredito bit
		Declare @vVentaContado bit
		Declare @vCodigoCuentaCliente varchar(50)

		Declare cCFVentaCliente cursor for
		Select Codigo, Debe, Haber, Credito, Contado From VCFVentaCliente Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFVentaCliente   
		fetch next from cCFVentaCliente into @vCodigo, @vDebe, @vHaber, @vVentaCredito, @vVentaContado
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vCodigoCuentaCliente = dbo.FCuentaContableCliente(@IDTransaccion, @vIDSucursal)

			Declare @vProd as integer =(select top(1)IDProducto from Detalleventa where IDTransaccion = @IDTransaccion)
		    set @vCodigoCuentaCliente = dbo.FCuentaContableVentaCancelarAutomatico(@vProd, @vIDSucursal, @vIDFormaPagoFactura)
		
			If @vCodigoCuentaCliente != '' Begin
				Set @vCodigo = @vCodigoCuentaCliente
			End

			--Obtener la cuenta
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			Set @vImporte = (Select round(SUM(Total * @vCotizacion),0) From DetalleImpuesto Where IDTransaccion=@IDTransaccion)

			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				

			If @vCredito = 'True' And @vContado = 'False' Begin
				If @vVentaCredito = 'False' Begin
					Set @vImporteHaber = 0
					Set @vImporteDebe = 0
				End
			End 
			
			If @vCredito = 'False' And @vContado = 'True' Begin
				If @vVentaContado = 'False' Begin
					Set @vImporteHaber = 0
					Set @vImporteDebe = 0
				End
			End
				
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin				
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFVentaCliente into @vCodigo, @vDebe, @vHaber, @vVentaCredito, @vVentaContado
			
		End
		
		close cCFVentaCliente 
		deallocate cCFVentaCliente
	End
	
	--Cuentas Fijas Descuentos
	Begin
		
		--Variables
		Declare @vIDTipoDescuento tinyint
		Declare @vIDProveedorDescuento int
		
		Declare cCFVentaDescuento cursor for
		Select 
		'CuentaContableVenta'=IsNull(dbo.FCuentaContableDescuento(D.IDProducto, @IDTransaccion),''), 
		'TotalDiscriminado'=round(Sum(D.TotalDescuentoDiscriminado *  @vCotizacion),0) --ver que guarde en la moneda de operacion para multiplicar por la cotizacion
		From VDescuento D 
		Where D.IDTransaccion=@IDTransaccion 
		Group By D.IDTransaccion, D.IDProducto
		Open cCFVentaDescuento
		fetch next from cCFVentaDescuento into @vCodigo, @vImporte
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			print concat('Descuentos ', @vIDCuentaContable, ' Codigo:',@vCodigo)
			If @vImporte > 0 And @vIDCuentaContable Is Not Null Begin
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, @vImporte, 0, '')
			End
			
			fetch next from cCFVentaDescuento into @vCodigo, @vImporte
			
		End
		
		close cCFVentaDescuento
		deallocate cCFVentaDescuento
	End
					
	--Cuentas Fijas de Venta 
	Begin
	
		--Cuenta Fija Venta
		Set @vIDCuentaContable = NULL
		Set @vCuentaContableVenta = ''
		Set @vTotalDiscriminado = 0
		Set @vTotalDescuentoDiscriminado = 0
		
		Declare cDetalleVenta cursor for
		Select 
		'CuentaContableVenta'=IsNull(dbo.FCuentaContableVenta(D.IDProducto, @vIDSucursal, 0),''), 
		'TotalDiscriminado'=round(Sum(D.TotalDiscriminado * @vCotizacion),0), 
		'TotalDescuentoDiscriminado'=IsNull((Select round(Sum(DE.DescuentoDiscriminado * @vCotizacion),0) From Descuento DE Where DE.IDTransaccion=D.IDTransaccion And DE.IDProducto=D.IDProducto And DE.ID=D.ID),0) * D.Cantidad
		From VDetalleVenta D 
		Where D.IDTransaccion=@IDTransaccion 
		Group By D.IDTransaccion, D.IDProducto, D.ID, D.Cantidad
		Open cDetalleVenta 
		fetch next from cDetalleVenta into @vCuentaContableVenta, @vTotalDiscriminado, @vTotalDescuentoDiscriminado

		
		While @@FETCH_STATUS = 0 Begin  
			
			set @vImporte = 0
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vCodigo = @vCuentaContableVenta
			
			Print concat('CC Venta ',@vcodigo)
			declare @vDesc varchar(100) = (select descripcion from cuentacontable where codigo = @vcodigo)
			Print concat('CC Venta ',@vdesc)

			--Si no existe, poner el predeterminado
			If @vCuentaContableVenta Is Null or @vCuentaContableVenta = '' Begin
				Set @vCodigo = (Select IsNull((Select Top(1) Codigo From VCFVentaVenta Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda),''))				
			End
			
			--Validar codigo
			If @vCodigo = '' Begin
				fetch next from cDetalleVenta into @vCuentaContableVenta, @vTotalDiscriminado, @vTotalDescuentoDiscriminado
			End
						
			--Hayar el importe	
			Set @vImporte = @vTotalDiscriminado + @vTotalDescuentoDiscriminado
			Set @vDebe = (Select Top(1) Debe From VCFVentaVenta Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda)
			Set @vHaber = (Select Top(1) Haber From VCFVentaVenta Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
																
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			--Cargamod
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin
			print concat('Venta ', @vIDCuentaContable, ' Codigo:',@vCodigo)
				--Si existe la cuenta, actualizar
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+@vImporteHaber,
												Debito=Debito+@vImporteDebe,
												Importe=Importe+@vImporte
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
				End
			End
				
			fetch next from cDetalleVenta into @vCuentaContableVenta, @vTotalDiscriminado, @vTotalDescuentoDiscriminado
			
		End
		
		close cDetalleVenta 
		deallocate cDetalleVenta
		
	End
			
	--Cuentas Fijas Impuesto
	Begin
		
		--Variables
		Declare @vIDImpuesto tinyint
		
		Declare cCFVentaImpuesto cursor for
		Select Codigo, IDImpuesto, Debe, Haber From VCFVentaImpuesto Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFVentaImpuesto   
		fetch next from cCFVentaImpuesto into @vCodigo, @vIDImpuesto, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			Set @vImporte = (Select round(SUM(TotalImpuesto * @vCotizacion),0) From DetalleImpuesto Where IDTransaccion=@IDTransaccion And IDImpuesto=@vIDImpuesto)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFVentaImpuesto into @vCodigo, @vIDImpuesto, @vDebe, @vHaber
			
		End
		
		close cCFVentaImpuesto 
		deallocate cCFVentaImpuesto
	End					

	--Cuentas Fijas de Mercaderia
	Begin
	
		--Variables
		
		Declare cCFVentaMercaderia cursor for
		Select Codigo, Debe, Haber From VCFVentaMercaderia Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFVentaMercaderia   
		fetch next from cCFVentaMercaderia into @vCodigo, @vDebe, @vHaber

		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			Set @vImporte = (Select round(SUM(TotalCosto * @vcotizacion),0) From DetalleVenta Where IDTransaccion=@IDTransaccion)
			
			print @vimporte

			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin	
			print concat('MErcaderia ', @vIDCuentaContable, ' Codigo:',@vCodigo)	
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFVentaMercaderia into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFVentaMercaderia 
		deallocate cCFVentaMercaderia
	End
	
	--Cuentas Fijas de Costo de Mercaderia
	Begin
	
		--Cuenta Fija Venta
		Set @vCuentaContableVenta = ''
		Set @vTotalCosto = 0
			
		Declare cDetalleCosto cursor for
		Select 'CuentaContableCosto'=IsNull(dbo.FCuentaContableCostoCancelarAutomatico(IDProducto, @vIDSucursal,@vIDFormaPagoFactura),''), 
			   'TotalCosto'=round(Sum(TotalCosto * @vCotizacion),0) 
			    From VDetalleVenta 
				Where IDTransaccion=@IDTransaccion 
				Group By IDProducto
		Open cDetalleCosto
		fetch next from cDetalleCosto into @vCuentaContableCosto, @vTotalCosto
		
		While @@FETCH_STATUS = 0 Begin  
			
			set @vImporte = 0
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vCodigo = @vCuentaContableCosto
			
			--Si no existe, poner el predeterminado
			If @vCuentaContableCosto = '' Begin
				Set @vCodigo = (Select IsNull((Select Top(1) Codigo From VCFVentaCostoMercaderia Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda),''))								
			End
			
			--Validar codigo
			If @vCodigo = '' Begin
				fetch next from cDetalleCosto into @vCuentaContableCosto, @vTotalCosto
			End

			--Declare @vProdCosto as integer =(select top(1)IDProducto from Detalleventa where IDTransaccion = @IDTransaccion)
		 --   set @vCodigo = dbo.FCuentaContableCostoCancelarAutomatico(@vProdCosto, @vIDSucursal, @vIDFormaPagoFactura)
			
			--Hayar el importe	
			Set @vImporte = @vTotalCosto
			Set @vDebe = (Select Top(1) Debe From VCFVentaCostoMercaderia Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda)
			Set @vHaber = (Select Top(1) Haber From VCFVentaCostoMercaderia Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')			
										
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			--Cargamod
			If @vImporteHaber > 0 Or @vImporteDebe > 0 Begin
				print concat('Costo ', @vIDCuentaContable, ' Codigo:',@vCodigo)
				--Si existe la cuenta, actualizar
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+@vImporteHaber,
												Debito=Debito+@vImporteDebe,
												Importe=Importe+@vImporte
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
				End
			End
							
			fetch next from cDetalleCosto into @vCuentaContableCosto, @vTotalCosto
			
		End
		
		close cDetalleCosto 
		deallocate cDetalleCosto
		
	End

	----Reparar
		Execute SpRedondearAsientoVentaCancelarAutomatico @IDTransaccion=@IDTransaccion
	
	--Actualizamos la cabecera, el total
	Begin
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
		Set @vImporteHaber = ROUND(@vImporteHaber, 0)
		Set @vImporteDebe = ROUND(@vImporteDebe, 0)
	
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vImporteHaber - @vImporteDebe
		Where IDTransaccion=@IDTransaccion
	End

Salir:
	
End



﻿
CREATE Procedure [dbo].[SpOrdenDePedido]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Numero VARCHAR(50) = NULL,
	@Fecha date = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante varchar(50) = NULL,
	@Observacion varchar(200) = NULL,
	@Autorizacion varchar(50) = NULL,
	@FechaDesde date = null,
	@FechaHasta date = null,
	@Operacion varchar(50),
	@PedidoCliente bit = 'False',

	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin	
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 


	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from OrdenDePedido where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--Fecha de la compra que no sea mayor a hoy
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If @Fecha > getdate() Begin
				set @Mensaje = 'La fecha del comprobante no debe ser mayor a hoy!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
		End
	END

	--BLOQUES
	SET @Numero = REPLACE(@Numero,'.','')
	
	--INSERTAR
	If @Operacion = 'INS' Begin
			
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		------Insertar
		If @Numero=@NroComprobante Begin
			Set @Numero = IsNull((Select MAX(Numero)+1 From VOrdenDePedido Where IDSucursal = @IDSucursal),1)
			Set @NroComprobante = CONVERT(varchar(50), @Numero)	
		End Else Begin
			Set @Numero = IsNull((Select MAX(Numero)+1 From VOrdenDePedido Where IDSucursal = @IDSucursal),1)	
		End

		--Numero --JGR 20140814 Agregando control por IDsucursal
		If Exists(Select * From VOrdenDePedido Where Numero=@Numero AND IDSucursal = @IDSucursal) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		Insert Into OrdenDePedido(IDTransaccion,Numero,Fecha,IDTipoComprobante,NroComprobante,IDDeposito,Observacion,Autorizacion,Anulado,FechaDesde,FechaHasta, PedidoCliente)
		Values(@IDTransaccionSalida, @Numero, @Fecha, @IDTipoComprobante, @NroComprobante, @IDDeposito, @Observacion, @Autorizacion, 'False', @FechaDesde,@FechaHasta, @PedidoCliente)
				
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From OrdenDePedido Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que no este anulado
		if (Select Anulado From OrdenDePedido Where IDTransaccion=@IDTransaccion) = 'True' Begin
			set @Mensaje = 'El registro ya esta anulado!'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal

		--Anulamos el registro
		Update OrdenDePedido Set Anulado = 'True'
		Where IDTransaccion = @IDTransaccion
		
		print '[SpOrdenDePedido] ANULADO'
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From OrdenDePedido Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que no este anulado
					
		--Actualizamos el Stock
		
		--Insertamos el registro de anulacion
		
		--Anulamos el registro
		
	End

	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End


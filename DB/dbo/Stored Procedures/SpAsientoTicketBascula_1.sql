﻿CREATE Procedure [dbo].[SpAsientoTicketBascula]

	@IDTransaccion numeric(18,0)
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	
	--Venta
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vContado bit
	Declare @vCredito bit
	Declare @vCondicion varchar(50)
	Declare @vIDFormaPagoFactura int
	Declare @vFechaEmision date
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money
	Declare @vCuentaContableVenta varchar(50)
	Declare @vCuentaContableCosto varchar(50)
	Declare @vTotalDiscriminado money
	Declare @vTotalDescuentoDiscriminado money
	Declare @vTotalCosto money
	
	--Asiento
	Declare @vImporte decimal(18,4)
	Declare @vCodigo varchar(50)
	
	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vIDTipoProducto int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe decimal(18,4)
	Declare @vImporteHaber decimal(18,4)

	Declare @vCliente as varchar(32)
	Declare @vIDCliente as int
	
	--Obtener valores
	Begin
	
		Set @vContado='False'
		
		Select	@vIDSucursal=IDSucursal,
				@vFechaEmision=Fecha,
				@vIDMoneda=IDMoneda,
				@vCotizacion=(Case when IDMoneda > 1 then Cotizacion else 1 end),
				@vIDTipoProducto = IDTipoProducto,
				@vIDTipoComprobante =IDTipoComprobante,
				@vComprobante = NroComprobante,
				@vTipoComprobante = TipoComprobante
		From VTicketBascula 
		Where IDTransaccion=@IDTransaccion
		and NroAcuerdo > 0
		and Anulado = 'False'
			
	End
					
	--Verificar que el asiento se pueda modificar
	Begin
	
		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta procesado
		If (Select Procesado From TicketBascula Where IDTransaccion=@IDTransaccion) = 'False' Begin
			print 'La Ticket no es valido'
			GoTo salir
		End 	
		
		--Si esta anulado
		If (Select Anulado From TicketBascula Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'La venta esta anulada'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		
	End
				
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = @vTipoComprobante + ' ' + @vComprobante
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFechaEmision, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	
	--Cuentas Fijas de Clientes
	Begin
	
		--Variables
		--select * from vcfTicketBascula
		Declare cCFProvision cursor for
		Select Codigo, Debe, Haber From VCFTicketBascula 
		--Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda and IDTipoProducto = @vIDTipoProducto
		Where IDMoneda=@vIDMoneda and IDTipoProducto = @vIDTipoProducto
		and BuscarEnTipoProducto = 'False' and Flete = 'False'
		Open cCFProvision   
		fetch next from cCFProvision into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			--Obtener la cuenta
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			
			--if @vIDMoneda = 1 Begin
				Set @vImporte = (Select TotalDiscriminado From TicketBascula Where IDTransaccion=@IDTransaccion)
			--end
			--if @vIDMoneda > 1 begin
				--Set @vImporte = (Select Round((TotalDiscriminadoUS * Cotizacion),0) From TicketBascula Where IDTransaccion=@IDTransaccion)
			--end

			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End	
				
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin				
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFProvision into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFProvision 
		deallocate cCFProvision
	End
	
	--Costo del producto
	Declare cCFCosto cursor for
		Select Codigo, Debe, Haber From VCFTicketBascula 
--		Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda and IDTipoProducto = @vIDTipoProducto
		Where IDMoneda=@vIDMoneda and IDTipoProducto = @vIDTipoProducto
		and BuscarEnTipoProducto = 'True' and Flete = 'False'
		Open cCFCosto   
		fetch next from cCFCosto into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  

			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
		    Set @vCodigo = (Select IsNull(CuentaContableCosto,@vCodigo) from vTipoProducto where ID = @vIDTipoProducto)

			--Obtener la cuenta
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			if @vIDMoneda = 1 Begin
				Set @vImporte = (Select TotalDiscriminado From TicketBascula Where IDTransaccion=@IDTransaccion)
				set @vImporte = @vImporte + Isnull((Select Sum(ImporteGS) From TicketBasculaGastoAdicional Where IDTransaccion=@IDTransaccion),0)
			end
			if @vIDMoneda > 1 begin
				Set @vImporte = (Select Round((TotalDiscriminadoUS * Cotizacion),0) From TicketBascula Where IDTransaccion=@IDTransaccion)
				set @vImporte = @vImporte + Isnull((Select Sum((ImporteUS * CotizacionUS)) From TicketBasculaGastoAdicional Where IDTransaccion=@IDTransaccion),0)
			end
			--select * from ticketbasculagastoadicional
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End	
				
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin				
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFCosto into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFCosto 
		deallocate cCFCosto

	--Flete
	Declare cCFFletes cursor for
		Select Codigo, Debe, Haber From VCFTicketBascula 
		--Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Where IDMoneda=@vIDMoneda
		and BuscarEnTipoProducto = 'False' and Flete = 'True'
		Open cCFFletes   
		fetch next from cCFFletes into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			--Obtener la cuenta
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			if @vIDMoneda = 1 Begin
				Set @vImporte = (Select Sum(ImporteGS) From TicketBasculaGastoAdicional Where IDTransaccion=@IDTransaccion)
			end
			if @vIDMoneda > 1 begin
				Set @vImporte = (Select Round((ImporteUS * CotizacionUS),0) From TicketBasculaGastoAdicional Where IDTransaccion=@IDTransaccion)
			end

			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End	
				
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin				
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFFletes into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFFletes 
		deallocate cCFFletes

	--Actualizamos la cabecera, el total
	Begin
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
		Set @vImporteHaber = ROUND(@vImporteHaber, 0)
		Set @vImporteDebe = ROUND(@vImporteDebe, 0)
	
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vImporteHaber - @vImporteDebe
		Where IDTransaccion=@IDTransaccion
	End

	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
	
End



﻿CREATE Procedure [dbo].[SpActualizarEfectivo]
	
	--Entrada
	@IDTransaccion numeric(18,0),
	@ImporteHabilitado decimal,
	@Habilitado bit,
	@ID tinyint,
		
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	

As

Begin
	
	
	Begin
	
		--Actualizar Efectivo 
		Update Efectivo Set Habilitado = @Habilitado,
							ImporteHabilitado=@ImporteHabilitado 
		Where IDTransaccion = @IDTransaccion And ID=@ID
		
		Set @Mensaje = 'Registro procesado!'
		Set @Procesado = 'True'	
		Return @@rowcount
	
	End
	
End


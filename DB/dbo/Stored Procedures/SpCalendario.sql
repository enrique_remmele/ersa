﻿CREATE Procedure [dbo].[SpCalendario]

	--Entrada
	@Fecha date,
	@Motivo varchar(200),
	@Habil bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	Declare @vPrimerDia date = (SELECT DATEADD(MONTH, DATEDIFF(MONTH, 0,@fecha), 0) )
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Calendario Where MONTH(fecha) = MONTH(@Fecha) and YEAR(Fecha) = year(@Fecha) ) begin
			set @Mensaje = 'Los dias del Mes ya estan generados!'
			set @Procesado = 'False'
			return @@rowcount
		end

		--recuperamos la cantidad de dias del mes
		Declare @vDias as integer
		Set @vDias = (select DATEDIFF(dd, @fecha, DATEADD(mm, 1, @fecha)))
		
		--Insertamos
		Declare @cnt integer = 1
		WHILE @cnt <= @vDias
			BEGIN
			 IF ((SELECT DATENAME(dw, cast(@vPrimerDia as date))) = 'Sunday') or ((SELECT DATENAME(dw, cast(@vPrimerDia as date))) = 'Domingo') begin
			  Insert Into Calendario(fecha,Habil,Motivo) Values(@vPrimerDia,'False',@Motivo)
			 end
			 IF ((SELECT DATENAME(dw, cast(@vPrimerDia as date))) <> 'Sunday') and ((SELECT DATENAME(dw, cast(@vPrimerDia as date))) <> 'Domingo') begin
			  Insert Into Calendario(fecha,Habil,Motivo) Values(@vPrimerDia,'True',@Motivo)
			 end
			SET @cnt = @cnt + 1;
			set @vPrimerDia = (SELECT DATEADD(day, 1, @vPrimerDia))
		END;
		--Insert Into Calendario(Fecha,Habil,Motivo) Values(@Fecha,@Habil,@Motivo)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CALENDARIO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
				 		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From Calendario Where Fecha = @Fecha) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		
		--Actualizamos
		Update Calendario 
		Set Habil = @Habil,
			Motivo = @Motivo			
		Where Fecha = @Fecha
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CALENDARIO', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Calendario Where fecha = @Fecha) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		
		--Eliminamos
		--Delete From Calendario	Where Fecha = @Fecha
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CALENDARIO', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


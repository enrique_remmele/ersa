﻿CREATE Procedure [dbo].[SpImportarInventario]

	--Entrada
	--Identificadores
	@Producto varchar(200) = NULL,
	@Deposito varchar(50) = NULL,
	@ExistenciaMinima Decimal(10,2) = 0,
	@ExistenciaMaxima Decimal(10,2) = 0,
	@ExistenciaCritica Decimal(10,2) = 0,
	@Existencia Decimal(10,2) = 0,
	
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	@Actualizar bit
	
As

Begin

	Declare @vIDTransaccion numeric(18,0)
	Declare @vIDDeposito int
	Declare @vIDSucursal int
	
	Declare @vExistenciaSistema decimal(10,2)
	Declare @vDiferencia decimal(10,2)
	Declare @vEntrada decimal(10,2)
	Declare @vSalida decimal(10,2)
	 
	Declare @vIDProducto int
	Declare @vOperacion varchar(50)
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
		
	--Hayar valores
	Begin
		Set @vOperacion = 'INS'
		Set @vMensaje = 'No se proceso!'
		Set @vProcesado = 'False'
		
		Set @vExistenciaSistema = 0
		Set @vDiferencia = 0
		Set @vEntrada = 0
		Set @vSalida = 0
				
		--Deposito
		Set @vIDDeposito = (IsNull((Select Top(1) ID From Deposito Where Descripcion=@Deposito),0))
		If @vIDDeposito = 0 Begin
			Set @vMensaje = 'No se encontro el deposito!'
			Set @vProcesado = 'False'
			Goto Salir
		End
				
		--Sucursal
		Set @vIDSucursal = (Select IDSucursal From Deposito Where ID=@vIDDeposito)
		
		--Producto
		Set @vIDProducto = (IsNull((Select Top(1) ID From Producto Where Referencia=@Producto),0))
		If @vIDProducto = 0 Begin
			Set @vMensaje = 'No se encontro el producto!'
			Set @vProcesado = 'False'
			Goto Salir
		End
				
		--Transaccion
		Set @vIDTransaccion = (ISNULL((Select MAX(IDTransaccion) From AjusteInicial Where Procesado = 'False'),0))
		If @vIDTransaccion = 0 Begin
			Set @vMensaje = 'Debe generar un Ajuste Inicial nuevo para continuar!'
			Set @vProcesado = 'False'
			Goto Salir
		End
		
		Set @vExistenciaSistema = (ISNULL((Select Existencia From ExistenciaDeposito Where IDDeposito=@vIDDeposito And IDProducto=@vIDProducto),0))
		Set @vDiferencia = IsNull(@Existencia - @vExistenciaSistema,0)
		
		If @vDiferencia > 0 Begin
			Set @vEntrada = @vDiferencia
		End
		
		If @vDiferencia < 0 Begin
			Set @vSalida = @vDiferencia * -1
		End
	
	End
	
	--Verificar si existe
	If Exists(Select * From DetalleAjusteInicial Where IDTransaccion=@vIDTransaccion And IDSucursal=@vIDSucursal And IDDeposito=@vIDDeposito And IDProducto=@vIDProducto) Begin
		Set @vOperacion = 'UPD'	
	End
	
	If @vOperacion = 'INS' Begin
		
		Insert Into DetalleAjusteInicial(IDTransaccion, IDProducto, IDSucursal, IDDeposito, ExistenciaSistema, Existencia, Diferencia, Entrada, Salida)
		Values(@vIDTransaccion, @vIDProducto, @vIDSucursal, @vIDDeposito, @vExistenciaSistema, @Existencia, @vDiferencia, @vEntrada, @vSalida)
			
		Set @vMensaje = 'Registro Insertado!'
		Set @vProcesado = 'True'
	End
	
	If @vOperacion = 'UPD' Begin
		
		If @Actualizar = 'True' Begin	
			
			Update DetalleAjusteInicial Set Existencia=@Existencia,
											ExistenciaSistema=@vExistenciaSistema,
											Diferencia=@vDiferencia,
											Entrada=@vEntrada,
											Salida=@vSalida
			Where IDTransaccion=@vIDTransaccion And IDProducto=@vIDProducto And IDSucursal=@vIDSucursal And IDDeposito=@vIDDeposito
			
			Set @vMensaje = 'Registro Actualizado!'
			Set @vProcesado = 'True'
		End
	End
	
Salir:

	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	
	
End



﻿CREATE Procedure [dbo].[SpVentaProcesar]

	--Entrada
	@IDTransaccion numeric(18,0),
	@IDTransaccionPedido numeric(18) = 0,
	@EsPedido bit = 'False',
	@EsMovil bit = 'False'	
	
As

Begin

	--Variables
	Declare @Mensaje varchar(200)
	Declare @Procesado bit
	Declare @vComprobante varchar(50)
	Declare @vNroComprobante int
	Declare @vIDPuntoExpedicion int
	Declare @vCantidad decimal(18,0)
	Declare @vIDProducto int
	Declare @vIDDeposito tinyint
	Declare @vIDSucursal tinyint
	Declare @vIDCliente int
	Declare @vVentaPermitirCantidadNegativa bit
	Declare @vIDSucursalCliente int = 0
	
	--Variables Kardex
	Declare @vFecha datetime
	Declare @vCostoUnitario decimal(18,2)
	Declare @vID int
	Declare @vNumeroPedido as integer = 0

	--Obtener valores
	Set @vComprobante = (Select Comprobante From Venta Where IDTransaccion=@IDTransaccion)
	Set @vNroComprobante = (Select NroComprobante From Venta Where IDTransaccion=@IDTransaccion)
	Set @vIDPuntoExpedicion = (Select IDPuntoExpedicion From Venta Where IDTransaccion=@IDTransaccion)
	Set @vIDCliente = (Select IDCliente From Venta Where IDTransaccion=@IDTransaccion)
	Set @vIDSucursal = (Select IDSucursal From Venta Where IDTransaccion=@IDTransaccion)
	Set @vFecha = (Select FechaEmision From Venta Where IDTransaccion=@IDTransaccion)
	Set @vIDSucursalCliente = (Select ISNull(IDSucursalCliente,0) From Venta Where IDTransaccion=@IDTransaccion)
	Set @vVentaPermitirCantidadNegativa = ISNull((Select Top(1) VentaPermitirCantidadNegativa From Configuraciones Where IDSucursal=@vIDSucursal), 'False')
	Set @Mensaje = 'No se realizo ninguna accion'
	Set @Procesado = 'False'
	
	--Validar
	
	--VENTA
	Begin
		--Validar Feha Operacion
		Declare @Fecha date = (Select FechaEmision from Venta where IDTransaccion = @IDTransaccion)
		Declare @IDSucursal int = (Select IDSucursal from Venta where IDTransaccion = @IDTransaccion)
		Declare @IDUsuario int = (Select IDUsuario from vVenta where IDTransaccion = @IDTransaccion)
		Declare @IDOperacion int = (Select IDOperacion from Transaccion where ID = @IDTransaccion)
		
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			Goto Error
		End

		if @IDTransaccionPedido > 0 begin
			if exists(select * from PedidoVenta where IDTransaccionPedido = @IDTransaccionPedido) begin
				Set @Mensaje  = 'El pedido ya esta asociado a una Factura!!'
				Set @Procesado = 'False'
				--set @IDTransaccionSalida  = 0
				Goto Error
			end 
			else begin
				set @vNumeroPedido = isnull((Select Numero From Pedido where IDTransaccion = @IDTransaccionPedido),0)
			end
		end

	
		--Transaccion
		If Not Exists(Select TOP 1 * From Venta Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'Error: El sistema no encuentra el registro de venta! Intente nuevamente.'
			set @Procesado = 'False'
			Goto Error
		End

		----Verificar que si es con cancelacion automatica se cargue por pedido y no directo facturado
		--If Exists(Select top(1)* from Venta where IDTransaccion = @IDTRansaccion and IdFormaPagoFactura in (Select id from FormaPagoFactura
		--where CancelarAutomatico = 1 and IDTransaccion in (select IDTransaccionVenta from PedidoVenta where IDTransaccionVenta = @IDTRansaccion))) Begin
		--	set @Mensaje = 'Forma de Pago Con Cancelacion automatica debe ingresar por pedido y ser aprobado.'
		--	set @Procesado = 'False'
		--	Goto Error
		--End
	
		--Verificar que no tenga factura CONTADO pendiente dias anteriores
		If Exists(select Top(1)* from vventa where credito ='False' and anulado = 'False' and procesado = 1
		and saldo >1 and IDCliente = @vIDCliente and FechaEmision < @vFecha and CancelarAutomatico=1) Begin
			set @Mensaje = 'El Cliente tiene Factura Contado pendiente de dias anteriores.'
			set @Procesado = 'False'
			Goto Error
		End
		
		--Comprobante
		If Exists(Select TOP 1 * From Venta Where IDPuntoExpedicion=@vIDPuntoExpedicion And NroComprobante=@vNroComprobante And Procesado='True') Begin
			set @Mensaje = 'El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			Goto Error
		End
		
		--Comprobante fuera del Rango
		If @vNroComprobante < (Select NumeracionDesde From PuntoExpedicion Where ID=@vIDPuntoExpedicion) Or @vNroComprobante > (Select NumeracionHasta From PuntoExpedicion Where ID=@vIDPuntoExpedicion) Begin
			set @Mensaje = 'El numero de comprobante no esta dentro del rango correcto! Cambie el punto de expedicion o actualicelo.'
			set @Procesado = 'False'
			Goto Error
		End
		
		--Credito
		If (Select Credito From Venta Where IDTransaccion=@IDTransaccion)='True' Begin
			
			--Fecha
			If DATEDIFF(dd, (Select FechaEmision From Venta Where IDTransaccion=@IDTransaccion), (Select FechaVencimiento From Venta Where IDTransaccion=@IDTransaccion)) < 0 Begin
				set @Mensaje = 'La fecha de de vencimiento no puede ser menor al del comprobante!'
				set @Procesado = 'False'
				Goto Error
			End
		
			--Plazo credito
			Declare @vPlazoCredito smallint
			IF @vIDSucursalCliente = 0 Begin
			 Set @vPlazoCredito = (Select ISNULL((Select PlazoCredito From Cliente Where ID=@vIDCliente),0))
			End
			IF @vIDSucursalCliente > 0 Begin
			 Set @vPlazoCredito = (Select ISNULL((Select PlazoCredito From ClienteSucursal Where IDCliente=@vIDCliente and ID = @vIDSucursalCliente),0))
			End
			
			If DATEDIFF(dd, (Select FechaEmision From Venta Where IDTransaccion=@IDTransaccion), (Select FechaVencimiento From Venta Where IDTransaccion=@IDTransaccion)) > @vPlazoCredito Begin
				set @Mensaje = 'El plazo de credito es mayor al del que tiene asignado el cliente!'
				set @Procesado = 'False'
				Goto Error
			End
			
		END
		
		----Controla deposito CAB vs DET //JGR 10/05/2014
		If Exists(Select TOP 1 * From Venta V JOIN DetalleVenta DV ON V.IDTransaccion = DV.IDTransaccion 
			Where V.IDTransaccion=@IDTransaccion AND V.IDDeposito <> DV.IDDeposito) Begin
			set @Mensaje = 'Error: Registro de depósitos no coinciden! Consulte con el administrador del sistema.'
			set @Procesado = 'False'
			Goto Error
		End
		
	End
		
	--Stock
	Begin
		
		Declare db_cursor cursor for
		Select Cantidad, IDProducto, IDDeposito From DetalleVenta Where IDTransaccion=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vCantidad, @vIDProducto, @vIDDeposito
		While @@FETCH_STATUS = 0 Begin  
		
			--Cantidad
			If @vVentaPermitirCantidadNegativa = 0 Begin
				If @vCantidad <= 0 Begin
					Set @Mensaje = 'La cantidad no puede ser igual ni menor a 0!'
					Set @Procesado = 'False'
					Goto Error		
				End	
			End

			--Verificar Existencia
			Declare @vExistenciaProducto decimal(10,2)
			Declare @vExistenciaReservado decimal(10,2)
			Declare @vExistenciaReal decimal(10,2)
			
			If @EsPedido='True' Begin
				Set @vExistenciaProducto = dbo.FExistenciaProducto(@vIDProducto, @vIDDeposito)
			End Else Begin
				Set @vExistenciaProducto = dbo.FExistenciaProducto(@vIDProducto, @vIDDeposito)
			End
			
			If @vVentaPermitirCantidadNegativa = 0 Begin
				--If  @vExistenciaProducto < @vCantidad Begin
				If (@vExistenciaProducto < @vCantidad) and  ((Select ControlarExistencia From Producto Where ID=@vIDProducto)= 1) Begin
					Declare @vProducto varchar(50)
					Declare @vExistencia decimal(18,0)
				
					Set @vProducto = (Select Descripcion From Producto Where ID=@vIDProducto)
					Set @vExistencia = (Select dbo.FExistenciaProducto(@vIDProducto, @vIDDeposito))
				
					Set @Mensaje = 'Stock insuficientes! ' + CONVERT(varchar(50), @vProducto) + ': ' + CONVERT(varchar(50), @vExistencia)
					Set @Procesado = 'False'
				
					Close db_cursor   
					Deallocate db_cursor
				
					Goto Error				
				End
			End
			
			Fetch Next From db_cursor Into @vCantidad, @vIDProducto, @vIDDeposito
			
		End
		
		Close db_cursor   
		Deallocate db_cursor	
		
	End
	
	--Actividades
	Begin
	
		Declare @vHabilitar bit
		Set @vHabilitar = 'False'
	
		If @vHabilitar = 'True' Begin
			Set @Mensaje = 'Stock insuficientes! ' + CONVERT(varchar(50), @vProducto) + ': ' + CONVERT(varchar(50), @vExistencia)
			Set @Procesado = 'False'
											
			Goto Error		
		End
		
	End
	
	--Procesar
	Begin
	
		--Actualizar Punto de Expedicion
		Update PuntoExpedicion Set UltimoNumero=@vNroComprobante
		Where ID=@vIDPuntoExpedicion
		
		--Actualizar los productos
		Declare db_cursorProducto cursor for
		Select Sum(DV.Cantidad), DV.IDProducto, 0, DV.IDDeposito, 'CostoPromedio'=[dbo].[FCostoProducto] (DV.IDProducto) 
		From DetalleVenta DV
		Where DV.IDTransaccion=@IDTransaccion
		group by DV.IDProducto,DV.IDDeposito
		Open db_cursorProducto   
		Fetch Next From db_cursorProducto Into @vCantidad, @vIDProducto, @vID, @vIDDeposito, @vCostoUnitario
		While @@FETCH_STATUS = 0 Begin  
			
			EXEC SpActualizarProductoDeposito
				@IDDeposito = @vIDDeposito,
				@IDProducto = @vIDProducto,
				@Cantidad = @vCantidad,
				@Signo = N'-',
				@Mensaje = @Mensaje OUTPUT,
				@Procesado = @Procesado OUTPUT
						 			
						--Actualiza el Kardex			
			EXEC SpKardex
				@Fecha = @vFecha,
				@IDTransaccion = @IDTransaccion,
				@IDProducto = @vIDProducto,
				@ID = @vID,
				@Orden = 10,
				@CantidadEntrada = 0,
				@CantidadSalida = @vCantidad,
				@CostoEntrada = 0,
				@CostoSalida = @vCostoUnitario,
				@IDSucursal = @vIDSucursal,
				@Comprobante = @vNroComprobante	
			
			Fetch Next From db_cursorProducto Into @vCantidad, @vIDProducto, @vID, @vIDDeposito, @vCostoUnitario
			
		End
		
		Close db_cursorProducto   
		Deallocate db_cursorProducto	
		
		Update Venta Set Procesado='True'
						--IDVendedor=@vIDVendedor
		Where IDTransaccion=@IDTransaccion
	
		--Actualizar el Cliente (Saldo Total, Saldo Credito, Ultima Compra)
		EXEC SpAcualizarClienteVenta
			@IDCliente = @vIDCliente,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT
			
		--Actualizar el Detalle Impuesto
		Exec SpVentaRecalcularImpuesto @IDTransaccion=@IDTransaccion

		--Si MOVIL = 'False' ejecutamos los siguientes procesos
		If @EsMovil = 'False' Begin
		
			--Actualizar los descuentos tacticos
			EXEC SpActualizarActividad
				@Operacion = 'INS',
				@IDTransaccion = @IDTransaccion,
				@Mensaje = @Mensaje OUTPUT,
				@Procesado = @Procesado OUTPUT
					
			--Generar Asiento - OCULTAR TEMPORALMENTE
			Exec SpAsientoVenta @IDTransaccion=@IDTransaccion

			if @vNumeroPedido > 0 begin
				--Asociar el pedido
				Exec SpVentaEstado @IDTransaccionVenta = @IDTransaccion, @IDPedido= @vNumeroPedido , @IDSucursal= @vIDSucursal
			
			end
			
			--Cargar Comisiones
			EXEC SpComision
				@IDTransaccion=@IDTransaccion,
				@Venta='True',
				@Devolucion='False',
				@Operacion= 'INS'
		
		End
		
		Select 'Procesado'='True', 'Mensaje'='Registro procesado!', 'Procesados'=@@ROWCOUNT
		Return @@Rowcount
			
	End
			
Error:
	--select * from venta where NroComprobante = '35949' and idsucursal = 4
	--Si se produjo un error, eliminar toda la transaccion
	--Actualizar el Cliente (Saldo Total, Saldo Credito, Ultima Compra)
	Declare @vMensaje varchar(200)
	Declare @vProcesado bit
	--Declare @IDTransaccion int = 731157
	--Eliminamos el Asiento
	Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
	Delete From Asiento Where IDTransaccion=@IDTransaccion	
	
	Delete From DetalleImpuesto Where IDTransaccion=@IDTransaccion	
	Delete From Descuento Where IDTransaccion=@IDTransaccion	
	Delete From DetalleVenta Where IDTransaccion=@IDTransaccion	
	Delete From Venta Where IDTransaccion=@IDTransaccion	
	--Delete From Comision Where IDTransaccion=@IDTransaccion	
	
	--Delete From Kardex Where IDTransaccion = @IDTransaccion

	Select 'Procesado'='False', 'Mensaje'=@Mensaje, 'Procesados'=@@ROWCOUNT
	
	EXEC SpAcualizarClienteVenta
		@IDCliente = @vIDCliente,
		@Mensaje = @vMensaje OUTPUT,
		@Procesado = @vProcesado OUTPUT
					
End



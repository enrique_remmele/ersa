﻿CREATE Procedure [dbo].[SpRegenerarAsientoTicketBascula]

	@Desde date,
	@Hasta date,
	@Todos bit = 'False',
	@SoloConDiferencias bit = 'False',
	@IDSucursal tinyint
	
As

Begin
	
	--Variables
	Begin
		Declare @vIDTransaccion numeric(18,0)		
		Declare @vComprobante varchar(50)	
		Declare @vCantidad int
		Declare @vRegistrosProcesados int
		Declare @vPorcentaje int			
	End
	
	If Not Exists (Select * From sys.objects Where object_id = OBJECT_ID(N'[dbo].[GeneracionAsientoNotaCredito]') And type in (N'U')) Begin
		Create Table GeneracionAsientoNotaCredito(ID int, Cantidad int, RegistrosProcesados int, Porcentaje int)
	End
	
	Delete From GeneracionAsientoNotaCredito
	
	Set @vCantidad = (Select Count(*) From VTicketBascula Where Fecha Between @Desde And @Hasta And IDSucursal=@IDSucursal)
	Set @vRegistrosProcesados = 0
	Set @vPorcentaje = 0
	
	Insert Into GeneracionAsientoNotaCredito(ID, Cantidad, RegistrosProcesados, Porcentaje)
	Values(1, @vCantidad, 0, 0)

	--Cobranzas Creditos	
	Begin
		Declare cCobranzaCredito cursor for
		Select IDTransaccion From TicketBascula Where Fecha Between @Desde And @Hasta And IDSucursal=@IDSucursal
		Open cCobranzaCredito 
		fetch next from cCobranzaCredito into @vIDTransaccion

		While @@FETCH_STATUS = 0 Begin  			  

			print @vComprobante
			
			--Generar
			Exec SpAsientoTicketBascula @IDTransaccion=@vIDTransaccion	
			
			--Contador
			Set @vRegistrosProcesados = @vRegistrosProcesados + 1
			Set @vPorcentaje = (@vRegistrosProcesados / @vCantidad) * 100
			
			Update GeneracionAsientoNotaCredito Set RegistrosProcesados=@vRegistrosProcesados,
														Porcentaje=@vPorcentaje
			Where ID=1
							
			--Siguiente
			fetch next from cCobranzaCredito into @vIDTransaccion
			
		End   
		
		close cCobranzaCredito
		deallocate cCobranzaCredito
	End
			
	Select 'Mensaje'='Registros procesados: ' + CONVERT(varchar(50), @vCantidad), 'Procesado'='True'
	
End

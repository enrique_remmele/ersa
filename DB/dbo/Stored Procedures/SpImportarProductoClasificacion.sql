﻿CREATE Procedure [dbo].[SpImportarProductoClasificacion]

	--Entrada
	@Codigo varchar(50) = NULL,
	@Clasificacion1 varchar(50) = NULL,
	@Clasificacion2 varchar(50) = NULL,
	@Clasificacion3 varchar(50) = NULL,
	@Clasificacion4 varchar(50) = NULL,
	@Clasificacion5 varchar(50) = NULL,
	@Actualizar bit

As
	
Begin

	--Variables
	Declare @vOperacion varchar(50)
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	Declare @vID int
	Declare @vIDClasificacion1 int
	Declare @vIDClasificacion2 int
	Declare @vIDClasificacion3 int
	Declare @vIDClasificacion4 int
	Declare @vIDClasificacion5 int
	
	Set @vOperacion = 'INS'
	Set @vMensaje = 'Sin proceso'
	Set @vProcesado = 'False'
	
	Begin
		Set @vID = (Select IsNull((Select Top(1) ID From Producto Where Referencia=@Codigo), 0))
		
		Exec @vIDClasificacion1 = SpTipoProductoInsUpd @Clasificacion1
		Exec @vIDClasificacion2 = SpLineaInsUpd @Clasificacion2
		Exec @vIDClasificacion3 = SpSubLineaInsUpd @Clasificacion3
		Exec @vIDClasificacion4 = SpSubLinea2InsUpd @Clasificacion4
		Exec @vIDClasificacion5 = SpMarcaInsUpd @Clasificacion5
		
		print 'Clasificacion 1: ' + convert(varchar(50), @vIDClasificacion1)
		print 'Clasificacion 2: ' + convert(varchar(50), @vIDClasificacion2)
		print 'Clasificacion 3: ' + convert(varchar(50), @vIDClasificacion3)
		print 'Clasificacion 4: ' + convert(varchar(50), @vIDClasificacion4)
		
	End
	
	If @vID = 0 Begin
		Set @vMensaje = 'No se encontro el producto'
		Set @vProcesado = 'False'
		GoTo Salir
	End
	
	Update Producto Set IDTipoProducto=@vIDClasificacion1,
						IDLinea=@vIDClasificacion2,
						IDSubLinea=@vIDClasificacion3,
						IDSubLinea2=@vIDClasificacion4,
						IDMarca=@vIDClasificacion5
	Where ID=@vID
	Set @vMensaje = 'Actualizado'
	Set @vProcesado = 'True'
	
	--Unir Clasificacion 1 y 2
	If Not Exists(Select * From TipoProductoLinea Where IDTipoProducto=@vIDClasificacion1 And IDLinea=@vIDClasificacion2) Begin
		Insert into TipoProductoLinea(IDTipoProducto, IDLinea)
		Values(@vIDClasificacion1, @vIDClasificacion2) 
	End					
	
	--Unir Clasificacion 2 y 3
	If Not Exists(Select * From LineaSubLinea Where IDTipoProducto=@vIDClasificacion1 And IDLinea=@vIDClasificacion2 And IDSubLinea=@vIDClasificacion3) Begin
		Insert into LineaSubLinea(IDTipoProducto, IDLinea , IDSubLinea)
		Values(@vIDClasificacion1, @vIDClasificacion2, @vIDClasificacion3) 
	End					
	
	--Unir Clasificacion 3 y 4
	If Not Exists(Select * From SubLineaSubLinea2 Where IDTipoProducto=@vIDClasificacion1 And IDLinea=@vIDClasificacion2 And IDSubLinea=@vIDClasificacion3 And IDSubLinea2=@vIDClasificacion4) Begin
		Insert into SubLineaSubLinea2(IDTipoProducto, IDLinea, IDSubLinea, IDSubLinea2)
		Values(@vIDClasificacion1, @vIDClasificacion2, @vIDClasificacion3, @vIDClasificacion4) 
	End					
	
	--Unir Clasificacion 4 y 5
	If Not Exists(Select * From SubLinea2Marca Where IDTipoProducto=@vIDClasificacion1 And IDLinea=@vIDClasificacion2 And IDSubLinea=@vIDClasificacion3 And IDSubLinea2=@vIDClasificacion4 And IDMarca=@vIDClasificacion5) Begin
		Insert into SubLinea2Marca(IDTipoProducto, IDLinea, IDSubLinea, IDSubLinea2, IDMarca)
		Values(@vIDClasificacion1, @vIDClasificacion2, @vIDClasificacion3, @vIDClasificacion4, @vIDClasificacion5) 
	End					
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	
End


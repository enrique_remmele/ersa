﻿CREATE Procedure [dbo].[SpZonaVentaImportar]

	--Entrada
	@Referencia varchar(100),
	@Descripcion varchar(100),
	
	@Actualizar bit = 'True'
	
As

Begin

	Declare @vID int
	
	--Si existe
	If Exists(Select * From ZonaVenta Where Descripcion=@Descripcion) Begin
		Update ZonaVenta Set Descripcion=@Descripcion, Referencia=@Referencia
		Where Descripcion=@Descripcion
	End
	
	--Si existe
	If Not Exists(Select * From ZonaVenta Where Descripcion=@Descripcion) Begin
		If @Actualizar = 'True' Begin
			Set @vID = (Select IsNull(Max(ID)+1,1) From ZonaVenta)
			Insert Into ZonaVenta(ID, Descripcion, Orden, Estado, Referencia)
			Values(@vID, @Descripcion,  0, 'True', @Referencia)
		End
	End
	
End



﻿CREATE Procedure [dbo].[SpSubLinea2Marca]

	--Entrada
	@IDSubLinea2 smallint,
	@IDMarca tinyint,
	@Relacionar bit
	
				
As

Begin

	--BLOQUES
	
	--RELACIONAR
	if @Relacionar='True' begin
		
		--Solo agregar si no existen
		If Exists(Select * From SubLinea2Marca Where IDSubLinea2=@IDSubLinea2 And IDMarca=@IDMarca) Begin
			return @@rowcount
		End
		
		--Insertamos
		Insert Into SubLinea2Marca(IDSubLinea2, IDMarca)
		Values(@IDSubLinea2, @IDMarca)		
		
		return @@rowcount
			
	End
	
	--NO RELACIONAR
	if @Relacionar='False' begin
		
		Delete From SubLinea2Marca 
		Where IDSubLinea2=@IDSubLinea2 And IDMarca=@IDMarca
		
		return @@rowcount
			
	End

End


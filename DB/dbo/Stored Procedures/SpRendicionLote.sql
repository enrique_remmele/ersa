﻿
CREATE Procedure [dbo].[SpRendicionLote]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDTransaccionLote numeric(18,0) = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@Numero int = NULL,
	@IDTipoComprobante smallint = NULL,
	@Comprobante varchar(50) = NULL,	
	@Fecha date = NULL,
	@Observacion varchar(100) = '',
	
	--Totales
	@TotalEfectivo money  = 0,
	@TotalDocumento money  = 0,
	@TotalAnulados money  = 0,
	@TotalCobranzaExtra money = 0,
	@TotalCheque money  = 0,
	@TotalChequeAlDiaBancoLocal money  = 0,
	@TotalChequeAlDiaOtrosBancos money  = 0,
	@TotalChequeDiferido money  = 0,
	@Total money  = 0,
	
	--Rendicion Chofer
	@UsuarioEsChofer bit = 'False',
	@IDChofer smallint = 0,

	--Operacion
	@Operacion varchar(10),
		
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
	
As

Begin
	
	--Procesar Saldos de Ventas
	If @Operacion = 'INS' Begin
	
		--Validar
		--Asegurarse que el lote no tenga mas de una Rendicion
		if Exists(Select * From RendicionLote Where IDTransaccionLote=@IDTransaccionLote And Anulado='False') Begin
			Set @Mensaje = 'El lote ya tiene una Rendicion anterior.....!'
			Set @Procesado = 'False'
			Return @@rowcount
		End
		
		--Que el Numero de operacion no exista
		If @UsuarioEsChofer = 'True' Begin
			If Exists(Select Top(1) RL.* From RendicionLote RL Join RendicionLoteChofer RLC On RL.IDTransaccion=RLC.IDTransaccion Where RLC.Numero=@Numero And RL.IDSucursal=@IDSucursal And RLC.IDChofer=@IDChofer and RL.Anulado = 'false') Begin
				Set @Mensaje = 'El lote ya tiene una Rendicion anterior!'
				Set @Procesado = 'False'
				Return @@rowcount
			End
		End

		If @UsuarioEsChofer = 'False' Begin
			If Exists(Select Top(1) RL.* From RendicionLote RL Where RL.Numero=@Numero And RL.IDSucursal=@IDSucursal and RL.Anulado = 'false') Begin
				Set @Mensaje = 'El lote ya tiene una Rendicion anterior!'
				Set @Procesado = 'False'
				Return @@rowcount
			End
		End

		--Que el comprobante no exista
		If Exists(Select * From RendicionLote Where IDTipoComprobante=@IDTipoComprobante And Comprobante=@Comprobante And IDSucursal=@IDSucursal And Anulado='False') Begin
			Set @Mensaje = 'El comprobante ya existe!'
			Set @Procesado = 'False'
			Return @@rowcount
		End
		
		--Insertar Transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT

		--Insertamos el registro
		Set @Numero = IsNull((Select Max(Numero) + 1 From RendicionLote),1)
		
		Insert Into RendicionLote(IDTransaccion, IDTransaccionLote, IDSucursal, Numero, IDTipoComprobante, Comprobante, Fecha, Anulado, TotalEfectivo, TotalDocumento, TotalAnulados, TotalCobranzaExtra, TotalCheque, TotalChequeAlDiaBancoLocal, TotalChequeAlDiaOtrosBancos, TotalChequeDiferido, Total)
		Values(@IDTransaccionSalida, @IDTransaccionLote, @IDSucursalOperacion, @Numero, @IDTipoComprobante, @Comprobante, @Fecha, 'False', @TotalEfectivo, @TotalDocumento, @TotalAnulados, @TotalCobranzaExtra, @TotalCheque, @TotalChequeAlDiaBancoLocal, @TotalChequeAlDiaOtrosBancos, @TotalChequeDiferido, @Total)
		
		If @UsuarioEsChofer = 'True' Begin
			
			Declare @vNumero int
			Set @vNumero = IsNull((Select Max(Numero) + 1 From RendicionLoteChofer Where IDChofer=@IDChofer),1)
			Insert Into RendicionLoteChofer(IDTransaccion, IDChofer, Numero)
			Values(@IDTransaccionSalida, @IDChofer, @vNumero)

		End
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		
		Return @@rowcount
		
	End
	
	If @Operacion = 'ANULAR' Begin
		
		--Validar
		--Que exista el registro
		if Not Exists(Select * From RendicionLote Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra el registro!'
			Set @Procesado = 'False'
			Return @@rowcount
		End
		
		--Que ya no este anulado el registro
		if Exists(Select * From RendicionLote Where IDTransaccion=@IDTransaccion And Anulado='True') Begin
			Set @Mensaje = 'El registro ya esta anulado!'
			Set @Procesado = 'False'
			Return @@rowcount
		End
		
				
		
		--Anular
		Update RendicionLote Set Anulado='True',
								 FechaAnulado=GETDATE(),
								 IDUsuarioAnulado=@IDUsuario
		Where IDTransaccion=@IDTransaccion
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		Return @@rowcount
		
	End
	
	If @Operacion = 'DEL' Begin
		
		
		Set @Mensaje = 'Registro procesado!'
		Set @Procesado = 'false'
		Return @@rowcount
		
	End
		
	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'
	
End



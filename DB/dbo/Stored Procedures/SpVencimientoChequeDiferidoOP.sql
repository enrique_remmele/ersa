﻿
CREATE Procedure [dbo].[SpVencimientoChequeDiferidoOP]

	--Entrada OP
	@IDTransaccion numeric(18,0) = NULL,
	@IDTransaccionOP numeric(18,0) = NULL,
	@ChequeEntregado bit = NULL,
	@Fecha date = NULL,
	
	--Operacion
	@Operacion varchar(50),
	--Transaccion
	@IDOperacion tinyint = 60,
	@IDUsuario smallint = 1,
	@IDSucursal tinyint = 1,
	@IDDeposito tinyint = 1,
	@IDTerminal int = 1,
	
	--Salida
	@Mensaje varchar(200) = '' output ,
	@Procesado bit = 'False' output ,
	@IDTransaccionSalida numeric(18,0) = 0 output
	
	
As

Begin
	if @Operacion = 'ANULAR' begin
		Set @Fecha = (select Fecha from vVencimientoChequeEmitido where IDTransaccion =@IDTransaccion)
	end
	--Validar Feha Operacion
	If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
		Set @Mensaje  = 'Fecha fuera de rango permitido!!'
		Set @Procesado = 'False'
		set @IDTransaccionSalida  = 0
		Return @@rowcount
	End

	If @Operacion = 'INS' Begin
	
		--Si NO existe el registro
		If Not Exists(Select * From OrdenPago Where IDTransaccion=@IDTransaccionOP) Begin
			set @Mensaje = 'El registro no existe!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Si NO existe el registro
		If Not Exists(Select * From vOrdenPago Where IDTransaccion=@IDTransaccionOP and Diferido <> 0) Begin
			set @Mensaje = 'El OP no es con cheque diferido!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Si NO existe el registro
		If Not Exists(Select * From OrdenPago Where IDTransaccion=@IDTransaccionOP and IDTransaccion in (select idtransaccionOP from EntregaChequeOP where Anulado = 0)) Begin
			set @Mensaje = 'El cheque de la OP aun no fue entregado!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Insertar Transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
			
	
		--Insertar en Orden de Pago
		Insert Into VencimientoChequeEmitido(IDTransaccion, IDTransaccionOP, Fecha, Anulado, Procesado)
		Values(@IDTransaccionSalida, @IDTransaccionOP, @Fecha, 'False', 'True')
 
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount	
	
	End

	If @Operacion = 'UPD' Begin
	
		--Si NO existe la OP
		If Not Exists(Select * From OrdenPago Where IDTransaccion=@IDTransaccionOP) Begin
			set @Mensaje = 'El registro no existe!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Si NO existe el registro
		If Not Exists(Select * From VencimientoChequeEmitido Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El registro no existe!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Actualizar
		Update VencimientoChequeEmitido Set	Fecha = @Fecha		
		Where IDTransaccion = @IDTransaccion

		--Actualizar fecha del asiento
		Update Asiento Set	Fecha = @Fecha		
		Where IDTransaccion = @IDTransaccion

		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount	
	
	End
	
	If @Operacion = 'ANULAR' Begin
	
		--Validar
		--Asientos Cerrados
		If Exists(Select * From Asiento A Where A.IDTransaccion=@IDTransaccion And A.Conciliado='True') Begin
			set @Mensaje = 'El asiento de este documento ya esta conciliado! No se puede anular.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
			
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal, @IDTerminal=@IDTerminal
		
		--Anulamos el Registro
		Delete VencimientoChequeEmitido Where IDTransaccion = @IDTransaccion
				
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount	
		
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
	
	
End

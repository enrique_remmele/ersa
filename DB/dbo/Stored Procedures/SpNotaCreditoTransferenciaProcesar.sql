﻿
 CREATE Procedure [dbo].[SpNotaCreditoTransferenciaProcesar]

	--Entrada
	@IDTransaccion numeric(18, 0),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	--Variable
	Declare @vIDTransaccionVenta numeric(18,0)
	Declare @vIDTransaccionNotaCredito numeric (18,0)
	Declare @vIDTransaccionTransferenciaNotaCredito numeric (18,0)
	Declare @vIDTransaccionNotaCreditoAplicacion numeric (18,0)
	
	Declare @vDescontado money -- variable para recuperar si tiene alguna NC a factura	
	Declare @vSaldo money
	Declare @vSaldoNotaCredito money
	Declare @vTotalNotaCredito money
	Declare @vImporte money
	Declare @vCancelado bit
	Declare @vEntrante bit
	Declare @vSaliente bit

		
	Set @Mensaje = ''
	Set @Procesado = 'False'
	
	If @Operacion = 'INS' Begin
		
		--Actualizacion de Ventas
		Begin
				
			Declare db_cursor cursor for
			
			Select IDTransaccionVenta, Importe, IDTransaccionTransferenciaNotaCredito, TNC.IDTransaccionNotaCredito, Entrante, Saliente 
			From DetalleTransferenciaNotaCredito DTNC
				Join TransferenciaNotaCredito TNC On DTNC.IDTransaccionTransferenciaNotaCredito=TNC.IDTransaccion
			Where TNC.IDTransaccion=@IDTransaccion 
				And IDTransaccionVenta Is Not Null 
				And IDTransaccionTransferenciaNotaCredito=TNC.IDTransaccion
			Order By Saliente Desc			
			
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionVenta, @vImporte, @vIDTransaccionTransferenciaNotaCredito,@vIDTransaccionNotaCredito,  @vEntrante, @vSaliente
			While @@FETCH_STATUS = 0 Begin 
			 
				--Hayar Valores
				
				If @vSaliente = 'True' Begin
					--Saldo
					Set @vSaldo = (Select Saldo From Venta Where IDTransaccion=@vIDTransaccionVenta)
					Set @vSaldo = @vSaldo + @vImporte
					
					--Cancelado
					Set @vCancelado = 'False'
					
					Delete NotaCreditoVenta 
					Where IDTransaccionNotaCredito=@vIDTransaccionNotaCredito And IDTransaccionVentaGenerada=@vIDTransaccionVenta 
					
					--Descontado
					Set @vDescontado = (Select Descontado From Venta Where IDTransaccion=@vIDTransaccionVenta)
					set @vDescontado = @vDescontado - @vImporte
				End
				
				--Entrante
				If @vEntrante = 'True' Begin
					--Saldo
					Set @vSaldo = (Select Saldo From Venta Where IDTransaccion=@vIDTransaccionVenta)
					Set @vSaldo = @vSaldo - @vImporte
				
					--Cancelado
					Set @vCancelado = 'False'
				
					If @vSaldo = 0 Begin
						Set @vCancelado = 'True'
					End

					--Descontado
					Set @vDescontado = (Select Descontado From Venta Where IDTransaccion=@vIDTransaccionVenta)
					set @vDescontado = @vDescontado + @vImporte
					
					Insert Into  NotaCreditoVenta (IDTransaccionNotaCredito, IDTransaccionVentaGenerada , ID, Importe, cobrado , Descontado, Saldo  )
							Values (@vIDTransaccionNotaCredito, @vIDTransaccionVenta, 0, @vImporte, 0, 0, 0) 
				End 
				
				--Actualizar Venta 
				Update Venta Set	Saldo=@vSaldo,
									Cancelado = @vCancelado,
									Descontado = @vDescontado
				Where IDTransaccion=@vIDTransaccionVenta
				
				Fetch Next From db_cursor Into @vIDTransaccionVenta, @vImporte, @vIDTransaccionTransferenciaNotaCredito,@vIDTransaccionNotaCredito, @vEntrante, @vSaliente
			                  								
			End
		   	 
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor
			
		End
				
		----Nuevo Cursor para Actualizar NotacreditoventaAplicada
		--Begin
		--	--Obtenemos el IDtransaccion de Notacredito 
		--	Set @vIDTransaccionNotaCredito = (Select IDTransaccionNotaCredito From TransferenciaNotaCredito Where IDTransaccion=@IDTransaccion )
		--	Declare db_cursor cursor for
		--	Select  IDTransaccionVenta, Entrante, Saliente, Importe, IDtransaccionNotaCreditoAplicacion 
		--	From DetalleTransferenciaNotaCredito
		--	Where IDTransaccionTransferenciaNotaCredito=@IDTransaccion
		--	Open db_cursor   
		--	Fetch Next From db_cursor Into  @vIDTransaccionVenta , @vEntrante, @vSAliente, @vImporte, @vIDTransaccionNotaCreditoAplicacion
		--	While @@FETCH_STATUS = 0 Begin 
				
		--		--Insertar en Nota credito venta Aplicada
		--		if @vSaliente = 'True' begin
				
		--				--Actualizar NotaCreditoVentaAplicada
		--				Update NotaCreditoVentaAplicada  Set	Transferido = 'True'
		--				Where	IDTransaccionNotaCredito=@vIDTransaccionNotaCredito And
		--						IDTransaccionVenta=@vIDTransaccionVenta And
		--						IDTransaccionNotaCreditoAplicacion=@vIDTransaccionNotaCreditoAplicacion
		--		End
				
		--		---Insertamos en TransferenciaNotaCreditoVenta
		--		if @vEntrante = 'True' Begin
		--				Insert Into TransferenciaNotaCreditoVenta(IDTransaccion, IDTransaccionVenta, IDTransaccionNotaCredito, Importe, IDTransaccionNotaCreditoAplicada) 
		--				Values (@IDTransaccion, @vIDtransaccionVenta, @vIDTransaccionNotaCredito, @vImporte,@vIDTransaccionNotaCreditoAplicacion )	 
		--		End
						 
		--		Fetch Next From db_cursor Into  @vIDTransaccionVenta , @vEntrante, @vSAliente, @vImporte, @vIDTransaccionNotaCreditoAplicacion
			
		--	End
			
		--	--Cierra el cursor
		--	Close db_cursor   
		--	Deallocate db_cursor
		
		--End
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
		
		
End
	






﻿CREATE Procedure [dbo].[SpCierreContable]
	
	--Entrada
	@Operacion varchar(50),
	@Fecha date = Null ,
	@Mes smallint = NUll,
	@Año int = Null,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	

As

Begin
	
	If @Operacion = 'CIERREDIARIO' Begin
	
		
		--Actualiza asiento
		Update Asiento Set Conciliado = 'True'
		Where Fecha <= @Fecha 
					       
		Set @Mensaje = 'Registro procesado!'
		Set @Procesado = 'True'	
		Return @@rowcount
		
	End 
	
	If @Operacion = 'DESCONCILIACIONDIARIO' Begin
	
		
		--Actualiza asiento
		Update Asiento Set Conciliado = 'False'
		Where Fecha >= @Fecha 
					     
		if @@rowcount = 0 Begin
			Set @Mensaje = 'No se proceso ningun registro!'
			Set @Procesado = 'False'	
			Return @@rowcount
		End
					       
		Set @Mensaje = 'Registro procesado!'
		Set @Procesado = 'True'	
		Return @@rowcount
		
	End 
	
	If @Operacion = 'CIERREMENSUAL' Begin
	
		--Actualiza asiento
		Update Asiento Set Conciliado = 'True'
		Where DatePart(M, Fecha) = @Mes And DatePart(YYYY, Fecha) = @Año
					       
		Set @Mensaje = 'Registro procesado!'
		Set @Procesado = 'True'	
		Return @@rowcount
		
	End 
	
	If @Operacion = 'CIERREDELEJERCICCIO' Begin
	
		--Actualiza asiento
		Update Asiento Set Conciliado = 'True'
		Where  DatePart(YYYY, Fecha) = @Año
					       
		Set @Mensaje = 'Registro procesado!'
		Set @Procesado = 'True'	
		Return @@rowcount
		
	End 
	
End


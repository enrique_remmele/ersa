﻿
CREATE Procedure [dbo].[SpRegenerarAsientoOrdenPago]

	@Desde date,
	@Hasta date,
	@Todos bit = 'False',
	@SoloConDiferencias bit = 'False',
	@IDSucursal tinyint
	
As

Begin
	
	--Variables
	Begin
		Declare @vIDTransaccion numeric(18,0)	
		Declare @vAnulado bit
		Declare @vComprobante varchar(50)	
		Declare @vCantidad int
		Declare @vRegistrosProcesados int
		Declare @vPorcentaje int
		Declare @vMateriaPrima bit			
	End
	
	If Not Exists (Select * From sys.objects Where object_id = OBJECT_ID(N'[dbo].[GeneracionAsientoNotaCredito]') And type in (N'U')) Begin
		Create Table GeneracionAsientoNotaCredito(ID int, Cantidad int, RegistrosProcesados int, Porcentaje int)
	End
	
	Delete From GeneracionAsientoNotaCredito
	
	Set @vCantidad = (Select Count(*) From VOrdenPago V Where V.Anulado = 0 and V.Fecha Between @Desde And @Hasta And V.IDSucursal=@IDSucursal And (Case When @SoloConDiferencias = 0 Then 1 Else (Case When (Select A.Saldo From Asiento A Where A.IDTransaccion=V.IDTransaccion)<>0 Then 1 Else 0 End) End) = 1)
		
	Set @vRegistrosProcesados = 0
	Set @vPorcentaje = 0
	
	Insert Into GeneracionAsientoNotaCredito(ID, Cantidad, RegistrosProcesados, Porcentaje)
	Values(1, @vCantidad, 0, 0)

	Begin
		
		Declare cMovimiento cursor for
		Select V.IDTransaccion, V.Anulado
		From VOrdenPago V Where  V.Fecha Between @Desde And @Hasta 
		And V.IDSucursal=@IDSucursal
		And (Case When @SoloConDiferencias = 0 Then 1 Else (Case When (Select A.Saldo From Asiento A Where A.IDTransaccion=V.IDTransaccion)<>0 Then 1 Else 0 End) End) = 1
		Open cMovimiento 
		fetch next from cMovimiento into @vIDTransaccion, @vAnulado

		While @@FETCH_STATUS = 0 Begin
		
			if @vAnulado = 1 begin
				delete from detalleasiento where idtransaccion = @vIDTransaccion
				delete from asiento where idtransaccion = @vIDTransaccion
				goto Siguiente
			end 
			
			print @vIDTransaccion
			Exec SpAsientoOrdenPago @vIDTransaccion		
			
			--Contador
			Set @vRegistrosProcesados = @vRegistrosProcesados + 1
			Set @vPorcentaje = (@vRegistrosProcesados / @vCantidad) * 100
			
			Update GeneracionAsientoNotaCredito Set RegistrosProcesados=@vRegistrosProcesados,
														Porcentaje=@vPorcentaje
			Where ID=1
							
Siguiente:
			fetch next from cMovimiento into @vIDTransaccion, @vAnulado
			
		End   
		
		close cMovimiento
		deallocate cMovimiento
	
	End
	
	Select 'Mensaje'='Registros procesados: ' + CONVERT(varchar(50), @vCantidad), 'Procesado'='True'
	
End

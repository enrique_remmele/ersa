﻿CREATE Procedure [dbo].[SpNotaCreditoProveedorActualizarCompra]

	--Entrada
	@IDTransaccion numeric(18, 0),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	--Variable
	Declare @vIDTransaccionEgreso numeric(18,0)
	Declare @vDescontado money
	Declare @vSaldo money
	Declare @vImporte money
	Declare @vCancelado bit
	
	Set @Mensaje = ''
	Set @Procesado = 'False'
	
	If @Operacion = 'INS' Begin
	
	--Saldar Compras
	if exists (Select * from NotaCreditoProveedorCompra NCPC Join Compra C On NCPC.IDTransaccionEgreso=C.IDTransaccion Where NCPC.IDTransaccionNotaCreditoProveedor=@IDTransaccion ) Begin
		Begin		
		Declare db_cursor cursor for
		Select  IDTransaccionEgreso,Importe From NotaCreditoProveedorCompra 
		Where IDTransaccionNotaCreditoProveedor=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte
		While @@FETCH_STATUS = 0 Begin 
		 
			--Saldo
			Set @vSaldo = (Select Saldo From Compra Where IDTransaccion=@vIDTransaccionEgreso)
			Set @vSaldo = @vSaldo - @vImporte
			
			--Cancelado
			Set @vCancelado = 'False'
			
			If @vSaldo = 0 Begin
				Set @vCancelado = 'True'
			End
			
			--Actualizar Compras
			Update Compra  Set Saldo=@vSaldo,
							Cancelado=@vCancelado 
			Where IDTransaccion=@vIDTransaccionEgreso 
			
			Fetch Next From db_cursor Into @vIDTransaccionEgreso , @vImporte
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		end
	End	
	--Saldar Gastos
	if exists (Select * from NotaCreditoProveedorCompra NCPC Join Gasto  G On NCPC.IDTransaccionEgreso=G.IDTransaccion Where NCPC.IDTransaccionNotaCreditoProveedor=@IDTransaccion ) Begin
				
		Declare db_cursor cursor for
		Select  IDTransaccionEgreso,Importe From NotaCreditoProveedorCompra 
		Where  IDTransaccionNotaCreditoProveedor=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte
		While @@FETCH_STATUS = 0 Begin 
		 
			--Saldo
			Set @vSaldo = (Select Saldo From Gasto  Where IDTransaccion=@vIDTransaccionEgreso)
			Set @vSaldo = @vSaldo - @vImporte
			--Cancelado
			Set @vCancelado = 'False'
			
			If @vSaldo = 0 Begin
				Set @vCancelado = 'True'
			End
			
			--Actualizar Gastos
			Update Gasto Set Saldo=@vSaldo,
							Cancelado=@vCancelado 
			Where IDTransaccion=@vIDTransaccionEgreso 
			
			Fetch Next From db_cursor Into @vIDTransaccionEgreso , @vImporte
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		end
	End	
	
	If @Operacion = 'DEL' Begin
	
		--Saldar Compras
		If exists (Select * from NotaCreditoProveedorCompra NCPC Join Compra C On NCPC.IDTransaccionEgreso=C.IDTransaccion Where NCPC.IDTransaccionNotaCreditoProveedor=@IDTransaccion ) Begin
				
			Declare db_cursor cursor for
			Select  IDTransaccionEgreso,Importe From NotaCreditoProveedorCompra 
			Where  IDTransaccionNotaCreditoProveedor=@IDTransaccion
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte
			While @@FETCH_STATUS = 0 Begin 
			 
				--Saldo
				Set @vSaldo = (Select Saldo From Compra Where IDTransaccion=@vIDTransaccionEgreso)
				Set @vSaldo = @vSaldo + @vImporte
				
				--Cancelado
				Set @vCancelado = 'False'
							
				--Actualizar Compras
				Update Compra  Set Saldo=@vSaldo,
									Cancelado=@vCancelado 
				Where IDTransaccion=@vIDTransaccionEgreso 
				
				Fetch Next From db_cursor Into @vIDTransaccionEgreso , @vImporte
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor
			
			Set @Mensaje = 'Registro guardado'
			Set @Procesado = 'True'
			return @@rowcount
		End
	
		--Saldar Gastos
		If exists (Select * from NotaCreditoProveedorCompra NCPC Join Gasto  G On NCPC.IDTransaccionEgreso=G.IDTransaccion Where NCPC.IDTransaccionNotaCreditoProveedor=@IDTransaccion) Begin
					
			Declare db_cursor cursor for
			Select  IDTransaccionEgreso,Importe From NotaCreditoProveedorCompra 
			Where  IDTransaccionNotaCreditoProveedor=@IDTransaccion
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionEgreso, @vImporte
			While @@FETCH_STATUS = 0 Begin 
			 
				--Saldo
				Set @vSaldo = (Select Saldo From Gasto Where IDTransaccion=@vIDTransaccionEgreso)
				Set @vSaldo = @vSaldo + @vImporte
				
				--Cancelado
				Set @vCancelado = 'False'
				
				
				--Actualizar Gastos
				Update Gasto Set Saldo=@vSaldo,
								Cancelado=@vCancelado 
				Where IDTransaccion=@vIDTransaccionEgreso 
				
				Fetch Next From db_cursor Into @vIDTransaccionEgreso , @vImporte
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor
			
			Set @Mensaje = 'Registro guardado'
			Set @Procesado = 'True'
			return @@rowcount
		End
		
	End	
	
	
End
	










﻿
CREATE Procedure [dbo].[SpRecalcularKardex]
	
	@ID int,
	@FechaInicio Date,
	@Operacion varchar(20) = 'PRODUCTO'

As

Begin
	
	--Declaracion de variables
	Declare @vIDProducto int
	Declare @vMensajeSalida varchar(500)
	--Bloquear Balance mientras dure el proceso
	Update KardexBloquearOperaciones
	set BloquearBalance = 1
	
	if @Operacion = 'PRODUCTO' begin
		Set @vIDProducto = @ID
		 
		-- begin try 
			Exec SpRecalcularKardexProducto @IDProducto = @vIDProducto, @FechaInicio = @FechaInicio
			exec SpProcesarKardex @IDProducto = @vIDProducto, @FechaInicio = @FechaInicio
			exec SpProcesarNOKardex @IDProducto = @vIDProducto, @FechaInicio = @FechaInicio
		 --end try

		 --begin catch
			--  Set @vMensajeSalida =concat('IDProducto=', @vIDProducto, ' Mensaje=', convert(varchar,ERROR_LINE()))
			--  goto Salir
		 --end catch    

		Set @vMensajeSalida= 'Proceso realizado con exito'

	end

	if @Operacion = 'TIPOPRODUCTO' Begin
		
			Declare db_cursorKardexProductos cursor for
			Select Distinct	IDProducto
			From VKardex
			where IDTipoProducto = @ID

			Open db_cursorKardexProductos
		
			Fetch Next From db_cursorKardexProductos Into @vIDProducto
			While @@FETCH_STATUS = 0 Begin 
			
				--begin try 
					Exec SpRecalcularKardexProducto @IDProducto = @vIDProducto, @FechaInicio = @FechaInicio
					exec SpProcesarKardex @IDProducto = @vIDProducto, @FechaInicio = @FechaInicio
					exec SpProcesarNOKardex @IDProducto = @vIDProducto, @FechaInicio = @FechaInicio
				-- end try

				 --begin catch
					--  Set @vMensajeSalida =concat('IDProducto=', @vIDProducto, ' Mensaje=', convert(varchar,ERROR_LINE()))
					--  goto Salir
				 --end catch    

				Fetch Next From db_cursorKardexProductos Into @vIDProducto
		
			End
		
			--Cierra el cursor
			Close db_cursorKardexProductos   
			Deallocate db_cursorKardexProductos
			Set @vMensajeSalida= 'Proceso realizado con exito'
		End		

		if @Operacion = 'TODOS' Begin
		
			Declare db_cursorKardexProductos cursor for
			Select Distinct	IDProducto
			From VKardex
			
			Open db_cursorKardexProductos
		
			Fetch Next From db_cursorKardexProductos Into @vIDProducto
			While @@FETCH_STATUS = 0 Begin 

				----begin try 
					Exec SpRecalcularKardexProducto @IDProducto = @vIDProducto, @FechaInicio = @FechaInicio
					exec SpProcesarKardex @IDProducto = @vIDProducto, @FechaInicio = @FechaInicio
					exec SpProcesarNOKardex @IDProducto = @vIDProducto, @FechaInicio = @FechaInicio
				 --end try

				 --begin catch
					--  Set @vMensajeSalida =concat('IDProducto=', @vIDProducto, ' Mensaje=', convert(varchar,ERROR_LINE()))
					--  goto Salir
				 --end catch    

				Fetch Next From db_cursorKardexProductos Into @vIDProducto
		
			End
		
			--Cierra el cursor
			Close db_cursorKardexProductos   
			Deallocate db_cursorKardexProductos
			Set @vMensajeSalida= 'Proceso realizado con exito'
		End		

Salir:
	--Desbloquear Balance
	Update KardexBloquearOperaciones
	set BloquearBalance = 0
	Select @vMensajeSalida

End

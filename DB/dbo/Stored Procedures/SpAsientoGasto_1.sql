﻿CREATE Procedure [dbo].[SpAsientoGasto]

	@IDTransaccion numeric(18,0)
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint = 0

	--Gasto
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vContado bit
	Declare @vCredito bit
	Declare @vCondicion varchar(50)
	Declare @vFecha date
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money
	Declare @vCuentaContableGasto varchar(50)
	Declare @vCuentaContableCosto varchar(50)
	Declare @vTotalDiscriminado money
	Declare @vTotalDescuentoDiscriminado money
	Declare @vTotalCosto money
	Declare @vFactorimpuesto decimal(18,7)
	Declare @vIDProducto int
	Declare @vIDTipoProducto int
	Declare @vTotal money
	Declare @vTotalImpuesto money
	Declare @vObservacion varchar(50)

	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	
	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money

	Declare @vProveedor as varchar(32)
	Declare @vIDProveedor as int
	
	--Obtener valores
	Begin
	
		Select	@vIDSucursal=IDSucursal,
				@vTipoComprobante=TipoComprobante,
				@vIDTipoComprobante=IDTipoComprobante,
				@vComprobante=Comprobante,
				@vCredito=Credito,
				@vFecha=Fecha,
				@vIDMoneda=IDMoneda,
				@vCotizacion=Cotizacion,
				@vProveedor=Proveedor,
				@vIDProveedor=IDProveedor,
				@vTotal=Total,
				@vTotalImpuesto=TotalImpuesto,
				@vObservacion = Observacion
		From VGasto Where IDTransaccion=@IDTransaccion
				
	End
					
	--Verificar que el asiento se pueda modificar
	Begin
		
		If not exists(Select * From Gasto Where IDTransaccion=@IDTransaccion) Begin
			print 'La operacion no es una Gasto'
			GoTo salir
		End 	

		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		
	End
				
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		--Set @vDetalleAsiento = @vTipoComprobante + ' ' + @vComprobante + ' ' + @vCondicion + ' - ' + @vProveedor
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vObservacion, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	
	--Cuentas Fijas de Proveedors
	Begin
	
		--Variables
		Declare @vGastoCredito bit
		Declare @vGastoContado bit
		Declare @vCodigoCuentaProveedor varchar(50)
		Declare @vTotalProducto money

		Declare cCFGastoProveedor cursor for
			Select Top(1) Codigo,Debe,Haber 
			From vcfGasto
			Where IDMoneda = 1 and BuscarEnProveedor = 'True' and Codigo = '21010101'
		Open cCFGastoProveedor   
		fetch next from cCFGastoProveedor into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			set @vCodigo = '11010106'
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0

			--Obtener la cuenta
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			Set @vImporteHaber = @vTotal * @vCotizacion
				
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin				
				--Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				--Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo) , 0, '')
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End
			End
			
			fetch next from cCFGastoProveedor into @vCodigo, @vDebe,@vHaber
		End
		close cCFGastoProveedor 
		deallocate cCFGastoProveedor
	End
					
	--Cuentas Fijas Impuesto
	Begin
		
		--Variables
		Declare @vIDImpuesto tinyint
		
		Declare cCFGastoImpuesto cursor for
		Select distinct Codigo, IDImpuesto, Debe, Haber from VCFGasto 
		Where TipoCuentaFija = 'IMPUESTO' 
		And IDSucursal=@vIDSucursal 
		And IDMoneda=@vIDMoneda
		
		Open cCFGastoImpuesto   
		fetch next from cCFGastoImpuesto into @vCodigo, @vIDImpuesto, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vCodigo = '11050103'	
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			
			--Set @vImporte = (Select SUM(TotalImpuesto * @vCotizacion) From DetalleImpuesto Where IDTransaccion=@IDTransaccion And IDImpuesto=@vIDImpuesto)
			Set @vImporte = (select sum(TotalImpuesto * @vCotizacion)  from VDetalleImpuesto where IDTransaccion = @IDTransaccion and IDImpuesto = @vIDImpuesto)
			
			print concat(@vIDImpuesto, ' ', @vImporte)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
										Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End
			End
			
			fetch next from cCFGastoImpuesto into @vCodigo, @vIDImpuesto, @vDebe, @vHaber
			
		End
		
		close cCFGastoImpuesto 
		deallocate cCFGastoImpuesto
	End					

	--Cuentas Fijas de Costo de Mercaderia
	Begin
	
		--Cuenta Fija Gasto
		Set @vCuentaContableGasto = ''
		Set @vTotalCosto = 0
			
		Declare cDetalleCosto cursor for
		Select Codigo
		From CuentaContable
		Where Codigo = '541010303'
		Open cDetalleCosto
		fetch next from cDetalleCosto into @vCodigo
		
		While @@FETCH_STATUS = 0 Begin  
			
			set @vDebe = 1
			set @vHaber = 0
			set @vImporte = 0
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			declare @vimpuesto money
			set @vimpuesto = (select sum(TotalImpuesto * @vCotizacion)  from VDetalleImpuesto where IDTransaccion = @IDTransaccion )
			--Hayar el importe	
			Set @vImporte = (@vTotal * @vCotizacion) - @vimpuesto
			print @vimporte
			print @vimpuesto
			Set @vImporteDebe = @vImporte
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			
			--Cargamod
			If @vImporteHaber > 0 Or @vImporteDebe > 0 Begin
			
				--Si existe la cuenta, actualizar
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
										Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End
			End
							
			fetch next from cDetalleCosto into @vCodigo
			
		End
		
		close cDetalleCosto 
		deallocate cDetalleCosto
		
	End

	--Actualizamos la cabecera, el total
	Begin
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
		Set @vImporteHaber = ROUND(@vImporteHaber, @vRedondeo)
		Set @vImporteDebe = ROUND(@vImporteDebe, @vRedondeo)
	
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vImporteHaber - @vImporteDebe
		Where IDTransaccion=@IDTransaccion
	End

	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
	
End

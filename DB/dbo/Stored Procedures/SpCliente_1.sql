﻿CREATE Procedure [dbo].[SpCliente]

	--Entrada
	@ID int,
	
	--Datos Obligatorios
	@RazonSocial varchar(50),
	@RUC varchar(15),
	@CI bit = NULL,
	@Referencia varchar(50),
	@Operacion varchar(10),
		
	--Datos Comerciales
	@NombreFantasia varchar(50) = NULL,
	@Direccion varchar(100) = NULL,
	@Telefono varchar(20) = NULL,
	@Celular varchar(20) = null,
	@IDEstado tinyint = NULL,
	
	--Localizacion
	@IDPais tinyint = NULL,
	@IDDepartamento tinyint = NULL,
	@IDCiudad smallint = NULL,
	@IDBarrio smallint = NULL,
	@IDZonaVenta tinyint = NULL,
	
	@IDArea tinyint = NULL,
	@IDRuta int = NULL,	


	--Configuraciones
	@IDListaPrecio tinyint = NULL,
	@IDTipoCliente tinyint = NULL,
	@IDCategoriaCliente tinyint = NULL,
	@Contado bit = NULL,
	@Credito bit = NULL,
	@IDMoneda tinyint = NULL,
    @IVAExento bit = NULL,
	@CodigoCuentaContable varchar(50) = NULL,
	        		 
	--De Referencias
	@IDSucursal tinyint = NULL,
	@IDPromotor tinyint = NULL,
	@IDVendedor tinyint = NULL,
	@IDCobrador tinyint = NULL,
	@IDDistribuidor tinyint = NULL,

	--Credito
	@LimiteCredito money = NULL,
	@Descuento money = NULL,
	@PlazoCredito integer = NULL,
	@PlazoCobro integer = NULL,
	@PlazoChequeDiferido integer = NULL,
	@Boleta bit = 'False',
	
	--Adicionales
	@PaginaWeb varchar(50) = NULL,
	@Email varchar(50) = NULL,
	@Fax varchar(20) = NULL,
	@Aniversario Date = NULL,
	@IDClienteProducto int = NULL,

	--Localizacion
	--Clientes varios
	@ClienteVario bit = NULL,

	--Judicial
	@IDAbogado int = null,
	@Judicial bit = False,
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As
	
Begin

	--BLOQUES
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'

	if @Celular is null begin
	set @Celular = '-'
	end

	IF @Judicial = 'False' begin
	 set @IDAbogado = null
	End

	Set @RazonSocial = Replace(@RazonSocial, CHAR(39), '´')
	Set @Direccion = Replace(@Direccion, CHAR(39), '´')
	Set @NombreFantasia = Replace(@NombreFantasia, CHAR(39), '´')

	--INSERTAR
	If @Operacion='INS' Begin
	
		--Validar
		Begin
			--Si es que el RUC ya existe
			If CHARINDEX('X', @RUC, 0) = 0 And CHARINDEX('x', @RUC, 0) = 0 Begin
				If Exists(Select * From Cliente Where RUC=@RUC And @RUC!='' And @RUC!=' ' And @RUC!='44444401-7' And @RUC!='77777701-0' And @RUC!='88888801-5') Begin
					Set @Mensaje = 'El RUC ya existe!'
					Set @Procesado = 'False'
					return @@rowcount
				End
			End
			
			--Si es que la Referencia ya existe
			If Exists(Select * From Cliente Where Referencia=@Referencia) Begin
				Set @Mensaje = 'La referencia ya existe!'
				Set @Procesado = 'False'
				return @@rowcount
			End
			
			--Sucursal
			If @IDSucursal Is Null Begin
				Set @Mensaje = 'Seleccione una sucursal!'
				Set @Procesado = 'False'
				return @@rowcount
			End
			
			--Lista de precio
			If @IDListaPrecio Is Null Begin
				Set @Mensaje = 'Seleccione una Lista de precio!'
				Set @Procesado = 'False'
				return @@rowcount
			End

			--Lista de precio
			If @IDVendedor Is Null Begin
				Set @Mensaje = 'Seleccione un Vendedor!'
				Set @Procesado = 'False'
				return @@rowcount
			End

			--Barrio
			If @IDBarrio Is Null Begin
				Set @Mensaje = 'Seleccione un Barrio!'
				Set @Procesado = 'False'
				return @@rowcount
			End
			--Zona de venta
			If @IDZonaVenta Is Null Begin
				Set @Mensaje = 'Seleccione una Zona!'
				Set @Procesado = 'False'
				return @@rowcount
			End
			--Area
			If @IDArea Is Null Begin
				Set @Mensaje = 'Seleccione un Area!'
				Set @Procesado = 'False'
				return @@rowcount
			End
		End
		
		--Si el cliente es nuevo y contado, datos de credito tiene que ser 0
		If @Contado = 'True' Begin
			Set @LimiteCredito = 0
			Set @Descuento = 0
			Set @PlazoCredito = 0
			Set @PlazoCobro = 0
			Set @PlazoChequeDiferido = 0
		End

				
		--Solo Crear el Registro con los datos mas importantes,
		--Luego pasamos a actualizar!
		Declare @vID int
		Set @vID = (Select ISNULL(Max(ID)+1, 1) From Cliente)
		
		Insert Into Cliente(ID, RazonSocial, RUC, Referencia, FechaAlta, IDUsuarioAlta, ClienteVario, Judicial, IDAbogado, Boleta)
		values(@vID, @RazonSocial, @RUC, @Referencia, GETDATE(), @IDUsuario, @ClienteVario, @Judicial, @IDAbogado, @Boleta)
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CLIENTE', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		--Actualizamos el registro
		Set @Operacion='UPD'
		Set @ID = @vID		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		
	End
	
	--ACTUALIZAR
	If @Operacion='UPD' Begin
	
		--Validar
		Begin
			
			--Si es que no existe el Cliente
			If Not Exists(Select * From Cliente Where ID=@ID) Begin
				Set @Mensaje = 'El sistema no encuentra el registro!'
				Set @Procesado = 'False'
				return @@rowcount
			End
			
			--Si es que el RUC ya existe - MOMENTANEAMENTE SE PUEDE REPETIR EL RUC!!!!
			If CHARINDEX('X', @RUC, 0) = 0 And CHARINDEX('x', @RUC, 0) = 0  Begin
				If Exists(Select * From Cliente Where RUC=@RUC And ID!=@ID And @RUC!='' And @RUC!='44444401-7' And @RUC!='77777701-0' And @RUC!='88888801-5') Begin
					Set @Mensaje = 'El RUC ya existe.....!'
					Set @Procesado = 'False'
					return @@rowcount
				End
			End
			
			--Si es que el RUC ya existe
			If Exists(Select * From Cliente Where Referencia=@Referencia And ID!=@ID) Begin
				Set @Mensaje = 'La referencia ya existe!'
				Set @Procesado = 'False'
				return @@rowcount
			End
		
			--Sucursal
			If @IDSucursal Is Null Begin
				Set @Mensaje = 'Seleccione una sucursal!'
				Set @Procesado = 'False'
				return @@rowcount
			End
			
			--Lista de precio
			If @IDListaPrecio Is Null Begin
				Set @Mensaje = 'Seleccione una Lista de precio!'
				Set @Procesado = 'False'
				return @@rowcount
			End

			--Lista de precio
			If @IDVendedor Is Null Begin
				Set @Mensaje = 'Seleccione un Vendedor!'
				Set @Procesado = 'False'
				return @@rowcount
			End
			--Barrio
			If @IDBarrio Is Null Begin
				Set @Mensaje = 'Seleccione un Barrio!'
				Set @Procesado = 'False'
				return @@rowcount
			End
			--Zona de venta
			If @IDZonaVenta Is Null Begin
				Set @Mensaje = 'Seleccione una Zona!'
				Set @Procesado = 'False'
				return @@rowcount
			End
			--Area
			If @IDArea Is Null Begin
				Set @Mensaje = 'Seleccione un Area!'
				Set @Procesado = 'False'
				return @@rowcount
			End
		End

		--Auditoria de cambios de clientes
		
		
		IF ((Select ISnull(LimiteCredito,0) from cliente where ID=@ID) <> @LimiteCredito) or
		((Select ISnull(IDListaPrecio,0) from cliente where ID=@ID) <> @IDListaPrecio) or
		((Select ISnull(IDVendedor,0) from cliente where ID=@ID) <> @IDVendedor) or 
		((Select ISnull(IDTipoCliente,0) from cliente where ID=@ID) <> @IDTipoCliente) or
		((Select ISnull(PlazoCredito,0) from cliente where ID=@ID) <> @PlazoCredito) or
		((Select ISnull(Contado,0) from cliente where ID=@ID) <> @Contado) or
		((Select ISnull(Credito,0) from cliente where ID=@ID) <> @Credito) Begin
		 
		 Declare @vLImite bit = 'False'
		 Declare @vListaPrecio bit = 'False'
		 Declare @vVendedor bit = 'False'
		 Declare @vTipoCliente bit = 'False'
		 Declare @vPlazo bit = 'False'
		 Declare @vCondicion bit = 'False'
		 
		 If ((Select ISnull(LimiteCredito,0) from cliente where ID=@ID) <> @LimiteCredito) begin set @vLImite = 'True' End
		 If ((Select ISnull(IDListaPrecio,0) from cliente where ID=@ID) <> @IDListaPrecio) begin set @vListaPrecio = 'True' End
		 If ((Select ISnull(IDVendedor,0) from cliente where ID=@ID) <> @IDVendedor) begin set @vVendedor = 'True' End
		 IF ((Select ISnull(IDTipoCliente,0) from cliente where ID=@ID) <> @IDTipoCliente) begin set @vTipoCliente = 'True' End
		 IF ((Select ISnull(PlazoCredito,0) from cliente where ID=@ID) <> @PlazoCredito) begin set @vPlazo = 'True' End
		 IF ((Select ISnull(Contado,0) from cliente where ID=@ID) <> @Contado) begin set @vCondicion = 'True' End
		 IF ((Select ISnull(Credito,0) from cliente where ID=@ID) <> @Credito) begin set @vCondicion = 'True' End


			Insert into ACliente(ID, Referencia, RazonSocial,CI,RUC,Nombrefantasia,Direccion,
			Telefono,IDListaPrecio, IDTipoCliente, IDCategoriaCliente, Contado, Credito, IdVendedor, LimiteCredito,
			FechaModificacion, PlazoCredito,IDUsuario, IdEstado, IdPais, IDDepartamento, IdCiudad, 
			IDSucursal,Accion, ModiLimiteCredito, ModiListaPrecio,ModiVendedor,ModiTipoCliente,ModiPlazo,ModiCondicion)
			Values(@ID, @Referencia,@RazonSocial,@CI,@RUC,@NombreFantasia,@Direccion,
			@Telefono,@IDListaPrecio, @IDTipoCliente,@IDCategoriaCliente, @Contado, @Credito, @IDVendedor, @LimiteCredito,
			getdate(),@PlazoCredito, @IDUsuario, @IDEstado, @IDPais, @IDDepartamento, @IDCiudad, 
			@IDSucursal,'MOD', @vLImite, @vListaPrecio,@vVendedor,@vTipoCliente,@vPlazo,@vCondicion)
			
		End

		

		--Actualizamos
		--Datos Comerciales
		Update Cliente Set Referencia=@Referencia,
							RazonSocial=@RazonSocial, 
							RUC=@RUC,
							CI=@CI, 
							NombreFantasia=@NombreFantasia, 	
							Direccion=@Direccion,
							Telefono= @Telefono,
							Celular = @Celular,
							IDEstado=@IDEstado,
							IDAbogado = @IDAbogado,
							Judicial = @Judicial,
							Boleta = @Boleta
		Where ID=@ID
		
		--Localizaciones
		Update Cliente Set IDPais=@IDPais, 
						   IDDepartamento=@IDDepartamento,
						   IDCiudad=@IDCiudad,
						   IDBarrio=@IDBarrio ,
						   IDZonaVenta=@IDZonaVenta						   
		Where ID=@ID
		
		--Area - Ruta
		Update Cliente Set IDArea=@IDArea, 
						   IDRuta = @IDRuta	
		Where ID=@ID
		
		--Referencia
		Update Cliente Set IDSucursal=@IDSucursal, 
						   IDPromotor=@IDPromotor,
						   IDVendedor=@IDVendedor,
						   IDCobrador=@IDCobrador,
						   IDDistribuidor=@IDDistribuidor
		Where ID=@ID
		
		--Configuraciones
		Update Cliente Set	IDListaPrecio=@IDListaPrecio,
							IDTipoCliente=@IDTipoCliente,
							IdCategoriaCliente=@IDCategoriaCliente,
							Contado=@Contado,
							Credito=@Credito,
							IDMoneda=@IDMoneda,
							IVAExento=@IVAExento,
							CodigoCuentaContable=@CodigoCuentaContable,
							--SM 20-07-2021 Nuevo Campo, Balanceado, Ambos, Aquay
							IDClienteProducto=@IDClienteProducto
		Where ID=@ID
		
		--Estadisticos
		Update Cliente Set	FechaModificacion=GETDATE(),
							IDUsuarioModificacion=@IDUsuario
		Where ID=@ID
		
		--Credito
		--Ejecutar ANALISIS DE CREDITO antes de entrar en el bloque
		Update Cliente Set	LimiteCredito=@LimiteCredito,
							Descuento=@Descuento,
							PlazoCobro=@PlazoCobro,
							PlazoCredito=@PlazoCredito,
							PlazoChequeDiferido=@PlazoChequeDiferido
							
		Where ID=@ID
		
		--Adicionales
		Update Cliente Set	PaginaWeb=@PaginaWeb,
							Email=@Email,
							Fax=@Fax,
							Aniversario=@Aniversario
		Where ID=@ID
		
		--Clientes varios
		Update Cliente Set	ClienteVario = @ClienteVario Where ID=@ID
				
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CLIENTE', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
					
	End
	
	--ELIMNAR
	If @Operacion='DEL' Begin
	
		--Si es que no existe el Cliente
		If Not Exists(Select * From Cliente Where ID=@ID) Begin
			Set @Mensaje = 'El sistema no encuentra el registro!'
			Set @Procesado = 'False'
			return @@rowcount
		End	
		
		--Si es que tiene ventas
		If Exists(Select * From Venta Where IDCliente=@ID) Begin
			Set @Mensaje = 'El cliente tiene ventas realizadas! No se puede eliminar'
			Set @Procesado = 'False'
			return @@rowcount			
		End	

		--Auditoria

		
		--Eliminar Cliente Sucursal
		Delete From ClienteSucursal Where IDCliente=@ID
		
		--Eliminar Cliente Contacto
		Delete From ClienteContacto Where IDCliente=@ID
		
		--Eliminar Cliente
		Delete From Cliente Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CLIENTE', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		
	End
	
	return @@rowcount
	
End


﻿CREATE Procedure [dbo].[SpEfectivizacion]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDTipoComprobante smallint = NULL,
	@Numero int = null,
	@NroComprobante int = NULL,
	@Fecha date = NULL,
	@IDTransaccionCheque numeric(18,0)=NULL,
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from Efectivizacion where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	---Variables para CALCULAR SALDO DEPOSITADO
	declare @Saldo money
	declare @Cancelado bit
	declare @vImporte money
	declare @vIDTransaccionEfectivo numeric(18,0)
	declare @vIDEfectivo tinyint

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		Declare @vNumero int
		Set @vNumero = ISNULL((Select MAX(Numero)+1 From Efectivizacion Where IDSucursal=@IDSucursalOperacion),1)
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--Insertar en DepositoBancario
		Insert Into Efectivizacion(IDTransaccion,IDSucursal,Numero,IDTipoComprobante,NroComprobante,Fecha,IDTransaccionCheque)
		Values(@IDTransaccionSalida ,@IDSucursalOperacion,@Numero,@IDTipoComprobante,@NroComprobante,@Fecha,@IDTransaccionCheque)
				
										
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Efectivizacion Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Anulado = 'True'
		If  Exists(Select * From Efectivizacion Where IDTransaccion=@IDTransaccion And  Anulado='True') Begin
			set @Mensaje = 'La efectivización ya encuentra anulada.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		Exec SpEfectivizacionProcesar @IDTransaccion,@Operacion,@Mensaje,@Procesado
		
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal		
		
			
		--Anulamos el registro
		Update Efectivizacion Set Anulado='True', IDUsuarioAnulado=@IDUsuario, FechaAnulado=GETDATE()
		Where IDTransaccion = @IDTransaccion	
		
		Delete From DetalleEfectivizacion Where IDTransaccion=@IDTransaccion
						
				
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
End
	
	
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End





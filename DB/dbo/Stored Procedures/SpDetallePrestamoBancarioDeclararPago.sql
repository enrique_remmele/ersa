﻿CREATE Procedure [dbo].[SpDetallePrestamoBancarioDeclararPago]

	--Entrada
	@IDTransaccion numeric(18,0),
	@NroCuota smallint,
	@FechaPago date = NULL,
	@ImporteAPagar money = NULL,	
	@PagosVarios money = NULL,
	@ObservacionPago varchar(100) = NULL,
	@Operacion varchar(50),
	
--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin

	--Validar
	--Transaccion
	If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--Transaccion
	If Not Exists(Select IDTransaccion From PrestamoBancario Where IDTransaccion=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	
	--BLOQUES
	
	if @Operacion='UPD' Begin
		
		Declare @vImporteCuota Money
		Declare @vCancelado Bit = 'True'
		Declare @vTotalImporteAPagar Money
		
		--Por si se paga parcialmente va sumando los pago en una cuota determinada
		Set @vTotalImporteAPagar = (Select IsNull(ImporteAPagar,0) From DetallePrestamoBancario Where IDTransaccion=@IDTransaccion And NroCuota=@NroCuota)+@ImporteAPagar

		Set @vImporteCuota = (Select ImporteCuota From DetallePrestamoBancario Where IDTransaccion=@IDTransaccion And NroCuota=@NroCuota)
		
		If @vTotalImporteAPagar < @vImporteCuota Begin
			Set @vCancelado = 'False'
		End
		
		Update DetallePrestamoBancario	Set	Pagar = 'True',
											FechaPago=@FechaPago,
											ImporteAPagar=@ImporteAPagar,
											PagosVarios=@PagosVarios,
											ObservacionPago=@ObservacionPago,
											Cancelado=@vCancelado
		Where IDTransaccion=@IDTransaccion And NroCuota=@NroCuota
					
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	return @@rowcount
	
		
End

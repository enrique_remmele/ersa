﻿CREATE Procedure [dbo].[SpViewResumedeMovimientosSaldosDetallado]

	--Entrada
	@Fecha1 Date,
	@Fecha2 Date,
	
	
	--Salida
	@Mensaje varchar(200) output ,
	@IDTransaccionSalida numeric(18,0) output
As

Begin
    
    ---Variables para CALCULAR SALDO 
    --Variables Para Calculo de Saldo Anterior
    declare @vSaldo money
	declare @vSaldoAnterior money
	declare @vTotalVenta money
	declare @vTotalNotaDebito money
	declare @vTotalNotaCredito money
	declare @vTotalCobranza money
	
	--Variables Para Calculo de Saldo Posterior
	declare @vTotalVenta2 money
	declare @vTotalNotaDebito2 money
	declare @vTotalNotaCredito2 money
	declare @vTotalCobranza2 money
	
	--Declarar variables Variables
	declare @vIDCliente int
	declare @vID tinyint
	declare @vIDTransaccion Numeric (18,0)
	declare @vReferencia varchar (50)
	declare @vCliente varchar (100)

	set @vID = (Select IsNull(MAX(ID)+1,1) From ResumenMovimientoSaldoDetallado)
	
	--Insertar datos	
	Begin
	
		Declare db_cursor cursor for
		Select ID, RazonSocial From VCliente V Order By RazonSocial
		Open db_cursor   
		Fetch Next From db_cursor Into  @vIDCliente, @vCliente
		While @@FETCH_STATUS = 0 Begin 
			--Hallar Saldo Anterior
			set @vTotalVenta  = IsNull((Select Sum(Total) From Venta  Where IDCliente=@vIDCliente And FechaEmision<@Fecha1),0)
			Set @vTotalNotaCredito=IsNull((Select Sum(Importe) From VNotaCreditoVenta Where IDCliente=@vIDCliente And Fecha<@Fecha1),0)
			Set @vTotalNotadebito=IsNull((Select Sum(Importe) From VNotaDebitoVenta Where IDCliente=@vIDCliente And Fecha<@Fecha1),0)
			Set @vTotalCobranza=IsNull((Select  Sum(Importe) From VVentaDetalleCobranza Where IDCliente=@vIDCliente And FechaCobranza<@Fecha1),0)
						
			set @vSaldoAnterior  = (@vTotalVenta + @vTotalNotaDebito) - (@vTotalCobranza + @vTotalNotaCredito) 
			
			--Hallar Saldo
			set @vTotalVenta2  = IsNull((Select Sum(Total) From Venta  Where IDCliente=@vIDCliente And FechaEmision Between @Fecha1  And @Fecha2),0)
			Set @vTotalNotadebito2=IsNull((Select Sum(Importe) From VNotaDebitoVenta Where IDCliente=@vIDCliente And Fecha Between @Fecha1 And @Fecha2),0)
			Set @vTotalCobranza2=IsNull((Select  Sum(Importe) From VVentaDetalleCobranza Where IDCliente=@vIDCliente And FechaCobranza Between @Fecha1 And @Fecha2),0)
			Set @vTotalNotaCredito2=IsNull((Select Sum(Importe) From VNotaCreditoVenta Where IDCliente=@vIDCliente And Fecha Between @Fecha1 And @Fecha2),0)
			
			set @vSaldo= (@vSaldoAnterior + @vTotalVenta2+@vTotalNotadebito2 )- (@vTotalCobranza2+@vTotalNotaCredito2)
			
			--If @vTotalNotaCredito > 0 Begin
				print @vCliente 
				print 'Ventas: ' + convert(varchar(50), @vTotalVenta)
				print 'Ventas2: ' + convert(varchar(50), @vTotalVenta2)
			--	print 'NC: ' + convert(varchar(50), @vTotalNotaCredito)
			--	print 'ND: ' + convert(varchar(50), @vTotalNotaDebito)
			--	print 'Cobranzas: ' + convert(varchar(50), @vTotalCobranza)
			--	print 'Saldo: ' + convert(varchar(50), @vSaldo)
				
			--	print ''
			--End
			Insert Into  ResumenMovimientoSaldoDetallado(ID,IDCliente, RazonSocial, TotalVenta, TotalCobranza, TotalDebito, TotalCredito,SaldoAnterior, Saldo) 
			Values (@vID,@vIDCliente, @vCliente, @vTotalVenta2, @vTotalCobranza2, @vTotalNotadebito2, @vTotalNotaCredito2, @vSaldoAnterior, @vSaldo)
			
			Fetch Next From db_cursor Into  @vIDCliente, @vCliente
			
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		
	End	
	set @Mensaje = 'Sin error!'
	Set @IDTransaccionSalida = @vID
	
	
End
	




﻿CREATE Procedure [dbo].[SpTerminalPuntoExpedicion]

	--Entrada
	@IDTerminalReferencia tinyint,
	@IDOperacion tinyint,
	@IDPuntoExpedicion int,
					   	
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Validar
		--Si ya existe la relacion
		If Exists(Select * From TerminalPuntoExpedicion Where IDTerminal=@IDTerminalReferencia And IDOperacion=@IDOperacion And IDPuntoExpedicion=@IDPuntoExpedicion) Begin
			set @Mensaje = 'Esta terminal ya tiene asignado estos parametros!'
			set @Procesado = 'False'
			return @@rowcount
		End
				
		--Insertamos
		Insert Into TerminalPuntoExpedicion(IDTerminal, IDOperacion, IDPuntoExpedicion)
		Values(@IDTerminalReferencia, @IDOperacion, @IDPuntoExpedicion)
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PUNTO DE EXPEDICION', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@IDPuntoExpedicion
						
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Eliminar primero		
		Delete From TerminalPuntoExpedicion 
		Where IDTerminal=@IDTerminalReferencia and IDOperacion=@IDOperacion And IDPuntoExpedicion=@IDPuntoExpedicion 									
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PUNTO DE EXPEDICION', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@IDPuntoExpedicion
								
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount
	
End


﻿CREATE Procedure [dbo].[SpNotaCreditoProcesar]

	--Entrada
	@IDTransaccion numeric(18),
	@IDTransaccionPedido numeric(18) = 0,
	@Operacion varchar(10),
	
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As	

Begin
	
	--Validar Feha Operacion
	Declare @vIDSucursal as integer
	Declare @vFecha as Date
	Declare @vIDUsuario as integer
	Declare @vIDOperacion as integer
	Declare @vNumeroPedido as integer = 0
	

	Select	@vIDSucursal=IDSucursal,
			@vFecha=Fecha,
			@vIDUsuario=IDUsuario,
			@vIDOperacion=IDOperacion
	 From Transaccion Where ID = @IDTransaccion

	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@vIDSucursal, @vFecha, @vIDUsuario, @vIDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			--set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		If dbo.FValidarFechaOperacion(@vIDSucursal, @vFecha, @vIDUsuario, @vIDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			--set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 
	
	Declare @vAplicar Bit = Cast(0 as bit)

	Set @vAplicar = Cast(isnull((Select Aplicar  From NotaCredito Where IDTransaccion = @IDTransaccion ),0) as bit)

	If @Operacion = 'INS' Begin

		if @IDTransaccionPedido > 0 begin
			if exists(select * from PedidoNCNotaCredito where IDTransaccionPedido = @IDTransaccionPedido) begin
				Set @Mensaje  = 'El pedido ya esta asociado a una Nota de credito!!'
				Set @Procesado = 'False'
				--set @IDTransaccionSalida  = 0
				Return @@rowcount
			end 
			else begin
				set @vNumeroPedido = isnull((Select Numero From PedidoNotaCredito where IDTransaccion = @IDTransaccionPedido),0)
			end
		end
		
		
		--Actualizamos el inventario
		Exec SpNotaCreditoActualizarStock @IDTransaccion=@IDTransaccion , @Operacion=@Operacion, @Mensaje=@Mensaje output, @Procesado=@Procesado output 
		
		--Aplicamos los saldos de las facturas
		If @vAplicar = 0 Begin
			Exec SpNotaCreditoActualizarVanta @IDTransaccion=@IDTransaccion , @Operacion=@Operacion, @Mensaje=@Mensaje output, @Procesado=@Procesado output   
		End 
		
		
		--Aplicar las comisiones
		EXEC SpComision
			@IDTransaccion=@IDTransaccion,
			@Venta='False',
			@Devolucion='True',
			@Operacion= 'INS' 	   			

		--Damos el OK Nota Credito
		If @Procesado = 1 Begin
			
			Update NotaCredito  Set Procesado='True' Where IDTransaccion=@IDTransaccion
			Set @Mensaje = 'Registro procesado!'

			--Generar el Asiento
			Exec SpAsientoNotaCredito @IDTransaccion=@IDTransaccion

			--Asociar el pedido
			Exec SpPedidoNotaCreditoEstado @IDTransaccionNotaCredito = @IDTransaccion, @IDPedido= @vNumeroPedido , @IDSucursal= @vIDSucursal
			
			--Controlar el submotivo
			exec SpNotaCreditoControlarDetallePedido @IDTransaccion
			
			Return @@rowcount
			
		End Else Begin

			--Si hubo un error, eliminar todo el registro
			Set @Operacion = 'DEL'			
			
		End
		
		
	End

	If @Operacion = 'ANULAR' Begin

		Exec SpNotaCreditoActualizarStock @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output   
		Exec SpNotaCreditoActualizarVanta @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output  
		
		Set @Mensaje = 'Registro anulado!'
		Set @Procesado = 'True'
		return @@rowcount
			
	End

	If @Operacion  = 'DEL' Begin

		--Validar
		If (Select Anulado From NotaCredito Where IDTransaccion=@IDTransaccion) = 'True' Begin
			Set @Mensaje = 'El registro debe estar anulado!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--Eliminar
		Delete DetalleNotaCredito
		Where IDTransaccion=@IDTransaccion 
		
		Delete DetalleImpuesto
		Where IDTransaccion=@IDTransaccion 
		
		Delete NotaCreditoVenta 
		Where IDTransaccionNotaCredito =@IDTransaccion 

		Delete NotaCredito
		Where IDTransaccion=@IDTransaccion 
		
		Set @Mensaje = 'Registro eliminado!'
		Set @Procesado = 'True'
		return @@rowcount
		
	End
	
End


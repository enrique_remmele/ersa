﻿CREATE Procedure [dbo].[SpDetalleMovimiento]

	--Entrada
	@IDTransaccion numeric(18,0),
	@IDProducto int = NULL,
	@ID tinyint = NULL,
	@IDDepositoEntrada tinyint = NULL,
	@IDDepositoSalida tinyint = NULL,
	@Observacion varchar(50) = NULL,
	@Cantidad decimal(10,2) = NULL,
	@IDImpuesto tinyint = NULL,
	@PrecioUnitario money = NULL,
	@Total money = NULL,
	@TotalImpuesto money = 0,
	@TotalDiscriminado money = 0,
	@Caja bit = NULL,
	@CantidadCaja smallint = NULL,
	@Operacion varchar(20),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin
	
	--volver a calcular los totales
	Set @Total = @Cantidad * @PrecioUnitario
	Set @TotalImpuesto = 0
	Set @TotalDiscriminado = 0



	--Variables Kardex
	Declare @vFecha datetime
	Declare @vIDSucursal integer
	Declare @vNroComprobante varchar(50)
	Declare @vTipo varchar(50) = 'MOVIMIENTOS'
	
	if (select idtipocomprobante from Movimiento where IDTransaccion = @IDTransaccion) = (select top(1) IDTipoComprobanteProduccion from configuraciones) begin
		Set @vTipo = 'PRODUCCION'
	end

	if (select idtipocomprobante from Movimiento where IDTransaccion = @IDTransaccion) = (select top(1) IDTipoComprobanteImportacion from configuraciones) begin
		Set @vTipo = 'PRODUCCION'
	end

	Set @vFecha = (Select Fecha From Movimiento Where IDTransaccion = @IDTransaccion)
	If @IDDepositoSalida Is Not NULL Begin
		Set @vIDSucursal = (Select IDSucursal From Deposito Where ID = @IDDepositoSalida)
	End Else Begin
		Set @vIDSucursal = (Select IDSucursal From Deposito Where ID = @IDDepositoEntrada)
	End
	Set @vNroComprobante = (Select NroComprobante From Movimiento Where IDTransaccion = @IDTransaccion)


	--Validar
	--Transaccion
	If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--Transaccion
	If Not Exists(Select IDTransaccion From Movimiento Where IDTransaccion=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--Cantidad
	If @Cantidad <= 0 Begin
		Set @Mensaje = 'La cantidad no puede ser igual ni menor a 0!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--BLOQUES
	if @Operacion='INS' Begin
	
		
		--Si es de Salida, verificar la existencia
		--if @IDDepositoSalida Is Not NULL Begin
			
		--	If dbo.FExistenciaProducto(@IDProducto, @IDDepositoSalida) < @Cantidad Begin
		--		Set @Mensaje = 'Stock insuficiente en deposito para el producto: ' + CONVERT(varchar(50), @IDProducto) + ' '
		--		Set @Procesado = 'False'
		--		return @@rowcount
		--	End
			
		--End
		Set @IDDepositoEntrada = (select IDDepositoEntrada from Movimiento where IDTransaccion = @IDTransaccion)
		Set @IDDepositoSalida = (select IDDepositoSalida from Movimiento where IDTransaccion = @IDTransaccion)

		--Insertar el detalle
		Insert Into DetalleMovimiento(IDTransaccion, IDProducto, ID, IDDepositoEntrada, IDDepositoSalida, Observacion, Cantidad, IDImpuesto, PrecioUnitario, Total, TotalImpuesto, TotalDiscriminado, Caja, CantidadCaja, Anulado)
		Values(@IDTransaccion, @IDProducto, @ID, @IDDepositoEntrada, @IDDepositoSalida, @Observacion, @Cantidad, @IDImpuesto, @PrecioUnitario, @Total, @TotalImpuesto, @TotalDiscriminado, @Caja, @CantidadCaja, 'False')
		
		--Actualizar el Stock
		if @IDDepositoSalida Is Not NULL Begin
			
			EXEC SpActualizarProductoDeposito
				@IDDeposito = @IDDepositoSalida,
				@IDProducto = @IDProducto,
				@Cantidad = @Cantidad,
				@Signo = N'-',
				@Mensaje = @Mensaje OUTPUT,
				@Procesado = @Procesado OUTPUT

			----Actualiza el Kardex
			If @IDDepositoEntrada Is Null begin	

				EXEC SpKardex
					@Fecha= @vFecha,
					@IDTransaccion = @IDTransaccion,
					@IDProducto = @IDProducto,
					@ID = @ID,
					@Orden = 11, 
					@CantidadEntrada = 0,
					@CantidadSalida = @Cantidad,
					@CostoEntrada = 0,
					@CostoSalida = @PrecioUnitario,
					@IDSucursal = @vIDSucursal,
					@Comprobante = @vNroComprobante	
			End

		End
		
		if @IDDepositoEntrada Is Not NULL Begin
			EXEC SpActualizarProductoDeposito
				@IDDeposito = @IDDepositoEntrada,
				@IDProducto = @IDProducto,
				@Cantidad = @Cantidad,
				@Signo = N'+',
				@Mensaje = @Mensaje OUTPUT,
				@Procesado = @Procesado OUTPUT
		
			----Actualiza el Kardex
			If @IDDepositoSalida Is Null begin
				
					EXEC SpKardex
					@Fecha= @vFecha,
					@IDTransaccion = @IDTransaccion,
					@IDProducto = @IDProducto,
					@ID = @ID,
					@Orden = 2,
					@CantidadEntrada = @Cantidad,
					@CantidadSalida = 0,
					@CostoEntrada = @PrecioUnitario,
					@CostoSalida = 0,
					@IDSucursal = @vIDSucursal,
					@Comprobante = @vNroComprobante	

					if @vTipo = 'PRODUCCION' begin
						EXEC SpActualizarCostoProducto @ID = @IDProducto, @Costo = @PrecioUnitario, @IDTransaccion = @IDTransaccion, @Operacion = 'PRODUCCION'
					end
			End
		End
		Set @Procesado = 'False'
		return @@rowcount					
			
	End
	
	if @Operacion='DEL' Begin
		
		--Validar
		--Transaccion
		If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra la transaccion!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--Transaccion
		If Not Exists(Select IDTransaccion From Movimiento Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra la transaccion!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	if @Operacion='ANULAR' Begin
		
		--Verificar que exista
		If Not Exists(Select * From DetalleMovimiento Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El Registro ya esta anulado!'
			Set @Procesado = 'True'
			return @@rowcount
		End
		
		--Verificamos que los montos no lleguen a negativo,
		--Que llegue a negativo
		--Begin
			
		--	If Exists(Select * From DetalleMovimiento DM Where DM.IDDepositoEntrada Is Not Null And DM.IDTransaccion=@IDTransaccion And dbo.FExistenciaProducto(DM.IDProducto, DM.IDDepositoEntrada) < DM.Cantidad) Begin
		--		Set @Mensaje = 'Stock insuficiente en algun producto! Verifique la existencia.'
		--		Set @Procesado = 'False'
		--		print @Mensaje
		--		return @@rowcount			
		--	End
			
		--End
		
		--Si todo es correcto, anulamos con el cursor
		Begin
			
			Declare @vID tinyint
			Declare @vIDDepositoEntrada tinyint
			Declare @vIDDepositoSalida tinyint
			Declare @vIDProducto int
			Declare @vCantidad decimal(10,2)
			Declare @vPrecioUnitario decimal(10,2)
			Declare @vControlarExistencia bit
			Declare @vFechaEmision date = (Select Fecha from Movimiento where IDTransaccion =@IDTransaccion)
						
			Declare db_cursorDetalleMovimiento cursor for
			Select DM.ID, DM.IDProducto, DM.IDDepositoEntrada, DM.IDDepositoSalida, DM.Cantidad, DM.PrecioUnitario, P.ControlarExistencia
			From Movimiento M Join DetalleMovimiento DM On M.IDTransaccion=DM.IDTransaccion Join Producto P On DM.IDProducto=P.ID
			Where M.IDTransaccion=@IDTransaccion And M.Anulado='False'
			Open db_cursorDetalleMovimiento   
			fetch next from db_cursorDetalleMovimiento into @vID, @vIDProducto, @vIDDepositoEntrada, @vIDDepositoSalida, @vCantidad, @vPrecioUnitario, @vControlarExistencia

			While @@FETCH_STATUS = 0 Begin  			  
					print concat('entro al cursor ', 'IDProducto ', @vIDProducto, ' Cantidad ', @vCantidad)
				  --Actualizar el Stock
				  if @vControlarExistencia = 'True' begin

					if @vIDDepositoSalida Is Not NULL Begin
						print concat('DepositoSalida ', @vIDDepositoSalida)
						print 'Afectar al deposito de salida'
						EXEC SpActualizarProductoDeposito
							@IDDeposito = @vIDDepositoSalida,
							@IDProducto = @vIDProducto,
							@Cantidad = @vCantidad,
							@Signo = N'+',
							@Mensaje = @Mensaje OUTPUT,
							@Procesado = @Procesado OUTPUT

						----Actualiza el Kardex
						--If @IDDepositoEntrada Is Null begin	

						--	Set @vCantidad = @vCantidad * -1
						--	Set @PrecioUnitario = @vPrecioUnitario * -1

						--	EXEC SpKardex
						--		@Fecha= @vFecha,
						--		@IDTransaccion = @IDTransaccion,
						--		@IDProducto = @vIDProducto,
						--		@ID = @vID,
						--		@Orden = 11,
						--		@CantidadEntrada = 0,
						--		@CantidadSalida = @vCantidad,
						--		@CostoEntrada = 0,
						--		@CostoSalida = @vPrecioUnitario,
						--		@IDSucursal = @vIDSucursal,
						--		@Comprobante = @vNroComprobante	
						--End
					End
					
					if @vIDDepositoEntrada Is Not NULL Begin
					
						print 'Afectar al deposito de entrada'
						print concat('DepositoEntrada ', @vIDDepositoEntrada)
						EXEC SpActualizarProductoDeposito
							@IDDeposito = @vIDDepositoEntrada,
							@IDProducto = @vIDProducto,
							@Cantidad = @vCantidad,
							@Signo = N'-',
							@ControlStock  = 'False',
							@Mensaje = @Mensaje OUTPUT,
							@Procesado = @Procesado OUTPUT

						----Actualiza el Kardex
						--If @IDDepositoSalida Is Null begin	

						--	Set @vCantidad = @vCantidad * -1
						--	Set @PrecioUnitario = @vPrecioUnitario * -1

						--	EXEC SpKardex
						--	@Fecha= @vFecha,
						--	@IDTransaccion = @IDTransaccion,
						--	@IDProducto = @vIDProducto,
						--	@ID = @vID,
						--	@Orden = 2,
						--	@CantidadEntrada = @vCantidad,
						--	@CantidadSalida = 0,
						--	@CostoEntrada = @vPrecioUnitario,
						--	@CostoSalida = 0,
						--	@IDSucursal = @vIDSucursal,
						--	@Comprobante = @vNroComprobante	
						--End
					End
				End
					--Poner en Anulado el registro
					Update DetalleMovimiento Set Anulado='True'
					Where @IDTransaccion=@IDTransaccion And IDProducto=@vIDProducto And ID=@vID
					
					Delete from kardex where idtransaccion = @IDTransaccion
					--Exec SPRecalcularKardexProducto @vIDProducto, @vFechaEmision

					
					--Siguiente
				   fetch next from db_cursorDetalleMovimiento into @vID, @vIDProducto, @vIDDepositoEntrada, @vIDDepositoSalida, @vCantidad, @vPrecioUnitario, @vControlarExistencia
			End   

			Close db_cursorDetalleMovimiento   
			deallocate db_cursorDetalleMovimiento

			

		End
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	return @@rowcount
	
End

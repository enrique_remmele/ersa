﻿
CREATE procedure [dbo].[SpETLVenta]

	@IDSucursal int,
	@Desde date,
	@Hasta date
	
As

Begin
	
	Declare @vCodigoDistribuidor int
	Set @vCodigoDistribuidor = (Select CodigoDistribuidor From Sucursal Where ID=@IDSucursal)
	
	Select
	'CodDistribuidor'=dbo.FFormatoTamañoCaracter (15,@vCodigoDistribuidor),
	'CodCliente'=dbo.FFormatoTamañoCaracter (15, (Case When (DV.IDSucursalCliente)=0 Then C.Referencia Else C.Referencia + Convert(varchar(50), DV.IDSucursalCliente) End)),
	'IndPromocion'=dbo.FFormatoTamañoCaracter (1,'N'),
	'CodZona'=dbo.FFormatoTamañoCaracter (15,IsNull(V.Referencia,'01')),
	'CodProducto'=dbo.FFormatoTamañoCaracter (15,DV.Referencia),
	'EAN13Producto'=dbo.FFormatoTamañoCaracter (15,CodigoBarra),
	'VentaUnidades'=dbo.FFormatoTamañoNumerico (20.2,Sum(Cantidad) - IsNull((Select SUM(DVD.Cantidad) From VDetalleVentaDevoluciones DVD Where DVD.IDProducto=DV.IDProducto And DVD.Fecha Between @Desde And @Hasta And DVD.IDTransaccion=DV.IDTransaccion),0)),
	'VentasEn'=dbo.FFormatoTamañoNumerico (24.6,Sum(Round(TotalDiscriminado,0)) - IsNull((Select SUM(Round(DVD.TotalDiscriminado, 0)) From VDetalleVentaDevoluciones DVD Where DVD.IDProducto=DV.IDProducto And DVD.Fecha Between @Desde And @Hasta And DVD.IDTransaccion=DV.IDTransaccion),0)),
	'FechaInicio'=CONVERT(varchar(5), Year(FechaEmision)) + dbo.FFormatoDosDigitos(Month(FechaEmision)) + dbo.FFormatoDosDigitos(Day(FechaEmision)),
	'FechaFin'=CONVERT(varchar(5), Year(FechaEmision)) + dbo.FFormatoDosDigitos(Month(FechaEmision)) + dbo.FFormatoDosDigitos(Day(FechaEmision))
	From VDetalleVenta DV
	Join Cliente C On DV.IDCliente=C.ID
	Left Outer Join VVendedor V On C.IDVendedor=V.ID
	Where DV.IDSucursal=@IDSucursal And FechaEmision between @Desde And @Hasta And Anulado='False'
	
	And DV.IDTipoProducto=7 --<- Este es FOOD, solo estos se debe exportar
	And DV.IDProveedor = 2334--- Solo Unilever no conti
	
	Group By DV.IDTransaccion, FechaEmision, C.Referencia, DV.IDSucursalCliente, DV.IDZonaVenta, DV.Referencia, CodigoBarra, DV.IDProducto, V.Referencia
	

End	


﻿CREATE Procedure [dbo].[SpZonaDeposito]

	--Entrada
	@ID tinyint,
	@IDSucursal tinyint,
	@IDDeposito smallint,
	@Descripcion varchar(50),
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From ZonaDeposito Where IDSucursal=@IDSucursal And IDDeposito=@IDDeposito And Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDZonaDeposito tinyint
		set @vIDZonaDeposito = (Select IsNull((Max(ID)+1), 1) From ZonaDeposito)

		--Insertamos
		Insert Into ZonaDeposito(ID, IDSucursal, IDDeposito,Descripcion, Estado)
		Values(@vIDZonaDeposito, @IDSucursal, @IDDeposito, @Descripcion, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='ZONA DE DEPOSITO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From ZonaDeposito Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From ZonaDeposito Where Descripcion=@Descripcion And ID!=@ID And IDSucursal=@IDSucursal And IDDeposito=@IDDeposito ) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update ZonaDeposito Set Descripcion=@Descripcion,
								IDSucursal=@IDSucursal,
								IDDeposito=@IDDeposito, 
								Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='ZONA DE DEPOSITO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From ZonaDeposito Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Equipo
		if exists(Select * From EquipoConteo Where IDZonaDeposito=@ID) begin
			set @Mensaje = 'El registro tiene equipos asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From ZonaDeposito 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='ZONA DE DEPOSITO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿
CREATE Procedure [dbo].[SpViewResumenMovimientosSaldosPago]

	--Entrada
	@Fecha Date
		
	
As

Begin
	--Crear la tabla temporal
    create table #TablaTemporal(ID tinyint,
								IDTransaccion int,
								IDProveedor int,
								Proveedor varchar(50),
								Referencia varchar(50),
								Saldo money,
								UltimaCompra date,
								UltimoPago date,
								IDTipoProveedor int,
								TipoProveedor varchar(50),
								IDMoneda int,
								primary key(IDTransaccion,ID))
    
    ---Variables para CALCULAR SALDO 
	declare @vSaldo money
	declare @vTotalEgreso money
	declare @vTotalNotaDebitoProveedor money
	declare @vTotalOrdenPagoEgreso money
	declare @vTotalNotaCreditoProveedor money
	
	--Declarar variables Variables
	declare @vID tinyint
	declare @vIDTransaccion Numeric (18,0)
	declare @vIDProveedor int
	declare @vProveedor varchar (100)
	declare @vTipoProveedor varchar(50)
	declare @vReferencia varchar (50)
	declare @vIDTipoProveedor int
	declare @vIDMoneda int
	declare @vUltimaCompra date
	declare @vUltimoPago date
	set @vID = (Select IsNull(MAX(ID)+1,1) From #TablaTemporal )
	
	--Insertar datos	
	Begin
	
		Declare db_cursor cursor for
		
		Select 
		
		C.IDTransaccion, 
		P.ID,
		P.RazonSocial,
		P.Referencia,
		C.Saldo,
		P.FechaUltimaCompra,
		P.FechaUltimoPago,
		P.IDTipoProveedor,
		P.TipoProveedor,
		C.IDMoneda
		From VProveedor P
		Join Compra C On C.IDProveedor=P.ID 
					
		Open db_cursor   
		Fetch Next From db_cursor Into  @vIDTransaccion,@vIDProveedor,@vProveedor,@vReferencia,@vSaldo,@vUltimaCompra,@vUltimoPago,@vIDTipoProveedor,@vTipoProveedor,@vIDMoneda
		While @@FETCH_STATUS = 0 Begin 
		 
			Set @vTotalEgreso  = IsNull((Select Sum(Total) From VCompraGasto Where IDProveedor=@vIDProveedor And Fecha<=@Fecha),0)
			Set @vTotalNotaDebitoProveedor = IsNull((Select Sum(Total) From VNotaDebitoProveedor Where IDProveedor=@vIDProveedor And Fecha<=@Fecha),0)
			Set @vTotalOrdenPagoEgreso = IsNull((Select Sum(Importe) From VOrdenPagoEgreso  Where IDProveedor=@vIDProveedor And Fecha<=@Fecha),0)
			Set @vTotalNotaCreditoProveedor = IsNull((Select  Sum(Total) From VNotaCreditoProveedor   Where IDProveedor=@vIDProveedor And Fecha<=@Fecha),0)
						
			set @vSaldo = (@vTotalEgreso + @vTotalNotaDebitoProveedor) - (@vTotalOrdenPagoEgreso + @vTotalNotaCreditoProveedor) 
			
		
			Insert Into  #TablaTemporal(ID,IDTransaccion,IDProveedor,Proveedor,Referencia,Saldo,UltimaCompra,UltimoPago,IDTipoProveedor,TipoProveedor,IDMoneda) 
								Values (@vID,@vIDTransaccion,@vIDProveedor,@vProveedor,@vReferencia,@vSaldo,@vUltimaCompra,@vUltimoPago,@vIDTipoProveedor,@vTipoProveedor,@vIDMoneda)
			
			Fetch Next From db_cursor Into   @vIDTransaccion,@vIDProveedor,@vProveedor,@vReferencia,@vSaldo,@vUltimaCompra,@vUltimoPago,@vIDTipoProveedor,@vTipoProveedor,@vIDMoneda
			
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		
	End	
	
	--Select * From #TablaTemporal Where ID=@vID
	Select 
	Referencia,
	Proveedor,
	'Saldo'=sum(Saldo),
	UltimaCompra,
	UltimoPago='',
	TipoProveedor,
	IDMoneda ,
	IDProveedor  
	From #TablaTemporal 
	Where ID=@vID 
	group by Referencia,
			Proveedor,
			UltimaCompra,
			UltimoPago,
			TipoProveedor,
			IDMoneda,
			IDProveedor 
	
End
	












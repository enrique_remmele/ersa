﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[pa_WsCliente]
	@Opcion SMALLINT,
	@ID INT = NULL,
	@Referencia VARCHAR(50)=null,
	@RazonSocial VARCHAR(100)=NULL,
	@RUC VARCHAR(12)=NULL,
	@IDVendedor INT=NULL,
	@Latitud FLOAT=NULL,
	@Longitud FLOAT = NULL,
	@IDCliente INT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @consulta VARCHAR(1000)
    -- Insert statements for procedure here
	IF @Opcion = 1
	BEGIN
		SET @consulta = '
		Select TOP 50
		[ID]
		,[RazonSocial]
		,[RUC]
		,[Referencia]		
		 From Cliente Where IDEstado=1 ' --Estado=' + CHAR(39) + 'ACTIVA' + CHAR(39)  
		 IF @ID IS NOT NULL AND @ID > 0 SET @consulta = @consulta + ' AND ID=' + CAST(@ID AS VARCHAR(10))
		 IF @Referencia IS NOT NULL SET @consulta = @consulta +  ' AND Referencia LIKE ('+ CHAR(39) + '%' + @Referencia + '%' + CHAR(39) +')'
		 IF @RazonSocial IS NOT NULL SET @consulta = @consulta +  ' AND RazonSocial LIKE ('+ CHAR(39) + '%' + @RazonSocial + '%' + CHAR(39) +')'
		 IF @RUC IS NOT NULL SET @consulta = @consulta +  ' AND RUC LIKE ('+ CHAR(39) + '%' + @RUC + '%' + CHAR(39) +')' 

		 EXEC (@consulta)
	END    
	ELSE IF @Opcion = 2
	BEGIN
		Select TOP 1
		[ID]
		,[RazonSocial]
		,[NombreFantasia]
		,[RUC]
		,[Referencia],		
		IDListaPrecio,
		ListaPrecio,
		IDTipoCliente,
		TipoCliente,
		Condicion,
		Email,
		Direccion,
		Telefono,
		Fax,
		Latitud,
		Longitud,
		Departamento,
		Ciudad,
		Barrio,
		IDSucursal,
		ZonaVenta
		From VCliente Where Estado='ACTIVA'  
		 AND ID=@ID 

	END    
	
	ELSE IF @Opcion = 3 --Tipos de clientes
	BEGIN
		SET @consulta = '  
		SELECT TC.ID, TC.Descripcion, COUNT(C.ID) AS Cantidad FROM Cliente C
		JOIN TipoCliente TC ON C.IDTipoCliente = TC.ID
		WHERE C.IDEstado = 1'
		IF @IDVendedor IS NOT NULL SET @consulta = @consulta + ' AND C.IDVendedor=' + CAST(@IDVendedor AS VARCHAR(10))
		SET @consulta = @consulta + ' GROUP BY TC.ID, TC.Descripcion ORDER BY TC.Descripcion'

		EXEC (@consulta)
	END 

	ELSE IF @Opcion = 4 -- Sucursales de clientes
	BEGIN
	  SELECT [IDCliente]
      ,[ID]
      ,CAST(ID AS VARCHAR(2)) + ' - ' + [Sucursal] AS RazonSocial
      ,[Direccion]
      ,[Contacto]
      ,[Telefono]
      ,[Vendedor]
      ,[Pais]
      ,[Departamento]
      ,[Ciudad]
      ,[Barrio]
      ,[ZonaVenta]
      ,[ClienteSucursal]
      ,[ListaPrecio]
      ,[Cobrador]
      ,[Latitud]
      ,[Longitud]
      ,[Condicion]
	  , ([Direccion] + ' / ' + Ciudad + ' / ' + Telefono) AS Referencia
	  FROM [VClienteSucursal] Where Estado=1 AND IDCliente= @ID

	END
	
	ELSE IF @Opcion = 5 -- Actualizar Localizacion de Sucursales de clientes
	BEGIN
	  UPDATE dbo.ClienteSucursal SET Latitud=@Latitud, Longitud=@Longitud
	  WHERE IDCliente= @IDCliente AND ID = @ID

	END
  
  ELSE IF @Opcion = 6 -- Actualizar localizacion de matriz del cliente
	BEGIN
	  UPDATE dbo.Cliente SET Latitud=@Latitud, Longitud=@Longitud
	  WHERE ID = @ID

	END  

	ELSE IF @Opcion = 7 -- Consulta matriz y sucursales del cliente
	BEGIN
	  SELECT [IDCliente]
      ,[ID]
      ,CAST(ID AS VARCHAR(2)) + ' - ' + [Sucursal] AS RazonSocial
      ,[Direccion] + ' / ' + Telefono + ' (' + TipoSucursal + ')' AS Direccion
      ,[Latitud]
      ,[Longitud]
	FROM [SAIN1].[dbo].[VClienteSucursalConsolidado] WHERE IDCliente = @ID

	END  
	
END


﻿CREATE Procedure [dbo].[SpClasificacionProducto]

	--Entrada
	@ID smallint = NULL,
	@IDPadre smallint = NULL,
	@Descripcion varchar(100) = NULL,
	@Alias varchar(10) = NULL,
	@Numero tinyint = NULL,
	@Imagen image = NULL,
	@IDAgrupador tinyint = NULL,
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
As

Begin

	--VARIABLES
	declare @vNivel tinyint
	declare @vNumero tinyint
	declare @vCodigo varchar(50)
	declare @vCodigoPadre varchar(50)
	
	if @Numero Is Not NULL Begin
		Set @vNumero = @Numero
	End
	
	--BLOQUES
	
	--INSERTAR
	If @Operacion='INS' Begin
		
		
		--Si la descripcion ya existe
		If Exists(Select * From ClasificacionProducto Where Descripcion=@Descripcion And IDPadre=@IDPadre) Begin
			set @Mensaje = 'La descripcion ya existe dentro del grupo!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si el Alias ya existe
		If Exists(Select * From ClasificacionProducto Where Alias=@Alias And Alias!='') Begin
			set @Mensaje = 'El alias ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Configurar varialbes
		Set @vCodigoPadre = (Select Codigo From ClasificacionProducto Where ID = @IDPadre)
		Set @vNivel = (Select Nivel From ClasificacionProducto Where ID = @IDPadre) + 1
		if @vNumero Is Null Begin
			Set @vNumero = (Select IsNull((Select Max(Numero) + 1 From ClasificacionProducto Where IDPadre = @IDPadre), 1))
		end
		Set @vCodigo = @vCodigoPadre + '' + CONVERT(varchar(50), @vNumero) 
		
		--Insertar
		Declare @vID smallint
		Set @vID = (Select ISNULL(MAX(ID)+1, 1) From ClasificacionProducto)
				
		Insert Into ClasificacionProducto(ID, IDPadre, Nivel, Numero, Codigo, Descripcion, Alias, Imagen, IDAgrupador)
		Values(@vID, @IDPadre, @vNivel, @vNumero, @vCodigo, @Descripcion, @Alias, @Imagen, @IDAgrupador)
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
				
	End
	
	--ACTUALIZAR
	If @Operacion='UPD' Begin
	
		--Si el registro no existe
		If Not Exists(Select * From ClasificacionProducto Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si la descripcion ya existe
		If Exists(Select * From ClasificacionProducto Where Descripcion=@Descripcion And ID!=@ID And IDPadre=@IDPadre) Begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si el Alias ya existe
		If Exists(Select * From ClasificacionProducto Where Alias=@Alias And Alias!='' And ID!=@ID) Begin
			set @Mensaje = 'El alias ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Configurar varialbes
		Set @vCodigoPadre = (Select Codigo From ClasificacionProducto Where ID = @IDPadre)
		Set @vNivel = (Select Nivel From ClasificacionProducto Where ID = @IDPadre) + 1
		if @vNumero Is Null Begin
			Set @vNumero = (Select IsNull((Select Max(Numero) + 1 From ClasificacionProducto Where IDPadre = @IDPadre), 1))
		end
		Set @vCodigo = @vCodigoPadre + '' + CONVERT(varchar(50), @vNumero) 
		
		--print('Codigo del Padre: ' + @vCodigoPadre)
		--print('Nivel: ' + convert(varchar(50), @vNivel))
		--print('Numero: ' + convert(varchar(50), @vNumero))
		--print('Codigo: ' + @vCodigo)
		
		Update ClasificacionProducto Set	Codigo=@vCodigo, 
									Nivel=@vNivel,
									Numero=@vNumero,
									Descripcion=@Descripcion, 
									Alias=@Alias,
									IDPadre=@IDPadre,
									Imagen=@Imagen,
									IDAgrupador=@IDAgrupador
									
		Where ID=@ID
													
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	--ELIMINAR
	If @Operacion='DEL' Begin
	
		--Si el registro no existe
		If Not Exists(Select * From ClasificacionProducto Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si es que no esta asociado a ningun producto
		If Exists(Select * From Producto Where IDClasificacion=@ID) Begin
			set @Mensaje = 'El registro tiene productos asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--*** Si es padre, no se puede eliminar hasta que sus hijos sean eliminados *** Esto puede cambiar, pero no se recomienda.
		If Exists(Select * From ClasificacionProducto Where IDPadre=@ID) Begin
			set @Mensaje = 'No se puede eliminar una cuenta cuando este tenga clasificaciones dependientes! Elimine primeramente las clasificaciones hijas.'
			set @Procesado = 'False'
			return @@rowcount
		End 
						
		--Eliminamos la clasificacion
		Delete From ClasificacionProducto Where ID=@ID
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
End



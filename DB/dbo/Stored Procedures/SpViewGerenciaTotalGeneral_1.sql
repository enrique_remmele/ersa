﻿CREATE Procedure [dbo].[SpViewGerenciaTotalGeneral]

	--Entrada
	@Fecha date,
	@IDSucursal tinyint = 1,
	@IDVendedor integer = 1,
	@IDTipoProducto tinyint = 1,
	@IDUnidadMedida tinyint = 1,
	@IDProducto int = 1,
	@IDListaPrecio tinyint = 1

As

Begin

	--variables
	Declare @vID tinyint
	Declare @Local varchar(16)
	Declare @Moneda varchar(4)
	Declare @Descripcion varchar(50)
	Declare @Referencia varchar(50)
	Declare @Decimales bit
	Declare @Divide bit
	Declare @Estado bit
	Declare @Multiplica bit
	Declare @Cotizacion money
	Declare @FechaCotizacion varchar(16)
	
	--Crear la tabla
	Create table #TotalGeneral(IDSucursal tinyint,
								IDVendedor integer,
								IDTipoProducto tinyint ,
								IDUnidadMedida tinyint ,
								IDProducto int ,
								IDListaPrecio tinyint 
															)
								

	Declare db_cursor cursor for
	Select IDSucursal,
	IDVendedor,
	IDTipoProducto,
	IDUnidadMedida,
	IDProducto,
	IDListaPrecio
	From VInformeGerencialVenta M
	Open db_cursor   
	Fetch Next From db_cursor Into	@IDSucursal,@IDVendedor,@IDTipoProducto,@IDUnidadMedida,@IDProducto,@IDListaPrecio
	While @@FETCH_STATUS = 0 Begin 
			Insert Into #TotalGeneral (IDSucursal,IDVendedor,IDTipoProducto,IDUnidadMedida,IDProducto,IDListaPrecio)
			Values(@IDSucursal,@IDVendedor,@IDTipoProducto,@IDUnidadMedida,@IDProducto,@IDListaPrecio)
		
Siguiente:
		
		Fetch Next From db_cursor Into	@IDSucursal,@IDVendedor,@IDTipoProducto,@IDUnidadMedida,@IDProducto,@IDListaPrecio
		
	End

	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor		   		
	
	
	Select * From #TotalGEneral 
	Order by IDSucursal,IDVendedor,IDTipoProducto,IDUnidadMedida,IDProducto,IDListaPrecio
End





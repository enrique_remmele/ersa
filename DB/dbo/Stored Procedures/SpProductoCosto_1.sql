﻿CREATE Procedure [dbo].[SpProductoCosto]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Numero VARCHAR(50) = NULL,
	@Fecha date = NULL,
	@Observacion varchar(200) = NULL,
	@Total money = NULL,
	@Operacion varchar(50),
	@ReestablecerCostoAnterior bit = 'False',
	@CostoProduccion bit = 'False',

	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--BLOQUES
	SET @Numero = REPLACE(@Numero,'.','')
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		If Exists(Select * From VProductoCosto Where Numero=@Numero AND IDSucursal = @IDSucursal) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		------Insertar
		Insert Into ProductoCosto(IDTransaccion, Numero, Fecha, Observacion, Total, Anulado,CostoProduccion)
		Values(@IDTransaccionSalida, @Numero, @Fecha, @Observacion, @Total, 'False', @CostoProduccion)
				
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From ProductoCosto Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que no este anulado
		if (Select Anulado From ProductoCosto Where IDTransaccion=@IDTransaccion) = 'True' Begin
			set @Mensaje = 'El registro ya esta anulado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Reestablecer los costos
		If @ReestablecerCostoAnterior = 'True' Begin
			
			Declare @vIDProducto int
			Declare @vCostoAnterior money
			Declare db_cursor cursor for
			Select IDProducto, CostoAnterior From DetalleProductoCosto
			Where IDTransaccion=@IDTransaccion
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDProducto, @vCostoAnterior
			While @@FETCH_STATUS = 0 Begin 
			
				--Actualizar el costo
				Declare @vConfiguracionCosto varchar(50)

				--Obtenemos la configuracion del Costo
				Set @vConfiguracionCosto = (Select IsNull((Select Top(1) MovimientoCostoProducto From Configuraciones), 'PROMEDIO'))
				
				If @vConfiguracionCosto = 'ULTIMO' Begin
					Update Producto Set CostoSinIVA=@vCostoAnterior
					Where ID=@vIDProducto
				End
				
				If @vConfiguracionCosto = 'PROMEDIO' Begin
					Update Producto Set CostoPromedio=@vCostoAnterior 
					Where ID=@vIDProducto
				End
				
				Fetch Next From db_cursor Into @vIDProducto, @vCostoAnterior
											
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor
			
		End	
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		--Anulamos el registro
		Update ProductoCosto Set Anulado = 'True'
		Where IDTransaccion = @IDTransaccion
		Delete from Kardex where IDTransaccion=@IDTransaccion
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End	
		
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End

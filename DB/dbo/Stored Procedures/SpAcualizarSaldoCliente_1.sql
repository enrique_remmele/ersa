﻿CREATE Procedure [dbo].[SpAcualizarSaldoCliente]

	--Entrada
	@IDCliente int
	

As

Begin
	
	Declare @vMensaje varchar(200)
	Declare @vProcesado bit
	Declare @vSaldo money
	Declare @vDeuda money

	--Valores Negativos
	Declare @vTotalVenta money
	Declare @vTotalNotaDebito money
	
	--Valores Positivos
	Declare @vTotalCobranzaCredito money
	Declare @vTotalAnticipoAplicacion money
	Declare @vTotalCobranzaContado money
	Declare @vTotalNotaCredito money
	Declare @vTotalChequeDiferido money
	Declare @vTotalCobroSistemaAnterior money
	
	--No tomar las ventas que se cancelan automaticamente
	Set @vTotalVenta = (IsNull((Select round(SUM(V.Total * Cotizacion),0) 
								From Venta V
								Join FormaPagoFactura FPF On V.IdFormaPagoFactura=FPF.ID
								Where V.IDCliente=@IDCliente 
								And V.Anulado='False' And V.Procesado='True'
								And FPF.CancelarAutomatico='False'),0))

	Set @vTotalNotaDebito = (IsNull((Select round(SUM(Total * Cotizacion),0) From NotaDebito Where IDCliente=@IDCliente And Anulado='False' And Procesado='True'),0))
	Set @vTotalCobranzaContado = (IsNull((Select round(SUM(VC.Importe * CC.Cotizacion),0) From VentaCobranza VC 
											Join Venta V On VC.IDTransaccionVenta=V.IDTransaccion 
											Join CobranzaContado CC On VC.IDTransaccionCobranza=CC.IDTransaccion
											--Where V.Credito='True' And V.IDCliente=@IDCliente And V.Anulado='False' And V.Procesado='True'
											Where  V.IDCliente=@IDCliente And V.Anulado='False' And V.Procesado='True'
											And CC.Anulado='False' And CC.Procesado='True'),0))
	Set @vTotalCobranzaCredito = (IsNull((Select round(SUM(VC.Importe * CC.Cotizacion),0) From VentaCobranza VC 
											Join Venta V On VC.IDTransaccionVenta=V.IDTransaccion 
											Join CobranzaCredito CC On VC.IDTransaccionCobranza=CC.IDTransaccion
											--Where V.Credito='True' And V.IDCliente=@IDCliente And V.Anulado='False' And V.Procesado='True'
											Where V.IDCliente=@IDCliente And V.Anulado='False' And V.Procesado='True'
											And CC.Anulado='False' And CC.Procesado='True'),0))

	Set @vTotalAnticipoAplicacion = (IsNull((Select round(SUM(VC.Importe * CC.Cotizacion),0) From AnticipoVenta VC 
											Join Venta V On VC.IDTransaccionVenta=V.IDTransaccion 
											Join AnticipoAplicacion AP on AP.IDTransaccion = VC.IDTransaccionAnticipo
											Join CobranzaCredito CC On AP.IDTransaccionCobranza=CC.IDTransaccion
											--Where V.Credito='True' And V.IDCliente=@IDCliente And V.Anulado='False' And V.Procesado='True'
											Where V.IDCliente=@IDCliente And V.Anulado='False' And V.Procesado='True'
											And CC.Anulado='False' And CC.Procesado='True'),0))
	
	Set @vTotalNotaCredito = (IsNull((Select round(SUM(Total * Cotizacion),0) From NotaCredito Where IDCliente=@IDCliente And Anulado='False' And Procesado='True'),0))
	set @vTotalChequeDiferido = (select Isnull(Sum(Importe),0) from vChequeCliente where IDCliente=@IDCliente and Diferido = 1 and estado <> 'DEPOSITADO' and estado <> 'RECHAZADO' and estado <> 'RECHAZADO CANCELADO')
	
	Set @vTotalCobroSistemaAnterior = (IsNull((Select SUM(VI.Cobrado) From VVenta V 
											Join VentaImportada VI On V.IDTransaccion=VI.IDTransaccion 
											Where V.IDCliente = @IDCliente And V.Anulado='False' And V.Procesado='True' And VI.Cobrado>0),0))
	
	declare @vDiferenciaCambio money 
	Set @vDiferenciaCambio = (IsNull((Select round(SUM(DiferenciaCambio),0) From CobranzaCredito Where IDCliente=@IDCliente And Anulado='False' And Procesado='True'),0))

	--Set @vDeuda = ((@vTotalVenta + @vTotalNotaDebito + @vTotalChequeDiferido) - (@vTotalCobranzaContado + @vTotalCobranzaCredito + @vTotalNotaCredito)) +  @vTotalChequeDiferido
	Set @vDeuda = ((@vTotalVenta + @vTotalNotaDebito +@vTotalChequeDiferido) - (@vTotalCobranzaContado + @vTotalCobranzaCredito + @vTotalAnticipoAplicacion + @vTotalNotaCredito + @vTotalCobroSistemaAnterior)) + @vDiferenciaCambio
	
	

	Declare @vCredito money
	Set @vCredito = (Select LimiteCredito From Cliente Where ID=@IDCliente)
		
	--Hasta aclarar bien, usar el saldo de las facturas solamente
	--Set @vSaldo = (Select SUM(Saldo) From Venta Where IDCliente=@IDCliente And Anulado='False' And Cancelado='False' And Procesado='True')
	--Set @vSaldo = @vCredito - @vSaldo
	Set @vSaldo = @vCredito - @vDeuda 

	Update Cliente Set Deuda=@vDeuda
	Where ID=@IDCliente
	
	Select 'Mensaje'='Registro guardado', 'Procesado'='True', 'Saldo'=@vSaldo, 'Deuda'=@vDeuda, 'Credito'=@vCredito
			
End
	
	

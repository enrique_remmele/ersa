﻿CREATE procedure [dbo].[SpImportOrderItem]

@IDSucursal int,
@Desde date,
@Hasta date

As

Begin

	Select

	'cdOrder'='P'+''+convert(Varchar(50),ReferenciaSucursal)+'-'+convert(varchar(50),ReferenciaPunto)+'-'+convert(varchar(50),NroComprobante),
	'cdProduct'=Referencia,
	'nrSequenceOrder'=ID,
	'nrQuantityOrder'=Cantidad,
	'vlDiscontSubTotal'=DescuentoUnitario,
	'vlSubtotalOrder'=TotalDescuento,
	'cdInvoice'=[Cod.]+''+convert(Varchar(50),ReferenciaSucursal)+'-'+convert(varchar(50),ReferenciaPunto)+'-'+convert(varchar(50),NroComprobante),
	'nrSequenceInvoice'=ID,
	
	--Cantidad de SKU
	'nrQuantityInvoice'=(Select COUNT(*)From VDetalleVenta V Where V.IDTransaccion=DV.IDTransaccion)
						-
						IsNull((Select COUNT(*)From VDetalleVentaDevoluciones V Where V.IDTransaccion=DV.IDTransaccion),0),

	--Cantidad de productos					
	'vlSubTotalInvoice'=(Select SUM(Cantidad) From VDetalleVenta V Where V.IDTransaccion=DV.IDTransaccion)
						-
						IsNull((Select SUM(Cantidad) From VDetalleVentaDevoluciones V Where V.IDTransaccion=DV.IDTransaccion),0)

	From VDetalleVenta DV 
	Where DV.IDSucursal=@IDSucursal 
	And FechaEmision Between @Desde And @Hasta
	And DV.Anulado='False'

End


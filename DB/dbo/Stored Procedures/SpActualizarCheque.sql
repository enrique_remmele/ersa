﻿CREATE Procedure [dbo].[SpActualizarCheque]

	--Entrada
	@IDTransaccion int,
	@NroCheque varchar(50)=NULL,
	@AlaOrden varchar(150)=NULL,
	@Observacion varchar(150)=NULL,
	@NroCuentaBancaria varchar(50)=NULL,
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin
	--Declaracion de variables
	declare @IdChequera tinyint
	declare @NroChequeAUtilizar int
	declare @NroHasta int

	--Verificar que la cuenta utilizada se encuentre en el listado de chequeras y el numero de cheque corresponda a dicha chequera
	select @IdChequera = ID, @NroChequeAUtilizar = AUtilizar, @NroHasta = NroHasta from VChequera where CuentaBancaria = @NroCuentaBancaria and @NroCheque between NroDesde and NroHasta and Estado = 1

	if @IdChequera is null begin
		set @Mensaje = 'El numero no existe en chequera.'
		set @Procesado = 'False'
		return @@rowcount
	end	

	if @NroChequeAUtilizar > @NroCheque begin
		set @Mensaje = CONCAT('El numero de cheque correlativo debe ser: ',@NroChequeAUtilizar)
		set @Procesado = 'False'
		return @@rowcount
	end
	if @NroChequeAUtilizar < @NroCheque begin
		set @Mensaje = CONCAT('Numero de cheque ya utilizado, debe ser: ',@NroChequeAUtilizar)
		set @Procesado = 'False'
		return @@rowcount
	end

	--Actualizar Chequera
	if @NroChequeAUtilizar = @NroHasta begin
		Update Chequera set Estado = 0 where ID = @IdChequera
		Update Chequera set AUtilizar = 0 where ID = @IdChequera
	end else begin
		Update Chequera set AUtilizar = AUtilizar + 1 where ID = @IdChequera
	end
			
	--Actualizar Cheque
	Update Cheque  Set NroCheque=@NroCheque,
					ALaOrden =@AlaOrden 
	Where IDTransaccion=@IDTransaccion 
	
	--Actualizar Observación OP
	Update OrdenPago Set Observacion=@Observacion 
	Where IDTransaccion=@IDTransaccion
			
	set @Mensaje = 'Registro guardado'
	set @Procesado = 'True'
	return @@rowcount
		

End
	
	


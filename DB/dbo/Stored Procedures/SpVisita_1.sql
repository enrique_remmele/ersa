﻿
CREATE Procedure [dbo].[SpVisita]
	
	--Entrada
	@ID integer,
	@IDCliente integer,
	@FechaVisita Date,
	@HoraVisita Time,
	@IDVendedor tinyint,
	@FechaProxima date,
	@HoraProxima Time,
	@Atendido Varchar(50),
	@Comentario varchar(254),
	@Estado bit,
	@Operacion varchar(10),

	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
	    If @ID = 0 Begin
	     set @ID = (select Max(Id)+1 from Visita) 
	    end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Visita Where ID = @ID) begin
			set @Mensaje = 'La visita ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDVisita tinyint
		set @vIDVisita = (Select IsNull((Max(ID)+1), 1) From Visita)

		--Insertamos
		Insert Into Visita(ID, IDcliente, FechaVisita, HoraVisita,IdVendedor, FechaProximaVisita,
		HoraProximaVisita, Atendido, Comentario, Estado)
		Values(@ID, @IDcliente, @FechaVisita, @HoraVisita, @IDVendedor,@FechaProxima, @HoraProxima,
		@Atendido, @Comentario, @Estado )		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='VISITA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
				 		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From Visita Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end

		
		--Actualizamos
		Update Visita 
		Set IDcliente = @IDcliente, 
		    FechaVisita = @FechaVisita,
		    HoraVisita = @HoraVisita,
			IDVendedor = @IDVendedor, 
			FechaProximaVisita = @FechaProxima,
			HoraProximaVisita = @HoraProxima,
			Atendido = @Atendido, 
			Comentario = @Comentario,
			Estado = @Estado		
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='VISITA', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Visita Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Update Visita 
		set Estado = 'False'
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='VISITA', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[pa_WsActualizarClienteSucursal]
	@opcion SMALLINT,
	@id INT = NULL,
	@idcliente INT=NULL,
	@latitud DECIMAL(18,16)=NULL,
	@longitud DECIMAL(18,16) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @Opcion = 1 -- Actualizar Localizacion de Sucursales de clientes
	BEGIN
	  UPDATE dbo.ClienteSucursal SET Latitud=@latitud, Longitud=@longitud
	  WHERE IDCliente= @idcliente AND ID = @id

	END
	ELSE IF @Opcion = 2 -- Actualizar Localizacion Matriz de CLiente
	BEGIN
	  UPDATE dbo.Cliente SET Latitud=@latitud, Longitud=@longitud
	  WHERE ID = @id

	END
	
END


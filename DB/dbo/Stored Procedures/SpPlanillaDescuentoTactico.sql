﻿CREATE Procedure [dbo].[SpPlanillaDescuentoTactico]

	--Entrada OP
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalOperacion	tinyint	= NULL,
	@Numero int = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante varchar(50) = NULL,
	@IDProveedor int = NULL,
	@Desde date = NULL,
	@Hasta date = NULL,
	@TotalPresupuesto money = NULL,
	@Observacion varchar(500) = NULL,
	
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
	
	
As

Begin

	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		If Exists(Select * From PlanillaDescuentoTactico Where Numero=@Numero And IDSucursal=@IDSucursalOperacion) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Si ya existe el documento
		If Exists(Select * From PlanillaDescuentoTactico Where IDTipoComprobante=@IDTipoComprobante And Comprobante=@NroComprobante And IDSucursal=@IDSucursal) Begin
			set @Mensaje = 'El numero de comprobante ya existe!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
	
		
		--Insertar Transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
			
		--Insertar en Orden de Pago
		Insert Into PlanillaDescuentoTactico(IDTransaccion, Numero, IDTipoComprobante, Comprobante, IDSucursal, Desde, Hasta, IDProveedor, Observacion, TotalPresupuesto, TotalUtilizado, TotalSaldo, TotalExedente, Anulado)
		Values(@IDTransaccionSalida, @Numero, @IDTipoComprobante, @NroComprobante, @IDSucursalOperacion, @Desde, @Hasta, @IDProveedor, @Observacion, @TotalPresupuesto, 0, @TotalPresupuesto, 0, 'False')
				
				
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount	
	
	End

	If @Operacion = 'ANULAR' Begin
		
		--Validar
		--Vericar si no existe en Pedidos
		If exists (Select * From PlanillaDescuentoTactico PDT Join DetallePlanillaDescuentoTactico DPT On PDT.IDTransaccion = DPT.IDTransaccion Join Actividad A On DPT.IDActividad=A.ID Join DescuentoPedido D On A.ID = D.IDActividad Join Pedido P On D.IDTransaccion = P.IDTransaccion Where P.Anulado = 'False' And PDT.IDTransaccion=@IDTransaccion )Begin
			set @Mensaje = 'No se puede anular descuento esta asociada a un pedido.'
			set @Procesado = 'False'
			return @@rowcount	
		end
		
		--Verificar si no existe en Ventas
		If exists (Select * From PlanillaDescuentoTactico PDT Join DetallePlanillaDescuentoTactico DPT On PDT.IDTransaccion = DPT.IDTransaccion Join Actividad A On DPT.IDActividad=A.ID Join Descuento D On A.ID = D.IDActividad Join Venta V On D.IDTransaccion = V.IDTransaccion Where V.Anulado = 'False' And PDT.IDTransaccion=@IDTransaccion )Begin
			set @Mensaje = 'No se puede anular descuento esta asociada a una venta.'
			set @Procesado = 'False'
			return @@rowcount	
		end
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
				
		--Anular
		Update PlanillaDescuentoTactico Set Anulado='True', IDUsuarioAnulado=@IDUsuario, FechaAnulado=GETDATE()
		Where IDTransaccion = @IDTransaccion 
				
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount	
		
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
	
	
End


﻿CREATE Procedure [dbo].[SpRendicionFondoFijo]

	--Entrada
	@IDTransaccion numeric(18,0)=NULL,
	@IDSucursalFF tinyint=NULL,
	@IDGrupo tinyint=NULL,
	@Numero int=NULL,
	@Fecha date=NULL,
	@Observacion varchar(100)=NULL,
	@IDTransaccionOrdenPago numeric(18,0)=NULL,
	@Operacion varchar(50),
	@Monto money=NULL,
	@Reponer money=NULL,
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
As

Begin
	
	
	--BLOQUES
	
	--INSERTAR
	If @Operacion='INS' begin
		
		--Validar que Cancelado=False
		If Exists(Select * From DetalleFondoFijo Where IDTransaccionRendicionFondoFijo=@IDTransaccion and Cancelado='False') Begin
			set @Mensaje = 'El DetalleFondoFijo no esta cancelado!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--Orden de Pago no debe ser cero
		If @IDTransaccionOrdenPago = 0   begin
			set @Mensaje = 'La Orden de pago no debe ser nulo!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--Validar que la orden de pago no exista en otra RendiciónFondoFijo
		If Exists(Select * From RendicionFondoFijo  Where IDTransaccionOrdenPago=@IDTransaccionOrdenPago) Begin
			set @Mensaje = 'Orden de pago existente en otra RendiciónFondoFijo!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--El Monto  del OP debe ser igual a Monto  a reponer
		if @Monto!= @Reponer  begin
			set @Mensaje = 'El monto de la OP debe ser igual al monto a reponer!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
	----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		Insert Into RendicionFondoFijo (IDTransaccion,IDSucursal,Numero,IDGrupo,Fecha,Observacion,IDTransaccionOrdenPago)
		Values(@IDTransaccionSalida,@IDSucursalFF,@Numero,@IDGrupo,@Fecha,@Observacion,@IDTransaccionOrdenPago)		
									
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--Anular
	If @Operacion='ANULAR' begin
		
			
		If not Exists(Select * From RendicionFondoFijo  Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'La Rendición esta inexistente!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
			
		Update RendicionFondoFijo Set Anulado='True',IDTransaccionOrdenPago=NULL
		Where IDTransaccion=@IDTransaccion	
		
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
									
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
		
	

End




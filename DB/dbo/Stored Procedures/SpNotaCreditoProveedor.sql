﻿CREATE Procedure [dbo].[SpNotaCreditoProveedor]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDTipoComprobante smallint = NULL,
	@Numero int = NULL,
	@NroComprobante Varchar(50) = NULL,	
	@NroTimbrado int = NULL,			
	@IDProveedor int = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDDepositoOperacion tinyint = NULL,
	@Fecha date = NULL,
	@FechaVencimientoTimbrado date = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money= NULL,
	@Observacion varchar(500) = NULL,
	@Total decimal(18,3) = NULL,
	@TotalImpuesto decimal(18,3) = NULL,
	@TotalDiscriminado decimal(18,3) = NULL,
	@TotalDescuento decimal(18,3) = NULL,
	@Aplicar bit = 'False',
	@Devolucion bit = 'False',
	@Descuento bit = 'False',	
	@ConComprobantes bit = 'True',
	@Saldo decimal(18,3)  = NULL,
			
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
	
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from NotaCreditoProveedor where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero ( Numero / Sucursal)
		if Exists(Select * From NotaCreditoProveedor Where IDSucursal=@IDSucursalOperacion   And Numero =@Numero ) Begin
			set @Mensaje = 'El numero de operacion ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Comprobante (Comprobante / Proveedor)
		if Exists(Select * From NotaCreditoProveedor Where IDProveedor =@IDProveedor   And NroComprobante=@NroComprobante And NroTimbrado=@NroTimbrado and Procesado = 'True') Begin
			set @Mensaje = 'El numero de comprobante y proveedor ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		print 'genera transaccion'
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--Calcular Saldo....
		--Si es Sin Comprobantes, SALDO = @TOTAL
		--Si no, SALDO = 0
		print 'controla concomprobantes'
		If @ConComprobantes = 'False' Begin
			Set @Saldo = @Total 		
		End Else Begin
			Set @Saldo = 0
		End
		print 'entra a insertar'
		print concat('idtransaccion: ',@IDTransaccionSalida)
		--Insertar en Nota de Credito Proveedor
		Insert Into NotaCreditoProveedor(IDTransaccion,IDTipoComprobante, Numero , NroComprobante, Nrotimbrado, IDProveedor, IDSucursal,			IDDeposito,				Fecha, FechaVencimientoTimbrado, IDMoneda, Cotizacion, Observacion, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, Saldo,			Anulado,FechaAnulado, IDUsuarioAnulado, Procesado, Devolucion, Descuento)
		Values(@IDTransaccionSalida, @IDTipoComprobante, @Numero, @NroComprobante,					@NroTimbrado, @IDProveedor,  @IDSucursalOperacion, @IDDepositoOperacion, @Fecha, @FechaVencimientoTimbrado, @IDMoneda, @Cotizacion, @Observacion, @Total, @TotalImpuesto, @TotalDiscriminado, @TotalDescuento, @Saldo, 0, NULL, NULL,						0,@Devolucion, @Descuento)

		print 'insertado'
										
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From NotaCreditoProveedor      Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Actualizamos el Stock
		If @Devolucion = 'True' Begin
		Exec SpNotaCreditoProveedorActualizarStock @IDTransaccion = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje output, @Procesado=@Procesado output
		print @Procesado
		End
		
		If @Procesado = 0 Begin
			set @Mensaje = 'No se puede ELIMINAR STOCK'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Actualizamos de saldos de las Compras
		Exec SpNotaCreditoProveedorActualizarCompra @IDTransaccion = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje output, @Procesado=@Procesado output
		
		If @Procesado = 0 Begin
			set @Mensaje = 'No se actualizaron las Compras :'
			return @@rowcount
		End
		
		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from NotaCreditoProveedor where IDTransaccion = @IDTransaccion)
		set @IDProveedor = (select IDProveedor from NotaCreditoProveedor where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from NotaCreditoProveedor where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from NotaCreditoProveedor where IDTransaccion = @IDTransaccion)
		declare @Comprobante varchar(16)= (select Comprobante from vNotaCreditoProveedor where IDTransaccion = @IDTransaccion)
		declare @VTipoComprobante varchar(16) = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from NotaCreditoProveedor where IDTransaccion = @IDTransaccion)
		declare @Condicion varchar(16) = 'CREDITO'

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,0,@IDProveedor,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ELI',@VTipoComprobante, @IDSucursal)


		--Eliminamos NotaCreditoProveedorCompra
		Delete NotaCreditoProveedorCompra where IDTransaccionNotaCreditoProveedor = @IDTransaccion 
		
		--Eliminamos impuesto
		Delete DetalleImpuesto where IDTransaccion= @IDTransaccion 
		
		--Eliminamos el detalle
		Delete DetalleNotaCreditoProveedor where IDTransaccion = @IDTransaccion				
				
		--Eliminamos el registro
		Delete NotaCreditoProveedor Where IDTransaccion = @IDTransaccion
						
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion	
		
		--Auditoria
		Exec SpLogSuceso  @Operacion=@Operacion, @Tabla='NOTACREDITOPROVEEDOR', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@NroComprobante 
					
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End







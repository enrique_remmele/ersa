﻿CREATE Procedure [dbo].[SpAccesoEspecificoPerfil]

	@IDPerfil tinyint,
	@IDAccesoEspecifico int,
	@IDMenu numeric(18,0),
	@Habilitar bit
	
As

Begin

	--Si no existe, agregamos
	If Not Exists(Select * From AccesoEspecificoPerfil Where IDPerfil=@IDPerfil And IDAccesoEspecifico=@IDAccesoEspecifico And IDMenu=@IDMenu) Begin
		Insert Into AccesoEspecificoPerfil(IDPerfil, IDAccesoEspecifico, IDMenu, Habilitar)
		Values(@IDPerfil, @IDAccesoEspecifico, @IDMenu, @Habilitar)
	End Else Begin
		Update AccesoEspecificoPerfil 
		Set Habilitar = @Habilitar
		Where IDPerfil=@IDPerfil And IDAccesoEspecifico=@IDAccesoEspecifico And IDMenu=@IDMenu
	
	End
		
End


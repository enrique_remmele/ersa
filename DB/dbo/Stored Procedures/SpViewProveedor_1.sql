﻿


CREATE Procedure [dbo].[SpViewProveedor]

	--Opciones
		
	--Identificadores
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int
	
As

Begin


	Select 
	V.ID, 
	V.Referencia,
	V.RazonSocial, 
	V.RUC, 
	V.NombreFantasia, 
	V.Direccion, 
	V.Telefono, 
	V.Barrio, 
	V.IDMoneda,
	V.Credito,
	V.Contado,
	V.Condicion,
	v.PlazoCredito,
	V.RazonSocialReferencia,
	
	--Cuenta Contable
	'IDCuentaContableCompra'=CC.ID,
	'CuentaCompra'=CC.Descripcion,
	V.CuentaContableCompra,
	V.SujetoRetencion
		
	From VProveedor V
	Left Outer Join VCuentaContable CC On V.CuentaContableCompra=CC.Codigo
	
	Where V.Estado='True'
	--And DateDiff(day, GetDate(), UltimaCompra) < 60 
	
	Order By V.RazonSocial
		
	
End








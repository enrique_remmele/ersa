﻿CREATE Procedure [dbo].[SpImportarProductoInventario]

	--Entrada
	--Identificadores
	@Producto varchar(200) = NULL,
	@CodigoBarra varchar(200) = NULL,
	@Referencia varchar(200) = NULL,
	
	--Configuraciones
	@UnidadCaja decimal(10,5) = 0,
	@Peso varchar(50) = '0',
	@Volumen varchar(50) = '0',
	
	@Impuesto varchar(50) = '10.00',
	@Estado bit = 'True',
	@ControlarExistencia bit = 'True',
	@CodigoCuentaContable varchar(50),
	@Costo varchar(50) = '0',
	@Procedencia varchar(100) = '',
	@ProcedenciaCodigo varchar(100) = '',
	@TipoProducto varchar(50) = '',
	@Clas1 varchar(50),
	@Clas2 varchar(50),
	@CcCompra as varchar(16),
	@CcCosto as varchar(16),
	@Ubicacion as varchar(16),
	@IDSucursal as tinyint,
	@Deposito as varchar(50),
	@UnidadMedida as varchar(20),
	@Actualizar bit
	
As

Begin

	Declare @vID int
	Declare @vOperacion varchar(50)
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	Declare @vIDImpuesto tinyint
	Declare @vIDTipoProducto tinyint
	--NUevo
	Declare @vIDClas1 integer
	Declare @vIDClas2 integer
	declare @vIDUbicacion as varchar(16)
	declare @vIDDeposito as integer
	declare @vIDUnidadMedida as integer
	Declare @vCuentaContableCompra varchar(16)
	Declare @vCuentaContableCosto varchar(16)

	Set	@vID = 0
	Set @vOperacion = 'INS'
	Set @vMensaje = 'Sin proceso'
	Set @vProcesado = 'False'
		
	If Exists(Select * From Producto Where Referencia=@Referencia) And @Actualizar='True' Begin
		Set @vID = (Select Top(1) ID From Producto Where Referencia=@Referencia)		
		Set @vOperacion = 'UPD'
	End 
	
	Set @Peso = '0' + @Peso
	Set @Impuesto = '0' + @Impuesto
	Set @Costo = '0' + @Costo
	
	Set @Peso = REPLACE(@Peso, ',','.')
	Set @Impuesto = REPLACE(@Impuesto, ',','.')
	Set @Impuesto = CONVERT(varchar(50), convert(decimal(10,2),@Impuesto))
	Set @Costo = REPLACE(@Costo, ',','.')
	Set @Costo = CONVERT(varchar(50), convert(money,@Costo))
	Set @vCuentaContableCompra = (select Codigo from CuentaContable where Codigo = @CcCompra)
	Set @vCuentaContableCosto = (select Codigo from CuentaContable where Codigo = @CcCosto)

	--Impuesto
	Begin
		If @Impuesto = '10.00' Begin
			Set @vIDImpuesto = 1
		End
		
		If @Impuesto = '5.00' Begin
			Set @vIDImpuesto = 2
		End
		
		If @Impuesto = '0.00' Begin
			Set @vIDImpuesto = 3
		End
		
	End
	
	--Cuenta Contable
	Begin
		
		--Unilever
		If @ProcedenciaCodigo = '001' Begin
			Set @CodigoCuentaContable = '411'
		End
		
		--Conti
		If @ProcedenciaCodigo = '002' Begin
			Set @CodigoCuentaContable = '412'
		End
		
	End
	-- Si no existe el Tipo de Producto
	If not Exists(Select Top(1) ID From TipoProducto Where Descripcion=@TipoProducto) Begin
	   Insert into TipoProducto (ID,Descripcion,Estado) values((select Max(id)+1 from TipoProducto),@TipoProducto,'True')	   
	End
		
	--Tipo de Producto
	If Exists(Select Top(1) ID From TipoProducto Where Descripcion=@TipoProducto) Begin
		Set @vIDTipoProducto = (Select Top(1) ID From TipoProducto Where Descripcion=@TipoProducto)
	End

-- Si no existe Clasificacion1
	If not Exists(Select Top(1) ID From Linea Where Descripcion=@Clas1) Begin
	   Insert into Linea(ID,Descripcion,Estado) values((select Isnull(Max(id)+1,1) from Linea),@Clas1,'True')
	End
		
	--Clasificacion1
	If Exists(Select Top(1) ID From Linea Where Descripcion=@Clas1) Begin
		Set @vIDClas1 = (Select Top(1) ID From Linea Where Descripcion=@Clas1)	   
	End	

	-- Si no existe Clasificacion2
	If not Exists(Select Top(1) ID From SubLinea Where Descripcion=@Clas2) Begin
	   Insert into SubLinea(ID,Descripcion,Estado) values((select Isnull(Max(id)+1,1) from SubLinea),@Clas2,'True')
	End
		
	--Clasificacion2
	If Exists(Select Top(1) ID From SubLinea Where Descripcion=@Clas2) Begin
		Set @vIDClas2 = (Select Top(1) ID From SubLinea Where Descripcion=@Clas2)
	   
	End	

	set @vIDDeposito = (select ID from Deposito where descripcion = @Deposito)

	-- Si no existe Zona deposito
	If not Exists(Select Top(1) ID From ZonaDeposito Where Descripcion=@Ubicacion and IDSucursal = @IDSucursal and IDDeposito = @vIDDeposito) Begin
	   Insert into ZonaDeposito(ID,IDSucursal,IDDeposito,Descripcion,Estado) values((select Isnull(Max(id)+1,1) from ZonaDeposito),@IDSucursal, @vIDDeposito, @Ubicacion,'True')	   
	End
		
	--Zonadeposito
	If Exists(Select Top(1) ID From ZonaDeposito Where Descripcion=@Ubicacion and IDSucursal = @IDSucursal and IDDeposito = @vIDDeposito) Begin
		Set @vIDUbicacion = (Select Top(1) ID From ZonaDeposito Where Descripcion=@Ubicacion and IDSucursal = @IDSucursal and IDDeposito = @vIDDeposito)
	End

	-- Si no existe Unidad de medida
	If not Exists(Select Top(1) ID From UnidadMedida Where Descripcion=@UnidadMedida) Begin
	   Insert into UnidadMedida(ID,Descripcion,Estado) values((select Isnull(Max(id)+1,1) from UnidadMedida),@UnidadMedida,'True')	   
	End
		
	--Unidad de medida
	If Exists(Select Top(1) ID From UnidadMedida Where Descripcion=@UnidadMedida) Begin
		Set @vIDUnidadMedida = (Select Top(1) ID From UnidadMedida Where Descripcion=@UnidadMedida)
	End	


	EXEC SpProducto
	@ID = @vID,
	@Descripcion = @Producto,
	@CodigoBarra = @CodigoBarra,
	@Referencia = @Referencia,
	
	@IDUnidadMedida = @vIDUnidadMedida,
	@UnidadPorCaja = @UnidadCaja,
	@Peso = @Peso,
	@Volumen = @Volumen,
	
	@IDImpuesto = @vIDImpuesto,
	@Estado = 'True',
	@ControlarExistencia = 'True',
	
	@CuentaContableVenta = @CodigoCuentaContable,
	@IDUsuario = 1,
	@IDTerminal = 1,
	@IDTipoProducto = @vIDTipoProducto,
		
	@Operacion = @vOperacion,
	
	@Mensaje = @vMensaje OUTPUT,
	@Procesado = @vProcesado OUTPUT

	set @vId = (select Id from producto where Referencia = @Referencia)
	--Actualizar el Costo
	If (Select ISNull(CostoCG,0) FRom Producto Where ID=@vID) = 0 Begin
		If @vOperacion = 'UPD' Begin
			Update Producto Set CostoCG=@Costo,
								CostoPromedio=@Costo,
								Estado=@Estado
			Where ID=@vID
		End
	End

		-- Actualizar Otros
	Update Producto
	Set CuentaContableCompra = @vCuentaContableCompra,
	CuentaContableCosto = @vCuentaContableCosto,
	IDLinea = @vIDClas1,
	IDSubLinea = @vIDClas2,
	Vendible = 'False'
	where ID = @vID
	
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	

	
End

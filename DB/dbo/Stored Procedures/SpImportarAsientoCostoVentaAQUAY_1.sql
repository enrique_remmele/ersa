﻿
CREATE Procedure [dbo].[SpImportarAsientoCostoVentaAQUAY]

	--Entrada
	--Cabecera
	@Comprobante varchar(50),
	@Fecha date,
	@Debe money,
	@Haber money,
	@CuentaContableERSA varchar(50)	
	
As

Begin
	
	Declare @vMensaje varchar(200) = ''
	Declare @vProcesado bit = 'False'

	Declare @vIDTransaccion int = 0
	Declare @vIDCuentaContable int
	Declare @vID int
	Declare @vObservacion varchar(200)
	Declare @vIDSucursal int
	Declare @vTipoComprobante varchar(100)
	Declare @vNroComprobante varchar(100)
	Declare @vIDCentroCosto int
	Declare @vIDUnidadNegocio int
	Declare @vImporteHaber Money
	Declare @vImporteDebe Money

	 
	

	if not exists (select * from venta where idsucursal = 5 and comprobante = @Comprobante and FechaEmision = @Fecha and Procesado = 1) begin
		Set @vMensaje = concat('No se encontro el comprobante :', @Comprobante)
		Set @vProcesado = 'False'
		goto Salir
	end

	if exists (select * from venta where idsucursal = 5 and comprobante = @Comprobante and FechaEmision = @Fecha and Anulado = 1) begin
		Set @vMensaje = concat('El comprobante esta anulado :', @Comprobante)
		Set @vProcesado = 'False'
		goto Salir
	end
	
	Set @vIDTransaccion = (select IDTransaccion from venta where idsucursal = 5 and comprobante = @Comprobante and FechaEmision = @Fecha)
	

	if not exists (select * from asiento where idtransaccion = @vIDTransaccion) begin
		Set @vMensaje = concat('El comprobante no tiene asiento generado :', @Comprobante)
		Set @vProcesado = 'False'
		goto Salir
	end

	if not exists (select * from detalleasiento where idtransaccion = @vIDTransaccion) begin
		Set @vMensaje = concat('El comprobante no tiene detalles de asiento generado :', @Comprobante)
		Set @vProcesado = 'False'
		goto Salir
	end

	Set @vIDCuentaContable = (Select ID from CuentaContable where codigo = @CuentaContableERSA)
	Set @vID = (Select max(ID)+1 from DetalleAsiento where IDTransaccion = @vIDTransaccion)
	Set @vObservacion = (Select Top(1) Observacion from DetalleAsiento where IDTransaccion = @vIDTransaccion)
	Set @vIDSucursal = (Select Top(1) IDSucursal from DetalleAsiento where IDTransaccion = @vIDTransaccion)
	Set @vTipoComprobante = (Select Top(1) TipoComprobante from DetalleAsiento where IDTransaccion = @vIDTransaccion)
	Set @vNroComprobante = (Select Top(1) NroComprobante from DetalleAsiento where IDTransaccion = @vIDTransaccion)
	Set @vIDCentroCosto = (Select IDCentroCosto from CuentaContable where codigo = @CuentaContableERSA)
	Set @vIDUnidadNegocio = (Select IDUnidadNegocio from CuentaContable where codigo = @CuentaContableERSA)


	if not exists (select * from DetalleAsiento where IDTransaccion = @vIDTransaccion and CuentaContable=@CuentaContableERSA) begin
		insert into detalleasiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe,Observacion,IDSucursal,TipoComprobante, NroComprobante,IDCentroCosto, IDUnidadNegocio)
							values(@vIDTransaccion, @vID, @vIDCuentaContable, @CuentaContableERSA, isnull(@Haber,0), isnull(@Debe,0), isnull(@Haber,0)+isnull(@Debe,0), @vObservacion, @vIDSucursal,@vTipoComprobante, @vNroComprobante, @vIDCentroCosto, @vIDUnidadNegocio)
	
		--Actualizamos la cabecera, el total
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@vIDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@vIDTransaccion),0)
	
		Set @vImporteHaber = ROUND(@vImporteHaber, 0)
		Set @vImporteDebe = ROUND(@vImporteDebe, 0)
	
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vImporteHaber - @vImporteDebe
		Where IDTransaccion=@vIDTransaccion
	
	end 
	else begin
		Update detalleasiento
		set Credito = Credito + isnull(@Haber,0),
				Debito = Debito + isnull(@Debe,0)
		where IDTransaccion = @vIDTransaccion
		and CuentaContable = @CuentaContableERSA


		--Actualizamos la cabecera, el total
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@vIDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@vIDTransaccion),0)
	
		Set @vImporteHaber = ROUND(@vImporteHaber, 0)
		Set @vImporteDebe = ROUND(@vImporteDebe, 0)
	
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vImporteHaber - @vImporteDebe,
							Bloquear = 1
		Where IDTransaccion=@vIDTransaccion

	end

	



Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End



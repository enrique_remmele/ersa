﻿CREATE Procedure [dbo].[SpViewEstadoResultado]

	--Entrada
	@Año smallint,
	@Mes tinyint,
	
	@IDSucursal int = 0,
	@IDUnidadNegocio int = 0,
	@Factor int
	
As

Begin

	--variables
	Declare @vID tinyint
	Declare @vM varchar(50)
	Declare @vCodigo varchar(50)
	Declare @vDenominacion varchar(200)
	Declare @vCategoria tinyint
	Declare @vImputable bit
	Declare @vDebito money
	Declare @vCredito money
	Declare @vSaldo money
	Declare @vDebitoAnterior money
	Declare @vCreditoAnterior money
	Declare @vSaldoAnterior money
	Declare @vDebitoActual money
	Declare @vMovimientoMes money
	Declare @vCreditoActual money
	Declare @vSaldoActual money
	Declare @vIDUnidadNegocio int
	
	Declare @vMovimientoCierre money
	Declare @vCreditoCierre money
	Declare @vDebitoCierre money
	
	--Crear la tabla
	Create table #EstadoResultado(ID tinyint,
								Año smallint,
								Mes tinyint,
								IDSucursal int,
								M varchar(50),
								Codigo varchar(50),
								Denominacion varchar(200),
								Categoria tinyint,
								Imputable bit,
								MostrarCodigo varchar(50),
								
								IDUnidadNegocio int,
								SaldoAnterior money,
								MovimientoMes money,
								MovimientoCierre money,
								SaldoActual money
								)
								
	set @vID = (Select IsNull(MAX(ID)+1,1) From #EstadoResultado)
	
	If @Factor = 0 Begin
		Set @Factor = 1
	End
	
	Declare db_cursor cursor for
	Select	CC.Codigo, CC.Descripcion, CC.Categoria, CC.Imputable, Isnull(CC.IDUnidadNegocio,0)	
	From VCuentaContable CC	
	Where (SUBSTRING(CC.Codigo, 0, 2) Between '4' And '9')
	And (Case When @IDUnidadNegocio = 0 then 0 else IDUnidadNegocio end) = (Case When @IDUnidadNegocio = 0 then 0 else @IDUnidadNegocio end)
	Open db_cursor   
	Fetch Next From db_cursor Into	@vCodigo, @vDenominacion, @vCategoria, @vImputable, @vIDUnidadNegocio
	While @@FETCH_STATUS = 0 Begin 
		
		--Tipo de Cuenta
		Declare @vCuentaTipo varchar(50)
		Declare @vPrefijo varchar(50)

		Set @vPrefijo = SubString(@vCodigo, 0, 2)

		--Cuentas del Debe
		If @vPrefijo = '1' Or @vPrefijo = '5' Begin
			Set @vCuentaTipo = 'DEBE'
		End

		--Cuentas del Haber
		If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
			Set @vCuentaTipo = 'HABER'
		End
					
		Set @vDebitoAnterior = 0
		Set @vCreditoAnterior = 0
		
		--Saldo Anterior
		If @Mes > 1 Begin
			
			--Toda la Empresa
			If @IDSucursal = 0 Begin
				Set @vDebitoAnterior = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes<@Mes And Cuenta=@vCodigo),0)
				Set @vCreditoAnterior = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes<@Mes And Cuenta=@vCodigo),0)
			End
			
			--Por Sucursal
			If @IDSucursal > 0 Begin
				Set @vDebitoAnterior = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes<@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
				Set @vCreditoAnterior = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes<@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
			End
			
		End
		
		--Saldo Actual
		--Toda la Empresa
		If @IDSucursal = 0 Begin
			Set @vDebito = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes=@Mes And Cuenta=@vCodigo),0)
			Set @vCredito = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes=@Mes And Cuenta=@vCodigo),0)
		End
		
		--Por Sucursal
		If @IDSucursal > 0 Begin
			Set @vDebito = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes=@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
			Set @vCredito = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes=@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
		End
		
--Saldo de CierreContable
		if @IdSucursal = 0 begin
			Set @vDebitoCierre = Isnull((select DA.Debito from Asiento A
								join DetalleAsiento DA on DA.IDTransaccion = A.IDTransaccion
								join CierreReapertura CR on CR.IDtransaccion = A.Idtransaccion
								where CR.Comprobante = 'CIERRE'
								and DA.CuentaContable = @vCodigo
								and Anho = @Año),0)
			Set @vCreditoCierre = Isnull((select DA.Credito from Asiento A
								join DetalleAsiento DA on DA.IDTransaccion = A.IDTransaccion
								join CierreReapertura CR on CR.IDtransaccion = A.Idtransaccion
								where CR.Comprobante = 'CIERRE'
								and DA.CuentaContable = @vCodigo
								and Anho = @Año),0)
	   End
	   If @IdSucursal > 0 begin
	   Set @vDebitoCierre = Isnull((select DA.Debito from Asiento A
								join DetalleAsiento DA on DA.IDTransaccion = A.IDTransaccion
								join CierreReapertura CR on CR.IDtransaccion = A.Idtransaccion
								where CR.Comprobante = 'CIERRE'
								and DA.CuentaContable = @vCodigo
								and CR.IDSucursal =@IdSucursal
								and Anho = @Año),0)
				Set @vCreditoCierre = Isnull((select DA.Credito from Asiento A
								join DetalleAsiento DA on DA.IDTransaccion = A.IDTransaccion
								join CierreReapertura CR on CR.IDtransaccion = A.Idtransaccion
								where CR.Comprobante = 'CIERRE'
								and DA.CuentaContable = @vCodigo
								and CR.IDSucursal =@IdSucursal
								and Anho = @Año),0)
	   end		
		
		If @vCuentaTipo = 'DEBE' Begin
			Set @vSaldoAnterior = IsNUll(@vDebitoAnterior  - @vCreditoAnterior,0)
			Set @vSaldoActual = @vSaldoAnterior + (@vDebito - @vCredito) + (@vCreditoCierre - @vDebitoCierre)
			Set @vMovimientoMes = @vDebito - @vCredito
			Set @vMovimientoCierre = @vCreditoCierre - @vDebitoCierre
		End
		
		If @vCuentaTipo = 'HABER' Begin
			Set @vSaldoAnterior = IsNUll(@vCreditoAnterior-@vDebitoAnterior,0) 
			Set @vSaldoActual = @vSaldoAnterior + (@vCredito - @vDebito) + (@vDebitoCierre - @vCreditoCierre)
			Set @vMovimientoMes = @vCredito - @vDebito 
			set @vMovimientoCierre = @vDebitoCierre - @vCreditoCierre
		End
		
		Set @vSaldoAnterior = @vSaldoAnterior / @Factor
		Set @vSaldoActual = @vSaldoActual / @Factor 
		
		Set @vDebito = @vDebito / @Factor
		Set @vCredito = @vCredito / @Factor
		Set @vMovimientoMes = @vMovimientoMes / @Factor
		set @vMovimientoCierre = @vMovimientoCierre / @Factor
			
		--Si ya existe, actualizar
		--Toda la Empresa
		If @IDSucursal = 0 Begin
			If Exists(Select * From #EstadoResultado Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo) Begin
			
				Update #EstadoResultado Set 
				SaldoAnterior=SaldoAnterior+@vSaldoAnterior,
				MovimientoMes=MovimientoMes+@vMovimientoMes,
				MovimientoMes=MovimientoCierre+@vMovimientoCierre,
				SaldoActual=SaldoActual+@vSaldoActual
				Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo
				
			End Else Begin
				Insert Into #EstadoResultado(ID, Año, Mes, M, Codigo, Denominacion, Categoria, Imputable, SaldoAnterior, MovimientoMes, MovimientoCierre, SaldoActual, MostrarCodigo, IDUnidadNegocio)
				Values(@vID, @Año, @Mes, dbo.FMes(@Mes), @vCodigo, @vDenominacion, @vCategoria, @vImputable, @vSaldoAnterior, @vMovimientoMes,@vMovimientoCierre, @vSaldoActual, '1', @vIDUnidadNegocio)
			End
		End
		
		--Por Sucursal
		If @IDSucursal > 0 Begin
			If Exists(Select * From #EstadoResultado Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo And IDSucursal=@IDSucursal) Begin
			
				Update #EstadoResultado Set 
				SaldoAnterior=SaldoAnterior+@vSaldoAnterior,
				MovimientoMes=MovimientoMes+@vMovimientoMes,
				SaldoActual=SaldoActual+@vSaldoActual,
				MovimientoCierre = MovimientoCierre+@vMovimientoCierre
				Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo
				
			End Else Begin
				Insert Into #EstadoResultado(ID, Año, Mes, IDSucursal, M, Codigo, Denominacion, Categoria, Imputable, SaldoAnterior, MovimientoMes,MovimientoCierre, SaldoActual, MostrarCodigo, IDUnidadNegocio)
				Values(@vID, @Año, @Mes, @IDSucursal, dbo.FMes(@Mes), @vCodigo, @vDenominacion, @vCategoria, @vImputable, @vSaldoAnterior, @vMovimientoMes,@vMovimientoCierre, @vSaldoActual, '1',@vIDUnidadNegocio)
			End
		End
		
Siguiente:
		
		Fetch Next From db_cursor Into	@vCodigo, @vDenominacion, @vCategoria, @vImputable, @vIDUnidadNegocio
		
	End

	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor		   		
			
	Select 
	* 
	From #EstadoResultado 
	Where ID=@vID 
	And (Case When @IDUnidadNegocio = 0 then 0 else IDUnidadNegocio end) = (Case When @IDUnidadNegocio = 0 then 0 else @IDUnidadNegocio end)
	order by CODIGO
	
End

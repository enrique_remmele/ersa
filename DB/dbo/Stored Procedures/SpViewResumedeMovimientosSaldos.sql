﻿CREATE Procedure [dbo].[SpViewResumedeMovimientosSaldos]

	--Entrada
	@Fecha Date,
	
	
	--Salida
	@Mensaje varchar(200) output ,
	@IDTransaccionSalida numeric(18,0) output
As

Begin
    
    ---Variables para CALCULAR SALDO 
	declare @vSaldo money
	declare @vTotalVenta money
	declare @vTotalNotaDebito money
	declare @vTotalNotaCredito money
	declare @vTotalCobranza money
	
	--Declarar variables Variables
	declare @vIDCliente int
	declare @vID tinyint
	declare @vIDTransaccion Numeric (18,0)
	declare @vReferencia varchar (50)
	declare @vCliente varchar (100)

	set @vID = (Select IsNull(MAX(ID)+1,1) From ResumenMovimientoSaldo )
	
	--Insertar datos	
	Begin
	
		Declare db_cursor cursor for
		Select ID, RazonSocial From VCliente V Order By RazonSocial
		Open db_cursor   
		Fetch Next From db_cursor Into  @vIDCliente, @vCliente
		While @@FETCH_STATUS = 0 Begin 
		 
			set @vTotalVenta  = IsNull((Select Sum(Total) From Venta  Where IDCliente=@vIDCliente And FechaEmision<=@Fecha),0)
			Set @vTotalNotaCredito=IsNull((Select Sum(Importe) From VNotaCreditoVenta Where IDCliente=@vIDCliente And Fecha<=@Fecha),0)
			Set @vTotalNotadebito=IsNull((Select Sum(Importe) From VNotaDebitoVenta Where IDCliente=@vIDCliente And Fecha<=@Fecha),0)
			Set @vTotalCobranza=IsNull((Select  Sum(Importe) From VVentaDetalleCobranza Where IDCliente=@vIDCliente And FechaCobranza<=@Fecha),0)
						
			set @vSaldo = (@vTotalVenta + @vTotalNotaDebito) - (@vTotalCobranza + @vTotalNotaCredito) 
			
			--If @vTotalNotaCredito > 0 Begin
			--	print @vCliente 
			--	print 'Ventas: ' + convert(varchar(50), @vTotalVenta)
			--	print 'NC: ' + convert(varchar(50), @vTotalNotaCredito)
			--	print 'ND: ' + convert(varchar(50), @vTotalNotaDebito)
			--	print 'Cobranzas: ' + convert(varchar(50), @vTotalCobranza)
			--	print 'Saldo: ' + convert(varchar(50), @vSaldo)
				
			--	print ''
			--End
			Insert Into  ResumenMovimientoSaldo(ID,IDCliente, RazonSocial, Saldo) 
			Values (@vID,@vIDCliente, @vCliente,@vSaldo)
			
			Fetch Next From db_cursor Into  @vIDCliente, @vCliente
			
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		
	End	
	set @Mensaje = 'Sin error!'
	Set @IDTransaccionSalida = @vID
	
	
End
	




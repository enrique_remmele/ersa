﻿

CREATE Procedure [dbo].[SpTerminal]

	--Entrada
	@ID int,
	@IDSucursal int = NULL,
	@IDDeposito int,
	@Descripcion varchar(50),
	@CodigoActivacion varchar(300) = NULL,
	@Impresora varchar(50) = NULL,
	
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal int = NULL,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	Declare @vIDSucursal int
	
	If @IDSucursal Is Null Begin
		Set @vIDSucursal=(Select IDSucursal From Deposito Where ID = @IDDeposito)		
		Set @IDSucursal = @vIDSucursal
	End
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Terminal Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
				
		--Obtenemos el nuevo ID
		declare @vID int
		set @vID = (Select IsNull((Max(ID)+1), 1) From Terminal)

		--Insertamos
		Insert Into Terminal(ID, IDSucursal, IDDeposito, Descripcion, Impresora, Activado, CodigoActivacion)
		Values(@vID, @IDSucursal, @IDDeposito, @Descripcion, @Impresora, 'True', @CodigoActivacion)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TERMINAL', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From Terminal Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Terminal Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Terminal Set IDSucursal=@IDSucursal, 
							IDDeposito=@IDDeposito, 
							Descripcion=@Descripcion, 
							Impresora=@Impresora
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TERMINAL', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Terminal Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Transaccion
		if exists(Select * From Transaccion Where IDTerminal=@ID) begin
			set @Mensaje = 'El registro tiene transacciones asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Terminal
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TERMINAL', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End



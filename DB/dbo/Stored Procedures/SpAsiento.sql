﻿CREATE Procedure [dbo].[SpAsiento]

	--Entrada
	@Operacion varchar(50),
	@IDTransaccion numeric(18,0),
	@Numero int = NULL,
	@IDCiudad int = NULL,
	@IDSucursal tinyint = NULL,
	@IDDeposito tinyint = NULL,
	@Fecha date = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money = NULL,
	@IDTipoComprobante	smallint = NULL,
	@NroComprobante	varchar(50) = NULL,
	@Detalle varchar(500) = NULL,
	@Debito	money	= NULL,
	@Credito	money	= NULL,
	@Saldo	money	= NULL,
	@Total	money	= NULL,
	@Bloquear	bit	= 'False',
	@IDCentroCosto	tinyint	= NULL,
	
	--
	@IDUsuario int = NULL,

	--Caja
	@IDTransaccionCaja numeric(18,0) = 0,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
			
As

Begin
	
	--Validar Feha Operacion
	Declare @vIDUsuario as integer
	Declare @vIDOperacion as integer
	Declare @vFecha date

	--BLOQUES
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		Select	@vIDUsuario=IDUsuario,
			@vIDOperacion=IDOperacion
		From Transaccion Where ID = @IDTransaccion

		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @vIDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			Return @@rowcount
		End

		--Validar
		--Transaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro asociado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Caja Cerrada
		--If (Select Habilitado From Caja Where IDTransaccion=@IDTransaccionCaja) = 'False' Begin
		--	set @Mensaje = 'La caja al que se hace referencia esta cerrada! Seleccione otra caja.'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		--Si ya existe asiento para esta transaccion
		If Exists(Select * From Asiento Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'Ya existe un asiento para este documento! No se puede volver a cargar hasta que se elimine el existente.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Optenemos la Sucursal
		Declare @vIDSucursal tinyint
		Begin
			
			If @IDCiudad Is Not Null Begin
				Set @vIDSucursal = (Select Top(1) ID From Sucursal Where IDCiudad=@IDCiudad)
			End
			
			If @IDDeposito Is Not Null Begin
				Set @vIDSucursal = (Select top(1) IDSucursal From Deposito Where ID=@IDDeposito)
			End
			
			If @IDSucursal Is Not Null Begin
				Set @vIDSucursal = @IDSucursal
			End
			
		End
		
		--Insertamos
		Declare @vNumero int
		Set @vNumero = (Select IsNull(Max(Numero)+1,1) From Asiento)
		---Todos los asientos manuales deben estar bloqueados
		--Set @Bloquear = 'True'
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Debito, Credito, Saldo, Total, Anulado, IDCentroCosto, Conciliado, Bloquear)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @Fecha, @IDMoneda, @Cotizacion, @IDTipoComprobante, @NroComprobante, @Detalle, @Debito, @Credito, @Saldo, @Total, 'False', @IDCentroCosto, 'False', @Bloquear)
		
		Update Transaccion Set IDTransaccionCaja=@IDTransaccionCaja
		where ID=@IDTransaccion

		--Actualizar el nombre del Proveedor en la OP
		 If Exists(Select * From OrdenPago Where IDTransaccion=@IDTransaccion) Begin
			Update Asiento
			set Detalle = concat(Detalle, ' Prov: ', (select Proveedor from VOrdenPago where IDTransaccion = @IDTransaccion))
			where IDTransaccion = @IDTransaccion
		End

		--Actualizar el nombre del Proveedor en la Entrega de Cheque
		 If Exists(Select * From EntregaChequeOP Where IDTransaccion=@IDTransaccion) Begin
			Update Asiento
			set Detalle = concat(Detalle, ' Prov: ', (select Proveedor from VEntregaChequeOP EC 
						Join VordenPago OP on OP.IDTransaccion = EC.IDTransaccionOP
						and EC.IDTransaccion =  @IDTransaccion))
			where IDTransaccion = @IDTransaccion
		End

		--Actualizar el nombre del Proveedor en el asiento de gasto
		 If Exists(Select * From Gasto Where IDTransaccion=@IDTransaccion) Begin
			Update Asiento
			set Detalle = concat(Detalle, ' Prov: ', (select Proveedor from vGasto where IDTransaccion = @IDTransaccion))
			where IDTransaccion = @IDTransaccion
		End

		--Actualizar el nombre del Proveedor en el asiento de entrada por compra
		 If Exists(Select * From Compra Where IDTransaccion=@IDTransaccion) Begin
			Update Asiento
			set Detalle = concat(Detalle, ' Prov: ', (select Proveedor from vCompra where IDTransaccion = @IDTransaccion))
			where IDTransaccion = @IDTransaccion
		End


											
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'UPD' Begin
	
		Set @vIDOperacion = (Select IDOperacion	From Transaccion Where ID = @IDTransaccion)
		Set @vFecha = (Select Fecha	From Asiento Where IDTransaccion = @IDTransaccion)
		--Set @IDUsuario = (Select IDUsuario	From Transaccion Where ID = @IDTransaccion)

		If dbo.FValidarFechaOperacion(@IDSucursal, @vFecha, @IDUsuario, @vIDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			Return @@rowcount
		End

		--Validamos
		--IDTransaccion
		If Not Exists(Select * From Asiento Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Caja Cerrada
		--If (Select Habilitado From Caja Where IDTransaccion=@IDTransaccionCaja) = 'False' Begin
		--	set @Mensaje = 'La caja al que se hace referencia esta cerrada! Seleccione otra caja.'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		--Solo si el asiento no esta cerrado
		If (Select IsNull(Conciliado, 0) From Asiento Where IDTransaccion=@IDTransaccion)=1 Begin
			set @Mensaje = 'El asiento ya esta conciliado! No se puede modificar.'
			set @Procesado = 'False'
			return @@rowcount
		End
		print '185'			
		Update Asiento set  Detalle=@Detalle,
							NroComprobante=@NroComprobante,
							Bloquear = @Bloquear,
							IDTipoComprobante = @IDTipoComprobante
		Where IDTransaccion=@IDTransaccion
		print '190'		
			
		update detalleasiento 
			set observacion = Substring(@Detalle,1,200)
			where IDTransaccion=@IDTransaccion
					
		Update Transaccion set IDTransaccionCaja=@IDTransaccionCaja
		Where ID=@IDTransaccion
		print '197'			
		--Eliminamos el registro de detalle
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
			
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	If @Operacion = 'DEL' Begin
	
		Set @vIDOperacion = (Select IDOperacion	From Transaccion Where ID = @IDTransaccion)
		
		Select	@IDSucursal=IDSucursal,
				@vFecha=Fecha
		From Asiento Where IDTransaccion = @IDTransaccion

		Select @IDSucursal, @vFecha, @IDUsuario, @vIDOperacion

		set @Fecha = (Select Fecha from asiento where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @vFecha, @IDUsuario, @vIDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			Return @@rowcount
		End

		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Solo si el asiento no esta cerrado
		If (Select IsNull(Conciliado, 0) From Asiento Where IDTransaccion=@IDTransaccion)=1 Begin
			set @Mensaje = 'El asiento ya esta conciliado! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Eliminamos el registro de detalle
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		
		--Eliminamos el Asiento	
		Delete From Asiento Where IDTransaccion=@IDTransaccion
				
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	return @@rowcount
		
End





﻿-- Stored Procedure

CREATE Procedure [dbo].[SpDetalleCierreAperturaStock]

	--Entrada
	@Numero numeric(18,0),
	@IDTransaccion numeric(18,0),
	@Fecha date

As

Begin

	SET NOCOUNT ON


	Begin

		Declare @vCantidad as decimal(18,2) = 0
		Declare @vCosto as money = 0
		Declare @vIDTransaccion numeric(18,0)
		Declare @vIDProducto int
		Declare @vIDSucursal tinyint = 0
		Declare @vIDDeposito tinyint = 0
		Declare @vFechaUltimoCierre date = NULL
		Declare @vCantidadUltimoCierre numeric(18,0)
		Declare @vIDTransaccionUltimoCierre numeric(18,0)

		Declare db_cursor cursor for

		Select P.ID
		From VProducto P
		Where P.ControlarExistencia = 'True'

		Open db_cursor   

		Fetch Next From db_cursor Into @vIDProducto
		While @@FETCH_STATUS = 0 Begin 

			Set @vCantidad = 0
			Set @vCosto = 0

			Select Top(1) @vCantidad = ISNULL(CantidadSaldo,0),
						  @vCosto=ISNULL(CostoPromedio,0) 
			from Kardex Where IDProducto=@vIDProducto And Fecha<=@Fecha 
			Order by Fecha Desc, Indice Desc

			--Insertar registro									
			Insert into DetalleCierreAperturaStock (IDTransaccion, IDProducto, IDSucursal, IDDeposito, Existencia, Costo)
			Values(@IDTransaccion, @vIDProducto, @vIDSucursal, @vIDDeposito, @vCantidad, @vCosto)

			--Actualiza el Kardex			
			EXEC SpKardex
				@Fecha = @Fecha,
				@IDTransaccion = @IDTransaccion,
				@IDProducto = @vIDProducto,
				@ID = 0,
				@Orden = 100,
				@CantidadEntrada = @vCantidad,
				@CantidadSalida = @vCantidad,
				@CostoEntrada = @vCosto,
				@CostoSalida = @vCosto,
				@IDSucursal = @vIDSucursal,
				@Comprobante = @Numero

			--Print 'Producto: ' + convert(varchar(50),  @vIDProducto) + '	- Cantidad:	' + convert(varchar(50),  @vCantidad) + '	 - Costo: ' + convert(varchar(50),  @vCosto) + '	- Fecha Ult.: ' + convert(varchar(50),  @vFechaUltimoCierre)
			Fetch Next From db_cursor Into @vIDProducto

		End

		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor

	End

End

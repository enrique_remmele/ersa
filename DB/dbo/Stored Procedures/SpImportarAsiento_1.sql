﻿CREATE Procedure [dbo].[SpImportarAsiento]

	--Entrada
	--Cabecera
	@Numero int,
	@Sucursal varchar(50),
	@Fecha date,
	@Observacion varchar(200),
	@Operacion varchar(200),
	
	--Detalle
	@CuentaContable varchar(50),
	@TipoComprobante varchar(50),
	@NroComprobante varchar(50),
	@Importe money,
	@TipoMovimiento tinyint,
	@Conciliado bit,
	
	--Opciones
	@Actualizar Bit = 'False'
		
	
As

Begin

	--Variables
	Declare @vMensaje varchar(200)
	Declare @vProcesado bit
	
	Declare @vIDSucursal int
	Declare @vIDDeposito int
	Declare @vIDOperacion int
	
	Declare @vIDTipoDocumento int
	Declare @vIDTransaccion numeric(18,0)
	Declare @vExiste bit
	
	Set @vMensaje = 'No se proceso'
	Set @vProcesado = 'False'
	Set @vExiste = 'False'
	
	--Codigos de Transaccion
	Begin
		
		--Sucursal
		If Not Exists(Select * From Sucursal Where Codigo=@Sucursal) Begin
			Set @vMensaje = 'No se encuentra la sucursal!'
			Set @vProcesado = 'False'
			Goto Salir			
		End
		
		--Depossito
		Set @vIDSucursal = (Select Top(1) ID From Sucursal Where Codigo=@Sucursal)
		Set @vIDDeposito = (Select Top(1) ID From Deposito Where IDSucursal=@vIDSucursal)
		
		--Operacion
		If Not Exists(Select * From Operacion Where Descripcion='ASIENTOS IMPORTADOS') Begin
			
			Set @vIDOperacion = (Select ISNULL(Max(ID)+1,1) From Operacion)
			
			Insert Into Operacion(ID, Descripcion, Codigo, FormName)
			Values(@vIDOperacion, 'ASIENTOS IMPORTADOS', 'AOLD', '')
			
		End
		
		Set @vIDOperacion = (Select Top(1) ID From Operacion Where Descripcion='ASIENTOS IMPORTADOS')
		
	End
	
	--Cabecera
	Begin
		
		--Verificamos si existe
		If Not Exists(Select * From AsientoImportado Where Numero=@Numero) Begin
			
			--Creamos la transacciion
			EXEC SpTransaccion
				@IDUsuario = 1,
				@IDSucursal = @vIDSucursal,
				@IDDeposito = @vIDDeposito,
				@IDTerminal = 1,
				@IDOperacion = @vIDOperacion,
				@MostrarTransaccion = 'False',
				@Mensaje = @vMensaje OUTPUT,
				@Procesado = @vProcesado OUTPUT,
				@IDTransaccion = @vIDTransaccion OUTPUT
			
			--Cargamos la cabecera
			Insert Into AsientoImportado(IDTransaccion, IDSucursal, Numero, Fecha, Cotizacion, Observacion, Conciliado, Operacion, Total)
			Values(@vIDTransaccion, @vIDSucursal, @Numero, @Fecha, '1', @Observacion, @Conciliado, @Operacion, @Importe)
			
			Set @vExiste = 'False'
						
		End Else Begin
			
			--Si existe, eliminamos los asientos
			Set @vIDTransaccion = (Select Top(1) IDTransaccion From AsientoImportado Where Numero=@Numero)			
			Set @vExiste = 'True'
			
		End
		
	End
	
	--Asiento
	Begin
	
		--Variables
		Declare @vIDMoneda int
		Declare @vIDTipoComprobante int
		Declare @vDebito money
		Declare @vCredito money
		
		--Obtener valores
		Set @vIDTransaccion = (Select Top(1) IDTransaccion From AsientoImportado Where Numero=@Numero)
		Set @vIDMoneda = 1
		If Not Exists(Select * From TipoComprobante Where IDOperacion=@vIDOperacion And Codigo=@TipoComprobante) Begin
			Set @vIDTipoComprobante = (Select ISNULL(Max(ID)+1,1) From TipoComprobante)
			Insert Into TipoComprobante(ID, Cliente, Proveedor, IDOperacion, Descripcion, Codigo, Resumen, Estado, ComprobanteTimbrado,CalcularIVA,IVAIncluido,LibroVenta,LibroCompra,Signo,Autonumerico)
			Values(@vIDTipoComprobante, 'False', 'False', @vIDOperacion, @TipoComprobante, @TipoComprobante, @TipoComprobante, 'True', 'False', 'True', 'True', 'False', 'False', '+', 'False')
		End
		
		Set @vIDTipoComprobante = (Select Top(1) ID From TipoComprobante Where IDOperacion=@vIDOperacion And Codigo=@TipoComprobante)
				
		If @TipoMovimiento = 1 Begin
			Set @vDebito = @Importe
			Set @vCredito = 0
		End
		
		If @TipoMovimiento = 2 Begin
			Set @vDebito = 0
			Set @vCredito = @Importe
		End
		
		--Cabecera
		If @vExiste = 'False' Begin
			
			Declare @vNumeroAsiento int
			Set @vNumeroAsiento = (Select ISNULL(Max(Numero)+1,1) From Asiento)
			
			Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado, FechaConciliado, IDUsuarioConciliado, Bloquear)
			Values(@vIDTransaccion, @vNumeroAsiento, @vIDSucursal, @Fecha, @vIDMoneda, 1, @vIDTipoComprobante, @NroComprobante, @Observacion, @Importe, 0, 0, 0, 'False', 0, @Conciliado, @Fecha, 1, 'True')
			
		End

		--Detalle
		Declare @vID smallint
		Declare @vIDCuentaContable int
		
		Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@vIDTransaccion)
		Set @vIDCuentaContable = (Select ID From VCuentaContable Where Codigo=@CuentaContable And PlanCuentaTitular='True')
		
		--Insertar el detalle del Asiento
		Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion, IDSucursal, TipoComprobante, NroComprobante)		
		Values(@vIDTransaccion, @vID, @vIDCuentaContable, @CuentaContable, @vCredito, @vDebito, @Importe, @Observacion, @vIDSucursal, @TipoComprobante, @NroComprobante)
		
		--Actualizar el total del Asiento
		Update Asiento Set Credito = Credito + @vCredito,
							Debito = Debito + @vDebito
		Where IDTransaccion=@vIDTransaccion
		
		Update Asiento Set Total = Debito
							
		Where IDTransaccion=@vIDTransaccion
		
		--Hayamos el total del comprobante
		Declare @vTotal money
		Set @vTotal = (Select Top(1) Total From Asiento Where IDTransaccion=@vIDTransaccion)
		
		--Actualizamos la Cabecera
		Update AsientoImportado Set Total = @vTotal
		Where IDTransaccion = @vIDTransaccion			
			
		Set @vMensaje = 'Registro guardado'
		Set @vProcesado = 'True'
		
	End
	
	--Agregar el detalle
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End



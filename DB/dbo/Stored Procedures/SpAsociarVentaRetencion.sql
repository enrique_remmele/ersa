﻿CREATE Procedure [dbo].[SpAsociarVentaRetencion]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@Numero int = 0,
	@Fecha date = NULL,
	@IDCliente int = NULL,
	@Observacion varchar(200) = NULL,
	@Operacion varchar(50) = '',

	--Retencion
	@IDTransaccionRetencion numeric(18,0) = NULL,
	@IDFormaPago smallint = NULL,
	@PorcentajeRetencion decimal(5,2) = 0,
	@Total money = 0,
				
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--BLOQUES
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		Declare @vNumero int
		Set @vNumero = ISNull((Select MAX(Numero)+1 From AsociarVentaRetencion Where IDSucursal=@IDSucursalOperacion),1)
		
		--Insertar Transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
			
		--Insertar en Cobranza Credito
		Insert Into AsociarVentaRetencion(IDTransaccion, IDSucursal, Numero, IDCliente, Fecha, IDTransaccionRetencion, IDFormaPago, PorcentajeRetencion, Total, Anulado, Observacion)
		Values(@IDTransaccionSalida, @IDSucursalOperacion, @vNumero, @IDCliente, @Fecha, @IDTransaccionRetencion, @IDFormaPago, @PorcentajeRetencion, @Total, 'False', @Observacion)
					
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	--ANULAR
	If @Operacion = 'ANULAR' Begin
		
		Set @IDTransaccionRetencion = (Select Top(1) IDTransaccionRetencion From AsociarVentaRetencion Where IDTransaccion=@IDTransaccion)
		Set @IDFormaPago = (Select Top(1) IDFormaPago From AsociarVentaRetencion Where IDTransaccion=@IDTransaccion)
		
		--Eliminamos los asientos
		Delete FormaPagoDocumentoRetencion Where IDTransaccion = @IDTransaccionRetencion And ID=@IDFormaPago
				
		--Anulamos el registro
		Update AsociarVentaRetencion Set Anulado='True'
		Where IDTransaccion = @IDTransaccion
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal		
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		GoTo Salir
		
	End
	
	--ELIMINAR
	If @Operacion = 'DEL' Begin
	
		--Validar
		--Si el efectivo esta depositado
		
		--Si los cheques estan depositados
		
		--Controlar los asientos asociados
		
		--Procesar
		--Eliminar la Forma de Pago (Eliminar el efectivo y actualizarlo - Reestablecer el saldo del cheque)

		--Eliminar relacion de las ventas asociadas (Actualizar saldos de la venta y cliente)
		
		--Eliminamos el registro
		Delete AsociarVentaRetencion Where IDTransaccion = @IDTransaccion
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		GoTo Salir
		
	End

Salir:
		
End


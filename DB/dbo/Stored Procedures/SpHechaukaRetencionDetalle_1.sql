﻿CREATE procedure [dbo].[SpHechaukaRetencionDetalle] 
	@Año smallint,
	@Mes tinyint
	
As
Begin

	--CON COMPROBANTES
	Select
	'TipoRegistro'= 2,
	'RUCProveedor'= (Case When(CharIndex('-', RUC,0)) = 0 Then RUC Else (SubString(RUC,0, CharIndex('-', RUC,0))) End),
	'DVProveedor'=SubString(RUC, CharIndex('-', RUC,0)+1,1),
	'NombreProveedor'= RI.Proveedor,
	'TipoOperacion'= 1,
	'FechaComprobanteVenta'= dbo.FFormatoFecha(MAX(OPE.Fecha)),
	'NumeroComprobanteVenta'= MAX(OPE.NroComprobante),
	'Numero ComprobanteRetencion'= RI.Comprobante,
	'FechaComprobanteRetencion'= dbo.FFormatoFecha(RI.fecha),
	'MontoImponibleRetencionRenta'=0,
	'MontoImponibleRetencionIVA'= Case When RI.IDMoneda=1 Then ROUND(SUM(OPE.TotalImpuesto),0) Else ROUND(SUM(OPE.TotalImpuesto*RI.Cotizacion),0) End,
	'ImporteRetenidoIVA'= Case When RI.IDMoneda=1 Then ROUND(Sum(OPE.RetencionIVA), 0) Else ROUND(Sum(OPE.RetencionIVA*RI.Cotizacion), 0) End,   
	'ImporteRetenidoRenta'=0,
	'NumeroTimbrado'=RI.Timbrado

	From
	VRetencionIVA RI
	join VOrdenPagoEgreso OPE On RI.IDTransaccionOrdenPago= OPE.IDTransaccionOrdenPago And RI.IDProveedor=OPE.IDProveedor
	Where YEAR(RI.Fecha)= @Año  And MONTH(RI.fecha)= @Mes   And RI.Anulado='False' 
	
	GROUP BY RI.Comprobante, RI.RUC, RI.proveedor, RI.Fecha, RI.IDTransaccionOrdenPago, RI.IDProveedor, RI.Timbrado, RI.IDMoneda
	
	UNION ALL
	
	--SIN COMPROBANTES
	Select
	'TipoRegistro'= 2,
	'RUCProveedor'= (Case When(CharIndex('-', RUC,0)) = 0 Then RUC Else (SubString(RUC,0, CharIndex('-', RUC,0))) End),
	'DVProveedor'=SubString(RUC, CharIndex('-', RUC,0)+1,1),
	'NombreProveedor'= RI.Proveedor,
	'TipoOperacion'= 1,
	'FechaComprobanteVenta'= dbo.FFormatoFecha(MAX(DR.Fecha)),
	'NumeroComprobanteVenta'= MAX(DR.TipoyComprobante),
	'Numero ComprobanteRetencion'= RI.Comprobante,
	'FechaComprobanteRetencion'= dbo.FFormatoFecha(RI.fecha),
	'MontoImponibleRetencionRenta'=0,
	'MontoImponibleRetencionIVA'=Case When RI.IDMoneda=1 Then (ROUND(SUM(DR.IVA10) + SUM(DR.IVA5), 0)) Else (ROUND(SUM(DR.IVA10*RI.Cotizacion) + SUM(DR.IVA5*RI.Cotizacion), 0)) End,
	'ImporteRetenidoIVA'= Case When RI.IDMoneda=1 Then ROUND(Sum(DR.RetencionIVA), 0) Else (ROUND(Sum(DR.RetencionIVA*RI.Cotizacion), 0)) End,   
	'ImporteRetenidoRenta'=0,
	'NumeroTimbrado'=RI.Timbrado

	From
	VRetencionIVA RI
	join dbo.DetalleRetencion DR ON Ri.IDTransaccion=DR.IDTransaccion
	Where YEAR(RI.Fecha)= @Año  And MONTH(RI.fecha)= @Mes   And RI.Anulado='False' 
	
	GROUP BY RI.Comprobante, RI.RUC, RI.proveedor, RI.Fecha, RI.IDTransaccionOrdenPago, RI.IDProveedor, RI.Timbrado, RI.IDMoneda
			
End




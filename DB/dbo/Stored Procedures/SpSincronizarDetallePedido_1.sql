﻿CREATE Procedure [dbo].[SpSincronizarDetallePedido]
	
	--Entrada
	@IDTransaccion numeric(18, 0),
	@IDProducto int,
	@ID tinyint,
	@IDDeposito tinyint,
	@Observacion varchar(50),
	@IDImpuesto tinyint,
	@Cantidad decimal(10, 2),
	@PrecioUnitario money,
	@Total money,
	@TotalImpuesto money,
	@TotalDiscriminado money,
	@PorcentajeDescuento numeric(5, 2),
	@DescuentoUnitario money,
	@DescuentoUnitarioDiscriminado money,
	@TotalDescuento money,
	@TotalDescuentoDiscriminado money,
	@CostoUnitario money,
	@TotalCosto money,
	@TotalCostoImpuesto money,
	@TotalCostoDiscriminado money,
	@Caja bit,
	@CantidadCaja decimal(10, 2),
	@Secuencia varchar(50) = '',
	
	--Pedido
	@IDVendedor int,
	@IDCliente int,
	@Fecha date,
	@TotalPedido money,
	
	--Transaccion
	@IDSucursalOrigen int,
	@IDDepositoOrigen int,
	@IDTerminalOrigen int
  
As

Begin

	--Variables
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	Declare @vIDTransaccion numeric(18,0)
	 
	Set @vMensaje = 'No se proceso'
	Set @vProcesado = 'False'
	 
	--Verificar si existe
	If Not Exists(Select * From PedidoMovil Where IDVendedor=@IDVendedor And IDCliente=@IDCliente And DATEDIFF(dd,Fecha, @Fecha) = 0 And Total=@TotalPedido) Begin
		Set @vMensaje = 'El registro no se encuentra!'
		Set @vProcesado = 'False'
		GoTo Salir
	End 
 
	--Si no existe en Pedido
	Set @vIDTransaccion = (Select Top(1) IDTransaccion From PedidoMovil Where IDVendedor=@IDVendedor And IDCliente=@IDCliente And DATEDIFF(dd,Fecha, @Fecha) = 0 And Total=@TotalPedido)
	
	--Verificamos si existe
	--Actualizamos
	If Exists(Select * From DetallePedido Where IDTransaccion=@vIDTransaccion And IDProducto=@IDProducto And ID=@ID) Begin
		
		Set @vMensaje = 'Registro actualizado!'
		Set @vProcesado = 'True'
		GoTo Salir
	
	End
	
	--Si no existe, insertamos
	Insert Into DetallePedido(IDTransaccion, IDProducto, ID, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, Total, TotalImpuesto, TotalDiscriminado, PorcentajeDescuento, DescuentoUnitario, DescuentoUnitarioDiscriminado, TotalDescuento, TotalDescuentoDiscriminado, CostoUnitario, TotalCosto, TotalCostoImpuesto, TotalCostoDiscriminado, Caja, CantidadCaja)
	Values(@vIDTransaccion, @IDProducto, @ID, @IDDeposito, @Observacion, @IDImpuesto, @Cantidad, @PrecioUnitario, @Total, @TotalImpuesto, @TotalDiscriminado, @PorcentajeDescuento, @DescuentoUnitario, @DescuentoUnitarioDiscriminado, @TotalDescuento, @TotalDescuentoDiscriminado, @CostoUnitario, @TotalCosto, @TotalCostoImpuesto, @TotalCostoDiscriminado, @Caja, @CantidadCaja)	
	
	Set @vMensaje = 'Registro actualizado!'
	Set @vProcesado = 'True'
	GoTo Salir
		
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


﻿
CREATE procedure [dbo].[spInactivaClientes]
@IDEstado tinyint,
@Fecha Datetime,
@IDUsuario as int,
@Mensaje varchar(100) output,
@Procesado varchar(5) output
as
begin  
	Declare @vID as int
	Declare @vFechaUltimaCompra Date

	Declare db_cursor cursor for
		
	Select ID, Isnull((Select max(FechaEmision) 
				from Venta 
				where anulado = 0 and IDCliente = Cliente.ID), @Fecha-1)
	from Cliente 
	Order by 2 desc
	--where (Select isnull(max(FechaEmision),cast(@Fecha as date)) 
	--		from Venta 
	--		where anulado = 0 and IDCliente = Cliente.ID 
	--		and saldo = 0) <= @Fecha
	
	Open db_cursor   
	Fetch Next From db_cursor Into  @vID, @vFechaUltimaCompra
	While @@FETCH_STATUS = 0 Begin 
	
	If @Fecha > @vFechaUltimaCompra begin
	 print concat('Cliente=',@vID,'  - Fecha=',@vFechaUltimaCompra) 

	 Update CLiente
	 set IDEstado = @IDEstado,
	 IDUsuarioModificacion = @IDUsuario,
	 FechaModificacion = GETDATE()
	 where ID = @vID
	End



	Fetch Next From db_cursor Into @vID, @vFechaUltimaCompra
	End
		
	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor		   			
		
		

 
	set @Mensaje = 'Registro guardado!'
	set @Procesado = 'True'
	return @@rowcount
		
End











﻿CREATE Procedure [dbo].[SpViewTicketBasculaPendientesPorFecha]
	
	--Entrada
	
	--@Desde date,
	@Hasta date,
	@IDProducto int = 0,
	@IDProveedor int = 0

As

	
Begin
		
		
		--Crear la tabla temporal
		create table #TablaTemporal(IDTransaccion int,
								Numero int,
								NroAcuerdo int,
								IDMoneda int,
								Moneda varchar(15),
								Fecha date,
								IDProveedor int,
								Proveedor varchar(100),
								IDProducto int,
								Producto varchar(50),
								TipoProducto varchar(50),
								PesoRemision decimal(18,3),
								PesoBascula decimal(18,3),
								Diferencia decimal(18,3),
								PrecioUnitarioDiscriminado money,
								PrecioUnitarioDiscriminadoUS money,
								TotalDiscriminado money,
								TotalDiscriminadoUS money,
								FechaFactura Date,
								FechaMacheo Date)
								
 
	
		--Declarar variables 
		declare @vIDTransaccion int
		declare @vNumero int
		declare @vNroAcuerdo int
		declare @vIDMoneda int
		declare @vMoneda varchar(15)
		declare @vFecha date
		declare @vIDProveedor int
		declare @vProveedor varchar(100)
		declare @vIDProducto int
		declare @vProducto varchar(50)
		declare @vTipoProducto varchar(50)
		declare @vPesoRemision decimal(18,3)
		declare @vPesoBascula decimal(18,3)
		declare @vDiferencia decimal(18,3)
		declare @vPrecio money
		declare @vPrecioUS money
		declare @vTotal money
		declare @vTotalUS money

		declare @vFechaFactura date = null
		declare @vFechaMacheo date = null
		declare @vIDTransaccionGasto int
		declare @vIDTransaccionMacheo int
		
		Declare db_cursor cursor for
		
		Select
		IDTransaccion,
		Numero,
		NroAcuerdo,
		IDMoneda,
		Moneda,
		Fecha,
		IDProveedor,
		Proveedor,
		IDProducto,
		Producto,
		TipoProducto,
		PesoRemision,
		PesoBascula,
		Diferencia,
		PrecioUnitarioDiscriminado,
		PrecioUnitarioDiscriminadoUS,
		TotalDiscriminado,
		TotalDiscriminadoUS
		From VTicketBascula
		--select * from vticketbascula
		Where Fecha <= @Hasta and Anulado='False' and Procesado = 1
		and (Case When @IDProducto = 0 then 0 else IDProducto end) = (Case When @IDProducto = 0 then 0 else @IDProducto end)
		and (Case When @IDProveedor = 0 then 0 else IDProveedor end) = (Case When @IDProveedor = 0 then 0 else @IDProveedor end)
								
		Open db_cursor   
		
		Fetch Next From db_cursor Into @vIDTransaccion,@vNumero,@vNroAcuerdo,@vIDMoneda,@vMoneda,@vFecha,@vIDProveedor,@vProveedor,@vIDProducto,@vProducto,@vTipoProducto,@vPesoRemision,@vPesoBascula,@vDiferencia,@vPrecio,@vPrecioUS,@vTotal,@vTotalUS
		
		While @@FETCH_STATUS = 0 Begin
		
			set @vIDTransaccionMacheo = (Select IDTransaccionMacheo from DetalleMacheo where IDTransaccionTicket = @vIDTransaccion)
			
			Select @vFechaMacheo = Fecha, 
					@vIDTransaccionGasto=Isnull(IDTransaccionGasto ,0)
			from MacheoFacturaTicket 
			where IDTransaccion = @vIDTransaccionMacheo
			if @vFechaMacheo is not null begin
				Set @vFechaFactura = (Select Fecha from gasto where idtransaccion = Isnull(@vIDTransaccionGasto,0))
			end
			else begin
				Set @vFechaFactura = null
			end
			
			
			if exists(Select * from asiento where idtransaccion = @vIDTRansaccionMacheo and bloquear = 1 and total = 0)begin
				goto Seguir
			end

			if not exists(Select * from MacheoFacturaTicket where IDTransaccion = @vIDTransaccionMacheo and Fecha <= @Hasta) begin
				Insert into #TablaTemporal(IDTransaccion,Numero,NroAcuerdo,IDMoneda,Moneda,Fecha,IDProveedor,Proveedor,IDProducto,Producto,TipoProducto,PesoRemision,PesoBascula,Diferencia,PrecioUnitarioDiscriminado,PrecioUnitarioDiscriminadoUS,TotalDiscriminado,TotalDiscriminadoUS,FechaFactura,FechaMacheo) 
				Values (@vIDTransaccion,@vNumero,@vNroAcuerdo,@vIDMoneda,@vMoneda,@vFecha,@vIDProveedor,@vProveedor,@vIDProducto,@vProducto,@vTipoProducto,@vPesoRemision,@vPesoBascula,@vDiferencia,@vPrecio,@vPrecioUS,@vTotal,@vTotalUS, @vFechaFactura, @vFechaMacheo) 													 
			end						  
	Seguir:
			set @vFechaFactura = null
			set @vFechaMacheo = null
		
			Fetch Next From db_cursor Into @vIDTransaccion,@vNumero,@vNroAcuerdo,@vIDMoneda,@vMoneda,@vFecha,@vIDProveedor,@vProveedor,@vIDProducto,@vProducto,@vTipoProducto,@vPesoRemision,@vPesoBascula,@vDiferencia,@vPrecio,@vPrecioUS,@vTotal,@vTotalUS
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		

		Select *   From #TablaTemporal 
	
	
	
End

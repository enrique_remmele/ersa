﻿CREATE Procedure [dbo].[SpPaisInsUpd]

	--Entrada
	@Descripcion varchar(100)
	
	
As

Begin

	Declare @vID int
	
	--Si existe
	If Exists(Select * From Pais Where Descripcion = @Descripcion) Begin
		Set @vID = (Select Top(1) ID From Pais Where Descripcion = @Descripcion)
		
	End Else Begin
		Set @vID = (Select IsNull(Max(ID)+1,1) From Pais)
		
		If @Descripcion = '' Begin
			Set @Descripcion = 'PARAGUAY'
		End
		
		Insert Into Pais(ID, Descripcion, Orden, Estado)
		Values(@vID, @Descripcion, 0, 'True')
	End
	
	return @vID
	
End


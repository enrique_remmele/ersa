﻿CREATE Procedure [dbo].[SpImportarConteoInvecont]

	--Entrada
	@IdTransaccion int,
	@Codigo varchar(50),
	@Equipo1 int,
	@Averiado1 int = 0,
	@Diferencia int = 0
As

Begin
		Declare @vIDProducto Int
		Set @vIDProducto= IsNull((Select ID from Producto where Referencia=@Codigo),0)
		
		If Not Exists(Select * From ExistenciaDepositoInventario Where IDTransaccion=@IDTransaccion) Begin
			print 'No se encontro la operacion'
			Return @@Rowcount
		End
		
		If @vIDProducto = 0 Begin
			print 'No se encontro el producto'
			Return @@Rowcount
		End
		
		Update ExistenciaDepositoInventario
		Set Equipo1=@Equipo1,
			Averiados1=@Averiado1,
			DiferenciaSistema=@Diferencia
				
		Where IDTransaccion=@IDTransaccion And IDProducto=@vIDProducto
			
			
End

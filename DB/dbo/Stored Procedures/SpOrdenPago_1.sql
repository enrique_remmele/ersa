﻿CREATE Procedure [dbo].[SpOrdenPago]

	--JET 28-10-2015
	
	--Entrada OP
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalOperacion	tinyint	= NULL,
	@Numero int = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante varchar(50) = NULL,
	@Fecha date = NULL,
	@IDProveedor int = NULL,
	@Observacion varchar(500) = NULL,
	@Total money = 0,
	@PagoProveedor bit = 'False',
	@AnticipoProveedor bit = 'False',
	@EgresoRendir bit = 'False',
	@AplicarRetencion bit = 'False',
	@IDMoneda tinyint = 1,
	@DiferenciaCambio money = 0,

	--Cheque
	@Cheque bit = NULL,
	@IDCuentaBancaria tinyint = NULL,
	@NroCheque varchar(50) = NULL,
	@FechaCheque date = NULL,
	@FechaPago date = NULL,
	@ImporteMoneda money = NULL,
	@Cotizacion money = NULL,
	@Importe money = 0,
	@Diferido bit = NULL,
	@FechaVencimiento date = NULL,
	@ALaOrden varchar(100) = NULL,
	
	--Documento
	@CotizacionDocumento money = 0,
	@ImporteDocumento money = 0,
	@IDMonedaDocumento money = 0,
	@ImporteMonedaDocumento money = 0,
	
	@IDMotivoAnulacion integer = 0,

	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
	
	
As

Begin
	--Variables
	declare @IdChequera tinyint
	declare @NroChequeAUtilizar int
	declare @NroHasta int
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from OrdenPago where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 
	IF @Operacion = 'ANULAR' Begin
		set @Fecha = (Select Fecha from OrdenPago where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 
	

	If @Operacion = 'INS' Begin
		
		--En el caso de que NroCheque se utilize			
		if @NroCheque <> '' begin

			--Verificar que la cuenta utilizada se encuentre en el listado de chequeras y el numero de cheque corresponda a dicha chequera
			select @IdChequera = ID, @NroChequeAUtilizar = AUtilizar, @NroHasta = NroHasta from VChequera where IdCuentaBancaria = @IDCuentaBancaria and @NroCheque between NroDesde and NroHasta and Estado = 1

			if @IdChequera is null begin
				set @Mensaje = 'El numero no existe en chequera.'
				set @Procesado = 'False'
				return @@rowcount
			end
			if @NroChequeAUtilizar > @NroCheque begin
				set @Mensaje = CONCAT('El numero de cheque correlativo debe ser: ',@NroChequeAUtilizar)
				set @Procesado = 'False'
				return @@rowcount
			end
			if @NroChequeAUtilizar < @NroCheque begin
				set @Mensaje = CONCAT('Numero de cheque ya utilizado, debe ser: ',@NroChequeAUtilizar)
				set @Procesado = 'False'
				return @@rowcount
			end

		end	

		--Numero
		Set @Numero = ISNULL((Select MAX(Numero+1) From OrdenPago Where IDSucursal=@IDSucursalOperacion),1)
		
		--Si ya existe el documento
		--Atencion!!! No se puede aplicar ya que hay OP's importadas que tienen
		--asignado nrocomprobantes
		
		--If Exists(Select * From OrdenPago Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante And IDSucursal=@IDSucursalOperacion) Begin
			
		--	--Si es importado el comprobante, pasar
		--	set @Mensaje = 'El numero de comprobante ya existe!'
		--	set @Procesado = 'False'
		--	set @IDTransaccionSalida  = 0
		--	return @@rowcount

		--End
	
		
		--Si el cheque ya existe, si viene en blanco no controlar
		If @Cheque = 'True' Begin
			If @NroCheque != '' Begin
				If Exists(Select * From Cheque Where IDCuentaBancaria=@IDCuentaBancaria And NroCheque=@NroCheque And Anulado='False') Begin
					set @Mensaje = 'El numero de cheque ya existe!'
					set @Procesado = 'False'
					set @IDTransaccionSalida  = 0
					return @@rowcount
				End
			End
		End	
		
		if @Observacion = '' begin
				set @Mensaje = 'Ingrese observacion de la OP.'
				set @Procesado = 'False'
				return @@rowcount
		end	
		
		--Insertar Transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
					
		--Insertar en Orden de Pago
		Insert Into OrdenPago(IDTransaccion, Numero, IDTipoComprobante, NroComprobante, IDSucursal, Fecha, IDProveedor, Observacion, IDMoneda, Cotizacion, Total, Cancelado, Anulado, FechaAnulado, IDUsuarioAnulado, Conciliado,PagoProveedor, AnticipoProveedor, EgresoRendir, AplicarRetencion, DiferenciaCambio, IDMonedaDocumento, ImporteDocumento, ImporteMonedaDocumento, CotizacionDocumento)
		Values(@IDTransaccionSalida, @Numero, @IDTipoComprobante, @NroComprobante, @IDSucursalOperacion, @Fecha, @IDProveedor, @Observacion, @IDMoneda, @Cotizacion, @Total, 'False', 'False', NULL, NULL, 'False', @PagoProveedor, @AnticipoProveedor, @EgresoRendir, @AplicarRetencion, @DiferenciaCambio, @IDMonedaDocumento, @ImporteDocumento, @ImporteMonedaDocumento, @CotizacionDocumento)
		
		--Si se pago con Cheque
		If @Cheque = 'True' Begin

			if @IdChequera is not null begin
				--Actualizar Chequera
				if @NroChequeAUtilizar = @NroHasta begin
					Update Chequera set Estado = 0 where ID = @IdChequera
					Update Chequera set AUtilizar = 0 where ID = @IdChequera
				end else begin
					Update Chequera set AUtilizar = AUtilizar + 1 where ID = @IdChequera
				end
			end

			--Validar Fecha de vencimiento si no es diferido
			If @Diferido = 'False' Begin
				set @FechaVencimiento = @FechaCheque
			End

			--Insertar en Cheque	
			Insert Into Cheque(IDTransaccion, IDSucursal, Numero, IDCuentaBancaria, NroCheque, Fecha, FechaPago, Cotizacion, ImporteMoneda, Importe, Diferido, FechaVencimiento, ALaOrden, Conciliado, Anulado )
			Values(@IDTransaccionSalida, @IDSucursalOperacion, @Numero, @IDCuentaBancaria, @NroCheque, @FechaCheque, @FechaPago, @Cotizacion, @ImporteMoneda, @Importe, @Diferido, @FechaVencimiento, @ALaOrden, 'False', 'False')
		End
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount	
	
	End

	If @Operacion = 'ANULAR' Begin
	
		--Validar
		--Asientos Cerrados
		If Exists(Select * From Asiento A Where A.IDTransaccion=@IDTransaccion And A.Conciliado='True') Begin
			set @Mensaje = 'El asiento de este documento ya esta conciliado! No se puede anular.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Verificar dias de bloqueo
		If (Select Top(1) OrdenPagoBloquearAnulacion  From Configuraciones where IDSucursal = @IDSucursal) = 'True' Begin
			
			Declare @vDias tinyint
			Set @vDias = (Select Top(1) OrdenPagoDiasBloqueo From Configuraciones where IDSucursal = @IDSucursal)

			If (Select DATEDIFF(dd,(Select V.Fecha From OrdenPago V Where IDTransaccion=@IDTransaccion), GETDATE())) > @vDias Begin
			 			
				set @Mensaje = 'El sistema no puede anular este registro ya que la configuracion lo restringe por la antigüedad del documento. Cambie la configuracion de bloqueo para anular!'
				set @Procesado = 'False'
				return @@rowcount
				
			End
			
		End		
		
		--Verificar si existe en el detalle de deposito bancario
		if exists(select top(1) * from DetalleDepositoBancario where IDTransaccionDocumento = @IDTransaccion) begin
			set @Mensaje = 'El sistema no puede anular este registro porque ya se encuentra depositado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Verificar si existe en el detalle de deposito bancario
		if exists(select top(1) * from Vencimientochequeemitido where isnull(anulado,0) = 0 and  IDTransaccionOP = @IDTransaccion) begin
			set @Mensaje = 'El sistema no puede anular este registro porque ya tiene un vencimiento de cheque asociado!'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Verificar si existe en el detalle de deposito bancario
		if exists(select top(1) * from EntregaChequeOP where isnull(anulado,0) = 0 and IDTransaccionOP = @IDTransaccion) begin
			set @Mensaje = 'El sistema no puede anular este registro porque ya tiene una entrega de cheque asociada!'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Procesar
		--Reestablecer los egresos
		Exec SpOrdenPagoProcesar @IDTransaccion = @IDTransaccion, @Operacion = @Operacion, @Procesado = @Procesado, @Mensaje = @Mensaje

		--Eliminamos las Relaciones
		Delete From OrdenPagoEgreso Where IDTransaccionOrdenPago = @IDTransaccion
		
		--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion

		--Anular la entrega de Cheque
		declare @vIDTransaccionEC int = (select IDTransaccion from EntregaChequeOP where IDTransaccionOP = @IDTransaccion)
		delete from DetalleAsiento where idtransaccion = @vIDTransaccionEC
		delete from Asiento where idtransaccion = @vIDTransaccionEC
		Update EntregaChequeOP set Anulado = 'True' where IDTransaccionOP = @IDTransaccion

		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		--Anulamos el Registro
		Update OrdenPago Set Anulado = 'True', IDMotivoAnulacion = @IDMotivoAnulacion		
		Where IDTransaccion = @IDTransaccion
		
		--Anulamos el Cheque
		Update Cheque Set Anulado = 'True'
		Where IDTransaccion = @IDTransaccion
		
		--Anular la Retencion
		Update RetencionIVA Set Anulado='True'
		Where IDTransaccionOrdenPago=@IDTransaccion
		
		exec SpRepararTodosLosEgresos

		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount	
		
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
	
	
End


﻿CREATE Procedure [dbo].[SpCaja]

	--Entrada
	@Numero numeric(18,0) = NULL, 
	@IDSucursalOperacion int,
	@Observacion varchar(200) = '',
	@Fecha date = NULL,
	
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--Variable
	Declare @vNumero int
	Declare @vIDTransaccion int
		
	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		Set @vNumero = ISNULL((Select MAX(Numero)+1 From Caja Where IDSucursal=@IDSucursalOperacion),1)
	
		----Validar
		--Si ya existe una caja de este dia, y sin movimientos
		If (Select COUNT(*) From Caja C 
		Where C.IDSucursal=@IDSucursalOperacion 
		And C.Habilitado = 'True'
		And DATEDIFF(dd, C.Fecha, GetDate())= 0 
		And (Select COUNT(*) From Transaccion T Where T.IDTransaccionCaja=C.IDTransaccion) = 0) > 0 Begin
			set @Mensaje = 'Ya existe una caja abierta sin movimientos para ete dia!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @vIDTransaccion OUTPUT
		
		--Insertar
		Insert Into Caja(IDTransaccion, IDSucursal, Numero, Habilitado, Fecha, Anulado, FechaCierre, IDUsuarioCierre, Observacion)
		Values(@vIDTransaccion, @IDSucursalOperacion, @vNumero, 'True', @Fecha, 'False', NULL, NULL, @Observacion)
		
		--Cerrar todas las cajas
		Update Caja Set Habilitado = 'False'						
		Where IDTransaccion != @vIDTransaccion
		
		--Cerrar todas las cajas
		Update Caja Set IDUsuarioCierre = @IDUsuario,
						FechaCierre=GETDATE()						
		Where IDTransaccion != @vIDTransaccion And IDUsuarioCierre Is Null
								
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--If @Operacion = 'CERRAR' Begin
	
		--Set @vIDTransaccion = (Select Top(1) IDTransaccion From Caja where IDSucursal=@IDSucursalOperacion	 And Numero=@Numero)
		 
		----IDTransaccion
		--If Not Exists(Select * From Caja Where IDTransaccion=@vIDTransaccion) Begin
		--	set @Mensaje = 'El sistema no encontro el registro solicitado.'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		----Si ya esta cerrado
		--If (Select Habilitado From Caja Where IDTransaccion=@vIDTransaccion) = 'False' Begin
		--	set @Mensaje = 'La caja ya esta cerrada!'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		----Update Caja Set Habilitado='False',
		----				FechaCierre=GETDATE(),
		----				IDUsuarioCierre=@IDUsuario
		----Where IDTransaccion=@vIDTransaccion
				
		--Set @Mensaje = 'Registro guardado!'
		--Set @Procesado = 'True'
		--return @@rowcount
			
	--End
	
	If @Operacion = 'ABRIR' Begin

		Set @vIDTransaccion = (Select Top(1) IDTransaccion From Caja where IDSucursal=@IDSucursalOperacion And Numero=@Numero)
		
		--IDTransaccion
		If Not Exists(Select * From Caja Where IDTransaccion=@vIDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si ya esta abierto
		If (Select Habilitado From Caja Where IDTransaccion=@vIDTransaccion) = 'True' Begin
			set @Mensaje = 'La caja ya esta abierta!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		Update Caja Set Habilitado = 'True'
		Where IDTransaccion=@vIDTransaccion
		
		----Cerrar automaticamente las demas cajas de la sucursal
		--Update Caja Set Habilitado = 'False',
		--				FechaCierre=GETDATE(),
		--				IDUsuarioCierre=@IDUsuario
		--Where IDTransaccion!=@vIDTransaccion And IDSucursal=@IDSucursalOperacion
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount	
			
	End
	
--	If @Operacion = 'ANULAR' Begin

		--Set @vIDTransaccion = (Select Top(1) IDTransaccion From Caja where IDSucursal=@IDSucursalOperacion And Numero=@Numero)
		
		----IDTransaccion
		--If Not Exists(Select * From Caja Where IDTransaccion=@vIDTransaccion) Begin
		--	set @Mensaje = 'El sistema no encontro el registro solicitado.'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		----Si ya tiene registros asociados
		--If Exists (Select * From Transaccion Where ID=@vIDTransaccion) Begin
		--	set @Mensaje = 'La caja ya tiene registros asociados! No se puede anular'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		--Update Caja Set Habilitado = 'False',
		--				Anulado='True'
		--Where IDTransaccion=@vIDTransaccion
			
		--return @@rowcount
			
	--End
	
	set @Mensaje = 'No se proceso ninguna transacción!'
	set @Procesado = 'False'
	
	return @@rowcount
		
End







﻿CREATE Procedure [dbo].[SpDetalleVenta]

	--Entrada
	@IDTransaccion numeric(18,0),
	@IDProducto int = NULL,
	@ID tinyint = NULL,
	@IDDeposito tinyint = NULL,
	@Observacion varchar(50) = NULL,
	@IDImpuesto tinyint = NULL,
	@Cantidad decimal(10,2) = NULL,
	@PrecioUnitario money = NULL,
	@DescuentoUnitario money = NULL,
	@PorcentajeDescuento numeric(2,0) = NULL,
	@Total money = NULL,
	@TotalImpuesto money = NULL,
	@TotalDescuento money = NULL,
	@TotalDiscriminado money = NULL,
	@Caja bit = NULL,
	@CantidadCaja smallint = NULL,
	@Operacion varchar(20),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin

	--Validar
	--Transaccion
	If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--Transaccion
	If Not Exists(Select IDTransaccion From Venta Where IDTransaccion=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--BLOQUES
	if @Operacion='INS' Begin
	
		--Cantidad
		If @Cantidad <= 0 Begin
			Set @Mensaje = 'La cantidad no puede ser igual ni menor a 0!'
			Set @Procesado = 'False'
			return @@rowcount
		End	
		
		--Verificar Existencia
		If (Select dbo.FExistenciaProducto(@IDProducto, @IDDeposito)) < @Cantidad Begin
			Set @Mensaje = 'Stock insuficiente!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		 
		--Insertar el detalle
		Insert Into DetalleVenta(IDTransaccion, IDProducto, ID, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, DescuentoUnitario, PorcentajeDescuento, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado, Caja, CantidadCaja)
		Values(@IDTransaccion, @IDProducto, @ID, @IDDeposito, @Observacion, @IDImpuesto, @Cantidad, @PrecioUnitario, @DescuentoUnitario, @PorcentajeDescuento, @Total, @TotalImpuesto, @TotalDescuento, @TotalDiscriminado, @Caja, @CantidadCaja)
		
		EXEC SpActualizarProductoDeposito
			@IDDeposito = @IDDeposito,
			@IDProducto = @IDProducto,
			@Cantidad = @Cantidad,
			@Signo = N'-',
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount	
		
		--EXPRESS - Proceso de Tacticos
			
	End
	
	Declare @vID tinyint
	Declare @vIDDeposito tinyint
	Declare @vIDProducto int
	Declare @vCantidad decimal(10,2)
	
	if @Operacion='DEL' Begin
		
		Begin
			
			Declare db_cursor cursor for
			Select DV.ID, DV.IDProducto, DV.IDDeposito, DV.Cantidad
			From Venta V Join DetalleVenta DV On V.IDTransaccion=DV.IDTransaccion Join Producto P On DV.IDProducto=P.ID
			Where P.ControlarExistencia='True' And V.IDTransaccion=@IDTransaccion And V.Anulado='False'
			Open db_cursor   
			fetch next from db_cursor into @vID, @vIDProducto, @vIDDeposito, @vCantidad   

			While @@FETCH_STATUS = 0 Begin  			  
					
				  EXEC SpActualizarProductoDeposito
					@IDDeposito = @vIDDeposito,
					@IDProducto = @vIDProducto,
					@Cantidad = @vCantidad,
					@Signo = N'+',
					@Mensaje = @Mensaje OUTPUT,
					@Procesado = @Procesado OUTPUT
					
					--Siguiente
				   fetch next from db_cursor into @vID, @vIDProducto, @vIDDeposito, @vCantidad   
			End   

			Close db_cursor   
			deallocate db_cursor
		End
		
		--Eliminamos los registros
		Delete From DetalleVenta Where IDTransaccion=@IDTransaccion
		
		--Eliminamos del Kardex
		Delete From Kardex Where IDTransaccion=@IDTransaccion
						
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount	
			
	End
	
	if @Operacion='ANULAR' Begin
		
		Begin
			--print 'entro anular'
			--Variables Kardex
			Declare @vFechaAnulacion datetime			
			Declare @vCostoUnitario money
			Declare @vNroComprobante varchar(50)
			Declare @vIDSucursal int
			Declare @vFechaEmision datetime 

			--Obtener valores Kardex
			Select	@vFechaEmision = FechaEmision,
					@vFechaAnulacion=GetDate(),
					@vNroComprobante=NroComprobante,
					@vIDSucursal=IDSucursal
			From Venta Where IDTransaccion=@IDTransaccion
						
			Declare db_cursor cursor for
			Select DV.ID, DV.IDProducto, DV.IDDeposito, DV.Cantidad
			From Venta V Join DetalleVenta DV On V.IDTransaccion=DV.IDTransaccion Join Producto P On DV.IDProducto=P.ID
			Where P.ControlarExistencia='True' And V.IDTransaccion=@IDTransaccion And V.Anulado='False'
			Open db_cursor   
			fetch next from db_cursor into @vID, @vIDProducto, @vIDDeposito, @vCantidad   

			While @@FETCH_STATUS = 0 Begin  			  
				--print 'entro cursor'
						
				  EXEC SpActualizarProductoDeposito
					@IDDeposito = @vIDDeposito,
					@IDProducto = @vIDProducto,
					@Cantidad = @vCantidad,
					@Signo = N'+',
					@Mensaje = @Mensaje OUTPUT,
					@Procesado = @Procesado OUTPUT
					
				--Actualiza el Kardex
				Set @vCantidad = @vCantidad * -1
				Set @vCostoUnitario = (Select CostoPromedio From Producto Where ID = @vIDProducto)
				--print 'elimina del kardex'
				Delete from kardex where idtransaccion = @IDTransaccion
			--	Exec SPRecalcularKardexProducto @vIDProducto, @vFechaEmision

				--EXEC SpKardex
				--	@Fecha = @vFechaAnulacion,
				--	@IDTransaccion = @IDTransaccion,
				--	@IDProducto = @vIDProducto,
				--	@ID = @vID,
				--	@Orden = 10,
				--	@CantidadEntrada = 0,
				--	@CantidadSalida = @vCantidad,
				--	@CostoEntrada = 0,
				--	@CostoSalida = @vCostoUnitario,
				--	@IDSucursal = @vIDSucursal,
				--	@Comprobante = @vNroComprobante	

					--Siguiente
				   fetch next from db_cursor into @vID, @vIDProducto, @vIDDeposito, @vCantidad   
			End   

			Close db_cursor   
			deallocate db_cursor
		End
						
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount	
			
	End
	
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	return @@rowcount
	
End


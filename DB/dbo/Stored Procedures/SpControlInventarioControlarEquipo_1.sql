﻿CREATE Procedure [dbo].[SpControlInventarioControlarEquipo]

	--Entrada
	@IDTransaccion numeric,
	@Operacion varchar(10),
	
	--Transaccion	
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
			
As

Begin

	--BLOQUES
		
	--ACTUALIZAR
	If @Operacion='INS' Begin

		--Validar
		--Si no existe
		If Not Exists(Select * From ControlInventario Where IDTransaccion = @IDTransaccion) Begin
			set @Mensaje = 'El sistema no encuentra el registro seleccionado!'
			set @Procesado = 'False'
			return @@rowcount	
		End
		
		--Si ya esta controlado
		If (Select ControlEquipo From ControlInventario Where IDTransaccion = @IDTransaccion) = 'True' Begin
			set @Mensaje = 'El registro ya esta controlado!'
			set @Procesado = 'False'
			return @@rowcount	
		End
		
		--Si hay diferencias todavia
		If Exists(Select * From ExistenciaDepositoInventario Where IDTransaccion=@IDTransaccion And TotalDiferencia>0) Begin
			set @Mensaje = 'El control entre equipos todavia contiene diferencias en algunos productos!'
			set @Procesado = 'False'
			return @@rowcount	
		End
	
		Update ControlInventario Set ControlEquipo = 'True' 
		Where IDTransaccion=@IDTransaccion
			
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount	
		   
	End

	--ANULAR
	If @Operacion='ANULAR' Begin
		
		--Validar
		--Si no existe
		If Not Exists(Select * From ControlInventario Where IDTransaccion = @IDTransaccion) Begin
			set @Mensaje = 'El sistema no encuentra el registro seleccionado!'
			set @Procesado = 'False'
			return @@rowcount	
		End
		
		--Si ya esta controlado
		If (Select ControlEquipo From ControlInventario Where IDTransaccion = @IDTransaccion) = 'False' Begin
			set @Mensaje = 'El registro no esta controlado aun!'
			set @Procesado = 'False'
			return @@rowcount	
		End
		
		Update ControlInventario Set ControlEquipo = 'False' 
		Where IDTransaccion=@IDTransaccion
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount	
		
	End
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


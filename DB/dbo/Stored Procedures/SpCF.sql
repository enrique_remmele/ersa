﻿CREATE Procedure [dbo].[SpCF]

	--Entrada
	@ID tinyint,
	@IDOperacion tinyint,
	@IDSucursal tinyint,
	@IDMoneda tinyint,
	@CuentaContable varchar(50),
	@Debe bit,
	@Haber bit,
	@IDTipoComprobante smallint = NULL,
	@Descripcion varchar(50),
	@Orden tinyint,
	
	@Operacion varchar(10),
	
	--Salida
	@vID tinyint output
	
As

Begin

	Set @vID = 0
	
	--INSERTAR
	if @Operacion='INS' begin
	
		--Obtener el nuevo ID
		Set @vID = (Select ISNULL((Select MAX(ID + 1) From CF Where IDOperacion=@IDOperacion),1))
		
		--Todo OK, Insertamos
		Insert Into CF(IDOperacion, ID, IDSucursal, IDMoneda, CuentaContable, Debe, Haber, IDTipoComprobante, Descripcion, Orden)
		values(@IDOperacion, @vID, @IDSucursal, @IDMoneda, @CuentaContable, @Debe, @Haber, @IDTipoComprobante, @Descripcion, @Orden)
		
		return @@rowcount
	End 
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		Update CF Set	IDSucursal=@IDSucursal, 
						IDMoneda=@IDMoneda, 
						CuentaContable=@CuentaContable, 
						Debe=@Debe, 
						Haber=@Haber, 
						IDTipoComprobante=@IDTipoComprobante, 
						Descripcion=@Descripcion, 
						Orden=@Orden
						
		Where IDOperacion=@IDOperacion And ID=@ID
		 		
		return @@rowcount
			
	end
	
End


﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[pa_WsProducto]
	@Opcion SMALLINT,
	@ID INT = NULL,
	@Referencia VARCHAR(50)=null,
	@Descripcion VARCHAR(100)=NULL,
	@CodigoBarra VARCHAR(30)=NULL,
	@IDCliente INT=NULL,
	@IDSucursal INT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @consulta VARCHAR(1000)

    -- Insert statements for procedure here
	IF @Opcion=1
	BEGIN
		Select Sucursal, 
		Deposito, 
		Existencia 
		From VExistenciaDeposito 
		Where IDProducto=@ID 
		Order By Sucursal 

		SET @consulta = '
		Select Sucursal, 
		Deposito, 
		Existencia 
		From VExistenciaDeposito 
		 Where 1=1 '
		 IF @ID IS NOT NULL SET @consulta = @consulta + ' AND ID=' + CAST(@ID AS VARCHAR(10))
		 IF @Referencia IS NOT NULL SET @consulta = @consulta +  ' AND Referencia LIKE ('+ CHAR(39) + '%' + @Referencia + '%' + CHAR(39) +')'
		 IF @Descripcion IS NOT NULL SET @consulta = @consulta +  ' AND Descripcion LIKE ('+ CHAR(39) + '%' + @Descripcion + '%' + CHAR(39) +')'
		 IF @CodigoBarra IS NOT NULL SET @consulta = @consulta +  ' AND CodigoBarra LIKE ('+ CHAR(39) + '%' + @CodigoBarra + '%' + CHAR(39) +')' 
		 SET @consulta = @consulta + ' Order By Sucursal '
	END

	ELSE IF @Opcion=2
	BEGIN
		Select Top(50) 
		 ID, Ref, Descripcion, CodigoBarra, TipoProducto, 
		 'Precio'=(Select dbo.FPrecioProducto(ID, @IDCliente, @IDSucursal)), 
		 'Existencia'=(Select dbo.FExistenciaProducto(ID, @IDSucursal)), 
		 'UnidadesPorCaja'=UnidadPorCaja, 
		 'UnidadMedida'=UnidadMedida, 
		 ControlarExistencia From VProducto  Where ID=@ID
	END
	
	EXEC(@consulta)
END


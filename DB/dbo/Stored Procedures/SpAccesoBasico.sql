﻿CREATE Procedure [dbo].[SpAccesoBasico]

	--Entrada
	@IDPerfil smallint,
	@IDMenu numeric(18,0),
	@Visualizar bit,
	@Agregar bit,
	@Modificar bit,
	@Eliminar bit,
	@Anular bit,
	@Imprimir bit,
	
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int
	
	
As

Begin

	--Actualizar
	If Exists (Select * From AccesoBasico where IDPerfil=@IDPerfil And IDMenu=@IDMenu) Begin
		
		Insert Into AccesoBasico(IDPerfil, IDMenu, Visualizar, Agregar, Modificar, Eliminar, Anular, Imprimir)
		Values(@IDPerfil, @IDMenu, @Visualizar, @Agregar, @Modificar, @Eliminar, @Anular, @Imprimir)
		
	End Else Begin
		
		Update AccesoBasico Set Visualizar=@Visualizar, 
								Agregar=@Agregar, 
								Modificar=@Modificar, 
								Eliminar=@Eliminar, 
								Anular=@Anular, 
								Imprimir=@Imprimir
		Where IDPerfil=@IDPerfil And IDMenu=@IDMenu
		
	End
	
	return @@rowcount

End


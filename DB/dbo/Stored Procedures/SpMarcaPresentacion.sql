﻿CREATE Procedure [dbo].[SpMarcaPresentacion]

	--Entrada
	@IDMarca smallint,
	@IDPresentacion tinyint,
	@Relacionar bit
	
				
As

Begin

	--BLOQUES
	
	--RELACIONAR
	if @Relacionar='True' begin
		
		--Solo agregar si no existen
		If Exists(Select * From MarcaPresentacion Where IDMarca=@IDMarca And IDPresentacion=@IDPresentacion) Begin
			return @@rowcount
		End
		
		--Insertamos
		Insert Into MarcaPresentacion(IDMarca, IDPresentacion)
		Values(@IDMarca, @IDPresentacion)		
		
		return @@rowcount
			
	End
	
	--NO RELACIONAR
	if @Relacionar='False' begin
		
		Delete From MarcaPresentacion 
		Where IDMarca=@IDMarca And IDPresentacion=@IDPresentacion
		
		return @@rowcount
			
	End

End


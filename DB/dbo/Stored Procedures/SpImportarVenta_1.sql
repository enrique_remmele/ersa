﻿CREATE Procedure [dbo].[SpImportarVenta]

	--Entrada
	--Punto de Expedicion
	@TipoComprobante varchar(10),
	@ReferenciaSucursal varchar(5),
	@ReferenciaPuntoExpedicion varchar(5),
	@NroComprobante varchar(50),
	
	--Cliente
	@Cliente varchar(50),
	@RUC varchar(50),
	@Direccion varchar(200),
	
	--
	@FechaEmision varchar(50),
	@Sucursal varchar(50),
	@Deposito varchar(50),
	@CondicionVenta varchar(5),
	@FechaVencimiento varchar(50),
	@ListaPrecio varchar(100),
	@Vendedor varchar(100),
	@Cotizacion tinyint,
	@Observacion varchar(200),
	
	--Totales
	@Total money,
	@TotalImpuesto money,
	@TotalDescuento money,
	@Saldo money,
	
	--Impuesto
	@Exento as money,
	@DescuentoExento as money,
	@Gravado10 as money,
	@IVA10 as money,
	@Descuento10 as money,
	@Gravado5 as money,
	@IVA5 as money,
	@Descuento5 as money,

	--Configuraciones
	@Anulado bit,
	
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
		
	@Actualizar bit
	
As

Begin
		
	--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		
		Declare @vOperacion varchar(50)
		Declare @vIDTransaccion int
		Declare @vComprobante varchar(50)
		Declare @vTelefono varchar(100)
		Declare @vCredito bit
		Declare @vTotalDiscriminado money
		Declare @vTotalDescuento money
		Declare @vCobrado money
		Declare @vDescontado money
		Declare @vAcreditado money
		Declare @vEsVentaSucursal bit
		
		--Codigos
		Declare @vIDPuntoExpedicion int 
		Declare @vIDTipoComprobante int
		Declare @vIDCliente int
		Declare @vIDSucursalCliente int
		Declare @vIDSucursal int
		Declare @vIDDeposito int
		Declare @vIDListaPrecio int
		Declare @vIDVendedor int
		
	End
	
	--Establecer valores predefinidos
	Begin
		
		Set @vOperacion = 'INS'
		Set @vIDTransaccion = 0
		Set @vTelefono = ''
		Set @vCredito = 'False'
		If @CondicionVenta = 'C' Begin
			Set @vCredito = 'True'
		End
		
		Set @vTotalDiscriminado = @Total - @TotalImpuesto
		Set @vCobrado = @Total - @Saldo
		Set @vDescontado = 0
		Set @vAcreditado = 0
		Set @vEsVentaSucursal = 'False'
	
		--Comprobante
		Set @ReferenciaSucursal = '0' + dbo.FFormatoDosDigitos(CONVERT(tinyint, @ReferenciaSucursal))
		Set @ReferenciaPuntoExpedicion = '0' + dbo.FFormatoDosDigitos(CONVERT(tinyint, @ReferenciaPuntoExpedicion))
		Set @vComprobante = @ReferenciaSucursal + '-' + @ReferenciaPuntoExpedicion + '-' + @NroComprobante
		
		Declare @vCancelado bit
		Set @vCancelado = 'False'
		If @Saldo = 0 Begin
			Set @vCancelado = 'True'
		End
				
	End
	
	--Verificar si ya existe
	If Exists(Select * From VentaImportada VI  Join Venta V On VI.IDTransaccion= V.IDTransaccion Where V.Comprobante=@vComprobante) Begin
		set @vIDTransaccion = (Select VI.IDTransaccion  From VentaImportada VI  Join Venta V On VI.IDTransaccion= V.IDTransaccion Where V.Comprobante=@vComprobante)
		Set @vOperacion = 'UPD'
	End
	
	--Puede que la factura sea cargada manualmente, consultar en la tabla ventas
	If @vIDTransaccion = 0 Begin
		If Exists(Select * From Venta V Where V.Comprobante=@vComprobante) Begin
			Set @vMensaje = 'El comprobante ya fue cargado por algun usuario!'
			Set @vProcesado = 'True'
			GoTo Salir
		End
	End

	--ACTUALIZAR
	If @vOperacion = 'UPD' Begin
	
		If @Actualizar = 'True' Begin

			--Sucursal
			Set @vIDSucursal = (IsNull((Select Top(1) ID From Sucursal Where Referencia=@ReferenciaSucursal),0))
			If @vIDSucursal = 0 Begin
				Set @vMensaje = 'Sucursal incorrecto 141!'
				Set @vProcesado = 'False'
				GoTo Salir
			End
			
			--Deposito
			--Establecemos como deposito el primero que exista en la sucursal
			Set @vIDDeposito = IsNull((Select Top(1) ID From Deposito Where IDSucursal=@vIDSucursal), 0)
			If @vIDDeposito = 0 Begin
				Set @vMensaje = 'Deposito no concuerda!'
				Set @vProcesado = 'False'
				GoTo Salir
			End

			--Cliente, los clientes son unicos, no por sucural
			Set @vIDCliente=(IsNull((Select Top(1) ID From Cliente Where Referencia=@Cliente),0))
			If @vIDCliente = 0 Begin
				
				--Si el RUC no tiene, salir
				If LTrim(@RUC) = '' Or RTrim(@RUC) = '' Begin 
					Set @vMensaje = 'Cliente incorrecto! El codigo no existe y no tiene RUC'
					Set @vProcesado = 'False'
					GoTo Salir
				End

				Set @vIDCliente=(IsNull((Select Top(1) ID From Cliente Where RUC=@RUC),0))
				
				If @vIDCliente = 0 Begin
					Set @vMensaje = 'Cliente incorrecto UPD!'
					Set @vProcesado = 'False'
					GoTo Salir
				End
				
			End
			
			--Lista de Precio
			--Establecer el Minorista de la sucursal seleccionada
			Set @vIDListaPrecio = IsNull((Select Top(1) ID From ListaPrecio Where IDSucursal=@vIDSucursal And Descripcion='MINORISTA'), 0)
			If @vIDListaPrecio = 0 Begin
				Set @vMensaje = 'No se establecio la lista de precio minorista para esta sucursal!'
				Set @vProcesado = 'False'
				GoTo Salir
			End

			--Vendedor
			Exec @vIDVendedor = SpVendedorInsUpd @Descripcion=@Vendedor, @IDSucursal=@vIDSucursal 

			--Venta
			Update Venta Set Credito=@vCredito,
							IDCliente=@vIDCliente,
							Saldo=@Saldo,
							Cobrado = @vCobrado,
							Cancelado=@vCancelado,
							TotalImpuesto=@IVA10+@IVA5,
							TotalDiscriminado=(@Gravado10 - (@IVA10 + @Descuento10)) + (@Gravado5 - (@IVA5 + @Descuento5)),
							Anulado=@Anulado
			Where IDTransaccion = @vIDTransaccion
			
			--Venta Importacion
			--Actualizar en VentaImportacion
			
			--Actualizar Impuesto
			--10%
			Update DetalleImpuesto Set  Total = @Gravado10, 
										TotalImpuesto = @IVA10, 
										TotalDescuento = @Descuento10, 
										TotalDiscriminado = @Gravado10 - (@IVA10 + @Descuento10)
			Where IDTransaccion = @vIDTransaccion And IDImpuesto = 1
			
			--5%
			Update DetalleImpuesto Set  Total = @Gravado5, 
										TotalImpuesto = @IVA5, 
										TotalDescuento = @Descuento5, 
										TotalDiscriminado = @Gravado5 - (@IVA5 + @Descuento5)
			Where IDTransaccion = @vIDTransaccion And IDImpuesto = 2
			
			--10%
			Update DetalleImpuesto Set  Total = @Exento, 
										TotalImpuesto = 0, 
										TotalDescuento = @DescuentoExento, 
										TotalDiscriminado = @Exento - @DescuentoExento
			Where IDTransaccion = @vIDTransaccion And IDImpuesto = 3
			
			--Actualizar el Cliente (Saldo Total, Saldo Credito, Ultima Compra)
			Set @vIDCliente=(IsNull((Select Top(1) IDCliente From Venta Where IDTransaccion=@vIDTransaccion),0))
			EXEC SpAcualizarClienteVenta
			@IDCliente = @vIDCliente,
			@Mensaje = @vMensaje OUTPUT,
			@Procesado = @vProcesado OUTPUT
			
			Set @vMensaje = 'Actualizado!'
			Set @vProcesado = 'True'
			GoTo Salir

		End
		
		Set @vMensaje = 'Sin Act.!'
		Set @vProcesado = 'True'
		GoTo Salir
		
	End
	
	--Hayar Valores
	Begin
		
		Set @vMensaje = 'No se proceso!'
		Set @vProcesado = 'False'
		
		--Sucursal
		Set @vIDSucursal = (IsNull((Select Top(1) ID From Sucursal Where Referencia=@ReferenciaSucursal),0))
		If @vIDSucursal = 0 Begin
			Set @vMensaje = 'Sucursal incorrecto 242!'
			Set @vProcesado = 'False'
			GoTo Salir
		End

		--Tipo de Compobante
		Set @vIDTipoComprobante = ISNULL((Select top(1) ID From TipoComprobante Where Codigo=@TipoComprobante And IDOperacion=@IDOperacion), 0)
		If @vIDTipoComprobante = 0 Begin
			Set @vIDTipoComprobante = ISNULL((Select Max(ID) + 1 From TipoComprobante), 1)
			Insert Into TipoComprobante(ID, Cliente, Proveedor, IDOperacion, Descripcion, Codigo, Resumen, Autonumerico, ComprobanteTimbrado, CalcularIVA, IVAIncluido, EsInterno, LibroCompra, LibroVenta, Signo, HechaukaTipoDocumento, HechaukaNumeroTimbrado, HechaukaTimbradoReemplazar, Estado)  
			Values(@vIDTipoComprobante, 1, 0, @IDOperacion, @TipoComprobante, @TipoComprobante, @TipoComprobante, 0, 1, 0, 1, 0, 0, 1, '+', 1, '', 1, 1)
		End

		--Punto de Expedicion
		Set @vIDPuntoExpedicion = IsNull((Select Top(1) ID From VPuntoExpedicion Where ReferenciaSucursal=@ReferenciaSucursal And ReferenciaPunto=@ReferenciaPuntoExpedicion And TipoComprobante=@TipoComprobante And NumeracionDesde <= @NroComprobante And NumeracionHasta >= @NroComprobante),0)
		If @vIDPuntoExpedicion = 0 Begin
			
			Declare @vTimbrado varchar(10)
			Declare @vDescripcionTimbrado varchar(50)

			Set @vTimbrado = '00' + @ReferenciaSucursal + @ReferenciaPuntoExpedicion
			Set @vDescripcionTimbrado = 'Importados ' + @vTimbrado

			--Verificar si ya existe el timbrado importado
			Set @vIDPuntoExpedicion = IsNull((Select Top(1) ID From VPuntoExpedicion Where Descripcion=@vDescripcionTimbrado),0)

			--Si no existe, creamos
			If @vIDPuntoExpedicion = 0 Begin
			
				Set @vIDPuntoExpedicion = ISNULL((Select MAX(ID) + 1 From PuntoExpedicion), 1)
			
				Insert Into PuntoExpedicion(ID, IDSucursal, Referencia, IDTipoComprobante, Timbrado, Vencimiento, NumeracionDesde, NumeracionHasta, CantidadPorTalonario, Estado, Descripcion)
				Values(@vIDPuntoExpedicion, @vIDSucursal, @ReferenciaPuntoExpedicion, @vIDTipoComprobante, @vTimbrado, GETDATE(), @NroComprobante, @NroComprobante, 500, 1, @vDescripcionTimbrado)
			
			End

			--Comprobante fuera del Rango
			if @NroComprobante < (Select NumeracionDesde From PuntoExpedicion Where ID=@vIDPuntoExpedicion) Begin
				Update PuntoExpedicion Set NumeracionDesde=@NroComprobante
				Where ID=@vIDPuntoExpedicion
			End
			
			--Comprobante fuera del Rango
			if @NroComprobante > (Select NumeracionHasta From PuntoExpedicion Where ID=@vIDPuntoExpedicion) Begin
				Update PuntoExpedicion Set NumeracionHasta=@NroComprobante
				Where ID=@vIDPuntoExpedicion
			End

		End
				
		--Tipo de Comprobante
		Set @vIDTipoComprobante = (IsNull((Select Top(1) ID From TipoComprobante Where Codigo=@TipoComprobante And IDOperacion=@IDOperacion), 0))
		If @vIDTipoComprobante = 0 Begin
			Set @vMensaje = 'Tipo de comprobante incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Sucursal
		Set @vIDSucursal = (IsNull((Select Top(1) ID From Sucursal Where Referencia=@ReferenciaSucursal),0))
		If @vIDSucursal = 0 Begin
			Set @vMensaje = 'Sucursal incorrecto 291!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Cliente
		Set @vIDCliente=(IsNull((Select Top(1) ID From Cliente Where Referencia=@Cliente),0))
		If @vIDCliente = 0 Begin
			
			Set @vIDCliente=(IsNull((Select Top(1) ID From Cliente Where RUC=@RUC),0))
			
			If @vIDCliente = 0 Begin
				Set @vMensaje = 'Cliente incorrecto!'
				Set @vProcesado = 'False'
				GoTo Salir
			End
			
		End
		
		--Sucursal Cliente
		Set @vIDSucursalCliente = NULL
		
		--Deposito
		--Establecemos como deposito el primero que exista en la sucursal
		Set @vIDDeposito = IsNull((Select Top(1) ID From Deposito Where IDSucursal=@vIDSucursal), 0)
		If @vIDDeposito = 0 Begin
			Set @vMensaje = 'Deposito no concuerda!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Lista de Precio
		Set @vIDListaPrecio = (IsNull((Select Top(1) ID  From ListaPrecio Where Descripcion=@ListaPrecio And IDSucursal=@vIDSucursal),0))
		If @vIDListaPrecio = 0 Begin
			
			--Insertamos
			Set @vIDListaPrecio = (IsNull((Select Max(ID) + 1  From ListaPrecio),0))
			
			Insert Into ListaPrecio(ID, IDSucursal, Descripcion, Estado)
			Values(@vIDListaPrecio, @vIDSucursal, @ListaPrecio, 'True')

		End
		
		--Vendedor
		Exec @vIDVendedor = SpVendedorInsUpd @Descripcion=@Vendedor, @IDSucursal=@vIDSucursal 

		--Actualizamos el Cliente
		If (Select IDSucursal From Cliente Where ID=@vIDCliente) Is Null Begin
			Update Cliente Set IDSucursal=@vIDSucursal
			Where ID=@vIDCliente
		End
		
	End
	
	--INSERTAR
	If @vOperacion = 'INS' Begin
		
		--Validar Informacion
		Begin
		
			--Numero
			If Exists(Select * From Venta Where Comprobante=@vComprobante) Begin
				set @vMensaje = 'El sistema detecto que el numero de comprobante ya esta registrado! Obtenga un numero siguiente.'
				set @vProcesado = 'False'
				GoTo Salir
			End
			
			--Comprobante
			if Exists(Select * From Venta Where IDPuntoExpedicion=@vIDPuntoExpedicion And NroComprobante=@NroComprobante) Begin
				set @vMensaje = 'El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
				set @vProcesado = 'False'
				GoTo Salir
			End
			
			--Comprobante fuera del Rango
			if @NroComprobante < (Select NumeracionDesde From PuntoExpedicion Where ID=@vIDPuntoExpedicion) Or @NroComprobante > (Select NumeracionHasta From PuntoExpedicion Where ID=@vIDPuntoExpedicion) Begin
				set @vMensaje = 'El numero de comprobante no esta dentro del rango correcto! Cambie el punto de expedicion o actualicelo.'
				set @vProcesado = 'False'
				GoTo Salir
			End
			
		End
		
		----Insertar la transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@MostrarTransaccion = 'False',
			@Mensaje = @vMensaje OUTPUT,
			@Procesado = @vProcesado OUTPUT,
			@IDTransaccion = @vIDTransaccion OUTPUT
		
		If @vProcesado = 'False' Begin
			GoTo Salir
		End
			
		--Insertar en Venta
		Insert Into Venta(IDTransaccion, IDPuntoExpedicion,   IDTipoComprobante,   NroComprobante,  Comprobante,   IDCliente,   IDSucursalCliente,   Direccion,  Telefono,   FechaEmision,  IDSucursal,   IDDeposito,   Credito,   FechaVencimiento,  IDListaPrecio,   Descuento, IDVendedor,   IDPromotor, IDMoneda, Cotizacion,  Observacion,   NroComprobanteRemision, Total,  TotalImpuesto,  TotalDiscriminado,   TotalDescuento,   Cobrado,   Descontado,   Acreditado,   Saldo,  Cancelado, Despachado, EsVentaSucursal,   Anulado,  FechaAnulado, IDUsuarioAnulado, Procesado)
		Values(@vIDTransaccion, @vIDPuntoExpedicion, @vIDTipoComprobante, @NroComprobante, @vComprobante, @vIDCliente, @vIDSucursalCliente, @Direccion, @vTelefono, @FechaEmision, @vIDSucursal, @vIDDeposito, @vCredito, @FechaVencimiento, @vIDListaPrecio, 0,         @vIDVendedor, NULL,       1,        @Cotizacion, @Observacion,  NULL,                   @Total, @TotalImpuesto, @vTotalDiscriminado, @TotalDescuento, @vCobrado, @vDescontado, @vAcreditado, @Saldo, @vCancelado,   'False',     @vEsVentaSucursal, @Anulado, @Anulado,    1,                'True')
		
		--Insertar en VentaImportacion
		Insert Into VentaImportada(IDTransaccion, Comprobante, TipoComprobante, Timbrado)
		Values(@vIDTransaccion, @vComprobante, @TipoComprobante, '')
		
		--Insertar Impuesto
		--10%
		Insert into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado)
		Values(@vIDTransaccion, 1, @Gravado10, @IVA10, @Descuento10, @Gravado10 - (@IVA10 + @Descuento10))
		
		--5%
		Insert into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado)
		Values(@vIDTransaccion, 2, @Gravado5, @IVA5, @Descuento5, @Gravado5 - (@IVA5 + @Descuento5))
		
		--Exento
		Insert into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado)
		Values(@vIDTransaccion, 3, @Exento, 0, @DescuentoExento, @Exento - @DescuentoExento)
		
		--Actualizar el Cliente (Saldo Total, Saldo Credito, Ultima Compra)
		EXEC SpAcualizarClienteVenta
			@IDCliente = @vIDCliente,
			@Mensaje = @vMensaje OUTPUT,
			@Procesado = @vProcesado OUTPUT
		
		Set @vMensaje = 'Registro guardado!'
		Set @vProcesado = 'True'
		GoTo Salir
				
	End
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End

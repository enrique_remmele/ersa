﻿CREATE Procedure [dbo].[SpRegistroHabilitacion]

	--Entrada
	@Asunto varchar(50),
	@Mensaje varchar(500),
	@IDUsuarioSolicitud int,
	@Comprobante varchar(50),
	@FechaComprobante date, 
	@Importe money,
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int
		
As

Begin

	--Variables
	Declare @vIDTransaccion numeric(18,0)
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	Declare @vNumero int
	Declare @vCodigoActivacion varchar(50)
		
	--Insertar la transaccion				
	EXEC SpTransaccion
		@IDUsuario = @IDUsuario,
		@IDSucursal = @IDSucursal,
		@IDDeposito = @IDDeposito,
		@IDTerminal = @IDTerminal,
		@IDOperacion = @IDOperacion,
		@Mensaje = @vMensaje OUTPUT,
		@Procesado = @vProcesado OUTPUT,
		@IDTransaccion = @vIDTransaccion OUTPUT
	
	--Codigo de Activacion
	Declare @Random int
	Select @Random = ROUND(((9999 - 1000 -1) * RAND() + 1000), 0)
	Set @vCodigoActivacion = CONVERT(varchar(50), @Random)

	--Nuevo Numero
	Set @vNumero = (Select ISNULL(Max(Numero)+1, 1) From RegistroHabilitacion)	
	
	--Insertar
	Insert Into RegistroHabilitacion(IDTransaccion, Numero, Asunto, Mensaje, IDUsuarioSolicitud, CodigoActivacion, FechaActivacion, Pendiente, Comprobante, FechaComprobante, Importe)
	Values(@vIDTransaccion, @vNumero, @Asunto, @Mensaje, @IDUsuarioSolicitud, @vCodigoActivacion, NULL, 'True', @Comprobante, @FechaComprobante, @Importe)
	
Salir:
	Select * From RegistroHabilitacion Where IDTransaccion=@vIDTransaccion
End







﻿CREATE Procedure [dbo].[SpCargarKardex]

@IDProducto int

As

Begin

	SET NOCOUNT ON

	Declare @vIDKardex numeric(18,0)
	Declare @vOrden tinyint
	Declare @vAño smallint
	Declare @vMes tinyint
	Declare @vFecha datetime
	Declare @vIDTransaccion numeric(18,0)
	Declare @vIDProducto int
	Declare @vID tinyint
	Declare @vCantidadEntrada decimal(10,2)
	Declare @vCantidadSalida decimal(10,2)
	Declare @vCantidadSaldo decimal(10,2)
	Declare @vCostoEntrada money
	Declare @vCostoSalida money
	Declare @vCostoEntradaAnterior money
	Declare @vCostoSalidaAnterior money
	Declare @vCostoPromedio money
	Declare @vTotalSaldo money
	Declare @vIDSucursal int
	Declare @vComprobante varchar(50)
	Declare @vTipo varchar(15)
	Declare @vIDKardexAnterior int
	Declare @vCantidadSaldoAnterior decimal(10,2)
	Declare @vCostoPromedioAnterior money
	Declare @vTotalSaldoAnterior money
	Declare @vCostoPromedioOperacion money
	Declare @vFechaInicio Date
	Declare @vIDProductoActual int = 0
	Declare @vIndice int = 0

	
	Begin

		print 'inicio'

		--Set @vFechaInicio = (Select Max(Fecha) From CierreAperturaStock Where Anulado='False')
		If @vFechaInicio Is Null Begin
			Set @vFechaInicio = '20190901'
		End
		
		print 'Obtencion de fecha'

		--Delete From Kardex Where Fecha>=@vFechaInicio and IDProducto = @IDProducto and Indice <> 0
		print 'Eliminacin de tabla'
		
		Declare db_cursor cursor for
		Select	FechaEntrada, IDTransaccion, IDProducto, ID, Orden, IDSucursal, Comprobante, CantidadEntrada, CantidadSalida, CostoEntrada, CostoSalida, Tipo 
		From VCargaKardex
		Where FechaEntrada>=@vFechaInicio
		and IDPRoducto = @IDProducto
				
		Order by FechaEntrada
		Open db_cursor   

		Fetch Next From db_cursor Into @vFecha, @vIDTransaccion, @vIDProducto, @vID, @vOrden, @vIDSucursal, @vComprobante, @vCantidadEntrada, @vCantidadSalida, @vCostoEntrada, @vCostoSalida, @vTipo
		While @@FETCH_STATUS = 0 Begin 
			
			Set @vCostoPromedioOperacion = @vCostoEntrada + @vCostoSalida

			--Si la operacion del producto es la primera
	--		If @vIDProducto != @vIDProductoActual Begin	
			if @vIndice = 0 begin
				print 'primera vez'

				Set @vIDProductoActual = @vIDProducto
				Set @vIDKardex = IsNull((Select Max(IDKardex) + 1 From Kardex), 1)
				Set @vIDKardexAnterior = 0

				Set @vTotalSaldo = (IsNull(@vCantidadEntrada,0)*IsNull(@vCostoEntrada, 0)) - (IsNull(@vCantidadSalida,0)*IsNull(@vCostoSalida, 0)) 
				Set @vCantidadSaldo = IsNull(@vCantidadEntrada,0) - IsNull(@vCantidadSalida,0)
				
				Insert Into Kardex(IDKardex, Fecha, IDTransaccion, IDProducto, ID, Orden, Indice, IDSucursal, Comprobante, CantidadEntrada, CantidadSalida, CantidadSaldo, CostoUnitarioEntrada, CostoUnitarioSalida, TotalSaldo, IDKardexAnterior, CostoPromedioOperacion, CostoPromedio)
				Values(@vIDKardex, @vFecha, @vIDTransaccion, @IDProducto, @vID, @vOrden, 0, @vIDSucursal, @vComprobante, @vCantidadEntrada, @vCantidadSalida, @vCantidadSaldo, @vCostoEntrada, @vCostoSalida, @vTotalSaldo, @vIDKardexAnterior,@vCostoPromedioOperacion,@vCostoPromedioOperacion)
				Set @vIndice = @vIndice +1
			end
			else begin
				print concat('entro', @vIDTransaccion)
				Select @vIDKardexAnterior=Max(IDKardex),
						@vIndice=ISNULL(Max(Indice) + 1, 1) 
				from Kardex Where IDProducto=@IDProducto And Fecha<=@vFecha

				Select	@vCantidadSaldoAnterior=CantidadSaldo,
						@vTotalSaldoAnterior=TotalSaldo,
						@vCostoPromedioAnterior =CostoPromedio
				from Kardex Where IDKardex=@vIDKardexAnterior

					--Si NO es compra, tomar el costo anterior
				If @vTipo not in ('COMPRA','PRODUCCION')  Begin
				print 'aqui 105'
					If @vTipo = 'AJUSTECOSTO'  Begin
						Set @vCostoPromedioAnterior = @vCostoEntrada + @vCostoSalida 
					end

					--En caso de que el costo promedio anterior sea cero, se toma el costo de la operacion.
					If @vCostoPromedioAnterior = 0 Begin
						Set @vCostoPromedioAnterior = @vCostoEntrada + @vCostoSalida 
					End

					If isnull(@vCostoEntrada,0) <> 0 and isnull(@vCostoSalida,0) = 0 begin
						Set @vCostoEntrada = @vCostoPromedioAnterior
					end

					If isnull(@vCostoSalida,0) <> 0 and isnull(@vCostoEntrada,0) = 0 begin
						Set @vCostoSalida = @vCostoPromedioAnterior
						Set @vCostoPromedio = @vCostoPromedioAnterior
					end

					Set @vCostoPromedioOperacion = @vCostoPromedioAnterior
					Set @vCostoPromedio = @vCostoPromedioAnterior

				End
				-- Si ES compra o ticket calcula el costo promedio
				else begin
				print 'aqui 130'
					if @vCantidadSalida <> 0 begin
						Set @vCostoPromedio = @vCostoPromedioAnterior
					end
					else begin
						-- Si es compra y la cantidad anterior es menor o igual a cero NO PROMEDIA
						If  @vCantidadSaldoAnterior <= 0 And @vTipo in ('COMPRA','PRODUCCION') Begin
							Set @vCantidadSaldo = (IsNull(@vCantidadEntrada,0))+(IsNull(@vCantidadSalida,0))
							Set @vTotalSaldo = (IsNull((@vCostoEntrada * @vCantidadEntrada), 0))+(ISNULL(@vCostoSalida * @vCantidadSalida,0)) 
							Set @vCostoPromedio = @vCostoPromedioOperacion
							Goto Actualizar
						End
						ELSE BEGIN
							
							Set @vCostoPromedio = ((
							(@vCantidadSaldoAnterior*@vCostoPromedioAnterior)+
							(IsNull((@vCostoEntrada * @vCantidadEntrada), 0)-(ISNULL(@vCostoSalida * @vCantidadSalida,0)))
							)/@vCantidadSaldo)

						END
					end
				end
				print 'aqui 151'
				--Saldos
				Set @vCantidadSaldo = (IsNull(@vCantidadSaldoAnterior,0)+IsNull(@vCantidadEntrada,0))-(IsNull(@vCantidadSalida,0))
				If @vCostoEntrada=0 And @vCostoSalida=0 Begin --si no se pasa costo se pone totalsaldo cero para que al dividir por la cantidad el costo promedio sea 0
					Set @vTotalSaldo = 0
				End Else Begin
					print 'aqui 157'
					Set @vTotalSaldo = (IsNull((@vTotalSaldoAnterior),0)+IsNull((@vCostoEntrada * @vCantidadEntrada), 0))-(ISNULL(@vCostoSalida * @vCantidadSalida,0)) 	
				End
				
	Actualizar:			
				Set @vIDKardex = (Select ISNULL(Max(IDKardex) + 1, 1) from Kardex)

				Insert Into Kardex(IDKardex, Fecha, IDTransaccion, IDProducto, ID, Orden, Indice ,IDSucursal, Comprobante, CantidadEntrada, CantidadSalida, CantidadSaldo, CostoUnitarioEntrada, CostoUnitarioSalida, TotalSaldo, IDKardexAnterior, CostoPromedioOperacion, CostoPromedio)
				Values(@vIDKardex, @vFecha, @vIDTransaccion, @IDProducto, @vID, @vOrden, @vIndice, @vIDSucursal, @vComprobante, @vCantidadEntrada, @vCantidadSalida, @vCantidadSaldo, @vCostoEntrada, @vCostoSalida, @vTotalSaldo, @vIDKardexAnterior, @vCostoPromedioOperacion, @vCostoPromedio)		


			end
			
			Fetch Next From db_cursor Into @vFecha, @vIDTransaccion, @vIDProducto, @vID, @vOrden, @vIDSucursal, @vComprobante, @vCantidadEntrada, @vCantidadSalida, @vCostoEntrada, @vCostoSalida, @vTipo

		End

		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor

	End

End

﻿CREATE Procedure [dbo].[SpOrdenPagoEliminarAplicacion]

	--Entrada
	@IDTransaccionOP int,
	@IDTransaccionEgreso int,
	@Cuota tinyint,
	@Operacion varchar(10),

	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

  
	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'

	If @Operacion = 'SOLICITAR' Begin
		
		if exists(Select * from SolicitudEliminacionOrdenPagoEgreso where IDTransaccionOrdenPago = @IDTransaccionOP and IDTransaccionEgreso = @IDTransaccionEgreso and Cuota = @Cuota and Aprobado is null) begin
			Set @Mensaje = 'Ya existe una solicitud para este Comprobante/Cuota'
			Set @Procesado = 'False'
			return @@rowcount
		end
		
		Insert into SolicitudEliminacionOrdenPagoEgreso(IDTransaccionOrdenPago, IDTransaccionEgreso, Cuota, IDUsuarioSolicitud, FechaSolicitud) 
		values(@IDTransaccionOP, @IDTransaccionEgreso, @Cuota, @IDUsuario, GetDate())

		Set @Mensaje = 'Registro Insertado'
		Set @Procesado = 'True'
		return @@rowcount
		
	End

	If @Operacion = 'APROBAR' Begin
		
		if not exists(Select * from SolicitudEliminacionOrdenPagoEgreso where IDTransaccionOrdenPago = @IDTransaccionOP and IDTransaccionEgreso = @IDTransaccionEgreso and Cuota = @Cuota) begin
			Set @Mensaje = 'No se encuentra registro solicitado'
			Set @Procesado = 'False'
			return @@rowcount
		end
		
		Update SolicitudEliminacionOrdenPagoEgreso
			Set Aprobado = 'True'
			,IDUsuarioAprobador = @IDUsuario
			,FechaAprobacion = GetDate()
		Where IDTransaccionOrdenPago = @IDTransaccionOP
		and IDTransaccionEgreso = @IDTransaccionEgreso 
		and Cuota = @Cuota
		and Aprobado is null

		Delete from OrdenPagoEgreso
		Where IDTransaccionOrdenPago = @IDTransaccionOP
		and IDTransaccionEgreso = @IDTransaccionEgreso 
		and Cuota = @Cuota
		
		exec [dbo].[SpRepararEgreso] @IDTransaccion=@IDTransaccionEgreso

		
		Set @Mensaje = 'Registro Aprobado'
		Set @Procesado = 'True'
		return @@rowcount

	End

	If @Operacion = 'RECHAZAR' Begin
		
		if not exists(Select * from SolicitudEliminacionOrdenPagoEgreso where IDTransaccionOrdenPago = @IDTransaccionOP and IDTransaccionEgreso = @IDTransaccionEgreso and Cuota = @Cuota) begin
			
			Set @Mensaje = 'No se encuentra registro solicitado'
			Set @Procesado = 'False'
			print @Mensaje
			print concat('IDTransaccionOrdenPago = ',@IDTransaccionOP,' and IDTransaccionEgreso = ',@IDTransaccionEgreso ,'and Cuota =', @Cuota)
			return @@rowcount
		end

		Update SolicitudEliminacionOrdenPagoEgreso
			Set Aprobado = 'False'
			,IDUsuarioAprobador = @IDUsuario
			,FechaAprobacion = GetDate()
		Where IDTransaccionOrdenPago = @IDTransaccionOP
		and IDTransaccionEgreso = @IDTransaccionEgreso 
		and Cuota = @Cuota
		and Aprobado is null

		Set @Mensaje = 'Registro Rechazado'
		Set @Procesado = 'True'
		return @@rowcount
	End

	If @Operacion = 'RECUPERAR' Begin
		
		if not exists(Select * from SolicitudEliminacionOrdenPagoEgreso where IDTransaccionOrdenPago = @IDTransaccionOP and IDTransaccionEgreso = @IDTransaccionEgreso and Cuota = @Cuota) begin
			Set @Mensaje = 'No se encuentra registro solicitado'
			Set @Procesado = 'False'
			return @@rowcount
		end

		Update SolicitudEliminacionOrdenPagoEgreso
			Set Aprobado = null
			,IDUsuarioAprobador = null
			,FechaAprobacion = null
		Where IDTransaccionOrdenPago = @IDTransaccionOP
		and IDTransaccionEgreso = @IDTransaccionEgreso 
		and Cuota = @Cuota
		and Aprobado is not null

		Set @Mensaje = 'Registro Rechazado'
		Set @Procesado = 'True'
		return @@rowcount
	End
	
End
	





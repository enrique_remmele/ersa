﻿CREATE Procedure [dbo].[SpImportarDatos]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50) = '',
	@Scrip varchar(50) = '',
	@EsTransaccion bit = 'False',
	@IDOperacion int = 0,
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From ImportarDatos Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDImportarDatos tinyint
		set @vIDImportarDatos = (Select IsNull((Max(ID)+1), 1) From ImportarDatos)

		--Insertamos
		Insert Into ImportarDatos(ID, Descripcion, Scrip, EsTransaccion, IDOperacion)
		Values(@vIDImportarDatos, @Descripcion, @Scrip, @EsTransaccion, @IDOperacion)		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From ImportarDatos Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From ImportarDatos Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update ImportarDatos Set Descripcion=@Descripcion, 
								Scrip=@Scrip,
								EsTransaccion=@EsTransaccion,
								IDOperacion=@IDOperacion 
						
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From ImportarDatos Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From ImportarDatos 
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


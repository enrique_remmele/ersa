﻿CREATE Procedure [dbo].[SpSubLineaInsUpd]

	--Entrada
	@Descripcion varchar(100)
	
As

Begin

	Declare @vID int
	
	If @Descripcion = '' Begin
		Set @Descripcion = '---'
	End
	
	--Si existe
	If Exists(Select * From SubLinea Where Descripcion = @Descripcion) Begin
		Set @vID = (Select Top(1) ID From SubLinea Where Descripcion = @Descripcion)
		
	End Else Begin
		Set @vID = (Select IsNull(Max(ID)+1,1) From SubLinea)
		Insert Into SubLinea(ID, Descripcion, Estado)
		Values(@vID, @Descripcion, 'True')
	End
	
	return @vID
	
End


﻿CREATE Procedure [dbo].[SpViewChequerasTotal]

As

Begin

	--variables
	Declare @ID int
	Declare @IDCuentaBancaria int
	Declare @Desde int
	Declare @Hasta int

	
	--Crear la tabla
	Create table #Moneda(IDChequera int,
						 IdCuentaBancaria int,
						 Cantidad int,
						 utilizados int,
						 Saldo int,
						 Desde int,
						 HAsta int
								)
								

	Declare db_cursor cursor for
	Select CC.ID, C.ID, C.NroDesde, C.NroHasta
	From CuentaBancaria CC
	Join Chequera C on C.IDCuentaBancaria = CC.ID
	--where IDCuentaBancaria = 5
	order by CC.ID
	Open db_cursor   
	Fetch Next From db_cursor Into	@IDCuentaBancaria, @ID, @Desde, @Hasta
	While @@FETCH_STATUS = 0 Begin 
	 Declare @Saldo integer = ISnull((select  count(distinct NroCheque) from vCheque where IDCuentaBancaria =@IDCuentaBancaria and NroCheque between @Desde and @Hasta ),0)
	 		insert into #Moneda (IDChequera, IDCuentaBancaria,Cantidad,utilizados, Saldo, Desde, HAsta)
		values (@ID, @IDCuentaBancaria,(@Hasta - @Desde + 1),@saldo,((@Hasta-@Desde+1)-@Saldo), @Desde, @Hasta)
Siguiente:
		
		Fetch Next From db_cursor Into  @IDCuentaBancaria, @ID,@Desde, @Hasta
		
	End

	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor		   		
	
	
	Select M.*, CC.CuentaBancaria, CC.Banco, CC.Moneda, CC.CodigoCuentaContable
	From #Moneda M
	Join vCuentaBancaria CC on CC.ID = M.IdCuentaBancaria
	Join Chequera CH on CH.IDCuentaBancaria = CC.ID and CH.NRoDesde = M.Desde and CH.NroHasta = M.Hasta
End





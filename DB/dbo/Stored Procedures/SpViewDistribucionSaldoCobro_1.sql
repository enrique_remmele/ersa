﻿CREATE procedure [dbo].[SpViewDistribucionSaldoCobro]
					   	
	--Entrada
	@Fecha1 Date,
	@Fecha2 Date,
	@Fecha3 Date,
	@Fecha4 Date,
	@Fecha5 Date,
	@Fecha6 Date,
	@Fecha7 Date,
	@Fecha8 Date,
	@Fecha9 Date,
	@FechaReferencia date,
	@Condicion bit,
	@Credito bit = 'True',
	@Contado bit = 'True'
	    

As

Begin
    
    
    
    --Crear la tabla temporal
    create table #TablaTemporal(ID tinyint,
								IDTransaccion int,
								Cliente varchar(50),
								Referencia varchar(50),
								IDTipoComprobante int,
								IDVendedor int,
								Vendedor varchar(50),
								IDCobrador int,
								Cobrador varchar(50),
								IDTipoCliente int,
								IDEstado int,
								IDSucursal int,
								Sucursal varchar(50),
								IDMoneda int,
								IDZonaVenta int,
								FechaVencimiento date,
								Credito bit,
								Comprobante varchar(50),
								Total money,
								Fecha1 money,
								Fecha2 money,
								Fecha3 money,
								Fecha4 money,
								Fecha5 money,
								Fecha6 money,
								Fecha7 money,
								Fecha8 money,
								Fecha9 money,
								Fecha10 money,
								IDTipoProducto int,
								Judicial bit,
								IDSucursalFiltro int,
								IDCliente int)
    
    
	
	--Declarar variables 
	declare @vID tinyint
	declare @vIDTransaccion numeric (18,0)
	declare @vRazonSocial varchar (100)
	declare @vReferencia varchar(50)
	declare @vIDTipoComprobante int
	declare @vIDVendedor int
	declare @vVendedor varchar(50)
	declare @vIDCobrador int
	declare @vCobrador varchar(50)
	declare @vIDTipoCliente int
	declare @vIDEstadoCliente int
	declare @vIDSucursal int
	declare @vSucursal varchar(50)
	declare @vIDMoneda int
	declare @vIDZonaVenta int
	declare @vSaldo money
	declare @vFechaVencimiento date
	declare @vTotal money
	declare @vCredito bit
	declare @vComprobante varchar(50)
	declare @vIDTipoProducto int
	declare @vJudicial bit
	declare @vIDSucursalFiltro int
	declare @vIDCliente int

	declare @vFecha1 money
	declare @vFecha2 money
	declare @vFecha3 money
	declare @vFecha4 money
	declare @vFecha5 money
	declare @vFecha6 money
	declare @vFecha7 money
	declare @vFecha8 money
	declare @vFecha9 money
	declare @vFecha10 money
	
	
	set @vID= (Select ISNULL (MAX(ID)+1,1) from #TablaTemporal)
	
--Condicion= A Vencer
If @Condicion = 0 Begin	

	--Contado o Credito
	If @Credito != @Contado Begin
		Declare db_cursor cursor for
		Select 
		V.IDTransaccion, 
		C.RazonSocial,
		C.Referencia,
		V.IDTipoComprobante,
		V.IDVendedor,
		V.Vendedor,
		C.IDCobrador,
		V.Cobrador,
		C.IDTipoCliente,
		C.IDEstado,
		V.IDSucursal,
		V.Sucursal,
		V.IDMoneda,
		C.IDZonaVenta,
		V.Saldo,
		V.FechaVencimiento,
		V.Credito,
		V.Comprobante,
		'IDTipoProducto'=Isnull((Select top(1) IDTipoProducto from vdetalleVenta where IDTransaccion = V.IDTransaccion),0),
		C.Judicial,
		V.IDSucursalFiltro,
		V.IDCliente
		From Cliente C
		Join VVenta V On C.ID=V.IDCliente
		Where V.Saldo >= 1 And V.Anulado='False' And V.Contado=@Contado And V.Credito=@Credito
		ORder by IDCliente, IDMoneda
 		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccion,@vRazonSocial,@vReferencia,@vIDTipoComprobante,@vIDVendedor,@vVendedor,@vIDCobrador,@vCobrador,@vIDTipoCliente,@vIDEstadoCliente,@vIDSucursal,@vSucursal,@vIDMoneda,@vIDZonaVenta,@vSaldo,@vFechaVencimiento,@vCredito,@vComprobante, @vIDTipoProducto,@vJudicial, @vIDSucursalFiltro,@vIDCliente
		While @@FETCH_STATUS = 0 Begin  
		
			set @vFecha1 = 0
			set @vFecha2 = 0
			set @vFecha3 = 0
			set @vFecha4 = 0
			set @vFecha5 = 0
			set @vFecha6 = 0
			set @vFecha7 = 0
			set @vFecha8 = 0
			set @vFecha9 = 0
			set @vFecha10 = 0
			set @vTotal = 0
		
								
			----Fecha1
			If @vFechaVencimiento <= @Fecha1 And @vFechaVencimiento >= @FechaReferencia
			Begin
				Set @vFecha1 = @vSaldo
			End
								
			--Fecha2
			If @vFechaVencimiento > @Fecha1 and @vFechaVencimiento <= @Fecha2
			Begin
				Set @vFecha2 = @vSaldo
			End
			
			--Fecha3
			If @vFechaVencimiento > @Fecha2 And @vFechaVencimiento <= @Fecha3 
			Begin
				Set @vFecha3 = @vSaldo
			End
			
			--Fecha4
			If @vFechaVencimiento > @Fecha3 And @vFechaVencimiento <= @Fecha4 
			Begin
				Set @vFecha4 = @vSaldo
			End
			
			--Fecha5
			If @vFechaVencimiento > @Fecha4 And @vFechaVencimiento <= @Fecha5 
			Begin
				Set @vFecha5 = @vSaldo
			End
			
			--Fecha6
			If @vFechaVencimiento > @Fecha5 And @vFechaVencimiento<= @Fecha6 
			Begin
				Set @vFecha6 = @vSaldo
			End
			
			--Fecha7
			If @vFechaVencimiento > @Fecha6 And @vFechaVencimiento <= @Fecha7
			Begin
				Set @vFecha7 = @vSaldo
			End
			
			--Fecha8
			If @vFechaVencimiento > @Fecha7 And @vFechaVencimiento <= @Fecha8
			Begin
				Set @vFecha8 = @vSaldo
			End
			
			--Fecha9
			If @vFechaVencimiento > @Fecha8 And @vFechaVencimiento <= @Fecha9
			Begin
				Set @vFecha9 = @vSaldo
			End
			
			--Fecha10
			If @vFechaVencimiento > @Fecha9 
			Begin
				Set @vFecha10 = @vSaldo
			End
							
			--Set @vTotal = @vSaldo 
			Set @vTotal = @vFecha1 + @vFecha2 + @vFecha3 + @vFecha4 + @vFecha5 +
			              @vFecha6 + @vFecha7 + @vFecha8 + @vFecha9 + @vFecha10 		
			
			--Actualizar si ya existe
			If  Exists(Select * From #TablaTemporal Where Referencia=@vReferencia) Begin				
			--Update #TablaTemporal Set Total=Total+@vSaldo,
			Update #TablaTemporal Set Total=Total+@vFecha1 + @vFecha2 + @vFecha3 + @vFecha4 + @vFecha5 +
			              @vFecha6 + @vFecha7 + @vFecha8 + @vFecha9 + @vFecha10,
										  Fecha1=Fecha1+@vFecha1,
										  Fecha2=Fecha2+@vFecha2,
										  Fecha3=Fecha3+@vFecha3,
										  Fecha4=Fecha4+@vFecha4,
										  Fecha5=Fecha5+@vFecha5,
										  Fecha6=Fecha6+@vFecha6,
										  Fecha7=Fecha7+@vFecha7,
										  Fecha8=Fecha8+@vFecha8,
										  Fecha9=Fecha9+@vFecha9,
										  Fecha10=Fecha10+@vFecha10
										Where Referencia=@vReferencia 
										and IDMoneda = @vIDMoneda
			End
			
			--Insertar si aún no existe		
			If Not Exists(Select * From #TablaTemporal Where Referencia=@vReferencia) Begin			
				Insert into #TablaTemporal(ID,IDTransaccion,Cliente,Referencia,IDTipoComprobante,IDVendedor,Vendedor,IDCobrador,Cobrador,IDTipoCliente,IDEstado,IDSucursal,Sucursal,IDMoneda,IDZonaVenta,FechaVencimiento,Credito,Comprobante,Total,Fecha1,Fecha2,Fecha3,Fecha4,Fecha5,Fecha6,Fecha7,Fecha8,Fecha9,Fecha10,IDTipoProducto, Judicial, IDSucursalFiltro, IDCliente)
				Values (@vID,@vIDTransaccion,@vRazonSocial,@vReferencia,@vIDTipoComprobante,@vIDVendedor,@vVendedor,@vIDCobrador,@vCobrador,@vIDTipoCliente,@vIDEstadoCliente,@vIDSucursal,@vSucursal,@vIDMoneda,@vIDZonaVenta,@vFechaVencimiento,@vCredito,@vComprobante,@vTotal,@vFecha1, @vFecha2, @vFecha3, @vFecha4, @vFecha5, @vFecha6, @vFecha7, @vFecha8, @vFecha9,@vFecha10,@vIDTipoProducto,@vJudicial,@vIDSucursalFiltro, @vIDCliente) 	
			End
			
			
			
			Fetch Next From db_cursor Into @vIDTransaccion,@vRazonSocial,@vReferencia,@vIDTipoComprobante,@vIDVendedor,@vVendedor,@vIDCobrador,@vCobrador,@vIDTipoCliente,@vIDEstadoCliente,@vIDSucursal,@vSucursal,@vIDMoneda,@vIDZonaVenta,@vSaldo,@vFechaVencimiento,@vCredito,@vComprobante,@vIDTipoProducto,@vJudicial,@vIDSucursalFiltro, @vIDCliente
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
	End
	
	--Contado + Credito
	If @Credito = @Contado Begin
		Declare db_cursor cursor for
		Select 
		V.IDTransaccion, 
		C.RazonSocial,
		C.Referencia,
		V.IDTipoComprobante,
		V.IDVendedor,
		V.Vendedor,
		C.IDCobrador,
		V.Cobrador,
		C.IDTipoCliente,
		C.IDEstado,
		V.IDSucursal,
		V.Sucursal,
		V.IDMoneda,
		C.IDZonaVenta,
		V.Saldo,
		V.FechaVencimiento,
		V.Credito,
		V.Comprobante,
		'IDTipoProducto'=Isnull((Select top(1) IDTipoProducto from vdetalleVenta where IDTransaccion = V.IDTransaccion),0),
		C.Judicial,
		V.IDSucursalFiltro,
		V.IDCliente
		From Cliente C
		Join VVenta V On C.ID=V.IDCliente
		Where V.Saldo >= 1 And V.Anulado='False' 
		ORder by IDCliente, IDMoneda
 		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccion,@vRazonSocial,@vReferencia,@vIDTipoComprobante,@vIDVendedor,@vVendedor,@vIDCobrador,@vCobrador,@vIDTipoCliente,@vIDEstadoCliente,@vIDSucursal,@vSucursal,@vIDMoneda,@vIDZonaVenta,@vSaldo,@vFechaVencimiento,@vCredito,@vComprobante,@vIDTipoProducto,@vJudicial,@vIDSucursalFiltro, @vIDCliente
		While @@FETCH_STATUS = 0 Begin  
		
			set @vFecha1 = 0
			set @vFecha2 = 0
			set @vFecha3 = 0
			set @vFecha4 = 0
			set @vFecha5 = 0
			set @vFecha6 = 0
			set @vFecha7 = 0
			set @vFecha8 = 0
			set @vFecha9 = 0
			set @vFecha10 = 0
			set @vTotal = 0
		
								
			----Fecha1
			If @vFechaVencimiento <= @Fecha1 And @vFechaVencimiento >= @FechaReferencia
			Begin
				Set @vFecha1 = @vSaldo
			End
								
			--Fecha2
			If @vFechaVencimiento > @Fecha1 and @vFechaVencimiento <= @Fecha2
			Begin
				Set @vFecha2 = @vSaldo
			End
			
			--Fecha3
			If @vFechaVencimiento > @Fecha2 And @vFechaVencimiento <= @Fecha3 
			Begin
				Set @vFecha3 = @vSaldo
			End
			
			--Fecha4
			If @vFechaVencimiento > @Fecha3 And @vFechaVencimiento <= @Fecha4 
			Begin
				Set @vFecha4 = @vSaldo
			End
			
			--Fecha5
			If @vFechaVencimiento > @Fecha4 And @vFechaVencimiento <= @Fecha5 
			Begin
				Set @vFecha5 = @vSaldo
			End
			
			--Fecha6
			If @vFechaVencimiento > @Fecha5 And @vFechaVencimiento<= @Fecha6 
			Begin
				Set @vFecha6 = @vSaldo
			End
			
			--Fecha7
			If @vFechaVencimiento > @Fecha6 And @vFechaVencimiento <= @Fecha7
			Begin
				Set @vFecha7 = @vSaldo
			End
			
			--Fecha8
			If @vFechaVencimiento > @Fecha7 And @vFechaVencimiento <= @Fecha8
			Begin
				Set @vFecha8 = @vSaldo
			End
			
			--Fecha9
			If @vFechaVencimiento > @Fecha8 And @vFechaVencimiento <= @Fecha9
			Begin
				Set @vFecha9 = @vSaldo
			End
			
			--Fecha10
			If @vFechaVencimiento > @Fecha9 
			Begin
				Set @vFecha10 = @vSaldo
			End
							
			--Set @vTotal = @vSaldo 
			Set @vTotal = @vFecha1 + @vFecha2 + @vFecha3 + @vFecha4 + @vFecha5 +
			              @vFecha6 + @vFecha7 + @vFecha8 + @vFecha9 + @vFecha10
			
			--Actualizar si ya existe
			If  Exists(Select * From #TablaTemporal Where Referencia=@vReferencia) Begin				
			--Update #TablaTemporal Set Total=Total+@vSaldo,
			Update #TablaTemporal Set Total=Total+@vFecha1 + @vFecha2 + @vFecha3 + @vFecha4 + @vFecha5 +
			              @vFecha6 + @vFecha7 + @vFecha8 + @vFecha9 + @vFecha10,
										  Fecha1=Fecha1+@vFecha1,
										  Fecha2=Fecha2+@vFecha2,
										  Fecha3=Fecha3+@vFecha3,
										  Fecha4=Fecha4+@vFecha4,
										  Fecha5=Fecha5+@vFecha5,
										  Fecha6=Fecha6+@vFecha6,
										  Fecha7=Fecha7+@vFecha7,
										  Fecha8=Fecha8+@vFecha8,
										  Fecha9=Fecha9+@vFecha9,
										  Fecha10=Fecha10+@vFecha10
										Where Referencia=@vReferencia 
										and IDMoneda = @vIDMoneda
			End
			
			--Insertar si aún no existe		
			If Not Exists(Select * From #TablaTemporal Where Referencia=@vReferencia) Begin			
				Insert into #TablaTemporal(ID,IDTransaccion,Cliente,Referencia,IDTipoComprobante,IDVendedor,Vendedor,IDCobrador,Cobrador,IDTipoCliente,IDEstado,IDSucursal,Sucursal,IDMoneda,IDZonaVenta,FechaVencimiento,Credito,Comprobante,Total,Fecha1,Fecha2,Fecha3,Fecha4,Fecha5,Fecha6,Fecha7,Fecha8,Fecha9,Fecha10,IDTipoProducto, Judicial, IDSucursalFiltro, IDCliente)
				Values (@vID,@vIDTransaccion,@vRazonSocial,@vReferencia,@vIDTipoComprobante,@vIDVendedor,@vVendedor,@vIDCobrador,@vCobrador,@vIDTipoCliente,@vIDEstadoCliente,@vIDSucursal,@vSucursal,@vIDMoneda,@vIDZonaVenta,@vFechaVencimiento,@vCredito,@vComprobante,@vTotal,@vFecha1, @vFecha2, @vFecha3, @vFecha4, @vFecha5, @vFecha6, @vFecha7, @vFecha8, @vFecha9,@vFecha10,@vIDTipoProducto,@vJudicial,@vIDSucursalFiltro, @vIDCliente) 	
			End
			
			
			
			Fetch Next From db_cursor Into @vIDTransaccion,@vRazonSocial,@vReferencia,@vIDTipoComprobante,@vIDVendedor,@vVendedor,@vIDCobrador,@vCobrador,@vIDTipoCliente,@vIDEstadoCliente,@vIDSucursal,@vSucursal,@vIDMoneda,@vIDZonaVenta,@vSaldo,@vFechaVencimiento,@vCredito,@vComprobante,@vIDTipoProducto, @vJudicial,@vIDSucursalFiltro, @vIDCliente
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
	End
	
End	

--VENCIDOS
If @Condicion = 1 Begin		
  IF @Contado != @Credito Begin
	Declare db_cursor cursor for
	Select 
	V.IDTransaccion, 
	C.RazonSocial,
	C.Referencia,
	V.IDTipoComprobante,
	V.IDVendedor,
	V.Vendedor,
	C.IDCobrador,
	V.Cobrador,
	C.IDTipoCliente,
	C.IDEstado,
	V.IDSucursal,
	V.Sucursal,
	V.IDMoneda,
	C.IDZonaVenta,
	V.Saldo,
	V.FechaVencimiento,
	V.Credito,
	V.Comprobante,
	'IDTipoProducto'=Isnull((Select top(1) IDTipoProducto from vdetalleVenta where IDTransaccion = V.IDTransaccion),0)	,
	C.Judicial,
	V.IDSucursalFiltro,
	V.IDCliente
	From Cliente C
	Join VVenta V On C.ID=V.IDCliente
	Where V.Saldo >= 1 And V.Anulado='False' And V.Contado=@Contado And V.Credito=@Credito
	Open db_cursor   
	Fetch Next From db_cursor Into @vIDTransaccion,@vRazonSocial,@vReferencia,@vIDTipoComprobante,@vIDVendedor,@vVendedor,@vIDCobrador,@vCobrador,@vIDTipoCliente,@vIDEstadoCliente,@vIDSucursal,@vSucursal,@vIDMoneda,@vIDZonaVenta,@vSaldo,@vFechaVencimiento,@vCredito,@vComprobante, @vIDTipoProducto, @vJudicial,@vIDSucursalFiltro, @vIDCliente
	While @@FETCH_STATUS = 0 Begin  

		set @vFecha1 = 0
		set @vFecha2 = 0
		set @vFecha3 = 0
		set @vFecha4 = 0
		set @vFecha5 = 0
		set @vFecha6 = 0
		set @vFecha7 = 0
		set @vFecha8 = 0
		set @vFecha9 = 0
		set @vFecha10 = 0
		set @vTotal = 0	
							
		--Fecha1
		If @vFechaVencimiento >= @Fecha1  And @vFechaVencimiento <= @FechaReferencia 
		Begin
			Set @vFecha1 = @vSaldo
		End
				
		--Fecha2
		If @vFechaVencimiento >= @Fecha2 And @vFechaVencimiento < @Fecha1
		Begin
			Set @vFecha2 = @vSaldo
		End
		
		--Fecha3
		If @vFechaVencimiento >= @Fecha3 And @vFechaVencimiento < @Fecha2
		Begin
			Set @vFecha3 = @vSaldo
		End
		
		--Fecha4
		If @vFechaVencimiento >= @Fecha4 And @vFechaVencimiento < @Fecha3 
		Begin
			Set @vFecha4 = @vSaldo
		End
		
		--Fecha5
		If @vFechaVencimiento >= @Fecha5 And @vFechaVencimiento < @Fecha4
		Begin
			Set @vFecha5= @vSaldo
		End
		
		--Fecha6
		If @vFechaVencimiento >= @Fecha6 And @vFechaVencimiento < @Fecha5 
		Begin
			Set @vFecha6= @vSaldo
		End
						
		--Fecha7
		if @vFechaVencimiento >= @Fecha7 And @vFechaVencimiento < @Fecha6 
		Begin
			Set @vFecha7= @vSaldo
		End	
		
		--Fecha8
		if @vFechaVencimiento >= @Fecha8 And @vFechaVencimiento < @Fecha7 
		Begin
			Set @vFecha8= @vSaldo
		End	
		
		--Fecha9
		if @vFechaVencimiento >= @Fecha9 And @vFechaVencimiento < @Fecha8 
		Begin
			Set @vFecha9= @vSaldo
		End	
		
		--Fecha10
		if @vFechaVencimiento < @Fecha9 
		Begin
			Set @vFecha10= @vSaldo
		End	
										
		--Set @vTotal = @vSaldo 
		Set @vTotal = @vFecha1 + @vFecha2 + @vFecha3 + @vFecha4 + @vFecha5 +
			              @vFecha6 + @vFecha7 + @vFecha8 + @vFecha9 + @vFecha10 
		
		--Actualizar si ya existe
		If  Exists(Select * From #TablaTemporal Where Referencia=@vReferencia) Begin				
		--Update #TablaTemporal Set Total=Total+@vSaldo,
			Update #TablaTemporal Set Total=Total+@vFecha1 + @vFecha2 + @vFecha3 + @vFecha4 + @vFecha5 +
			              @vFecha6 + @vFecha7 + @vFecha8 + @vFecha9 + @vFecha10,
									  Fecha1=Fecha1+@vFecha1,
									  Fecha2=Fecha2+@vFecha2,
									  Fecha3=Fecha3+@vFecha3,
									  Fecha4=Fecha4+@vFecha4,
									  Fecha5=Fecha5+@vFecha5,
									  Fecha6=Fecha6+@vFecha6,
									  Fecha7=Fecha7+@vFecha7,
									  Fecha8=Fecha8+@vFecha8,
									  Fecha9=Fecha9+@vFecha9,
									  Fecha10=Fecha10+@vFecha10
									Where Referencia=@vReferencia 
									and IDMoneda = @vIDMoneda
		End
		
		
		--Insertar si aún no existe
		If Not Exists(Select * From #TablaTemporal Where Referencia=@vReferencia) Begin			
			Insert into #TablaTemporal(ID,IDTransaccion,Cliente,Referencia,IDTipoComprobante,IDVendedor,Vendedor,IDCobrador,Cobrador,IDTipoCliente,IDEstado,IDSucursal,Sucursal,IDMoneda,IDZonaVenta,FechaVencimiento,Credito,Comprobante,Total,Fecha1,Fecha2,Fecha3,Fecha4,Fecha5,Fecha6,Fecha7,Fecha8,Fecha9,Fecha10, IDTipoProducto,Judicial,IDSucursalFiltro, IDCliente)
			Values (@vID,@vIDTransaccion,@vRazonSocial,@vReferencia,@vIDTipoComprobante,@vIDVendedor,@vVendedor,@vIDCobrador,@vCobrador,@vIDTipoCliente,@vIDEstadoCliente,@vIDSucursal,@vSucursal,@vIDMoneda,@vIDZonaVenta,@vFechaVencimiento,@vCredito,@vComprobante,@vTotal,@vFecha1, @vFecha2, @vFecha3, @vFecha4, @vFecha5, @vFecha6, @vFecha7, @vFecha8, @vFecha9,@vFecha10, @vIDTipoProducto,@vJudicial,@vIDSucursalFiltro, @vIDCliente) 	
		End
				
		Fetch Next From db_cursor Into @vIDTransaccion,@vRazonSocial,@vReferencia,@vIDTipoComprobante,@vIDVendedor,@vVendedor,@vIDCobrador,@vCobrador,@vIDTipoCliente,@vIDEstadoCliente,@vIDSucursal,@vSucursal,@vIDMoneda,@vIDZonaVenta,@vSaldo,@vFechaVencimiento,@vCredito,@vComprobante, @vIDTipoProducto, @vJudicial,@vIDSucursalFiltro, @vIDCliente
						
						
	End

	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor
	End

	 IF @Contado = @Credito Begin
	Declare db_cursor cursor for
	Select 
	V.IDTransaccion, 
	C.RazonSocial,
	C.Referencia,
	V.IDTipoComprobante,
	V.IDVendedor,
	V.Vendedor,
	C.IDCobrador,
	V.Cobrador,
	C.IDTipoCliente,
	C.IDEstado,
	V.IDSucursal,
	V.Sucursal,
	V.IDMoneda,
	C.IDZonaVenta,
	V.Saldo,
	V.FechaVencimiento,
	V.Credito,
	V.Comprobante	,
	'IDTipoProducto'=Isnull((Select top(1) IDTipoProducto from vdetalleVenta where IDTransaccion = V.IDTransaccion),0),
	C.Judicial,
	V.IDSucursalFiltro,
	V.IDCliente
	From Cliente C
	Join VVenta V On C.ID=V.IDCliente
	Where V.Saldo >= 1 And V.Anulado='False' --And V.Contado=@Contado And V.Credito=@Credito
	Open db_cursor   
	Fetch Next From db_cursor Into @vIDTransaccion,@vRazonSocial,@vReferencia,@vIDTipoComprobante,@vIDVendedor,@vVendedor,@vIDCobrador,@vCobrador,@vIDTipoCliente,@vIDEstadoCliente,@vIDSucursal,@vSucursal,@vIDMoneda,@vIDZonaVenta,@vSaldo,@vFechaVencimiento,@vCredito,@vComprobante, @vIDTipoProducto, @vJudicial,@vIDSucursalFiltro, @vIDCliente
	While @@FETCH_STATUS = 0 Begin  

		set @vFecha1 = 0
		set @vFecha2 = 0
		set @vFecha3 = 0
		set @vFecha4 = 0
		set @vFecha5 = 0
		set @vFecha6 = 0
		set @vFecha7 = 0
		set @vFecha8 = 0
		set @vFecha9 = 0
		set @vFecha10 = 0
		set @vTotal = 0	
							
		--Fecha1
		If @vFechaVencimiento >= @Fecha1  And @vFechaVencimiento <= @FechaReferencia 
		Begin
			Set @vFecha1 = @vSaldo
		End
				
		--Fecha2
		If @vFechaVencimiento >= @Fecha2 And @vFechaVencimiento < @Fecha1
		Begin
			Set @vFecha2 = @vSaldo
		End
		
		--Fecha3
		If @vFechaVencimiento >= @Fecha3 And @vFechaVencimiento < @Fecha2
		Begin
			Set @vFecha3 = @vSaldo
		End
		
		--Fecha4
		If @vFechaVencimiento >= @Fecha4 And @vFechaVencimiento < @Fecha3 
		Begin
			Set @vFecha4 = @vSaldo
		End
		
		--Fecha5
		If @vFechaVencimiento >= @Fecha5 And @vFechaVencimiento < @Fecha4
		Begin
			Set @vFecha5= @vSaldo
		End
		
		--Fecha6
		If @vFechaVencimiento >= @Fecha6 And @vFechaVencimiento < @Fecha5 
		Begin
			Set @vFecha6= @vSaldo
		End
						
		--Fecha7
		if @vFechaVencimiento >= @Fecha7 And @vFechaVencimiento < @Fecha6 
		Begin
			Set @vFecha7= @vSaldo
		End	
		
		--Fecha8
		if @vFechaVencimiento >= @Fecha8 And @vFechaVencimiento < @Fecha7 
		Begin
			Set @vFecha8= @vSaldo
		End	
		
		--Fecha9
		if @vFechaVencimiento >= @Fecha9 And @vFechaVencimiento < @Fecha8 
		Begin
			Set @vFecha9= @vSaldo
		End	
		
		--Fecha10
		if @vFechaVencimiento < @Fecha9 
		Begin
			Set @vFecha10= @vSaldo
		End	
										
		--Set @vTotal = @vSaldo 
		Set @vTotal = @vFecha1 + @vFecha2 + @vFecha3 + @vFecha4 + @vFecha5 +
			              @vFecha6 + @vFecha7 + @vFecha8 + @vFecha9 + @vFecha10 
		
		--Actualizar si ya existe
		If  Exists(Select * From #TablaTemporal Where Referencia=@vReferencia) Begin				
		--Update #TablaTemporal Set Total=Total+@vSaldo,
			Update #TablaTemporal Set Total=Total+@vFecha1 + @vFecha2 + @vFecha3 + @vFecha4 + @vFecha5 +
			              @vFecha6 + @vFecha7 + @vFecha8 + @vFecha9 + @vFecha10,
									  Fecha1=Fecha1+@vFecha1,
									  Fecha2=Fecha2+@vFecha2,
									  Fecha3=Fecha3+@vFecha3,
									  Fecha4=Fecha4+@vFecha4,
									  Fecha5=Fecha5+@vFecha5,
									  Fecha6=Fecha6+@vFecha6,
									  Fecha7=Fecha7+@vFecha7,
									  Fecha8=Fecha8+@vFecha8,
									  Fecha9=Fecha9+@vFecha9,
									  Fecha10=Fecha10+@vFecha10
									Where Referencia=@vReferencia 
									and IDMoneda = @vIDMoneda
		End
		
		
		--Insertar si aún no existe
		If Not Exists(Select * From #TablaTemporal Where Referencia=@vReferencia) Begin			
			Insert into #TablaTemporal(ID,IDTransaccion,Cliente,Referencia,IDTipoComprobante,IDVendedor,Vendedor,IDCobrador,Cobrador,IDTipoCliente,IDEstado,IDSucursal,Sucursal,IDMoneda,IDZonaVenta,FechaVencimiento,Credito,Comprobante,Total,Fecha1,Fecha2,Fecha3,Fecha4,Fecha5,Fecha6,Fecha7,Fecha8,Fecha9,Fecha10, IDTipoProducto, Judicial,IDSucursalFiltro, IDCliente)
			Values (@vID,@vIDTransaccion,@vRazonSocial,@vReferencia,@vIDTipoComprobante,@vIDVendedor,@vVendedor,@vIDCobrador,@vCobrador,@vIDTipoCliente,@vIDEstadoCliente,@vIDSucursal,@vSucursal,@vIDMoneda,@vIDZonaVenta,@vFechaVencimiento,@vCredito,@vComprobante,@vTotal,@vFecha1, @vFecha2, @vFecha3, @vFecha4, @vFecha5, @vFecha6, @vFecha7, @vFecha8, @vFecha9,@vFecha10, @vIDTipoProducto,@vJudicial,@vIDSucursalFiltro, @vIDCliente) 	
		End
				
		Fetch Next From db_cursor Into @vIDTransaccion,@vRazonSocial,@vReferencia,@vIDTipoComprobante,@vIDVendedor,@vVendedor,@vIDCobrador,@vCobrador,@vIDTipoCliente,@vIDEstadoCliente,@vIDSucursal,@vSucursal,@vIDMoneda,@vIDZonaVenta,@vSaldo,@vFechaVencimiento,@vCredito,@vComprobante, @vIDTipoProducto, @vJudicial,@vIDSucursalFiltro, @vIDCliente
						
						
	End

	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor
	End
End



Select * From #TablaTemporal Where ID=@vID And (Fecha1 > 0 or Fecha2 > 0 or Fecha3 > 0 or Fecha4 > 0 or Fecha5 > 0 or Fecha6 > 0 or  Fecha7 > 0 or  Fecha8 > 0 or Fecha9 > 0  or Fecha10 > 0)
Order By Cliente, IDMoneda
End
	







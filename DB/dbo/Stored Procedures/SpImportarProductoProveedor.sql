﻿CREATE Procedure [dbo].[SpImportarProductoProveedor]

	--Entrada
	@Referencia varchar(100) = NULL,
	@Proveedor varchar(50) = NULL,
	
	@Actualizar bit
	
As

Begin

	Declare @vIDProducto int
	Declare @vIDProveedor int
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	
	Set @vMensaje = 'No se proceso'
	Set @vProcesado = 'False'
	
	--Hayar los valores
	Set @vIDProducto = IsNull((Select ID From Producto Where Referencia = @Referencia),0)
	
	If @vIDProducto = 0 Begin
		Set @vMensaje = 'No se encontro el producto'
		Set @vProcesado = 'False'
		GoTo Salir
	End
	
	Set @vIDProveedor = Case 
							When @Proveedor = '001' Then
								(Select ID From Proveedor Where Referencia='01')
							When @Proveedor = '002' Then
								(Select ID From Proveedor Where Referencia='02')
							When @Proveedor = '30' Then
								(Select ID From Proveedor Where Referencia='800032322')
							When @Proveedor = '100' Then
								(Select ID From Proveedor Where Referencia='800108175')
							Else
								0
						End
	
	If @vIDProveedor = 0 Begin
		Set @vMensaje = 'No se encontro el proveedor!'
		Set @vProcesado = 'False'
		GoTo Salir
	End
	
	If @Actualizar = 'False' Begin
		Goto Salir
	End

	Update Producto set IDProveedor=@vIDProveedor
	Where ID = @vIDProducto
		
	Set @vMensaje = 'Registro actualizado'
	Set @vProcesado = 'True'
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


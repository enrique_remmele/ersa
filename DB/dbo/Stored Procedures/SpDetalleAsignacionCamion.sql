﻿CREATE Procedure [dbo].[SpDetalleAsignacionCamion]

	--Entrada
	@IDTransaccion numeric(18,0),
	@Operacion varchar(20),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin	

	--Validar
	--Transaccion
	If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--Transaccion
	If Not Exists(Select IDTransaccion From AsignacionCamion Where IDTransaccion=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--BLOQUES
	if @Operacion='INS' Begin
		--Si todo es correcto, anulamos con el cursor
		Begin
			Declare @vIDTransaccion decimal(18,2)
			Declare @vID tinyint
			Declare @vIDTransaccionAbastecimiento decimal(18,2)
			Declare @vIDTransaccionPedido decimal(18,2)
			Declare @vIDOperacion tinyint
			Declare @vIDProducto int
			Declare @vCantidad decimal(10,2)
						
			Declare db_cursor cursor for
			Select IDTransaccion,ID,  IDTransaccionAbastecimiento, IDTransaccionPedido, IDOperacion, IDPRoducto, Cantidad
			From DetalleAsignacionCamion where IDTransaccion = @IDTransaccion
			Open db_cursor   
			fetch next from db_cursor into @vIDTransaccion, @vID, @vIDTransaccionAbastecimiento, @vIDTransaccionPedido, @vIDOperacion,@vIDProducto, @vCantidad 

			While @@FETCH_STATUS = 0 Begin  
					
					--Actualizar Orden de pedido de abastecimiento
					IF @vIDTransaccionAbastecimiento > 0 Begin
						Update DetalleOrdenDePedido
						set CantidadAEntregar = Isnull(CantidadAEntregar,Extra) - @vCantidad
						where IDTransaccion = @vIDTransaccionAbastecimiento
						and ID = @vIDOperacion
						and IDProducto = @vIDProducto
					End

					--Actualizar Pedido de clientes	
					IF @vIDTransaccionPedido > 0  Begin
						Update detallePedido
						set CantidadAEntregar = Isnull(CantidadAEntregar,Cantidad) - @vCantidad
						where IDTransaccion = @vIDTransaccionPedido
						and ID = @vIDOperacion
						and IDProducto = @vIDProducto
					End
			
						   
				  
					--Poner en Anulado el registro
					Update DetalleAsignacionCamion Set Anulado='True'
					Where IDTransaccion=@IDTransaccion And IDProducto=@vIDProducto And ID=@vID
					
					--Siguiente
				   fetch next from db_cursor into @vIDTransaccion, @vID, @vIDTransaccionAbastecimiento, @vIDTransaccionPedido, @vIDOperacion,@vIDProducto, @vCantidad 
			End   

			Close db_cursor   
			deallocate db_cursor
		End
			 
		Set @Procesado = 'True'
		return @@rowcount	
	End
	
	if @Operacion='DEL' Begin
		
		--Validar
		--Transaccion
		If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra la transaccion!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--Transaccion
		If Not Exists(Select IDTransaccion From AsignacionCamion Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra la transaccion!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	if @Operacion='ANULAR' Begin
		
		--Verificar que exista
		If Not Exists(Select * From DetalleAsignacionCamion Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El Registro ya esta anulado!'
			Set @Procesado = 'True'
			return @@rowcount
		End


		--Si todo es correcto, anulamos con el cursor
		Begin						
			Declare db_cursor cursor for
			Select IDTransaccion,ID, IDTransaccionAbastecimiento, IDTransaccionPedido, IDOperacion, IDPRoducto, Cantidad
			From DetalleAsignacionCamion where IDTransaccion = @IDTransaccion
			Open db_cursor   
			fetch next from db_cursor into @vIDTransaccion, @vID, @vIDTransaccionAbastecimiento, @vIDTransaccionPedido, @vIDOperacion,@vIDProducto, @vCantidad 

			While @@FETCH_STATUS = 0 Begin  
					
					--Actualizar Orden de pedido de abastecimiento
					IF @vIDTransaccionAbastecimiento > 0 Begin
						Update DetalleOrdenDePedido
						set CantidadAEntregar = CantidadAEntregar + @vCantidad
						where IDTransaccion = @vIDTransaccionAbastecimiento
						and ID = @vIDOperacion
						and IDProducto = @vIDProducto
					End

					--Actualizar Pedido de clientes	
					IF @vIDTransaccionPedido > 0  Begin
						Update detallePedido
						set CantidadAEntregar = CantidadAEntregar + @vCantidad
						where IDTransaccion = @vIDTransaccionPedido
						and ID = @vIDOperacion
						and IDProducto = @vIDProducto
					End
			
						   
				  
					--Poner en Anulado el registro
					Update DetalleAsignacionCamion Set Anulado='True'
					Where IDTransaccion=@IDTransaccion And IDProducto=@vIDProducto And ID=@vID
					
					--Siguiente
				   fetch next from db_cursor into @vIDTransaccion, @vID, @vIDTransaccionAbastecimiento, @vIDTransaccionPedido, @vIDOperacion,@vIDProducto, @vCantidad 
			End   

			Close db_cursor   
			deallocate db_cursor
		End
				
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	return @@rowcount
	
End
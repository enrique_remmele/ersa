﻿CREATE Procedure [dbo].[SpCamionProveedor]

	--Entrada
	@ID int = NULL,
	@Descripcion varchar(50) = NULL,
	@Patente varchar(10) = NULL,
	@Chasis varchar(20) = NULL,
	@Capacidad smallint = NULL,
	@Estado bit = NULL,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From CamionProveedor Where Patente=@Patente) begin
			set @Mensaje = 'La Patente ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDCamion int
		set @vIDCamion = (Select IsNull((Max(ID)+1), 1) From CamionProveedor)

		--Insertamos
		Insert Into CamionProveedor(ID, Descripcion, Patente, Chasis, Capacidad, Estado)
		Values(@vIDCamion, @Descripcion, @Patente, @Chasis, @Capacidad, @Estado)	
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CAMIONPROVEEDOR', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
					
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From CamionProveedor Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From CamionProveedor Where Patente=@Patente And ID!=@ID) begin
			set @Mensaje = 'La Patente ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update CamionProveedor Set	Descripcion=@Descripcion, 
							Patente=@Patente,
							Chasis = @Chasis,
							Capacidad = @Capacidad,
							Estado=@Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CAMIONPROVEEDOR', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
			
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From CamionPRoveedor Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una transaccion
		--if exists(Select * From TicketBascula Where IDCamion=@ID) begin
		--	set @Mensaje = 'El registro tiene transacciones asociadas! No se puede eliminar.'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--end
		
		
		--Eliminamos
		Delete From CamionProveedor 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CAMIONPROVEEDOR', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
			
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


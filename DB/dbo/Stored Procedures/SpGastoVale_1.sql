﻿CREATE Procedure [dbo].[SpGastoVale]

	--Entrada
	@IDSucursal tinyint,
	@IDGrupo int,	
	@IDTransaccionGasto numeric (18,0),
	@IDTransaccionVale numeric (18,0),
	@Importe money,
	@Saldo money,
		
	--Operacion
	@Operacion varchar(50),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		declare @vID int --tinyint
		Declare @vCancelado bit = 'False'
			
		--Obtenemos el nuevo ID
		set @vID = (Select IsNull((Max(ID)+1), 1) From DetalleFondoFijo )
		set @Saldo = @Saldo - @Importe
					
		--Insertamos en Gasto Vale
		Insert Into GastoVale (IDTransaccionGasto, IDTransaccionVale, Importe)
		Values(@IDTransaccionGasto,@IDTransaccionVale,@Importe)		
		
		--Insertamos en Detalle Fondo Fijo
		Insert Into DetalleFondoFijo (IDSucursal,IDGrupo,ID,IDTransaccionVale,IDTransaccionGasto,Importe,Cancelado,IDTransaccionRendicionFondoFijo)
		values (@IDSucursal,@IDGrupo,@vID,@IDTransaccionVale,@IDTransaccionGasto,@Importe,'False', NULL )
		
		If @Saldo < 1 Begin
			Set @vCancelado = 'True'
		End

		Update Vale Set Saldo=@Saldo,
						Cancelado=@vCancelado
		Where IDTransaccion=@IDTransaccionVale
			
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--Actualizar Saldo del vale
	If @Operacion='DEL' begin
		
		set @Saldo = @Saldo + @Importe
				
		Update Vale Set Saldo=@Saldo,
						Cancelado = 'False' 
		Where IDTransaccion=@IDTransaccionVale
			
		--Eliminamos de Gasto Vale		
		delete from GastoVale 
		where IDTransaccionVale = @IDTransaccionVale 
		and IDTransaccionGasto = @IDTransaccionGasto	
		
		--Insertamos en Detalle Fondo Fijo
		delete from DetalleFondoFijo 
		where IDTransaccionVale = @IDTransaccionVale 
		and IDTransaccionGasto = @IDTransaccionGasto

		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
End


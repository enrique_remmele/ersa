﻿CREATE Procedure [dbo].[SpPorcentajeExcepcion]

	--Entrada
	@IDUsuarioExcepcion int,
	@Porcentaje decimal(18,2),
	@Importe money,
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if not exists(Select * From Usuario Where ID=@IDUsuarioExcepcion) begin
			set @Mensaje = 'El usuario no existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		--Si es que la configuracion ya existe
		if exists(Select * From PorcentajeExcepcion Where IDUsuario=@IDUsuarioExcepcion) begin
			set @Mensaje = 'La configuracion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Insertamos
		Insert Into PorcentajeExcepcion(IDUsuario, Porcentaje, Importe, Estado)
		Values(@IDUsuarioExcepcion, @Porcentaje, @Importe, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PORCENTAJEEXEPCION', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From PorcentajeExcepcion Where IDUsuario=@IDUsuarioExcepcion) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update PorcentajeExcepcion Set Porcentaje=@Porcentaje, Importe = @Importe, Estado =@Estado
										Where IDUsuario=@IDUsuarioExcepcion
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PORCENTAJEEXEPCION', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From PorcentajeExcepcion Where IDUsuario=@IDUsuarioExcepcion) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		Delete From PorcentajeExcepcion 
		Where IDUsuario=@IDUsuarioExcepcion 
		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PORCENTAJEEXEPCION', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


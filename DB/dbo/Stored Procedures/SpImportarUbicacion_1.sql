﻿CREATE Procedure [dbo].[SpImportarUbicacion]

	--Entrada
	--Identificadores
	@Referencia varchar(50) = NULL,
	@Ubicacion varchar(150),
	@Deposito varchar(100),
	
	--Opciones
	@Actualizar Bit = 'False'
	
As

Begin
	Declare @vID int
	Declare @vOperacion varchar(10) 
	Declare @Mensaje varchar(200)
	Declare @Procesado bit
	
	Declare @vIdProducto as integer
	declare @vIDZonaDeposito as integer	
	Declare @vIDSucursal as integer
	Declare @vIDDeposito as integer
	Begin
		set @vIdProducto = ISNULL((select ID from Producto where Referencia = @Referencia),0)
		set @vIDZonaDeposito = ISNULL((select top (1)ID from ZonaDeposito where Descripcion = @Ubicacion),0)
		set @vIDSucursal = ISNULL((select top(1) IDSucursal from ZonaDeposito where Descripcion = @Ubicacion),0)
		set @vIDDeposito = ISNULL((select top(1) Id  from Deposito where Descripcion = @Deposito),0)
	End

	Set	@vID = 0
	Set @vOperacion = 'INS'


	IF @vIdProducto > 0 and @vIDZonaDeposito > 0 Begin
		If not Exists(Select * From ProductoZona Where IDProducto = @vIdProducto and IDZonaDeposito = @vIDZonaDeposito and IDSucursal = 4)  Begin
			Insert into ProductoZona values (4,@vIDDeposito,@vIDZonaDeposito,@vIdProducto)
			set @Mensaje = 'Registro guardado'
			set @Procesado = 'True'
		End 		
	End 
	Select 'Mensaje'=@Mensaje + ' ' + @vOperacion, 'Procesado'=@Procesado, 'Cantidad'=@@ROWCOUNT
End



﻿CREATE Procedure [dbo].[SpListaPrecioInsUpd]

	--Entrada
	@Descripcion varchar(100),
	@IDSucursal int
	
As

Begin

	Declare @vID int
	
	If @Descripcion = '' Begin
		Set @Descripcion = 'VARIOS Y OTROS'
	End
	
	--Si existe
	If Exists(Select * From ListaPrecio Where Descripcion = @Descripcion And IDSucursal=@IDSucursal) Begin
		Set @vID = (Select Top(1) ID From ListaPrecio Where Descripcion = @Descripcion And IDSucursal=@IDSucursal)
		
	End Else Begin
		Set @vID = (Select IsNull(Max(ID)+1,1) From ListaPrecio)
		Insert Into ListaPrecio(ID, IDSucursal, Descripcion, Estado)
		Values(@vID, @IDSucursal, @Descripcion, 'True')
	End
	
	return @vID
	
End


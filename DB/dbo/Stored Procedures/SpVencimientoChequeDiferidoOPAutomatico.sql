﻿
CREATE Procedure [dbo].[SpVencimientoChequeDiferidoOPAutomatico]

	--Entrada OP
	@IDTransaccionOP numeric(18,0),
	@Fecha date,
	
	--Operacion
	@Operacion varchar(50),
	--Transaccion
	@IDOperacion tinyint = 60,
	@IDUsuario smallint = 1,
	@IDSucursal tinyint = 1,
	@IDDeposito tinyint = 1,
	@IDTerminal int = 1,
	
	--Salida
	@Mensaje varchar(200) = '' output ,
	@Procesado bit = 'False' output ,
	@IDTransaccionSalida numeric(18,0) = 0 output
	
As

Begin
	
	If @Operacion = 'INS' Begin
	
		--Si NO existe el registro
		If Not Exists(Select * From OrdenPago Where IDTransaccion=@IDTransaccionOP and Anulado = 0) Begin
			set @Mensaje = 'El registro no existe!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Si NO existe el registro
		If Not Exists(Select * From vOrdenPago Where IDTransaccion=@IDTransaccionOP and Diferido <> 0) Begin
			set @Mensaje = 'El OP no es con cheque diferido!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Si NO existe el registro
		If Not Exists(Select * From OrdenPago Where IDTransaccion=@IDTransaccionOP and IDTransaccion in (select idtransaccionOP from EntregaChequeOP where Anulado = 0)) Begin
			set @Mensaje = 'El cheque de la OP aun no fue entregado!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End


		if exists (select * from VencimientoChequeEmitido where IDTransaccionOP = @IDTransaccionOP) begin
			set @Mensaje = 'El cheque de la OP ya tiene vencimiento registrado!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		end

		--Insertar Transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
	
		--Insertar en Orden de Pago
		Insert Into VencimientoChequeEmitido(IDTransaccion, IDTransaccionOP, Fecha, Anulado, Procesado)
		Values(@IDTransaccionSalida, @IDTransaccionOP, @Fecha, 'False', 'True')

		Insert Into VencimientoChequeEmitidoAutomatico(IDTransaccion, IDTransaccionOP)
		Values(@IDTransaccionSalida, @IDTransaccionOP)
		
		exec SpAsientoOrdenPagoVencimientoCheque @IDTransaccionSalida

		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount	
	
	End

	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
	
	
End

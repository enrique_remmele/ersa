﻿CREATE Procedure [dbo].[SpImpresionCampo]

	--Entrada
	@IDOperacion tinyint,
	@IDTipoComprobante tinyint,
	@IDSeccion tinyint,
	@ID tinyint,
	@Descripcion varchar(50),
	@Campo varchar(50),
	@Columna tinyint,
	@TipoCampo varchar(50),
	@IDFormato tinyint = NULL,
	@IDTipo tinyint = NULL,
	@Color varchar(50) = '0,0,0',
	@PuedeCrecer bit = 'False',
	@MantenerJunto bit = 'False',
	@Mostrar bit = 'True',
	@CoordenadaXY varchar(50) = '0,0',
	@TamañoAnchoAlto varchar(50) = '10,10',
	@MargenIzquierdaDerecha varchar(50) = '0,0',
	@Operacion varchar(10),
	
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
		
	--INSERTAR
	if @Operacion='INS' begin
	
		
		--Insertamos
		Insert Into ImpresionCampo(IDOperacion, IDTipoComprobante, IDSeccion, ID, Descripcion, Campo, Columna, TipoCampo, IDFormato, IDTipo, Color, PuedeCrecer, MantenerJunto, Mostrar, CoordenadaXY, TamañoAnchoAlto, MargenIzquierdaDerecha)
		Values(@IDOperacion, @IDTipoComprobante, @IDSeccion, @ID, @Descripcion, @Campo, @Columna, @TipoCampo, @IDFormato, @IDTipo, @Color, @PuedeCrecer, @MantenerJunto, @Mostrar, @CoordenadaXY, @TamañoAnchoAlto, @MargenIzquierdaDerecha)		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
	
		--Si no existe el registro
		if Not Exists(Select * From ImpresionCampo Where IDTipoComprobante=@IDTipoComprobante And IDOperacion=@IDOperacion And IDSeccion=@IDSeccion And ID=@ID) Begin
			set @Mensaje = 'El registro no existe!'
			set @Procesado = 'false'
			return @@rowcount
		End		
	
		--Actualizamos
		Update ImpresionCampo Set Descripcion=@Descripcion,
									Campo=@Campo,
									Columna=@Columna,
									TipoCampo=@TipoCampo,
									IDFormato=@IDFormato,
									IDTipo=@IDTipo,
									Color=@Color,
									PuedeCrecer=@PuedeCrecer,
									MantenerJunto=@MantenerJunto,
									Mostrar=@Mostrar,
									CoordenadaXY=@CoordenadaXY,
									TamañoAnchoAlto=@TamañoAnchoAlto,
									MargenIzquierdaDerecha=@MargenIzquierdaDerecha
		
		Where IDTipoComprobante=@IDTipoComprobante And IDOperacion=@IDOperacion And IDSeccion=@IDSeccion And ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	--ELIMINAR
	if @Operacion='DEL' begin
	
		--Si no existe el registro
		if Not Exists(Select * From ImpresionCampo Where IDTipoComprobante=@IDTipoComprobante And IDOperacion=@IDOperacion And IDSeccion=@IDSeccion And ID=@ID) Begin
			set @Mensaje = 'El registro no existe!'
			set @Procesado = 'false'
			return @@rowcount
		End		
	
		--Eliminamos los campos
		Delete From ImpresionCampo Where ID=@ID And IDSeccion=@IDSeccion And IDTipoComprobante=@IDTipoComprobante And IDOperacion=@IDOperacion
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	set @Mensaje = 'No se proceso!'
	set @Procesado = 'False'
	return @@rowcount
		
End




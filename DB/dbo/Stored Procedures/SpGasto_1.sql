﻿CREATE Procedure [dbo].[SpGasto]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Numero int = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante varchar(50) = NULL,
	@IDProveedor int = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDGrupo tinyint = NULL,
	@Fecha date = NULL,
	@Credito bit = NULL,
	@FechaVencimiento date = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money = NULL,
	@Observacion varchar(200) = NULL,
	@Total money = NULL,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@Directo bit = NULL,
	@Efectivo bit = 'False',
	@Cheque bit = 'False',
	@CajaChica bit =NULL,
	@RRHH bit = Null,
	@Cuota int = 0,
	@NroTimbrado varchar(50)=NULL,
	@FechaVencimientoTimbrado date=NULL,
	@IncluirLibro bit='True',
	@IDDepartamentoEmpresa tinyint = null,
	
	--Distribucion
	@IDDepositoOperacion int = NULL,
	@IDChofer int = NULL,
	@IDCamion int = NULL,
	
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	@IDTransaccionCaja numeric(18,0) = 0,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
	
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from Gasto where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--Variable
	Declare @RetencionIVA money
	Declare @PorcentajeRetencion tinyint

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		Declare @vNumero int
		Set @vNumero = ISNULL((Select MAX(Numero)+1 From Gasto Where IDSucursal=@IDSucursalOperacion),1)
		if isnull(@Cotizacion,0)<1 begin
			set @Cotizacion = 1
		end
		----Validar
		--Caja Cerrada
		If (Select Habilitado From Caja Where IDTransaccion=@IDTransaccionCaja) = 'False' Begin
			set @Mensaje = 'La caja al que se hace referencia esta cerrada! Seleccione otra caja.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Fecha de Emision > Fecha de vencimiento del timbrado
		if @Fecha > @FechaVencimientoTimbrado Begin
			set @Mensaje = 'La Fecha de Emision no puede ser mayor a la Fecha de Vencimiento del Timbrado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		
		--Comprobante
		if Exists(Select * From Gasto Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante AND NroTimbrado = @NroTimbrado and IDProveedor = @IDProveedor) Begin
			set @Mensaje = 'El sistema encontro el mismo tipo y numero de comprobante del mismo timbrado! Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Comprobante
		if Exists(Select * From Compra Where NroComprobante=@NroComprobante and NroTimbrado = @NroTimbrado) Begin
			set @Mensaje = 'Existe compra con el mismo numero de comprobante y timbrado! Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Fecha de la compra que no sea mayor a hoy
		If @IDProveedor not in (257,349,158)  begin
			If @Fecha > getdate() Begin 
				set @Mensaje = 'La fecha del comprobante no debe ser mayor a hoy!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
		End
	
	-- IdGrupo exista en Fondo Fijo con la Sucursal
	If @CajaChica = 'True' Begin
	if not Exists(Select * From FondoFijo Where IdGrupo=@IDGrupo and IDSucursal =@IdSucursalOperacion ) Begin
			set @Mensaje = 'El Grupo no pertenece a la Sucursal'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
	end

		--Credito
		If @Credito='True' Begin
			
			--Fecha
			If DATEDIFF(dd, @Fecha, @FechaVencimiento) < 0 Begin
				set @Mensaje = 'La fecha de de vencimiento no puede ser menor al del comprobante!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
						
		End
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		------Insertar
		Declare @vSaldo money
		Declare @vCancelado bit
		
		If @Credito = 'True' Begin
			Set @vSaldo = @Total
			Set @vCancelado = 'False'
		End Else Begin
			Set @vSaldo = @Total
			Set @vCancelado = 'False'
		End
		
		--Retencion
		Declare @vDecimal bit = 'False'
		Set @vDecimal = IsNull((Select Top(1) [Decimales] From Moneda Where ID=@IDMoneda), 'True')
		set @PorcentajeRetencion = (Select Top(1) CompraPorcentajeRetencion  from Configuraciones )
		set @RetencionIVA  = (@TotalImpuesto  * @PorcentajeRetencion) / 100
		
		--Redondear si la moneda no tiene decimales!!!!!!
		If @vDecimal = 'False' Begin
			set @RetencionIVA = ROUND(@RetencionIVA, 0)
		End Else Begin
			set @RetencionIVA = ROUND(@RetencionIVA, 2)
		End
		
		--Insertar el Registro de Gasto		
		Insert Into Gasto(IDTransaccion, Numero, IDTipoComprobante, NroComprobante, IDProveedor, IDSucursal, IDGrupo, Fecha, Credito, FechaVencimiento, IDMoneda, Cotizacion, Observacion, Total, TotalImpuesto, TotalDiscriminado, Saldo, Cancelado, Directo, Efectivo, Cheque, CajaChica, Cuota, RetencionIVA,NroTimbrado,FechaVencimientoTimbrado, IncluirLibro, IDDeposito, IDCamion, IDChofer, RRHH,IDDepartamentoEmpresa)
		Values(@IDTransaccionSalida, @vNumero, @IDTipoComprobante, @NroComprobante, @IDProveedor, @IDSucursalOperacion, @IDGrupo,  @Fecha, @Credito, @FechaVencimiento, @IDMoneda, @Cotizacion, @Observacion, @Total, @TotalImpuesto, @TotalDiscriminado,  @vSaldo, @vCancelado, @Directo, @Efectivo, @Cheque, @CajaChica, @Cuota, @RetencionIVA,@NroTimbrado,@FechaVencimientoTimbrado, @IncluirLibro, @IDDepositoOperacion, @IDCamion, @IDChofer, @RRHH,@IDDepartamentoEmpresa)
		
		--select * from agasto
		insert into aGasto (IDTransaccion,Numero,IDTipoComprobante,NroComprobante,IDProveedor,IDSucursal,IDGrupo,Fecha,Credito,FechaVencimiento,IDMoneda,Cotizacion,Observacion,IDTransaccionRemision,Total,TotalImpuesto,TotalDiscriminado,Saldo,Cancelado,Directo,Efectivo,Cheque,CajaChica,Cuota,RetencionIVA,RetencionRenta,NroTimbrado,FechaVencimientoTimbrado,IncluirLibro,IDDeposito,IDChofer,IDCamion,Pagar,ImporteAPagar,IDCuentaBancaria,ObservacionPago,RRHH,IDDepartamentoEmpresa,FechaModificacion,IDUsuarioModificacion,IDTerminalModificacion,Operacion)
		Select IDTransaccion,Numero,IDTipoComprobante,NroComprobante,IDProveedor,IDSucursal,IDGrupo,Fecha,Credito,FechaVencimiento,IDMoneda,Cotizacion,Observacion,IDTransaccionRemision,Total,TotalImpuesto,TotalDiscriminado,Saldo,Cancelado,Directo,Efectivo,Cheque,CajaChica,Cuota,RetencionIVA,RetencionRenta,NroTimbrado,FechaVencimientoTimbrado,IncluirLibro,IDDeposito,IDChofer,IDCamion,Pagar,ImporteAPagar,IDCuentaBancaria,ObservacionPago,RRHH,IDDepartamentoEmpresa,GetDate(),@IDUsuario,@IDTerminal,@Operacion from Gasto where IDTransaccion = @IDTransaccionSalida

		--Actualizamo la caja
		Update Transaccion Set IDTransaccionCaja=@IDTransaccionCaja
		Where ID=@IDTransaccionSalida
		
		If @CajaChica = 'False' Begin
		
			Set @vNumero = ISNULL((Select MAX(Numero)+1 From GastoTipoComprobante Where IDSucursal=@IDSucursalOperacion),1)
			
			Insert Into GastoTipoComprobante(IDTransaccion,Numero,IDSucursal) 
			Values(@IDTransaccionSalida, @vNumero, @IDSucursalOperacion)
			
		End 
		
		If @CajaChica = 'True' Begin
		
			Set @vNumero = ISNULL((Select MAX(Numero)+1 From GastoFondoFijo Where IDSucursal=@IDSucursalOperacion),1)
			
			Insert Into GastoFondoFijo(IDTransaccion,Numero,IDSucursal) 
			Values(@IDTransaccionSalida, @vNumero,@IDSucursalOperacion)
			
		End 
		
		--Actualizar el proveedor   
		Update Proveedor Set FechaUltimaCompra=@Fecha
		Where ID=@IDProveedor
								
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Gasto  Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--IDTransaccion
		--If Exists(Select * From GastoProducto  Where IDTransaccionGasto=@IDTransaccion) Begin
		   If Exists(Select * From MacheoFacturaTicket  Where IDTransaccionGasto=@IDTransaccion) Begin
			 set @Mensaje = 'El Gasto esta asociado en un Macheo de Materia Prima.'
			 set @Procesado = 'False'
			 return @@rowcount
		   End			
		--End

		--Verificar que el documento no tenga otros registros asociados
		If Exists(Select * From OrdenPagoEgreso Where IDTransaccionEgreso=@IDTransaccion) Begin
			Declare @vIDTransaccionOP numeric(18,0)
			Declare @vNumeroOperacionOP int
			Declare @vSucursalOP varchar(10)

			Select Top(1) @vIDTransaccionOP=OP.IDTransaccion,
					@vNumeroOperacionOP=OP.Numero,
					@vSucursalOP=OP.Suc
			From VOrdenPago OP
			Join OrdenPagoEgreso OPE On OP.IDTransaccion=OPE.IDTransaccionOrdenPago
			Where OPE.IDTransaccionEgreso=@IDTransaccion

			set @Mensaje = 'El registro ya tiene un pago realizado! ' + @vSucursalOP + ': ' + Convert(varchar(50), @vNumeroOperacionOP)
			set @Procesado = 'False'
			return @@rowcount

		End
		
		--Actualizar el proveedor
		Declare @vIDProveedor int
		Declare @vUltimaCompra date
		
		Set @vIDProveedor = (Select IDProveedor From Gasto Where IDTransaccion=@IDTransaccion)
		
		--Eliminamos el DetalleImpuesto
		Delete DetalleImpuesto Where IDTransaccion = @IDTransaccion
		
		--Eliminamos DetalleGastoDebitoCredito
		Exec SpDetalleGastoDebitoCredito
		 @IDTransaccionGasto=@IDTransaccion, 
		 @Operacion = 'DEL',
		 @Mensaje=@Mensaje OUTPUT, 
		 @Procesado=@Procesado OUTPUT
		 
		 --Eliminamos DetalleFondo Fijo
		Delete DetalleFondoFijo Where IDTransaccionGasto=@IDTransaccion And Cancelado='False'
		 
		 --Eliminamos Gasto vale
		Delete GastoVale Where IDTransaccionGasto=@IDTransaccion 

		 --Eliminamos Detalle de Logistica
		Delete GastoLogistica Where IDTransaccion=@IDTransaccion 



		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from Gasto where IDTransaccion = @IDTransaccion)
		set @IDProveedor = (select IDProveedor from Gasto where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from Gasto where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from Gasto where IDTransaccion = @IDTransaccion)
		declare @Comprobante as varchar(16)= (select Comprobante from VGasto where IDTransaccion = @IDTransaccion)
		declare @VTipoComprobante varchar(16) = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from Gasto where IDTransaccion = @IDTransaccion)
		declare @Condicion varchar(16) = (select (Case when Credito = 'True' then 'CREDITO' else 'CONTADO' end) from Gasto where IDTransaccion = @IDTransaccion)

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,0,@IDProveedor,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ELI',@VTipoComprobante, @IDSucursal)
		
			--select * from agasto
		insert into aGasto (IDTransaccion,Numero,IDTipoComprobante,NroComprobante,IDProveedor,IDSucursal,IDGrupo,Fecha,Credito,FechaVencimiento,IDMoneda,Cotizacion,Observacion,IDTransaccionRemision,Total,TotalImpuesto,TotalDiscriminado,Saldo,Cancelado,Directo,Efectivo,Cheque,CajaChica,Cuota,RetencionIVA,RetencionRenta,NroTimbrado,FechaVencimientoTimbrado,IncluirLibro,IDDeposito,IDChofer,IDCamion,Pagar,ImporteAPagar,IDCuentaBancaria,ObservacionPago,RRHH,IDDepartamentoEmpresa,FechaModificacion,IDUsuarioModificacion,IDTerminalModificacion,Operacion)
		Select IDTransaccion,Numero,IDTipoComprobante,NroComprobante,IDProveedor,IDSucursal,IDGrupo,Fecha,Credito,FechaVencimiento,IDMoneda,Cotizacion,Observacion,IDTransaccionRemision,Total,TotalImpuesto,TotalDiscriminado,Saldo,Cancelado,Directo,Efectivo,Cheque,CajaChica,Cuota,RetencionIVA,RetencionRenta,NroTimbrado,FechaVencimientoTimbrado,IncluirLibro,IDDeposito,IDChofer,IDCamion,Pagar,ImporteAPagar,IDCuentaBancaria,ObservacionPago,RRHH,IDDepartamentoEmpresa,GetDate(),@IDUsuario,@IDTerminal,@Operacion from Gasto where IDTransaccion = @IDTransaccionSalida

		--Eliminamos el registro
		Delete Gasto  Where IDTransaccion = @IDTransaccion
		
		--Eliminamos el GastoFondoFijo si esta relacionado
		Delete GastoFondoFijo Where IDTransaccion=@IDTransaccion
		
		--Eliminamo el GastoTipoComprobante si esta relacionado
		Delete GastoTipoComprobante Where IDTransaccion=@IDTransaccion
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion
		
		Declare @vUltimoGasto date
		Declare @vUltimoTemp date
		
		Set @vUltimaCompra = (Select Max(Fecha) From Compra Where IDProveedor=@vIDProveedor)
		Set @vUltimoGasto = (Select Max(Fecha) From Gasto Where IDProveedor=@vIDProveedor)
		
		Set @vUltimoTemp = @vUltimaCompra
		
		--Si gasto es mayor
		If  @vUltimoTemp < @vUltimoGasto Begin
			Set @vUltimoTemp = @vUltimoGasto
		End
		
		--Actualizas la ultima fecha de compra
		Update Proveedor Set FechaUltimaCompra=@vUltimoTemp
		Where ID=@vIDProveedor
				
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	If @Operacion = 'UPD' Begin
	
	--Fecha de la compra que no sea mayor a hoy
		If @Fecha > getdate() Begin
				set @Mensaje = 'La fecha del comprobante no debe ser mayor a hoy!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
		End

		--Fecha de Emision > Fecha de vencimiento del timbrado
		if @Fecha > @FechaVencimientoTimbrado Begin
			set @Mensaje = 'La Fecha de Emision no puede ser mayor a la Fecha de Vencimiento del Timbrado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Comprobante
		if Exists(Select * From Gasto Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante AND NroTimbrado = @NroTimbrado and IDTransaccion <> @IDTransaccion) Begin
			set @Mensaje = 'El sistema encontro el mismo tipo y numero de comprobante del mismo timbrado! Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Comprobante
		if Exists(Select * From Compra Where NroComprobante=@NroComprobante and NroTimbrado = @NroTimbrado and IDTransaccion <> @IDTransaccion) Begin
			set @Mensaje = 'Existe compra con el mismo numero de comprobante y timbrado! Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Validar Fecha si es credito
		--Credito
		If @Credito='True' Begin
			
			--Fecha
			If DATEDIFF(dd, @Fecha, @FechaVencimiento) < 0 Begin
				set @Mensaje = 'La fecha de de vencimiento no puede ser menor al del comprobante!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
						
		End

		
		-- Si se selecciono credito
		If @Credito = 'True' Begin
				Set @vSaldo = @Total
				Set @vCancelado = 'False'
			End Else Begin
				Set @vSaldo = @Total
				Set @vCancelado = 'False'
		End
		
		--Validar que la caja no este cerrada
		--If (Select C.Habilitado from Transaccion T Join Caja C on T.IDTransaccionCaja=C.IDTransaccion Where ID=@IDTransaccion)='False' Begin 
		--		set @Mensaje = 'El comprobante no puede ser modificado! Su caja esta cerrada.'
		--		set @Procesado = 'False'
		--		set @IDTransaccionSalida  = 0
		--		return @@rowcount
		--End
				
		--Validar que los Asientos no esten conciliados
		If Exists(Select * from Asiento Where Conciliado='True' And IDTransaccion=@IDTransaccion)Begin 
			set @Mensaje = 'El asiento ya está conciliado!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Validar cuotas
			
		--Si ya se pago la cuota
		If (Select Count(*) From Cuota Where IDTransaccion=@IDTransaccion And Cancelado='True') > 0 Begin
			set @Mensaje = 'El comprobante ya tiene cuotas pagadas! No se puede modificar.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Si hay cuotas pendiente de pago
		If (Select Count(*) From Cuota Where IDTransaccion=@IDTransaccion And Pagar='True') > 0 Begin
			set @Mensaje = 'El comprobante ya tiene cuotas pendiente de pago! No se puede modificar.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		
		--Eliminamos los asientos
		--Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		--Delete Asiento Where IDTransaccion = @IDTransaccion
				
		--Eliminamos el DetalleImpuesto
		--Delete DetalleImpuesto Where IDTransaccion = @IDTransaccion
	
		--Actualizar Datos

		
		-- Validar si es que esta cancelado
		If Exists(Select * from Gasto Where Cancelado='True' And IDTransaccion=@IDTransaccion )Begin
			--Set @Mensaje = 'Registro Cancelado por OP, por lo tanto solo se modificaron: fecha,Tipo de Impuesto, Número de Timbrado y Vigencia de Timbrado!'
			Set @Mensaje = 'Registro Cancelado por OP.'
			Set @Procesado = 'True'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		end Else IF Exists(Select * from Gasto Where Cancelado='False' And IDTransaccion=@IDTransaccion )Begin
			Update Gasto set
							FechaVencimientoTimbrado=@FechaVencimientoTimbrado,
							Directo=@Directo,
							NroTimbrado=@NroTimbrado,
							Fecha = @Fecha,
							IDProveedor = @IDProveedor,
							IDTipoComprobante = @IDTipoComprobante
			Where IDTransaccion=@IDTransaccion

			
			--Eliminamos los asientos
			Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
			Delete Asiento Where IDTransaccion = @IDTransaccion
			--Eliminamos el DetalleImpuesto
			Delete DetalleImpuesto Where IDTransaccion = @IDTransaccion
			
		End

		--Retencion
		Set @vDecimal = IsNull((Select Top(1) [Decimales] From Moneda Where ID=@IDMoneda), 'True')
		set @PorcentajeRetencion = (Select Top(1) CompraPorcentajeRetencion  from Configuraciones )
		set @RetencionIVA  = (@TotalImpuesto  * @PorcentajeRetencion) / 100
		
		--Redondear si la moneda no tiene decimales!!!!!!
		If @vDecimal = 'False' Begin
			set @RetencionIVA = ROUND(@RetencionIVA, 0)
		End Else Begin
			set @RetencionIVA = ROUND(@RetencionIVA, 2)
		End
		
		-- Validar sin no esta cancelado
		If Not Exists(Select * from Gasto Where Cancelado='True' And IDTransaccion=@IDTransaccion )Begin
		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from Gasto where IDTransaccion = @IDTransaccion)
		set @IDProveedor = (select IDProveedor from Gasto where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from Gasto where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from Gasto where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select Comprobante from VGasto where IDTransaccion = @IDTransaccion)
		set @VTipoComprobante = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from Gasto where IDTransaccion = @IDTransaccion)
		set @Condicion = (select (Case when Credito = 'True' then 'CREDITO' else 'CONTADO' end) from Gasto where IDTransaccion = @IDTransaccion)

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,0,@IDProveedor,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'MOD',@VTipoComprobante, @IDSucursal)

			Update Gasto Set 
							IDTipoComprobante=@IDTipoComprobante,
							NroComprobante=@NroComprobante,     
							IDProveedor=@IDProveedor,  	
							IDSucursal=@IDSucursalOperacion, 	
							IDGrupo=@IDGrupo,	
							Fecha=@Fecha,	
							Credito=@Credito,
							FechaVencimiento=@FechaVencimiento,
							FechaVencimientoTimbrado=@FechaVencimientoTimbrado,
							IDMoneda=@IDMoneda,
							Cotizacion=@Cotizacion,
							Observacion=@Observacion,
							Total=@vSaldo,
							TotalImpuesto=@TotalImpuesto,
							TotalDiscriminado=@TotalDiscriminado,
							Saldo=@vSaldo,
							Cancelado=@vCancelado,
							Directo=@Directo,
							Efectivo=@Efectivo,
							Cheque=@Cheque,
							CajaChica=@CajaChica ,
							Cuota=@Cuota,
							IncluirLibro=@IncluirLibro,
							NroTimbrado=@NroTimbrado,
							RetencionIVA = @RetencionIVA
			Where IDTransaccion=@IDTransaccion
			
			Set @Mensaje = 'Registro guardado!'
			Set @Procesado = 'True'
			
		End
		
		--Modificar Gasto Tipo Comprobante si está relacionado
		Update GastoTipoComprobante Set IDSucursal=@IDSucursalOperacion
		Where IDTransaccion=@IDTransaccion
		
		--Modificar Gasto Fondo Fijo si está relacionado
		Update GastoFondoFijo Set IDSucursal=@IDSucursalOperacion 
		Where IDTransaccion=@IDTransaccion 
		
			--select * from agasto
		insert into aGasto (IDTransaccion,Numero,IDTipoComprobante,NroComprobante,IDProveedor,IDSucursal,IDGrupo,Fecha,Credito,FechaVencimiento,IDMoneda,Cotizacion,Observacion,IDTransaccionRemision,Total,TotalImpuesto,TotalDiscriminado,Saldo,Cancelado,Directo,Efectivo,Cheque,CajaChica,Cuota,RetencionIVA,RetencionRenta,NroTimbrado,FechaVencimientoTimbrado,IncluirLibro,IDDeposito,IDChofer,IDCamion,Pagar,ImporteAPagar,IDCuentaBancaria,ObservacionPago,RRHH,IDDepartamentoEmpresa,FechaModificacion,IDUsuarioModificacion,IDTerminalModificacion,Operacion)
		Select IDTransaccion,Numero,IDTipoComprobante,NroComprobante,IDProveedor,IDSucursal,IDGrupo,Fecha,Credito,FechaVencimiento,IDMoneda,Cotizacion,Observacion,IDTransaccionRemision,Total,TotalImpuesto,TotalDiscriminado,Saldo,Cancelado,Directo,Efectivo,Cheque,CajaChica,Cuota,RetencionIVA,RetencionRenta,NroTimbrado,FechaVencimientoTimbrado,IncluirLibro,IDDeposito,IDChofer,IDCamion,Pagar,ImporteAPagar,IDCuentaBancaria,ObservacionPago,RRHH,IDDepartamentoEmpresa,GetDate(),@IDUsuario,@IDTerminal,@Operacion from Gasto where IDTransaccion = @IDTransaccionSalida

		--Validar cuotas
		If @Cuota > 0 Begin
			Delete From Cuota 
			Where IDTransaccion=@IDTransaccion
		End

		Set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
			
	End

	If @Operacion = 'UPD2' Begin
	
		--Credito
		If @Credito='True' Begin
			Set @Fecha = (Select Fecha from Gasto where idtransaccion = @IDTransaccion)
			--Fecha
			If DATEDIFF(dd, @Fecha, @FechaVencimiento) < 0 Begin
				set @Mensaje = 'La fecha de de vencimiento no puede ser menor al del comprobante!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
						
		End
		
		Update Gasto set Credito = @Credito,
						Observacion = @Observacion,
						FechaVencimiento = @FechaVencimiento,
						FechaVencimientoTimbrado = @FechaVencimientoTimbrado,
						NroTimbrado = @NroTimbrado
	    Where IDtransaccion = @IDTransaccion
		
		Set @IDTransaccionSalida = @IDTransaccion
		
		insert into aGasto (IDTransaccion,Numero,IDTipoComprobante,NroComprobante,IDProveedor,IDSucursal,IDGrupo,Fecha,Credito,FechaVencimiento,IDMoneda,Cotizacion,Observacion,IDTransaccionRemision,Total,TotalImpuesto,TotalDiscriminado,Saldo,Cancelado,Directo,Efectivo,Cheque,CajaChica,Cuota,RetencionIVA,RetencionRenta,NroTimbrado,FechaVencimientoTimbrado,IncluirLibro,IDDeposito,IDChofer,IDCamion,Pagar,ImporteAPagar,IDCuentaBancaria,ObservacionPago,RRHH,IDDepartamentoEmpresa,FechaModificacion,IDUsuarioModificacion,IDTerminalModificacion,Operacion)
		Select IDTransaccion,Numero,IDTipoComprobante,NroComprobante,IDProveedor,IDSucursal,IDGrupo,Fecha,Credito,FechaVencimiento,IDMoneda,Cotizacion,Observacion,IDTransaccionRemision,Total,TotalImpuesto,TotalDiscriminado,Saldo,Cancelado,Directo,Efectivo,Cheque,CajaChica,Cuota,RetencionIVA,RetencionRenta,NroTimbrado,FechaVencimientoTimbrado,IncluirLibro,IDDeposito,IDChofer,IDCamion,Pagar,ImporteAPagar,IDCuentaBancaria,ObservacionPago,RRHH,IDDepartamentoEmpresa,GetDate(),@IDUsuario,@IDTerminal,@Operacion from Gasto where IDTransaccion = @IDTransaccionSalida

		Set @Mensaje = 'Registro Cancelado por OP.'
		Set @Procesado = 'True'
		return @@rowcount
			
	End

	--Cuando se va generar cuotas por el saldo del documento
	IF @Operacion = 'CUOTA' Begin
	 --Validar cuotas
		If @Cuota > 0 Begin

			--Si ya se pago la cuota
			If (Select Count(*) From Cuota Where IDTransaccion=@IDTransaccion) > 0 Begin
				set @Mensaje = 'El comprobante ya tiene cuotas pagadas! No se puede Generar.'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
		End

		update gasto 
		set Cuota = @Cuota
		where IDTransaccion = @IDTransaccion

			--select * from agasto
		insert into aGasto (IDTransaccion,Numero,IDTipoComprobante,NroComprobante,IDProveedor,IDSucursal,IDGrupo,Fecha,Credito,FechaVencimiento,IDMoneda,Cotizacion,Observacion,IDTransaccionRemision,Total,TotalImpuesto,TotalDiscriminado,Saldo,Cancelado,Directo,Efectivo,Cheque,CajaChica,Cuota,RetencionIVA,RetencionRenta,NroTimbrado,FechaVencimientoTimbrado,IncluirLibro,IDDeposito,IDChofer,IDCamion,Pagar,ImporteAPagar,IDCuentaBancaria,ObservacionPago,RRHH,IDDepartamentoEmpresa,FechaModificacion,IDUsuarioModificacion,IDTerminalModificacion,Operacion)
		Select IDTransaccion,Numero,IDTipoComprobante,NroComprobante,IDProveedor,IDSucursal,IDGrupo,Fecha,Credito,FechaVencimiento,IDMoneda,Cotizacion,Observacion,IDTransaccionRemision,Total,TotalImpuesto,TotalDiscriminado,Saldo,Cancelado,Directo,Efectivo,Cheque,CajaChica,Cuota,RetencionIVA,RetencionRenta,NroTimbrado,FechaVencimientoTimbrado,IncluirLibro,IDDeposito,IDChofer,IDCamion,Pagar,ImporteAPagar,IDCuentaBancaria,ObservacionPago,RRHH,IDDepartamentoEmpresa,GetDate(),@IDUsuario,@IDTerminal,@Operacion from Gasto where IDTransaccion = @IDTransaccionSalida


		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'

		Set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
	End
	
	set @Mensaje = 'No se proceso ninguna transacción!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End






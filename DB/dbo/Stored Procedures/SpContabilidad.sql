﻿CREATE Procedure [dbo].[SpContabilidad]

	--Entrada
	@Desde date,
	@Hasta date,
	@Tipo varchar(50)
	
As

Begin
	
	Declare @vMensaje varchar(100)
	Declare @vProcesado bit
	Declare @vNumero numeric(18,0)
	
	Set @Tipo = REPLACE(@Tipo, ' DETALLADO', '')
	Set @Tipo = REPLACE(@Tipo, ' RESUMIDO', '')
	
	Set @Tipo = REPLACE(@Tipo, 'COBRANZA CREDITO', 'COBRANZA')
	Set @Tipo = REPLACE(@Tipo, 'COBRANZA CONTADO', 'COBRANZA')
	
	Set @vMensaje = 'Sin procesar'
	Set @vProcesado = 'False'
	
	Declare db_cursor cursor for
	Select Numero From AsientoCG
	Where (Fecha Between @Desde And @Hasta) And Tipo=@Tipo
	Open db_cursor   
	Fetch Next From db_cursor Into @vNumero
	While @@FETCH_STATUS = 0 Begin 
	
		--Eliminar el Detalle
		Delete From DetalleAsientoCG Where Numero=@vNumero
	
		--Eliminar del Asiento
		Delete From AsientoCG Where Numero=@vNumero
	
		Fetch Next From db_cursor Into @vNumero
									
	End
	
	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor
	
	Set @vMensaje = 'Registro eliminado!'
	Set @vProcesado = 'True'
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	
End
	
	
	
	


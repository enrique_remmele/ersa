﻿CREATE Procedure [dbo].[SpCobranzaContadoProcesar]

	--Entrada
	@IDTransaccion numeric(18,0),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'
		
	--Validar Feha Operacion
	Declare @vIDSucursal as integer
	Declare @vFecha as Date
	Declare @vIDUsuario as integer
	Declare @vIDOperacion as integer

	Select	@vIDSucursal=IDSucursal,
			@vFecha=Fecha,
			@vIDUsuario=IDUsuario,
			@vIDOperacion=IDOperacion
	 From Transaccion Where ID = @IDTransaccion

--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@vIDSucursal, @vFecha, @vIDUsuario, @vIDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
		End
	End 

	IF @Operacion = 'DEL' Begin
		If dbo.FValidarFechaOperacion(@vIDSucursal, @vFecha, @vIDUsuario, @vIDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
		End
	End 

	--Aplicar Cobranza
	If @Operacion = 'INS' Begin
	
		--Procesamos las Ventas y Saldo del Cliente
		EXEC SpVentaCobranza @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Procesamos los Cheques si es que hay
		EXEC SpChequeClienteSaldar @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Damos el OK a la Cobranza
		If @Procesado = 'True' Begin
			Update CobranzaContado Set Procesado='True' Where IDTransaccion=@IDTransaccion
			Set @Mensaje = 'Registro procesado!'
			
			--Generar Asiento
			Exec SpAsientoCobranzaContado @IDTransaccion=@IDTransaccion
			
			GoTo Salir
		End 
		
		If @Procesado = 'True' Begin
			--Si hubo un error, eliminar todo el registro
			Set @Operacion = 'DEL'			
		End
				
	End
	
	--Eliminamos todo el registro
	If @Operacion = 'DEL' Begin
		
		--Reestablecemos las Ventas y Saldo del Cliente
		EXEC SpVentaCobranza @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Reestablecemos el cheque
		EXEC SpChequeClienteSaldar @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
				
		Delete From Efectivo Where IDTransaccion = @IDTransaccion
		Delete From FormaPagoDocumento Where IDTransaccion = @IDTransaccion
		Delete From FormaPago Where IDTransaccion = @IDTransaccion
		Delete From VentaCobranza Where IDTransaccionCobranza = @IDTransaccion
		Delete From CobranzaContado Where IDTransaccion = @IDTransaccion
		Delete From FormaPagoTarjeta Where IDTransaccion = @IDTransaccion --JGR 11/04/2014
		
		
	End
	
Salir:
	
	--Actualizar saldos de clientes
	Begin
		Declare @vIDTransaccionVenta numeric(18,0)
		Declare @vIDCliente int
		
		Declare db_cursor cursor for
		Select IDTransaccionVenta From VentaCobranza Where IDTransaccionCobranza=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVenta
		While @@FETCH_STATUS = 0 Begin 
			
			Set @vIDCliente = (Select IDCliente From Venta Where IDTransaccion=@vIDTransaccionVenta)
			
			Exec SpAcualizarSaldoCliente @IDCliente=@vIDCliente
			
			Fetch Next From db_cursor Into @vIDTransaccionVenta
										
		End
		
		Close db_cursor   
		Deallocate db_cursor
		
	End
	
End



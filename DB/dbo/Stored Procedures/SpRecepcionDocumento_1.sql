﻿CREATE Procedure [dbo].[SpRecepcionDocumento]

	
	--Entrada
	@IDTransaccion int,
	@NombreRecibido varchar(80),
	@FechaRecibido date,
	@Observacion varchar(100),
	@Operacion varchar(10),
	--Auditoria
	@IDUsuario smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
	
		--Insertamos
		Insert Into RecepcionDocumento(IDTransaccion, NombreRecibido, FechaRecibido, Observacion,IDUsuario,FechaRegistro)
		Values(@IDTransaccion, @NombreRecibido, @FechaRecibido, @Observacion, @IDUsuario,GetDate())
		
		Insert Into aRecepcionDocumento(IDTransaccion, NombreRecibido, FechaRecibido, Observacion,IDUsuario,FechaRegistro,OperacionAuditoria, FechaAuditoria, IDUsuarioAuditoria)
		Values(@IDTransaccion, @NombreRecibido, @FechaRecibido, @Observacion, @IDUsuario,GetDate(),'INS',GetDate(),@IDUsuario)
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
			
		--Actualizamos
		Update RecepcionDocumento Set NombreRecibido=@NombreRecibido
									,FechaRecibido=@FechaRecibido
									,Observacion=@Observacion
									,IDUsuario=@IDUsuario
									,FechaRegistro=GETDATE()
		Where IDTransaccion=@IDTransaccion
		
		Insert Into aRecepcionDocumento(IDTransaccion, NombreRecibido, FechaRecibido, Observacion,IDUsuario,FechaRegistro,OperacionAuditoria, FechaAuditoria, IDUsuarioAuditoria)
		Values(@IDTransaccion, @NombreRecibido, @FechaRecibido, @Observacion, @IDUsuario,GetDate(),'UPD',GetDate(),@IDUsuario)
		
		set @Mensaje = 'Registro actualizado!'
		set @Procesado = 'True'
		return @@rowcount
			
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		
		Insert Into aRecepcionDocumento(IDTransaccion, NombreRecibido, FechaRecibido, Observacion,IDUsuario,FechaRegistro,OperacionAuditoria, FechaAuditoria, IDUsuarioAuditoria)
		Values(@IDTransaccion, @NombreRecibido, @FechaRecibido, @Observacion, @IDUsuario,GetDate(),'DEL',GetDate(),@IDUsuario)
		
		--Eliminamos
		Delete From RecepcionDocumento 
		Where IDTransaccion=@IDTransaccion
		
		set @Mensaje = 'Registro eliminado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


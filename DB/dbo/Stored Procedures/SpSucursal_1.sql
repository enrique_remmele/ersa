﻿CREATE Procedure [dbo].[SpSucursal]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Codigo varchar(5),
	@CodigoDistribuidor varchar(50)=NULL,
	@IDPais tinyint = NULL,
	@IDDepartamento tinyint = NULL,
	@IDCiudad smallint = NULL,
	@Direccion varchar(100) = NULL,
	@Telefono varchar(20) = NULL,
	@Referencia varchar(5) = NULL,
	@CuentaContable varchar(50) = NULL,
	@Estado bit,
	@Operacion varchar(10),
		
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Sucursal Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que el codigo ya existe
		if exists(Select * From Sucursal Where Codigo=@Codigo) begin
			set @Mensaje = 'El codigo ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDSucursal tinyint
		set @vIDSucursal = (Select IsNull((Max(ID)+1), 1) From Sucursal)

		--Insertamos
		Insert Into Sucursal(ID, Descripcion, IDPais, IDDepartamento, Codigo, IDCiudad, Direccion, Telefono, Referencia, CuentaContable, Estado, CodigoDistribuidor)
		Values(@vIDSucursal, @Descripcion, @IDPais, @IDDepartamento, @Codigo, @IDCiudad, @Direccion, @Telefono, @Referencia, @CuentaContable, @Estado, @CodigoDistribuidor)		
		
		--Inicializar
		Insert into Configuraciones(IDSucursal,FormatoFecha,PropietarioBD,VersionSistema,BloquearDocumentos,DiasBloqueo,OperacionBloquearFechaActiva,OperacionBloquearFechaDesde,OperacionBloquearFechaHasta,MovimientoBloquearFecha,MovimientoExigirAutorizacion,MovimientoImprimirDocumento,MovimientoBloquearCampoCosto,MovimientoHabilitarSiCostoCero,MovimientoCostoProducto,MovimientoBloquearAnulacion,MovimientoDiasBloqueo,VentaBloquearFecha,VentaImprimirDocumento,VentaBloquearAnulacion,VentaDiasBloqueo,VentaFechaFacturacionActivar,VentaFechaFacturacion,VentaModificarDescuento,VentaModificarCantidad,VentaPermitirCantidadNegativa,VentaAdvertirCantidadNegativa,VentaControlarDescuentoTactico,VentaSuperarTacticoConCredencial,VentaControlarPorcentaje,VentaPedirCredencialAgregarDescuento,VentaControlarImporte,ERPTablasPrincipales,ERPTablasMayores,CobranzaCreditoBloquearFecha,CobranzaCreditoImprimirDocumento,CobranzaCreditoBloquearAnulacion,CobranzaCreditoDiasBloqueo,CobranzaCreditoTipoComprobanteRetencion,CobranzaContadoBloquearFecha,CobranzaContadoImprimirDocumento,CobranzaControlarNroRecibo,CobranzaContadoBloquearAnulacion,CobranzaContadoDiasBloqueo,OrdenPagoBloquearAnulacion,OrdenPagoDiasBloqueo,OrdenPagoALaOrdenPredefinido,PedidoCantidadProducto,PedidoBloquearTactico,PedidoControlarTactico,PedidoControlarTacticoPorcentaje,PedidoControlarTacticoImporte,PedidoPermitirCantidadNegativa,PedidoAdvertirCantidadNegativa,CompraBloquearEliminacion,CompraDiasBloqueo,CompraBloquearCondicion,CompraPorcentajeRetencion,CompraImporteMinimoRetencion,ProductoClasificacion1,ProductoClasificacion2,ProductoClasificacion3,ProductoClasificacion4,ProductoClasificacion5,ProductoClasificacion6,RetencionImprimirDocumento,ProductoClasificacion7,GastoBloquearEliminacion,GastoDiasBloqueo,GastoHabilitarDeposito,GastoHabilitarDistribucion,VentaModificarPrecio,VentaControlarBajoElMargen,VentaControlarBajoElCosto,MailCuenta,MailPassword,MailServer,MailPuerto,MailSSL,ImagenCabeceraInforme,UtilizaFactorCosto,UtilizaFactorVenta,VentaControlarStock,RemisionCantidadLineas,RemisionDireccionSalida,RemisionImprimir,CobranzaCreditoTipoCAcreditacionBancaria,BloquearListaPrecio,DescargaStockSinExistencia,VentaAnularFechaDistintaHoy,BloquearNroComprobante,VentaImprimirSinRpt,NCImprimirSinRpt,FechaEntregaChequeOP,ContadoChequeDiferido,TesoreriaBloquerFecha,ControlarPorcentajeDescuento,BloquearVentaSinPedido)		
		select @vIDSucursal,FormatoFecha,PropietarioBD,VersionSistema,BloquearDocumentos,DiasBloqueo,OperacionBloquearFechaActiva,OperacionBloquearFechaDesde,OperacionBloquearFechaHasta,MovimientoBloquearFecha,MovimientoExigirAutorizacion,MovimientoImprimirDocumento,MovimientoBloquearCampoCosto,MovimientoHabilitarSiCostoCero,MovimientoCostoProducto,MovimientoBloquearAnulacion,MovimientoDiasBloqueo,VentaBloquearFecha,VentaImprimirDocumento,VentaBloquearAnulacion,VentaDiasBloqueo,VentaFechaFacturacionActivar,VentaFechaFacturacion,VentaModificarDescuento,VentaModificarCantidad,VentaPermitirCantidadNegativa,VentaAdvertirCantidadNegativa,VentaControlarDescuentoTactico,VentaSuperarTacticoConCredencial,VentaControlarPorcentaje,VentaPedirCredencialAgregarDescuento,VentaControlarImporte,ERPTablasPrincipales,ERPTablasMayores,CobranzaCreditoBloquearFecha,CobranzaCreditoImprimirDocumento,CobranzaCreditoBloquearAnulacion,CobranzaCreditoDiasBloqueo,CobranzaCreditoTipoComprobanteRetencion,CobranzaContadoBloquearFecha,CobranzaContadoImprimirDocumento,CobranzaControlarNroRecibo,CobranzaContadoBloquearAnulacion,CobranzaContadoDiasBloqueo,OrdenPagoBloquearAnulacion,OrdenPagoDiasBloqueo,OrdenPagoALaOrdenPredefinido,PedidoCantidadProducto,PedidoBloquearTactico,PedidoControlarTactico,PedidoControlarTacticoPorcentaje,PedidoControlarTacticoImporte,PedidoPermitirCantidadNegativa,PedidoAdvertirCantidadNegativa,CompraBloquearEliminacion,CompraDiasBloqueo,CompraBloquearCondicion,CompraPorcentajeRetencion,CompraImporteMinimoRetencion,ProductoClasificacion1,ProductoClasificacion2,ProductoClasificacion3,ProductoClasificacion4,ProductoClasificacion5,ProductoClasificacion6,RetencionImprimirDocumento,ProductoClasificacion7,GastoBloquearEliminacion,GastoDiasBloqueo,GastoHabilitarDeposito,GastoHabilitarDistribucion,VentaModificarPrecio,VentaControlarBajoElMargen,VentaControlarBajoElCosto,MailCuenta,MailPassword,MailServer,MailPuerto,MailSSL,ImagenCabeceraInforme,UtilizaFactorCosto,UtilizaFactorVenta,VentaControlarStock,RemisionCantidadLineas,RemisionDireccionSalida,RemisionImprimir,CobranzaCreditoTipoCAcreditacionBancaria,BloquearListaPrecio,DescargaStockSinExistencia,VentaAnularFechaDistintaHoy,BloquearNroComprobante,VentaImprimirSinRpt,NCImprimirSinRpt,FechaEntregaChequeOP,ContadoChequeDiferido,TesoreriaBloquerFecha,ControlarPorcentajeDescuento,BloquearVentaSinPedido from Configuraciones where IDSucursal = 1

		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='SUCURSAL', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From Sucursal Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Sucursal Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que el codigo ya existe
		if exists(Select * From Sucursal Where Codigo=@Codigo And ID!=@ID) begin
			set @Mensaje = 'El codigo ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Sucursal Set Descripcion=@Descripcion,
							Codigo=@Codigo,
							IDPais=@IDPais, 
							IDDepartamento=@IDDepartamento, 
							IDCiudad=@IDCiudad, 
							Direccion=@Direccion, 
							Telefono=@Telefono, 
							Referencia=@Referencia, 
							CuentaContable=@CuentaContable, 
							Estado = @Estado,
							CodigoDistribuidor=@CodigoDistribuidor
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='SUCURSAL', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Sucursal Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Transaccion
		if exists(Select * From Transaccion Where IDSucursal=@ID) begin
			Declare @vOperacion varchar(50) = (Select Top(1) O.Descripcion From Transaccion T Join Operacion O On T.IDOperacion=O.ID And T.IDSucursal=@ID)
			set @Mensaje = 'El registro tiene transacciones asociados! No se puede eliminar. Operacion: ' +@vOperacion
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con CLIENTES
		if exists(Select * From Cliente Where IDSucursal=@ID) begin
			set @Mensaje = 'El registro tiene clientes asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Sucursal 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='SUCURSAL', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


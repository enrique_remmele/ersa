﻿
CREATE Procedure [dbo].[SpNotaCreditoActualizarVanta]

	--Entrada
	@IDTransaccion numeric(18, 0),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	--Variable
	Declare @vIDTransaccionVentaGenerada numeric(18,0)
	Declare @vDescontado money
	Declare @vSaldo money
	Declare @vImporte money
	Declare @vCancelado bit
	
	Set @Mensaje = ''
	Set @Procesado = 'False'
	
	If @Operacion = 'INS' Begin
				
		Declare db_cursor cursor for
		Select  V.IDTransaccionVentaGenerada, V.Importe From NotaCreditoVenta V
		Join NotaCredito N On V.IDTransaccionNotaCredito=N.IDTransaccion
		Where V.IDTransaccionVentaGenerada  Is Not Null 
		And V.IDTransaccionNotaCredito=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVentaGenerada , @vImporte
		While @@FETCH_STATUS = 0 Begin 
		 
			--Hayar Valores
			--Saldo
			Set @vSaldo = (Select Saldo From Venta Where IDTransaccion=@vIDTransaccionVentaGenerada )
			Set @vSaldo = @vSaldo - @vImporte
			
			--Cancelado
			Set @vCancelado = 'False'
			
			If @vSaldo < 1 Begin
				Set @vCancelado = 'True'
			End
			
			--Descontado
			Set @vDescontado = (Select Descontado From Venta Where IDTransaccion=@vIDTransaccionVentaGenerada )
			Set @vDescontado = @vDescontado + @vImporte
			
			if @vSaldo > (Select Total from Venta where IDTransaccion = @vIDTransaccionVentaGenerada) begin
				Set @Mensaje = 'Saldo supera Monto Total de la factura'
				Set @Procesado = 'False'
				return @@rowcount
			end

			--Actualizar Venta
			Update Venta Set Saldo=@vSaldo,
							Cancelado = @vCancelado,
							Descontado=@vDescontado
			Where IDTransaccion=@vIDTransaccionVentaGenerada 
			
			Fetch Next From db_cursor Into @vIDTransaccionVentaGenerada , @vImporte
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
		--Actualizar saldo de la Nota de Credito
		Declare @vSaldoNotaCredito money
		Declare @vTotalNotaCredito money
		
		Set @vTotalNotaCredito = (Select Total From NotaCredito Where IDTransaccion=@IDTransaccion)
		Set @vSaldoNotaCredito = (Select SUM(Importe) From NotaCreditoVenta Where IDTransaccionNotaCredito=@IDTransaccion)
		Set @vSaldoNotaCredito = @vTotalNotaCredito - @vSaldoNotaCredito
		
		Update NotaCredito Set Saldo=@vSaldoNotaCredito
		Where IDTransaccion=@IDTransaccion
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
		
	If @Operacion = 'ANULAR' Begin
		
		Declare db_cursor cursor for
		--Select IDTransaccionVentaGenerada, Importe From NotaCreditoVenta 
		--Where IDTransaccionVentaGenerada Is Not Null And IDTransaccionNotaCredito=@IDTransaccion
		Select NCV.IDTransaccionVentaGenerada, NCV.Importe 
		From NotaCreditoVenta NCV 
		Join NotaCredito NC on NC.IDTransaccion = NCV.IDTransaccionNotaCredito
		Where IDTransaccionVentaGenerada Is Not Null And IDTransaccionNotaCredito=@IDTransaccion
		and Aplicar = 0
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVentaGenerada , @vImporte
		While @@FETCH_STATUS = 0 Begin 
		 
			--Hayar Valores
			--Saldo
			Set @vSaldo = (Select Saldo From Venta Where IDTransaccion=@vIDTransaccionVentaGenerada )
			Set @vSaldo = @vSaldo + @vImporte
			
			--Cancelado
			Set @vCancelado = 'False'
			
			--Descontado
			Set @vDescontado = (Select Descontado From Venta Where IDTransaccion=@vIDTransaccionVentaGenerada )
			Set @vDescontado = @vDescontado - @vImporte
			
			--Actualizar Venta
			Update Venta Set Saldo=@vSaldo,
							Cancelado = @vCancelado,
							Descontado=@vDescontado							
			Where IDTransaccion=@vIDTransaccionVentaGenerada 
			--and Total <= @vSaldo --Se agrego porque actualizaba en algunos casos no saldo mayores al total
			
			Fetch Next From db_cursor Into @vIDTransaccionVentaGenerada , @vImporte
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		
	End
	
	If @Operacion = 'ELIMINAR' Begin
	
		If (Select Anulado From NotaCredito Where IDTransaccion=@IDTransaccion) = 'True' Begin
			Set @Mensaje = 'Registro guardado'
			Set @Procesado = 'True'
			return @@rowcount
		End
		
	    Declare db_cursor cursor for
		Select IDTransaccionVentaGenerada, Importe From NotaCreditoVenta 
		Where IDTransaccionVentaGenerada Is Not Null And IDTransaccionNotaCredito=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVentaGenerada , @vImporte
		While @@FETCH_STATUS = 0 Begin 
		 
			--Hayar Valores
			--Saldo
			Set @vSaldo = (Select Saldo From Venta Where IDTransaccion=@vIDTransaccionVentaGenerada )
			Set @vSaldo = @vSaldo + @vImporte
			
			--Cancelado
			Set @vCancelado = 'False'
		
			--Descontado
			Set @vDescontado = (Select Descontado From Venta Where IDTransaccion=@vIDTransaccionVentaGenerada )
			Set @vDescontado = @vDescontado - @vImporte
			
			--Actualizar Venta
			Update Venta Set Saldo=@vSaldo,
							Cancelado = @vCancelado,
							Descontado=@vDescontado
			Where IDTransaccion=@vIDTransaccionVentaGenerada 
			
			Fetch Next From db_cursor Into @vIDTransaccionVentaGenerada , @vImporte
										
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
	
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		
	End
	
End	






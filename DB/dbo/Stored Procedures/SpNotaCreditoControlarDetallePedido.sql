﻿CREATE Procedure [dbo].[SpNotaCreditoControlarDetallePedido]

	--Entrada
	@IDTransaccion numeric(18,0) 
		
As

Begin
	Declare @vSubMotivo varchar(50)
	Declare @vIDTransaccionPedidoNotaCredito int
	Declare @vIDTransaccionVenta int
	
	--Controlar que la nc no este anulada
	if exists(Select * from vnotacredito where idtransaccion = @IDTransaccion and anulado = 1) begin
		Goto Salir
	end
	
	Set @vIDTransaccionPedidoNotaCredito = (Select top(1) idtransaccionpedido from pedidoncnotacredito where idtransaccionnotacredito = @IDTransaccion)

	--Controlar que el pedido no este anulado
	if exists(Select * from PEDIDOnotacredito where idtransaccion = @vIDTransaccionPedidoNotaCredito and anulado = 1) begin
		Goto Salir
	end
	Set @vSubMotivo = (select SubMotivo from vnotacredito where idtransaccion = @IDTransaccion)

	if @vSubMotivo = 'DIFERENCIA DE PRECIO' begin
		
		Set @vIDTransaccionVenta = (select IDTransaccionVentaGenerada from NotaCreditoVenta where IDTransaccionNotaCredito = @IDTransaccion)

		if (select top(1) isnull(IDTransaccionVenta,0) from DetalleNotaCreditoDiferenciaPrecio where IDTransaccionNotaCredito = @IDTransaccion) = 0 begin
			Update DetalleNotaCreditoDiferenciaPrecio set IDTransaccionVenta = @vIDTransaccionVenta 
			where IDTransaccionNotaCredito = @IDTransaccion
		end

	end

	Salir:
			
End





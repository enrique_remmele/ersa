﻿CREATE Procedure [dbo].[SpDetalleFormulario]

	--Entrada
	@IDFormulario int = NULL,
	@IDDetalleFormulario tinyint = NULL,
	@NombreCampo varchar(50) = '',
	@Campo varchar(50) = '',
	@PosicionX smallint = 0,
	@PosicionY smallint = 0,
	@TipoLetra varchar(15) = 'Arial',
	@TamañoLetra tinyint = 7,
	@Formato varchar(50) = 'Normal',
	@Valor varchar(100) = '',
	@Detalle bit = 'False',

	@Numerico bit = 'False',
	@Alineacion varchar(15) = 'Izquierda', --Izquierda, Derecha, Centrada 
	@Largo tinyint = 0,
	@PuedeCrecer bit = 'False',
	@Lineas tinyint = 0,

	@Operacion varchar(10)

As

Begin

	Declare @vIDDetalleFormulario int
	Declare @vMensaje varchar(150)
	Declare @vProcesado bit
	
	Set @vMensaje = ''
	Set @vProcesado = 'False'
	Set @vIDDetalleFormulario = 0
	
	--Insertar
	If @Operacion='INS' Begin
		
		--Validar
		If Exists(Select * From DetalleFormulario Where IDFormulario=@IDFormulario And NombreCampo=@NombreCampo) Begin
			Set @vMensaje = 'El el campo ya existe!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		Set @vIDDetalleFormulario = (Select IsNull(Max(IDDetalleFormulario)+1, 1) From DetalleFormulario Where IDFormulario=@IDFormulario)
		
		Insert Into DetalleFormulario(IDFormulario, IDDetalleFormulario, NombreCampo, Campo, PosicionX, PosicionY, TipoLetra, TamañoLetra, Formato, Valor, Detalle)
		Values(@IDFormulario, @vIDDetalleFormulario, @NombreCampo, @Campo, @PosicionX, @PosicionY, @TipoLetra, @TamañoLetra, @Formato, @Valor, @Detalle)
		
		Set @vMensaje = 'Registro insertado!'
		Set @vProcesado = 'True'
		GoTo Salir

	End
	
	--Actualizar
	If @Operacion='UPD' Begin
		
		--Validar
		If Not Exists(Select * From DetalleFormulario Where IDFormulario=@IDFormulario And IDDetalleFormulario=@IDDetalleFormulario) Begin
			Set @vMensaje = 'El sistema no encuentra el registro solicitado!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		Update DetalleFormulario Set	PosicionX=@PosicionX,
										PosicionY=@PosicionY,
										TipoLetra=@TipoLetra,
										TamañoLetra=@TamañoLetra,
										Formato=@Formato,
										Numerico=@Numerico,
										Alineacion=@Alineacion,
										Largo=@Largo,
										PuedeCrecer=@PuedeCrecer,
										Lineas=@Lineas
		Where IDFormulario=@IDFormulario And IDDetalleFormulario=@IDDetalleFormulario
						
		Set @vMensaje = 'Registro actualizado!'
		Set @vProcesado = 'True'
		GoTo Salir

	End
	
	--Eliminar
	If @Operacion='DEL' Begin
		
		--Validar
		If Not Exists(Select * From DetalleFormulario Where IDFormulario=@IDFormulario And IDDetalleFormulario=@IDDetalleFormulario) Begin
			Set @vMensaje = 'El sistema no encuentra el registro solicitado!'
			Set @vProcesado = 'False'
			GoTo Salir
		End

		Delete From DetalleFormulario
		Where IDFormulario=@IDFormulario And IDDetalleFormulario=@IDDetalleFormulario
				
		Set @vMensaje = 'Registro eliminado!'
		Set @vProcesado = 'True'

	End
			
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	
End


﻿CREATE Procedure [dbo].[SpReestablecerContraseña]

	--Entrada
	@IDUsuarioCambio smallint,
	
	--Auditoria
	@IDUsuario smallint,
	@IDPerfil smallint,
	@IDSucursal tinyint,
	@IDTerminal smallint,
			
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output

As

Begin

	Set @Mensaje = '---'
	Set @Procesado = 'False'
		
	--Validar
	Declare @ContraseñaActual varchar(500)
	Set @ContraseñaActual = Convert(varchar(300), HASHBYTES('Sha1', '12345'))

	Exec SpLogSuceso @Operacion='INS', @Tabla='CONTRASEñA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@IDUsuario
	
	Update Usuario Set [Password]=@ContraseñaActual
	Where ID=@IDUsuarioCambio

	Set @Mensaje = 'Registro guardado!'
	Set @Procesado = 'True'	
		
End

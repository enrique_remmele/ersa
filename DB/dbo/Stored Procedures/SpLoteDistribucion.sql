﻿CREATE Procedure [dbo].[SpLoteDistribucion]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@Numero int = NULL,
	@IDTipoComprobante smallint = NULL,
	@Comprobante varchar(50) = NULL,	
	@Fecha date = NULL,
	@FechaReparto date = NULL,
	@IDDistribuidor tinyint,
	@IDCamion tinyint,
	@IDChofer tinyint,
	@IDZonaVenta tinyint,
	
	@TotalContado money,
	@TotalCredito money,
	@Total money,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@TotalDescuento money = NULL,
	@CantidadComprobantes smallint = NULL,
	@Observacion varchar(100) = NULL,
	@Operacion varchar(10),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) OUTPUT 
	
As

Begin
	SET @IDTransaccionSalida = @IDTransaccion
	--Procesar Saldos de Ventas
	If @Operacion = 'INS' Begin
	
		--Validar
		----Numero
		--If Exists(Select TOP 1 * From LoteDistribucion Where Numero=@Numero And IDSucursal=@IDSucursal) Begin
		--	set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
		--	set @Procesado = 'False'
		--	set @IDTransaccionSalida  = 0
		--	return @@rowcount
		--End
		
		--Si ya existe el documento
		If Exists(Select TOP 1 * From LoteDistribucion Where IDTipoComprobante=@IDTipoComprobante And Comprobante=@Comprobante And IDSucursal=@IDSucursal And Anulado='False') Begin
			set @Mensaje = 'El numero de comprobante ya existe!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		END
		
		
		
		--Insertar Transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT

		--Insertamos el registro
		Declare @vNumero int
		
		Set @vNumero= IsNull((Select Max(Numero)+1 From LoteDistribucion Where IDSucursal=@IDSucursalOperacion), 1)
		
		Insert Into LoteDistribucion(IDTransaccion, IDSucursal, Numero, IDTipoComprobante, Comprobante, Fecha, FechaReparto, IDDistribuidor, IDCamion, IDChofer, IDZonaVenta, TotalContado, TotalCredito, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, CantidadComprobantes, Anulado, Observacion)
		Values(@IDTransaccionSalida, @IDSucursalOperacion, @vNumero, @IDTipoComprobante, @Comprobante, @Fecha, @FechaReparto, @IDDistribuidor, @IDCamion, @IDChofer, @IDZonaVenta, @TotalContado, @TotalCredito, @Total, @TotalImpuesto, @TotalDiscriminado, @TotalDescuento, @CantidadComprobantes, 'False', @Observacion)
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		
		--Cargamos el Detalle
		EXEC SpDetalleLoteDistribucion @IDTransaccion = @IDTransaccionSalida, @Operacion = 'INS',	@Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
					
		
		Return @@rowcount
		
	End
	
	If @Operacion = 'ANULAR' Begin
		
		--Si existe
		If Not Exists(Select * From LoteDistribucion Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si ya esta anulado
		If (Select Anulado From LoteDistribucion Where IDTransaccion=@IDTransaccion) = 'True' Begin
		
			set @Mensaje = 'El registro ya esta anulado!'
			set @Procesado = 'False'
			
			--Eliminar las relaciones
			Delete From VentaLoteDistribucion Where IDTransaccionLote=@IDTransaccion
			Delete From DetalleLoteDistribucion Where IDTransaccion=@IDTransaccion
		
			return @@rowcount
			
		End
		
		--El lote no debe pertenecer a ninguna rendición de lote
		If  Exists(Select * From RendicionLote Where IDTransaccionLote=@IDTransaccion and Anulado = 0) Begin
			set @Mensaje = 'El lote ya pertenece a alguna rendición de lote!'
			set @Procesado = 'False'
			return @@rowcount
		End
								
		--Anular
		Update LoteDistribucion Set Anulado='True' 								
		Where IDTransaccion=@IDTransaccion
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		--Eliminar las relaciones
		Delete From VentaLoteDistribucion Where IDTransaccionLote=@IDTransaccion
		Delete From DetalleLoteDistribucion Where IDTransaccion=@IDTransaccion
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		Return @@rowcount
		
	End
	
	If @Operacion = 'DEL' Begin
		
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'false'
		Return @@rowcount
		
	END
	
	--Actualizar Saldos de Ventas
	If @Operacion = 'UPD' Begin
	
		--Validar
		--Numero
		If NOT Exists(Select TOP 1 * From LoteDistribucion Where IDTransaccion = @IDTransaccion)  Begin
			set @Mensaje = 'No se encontró la transacción a modificar. Contacte con el administrador del sistema.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		END
		
		--Anulado
		If Exists(Select TOP 1 * From LoteDistribucion Where IDTransaccion = @IDTransaccion AND Anulado=1) Begin
			set @Mensaje = 'El registro está anulado y no puede modificarse'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		----Si ya existe el documento
		--If Exists(Select TOP 1 * From LoteDistribucion Where IDTipoComprobante=@IDTipoComprobante And Comprobante=@Comprobante And IDSucursal=@IDSucursal And Anulado='False') Begin
		--	set @Mensaje = 'El numero de comprobante ya existe!'
		--	set @Procesado = 'False'
		--	set @IDTransaccionSalida  = 0
		--	return @@rowcount
		--End
		
		----Insertar Transaccion
		--EXEC SpTransaccion
		--	@IDUsuario = @IDUsuario,
		--	@IDSucursal = @IDSucursal,
		--	@IDDeposito = @IDDeposito,
		--	@IDTerminal = @IDTerminal,
		--	@IDOperacion = @IDOperacion,
		--	@Mensaje = @Mensaje OUTPUT,
		--	@Procesado = @Procesado OUTPUT,
		--	@IDTransaccion = @IDTransaccionSalida OUTPUT

		--Actualizamos el registro
		UPDATE LoteDistribucion 
		SET IDSucursal=@IDSucursalOperacion
			,IDDistribuidor=@IDDistribuidor, IDCamion=@IDCamion, IDChofer=@IDChofer, IDZonaVenta=@IDZonaVenta
			, TotalContado=@TotalContado, TotalCredito=@TotalCredito, Total=@Total, TotalImpuesto=@TotalImpuesto
			, TotalDiscriminado=@TotalDiscriminado, TotalDescuento=@TotalDescuento, CantidadComprobantes=@CantidadComprobantes
			, Observacion=@Observacion
		WHERE IDTransaccion=@IDTransaccion
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		
		----Cargamos el Detalle
		--EXEC SpDetalleLoteDistribucion @IDTransaccion = @IDTransaccionSalida, @Operacion = 'INS',	@Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
					
		
		Return @@rowcount
		
	End
		
	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'
	
End
	



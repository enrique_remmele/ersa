﻿CREATE Procedure [dbo].[SpProductoListaPrecio]

	--Entrada
	@IDProducto int,
	@IDListaPrecio int,
	@IDMoneda tinyint = 1,
	@Precio money,
	@TPRPorcentaje decimal(10, 3) = NULL,
	@TPRDesde date = NULL,
	@TPRHasta date = NULL,
	 
	@Operacion varchar(10),
	
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	Declare @vTPR money
	Declare @vIDListaPrecio int
	Declare @vPrecioTemp money
	Declare @vListaPrecio varchar(100)
	
	---Validar Usuario
	Declare @vIDPerfil int
	
	Set @vIDPerfil = isnull((select idperfil from usuario where id = @IDUsuario),0)
	if not exists (select * from vAccesoEspecificoPerfil where Acceso = 'Modificar Precio' and IDPerfil = @vIDPerfil) begin
		set @Mensaje = 'Usuario sin acceso!'
		set @Procesado = 'False'
		return @@rowcount
	end

	If @TPRPorcentaje Is Not Null Begin
		If @Precio != 0 And @TPRPorcentaje != 0 Begin
			Set @vTPR = Round((@Precio * @TPRPorcentaje) / 100, 0, 0)						
		End
	End
	
	--INSERTAR
	If @Operacion='INS' Begin
	
		--Verificar que los ID´s ya no esten cargados
		If Exists(Select * From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio) Begin
			set @Operacion = 'UPD'
			goto Actualizar
		End
		
		--Monto mayor a 0
		If @Precio < 0 Begin
			set @Mensaje = 'El precio debe ser mayor a 0!'
			set @Procesado = 'False'
			return @@rowcount
		End
		if @IDSucursal is null begin
			set @Mensaje = 'Debe seleccionar IDSucursal!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		Delete from productoprecio where idproducto = @IDProducto and idtransaccionpedidopreciounico is null

		--Insertar
		
		Insert Into ProductoListaPrecio(IDProducto, IDListaPrecio,Precio,IDMoneda, IDSucursal, TPRPorcentaje, TPR, TPRDesde, TPRHasta)
		values(@IDProducto, @IDListaPrecio, @Precio, @IDMoneda, @IDSucursal, @TPRPorcentaje, @vTPR, @TPRDesde, @TPRHasta)

		--Auditoria
		Insert Into AProductoListaPrecio(IDProducto, IDListaPrecio,Precio,IDMoneda, IDSucursal, TPRPorcentaje, TPR, TPRDesde, TPRHasta,IDUsuario,FechaModificacion,Accion)
		values(@IDProducto, @IDListaPrecio, @Precio, @IDMoneda, @IDSucursal, @TPRPorcentaje, @vTPR, @TPRDesde, @TPRHasta,@IDUsuario,Getdate(),'ALT')
		
		set @Operacion = 'REPLICAR'
		--set @Mensaje = 'Registro guardado!'
		--set @Procesado = 'True'
		--return @@rowcount
	End
Actualizar:	
	--ACTUALIZAR
	If @Operacion='UPD' Begin
	
		--Verificar que los ID´s ya no esten cargados
		If Not Exists(Select * From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio) Begin
			set @Mensaje = 'El sistema no encontro el registro seleccionado! Favor de verificar la informacion y continuar.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Monto mayor a 0
		If @Precio < 0 Begin
			set @Mensaje = 'El precio debe ser mayor a 0!'
			set @Procesado = 'False'
			return @@rowcount
		End
		if @IDSucursal is null begin
			set @Mensaje = 'Debe seleccionar IDSucursal!'
			set @Procesado = 'False'
			return @@rowcount
		end

		Delete from productoprecio where idproducto = @IDProducto and idtransaccionpedidopreciounico is null

		--Auditoria
		Insert Into AProductoListaPrecio(IDProducto, IDListaPrecio,Precio,IDMoneda, IDSucursal, TPRPorcentaje, TPR, TPRDesde, TPRHasta,IDUsuario,FechaModificacion,Accion)
		values(@IDProducto, @IDListaPrecio, @Precio, @IDMoneda, @IDSucursal, @TPRPorcentaje, @vTPR, @TPRDesde, @TPRHasta,@IDUsuario,Getdate(),'MOD')
		
		--Eliminar las excepciones de precio si se modifica el precio
		Execute SpEliminarExcepciones @IDUsuario,@IDProducto,@IDListaPrecio

		--Actualizar
		Update ProductoListaPrecio Set Precio=@Precio,
										TPR=@vTPR,
										TPRPorcentaje=@TPRPorcentaje,
										TPRDesde=@TPRDesde ,
										TPRHasta=@TPRHasta,
										IDMoneda = @IDMOneda
		Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio And IDSucursal=@IDSucursal
		
		set @Operacion = 'REPLICAR'
		--set @Mensaje = 'Registro guardado!'
		--set @Procesado = 'True'
		--return @@rowcount
	End
	
	--ELIMINAR
	If @Operacion='DEL' Begin

		--Verificar que los ID´s ya no esten cargados
		If Not Exists(Select * From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio) Begin
			set @Mensaje = 'El sistema no encontro el registro seleccionado! Favor de verificar la informacion y continuar.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Auditoria
		set @IDMoneda = (select IDMoneda From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio And IDSucursal=@IDSucursal)
		set @TPRPorcentaje = (select TPRPorcentaje From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio And IDSucursal=@IDSucursal)
		set @vTPR = (select TPR From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio And IDSucursal=@IDSucursal)
		set @TPRDesde = (select TPRDesde From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio And IDSucursal=@IDSucursal)
		set @TPRHasta = (select TPRHasta From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio And IDSucursal=@IDSucursal)

		Insert Into AProductoListaPrecio(IDProducto, IDListaPrecio,Precio,IDMoneda, IDSucursal, TPRPorcentaje, TPR, TPRDesde, TPRHasta,IDUsuario,FechaModificacion,Accion)
		values(@IDProducto, @IDListaPrecio, @Precio, @IDMoneda, @IDSucursal, @TPRPorcentaje, @vTPR, @TPRDesde, @TPRHasta,@IDUsuario,Getdate(),'ELI')

		--Eliminar las excepciones de precio si se modifica el precio
		Execute SpEliminarExcepciones @IDUsuario,@IDProducto,@IDListaPrecio
		

		--Eliminar
		Delete From ProductoListaPrecio
		Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio And IDSucursal=@IDSucursal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount	
	End
		
	--PRECIO
	If @Operacion = 'PRECIO' Begin
		
		Delete from productoprecio where idproducto = @IDProducto and idtransaccionpedidopreciounico is null

		Declare db_cursorPrecio cursor for
		Select ID From ListaPrecio Where Estado='True' And IDSucursal=@IDSucursal And ID!=@IDListaPrecio
		Open db_cursorPrecio   
		Fetch Next From db_cursorPrecio Into @vIDListaPrecio
		While @@FETCH_STATUS = 0 Begin  
			
			--Si Existe, actualizamos
			If Exists(Select * From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDMoneda=@IDMoneda And IDSucursal=@IDSucursal) Begin
			
				Update ProductoListaPrecio Set Precio=@Precio
				Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDMoneda=@IDMoneda And IDSucursal=@IDSucursal
				
				Set @IDListaPrecio = @vIDListaPrecio
				Exec SpProductoListaPrecio
				@IDProducto=@IDProducto
				,@IDListaPrecio=@vIDListaPrecio
				,@IDMoneda=@IDMoneda
				,@Precio=@Precio
				,@TPRPorcentaje=@TPRPorcentaje
				,@Operacion='REPLICAR'
				,@IDUsuario=@IDUsuario
				,@IDSucursal=@IDSucursal
				,@IDDeposito=@IDDeposito
				,@IDTerminal=@IDTerminal
				,@Mensaje=''
				,@Procesado=''

			End
			
			--Si no existe la configuracion
			If Not Exists(Select * From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDMoneda=@IDMoneda And IDSucursal=@IDSucursal) Begin 
			
				Insert Into ProductoListaPrecio(IDProducto, IDListaPrecio, IDSucursal, Precio, IDMoneda)
				values(@IDProducto, @vIDListaPrecio, @IDSucursal, @Precio, @IDMoneda)

				Set @IDListaPrecio = @vIDListaPrecio
				Exec SpProductoListaPrecio
				@IDProducto=@IDProducto
				,@IDListaPrecio=@vIDListaPrecio
				,@IDMoneda=@IDMoneda
				,@Precio=@Precio
				,@TPRPorcentaje=@TPRPorcentaje
				,@Operacion='REPLICAR'
				,@IDUsuario=@IDUsuario
				,@IDSucursal=@IDSucursal
				,@IDDeposito=@IDDeposito
				,@IDTerminal=@IDTerminal
				,@Mensaje=''
				,@Procesado=''
				
			End
			
			
			Fetch Next From db_cursorPrecio Into @vIDListaPrecio
		End
		
		Close db_cursorPrecio   
		Deallocate db_cursorPrecio		
		
		--Set @Operacion = 'REPLICAR'
		
		
	End
	
	--TPR
	If @Operacion = 'TPR' Begin
		
		Declare db_cursor cursor for
		Select ID From ListaPrecio Where Estado='True' And IDSucursal=@IDSucursal And ID!=@IDListaPrecio
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDListaPrecio
		While @@FETCH_STATUS = 0 Begin  
			
			--Obtener el Precio
			Set @vPrecioTemp=(Select Precio From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDMoneda=@IDMoneda And IDSucursal=@IDSucursal)
			
			--Calculamos el TPR
			If @TPRPorcentaje Is Not Null Begin
				If @vPrecioTemp != 0 And @TPRPorcentaje != 0 Begin
					Set @vTPR = Round((@vPrecioTemp * @TPRPorcentaje) / 100, 0, 0)						
				End
			End
				
			--Si Existe, actualizamos
			If Exists(Select * From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDMoneda=@IDMoneda And IDSucursal=@IDSucursal) Begin
			
				Update ProductoListaPrecio Set  Precio=@vPrecioTemp,
										TPR=@vTPR,
										TPRPorcentaje=@TPRPorcentaje,
										TPRDesde=@TPRDesde ,
										TPRHasta=@TPRHasta
				Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDMoneda=@IDMoneda And IDSucursal=@IDSucursal
				
			End
			
			--Si no existe la configuracion
			If Not Exists(Select * From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDMoneda=@IDMoneda And IDSucursal=@IDSucursal) Begin
			
				Insert Into ProductoListaPrecio(IDProducto, IDListaPrecio, Precio, IDMoneda, IDSucursal, TPR, TPRPorcentaje, TPRDesde, TPRHasta)
				values(@IDProducto, @vIDListaPrecio, @vPrecioTemp, @IDMoneda, @IDSucursal, @vTPR, @TPRPorcentaje, @TPRDesde, @TPRHasta)
				
			End
			
			
			Fetch Next From db_cursor Into @vIDListaPrecio
		End
		
		Close db_cursor   
		Deallocate db_cursor		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount	
		
	End
	
	--replicar precio en sucursales
	If @Operacion = 'REPLICAR' begin
	  
	  declare @ListaPrecio varchar(100) = (select Descripcion from ListaPrecio where ID = @IDListaPrecio)
	  print 'entro a replicar'
	  Update ProductoListaPrecio
	  Set Precio = @Precio,
	  TPRHasta = @TPRHasta
	  where IDListaPrecio in (select ID from ListaPrecio where Descripcion = @ListaPrecio)
	  and IDProducto = @IDProducto

	  Declare @IDListaPrecioExcepcion int
	  Declare @IDSucursalExcepcion int
	  --Eliminar excepciones de todas las listas luego de replicar
	  Declare db_cursorExcepcion cursor for
		Select ID, IDSucursal
		from ListaPrecio 
		where Descripcion = @ListaPrecio
		and ID <> @IDListaPrecio
		Open db_cursorExcepcion   
		Fetch Next From db_cursorExcepcion Into @IDListaPrecioExcepcion, @IDSucursalExcepcion
		While @@FETCH_STATUS = 0 Begin  
			--Eliminar las excepciones de precio si se modifica el precio
		Execute SpEliminarExcepciones @IDUsuario,@IDProducto,@IDListaPrecioExcepcion
		print 'excepciones eliminadas'

		if not exists(select * from productolistaprecio where idproducto = @IDProducto And IDListaPrecio=@IDListaPrecioExcepcion And IDSucursal=@IDSucursalExcepcion) begin
			Insert Into ProductoListaPrecio(IDProducto, IDListaPrecio,Precio,IDMoneda, IDSucursal, TPRPorcentaje, TPR, TPRDesde, TPRHasta)
			values(@IDProducto, @IDListaPrecioExcepcion, @Precio, @IDMoneda, @IDSucursalExcepcion, @TPRPorcentaje, @vTPR, @TPRDesde, @TPRHasta)

			--Auditoria
			Insert Into AProductoListaPrecio(IDProducto, IDListaPrecio,Precio,IDMoneda, IDSucursal, TPRPorcentaje, TPR, TPRDesde, TPRHasta,IDUsuario,FechaModificacion,Accion)
			values(@IDProducto, @IDListaPrecioExcepcion, @Precio, @IDMoneda, @IDSucursalExcepcion, @TPRPorcentaje, @vTPR, @TPRDesde, @TPRHasta,@IDUsuario,Getdate(),'ALT')
		
		end
		else  begin

			Update ProductoListaPrecio Set Precio=@Precio,
											TPR=@vTPR,
											TPRPorcentaje=@TPRPorcentaje,
											TPRDesde=@TPRDesde ,
											TPRHasta=@TPRHasta,
											IDMoneda = @IDMOneda
			Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecioExcepcion And IDSucursal=@IDSucursalExcepcion
		end

		set @Mensaje= 'Se Actualizo1'	
			Fetch Next From db_cursorExcepcion Into @IDListaPrecioExcepcion,@IDSucursalExcepcion
		End
		
		Close db_cursorExcepcion   
		Deallocate db_cursorExcepcion		

	  set @Mensaje = 'Registro guardado!'
	  set @Procesado = 'True'
	  return @@rowcount	
	End


	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount
		
End







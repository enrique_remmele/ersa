﻿
CREATE Procedure [dbo].[SpAsientoCanjeRecaudacion]

	@IDTransaccion numeric(18,0)
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint =0

	--Banco
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vObservacion varchar(100)
	Declare @vFecha date
	Declare @vIDCuentaBancaria int
	Declare @vCuentaBancaria varchar(50)
	Declare @vTipoCuenta bit = 'False'
	Declare @vIDMoneda int
	Declare @vCotizacion money

	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	
	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vIDTipoProducto int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money=0
	Declare @vImporteHaber money=0

	Declare @vIDTipoComprobanteEfectivo int
	Declare @vDiferido bit
	Declare @vIDTipoComprobanteDocumento int



	Declare @vCliente as varchar(32)
	Declare @vIDCliente as int
	
	--Obtener valores
	Begin
		
		Select	@vIDSucursal=IDSucursal,
				@vFecha=Fecha,
				@vComprobante = Numero,
				@vObservacion = Observacion,
				@vIDMoneda = IDMoneda,
				@vCotizacion = Cotizacion
		From vCanjeRecaudacion
		Where IDTransaccion=@IDTransaccion
		

	End
					
	--Verificar que el asiento se pueda modificar
	Begin
	
		--Si esta conciliado
		If not exists(Select * From CanjeRecaudacion Where IDTransaccion=@IDTransaccion) Begin
			print 'El asiento es un canje de recaudacion'
			GoTo salir
		End 	

		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
			
		--Si esta anulado
		If (Select Anulado From vMacheoFacturaTicket Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'La venta esta anulada'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		
	End
				
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = @vObservacion
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
					Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, 0, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	
	--detalle de canje
	begin
		Declare @TipoPago as varchar(50)
		Declare @ImportePago as money
		Declare @CotizacionFormaPago as money
		Declare cCFFormaPago cursor for
		Select Tipo, Importe, Cotizacion From VDetalleCanjeRecaudacion
		Where IDTransaccion = @IDTransaccion
		Open cCFFormaPago   
		fetch next from cCFFormaPago into @TipoPago, @ImportePago, @CotizacionFormaPago
		
		While @@FETCH_STATUS = 0 Begin  

				If @TipoPago = 'EFECTIVO' begin
					print concat('Efectivo ', @vIDSucursal, ' ', @vIDMoneda, ' ', @vTipoCuenta)
					Set @vCodigo = (select cuentacontable from VCFDepositoBancario where Efectivo = 1 and IDSucursal= @vIDSucursal and IDMoneda=@vIDMoneda and TipoCuenta = @vTipoCuenta and Haber = 1)
                End

                If @TipoPago = 'CHEQUE AL DIA' Begin
					Set @vCodigo = (select cuentacontable from VCFDepositoBancario where ChequeALDia = 1 and IDSucursal= @vIDSucursal and IDMoneda=@vIDMoneda and TipoCuenta = @vTipoCuenta and Haber = 1)
                End 

                If @TipoPago = 'CHEQUE DIFERIDO' Begin
					Set @vCodigo = (select cuentacontable from VCFDepositoBancario where ChequeDiferido = 1 and IDSucursal= @vIDSucursal and IDMoneda=@vIDMoneda and TipoCuenta = @vTipoCuenta and Haber = 1)
                End 

                If @TipoPago = 'CHEQUE RECHAZADO' Begin
					Set @vCodigo = (select cuentacontable from VCFDepositoBancario where ChequeRechazado = 1 and IDSucursal= @vIDSucursal and IDMoneda=@vIDMoneda and TipoCuenta = @vTipoCuenta and Haber = 1)
                End 

                If @TipoPago = 'DIFERENCIACAMBIO' Begin
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
					Set @vCodigo = (select cuentacontable from VCFDepositoBancario where DiferenciaCambio = 1 and IDSucursal= @vIDSucursal and IDMoneda=@vIDMoneda )
					Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
					print @vCodigo
						print @vIDCuentaContable
					if @ImportePago > 0 begin
						Set @vImporteDebe = @ImportePago --* @CotizacionFormaPago
					end

					if @ImportePago < 0 begin
						Set @vImporteHaber = @ImportePago * -1
						print @vImporteHaber
					end

					If (@vImporteDebe > 0)  or  (@vImporteHaber > 0) Begin
						
						If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
							print 'insert diferencia cambio'
						print @vCodigo
						print @vIDCuentaContable	
							Update DetalleAsiento Set Debito=Debito+Round(@vImporteDebe,@vRedondeo),
														Credito=Credito+Round(@vImporteHaber,@vRedondeo)
							Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
						End Else Begin				
						
							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo),  @vImporteDebe, 0, '')
						End
					end
					
					goto seguir
                End 
				If @TipoPago = 'DOCUMENTO' Begin
					Set @vCodigo = (select cuentacontable from VCFDepositoBancario where Documento = 1 and IDSucursal= @vIDSucursal and IDMoneda=@vIDMoneda and TipoCuenta = @vTipoCuenta and Haber = 1)
                End 
				
				Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				Set @vImporte = @ImportePago * @CotizacionFormaPago
				print @TipoPago

				If @vImporte > 0 Begin
					If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
						Update DetalleAsiento Set Credito=Credito+Round(@vImporte,@vRedondeo),
													Importe=Importe+Round(@vImporte,@vRedondeo)
						Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
					End Else Begin				
						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporte,@vRedondeo),  0, 0, '')
					End
				end
				seguir:
				
			fetch next from cCFFormaPago into @TipoPago, @ImportePago, @CotizacionFormaPago
			
		End
		
		close cCFFormaPago 
		deallocate cCFFormaPago
	End

--------------------------------
--Efectivo
	Begin
		
		--Variables
		Declare cCFEfectivo cursor for
		Select Codigo, Debe, Haber, IDTipoComprobante From VCFCobranzaEfectivo
		Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFEfectivo   
		fetch next from cCFEfectivo into @vCodigo, @vDebe, @vHaber, @vIDTipoComprobanteEfectivo
		
		While @@FETCH_STATUS = 0 Begin  
			print @vIDTipoComprobanteEfectivo
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Hayar el importe
			Set @vImporte = (Select SUM(FP.Importe) From FormaPago FP 
													Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
							Where FP.IDTransaccion=@IDTransaccion 
							And E.IDTipoComprobante=@vIDTipoComprobanteEfectivo)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFEfectivo into @vCodigo, @vDebe, @vHaber, @vIDTipoComprobanteEfectivo
			
		End
		
		close cCFEfectivo 
		deallocate cCFEfectivo
		
	End
	
	--Cheque
	Begin
		
		--Variables
		Declare cCFCheque cursor for
		Select Codigo, Debe, Haber, Diferido, IDCuentaBancaria From VCFCobranzaCheque
		Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFCheque 
		fetch next from cCFCheque into @vCodigo, @vDebe, @vHaber, @vDiferido, @vIDCuentaBancaria
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Hayar el importe
			Set @vImporte = IsNull((Select SUM(FP.Importe) From FormaPago FP 
															Join ChequeCliente CQH On FP.IDTransaccionCheque=CQH.IDTransaccion 
							Where FP.IDTransaccion=@IDTransaccion 
							And CQH.Diferido=@vDiferido),0)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFCheque into @vCodigo, @vDebe, @vHaber, @vDiferido, @vIDCuentaBancaria
			
		End
		
		close cCFCheque 
		deallocate cCFCheque
		
	End
	
	--Documentos
	Begin
		
		--Variables
		Declare cCFDocumento cursor for
		Select Codigo, Debe, Haber, IDTipoComprobante From VCFCobranzaDocumento
		Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		--Where IDMoneda=@vIDMoneda
		Open cCFDocumento 
		fetch next from cCFDocumento into @vCodigo, @vDebe, @vHaber, @vIDTipoComprobanteDocumento
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Hayar el importe
			Set @vImporte = IsNull((Select SUM(FP.Importe) From FormaPago FP 
														Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID 
							Where FP.IDTransaccion=@IDTransaccion 
							And FPD.IDTipoComprobante=@vIDTipoComprobanteDocumento),0)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFDocumento into @vCodigo, @vDebe, @vHaber, @vIDTipoComprobanteDocumento
			
		End
		
		close cCFDocumento 
		deallocate cCFDocumento
		
	End

--------------------------------	
	--Actualizamos la cabecera, el total
	Begin
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
		Set @vImporteHaber = ROUND(@vImporteHaber, @vRedondeo)
		Set @vImporteDebe = ROUND(@vImporteDebe, @vRedondeo)
	
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vImporteHaber - @vImporteDebe
		Where IDTransaccion=@IDTransaccion
	End

	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
	
End



﻿ CREATE Procedure [dbo].[SpNotaCreditoAplicacionProcesar]

	--Entrada
	@IDTransaccion numeric(18, 0),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	--Variable
	Declare @vIDTransaccionVenta numeric(18,0)
	Declare @vIDTransaccionNotaCredito numeric (18,0)
	Declare @vDescontado money
	Declare @vSaldo money
	Declare @vSaldoNotaCredito money
	Declare @vTotalNotaCredito money
	Declare @vImporte money
	Declare @vCancelado bit
	Declare @vAplicar bit
	
	Set @Mensaje = ''
	Set @Procesado = 'False'
	
	If @Operacion = 'INS' Begin
				
		Declare db_cursor cursor for
		Select IDTransaccionVenta, Importe From NotaCreditoVentaAplicada
		Where IDTransaccionVenta Is Not Null And IDTransaccionNotaCreditoAplicacion=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVenta, @vImporte
		While @@FETCH_STATUS = 0 Begin 
		 
			--Hayar Valores
			--Saldo
			Set @vSaldo = (Select Saldo From Venta Where IDTransaccion=@vIDTransaccionVenta)
			Set @vSaldo = @vSaldo - @vImporte
			
			--Cancelado
			Set @vCancelado = 'False'
			
			If @vSaldo < 1 Begin
				Set @vCancelado = 'True'
			End
			
			--Descontado
			Set @vDescontado = (Select Descontado From Venta Where IDTransaccion=@vIDTransaccionVenta)
			Set @vDescontado = @vDescontado + @vImporte
			
			--Actualizar Venta
			Update Venta Set Saldo=@vSaldo,
							Cancelado = @vCancelado,
							Descontado=@vDescontado
			Where IDTransaccion=@vIDTransaccionVenta
			
			Fetch Next From db_cursor Into @vIDTransaccionVenta, @vImporte
										
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
		--Saldo nota credito
		Set @vIDTransaccionNotaCredito = (Select Min(IDTransaccionNotaCredito) From NotaCreditoVentaAplicada Where IDTransaccionNotaCreditoAplicacion=@IDTransaccion)
		Set @vSaldoNotaCredito  = (Select Saldo From NotaCredito Where IDTransaccion=@vIDTransaccionNotaCredito)
		
		set @vTotalNotaCredito = (Select Total From NotaCreditoAplicacion Where IDTransaccion=@IDTransaccion)
		set @vSaldoNotaCredito = @vSaldoNotaCredito - @vTotalNotaCredito
				--Actualizar Saldo de Nota de Credito	
		Update NotaCredito Set Saldo = @vSaldoNotaCredito
		Where IDTransaccion = @vIDTransaccionNotaCredito
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
		
	If @Operacion = 'ANULAR' Begin
		
		Declare db_cursor cursor for
		Select IDTransaccionVenta, Importe From NotaCreditoVentaAplicada 
		Where IDTransaccionVenta Is Not Null And IDTransaccionNotaCreditoAplicacion=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVenta, @vImporte
		While @@FETCH_STATUS = 0 Begin 
		 
			--Hayar Valores
			--Saldo
			Set @vSaldo = (Select Saldo From Venta Where IDTransaccion=@vIDTransaccionVenta)
			Set @vSaldo = @vSaldo + @vImporte
			
			--Cancelado
			Set @vCancelado = 'False'
			
			--Descontado
			Set @vDescontado = (Select Descontado From Venta Where IDTransaccion=@vIDTransaccionVenta)
			Set @vDescontado = @vDescontado - @vImporte
			
			--Actualizar Venta
			Update Venta Set Saldo=@vSaldo,
							Cancelado = @vCancelado,
							Descontado=@vDescontado							
			Where IDTransaccion=@vIDTransaccionVenta
			
			Fetch Next From db_cursor Into @vIDTransaccionVenta, @vImporte
										
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
		
			--Saldo nota credito
		Set @vIDTransaccionNotaCredito = (Select Min(IDTransaccionNotaCredito) From NotaCreditoVentaAplicada Where IDTransaccionNotaCreditoAplicacion=@IDTransaccion)
		Set @vSaldoNotaCredito  = (Select Saldo From NotaCredito Where IDTransaccion=@vIDTransaccionNotaCredito)
		
		set @vTotalNotaCredito = (Select Total From NotaCreditoAplicacion Where IDTransaccion=@IDTransaccion)
		set @vSaldoNotaCredito = @vSaldoNotaCredito + @vTotalNotaCredito
				
		 --Actualizar Saldo de Nota de Credito	
		Update NotaCredito Set Saldo = @vSaldoNotaCredito
		Where IDTransaccion = @vIDTransaccionNotaCredito
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		
	End
	
	If @Operacion = 'ELIMINAR' Begin
	
		If (Select Anulado From NotaCreditoAplicacion  Where IDTransaccion=@IDTransaccion) = 'True' Begin
			Set @Mensaje = 'El Registro Debe estar Anulado '
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		Delete DocumentoAnulado where IDTransaccion = @IDTransaccion
	    Delete NotaCreditoVentaAplicada Where IDTransaccionNotaCreditoAplicacion =@IDTransaccion 
		Delete Transaccion Where ID = @IDTransaccion
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		
	End
	
End
	






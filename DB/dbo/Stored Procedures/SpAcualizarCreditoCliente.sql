﻿CREATE Procedure [dbo].[SpAcualizarCreditoCliente]

	--Entrada
	@IDCliente int,
	--Credito
	@LimiteCredito money,
	@Descuento money,
	@PlazoCredito integer,
	@PlazoCobro integer,
	@PlazoChequeDiferido integer,
	@Boleta bit = 'False',
	@Contado bit = 'True',
	@Credito bit = 'False	',

	--Auditoria
	@IDUsuario smallint,
	@IDTerminal smallint,

	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin

	--Si es que no existe el Cliente
	If Not Exists(Select * From Cliente Where ID=@IDCliente) Begin
		Set @Mensaje = 'El sistema no encuentra el registro!'
		Set @Procesado = 'False'
		return @@rowcount
	End

	--Actualizar la ultima compra
	Update Cliente Set	LimiteCredito=@LimiteCredito,
						Descuento=@Descuento,
						PlazoCobro=@PlazoCobro,
						PlazoCredito=@PlazoCredito,
						PlazoChequeDiferido=@PlazoChequeDiferido,
						Boleta = @Boleta,
						Contado=@Contado,
						Credito=@Credito
							
	Where ID=@IDCliente
			
	set @Mensaje = 'Registro guardado'
	set @Procesado = 'True'
	return @@rowcount
		

End
	
	


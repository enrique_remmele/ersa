﻿CREATE Procedure [dbo].[SpDetalleRendicionLoteCheque]

	--Entrada
	@IDTransaccion numeric(18,0),
	@ID tinyint = NULL,
	@Banco varchar(50) = NULL,
	@NroCheque varchar(50) = NULL,
	@Fecha date = NULL,
	@FechaVencimiento date = NULL,
	@IDTransaccionVenta numeric(18,0) = NULL,
	@Importe money = NULL,
	@Diferido bit = NULL,
	@BancoLocal bit = NULL,
	
	--Operacion
	@Operacion varchar(10),
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
As

Begin
	
	--Procesar Saldos de Ventas
	If @Operacion = 'INS' Begin
	
		--Validar
		--Asegurarse que exista la transaccion
		if Not Exists(Select * From RendicionLote Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra el registro!'
			Set @Procesado = 'False'
			Return @@rowcount
		End
		
		--si es que ya existe el detalle, eliminar primeramente
		--If Exists(Select * From DetalleRendicionLoteCheque Where IDTransaccion=@IDTransaccion) Begin
		--	Delete From DetalleRendicionLoteCheque Where IDTransaccion=@IDTransaccion 	
		--End
		
		--Insertar
		Insert Into DetalleRendicionLoteCheque(IDTransaccion, ID, Banco, NroCheque, Fecha, FechaVencimiento, IDTransaccionVenta, Importe, Diferido, BancoLocal)
		Values(@IDTransaccion, @ID, @Banco, @NroCheque, @Fecha, @FechaVencimiento, @IDTransaccionVenta, @Importe, @Diferido, @BancoLocal)
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		
		Return @@rowcount
		
	End
	
	If @Operacion = 'ANULAR' Begin
		
		--Validar
		--Que exista el registro
		if Not Exists(Select * From RendicionLote Where IDTransaccionLote=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra el registro!'
			Set @Procesado = 'False'
			Return @@rowcount
		End
	
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		Return @@rowcount
		
	End
	
	If @Operacion = 'DEL' Begin
		
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'false'
		Return @@rowcount
		
	End
		
	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'
	
End




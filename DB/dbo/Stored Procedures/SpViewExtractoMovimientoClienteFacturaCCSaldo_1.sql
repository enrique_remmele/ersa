﻿CREATE Procedure [dbo].[SpViewExtractoMovimientoClienteFacturaCCSaldo]

	--Identificadores
	@FechaCobro Date,
	@CuentaContable varchar(20) = '',
	@IDCliente int = 0
	
As

Begin

Select 
C.IDTransaccion, 
C.Operacion,
C.Codigo,
--'Cliente'=Concat(P.Ruc , '-', P.RazonSocial),
C.Fecha,
C.Documento,
C.[Detalle/Concepto],
'Debito'=Sum(Isnull(C.Debito,0)),
'Credito'=Sum(Isnull(D.Credito,0)),
'Saldo'=isnull(C.Debito,0)-Sum(Isnull(D.Credito,0)),
C.Cotizacion,
'DebitoGs'=Isnull(C.Debito,0) * C.Cotizacion,
'SaldoGs'=(C.Debito-Sum(Isnull(D.Credito,0)))*C.Cotizacion,
C.SaldoSistema,
C.FacturaCanceladaSistema,
C.IDMoneda,
C.Movimiento,
C.ComprobanteAsociado,
--'FechaPago'=D.Fecha,
C.CodigoCC
from VExtractoMovimientoClienteFacturaCCDebito C
--join Cliente P on C.Codigo = P.ID
left outer join VExtractoMovimientoClienteFacturaCredito D ON C.IDTransaccion = D.ComprobanteAsociado and Isnull(D.Fecha,GetDate()) <=@FechaCobro
--where Isnull(D.Fecha,C.Fecha) <=@FechaCobro
where Isnull(C.Fecha,GetDate()) <=@FechaCobro
and (Case when @CuentaContable = '' then '' else C.CodigoCC end) = (Case when @CuentaContable = '' then '' else @CuentaContable end)
and (Case when @IDCliente = 0 then 0 else C.Codigo end) = (Case when @IDCliente = 0 then 0 else @IDCliente end)
Group by C.IDTransaccion, 
C.Operacion,
C.Codigo,
C.Fecha,
C.Documento,
C.[Detalle/Concepto],
C.Debito,
C.IDMoneda,
C.Movimiento,
C.ComprobanteAsociado,
C.CodigoCC,
C.SaldoSistema,
C.FacturaCanceladaSistema,
--P.Ruc,
--P.RazonSocial,
C.Cotizacion
having isnull(C.Debito,0)-Sum(Isnull(D.Credito,0)) > 0
order by C.Fecha
		
	
End




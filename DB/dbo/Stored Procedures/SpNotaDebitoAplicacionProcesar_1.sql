﻿
 CREATE Procedure [dbo].[SpNotaDebitoAplicacionProcesar]

	--Entrada
	@IDTransaccion numeric(18, 0),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	--Variable
	Declare @vIDTransaccionVenta numeric(18,0)
	Declare @vIDTransaccionNotaDebito numeric (18,0)
	Declare @vAcreditado money
	Declare @vSaldo money
	Declare @vSaldoNotaDebito money
	Declare @vTotalNotaDebito money
	Declare @vImporte money
	Declare @vCancelado bit
	Declare @vAplicar bit
	
	Set @Mensaje = ''
	Set @Procesado = 'False'
	
	If @Operacion = 'INS' Begin
				
		Declare db_cursor cursor for
		Select IDTransaccionVenta, Importe From NotadebitoVentaAplicada
		Where IDTransaccionVenta Is Not Null And IDTransaccionNotaDebitoAplicacion=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVenta, @vImporte
		While @@FETCH_STATUS = 0 Begin 
		 
			--Hayar Valores
			--Saldo
			Set @vSaldo = (Select Saldo From Venta Where IDTransaccion=@vIDTransaccionVenta)
			Set @vSaldo = @vSaldo + @vImporte
			
			--Cancelado
			Set @vCancelado = 'False'
			
			If @vSaldo = 0 Begin
				Set @vCancelado = 'True'
			End
			
			--Descontado
			Set @vAcreditado = (Select Acreditado  From Venta Where IDTransaccion=@vIDTransaccionVenta)
			Set @vAcreditado = @vAcreditado + @vImporte
			
			--Actualizar Venta
			Update Venta Set Saldo=@vSaldo,
							Cancelado = @vCancelado,
							Acreditado = @vAcreditado 
			Where IDTransaccion=@vIDTransaccionVenta
			
			Fetch Next From db_cursor Into @vIDTransaccionVenta, @vImporte
										
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
		--Saldo nota debito
		Set @vIDTransaccionNotaDebito   = (Select Min(IDTransaccionNotaDebito) From NotaDebitoVentaAplicada Where IDTransaccionNotaDebitoAplicacion=@IDTransaccion)
		Set @vSaldoNotaDebito   = (Select Saldo From NotaDebito Where IDTransaccion=@vIDTransaccionNotaDebito)
		
		set @vTotalNotaDebito = (Select Total From NotaDebitoAplicacion Where IDTransaccion=@IDTransaccion)
		set @vSaldoNotaDebito = @vSaldoNotaDebito - @vTotalNotaDebito 
		
		--Actualizar Saldo de Nota de debito	
		Update NotaDebito  Set Saldo = @vSaldoNotaDebito 
		Where IDTransaccion = @vIDTransaccionNotaDebito 
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
		
	If @Operacion = 'ANULAR' Begin
		
		Declare db_cursor cursor for
		Select IDTransaccionVenta, Importe From NotaDebitoVentaAplicada 
		Where IDTransaccionVenta Is Not Null And IDTransaccionNotaDebitoAplicacion=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVenta, @vImporte
		While @@FETCH_STATUS = 0 Begin 
		 
			--Hayar Valores
			--Saldo
			Set @vSaldo = (Select Saldo From Venta Where IDTransaccion=@vIDTransaccionVenta)
			Set @vSaldo = @vSaldo - @vImporte
			
			--Cancelado
			Set @vCancelado = 'False'
			
			--Descontado
			Set @vAcreditado = (Select Acreditado From Venta Where IDTransaccion=@vIDTransaccionVenta)
			Set @vAcreditado = @vAcreditado - @vImporte
			
			--Actualizar Venta
			Update Venta Set Saldo=@vSaldo,
							Cancelado = @vCancelado,
							Acreditado=@vAcreditado 							
			Where IDTransaccion=@vIDTransaccionVenta
			
			Fetch Next From db_cursor Into @vIDTransaccionVenta, @vImporte
										
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
		
			--Saldo nota debito
		Set @vIDTransaccionNotaDebito  = (Select Min(IDTransaccionNotaDebito) From NotaDebitoVentaAplicada Where IDTransaccionNotaDebitoAplicacion=@IDTransaccion)
		Set @vSaldoNotaDebito  = (Select Saldo From NotaDebito Where IDTransaccion=@vIDTransaccionNotaDebito)
		
		set @vTotalNotaDebito = (Select Total From NotadebitoAplicacion Where IDTransaccion=@IDTransaccion)
		set @vSaldoNotaDebito = @vSaldoNotaDebito + @vTotalNotaDebito 
				
		 --Actualizar Saldo de Nota de Debito
		Update NotaDebito  Set Saldo = @vSaldoNotaDebito 
		Where IDTransaccion = @vIDTransaccionNotaDebito 
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		
	End
	
	If @Operacion = 'ELIMINAR' Begin
	
		If (Select Anulado From NotaDebitoAplicacion   Where IDTransaccion=@IDTransaccion) = 'True' Begin
			Set @Mensaje = 'Registro guardado'
			Set @Procesado = 'True'
			return @@rowcount
		End
		
	    Declare db_cursor cursor for
		Select IDTransaccionVenta, Importe From NotaDebitoVentaAplicada 
		Where IDTransaccionVenta Is Not Null And IDTransaccionNotaDebitoAplicacion=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVenta, @vImporte
		While @@FETCH_STATUS = 0 Begin 
		 
			--Hayar Valores
			--Saldo
			Set @vSaldo = (Select Saldo From Venta Where IDTransaccion=@vIDTransaccionVenta)
			Set @vSaldo = @vSaldo - @vImporte
			
			--Cancelado
			Set @vCancelado = 'False'
		
			--Descontado
			Set @vAcreditado  = (Select Acreditado From Venta Where IDTransaccion=@vIDTransaccionVenta)
			Set @vAcreditado = @vAcreditado - @vImporte
			
			--Actualizar Venta
			Update Venta Set Saldo=@vSaldo,
							Cancelado = @vCancelado,
							Acreditado=@vAcreditado 
			Where IDTransaccion=@vIDTransaccionVenta
			
			Fetch Next From db_cursor Into @vIDTransaccionVenta, @vImporte
										
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
		--Saldo nota debito
		Set @vIDTransaccionNotadebito = (Select Min(IDTransaccionNotadebito) From NotaDebitoVentaAplicada Where IDTransaccionNotaDebitoAplicacion=@IDTransaccion)
		Set @vAcreditado  = (Select Saldo From NotaDebito Where IDTransaccion=@vIDTransaccionNotaDebito)
		
		set @vTotalNotaDebito = (Select Total From NotaDebitoAplicacion  Where IDTransaccion=@IDTransaccion)
		set @vSaldoNotaDebito = @vSaldoNotaDebito + @vTotalNotaDebito 
				
		--Actualizar Saldo de Nota de debito	
		Update NotaDebito  Set Saldo = @vSaldoNotaDebito 
		Where IDTransaccion = @vIDTransaccionNotaDebito 
	
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		
	End
	
End
	







﻿CREATE Procedure [dbo].[SpPrecioProducto3]

	--Entrada
	@IDProducto int,
	@IDCliente int,
	@IDListaPrecio smallint = 0,
	@IDSucursal tinyint,
	@IDClienteSucursal tinyint=0,
	@IDDeposito tinyint = 0,
	@ControlarReserva bit = 'False',
	@IDMoneda Tinyint,
	@Cotizacion decimal(18,2) 
	
As

Begin

	--Variables
	Declare @vIDListaPrecio int
	Declare @vExistencia decimal(10,2)
	
	Declare @vFecha date
	
	Set @vFecha = getdate() --(Select Top(1) VentaFechaFacturacion From Configuraciones Where IDSucursal=@IDSucursal)
	
	--Si la lista de precio no se envia por parametros
	If @IDListaPrecio = 0 Begin	
		--Primero Hayamos el Precio			
		--Obtener la lista de precio del cliente
		--Si no se selecciono una sucursal
		If @IDClienteSucursal = 0 Begin
			Set @vIDListaPrecio = (Select IsNull((Select IDListaPrecio From Cliente Where ID=@IDCliente), 0))
		End
	
		--Si es por sucursal del cliente
		If @IDClienteSucursal > 0 Begin
			Set @vIDListaPrecio = (Select IsNull((Select IDListaPrecio From ClienteSucursal Where IDCliente=@IDCliente And ID=@IDClienteSucursal), 0))
		End
	End Else Begin
		Set @vIDListaPrecio = @IDListaPrecio
	End

	--Aprovechamos y sacamos la existencia, para la venta y pedido
	--y asi no consultamos tantas veces a la BD desde la aplicacion
	Set @vExistencia = dbo.FExistenciaProducto(@IDProducto, @IDDeposito)
	
	If @ControlarReserva = 'True' Begin
		Set @vExistencia = @vExistencia - ISNULL((dbo.FExistenciaProductoReservado(@IDProducto, @IDDeposito)), 0)
	End
	
	--ATENCION!!! - Saque la sucursal para poder vender a clientes de otra sucursal	
	--Este es para Precio Normal	
	-- Cuando el precio esta en la misma moneda que la operacion
	IF (Select IDMoneda from ProductoListaPrecio where IDProducto = @IDProducto and IDListaPrecio = @IDListaPrecio) = @IDMoneda Begin
		Select 'IDTipo'=-1, 'Tipo'='Precio', 'TipoDescuento'='', 'IDActividad'=NULL, 'Importe'=P.Precio, 'Porcentaje'=0, 'Existencia'=@vExistencia, 'Tactico'='False',IDMoneda
		From ProductoListaPrecio P
		Join ListaPrecio LP On P.IDListaPrecio=LP.ID
		Where P.IDProducto=@IDProducto 
		And P.IDListaPrecio=@vIDListaPrecio 
		--And LP.IDSucursal=@IDSucursal 
	
		Union All
	
		--Este es para TPR	
		Select 'IDTipoDescuento'=0, 'Tipo'='TPR', 'T. Desc.'='TPR', 'IDActividad'=NULL, 'Importe'=IsNull(P.TPR,0), 'Porcentaje'=IsNull(P.TPRPorcentaje,0), 'Existencia'=@vExistencia, 'Tactico'='False', IDMoneda
		From ProductoListaPrecio P
		Join ListaPrecio LP On P.IDListaPrecio=LP.ID
		Where P.IDProducto=@IDProducto 
		And P.IDListaPrecio=@vIDListaPrecio 
		--And LP.IDSucursal=@IDSucursal 
		And (@vFecha Between IsNull(P.TPRDesde, DateAdd(d,-1, @vFecha)) And IsNull(P.TPRHasta, DateAdd(d,1, @vFecha)))
	
		Union all
	
		--Descuentos normales
		Select PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, PLPE.Descuento, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='False' , IDMoneda
		From ProductoListaPrecioExcepciones PLPE 
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDCliente=@IDCliente  
		--And IDSucursal = @IDSucursal 
		And TD.PlanillaTactico = 'False'
		And (@vFecha Between Desde And Hasta)
	
		Union All
	
		--Excepciones TACTICO
		Select PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, PLPE.Descuento, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='True' , IDMoneda
		From ProductoListaPrecioExcepciones PLPE 
		Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto And PLPE.IDListaPrecio=@vIDListaPrecio And PLPE.IDCliente=@IDCliente  
		--And LP.IDSucursal = @IDSucursal 
		And @vFecha between PLPE.Desde And PLPE.Hasta
		And TD.PlanillaTactico = 'True'
		And (Case When Exists(Select * From VDetalleActividad D 
											Join VActividad A On D.IDActividad=A.ID 
											Join DetallePlanillaDescuentoTactico DP On A.ID=DP.IDActividad 
											Join PlanillaDescuentoTactico P On DP.IDTransaccion=P.IDTransaccion 
											Where D.ID=@IDProducto And (@vFecha between P.Desde And P.Hasta) And P.IDSucursal=@IDSucursal) Then 'True' Else 'False' End)='True'
    End -- Fin de la misma moneda
-- precio GS a FACT otras monedas
--ATENCION!!! - Saque la sucursal para poder vender a clientes de otra sucursal	
	IF (@IDMoneda <> 1) and 
	(Select IDMoneda from ProductoListaPrecio where IDProducto = @IDProducto and IDListaPrecio = @IDListaPrecio) = 1 Begin
		--Este es para Precio Normal	
		Select 'IDTipo'=-1, 'Tipo'='Precio', 'TipoDescuento'='', 'IDActividad'=NULL, 
			   'Importe'=P.Precio / @Cotizacion, 'Porcentaje'=0, 'Existencia'=@vExistencia, 'Tactico'='False', IDMoneda
		From ProductoListaPrecio P
		Join ListaPrecio LP On P.IDListaPrecio=LP.ID
		Where P.IDProducto=@IDProducto 
		And P.IDListaPrecio=@vIDListaPrecio 
		--And LP.IDSucursal=@IDSucursal 
	
		Union All
	
		--Este es para TPR	
		Select 'IDTipoDescuento'=0, 'Tipo'='TPR', 'T. Desc.'='TPR', 'IDActividad'=NULL, 'Importe'=IsNull(P.TPR / @Cotizacion,0), 'Porcentaje'=IsNull(P.TPRPorcentaje,0), 'Existencia'=@vExistencia, 'Tactico'='False' , IDMoneda
		From ProductoListaPrecio P
		Join ListaPrecio LP On P.IDListaPrecio=LP.ID
		Where P.IDProducto=@IDProducto 
		And P.IDListaPrecio=@vIDListaPrecio 
		--And LP.IDSucursal=@IDSucursal 
		And (@vFecha Between IsNull(P.TPRDesde, DateAdd(d,-1, @vFecha)) And IsNull(P.TPRHasta, DateAdd(d,1, @vFecha)))
	
		Union all
	
		--Descuentos normales 
		Select PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, PLPE.Descuento / @Cotizacion, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='False' , IDMoneda
		From ProductoListaPrecioExcepciones PLPE 
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDCliente=@IDCliente  
		--And IDSucursal = @IDSucursal 
		And TD.PlanillaTactico = 'False'
		And (@vFecha Between Desde And Hasta)
	
		Union All
	
		--Excepciones TACTICO
		Select PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, PLPE.Descuento / @Cotizacion, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='True' , IDMoneda
		From ProductoListaPrecioExcepciones PLPE 
		Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto And PLPE.IDListaPrecio=@vIDListaPrecio And PLPE.IDCliente=@IDCliente  
		--And LP.IDSucursal = @IDSucursal 
		And @vFecha between PLPE.Desde And PLPE.Hasta
		And TD.PlanillaTactico = 'True'
		And (Case When Exists(Select * From VDetalleActividad D 
											Join VActividad A On D.IDActividad=A.ID 
											Join DetallePlanillaDescuentoTactico DP On A.ID=DP.IDActividad 
											Join PlanillaDescuentoTactico P On DP.IDTransaccion=P.IDTransaccion 
											Where D.ID=@IDProducto And (@vFecha between P.Desde And P.Hasta) And P.IDSucursal=@IDSucursal) Then 'True' Else 'False' End)='True'

    ENd -- de Gs a otras monedas

	-- Precio otras monedas a Fact GS
--ATENCION!!! - Saque la sucursal para poder vender a clientes de otra sucursal	
	IF (@IDMoneda = 1) and 
	(Select IDMoneda from ProductoListaPrecio where IDProducto = @IDProducto and IDListaPrecio = @IDListaPrecio) <> 1 Begin
		--Este es para Precio Normal	
		declare @IDMonedaPrecio1 tinyint = (Select IDMoneda from ProductoListaPrecio where IDProducto = @IDProducto and IDListaPrecio = @IDListaPrecio)
		declare @vCotizacionMonedaPrecio1 Decimal = (Select top(1) Cotizacion from Cotizacion where IDMoneda = @IDMonedaPrecio1 order by ID desc )

		Select 'IDTipo'=-1, 'Tipo'='Precio', 'TipoDescuento'='', 'IDActividad'=NULL, 'Importe'=P.Precio * @vCotizacionMonedaPrecio1, 'Porcentaje'=0, 'Existencia'=@vExistencia, 'Tactico'='False', IDMoneda
		From ProductoListaPrecio P
		Join ListaPrecio LP On P.IDListaPrecio=LP.ID
		Where P.IDProducto=@IDProducto 
		And P.IDListaPrecio=@vIDListaPrecio 
		--And LP.IDSucursal=@IDSucursal 
	
		Union All
	
		--Este es para TPR	
		Select 'IDTipoDescuento'=0, 'Tipo'='TPR', 'T. Desc.'='TPR', 'IDActividad'=NULL, 'Importe'=IsNull(P.TPR * @vCotizacionMonedaPrecio1,0), 'Porcentaje'=IsNull(P.TPRPorcentaje,0), 'Existencia'=@vExistencia, 'Tactico'='False'  , IDMoneda
		From ProductoListaPrecio P
		Join ListaPrecio LP On P.IDListaPrecio=LP.ID
		Where P.IDProducto=@IDProducto 
		And P.IDListaPrecio=@vIDListaPrecio 
		--And LP.IDSucursal=@IDSucursal 
		And (@vFecha Between IsNull(P.TPRDesde, DateAdd(d,-1, @vFecha)) And IsNull(P.TPRHasta, DateAdd(d,1, @vFecha)))
	
		Union all
	
		--Descuentos normales
		Select PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, PLPE.Descuento * @vCotizacionMonedaPrecio1, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='False' , IDMoneda
		From ProductoListaPrecioExcepciones PLPE 
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDCliente=@IDCliente  
		--And IDSucursal = @IDSucursal 
		And TD.PlanillaTactico = 'False'
		And (@vFecha Between Desde And Hasta)
	
		Union All
	
		--Excepciones TACTICO
		Select PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, PLPE.Descuento * @vCotizacionMonedaPrecio1, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='True' , IDMoneda
		From ProductoListaPrecioExcepciones PLPE 
		Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto And PLPE.IDListaPrecio=@vIDListaPrecio And PLPE.IDCliente=@IDCliente  
		--And LP.IDSucursal = @IDSucursal 
		And @vFecha between PLPE.Desde And PLPE.Hasta
		And TD.PlanillaTactico = 'True'
		And (Case When Exists(Select * From VDetalleActividad D 
											Join VActividad A On D.IDActividad=A.ID 
											Join DetallePlanillaDescuentoTactico DP On A.ID=DP.IDActividad 
											Join PlanillaDescuentoTactico P On DP.IDTransaccion=P.IDTransaccion 
											Where D.ID=@IDProducto And (@vFecha between P.Desde And P.Hasta) And P.IDSucursal=@IDSucursal) Then 'True' Else 'False' End)='True'

    ENd -- de otras monedas a GS

	
	-- Precio otras monedas a otras monedas (US - RS)
--ATENCION!!! - Saque la sucursal para poder vender a clientes de otra sucursal	
	IF (@IDMoneda <> 1) and 
	(Select IDMoneda from ProductoListaPrecio where IDProducto = @IDProducto and IDListaPrecio = @IDListaPrecio) <> 1 Begin
		--Este es para Precio Normal	
		declare @IDMonedaPrecio tinyint = (Select IDMoneda from ProductoListaPrecio where IDProducto = @IDProducto and IDListaPrecio = @IDListaPrecio)
		declare @vCotizacionMonedaPrecio Decimal = (Select top(1) Cotizacion from Cotizacion where IDMoneda = @IDMonedaPrecio order by ID desc )

		Select 'IDTipo'=-1, 'Tipo'='Precio', 'TipoDescuento'='', 'IDActividad'=NULL, 'Importe'=(P.Precio * @vCotizacionMonedaPrecio)/ @Cotizacion, 'Porcentaje'=0, 'Existencia'=@vExistencia, 'Tactico'='False', IDMoneda
		From ProductoListaPrecio P
		Join ListaPrecio LP On P.IDListaPrecio=LP.ID
		Where P.IDProducto=@IDProducto 
		And P.IDListaPrecio=@vIDListaPrecio 
		--And LP.IDSucursal=@IDSucursal 
	
		Union All
	
		--Este es para TPR	
		Select 'IDTipoDescuento'=0, 'Tipo'='TPR', 'T. Desc.'='TPR', 'IDActividad'=NULL, 'Importe'=IsNull((P.TPR * @vCotizacionMonedaPrecio)/@Cotizacion,0), 'Porcentaje'=IsNull(P.TPRPorcentaje,0), 'Existencia'=@vExistencia, 'Tactico'='False', IDMoneda
		From ProductoListaPrecio P
		Join ListaPrecio LP On P.IDListaPrecio=LP.ID
		Where P.IDProducto=@IDProducto 
		And P.IDListaPrecio=@vIDListaPrecio 
		--And LP.IDSucursal=@IDSucursal 
		And (@vFecha Between IsNull(P.TPRDesde, DateAdd(d,-1, @vFecha)) And IsNull(P.TPRHasta, DateAdd(d,1, @vFecha)))
	
		Union all
	
		--Descuentos normales
		Select PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, (PLPE.Descuento * @vCotizacionMonedaPrecio)/@Cotizacion, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='False' , IDMoneda
		From ProductoListaPrecioExcepciones PLPE 
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDCliente=@IDCliente  
		--And IDSucursal = @IDSucursal 
		And TD.PlanillaTactico = 'False'
		And (@vFecha Between Desde And Hasta)
	
		Union All
	
		--Excepciones TACTICO
		Select PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, (PLPE.Descuento * @vCotizacionMonedaPrecio)/@Cotizacion, PLPE.Porcentaje, 'Existencia'=@vExistencia, 'Tactico'='True' , IDMoneda
		From ProductoListaPrecioExcepciones PLPE 
		Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
		Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
		Where PLPE.IDProducto=@IDProducto And PLPE.IDListaPrecio=@vIDListaPrecio And PLPE.IDCliente=@IDCliente  
		--And LP.IDSucursal = @IDSucursal 
		And @vFecha between PLPE.Desde And PLPE.Hasta
		And TD.PlanillaTactico = 'True'
		And (Case When Exists(Select * From VDetalleActividad D 
											Join VActividad A On D.IDActividad=A.ID 
											Join DetallePlanillaDescuentoTactico DP On A.ID=DP.IDActividad 
											Join PlanillaDescuentoTactico P On DP.IDTransaccion=P.IDTransaccion 
											Where D.ID=@IDProducto And (@vFecha between P.Desde And P.Hasta) And P.IDSucursal=@IDSucursal) Then 'True' Else 'False' End)='True'

    ENd -- de otras monedas a GS
End




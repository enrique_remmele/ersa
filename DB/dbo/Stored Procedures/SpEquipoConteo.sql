﻿CREATE Procedure [dbo].[SpEquipoConteo]

	--Entrada
	@ID tinyint,
	@EquipoConteo tinyint,
	@Descripcion varchar(50),
	@IDZonaDeposito smallint,
	@Estado bit,
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From EquipoConteo Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDEquipoConteo tinyint
		set @vIDEquipoConteo = (Select IsNull((Max(ID)+1), 1) From EquipoConteo)

		--Insertamos
		Insert Into EquipoConteo(ID, Descripcion, IDZonaDeposito, Estado, EquipoConteo )
		Values(@vIDEquipoConteo, @Descripcion, @IDZonaDeposito, @Estado, @EquipoConteo )		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From EquipoConteo Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From EquipoConteo Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update EquipoConteo Set Descripcion=@Descripcion,
								IDZonaDeposito=@IDZonaDeposito, 
								Estado = @Estado,
								EquipoConteo = @EquipoConteo 
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From EquipoConteo Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
				
		--Eliminamos las relaciones con usuarios
		Delete From EquipoConteoUsuario
		Where IDEquipo = @ID
		
		--Eliminamos
		Delete From EquipoConteo 
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿CREATE Procedure [dbo].[SpImpresionSeccion]

	--Entrada
	@IDOperacion tinyint,
	@IDTipoComprobante tinyint,
	@ID tinyint,
	@Descripcion varchar(50),
	@Alto integer,
	@Tabla varchar(50),
	@Operacion varchar(10),
	
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	--Si es que el tipo de comprobante no corresponde a la operacion
	If Not Exists(Select * From TipoComprobante Where ID=@IDTipoComprobante And IDOperacion=@IDOperacion) begin
		set @Mensaje = 'El tipo de comprobante no corresponde a la operacion!'
		set @Procesado = 'False'
		return @@rowcount
	end
	
	--INSERTAR
	if @Operacion='INS' begin
	
		
		--Si ya existe el registro
		If Exists(Select * From ImpresionSeccion Where IDTipoComprobante=@IDTipoComprobante And IDOperacion=@IDOperacion And ID=@ID) Begin
			set @Mensaje = 'El registro ya existe!'
			set @Procesado = 'false'
			return @@rowcount
		End
		
		--Insertamos
		Insert Into ImpresionSeccion(IDOperacion, IDTipoComprobante, ID, Descripcion, Alto, Tabla)
		Values(@IDOperacion, @IDTipoComprobante, @ID, @Descripcion, @Alto, @Tabla)		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
	
		--Si no existe el registro
		if Not Exists(Select * From ImpresionSeccion Where IDTipoComprobante=@IDTipoComprobante And IDOperacion=@IDOperacion And ID=@ID) Begin
			set @Mensaje = 'El registro no existe!'
			set @Procesado = 'false'
			return @@rowcount
		End		
	
		--Actualizamos
		Update ImpresionSeccion Set Alto=@Alto,
								Tabla=@Tabla							
		Where IDTipoComprobante=@IDTipoComprobante And IDOperacion=@IDOperacion And ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	--ELIMINAR
	if @Operacion='DEL' begin
	
		--Si no existe el registro
		if Not Exists(Select * From ImpresionSeccion Where IDTipoComprobante=@IDTipoComprobante And IDOperacion=@IDOperacion And ID=@ID) Begin
			set @Mensaje = 'El registro no existe!'
			set @Procesado = 'false'
			return @@rowcount
		End		
	
		--Eliminamos los campos
		Delete From ImpresionCampo Where IDSeccion=@ID And IDTipoComprobante=@IDTipoComprobante And IDOperacion=@IDOperacion
		 
		--Eliminamos la Seccion
		Delete From ImpresionSeccion 
		Where IDTipoComprobante=@IDTipoComprobante And IDOperacion=@IDOperacion And ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	set @Mensaje = 'No se proceso!'
	set @Procesado = 'False'
	return @@rowcount
		
End




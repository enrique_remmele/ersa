﻿CREATE Procedure [dbo].[SpViewSaldoBancario]
	
As

Begin
  
	--Declarar variables 
	declare @vID int
	declare @vBanco varchar(50)
	declare @vCuenta varchar(50)
	declare @vConciliado bit
	declare @vDebito money
	declare @vCredito money
	declare @vMovimiento varchar (50)
	declare @vSaldoContable money
	declare @vDepositoConfirmar money
	declare @vChequesNoPagados money
	declare @vSaldoConciliado money
	declare @vUltimoDebito date
	declare @vUltimoCredito date
	declare @vCuentaAnterior varchar(50) = ''
	declare @Moneda varchar(4)

	set @vSaldoContable = 0
	set @vDepositoConfirmar = 0
	set @vChequesNoPagados = 0
	set @vSaldoConciliado = 0
	
	
	Begin
		
	--Crear la tabla temporal
    create table #TablaTemporal(ID int,
								Banco varchar(50),
								Cuenta varchar(50),
								Moneda varchar(4),
								Debito money,
								Credito money,
								Movimiento varchar(50),
								UltimoDebito date,
								UltimoCredito date,
								SaldoContable money,
								DepositoConfirmar money,
								ChequesNoPagados money,
								SaldoConciliado money)
		
	Set @vID = (Select IsNull(MAX(ID)+1,1) From #TablaTemporal )
		
		
	Declare db_cursor cursor for
		
	Select 
	Banco,
	Cuenta,
	Conciliado,
	Moneda,
	SUM(Debito),
	SUM(Credito),
	Movimiento,
	'UltimoDebito'=MIN(UltimoDebito),
	'UltimoCredito'=MIN(UltimoCredito) 
	From VSaldoBanco
	Group By Banco, Cuenta, Conciliado, Movimiento,Moneda
	Order By Cuenta
	
	Open db_cursor   
	Fetch Next From db_cursor Into  @vBanco,@vCuenta,@vConciliado,@Moneda,@vDebito,@vCredito,@vMovimiento,@vUltimoDebito,@vUltimoCredito   
	While @@FETCH_STATUS = 0 Begin  
		

		If @vCuentaAnterior != @vCuenta Begin
			Set @vChequesNoPagados = 0
			Set @vSaldoConciliado = 0
			Set @vSaldoContable = 0
			Set @vDepositoConfirmar = 0
			Set @vCuentaAnterior = @vCuenta
		End
		
		 
		--Calcular Saldo Contable
		Set @vSaldoContable = @vSaldoContable + (@vDebito - @vCredito)
		
		--If @vSaldoContable < 0 Begin
		--	Set @vSaldoContable = @vSaldoContable *-1
		--End
	
		--Calular Saldo Conciliado 
		If @vConciliado= 'True' Begin
			Set @vSaldoConciliado = @vSaldoConciliado + (@vDebito - @vCredito)
		End		
			
		--Calcular Ordenes de Pago
		If @vMovimiento='OP' And @vConciliado = 'False' Begin
			Set @vChequesNoPagados = @vChequesNoPagados + (@vDebito - @vCredito) 
		End
			
					
		----Calcular Deposito a Confirmar
		If @vMovimiento <> 'OP' And @vMovimiento <> 'DEP' And @vConciliado = 'False' Begin
			Set @vDepositoConfirmar = @vDepositoConfirmar + (@vDebito - @vCredito) 
		End
		
		--Actualizar si ya existe
		If Exists(Select * From #TablaTemporal Where Cuenta = @vCuenta)Begin
			Update #TablaTemporal Set SaldoContable = @vSaldoContable,
									  DepositoConfirmar = @vDepositoConfirmar,
									  ChequesNoPagados = @vChequesNoPagados,
									  SaldoConciliado = @vSaldoConciliado,
									  UltimoDebito=@vUltimoDebito,
									  UltimoCredito=@vUltimoCredito
									  Where Cuenta = @vCuenta 	 

		End
		
		--Insertar si aún no existe		
		If Not exists(Select * From #TablaTemporal Where Cuenta = @vCuenta)Begin		
			Insert Into #TablaTemporal(ID,Banco,Cuenta,Debito,Credito,Moneda,Movimiento,UltimoDebito,UltimoCredito,SaldoContable,DepositoConfirmar,ChequesNoPagados,SaldoConciliado)  
			Values(@vID,@vBanco,@vCuenta,@vDebito,@vCredito,@Moneda,@vMovimiento,@vUltimoDebito,@vUltimoCredito,@vSaldoContable,@vDepositoConfirmar,@vChequesNoPagados,@vSaldoConciliado)
		End	
				
		Fetch Next From db_cursor Into @vBanco,@vCuenta,@vConciliado,@Moneda,@vDebito,@vCredito,@vMovimiento,@vUltimoDebito,@vUltimoCredito   
			
	End
		
	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor		   			
		
	End	
	
	Select * From #TablaTemporal Where ID=@vID  Order by Banco
	
	
End



﻿CREATE Procedure [dbo].[SpCobradorInsUpd]

	--Entrada
	@Descripcion varchar(100)
	
As

Begin

	Declare @vID int
	
	If @Descripcion = '' Begin
		Set @Descripcion = ''
	End
	
	--Si existe
	If Exists(Select * From Cobrador Where Nombres = @Descripcion) Begin
		Set @vID = (Select Top(1) ID From Cobrador Where Nombres = @Descripcion)
		
	End Else Begin
		Set @vID = (Select IsNull(Max(ID)+1,1) From Cobrador)
		Insert Into Cobrador(ID, Nombres, Estado)
		Values(@vID, @Descripcion, 'True')
	End
	
	return @vID
	
End


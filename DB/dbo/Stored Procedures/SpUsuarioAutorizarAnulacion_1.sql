﻿CREATe Procedure [dbo].[SpUsuarioAutorizarAnulacion]

	--Entrada
	@ID tinyint,
	@IDUsuarioAutorizar varchar(50),
	@PeriodoFueraDelMes bit,
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--INSERTAR
	if @Operacion='INS' begin
		
		if exists(Select * from UsuarioAutorizarAnulacion where idusuario = @IDUsuarioAutorizar) begin
			set @Mensaje = 'El usuario ya existe en la configuracion!'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Obtenemos el nuevo ID
		declare @vID tinyint
		set @vID = (Select IsNull((Max(ID)+1), 1) From UsuarioAutorizarAnulacion)

		--Insertamos
		Insert Into UsuarioAutorizarAnulacion(ID, IDUsuario, PeriodoFueraDelMes, Estado)
		Values(@vID, @IDUsuarioAutorizar, @PeriodoFueraDelMes, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='UsuarioAutorizarAnulacion', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
				 		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From UsuarioAutorizarAnulacion Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		if exists(Select * from UsuarioAutorizarAnulacion where idusuario = @IDUsuarioAutorizar and ID != @ID) begin
			 set @Mensaje = 'La configuracion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update UsuarioAutorizarAnulacion Set IDUsuario=@IDUsuarioAutorizar,
						PeriodoFueraDelMes =@PeriodoFueraDelMes,
						Estado =@Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='UsuarioAutorizarAnulacion', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From UsuarioAutorizarAnulacion Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
				
		--Eliminamos
		Delete From UsuarioAutorizarAnulacion 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='UsuarioAutorizarAnulacion', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


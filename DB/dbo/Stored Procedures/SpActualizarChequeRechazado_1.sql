﻿CREATE Procedure [dbo].[SpActualizarChequeRechazado]

	--Entrada
	@IDTransaccion int,
	@Fecha date,
	@Observacion varchar(150),
	@IDMotivoRechazo int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin
			
			
	--Actualizar 
	Update ChequeClienteRechazado  Set  Fecha=@Fecha,
								Observacion =@Observacion,
								IDMotivoRechazo=@IDMotivoRechazo
	Where IDTransaccion=@IDTransaccion 
	
			
	set @Mensaje = 'Registro guardado'
	set @Procesado = 'True'
	return @@rowcount
		

End
	
	


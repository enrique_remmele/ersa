﻿CREATE Procedure [dbo].[SpRemision]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDPuntoExpedicion int = Null,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante int = NULL,			
	@IDCliente int = NULL,
	@DireccionPartida varchar(200) = '',
	@DireccionLlegada varchar(200) = '',
	@IDSucursalOperacion int = NULL,
	@IDDepositoOperacion int = NULL,
	@FechaInicio date = NULL,
	@FechaFin date = NULL,
	
	@IDDistribuidor int = Null,
	@IDCamion int = Null,
	@IDChofer int = Null,
	
	@IDMotivo int = NULL,
	@MotivoObservacion varchar(200) = '',
	@ComprobanteVenta varchar(50) = '',
	@Observacion varchar(100) = NULL,
			
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion int,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

BEGIN
    DECLARE @vDescripcion varchar
    DECLARE @vFechaDocumento date
    DECLARE @vRazonSocial int
    
    SET @vDescripcion = (SELECT NroComprobante FROM dbo.Remision WHERE IDTransaccion = @IDTransaccion)
    SET @vFechaDocumento = (SELECT FechaInicio FROM dbo.Remision WHERE IDTransaccion = @IDTransaccion)
    SET @vRazonSocial = (SELECT IDCliente FROM dbo.Remision WHERE IDTransaccion = @IDTransaccion)

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	--Validar Fecha Timbrado 
		declare @fechaTimbrado as datetime
		select @fechaTimbrado = Vencimiento from PuntoExpedicion where Id = @IdPuntoExpedicion
		if @fechaTimbrado < @FechaInicio begin
			Set @Mensaje  = 'La fecha del timbrado ha vencido. Por favor seleccione otro numero de timbrado.'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		end
	
		--Comprobante fuera del Rango
		if @NroComprobante < (Select NumeracionDesde From PuntoExpedicion Where ID=@IDPuntoExpedicion) Or @NroComprobante > (Select NumeracionHasta From PuntoExpedicion Where ID=@IDPuntoExpedicion) Begin
			set @Mensaje = 'El numero de comprobante no esta dentro del rango correcto! Cambie el punto de expedicion o actualicelo.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
	
		--Validar
		--Comprobante
		if Exists(Select * From Remision  Where IDPuntoExpedicion=@IDPuntoExpedicion And NroComprobante=@NroComprobante) Begin
			set @Mensaje = 'El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--Insertar en Remision
		Insert Into Remision(IDTransaccion, IDPuntoExpedicion, IDTipoComprobante, NroComprobante, IDCliente, DireccionPartida, DireccionLlegada, IDSucursal, IDDeposito, FechaInicio, FechaFin, IDDistribuidor, IDCamion, IDChofer, IDMotivo, MotivoObservacion, ComprobanteVenta, Observacion, Anulado, FechaAnulado, IDUsuarioAnulado, Procesado)
		Values(@IDTransaccionSalida, @IDPuntoExpedicion, @IDTipoComprobante, @NroComprobante, @IDCliente, @DireccionPartida, @DireccionLlegada, @IDSucursalOperacion, @IDDepositoOperacion, @FechaInicio, @FechaFin, @IDDistribuidor, @IDCamion, @IDChofer, @IDMotivo, @MotivoObservacion, @ComprobanteVenta, @Observacion, 'False', NULL, NULL, 'True')

		--Actualizar Punto de Expedicion
		Update PuntoExpedicion Set UltimoNumero=@NroComprobante
		Where ID=@IDPuntoExpedicion
										
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	
		--Si ya esta anulado
		If Exists(Select * From Remision Where IDTransaccion=@IDTransaccion And Anulado='True') Begin
			set @Mensaje = 'El comprobante ya esta anulado!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		END
		
----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT		
		
		----Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		declare @Fecha as date
		declare @Comprobante as varchar(32)
		declare @Total as money
		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from Remision where IDTransaccion = @IDTransaccion)
		set @IDCliente = (select IDCliente from Remision where IDTransaccion = @IDTransaccion)
		set @Fecha = (select FechaInicio from Remision where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from Remision where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select Comprobante from VRemision where IDTransaccion = @IDTransaccion)
		declare @VTipoComprobante varchar(16) = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)


		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,@IDCliente,0,@Fecha,@Comprobante,0,'REMISION', getdate(),@IDUsuario,'ANU',@VTipoComprobante, @IDSucursal)

		----Anular
		Update Remision  Set Anulado='True', IDUsuarioAnulado=@IDUsuario, FechaAnulado = GETDATE()
		Where IDTransaccion = @IDTransaccion
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
		
	End
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Remision Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		END
		
----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT			
		
		--Eliminamos Anulados
		Delete DocumentoAnulado where IDTransaccion = @IDTransaccion	
		
		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from Remision where IDTransaccion = @IDTransaccion)
		set @IDCliente = (select IDCliente from Remision where IDTransaccion = @IDTransaccion)
		set @Fecha = (select FechaInicio from Remision where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from Remision where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select Comprobante from VRemision where IDTransaccion = @IDTransaccion)
		set @VTipoComprobante  = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)


		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,@IDCliente,0,@Fecha,@Comprobante,0,'REMISION', getdate(),@IDUsuario,'ELI',@VTipoComprobante, @IDSucursal)
			
		
		--Eliminamos el detalle
		Delete DetalleRemision   where IDTransaccion = @IDTransaccion	
						
		--Eliminamos el registro
		Delete Remision   Where IDTransaccion = @IDTransaccion
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End


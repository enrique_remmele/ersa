﻿CREATE Procedure [dbo].[SpAsientoBloquear]

	@IDTransaccion numeric(18,0),
	@Bloquear bit = 0
		
As

Begin
	Declare @vImporteHaber money 
	Declare @vImporteDebe money 
	--Actualizamos la cabecera, el total
	Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
	Set @vImporteHaber = ROUND(@vImporteHaber, 0)
	Set @vImporteDebe = Round(@vImporteHaber,0)
	
	Update Asiento Set	Bloquear=@Bloquear,
						Total = @vImporteHaber,
						Credito = @vImporteHaber,
						Debito = @vImporteDebe,
						Saldo = @vImporteHaber - @vImporteDebe
	Where IDTransaccion=@IDTransaccion
	
	
End


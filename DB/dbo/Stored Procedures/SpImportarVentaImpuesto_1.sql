﻿
CREATE Procedure [dbo].[SpImportarVentaImpuesto]

	--Entrada
	--Punto de Expedicion
	@TipoComprobante varchar(10),
	@ReferenciaSucursal varchar(5),
	@ReferenciaPuntoExpedicion varchar(5),
	@NroComprobante varchar(50),
	
	--Cliente
	@Cliente varchar(50),
	@RUC varchar(50),
	@Direccion varchar(200),
	
	--
	@FechaEmision varchar(50),
	@Sucursal varchar(50),
	@Deposito varchar(50),
	@CondicionVenta varchar(5),
	@FechaVencimiento varchar(50),
	@ListaPrecio varchar(100),
	@Vendedor varchar(100),
	@Cotizacion tinyint,
	@Observacion varchar(200),
	
	--Totales
	@Total money,
	@TotalImpuesto money,
	@TotalDescuento money,
	@Saldo money,
	
	--Impuesto
	@Exento as money,
	@DescuentoExento as money,
	@Gravado10 as money,
	@IVA10 as money,
	@Descuento10 as money,
	@Gravado5 as money,
	@IVA5 as money,
	@Descuento5 as money,

	--Configuraciones
	@Anulado bit,
	
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
		
	@Actualizar bit
	
As

Begin
		
	--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		
		Declare @vOperacion varchar(50)
		Declare @vIDTransaccion int
		Declare @vComprobante varchar(50)
		Declare @vTelefono varchar(100)
		Declare @vCredito bit
		Declare @vTotalDiscriminado money
		Declare @vTotalDescuento money
		Declare @vCobrado money
		Declare @vDescontado money
		Declare @vAcreditado money
		Declare @vEsVentaSucursal bit
		
		--Codigos
		Declare @vIDPuntoExpedicion int 
		Declare @vIDTipoComprobante int
		Declare @vIDCliente int
		Declare @vIDSucursalCliente int
		Declare @vIDSucursal int
		Declare @vIDDeposito int
		Declare @vIDListaPrecio int
		Declare @vIDVendedor int
		
	End
	
	--Establecer valores predefinidos
	Begin
		
		Set @vOperacion = 'INS'
		Set @vIDTransaccion = 0
		Set @vTelefono = ''
		Set @vCredito = 'False'
		If @CondicionVenta = 'C' Begin
			Set @vCredito = 'True'
		End
		
		Set @vTotalDiscriminado = @Total - @TotalImpuesto
		Set @vCobrado = @Total - @Saldo
		Set @vDescontado = 0
		Set @vAcreditado = 0
		Set @vEsVentaSucursal = 'False'
	
		--Comprobante
		Set @ReferenciaSucursal = '0' + dbo.FFormatoDosDigitos(CONVERT(tinyint, @ReferenciaSucursal))
		Set @ReferenciaPuntoExpedicion = '0' + dbo.FFormatoDosDigitos(CONVERT(tinyint, @ReferenciaPuntoExpedicion))
		Set @vComprobante = @ReferenciaSucursal + '-' + @ReferenciaPuntoExpedicion + '-' + @NroComprobante
		
		Declare @vCancelado bit
		Set @vCancelado = 'False'
		If @Saldo = 0 Begin
			Set @vCancelado = 'True'
		End
				
	End
	
	--Verificar si ya existe
	If Exists(Select * From VentaImportada VI  Join Venta V On VI.IDTransaccion= V.IDTransaccion Where V.Comprobante=@vComprobante) Begin
		set @vIDTransaccion = (Select VI.IDTransaccion  From VentaImportada VI  Join Venta V On VI.IDTransaccion= V.IDTransaccion Where V.Comprobante=@vComprobante)
		Set @vOperacion = 'UPD'
	End
	
	--Puede que la factura sea cargada manualmente, consultar en la tabla ventas
	If @vIDTransaccion = 0 Begin
		If Exists(Select * From Venta V Where V.Comprobante=@vComprobante) Begin
			Set @vMensaje = 'El comprobante ya fue cargado por algun usuario!'
			Set @vProcesado = 'True'
			GoTo Salir
		End
	End

	--ACTUALIZAR
	If @vOperacion = 'UPD' Begin
	
		If @Actualizar = 'True' Begin

			--Sucursal
			Set @vIDSucursal = (IsNull((Select Top(1) ID From Sucursal Where Referencia=@ReferenciaSucursal),0))
			If @vIDSucursal = 0 Begin
				Set @vMensaje = 'Sucursal incorrecto 141!'
				Set @vProcesado = 'False'
				GoTo Salir
			End
			
			--Deposito
			--Establecemos como deposito el primero que exista en la sucursal
			Set @vIDDeposito = IsNull((Select Top(1) ID From Deposito Where IDSucursal=@vIDSucursal), 0)
			If @vIDDeposito = 0 Begin
				Set @vMensaje = 'Deposito no concuerda!'
				Set @vProcesado = 'False'
				GoTo Salir
			End

			--Cliente, los clientes son unicos, no por sucural
			Set @vIDCliente=(IsNull((Select Top(1) ID From Cliente Where Referencia=@Cliente),0))
			If @vIDCliente = 0 Begin
				
				--Si el RUC no tiene, salir
				If LTrim(@RUC) = '' Or RTrim(@RUC) = '' Begin 
					Set @vMensaje = 'Cliente incorrecto! El codigo no existe y no tiene RUC'
					Set @vProcesado = 'False'
					GoTo Salir
				End

				Set @vIDCliente=(IsNull((Select Top(1) ID From Cliente Where RUC=@RUC),0))
				
				If @vIDCliente = 0 Begin
					Set @vMensaje = 'Cliente incorrecto UPD!'
					Set @vProcesado = 'False'
					GoTo Salir
				End
				
			End
			
			--Lista de Precio
			--Establecer el Minorista de la sucursal seleccionada
			Set @vIDListaPrecio = IsNull((Select Top(1) ID From ListaPrecio Where IDSucursal=@vIDSucursal And Descripcion='MINORISTA'), 0)
			If @vIDListaPrecio = 0 Begin
				Set @vMensaje = 'No se establecio la lista de precio minorista para esta sucursal!'
				Set @vProcesado = 'False'
				GoTo Salir
			End


			--Venta
			Update Venta Set TotalImpuesto=@IVA10+@IVA5,
							TotalDiscriminado=(@Gravado10 - (@IVA10 + @Descuento10)) + (@Gravado5 - (@IVA5 + @Descuento5))
			Where IDTransaccion = @vIDTransaccion
			
			--Venta Importacion
			--Actualizar en VentaImportacion
			
			--Actualizar Impuesto
			--10%
			Update DetalleImpuesto Set  Total = @Gravado10, 
										TotalImpuesto = @IVA10, 
										TotalDescuento = @Descuento10, 
										TotalDiscriminado = @Gravado10 - (@IVA10 + @Descuento10)
			Where IDTransaccion = @vIDTransaccion And IDImpuesto = 1
			
			--5%
			Update DetalleImpuesto Set  Total = @Gravado5, 
										TotalImpuesto = @IVA5, 
										TotalDescuento = @Descuento5, 
										TotalDiscriminado = @Gravado5 - (@IVA5 + @Descuento5)
			Where IDTransaccion = @vIDTransaccion And IDImpuesto = 2
			
			--10%
			Update DetalleImpuesto Set  Total = @Exento, 
										TotalImpuesto = 0, 
										TotalDescuento = @DescuentoExento, 
										TotalDiscriminado = @Exento - @DescuentoExento
			Where IDTransaccion = @vIDTransaccion And IDImpuesto = 3
			
					
			Set @vMensaje = 'Actualizado!'
			Set @vProcesado = 'True'
			GoTo Salir

		End
		
		Set @vMensaje = 'Sin Act.!'
		Set @vProcesado = 'True'
		GoTo Salir
		
	End
	


	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


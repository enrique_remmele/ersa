﻿CREATE Procedure [dbo].[SpPorcentajeProductoPrecio]

	--Entrada
	@IDUsuarioProductoPrecio int,
	@IDPresentacion int,
	@Porcentaje decimal(18,2),
	@Importe money,
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if not exists(Select * From Usuario Where ID=@IDUsuarioProductoPrecio) begin
			set @Mensaje = 'El usuario no existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		--Si es que la configuracion ya existe
		if exists(Select * From PorcentajeProductoPrecio Where IDPresentacion = @IDPresentacion and IDUsuario=@IDUsuarioProductoPrecio) begin
			set @Mensaje = 'La configuracion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Insertamos
		Insert Into PorcentajeProductoPrecio(IDUsuario, IDPresentacion, Porcentaje, Importe, Estado)
		Values(@IDUsuarioProductoPrecio, @IDPresentacion, @Porcentaje, @Importe, @Estado)		
		
		insert into aPorcentajeProductoPrecio(IDUsuario, IDPresentacion, Porcentaje, Importe, Estado, FechaModificacion,IDUsuarioModificacion,Operacion)
		Select IDUsuario, IDPresentacion, Porcentaje, Importe, Estado, GetDate(),@IDUsuario,@Operacion from PorcentajeProductoPrecio where IDUsuario = @IDUsuarioProductoPrecio and IDPresentacion = @IDPresentacion



		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PORCENTAJEPRODUCTOPRECIO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From PorcentajeProductoPrecio Where IDUsuario=@IDUsuarioProductoPrecio) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update PorcentajeProductoPrecio Set Porcentaje=@Porcentaje, Importe = @Importe, Estado =@Estado
										Where IDUsuario=@IDUsuarioProductoPrecio
										and IDPresentacion = @IDPresentacion

		insert into aPorcentajeProductoPrecio(IDUsuario, IDPresentacion, Porcentaje, Importe, Estado, FechaModificacion,IDUsuarioModificacion,Operacion)
		Select IDUsuario, IDPresentacion, Porcentaje, Importe, Estado, GetDate(),@IDUsuario,@Operacion from PorcentajeProductoPrecio where IDUsuario = @IDUsuarioProductoPrecio and IDPresentacion = @IDPresentacion

		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PORCENTAJEPRODUCTOPRECIO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From PorcentajeProductoPrecio Where IDUsuario=@IDUsuarioProductoPrecio) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		insert into aPorcentajeProductoPrecio(IDUsuario, IDPresentacion, Porcentaje, Importe, Estado, FechaModificacion,IDUsuarioModificacion,Operacion)
		Select IDUsuario, IDPresentacion, Porcentaje, Importe, Estado, GetDate(),@IDUsuario,@Operacion from PorcentajeProductoPrecio where IDUsuario = @IDUsuarioProductoPrecio and IDPresentacion = @IDPresentacion


		Delete From PorcentajeProductoPrecio 
		Where IDUsuario=@IDUsuarioProductoPrecio
		and IDPresentacion = @IDPresentacion
		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PORCENTAJEPRODUCTOPRECIO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


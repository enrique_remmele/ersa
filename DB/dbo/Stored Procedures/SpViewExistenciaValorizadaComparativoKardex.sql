﻿CREATE Procedure [dbo].[SpViewExistenciaValorizadaComparativoKardex]

	--Entrada
	@IDTipoProducto int = 0,
	@IDLinea int = 0,
	@IDSubLinea int = 0,
	@IDSucursal int = 0,
	@IDDeposito int = 0,
	@IDProducto int = 0,
	@Fecha date,
	@Resumido bit = 'False'
	
As

Begin

	--Variables
	Begin
		declare @vIDSucursal int
		declare @vIDDeposito int
		declare @vDeposito varchar(100)
		declare @vIDLinea int
		declare @vLinea varchar(100)
		declare @vIDTipoProducto int
		declare @vTipoProducto varchar(100)
		declare @vIDMarca int
		declare @vMarca varchar(100)
		declare @vIDProducto int
		declare @vReferencia varchar(100)
		declare @vProducto varchar(500)
		declare @vCostoPromedio money
		declare @vExistencia decimal(18,2)
		declare @vCantidadAnulados decimal(18,2)
		declare @vTotalValorizadoCostoPromedio money
		declare @vExistenciaKardex decimal(18,2)
		declare @vDiferenciaEnExistencias decimal(18,2)
		declare @vTotalValorizadoCostoPromedioKardex money
		declare @vPesoUnitario money
		
	End
	
	
	--Crear la tabla temporal
	create table #TablaTemporal(IDSucursal int,
								IDDeposito int,
								Deposito varchar(100),
								IDLinea int,
								Linea varchar(100),
								IDTipoProducto int,
								TipoProducto varchar(100),
								IDMarca int,
								Marca varchar(100),
								IDProducto int,
								Referencia varchar(100),
								Producto varchar(500),
								CostoPromedio money, 
								Existencia decimal(18,2), 
								CantidadAnulados decimal(18,2), 
								TotalValorizadoCostoPromedio money, 
								ExistenciaKardex decimal(18,2), 
								DiferenciaEnExistencias decimal(18,2), 
								TotalValorizadoCostoPromedioKardex money,  
								PesoUnitario money)
																	
		Begin
				
			Declare db_cursor cursor for
			
			Select  
			--E.IDSucursal,
			--E.IDDeposito,
			--E.Deposito,
			E.IDLinea,
			E.Linea,
			E.IDTipoProducto,
			E.TipoProducto,
			E.IDMarca,
			E.Marca,
			E.IDProducto,
			E.Referencia,
			E.Producto,
			ISNULL(AVG(E.PesoUnitario),1) PesoUnitario  
			From VExistenciaDepositoComparativoKardex E  

			Where E.Estado = 1
			and ((Case when @IDTipoProducto = 0 then 0  else E.IDTipoProducto end) = (Case when @IDTipoProducto = 0 then 0  else @IDTipoProducto end))
			and ((Case when @IDLinea = 0 then 0  else E.IDLinea end) = (Case when @IDLinea = 0 then 0  else @IDLinea end))
			and ((Case when @IDSubLinea = 0 then 0  else E.IDSubLinea end) = (Case when @IDSubLinea = 0 then 0  else @IDSubLinea end))
			--and ((Case when @IDSucursal = 0 then 0  else E.IDSucursal end) = (Case when @IDSucursal = 0 then 0  else @IDSucursal end))
			--and ((Case when @IDDeposito = 0 then 0  else E.IDDeposito end) = (Case when @IDDeposito = 0 then 0  else @IDDeposito end))
			and ((Case when @IDProducto = 0 then 0  else E.IDProducto end) = (Case when @IDProducto = 0 then 0  else @IDProducto end))

			GROUP BY 
			--E.IDSucursal,
			--E.IDDeposito,
			--E.Deposito,
			E.IDLinea,
			E.Linea,
			E.IDTipoProducto,
			E.TipoProducto, 
			E.IDMarca,
			E.Marca,
			E.IDProducto,
			E.Referencia,
			E.Producto 
			Order by E.TipoProducto, 
			E.Linea, 
			E.Producto 
			ASC
			Open db_cursor   --@vIDSucursal,@vIDDeposito,@vDeposito,
			Fetch Next From db_cursor Into	@vIDLinea,@vLinea,@vIDTipoProducto,@vTipoProducto,@vIDMarca,@vMarca,@vIDProducto,@vReferencia,@vProducto,@vPesoUnitario 
			While @@FETCH_STATUS = 0 Begin 
			
				Set @vCostoPromedio = (dbo.FCostoProductoFechaKardex(@vIDProducto, @Fecha))
				Set @vExistencia = (dbo.FExtractoMovimientoProducto(@vIDProducto, DateAdd(day,1,@Fecha), '', 'False'))
				Set @vTotalValorizadoCostoPromedio = @vCostoPromedio * @vExistencia
				--Set @vCantidadAnulados = (dbo.FExtractoMovimientoProductoSoloAnulados(@vIDProducto, '20200101', DateAdd(day,1,@Fecha), 'False'))

				Set @vExistenciaKardex =(dbo.FExistenciaProductoFechaKardex(@vIDProducto,@Fecha))
				
				Set @vTotalValorizadoCostoPromedioKardex = @vCostoPromedio * @vExistenciaKardex
				
				--if @vExistencia <> @vExistenciaKardex begin
					Select @vCantidadAnulados=( IsNull(Sum(Entrada), 0)-IsNull(Sum(Salida), 0))--,
					From VExtractoMovimientoProductoDetalleDatosMinimosValorizado
					Where IDProducto=@vIDProducto 
					And Fecha < DateAdd(day,1,@Fecha)
					And isnull(Anulado,0) = 1			
				--end 
				--else begin 
				--	Set @vCantidadAnulados = 0
				--end


				--Para el ajuste !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				--Set @vDiferenciaEnExistencias = @vExistencia - @vExistenciaKardex+@vCantidadAnulados
				
				--Para el informe
				Set @vDiferenciaEnExistencias = @vExistencia - @vExistenciaKardex-@vCantidadAnulados
				
				Insert Into  #TablaTemporal(IDSucursal, IDDeposito, Deposito, IDLinea, Linea, IDTipoProducto, TipoProducto, IDMarca, Marca, IDProducto, Referencia, Producto, CostoPromedio, Existencia, CantidadAnulados, TotalValorizadoCostoPromedio, ExistenciaKardex, DiferenciaEnExistencias, TotalValorizadoCostoPromedioKardex, PesoUnitario) 
									Values (@vIDSucursal,@vIDDeposito,@vDeposito,@vIDLinea,@vLinea,@vIDTipoProducto,@vTipoProducto,@vIDMarca,@vMarca,@vIDProducto,@vReferencia,@vProducto,@vCostoPromedio, @vExistencia, @vCantidadAnulados, @vTotalValorizadoCostoPromedio, @vExistenciaKardex, @vDiferenciaEnExistencias, @vTotalValorizadoCostoPromedioKardex, @vPesoUnitario)
								
							
						--@vIDSucursal,@vIDDeposito,@vDeposito,
			Fetch Next From db_cursor Into	@vIDLinea,@vLinea,@vIDTipoProducto,@vTipoProducto,@vIDMarca,@vMarca,@vIDProducto,@vReferencia,@vProducto,@vPesoUnitario 
				
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
			
		End	
		
		if @Resumido = 'True' begin
			SELECT  TipoProducto, SUM(Existencia) as Existencia, SUM(TotalValorizadoCostoPromedio) AS TotalValorizadoCostoPromedio From #TablaTemporal GROUP BY TipoProducto 
		end
		else begin

			Select * From #TablaTemporal
		end
	

End



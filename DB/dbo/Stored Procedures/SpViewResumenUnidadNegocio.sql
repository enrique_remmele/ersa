﻿CREATE Procedure [dbo].[SpViewResumenUnidadNegocio]

	@Fecha1 date,
	@Fecha2 date,
	@IDSucursal int = null,
	@Tipo int = 0

As

Begin
	
		declare @vCodigo varchar(100)
		declare @vPadre varchar(300)
		declare @vDescripcion varchar (300)
		declare @vFecha date
		declare @vDebito money
		declare @vCredito money
		declare @vIDSucursal int
		Declare @vSuc varchar(16)
		declare @IDUnidadNegocio int
		declare @vSaldo money
		Declare @vPrefijo varchar(100)
		
		--Balance General Tipo = And (SubString(CC.Codigo, 0, 2) = '1' Or SubString(CC.Codigo, 0, 2) = '2' Or SubString(CC.Codigo, 0, 2) = '3')
		--Estado de Resultado Tipo = And (SubString(CC.Codigo, 0, 2) = '4' Or SubString(CC.Codigo, 0, 2) = '5' Or SubString(CC.Codigo, 0, 2) = '6' Or SubString(CC.Codigo, 0, 2) = '7')

		--Crear la tabla temporal
		create table #TablaTemporal(Codigo varchar(100),
									Padre varchar(300),
									Descripcion varchar(300),
									Saldo money,
									IDSucursal int,
									Suc varchar(40),
									IDUnidadNegocio int
									)
																	
		--Insertar datos	
		Begin
		--Inicion Tipo = 0	
		If @Tipo = 0 Begin	
			Declare db_cursor cursor for
				Select 
				Codigo,
				'Padre'=(Select top(1)Padre2 from VCuentaContable v where v.Codigo = VLibroMayor.Codigo),
				Descripcion,
				'Credito'=sum(Credito),
				'Debito'=sum(Debito),
				IDSucursal,
				CodSucursal,
				IDUnidadNegocio
				From VLibroMayor
				Where  Fecha between @Fecha1 and  @Fecha2 
				Group by Codigo, Descripcion, IDSucursal, CodSucursal, IDUnidadNegocio, UnidadNegocio
				Order By Codigo
			Open db_cursor   
			Fetch Next From db_cursor Into	@vCodigo,@vPadre,@vDescripcion,@vCredito,@vDebito,@vIDSucursal, @vSuc, @IDUnidadNegocio
			While @@FETCH_STATUS = 0 Begin 
							
				If @IDSucursal Is Not null Begin If @IDSucursal!=@vIDSucursal GoTo Siguiente End
				
				Set @vPrefijo = SubString(@vCodigo, 0, 2)
				--Cuentas del Debe
				If @vPrefijo = '1' Or @vPrefijo = '5' Begin
					set @vSaldo = @vDebito - @vCredito
				End
				
				--Cuentas del Haber
				If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
					set @vSaldo = @vCredito - @vDebito
				End

				If Exists(Select top(1) Padre from #TablaTemporal where Padre = @vPadre) Begin
					Insert Into  #TablaTemporal(Codigo,Padre,Descripcion,Saldo,IDSucursal, Suc, IDUnidadNegocio) 
									Values (@vCodigo,'',@vDescripcion,@vSaldo,@vIDSucursal, @vSuc, @IDUnidadNegocio)
				End
				If not Exists(Select top(1) Padre from #TablaTemporal where Padre = @vPadre) Begin
					Insert Into  #TablaTemporal(Codigo,Padre,Descripcion,Saldo,IDSucursal, Suc, IDUnidadNegocio) 
									Values (@vCodigo,@vPadre,@vDescripcion,@vSaldo,@vIDSucursal, @vSuc, @IDUnidadNegocio)
					End
				Siguiente:			
				
				Fetch Next From db_cursor Into	@vCodigo,@vPadre,@vDescripcion,@vCredito,@vDebito,@vIDSucursal, @vSuc, @IDUnidadNegocio
				
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
			
		End	
		--Fin Tipo 0

		--Inicion Tipo = 1 Balance General		
		IF @Tipo = 1 Begin
			Declare db_cursor cursor for
				Select 
				Codigo,
				'Padre'=(Select top(1)Padre2 from VCuentaContable v where v.Codigo = VLibroMayor.Codigo),
				Descripcion,
				'Credito'=sum(Credito),
				'Debito'=sum(Debito),
				IDSucursal,
				CodSucursal,
				IDUnidadNegocio
				From VLibroMayor
				Where  Fecha between @Fecha1 and  @Fecha2 
				And (SubString(Codigo, 0, 2) = '1' Or SubString(Codigo, 0, 2) = '2' Or SubString(Codigo, 0, 2) = '3')
				Group by Codigo, Descripcion, IDSucursal, CodSucursal, IDUnidadNegocio, UnidadNegocio
				Order By Codigo
			Open db_cursor   
			Fetch Next From db_cursor Into	@vCodigo,@vPadre,@vDescripcion,@vCredito,@vDebito,@vIDSucursal, @vSuc, @IDUnidadNegocio
			While @@FETCH_STATUS = 0 Begin 
							
				If @IDSucursal Is Not null Begin If @IDSucursal!=@vIDSucursal GoTo Siguiente1 End
				
				Set @vPrefijo = SubString(@vCodigo, 0, 2)
				--Cuentas del Debe
				If @vPrefijo = '1' Or @vPrefijo = '5' Begin
					set @vSaldo = @vDebito - @vCredito
				End
				
				--Cuentas del Haber
				If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
					set @vSaldo = @vCredito - @vDebito
				End

				If Exists(Select top(1) Padre from #TablaTemporal where Padre = @vPadre) Begin
				Insert Into  #TablaTemporal(Codigo,Padre,Descripcion,Saldo,IDSucursal, Suc, IDUnidadNegocio) 
									Values (@vCodigo,'',@vDescripcion,@vSaldo,@vIDSucursal, @vSuc, @IDUnidadNegocio)
				End 
				If not Exists(Select top(1) Padre from #TablaTemporal where Padre = @vPadre) Begin
				Insert Into  #TablaTemporal(Codigo,Padre,Descripcion,Saldo,IDSucursal, Suc, IDUnidadNegocio) 
									Values (@vCodigo,@vPadre,@vDescripcion,@vSaldo,@vIDSucursal, @vSuc, @IDUnidadNegocio)
				End 
				Siguiente1:			
				
				Fetch Next From db_cursor Into	@vCodigo,@vPadre,@vDescripcion,@vCredito,@vDebito,@vIDSucursal, @vSuc, @IDUnidadNegocio
				
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		End
		End
		--Fin Tipo 1 Balance General

		--Inicion Tipo = 1 Estado de resultado	
		IF @Tipo = 2 Begin
			Declare db_cursor cursor for
				Select 
				Codigo,
				'Padre'=(Select top(1)Padre2 from VCuentaContable v where v.Codigo = VLibroMayor.Codigo),
				Descripcion,
				'Credito'=sum(Credito),
				'Debito'=sum(Debito),
				IDSucursal,
				CodSucursal,
				IDUnidadNegocio
				From VLibroMayor
				Where  Fecha between @Fecha1 and  @Fecha2 
				And (SubString(Codigo, 0, 2) = '4' Or SubString(Codigo, 0, 2) = '5' Or SubString(Codigo, 0, 2) = '6' Or SubString(Codigo, 0, 2) = '7')
				Group by Codigo, Descripcion, IDSucursal, CodSucursal, IDUnidadNegocio, UnidadNegocio
				Order By Codigo
			Open db_cursor   
			Fetch Next From db_cursor Into	@vCodigo,@vPadre,@vDescripcion,@vCredito,@vDebito,@vIDSucursal, @vSuc, @IDUnidadNegocio
			While @@FETCH_STATUS = 0 Begin 
							
				If @IDSucursal Is Not null Begin If @IDSucursal!=@vIDSucursal GoTo Siguiente2 End
				
				Set @vPrefijo = SubString(@vCodigo, 0, 2)
				--Cuentas del Debe
				If @vPrefijo = '1' Or @vPrefijo = '5' Begin
					set @vSaldo = @vDebito - @vCredito
				End
				
				--Cuentas del Haber
				If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
					set @vSaldo = @vCredito - @vDebito
				End

				
				If Exists(Select top(1) Padre from #TablaTemporal where Padre = @vPadre) Begin
				Insert Into  #TablaTemporal(Codigo,Padre,Descripcion,Saldo,IDSucursal, Suc, IDUnidadNegocio) 
									Values (@vCodigo,'',@vDescripcion,@vSaldo,@vIDSucursal, @vSuc, @IDUnidadNegocio)
				End 
				If not Exists(Select top(1) Padre from #TablaTemporal where Padre = @vPadre) Begin
				Insert Into  #TablaTemporal(Codigo,Padre,Descripcion,Saldo,IDSucursal, Suc, IDUnidadNegocio) 
									Values (@vCodigo,@vPadre,@vDescripcion,@vSaldo,@vIDSucursal, @vSuc, @IDUnidadNegocio)
				End
				Siguiente2:			
				
				Fetch Next From db_cursor Into	@vCodigo,@vPadre,@vDescripcion,@vCredito,@vDebito,@vIDSucursal, @vSuc, @IDUnidadNegocio
				
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		
		End
		--Fin Tipo 1 Balance General

		
		DECLARE @cols AS NVARCHAR(MAX),
			@query  AS NVARCHAR(MAX);

	SET @cols = STUFF((SELECT distinct ',' + QUOTENAME(c.IDUnidadNegocio) 
            FROM #TablaTemporal c
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

			
			
		set @query = 'SELECT Padre,Codigo, Descripcion, IDSucursal, Suc,' + @cols + ' from 
					(select Padre,Codigo, Descripcion, Saldo, IDSucursal, Suc, IDUnidadNegocio
						from #TablaTemporal) As SourceTable
					pivot 
					(
						avg(Saldo)
						for IDUnidadNegocio in (' + @cols + ')
					) AS PivotTable Order by Codigo,Suc'


		execute(@query)
	
	
End

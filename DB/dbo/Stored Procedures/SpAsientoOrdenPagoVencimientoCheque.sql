﻿CREATE Procedure [dbo].[SpAsientoOrdenPagoVencimientoCheque]

	@IDTransaccion int
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint = 0

	--
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vFecha date
	Declare @vIDMoneda tinyint
	Declare @vIDMonedaComprobante tinyint
	Declare @vCotizacion money
	Declare @vCuentaContableCheque varchar(50)
	Declare @vObservacion varchar(100)
	Declare @vDiferido bit
	Declare @vPagoProveedor bit
	Declare @vAnticipoProveedor bit
	Declare @vEgresoRendir bit
	Declare @vIDCuentaBancaria int
	Declare @vCheque bit
	Declare @vImporteMoneda money
	Declare @vImporteMonedaDocumento money
	Declare @vIDTransaccionOP int
	
	--Variables
	Declare @vCodigoCuentaProveedor varchar(50)
	Declare @vTotal money
	Declare @vTotalDocumento money
	Declare @vDiferenciaCambio money
	Declare @vIDTipoComprobantePago int
	Declare @vAplicarRetencion bit
	Declare @vImporteVencimiento money

	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	
	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money

	Declare @vProveedor as varchar(32)
	Declare @vIDProveedor as int
	
	--Obtener valores
	Begin
	
		Select	@vIDTransaccionOP= EOP.IDTransaccionOP,
				@vIDSucursal=OP.IDSucursal,
				@vTipoComprobante=OP.TipoComprobante,
				@vIDTipoComprobante=OP.IDTipoComprobante,
				@vComprobante=OP.Comprobante,
				@vFecha=EOP.Fecha,
				@vIDMoneda = OP.IDMoneda,
				@vCotizacion = (dbo.FCotizacionAlDiafecha(OP.IDMoneda,0,EOP.Fecha)),
				@vProveedor = OP.Proveedor,
				@vIDProveedor = OP.IDProveedor,
				@vObservacion = OP.Observacion,
				@vIDCuentaBancaria = OP.IDCuentaBancaria,
				@vAnticipoProveedor = OP.AnticipoProveedor,
				@vEgresoRendir = OP.EgresoRendir,
				@vDiferido = OP.Diferido,
				@vTotal = (Case when OP.IDMoneda <> 1 then OP.ImporteMoneda * (dbo.FCotizacionAlDiafecha(OP.IDMoneda,0,EOP.Fecha)) else OP.ImporteMoneda end)
		From VencimientoChequeEmitido EOP 
		join vOrdenPago OP on EOP.IDTransaccionOP = OP.IDTransaccion
		Where EOP.IDTransaccion=@IDTransaccion
	End
	--Verificar que el asiento se pueda modificar
	Begin
	
		If not exists(Select * From VencimientoChequeEmitido Where IDTransaccion=@IDTransaccion) Begin
			print 'La operacion no es un vencimiento de cheque emitido'
			GoTo salir
		End 	
		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		
		if @vAnticipoProveedor = 'True' begin
			print 'la op es un anticipo'
			Goto salir
		end

		if @vEgresoRendir = 'True' begin
			print 'la op es un egreso a rendir'
			Goto salir
		end

		if @vDiferido = 'False' begin
			print 'El cheque no es diferido'
			Goto Salir
		end

	End
				
	--Eliminar primero el asiento
	Begin
		print 'elimina asiento'
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = Concat(@vTipoComprobante,' ',@vComprobante)
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	--Cuentas Fijas proveedor cheque diferido a vencer/banco
		print 'Proveedor'
		Declare @vBuscarEnCuentaBancaria bit = 'False'
		Declare @vTipo as varchar(50) = ''
		Declare cCFCompraProveedor cursor for
		Select Codigo, BuscarEnBanco,Tipo from VCFVencimientoChequeOP
		Where IDMoneda=@vIDMoneda
		Open cCFCompraProveedor   
		fetch next from cCFCompraProveedor into @vCodigo, @vBuscarEnCuentaBancaria, @vTipo
		
		While @@FETCH_STATUS = 0 Begin  
			print 'entro a Proveedor'
			if (@vCodigo = '') begin
				Goto Seguir
			end
			if @vTipo = 'BANCO' begin
				if @vBuscarEnCuentaBancaria = 'True' begin
					Set @vCodigo = (select CodigoCuentaContable from CuentaBancaria where id = @vIDCuentaBancaria)
				end
				Set @vImporteHaber = @vTotal
				Set @vImporteDebe = 0
			end
			else begin
				Set @vImporteHaber = 0
				Set @vImporteDebe = (select Credito from DetalleAsiento where IDTransaccion = @vIDTransaccionOP and CuentaContable=@vCodigo)
				Set @vImporteVencimiento = @vImporteDebe
			end

			--Obtener la cuenta
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
						
			Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
			Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
			
Seguir:
			fetch next from cCFCompraProveedor into @vCodigo, @vBuscarEnCuentaBancaria, @vTipo
			
		End
		
		close cCFCompraProveedor 
		deallocate cCFCompraProveedor

		--Diferencia de Cambio
	Begin
			
		Set @vCodigo= (Select Codigo from VCFOrdenPago Where Tipo ='DIF. CAMBIO' And IDSucursal=@vIDSucursal 
		And IDMoneda=@vIDMoneda And SoloDiferido = 1)
		
		Set @vImporteDebe = 0
		Set @vImporteHaber = 0
			
		Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
		Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				
		Set @vImporte= @vTotal-@vImporteVencimiento
		
		If @vImporte <0 Begin
			set @vImporte = @vImporte * -1
			Set @vImporteHaber = @vImporte
			Set @vImporteDebe = 0
		End			
		else begin
					
			Set @vImporteDebe = @vImporte
			Set @vImporteHaber = 0
		End				
			
		if @vImporteDebe>0 or @vImporteHaber>0 begin
			Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
			Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
		end

	End
						
	--Actualizamos la cabecera, el total
	Begin
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
		Set @vImporteHaber = ROUND(@vImporteHaber, @vRedondeo)
		Set @vImporteDebe = ROUND(@vImporteDebe, @vRedondeo)
	
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vImporteHaber - @vImporteDebe
		Where IDTransaccion=@IDTransaccion
	End

	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
End

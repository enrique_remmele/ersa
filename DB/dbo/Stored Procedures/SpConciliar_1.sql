﻿CREATE Procedure [dbo].[SpConciliar]
--Milner 26-02-2014
	--Entrada
	@IDTransaccion numeric,
	@Conciliado bit,
	@Operacion varchar(10),
	
	--Transaccion	
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
			
As

Begin

	--BLOQUES
	Declare @vObservacion varchar(100)='' 
	Declare @vIDTerminal int = (Select IDTerminal from Transaccion where ID = @IDTransaccion)
	set @IDUsuario = (Select IDUsuario from Transaccion where ID = @IDTransaccion)
	Declare @IDOperacion int = (Select IDOperacion from Transaccion where ID = @IDTransaccion)
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
	
		--DebitoCredito
		if exists(Select * From DebitoCreditoBancario  Where   IDTransaccion=@IDTransaccion ) begin
		  
		   --INICIO SM 15072021 - Auditoria Informática		
             set @vObservacion = concat('Conciliacion Bancaria ',@Conciliado)

		     Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		     Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'DEBITO CREDITO BANCARIO' )
		

			--Actualizamos
			Update DebitoCreditoBancario  Set Conciliado=@Conciliado 
			Where IDTransaccion =@IDTransaccion 	
		
			set @Mensaje = 'Registro procesado!'
			set @Procesado = 'True'
			return @@rowcount
				
		End
		
		--DepositoBancario
		if exists(Select * From DepositoBancario   Where   IDTransaccion=@IDTransaccion) begin

		    --INICIO SM 15072021 - Auditoria Informática		
             set @vObservacion = concat('Conciliacion Bancaria ',@Conciliado)

		     Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		     Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'DEPOSITO BANCARIO' )
		

			--Actualizamos
			Update DepositoBancario  Set Conciliado=@Conciliado                                                        
			Where IDTransaccion =@IDTransaccion 
		
			set @Mensaje = 'Registro guardado!'
			set @Procesado = 'True'
			return @@rowcount
		End
		
		
		--OrdenPago
		if exists(Select * From OrdenPago    Where   IDTransaccion=@IDTransaccion ) begin
			 --INICIO SM 15072021 - Auditoria Informática		
             set @vObservacion = concat('Conciliacion Bancaria ',@Conciliado)

		     Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		     Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'ORDEN PAGO' )
		
			--Actualizamos
			Update OrdenPago Set Conciliado=@Conciliado 
			Where IDTransaccion =@IDTransaccion 
		    
			Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		    Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'CHEQUE' )
		
			Update Cheque Set Conciliado=@Conciliado 
			Where IDTransaccion =@IDTransaccion 
			
			set @Mensaje = 'Registro guardado!'
			set @Procesado = 'True'
			return @@rowcount	
		
	   End
	   
	   --DescuentoCheque
		if exists(Select * From DescuentoCheque     Where   IDTransaccion=@IDTransaccion ) begin
			
			 --INICIO SM 15072021 - Auditoria Informática		
             set @vObservacion = concat('Conciliacion Bancaria ',@Conciliado)

		     Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		     Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'DESCUENTO CHEQUE' )
		
			--Actualizamos
			Update DescuentoCheque Set Conciliado=@Conciliado 
			Where IDTransaccion =@IDTransaccion 
		
		    Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		    Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'CHEQUE CLIENTE' )
		

			Update ChequeCliente  Set Conciliado=@Conciliado 
			Where IDTransaccion =@IDTransaccion 
			
			set @Mensaje = 'Registro guardado!'
			set @Procesado = 'True'
			return @@rowcount	
		
	   End
	   
	    --Cheque Rechazado
			if exists(Select * From ChequeClienteRechazado      Where   IDTransaccion=@IDTransaccion ) begin
				--INICIO SM 15072021 - Auditoria Informática		
                set @vObservacion = concat('Conciliacion Bancaria ',@Conciliado)

		        Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		        Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'CHEQUE CLIENTE RECHAZADO' )
		
				--Actualizamos
				Update ChequeClienteRechazado Set Conciliado=@Conciliado 
				Where IDTransaccion =@IDTransaccion 
			
			    Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		        Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'CHEQUE CLIENTE' )
		
				Update ChequeCliente  Set Conciliado=@Conciliado 
				Where IDTransaccion =@IDTransaccion 
				
				set @Mensaje = 'Registro guardado!'
				set @Procesado = 'True'
				return @@rowcount	
			
		   End
	   
	End
		
		
	
	set @Mensaje = 'No se realizo ninguna operacion! '
	set @Procesado = 'False'
	return @@rowcount

End


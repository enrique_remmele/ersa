﻿CREATE Procedure [dbo].[SpMotivoRechazoCheque]

	--Entrada
	@Descripcion varchar(150),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
As

Begin

	declare @vID tinyint
	
	Set @vID=ISNull((Select MAX(ID+1)From MotivoRechazoCheque),1)
	
	--Si la Descripción ya existe
	If Exists(Select * From MotivoRechazoCheque Where Descripcion=@Descripcion) Begin
		set @Mensaje = 'La Descripción ya existe.'
		set @Procesado = 'False'
		Return @@rowcount
	End
	
	
	Insert Into MotivoRechazoCheque(ID,Descripcion)
						Values(@vID,@Descripcion)	
				
			
	set @Mensaje = 'Registro guardado'
	set @Procesado = 'True'
	return @@rowcount
		

End
	
	


﻿CREATE Procedure [dbo].[SpAsientoMovimientoDescargaCompra]

	@IDTransaccion numeric(18,0)
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	
	--Movimiento
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoOperacion smallint
	Declare @vIDTipoComprobante smallint
	Declare @vIDDepositoEntrada smallint
	Declare @vIDDepositoSalida smallint
	Declare @vEntrada bit
	Declare @vSalida bit
	Declare @vComprobante varchar(50)
	Declare @vFecha varchar(50)
	Declare @vObservacion varchar(100)
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money

	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	Declare @vCodigoDepositoEntrada varchar(50)
	Declare @vCodigoDepositoSalida varchar(50)
	Declare @vCodigoProducto varchar(50)

	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money
	Declare @vBuscarEnProducto bit
	

	Declare @MovimientoStock bit
	Declare @DescargaStock bit
	
	--Obtener valores
	Begin
	
		Set @vIDMoneda = 1
		Set @vCotizacion = 1
		
		Select	@vIDSucursal=IDSucursal,
				@vIDTipoOperacion=IDTipoOperacion,
				@vTipoComprobante=TipoComprobante,
				@vIDTipoComprobante=IDTipoComprobante,
				@vIDDepositoEntrada=IDDepositoEntrada,
				@vIDDepositoSalida=IDDepositoSalida,
				@vEntrada=Entrada,
				@vSalida=Salida,
				@vComprobante=Comprobante,
				@vFecha=Fecha,
				@vObservacion=Observacion
				
		From VMovimiento Where IDTransaccion=@IDTransaccion and (DescargaCompra = 'True') 
		print 'Entra Seccion 1'
	End
					
	--Verificar que el asiento se pueda modificar
	Begin
	
		If not exists(Select * From Movimiento Where IDTransaccion=@IDTransaccion) Begin
			print 'La operacion no es un Movimiento de stock'
			GoTo salir
		End 
		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta anulado
		If (Select Anulado From Movimiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El registro esta anulado'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		print 'Pasa filtros'
	End

	--Verificar si esta configurado
	begin 
		if not exists (select * from VCFMovimiento where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante and [Debe/Haber] = 'DEBE') begin
			goto salir
		end
		if not exists (select * from VCFMovimiento where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante and [Debe/Haber] = 'HABER') begin
			goto salir
		end
	end
				
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
			
		print 'Asientos Eliminados'	
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = @vObservacion
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		print 'Inserta Cabecera'
	End
	
	--Cuentas para Transferencias
	If @vEntrada=1 And @vSalida=1 Begin
		print 'entro en transferencia	'
		--Variables--select * from VCFMovimiento where IDOperacion = 59
		Declare cCFTranferencia cursor for
		Select Codigo, Debe, Haber, BuscarEnProducto From VCFMovimiento 
		Where IDTipoOperacion=@vIDTipoOperacion 
		And IDTipoComprobante=@vIDTipoComprobante
		Open cCFTranferencia
		fetch next from cCFTranferencia into @vCodigo, @vDebe, @vHaber, @vBuscarEnProducto
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporte = isnull((Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion),0)
			--Entrada
			--Cuenta contable Entrada
			
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
						
			
			if @vDebe = 'True' begin
				Declare cCxDetalleMovimiento cursor for
				Select D.CuentaContableEntrada,
				--P.CuentaContableCompra, 
				Sum(D.Total) 
				From vDetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion,
				D.CuentaContableEntrada
				 --P.CuentaContableCompra
				Open cCxDetalleMovimiento 
				fetch next from cCxDetalleMovimiento into @vCodigoProducto, @vImporte
				While @@FETCH_STATUS = 0 Begin  
		
					If @vCodigoProducto != '' Begin
						Set @vCodigo = @vCodigoProducto 
					End

					Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
					if @vImporte > 0 begin
						if exists(select * from detalleasiento where idtransaccion = @IDTransaccion and IDCuentaContable = @vIDCuentaContable) begin
							Update DetalleAsiento
								Set Debito = @vImporte
							Where IDTransaccion = @IDTransaccion 
							and IDCuentaContable = @vIDCuentaContable
						end
						else begin
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, @vImporte, 0, '')
						end
					end

				fetch next from cCxDetalleMovimiento into @vCodigoProducto, @vImporte

				End
		
				close cCxDetalleMovimiento
				deallocate cCxDetalleMovimiento
			end 
			else begin
				Declare cCxDetalleMovimiento cursor for
				Select --P.CuentaContableCompra, 
				D.CuentaContableSalida,
				Sum(D.Total) 
				From vDetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, D.CuentaContableSalida
				--P.CuentaContableCompra
				Open cCxDetalleMovimiento 
				fetch next from cCxDetalleMovimiento into @vCodigoProducto, @vImporte
				While @@FETCH_STATUS = 0 Begin  
		
					If @vCodigoProducto != '' Begin
						Set @vCodigo = @vCodigoProducto 
					End

					Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
					
					If @vImporte > 0 Begin		
						if exists(select * from detalleasiento where idtransaccion = @IDTransaccion and IDCuentaContable = @vIDCuentaContable) begin
							Update DetalleAsiento
								Set Credito = @vImporte
							Where IDTransaccion = @IDTransaccion 
							and IDCuentaContable = @vIDCuentaContable
						end
						else begin
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporte, 0, 0, '')
						end
					End
			
					fetch next from cCxDetalleMovimiento into @vCodigoProducto, @vImporte

				End
		
				close cCxDetalleMovimiento
				deallocate cCxDetalleMovimiento

				--if @vImporte > 0 begin
				--	Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				--	Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporte, 0, 0, '')
				--end
			end
			
			fetch next from cCFTranferencia into @vCodigo, @vDebe, @vHaber, @vBuscarEnProducto
			
		End
		
		close cCFTranferencia
		deallocate cCFTranferencia

	End

	--Cuentas para Entrada
	If @vEntrada=1 And @vSalida=0 Begin
	
		Declare cCFEntrada cursor for
		Select Codigo, Debe, Haber, BuscarEnProducto From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFEntrada 
		fetch next from cCFEntrada into @vCodigo, @vDebe, @vHaber, @vBuscarEnProducto
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			--Cuenta del debe
			If @vDebe = 1 Begin
				
				Declare cCFDetalleMovimiento cursor for
				Select P.CuentaContableCompra, Sum(D.Total) 
				From DetalleMovimiento D 
				Join Producto P On D.IDProducto=P.ID 
				Join TipoProducto TP On P.IDTipoProducto=TP.ID 
				
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCompra
				Open cCFDetalleMovimiento 
				fetch next from cCFDetalleMovimiento into @vCodigoProducto, @vImporte
				While @@FETCH_STATUS = 0 Begin  
		
					If @vCodigoProducto != '' Begin
						Set @vCodigo = @vCodigoProducto 
					End

					Set @vIDCuentaContable = (Select IDCuentaContable from VCuentaContable CC Where CC.Codigo=@vCodigo)

					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0,@vImporte, @vImporte, '')
					
					End
			
					fetch next from cCFDetalleMovimiento into @vCodigoProducto, @vImporte

				End
		
				close cCFDetalleMovimiento
				deallocate cCFDetalleMovimiento

			End
			
			If @vHaber = 1 Begin

				If @vCodigoDepositoEntrada != '' Begin
					Set @vCodigo = @vCodigoDepositoEntrada
				End

				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				Set @vImporteHaber = @vImporte

				If @vImporteHaber > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporte,0, @vImporte, '')
				
				End

			End				
			
			fetch next from cCFEntrada into @vCodigo, @vDebe, @vHaber, @vBuscarEnProducto
			
		End
		
		close cCFEntrada
		deallocate cCFEntrada

	End

	--Cuentas para Salida
	If @vEntrada=0 And @vSalida=1 Begin
	
		print 'Entra al cursor'
		Declare cCFSalida cursor for
		Select Codigo, Debe, Haber, BuscarEnProducto From VCFMovimiento
		Where IDTipoOperacion=@vIDTipoOperacion And IDTipoComprobante=@vIDTipoComprobante
		Open cCFSalida 
		fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber, @vBuscarEnProducto
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			--Cuenta del debe
			print concat('Debe: ',@vDebe, 'TipoComprobante: ',@vIDTipoComprobante, ' BuscarEnProducto: ', @vBuscarEnProducto, ' TipoOperacion:', @vIDTipoOperacion) 
			If @vDebe = 1 and @vIDTipoComprobante <> 89 and @vIDTipoComprobante <> 71 and @vBuscarEnProducto <>1 Begin -- QUe no sea MOVINT
				print 'Entra a cuenta debe'
				print @vCodigoDepositoSalida

				If @vCodigoDepositoSalida != '' Begin
					Set @vCodigo = @vCodigoDepositoSalida
				End
				 
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				
				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				Set @vImporteDebe = @vImporte

				If @vImporteDebe > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, @vImporte, @vImporte, '')
				
				End

			End

			If @vDebe = 1 and @vIDTipoComprobante <> 89 and @vIDTipoComprobante <> 71 and @vBuscarEnProducto = 1 begin
				Declare cCFDetalleMovimientoSalida cursor for
					Select P.CuentaContableCosto, CC.IDCuentaContable, Sum(D.Total) 
					From DetalleMovimiento D Join Producto P On D.IDProducto=P.ID 
					--Join TipoProducto TP On P.IDTipoProducto=TP.ID 
					Join VCuentaContable CC On CC.Codigo=P.CuentaContableCosto 
					Where D.IDTransaccion=@IDTransaccion
					Group By D.IDTransaccion, P.CuentaContableCosto, CC.IDCuentaContable
					Open cCFDetalleMovimientoSalida 
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
					While @@FETCH_STATUS = 0 Begin  
		
						If @vCodigoProducto != '' Begin
							Set @vCodigo = @vCodigoProducto 
						End

						If @vImporte > 0 Begin		
						
							Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

							Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
							Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, @vImporte, @vImporte, '')
					
						End
			
						fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

					End
		
					close cCFDetalleMovimientoSalida
					deallocate cCFDetalleMovimientoSalida
			end
			
			If @vDebe = 1 and @vIDTipoComprobante = 89 Begin -- Solo para MOVINT				 
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				
				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				Set @vImporteHaber = @vImporte

				If @vImporteHaber > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, 0, @vImporte, @vImporte, '')
				
				End

			End

			If @vHaber = 1 and @vIDTipoComprobante = 71 Begin -- Solo para REPRO				 
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				
				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				Set @vImporteHaber = @vImporte

				If @vImporteHaber > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporte,0, @vImporte, '')
				
				End

			End
			
			If @vHaber = 1 and @vIDTipoComprobante <> 71 and @vBuscarEnProducto=1 Begin
				print 'Entra a cuenta Haber'
				Declare cCFDetalleMovimientoSalida cursor for
				Select P.CuentaContableCompra, CC.IDCuentaContable, Sum(D.Total) 
				From DetalleMovimiento D Join Producto P On D.IDProducto=P.ID 
				Join VCuentaContable CC On CC.Codigo=P.CuentaContableCompra
				Where D.IDTransaccion=@IDTransaccion
				Group By D.IDTransaccion, P.CuentaContableCompra, CC.IDCuentaContable
				Open cCFDetalleMovimientoSalida 
				fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte
				While @@FETCH_STATUS = 0 Begin  
					print concat('CodigoProducto: ', @vCodigoProducto, ' IDCuentaContable: ', @vIDCuentaContable,' Importe:', @vImporte)
					If @vCodigoProducto != '' Begin
						Set @vCodigo = @vCodigoProducto 
					End

					If @vImporte > 0 Begin		
						
						Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

						Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
						Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporte, 0, @vImporte, '')
					
					End
			
					fetch next from cCFDetalleMovimientoSalida into @vCodigoProducto, @vIDCuentaContable, @vImporte

				End
		
				close cCFDetalleMovimientoSalida
				deallocate cCFDetalleMovimientoSalida

			End				
			
	If @vHaber = 1 and @vIDTipoComprobante <> 71 and @vBuscarEnProducto <>1 Begin
				print 'Entra a cuenta Haber'
				print @vCodigoDepositoSalida
				
				If @vCodigoDepositoSalida != '' Begin
					Set @vCodigo = @vCodigoDepositoSalida
				End
				 
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
				
				--Hayar el importe
				Set @vImporte = (Select SUM(Total) From DetalleMovimiento Where IDTransaccion=@IDTransaccion)
			
				Set @vImporteHaber = @vImporte

				If @vImporteHaber > 0 Begin		
					
					Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)

					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporte, 0, @vImporte, '')
				
				End
			
			end 

			fetch next from cCFSalida into @vCodigo, @vDebe, @vHaber, @vBuscarEnProducto
			
		End
		
		close cCFSalida
		deallocate cCFSalida

	End

	--Actualizamos la cabecera, el total
	Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
	Set @vImporteHaber = ROUND(@vImporteHaber, 0)
	Set @vImporteDebe = ROUND(@vImporteDebe, 0)
	
	Update Asiento Set Total = @vImporteHaber,
						Credito = @vImporteHaber,
						Debito = @vImporteDebe,
						Saldo = @vImporteHaber - @vImporteDebe
	Where IDTransaccion=@IDTransaccion
	
	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
	
End


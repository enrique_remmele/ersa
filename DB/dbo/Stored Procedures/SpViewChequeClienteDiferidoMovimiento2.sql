﻿

CREATE Procedure [dbo].[SpViewChequeClienteDiferidoMovimiento2]

	--Entrada
	@Mes tinyint,
	@Año Smallint
As

Begin

	--Crear la tabla temporal
    create table #TablaTemporal(ID tinyint,
								IDSucursal int,
								Fecha date,
								FechaSaldo varchar(50),
								Dia varchar(10),
								Ingresos money,
								Depositos money,
								DescuentoCheque money,
								SaldoAnterior money,
								Saldo money,
								Condicion bit)
	
    
    ---Variables para CALCULAR SALDO 
	declare @vSaldo money
	declare @vSaldoAnterior money
	declare @vIngresoSaldo money
	declare @vDepositoSaldo money
	declare @vDctoCheque money
	
	--Declarar variables Variables
	declare @vID tinyint
	declare @vFecha date
	declare @vDia varchar (10)
	declare @vIngreso money
	declare @vDeposito money
	declare @vDescuentoCheque money
	declare @vFechaFiltro date
	declare @vIDSucursal tinyint
	declare @vCondicion bit
	declare @vSaldoAl varchar(50)
	declare @vFechaSaldo date
	print 'aqui 52'
	Set @vFechaFiltro = '01-' + CONVERT(varchar(50), @Mes) + '-' + CONVERT(varchar(50), @Año)
	Set @vFechaSaldo = (SELECT DATEADD(dd, DATEPART(dd, @vFechaFiltro)* -1, @vFechaFiltro))
	Set @vFechaSaldo = CONVERT(varchar(50),@vFechaSaldo)
	Set @vSaldoAl = 'SALDO AL '+''+convert(varchar(50),@vFechaSaldo)
	print 'aqui 57'
				
	Set @vIngresoSaldo  = IsNull((Select Sum(Importe) From VChequeCliente Where Cartera='True' And FechaCobranza Is Not NUll And Diferido ='True' And Fecha < @vFechaFiltro),0)
	Set @vDepositoSaldo  = IsNull((Select Sum(Importe) From VChequeCliente Where FechaCobranza Is Not NUll And Diferido ='True' And Depositado ='True' And IDTransaccion not in(Select IDTransaccionChequeCliente From DetalleDescuentoCheque) And Fecha < @vFechaFiltro ),0)
	Set @vDctoCheque  = IsNull((Select Sum(Importe) From VChequeCliente Where FechaCobranza Is Not NUll And Diferido ='True' And Depositado ='False' And IDTransaccion  in(Select IDTransaccionChequeCliente From DetalleDescuentoCheque) And Fecha < @vFechaFiltro ),0)
		
	Set @vSaldoAnterior = @vIngresoSaldo  - (@vDepositoSaldo  + @vDctoCheque)  
	Set @vID = (Select IsNull(MAX(ID)+1,1) From #TablaTemporal)
	
	--Insertar datos	
	Begin
	
			
		Declare db_cursor cursor for
		
		Select 
		IDSucursal,
		Fecha, 
		--'Dia'=dbo.FDiaSemana(DATEPART(DW,Fecha)),
		'Dia'=dbo.FFormatoDosDigitos(DATEPART(DD,Fecha)),
		Condicion,
		'Ingreso'=Sum(Ingreso),
		'Deposito'=Sum(Deposito),
		'DescuentoCheque'=Sum(DescuentoCheque)
		From VSaldoChequesDiferidos2
		Where month(Fecha)=@Mes and year(Fecha)=@Año 
		Group By Fecha,Condicion,IDSucursal
					
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDSucursal,@vFecha,@vDia,@vCondicion,@vIngreso,@vDeposito,@vDescuentoCheque
		While @@FETCH_STATUS = 0 Begin  
			
			--Hallar Saldo
			Set @vSaldo = (@vSaldoAnterior+@vIngreso)-(@vDeposito+@vDescuentoCheque)  
				
			Insert Into #TablaTemporal(ID,IDSucursal,Fecha,FechaSaldo,Dia,Ingresos,Depositos,DescuentoCheque,SaldoAnterior,Saldo,Condicion)
			Values(@vID,@vIDSucursal,@vFecha,@vSaldoAl,@vDia,@vIngreso,@vDeposito,@vDescuentoCheque,@vSaldoAnterior,@vSaldo,@vCondicion)
			
			--Actualizar Saldo
			Set @vSaldoAnterior = @vSaldo 
			
		Fetch Next From db_cursor Into  @vIDSucursal,@vFecha,@vDia,@vCondicion,@vIngreso,@vDeposito,@vDescuentoCheque
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		
	End	
	
	
	Select * From #TablaTemporal Where ID=@vID 
	Order By Fecha ASC,Ingresos 
	
End
	






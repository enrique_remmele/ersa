﻿CREATE procedure [dbo].[spHechaukaCompraDetalle] 
	@Año smallint,
	@Mes tinyint,
	@Exportador varchar(50)=''
As
Begin

	--Compra
	Select
	'TipoRegistro'= 2,
	'RUCProveedor'= (Case When(CharIndex('-', C.RUC,0)) = 0 Then C.RUC Else (SubString(C.RUC,0, CharIndex('-', C.RUC,0))) End),
	'DVProveedor'=SubString(C.RUC, CharIndex('-', C.RUC,0)+1,1),
	'NombreProveedor'= C.RazonSocial,
	'NumeroTimbrado'= CASE WHEN TC.HechaukaTimbradoReemplazar = 'False' Then C.Timbrado ELSE TC.HechaukaNumeroTimbrado END,
	'TipoDocumento'= TC.HechaukaTipoDocumento,
	'NumeroDocumento'= C.Comprobante,
	'FechaDocumento'= dbo.FFormatoFecha(C.Fecha),
	'MontoCompra10%'= Convert(decimal(18,0), C.[GRAVADO10%]),
	'IVACredito10%'= Convert(decimal(18,0), C.[IVA10%]),
	'MontoCompra5%'= Convert(decimal(18,0), C.[GRAVADO5%]),
	'IVACredito5%'= Convert(decimal(18,0), C.[IVA5%]), 
	'MontoCompraNoGrabada'= Convert(decimal(18,0), C.EXENTO),
	'TipoOperacion'=0,
	'CondicionCompra'=(Case When C.Credito='True' Then 2 Else 1 End),
	'CantidadCuota'=(Case When C.Credito='True' Then 1 Else 0 End)
	
	From VLibroIVA C
	Join VCompra CO On C.IDTransaccion=CO.IDTransaccion
	JOIN VTipoComprobante TC ON C.IDTipocomprobante=TC.ID
	Where YEAR(C.Fecha)=@Año And MONTH(C.Fecha)=@Mes
	And TC.LibroCompra='True'

	Union all

	--NotaDebito
	Select
	'TipoRegistro'= 2,
	'RUCProveedor'= (Case When(CharIndex('-', C.RUC,0)) = 0 Then C.RUC Else (SubString(C.RUC,0, CharIndex('-', C.RUC,0))) End),
	'DVProveedor'=SubString(C.RUC, CharIndex('-', C.RUC,0)+1,1),
	'NombreProveedor'= NDP.Proveedor,
	'NumeroTimbrado'= CONVERT(VARCHAR(50), NroTimbrado),
	'TipoDocumento'= TC.HechaukaTipoDocumento,
	'NumeroDocumento'= C.Comprobante,
	'FechaDocumento'= dbo.FFormatoFecha(C.Fecha),
	'MontoCompra10%'= C.[GRAVADO10%],
	'IVACredito10%'= C.[IVA10%],
	'MontoCompra5%'= C.[GRAVADO5%],
	'IVACredito5%'= C.[IVA5%], 
	'MontoCompraNoGrabada'= C.EXENTO,
	'TipoOperacion'=0,
	'CondicionCompra'=(Case When C.Credito='True' Then 2 Else 1 End),
	'CantidadCuota'=(Case When C.Credito='True' Then 1 Else 0 End)
		
	From VLibroIVA C
	Join VNotaDebitoProveedor NDP On C.IDTransaccion=NDP.IDTransaccion 
	JOIN VTipoComprobante TC ON NDP.IDTipocomprobante=TC.ID
	Where YEAR(C.Fecha)=@Año And MONTH(C.Fecha)=@Mes
	AND TC.LibroCompra='True'
	
	Union All

	--NotaCredito Emitidas
	Select
	'TipoRegistro'= 2,
	'RUCProveedor'= (Case When(CharIndex('-', C.RUC,0)) = 0 Then C.RUC Else (SubString(C.RUC,0, CharIndex('-', C.RUC,0))) End),
	'DVProveedor'=(Case When(CharIndex('-', C.RUC,0)) = 0 Then '' Else SubString(C.RUC, CharIndex('-', C.RUC,0)+1,1) End),
	'NombreProveedor'= C.RazonSocial,
	'NumeroTimbrado'= PE.Timbrado,
	'TipoDocumento'= TC.HechaukaTipoDocumento,
	'NumeroDocumento'= C.Comprobante,
	'FechaDocumento'= dbo.FFormatoFecha(C.Fecha),
	'MontoCompra10%'= C.[GRAVADO10%]*-1,
	'IVACredito10%'= C.[IVA10%]*-1,
	'MontoCompra5%'= C.[GRAVADO5%]*-1,
	'IVACredito5%'= C.[IVA5%]*-1, 
	'MontoCompraNoGrabada'= C.EXENTO*-1,	
	'TipoOperacion'=0,
	'CondicionCompra'=(Case When C.Credito='True' Then 2 Else 1 End),
	'CantidadCuota'=(Case When C.Credito='True' Then 1 Else 0 End)
			
	From VLibroIVA C
	Join VNotaCredito NCP On C.IDTransaccion=NCP.IDTransaccion
	JOIN PuntoExpedicion PE ON NCP.IDPuntoExpedicion=PE.ID
	JOIN VTipoComprobante TC ON NCP.IDTipocomprobante=TC.ID
	Where YEAR(C.Fecha)=@Año And MONTH(C.Fecha)=@Mes
	AND NCP.Anulado='False' 
		
	Union All

	--Gasto
	Select
	'TipoRegistro'= 2,
	'RUCProveedor'= (Case When(CharIndex('-', C.RUC,0)) = 0 Then C.RUC Else (SubString(C.RUC,0, CharIndex('-', C.RUC,0))) End),
	'DVProveedor'=SubString(C.RUC, CharIndex('-', C.RUC,0)+1,1),
	'NombreProveedor'= G.Proveedor,
	'NumeroTimbrado'= CASE WHEN TC.HechaukaTimbradoReemplazar = 'False' Then NroTimbrado ELSE TC.HechaukaNumeroTimbrado END,
	'TipoDocumento'= TC.HechaukaTipoDocumento,
	'NumeroDocumento'= C.Comprobante,
	'FechaDocumento'= dbo.FFormatoFecha(C.Fecha),
	'MontoCompra10%'= C.[GRAVADO10%],
	'IVACredito10%'= C.[IVA10%],
	'MontoCompra5%'= C.[GRAVADO5%],
	'IVACredito5%'= C.[IVA5%], 
	'MontoCompraNoGrabada'= C.EXENTO,
	'TipoOperacion'=0,
	'CondicionCompra'=(Case When C.Credito='True' Then 2 Else 1 End),
	'CantidadCuota'=(Case When C.Credito='True' Then 1 Else 0 End)
		
	From VLibroIVA C
	Join VGasto G On C.IDTransaccion=G.IDTransaccion
	JOIN TipoComprobante TC ON G.IDTipoComprobante=TC.ID
	JOIN VDetalleImpuestoDesglosadoTotales2 DI ON G.IDTransaccion=DI.IDTransaccion
	Where YEAR(C.Fecha)=@Año And MONTH(C.Fecha)=@Mes
	AND TC.LibroCompra='True'

	Union All
	
	--Comprobante de Libro IVA
	Select
	'TipoRegistro'= 2,
	'RUCProveedor'= (Case When(CharIndex('-', C.RUC,0)) = 0 Then C.RUC Else (SubString(C.RUC,0, CharIndex('-', C.RUC,0))) End),
	'DVProveedor'=SubString(C.RUC, CharIndex('-', C.RUC,0)+1,1),
	'NombreProveedor'= C.RazonSocial,
	'NumeroTimbrado'= CASE WHEN TC.HechaukaTimbradoReemplazar = 'False' Then NroTimbrado ELSE TC.HechaukaNumeroTimbrado END,
	'TipoDocumento'= TC.HechaukaTipoDocumento,
	'NumeroDocumento'= C.Comprobante,
	'FechaDocumento'= dbo.FFormatoFecha(C.Fecha),
	'MontoCompra10%'= C.[GRAVADO10%],
	'IVACredito10%'= C.[IVA10%],
	'MontoCompra5%'= C.[GRAVADO5%],
	'IVACredito5%'= C.[IVA5%], 
	'MontoCompraNoGrabada'= C.EXENTO,
	'TipoOperacion'=0,
	'CondicionCompra'=(Case When C.Credito='True' Then 2 Else 1 End),
	'CantidadCuota'=(Case When C.Credito='True' Then 1 Else 0 End)
	
	From VLibroIVA C
	Join VComprobanteLibroIVA G On C.IDTransaccion=G.IDTransaccion
	JOIN TipoComprobante TC ON G.IDTipoComprobante=TC.ID
	JOIN VDetalleImpuestoDesglosadoTotales2 DI ON G.IDTransaccion=DI.IDTransaccion
	Where YEAR(C.Fecha)=@Año And MONTH(C.Fecha)=@Mes
	AND TC.LibroCompra='True'

End


﻿CREATE Procedure [dbo].[SpDetalleAsientoActualizar]

		--Entrada
	@IDTransaccion varchar(10)
		 
As

Begin

	
	Begin
		Declare @vIDTransaccion as numeric(18,0)
		Declare @vIDCuentaContable as numeric(18,0)
		Declare @vCodigo as varchar(50)
		Declare @vIDUnidadNegocio as numeric(18,0)
		Declare @vIDCentroCosto as numeric(18,0)

		Declare db_cursor cursor for
		Select	IDTransaccion, CuentaContable
		From DetalleAsiento
		Where IDTransaccion = @IDTransaccion

		Open db_cursor   

		Fetch Next From db_cursor Into @vIDTransaccion, @vCodigo
		While @@FETCH_STATUS = 0 Begin 
			
			Select @vIDUnidadNegocio = IsNull(IDUnidadNegocio,0),
				   @vIDCentroCosto = Isnull(IDCentroCosto,0),
				   @vIDCuentaContable = ID
			From CuentaContable
			Where Codigo = @vCodigo

			Update DetalleAsiento Set IDUnidadNegocio = @vIDUnidadNegocio,
									  IDCentroCosto = @vIDCentroCosto,
									  IDCuentaContable = @vIDCuentaContable
			Where IDTransaccion = @vIDTransaccion And CuentaContable = @vCodigo

			Fetch Next From db_cursor Into @vIDTransaccion, @vCodigo

		End

		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor

	End

End

﻿CREATE Procedure [dbo].[SpAutorizacionExcepcion]
	--Entrada
	@IDProducto int,
	@IDCliente int,
	@IDTipoDescuento int,
	@IDListaPrecio int,
	@Desde date,
	@Hasta date,
	@Valor varchar(20),
	@IDUsuario int,
	@IDTerminal int = 0,
	@autorizado bit = 'False' output
As
Begin
		
	Declare @vIDListaPrecio int
	Declare @vListaPrecio varchar(50)
	Declare @vIDSucursal int
	Declare @vDescuento money
	Declare @vCantidad decimal(18,3)
	Declare @vMensaje varchar(300)
	Declare @vProcesado bit = 0
	Declare @Precio decimal(18,6)
	Declare @vPorcentaje decimal(9,6)
	Declare @vIDMoneda tinyint


  if  @valor = 'APROBAR' begin	
	  if @IDTerminal = 0 begin
		goto Salir
	  end

		----Actualizar
		Set @vListaPrecio = (select Descripcion from ListaPrecio where id = @IDListaPrecio)

		Declare cCFVentaCliente cursor for
		Select L.ID, L.IDSucursal, S.CantidadLimite, S.Descuento
		From vSolicitudProductoListaPrecioExcepciones S
		join ListaPrecio L on L.Descripcion = S.ListaPrecio
		where IDProducto = @IDProducto
		and IDCliente = @IDCliente
		and IDTipoDescuento = @IDTipoDescuento
		and Desde = @Desde
		and Hasta = @Hasta
		and ListaPrecio = @vListaPrecio
		and L.IDSucursal = 1
		--and Descuento = @vDescuento
		--and S.Estado = 'APROBADO'
		and S.Estado = 'PENDIENTE'
		
		Open cCFVentaCliente   
		fetch next from cCFVentaCliente into @vIDListaPrecio, @vIDSucursal, @vCantidad, @vDescuento
		
		While @@FETCH_STATUS = 0 Begin  
			--SE ELIMINA IDLISTAPRECIO DEL VERIFICADOR  PORQUE PERMITE INTRODUCIR VARIAS VECES EL MISMO DESCUENTO
			--if not exists(select * from ProductoListaPrecioExcepciones where isnull(IDTransaccionPedido,0)= 0 and idproducto = @IDProducto and IDListaPrecio = @vIDListaPrecio and IDCliente = @IDCliente and IDTipoDescuento = @IDTipoDescuento and Hasta >= @Desde) begin
			Update SolicitudProductoListaPrecioExcepciones 
				Set Estado = 1,
				IDUsuarioAprobado= @IDUsuario,
				FechaAprobado = getdate(),
				IDTerminalAprobado = @IDTerminal
				Where IDProducto = @IDProducto
				and IDCliente = @IDCliente
				and IDTipoDescuento = @IDTipoDescuento
				and IDListaPrecio = @IDListaPrecio
				--and Descuento = @vDescuento
				and Desde = @Desde
				and Hasta = @Hasta
				and Estado is null
				--Eliminar Descuentos vigentes anteriores
				print 'Eliminar Descuentos vigentes anteriores'
			EXECUTE SpEliminarProductoListaPrecioExcepciones
				   @IDProducto = @IDProducto
				  ,@IDCliente = @IDCliente
				  ,@IDUsuario = @IDUsuario
				  ,@IDSucursal = @vIDSucursal
				  ,@IDTerminal = @IDTerminal
				  print 'Registros eliminados'
			if not exists(select * from ProductoListaPrecioExcepciones where (isnull(IDTransaccionPedido,0)= 0) and idproducto = @IDProducto and IDCliente = @IDCliente and IDTipoDescuento = @IDTipoDescuento and Hasta >= @Desde) begin

				exec SpProductoListaPrecioExcepciones @IDProducto = @IDProducto, 
													@IDListaPrecio = @vIDListaPrecio,
													@IDCliente=@IDCliente,
													@IDTipoDescuento=@IDTipoDescuento,
													@ImporteDescuento=@vDescuento,
													@Desde = @Desde,
													@Hasta = @Hasta,
													@Cantidad = @vCantidad,
													@Operacion = 'INS',
													@IDUsuario = @IDUsuario,
													@IDSucursal = @vIDSucursal
			
				print concat(@vIDSucursal, ' INS', '|', @vProcesado, '|',@vMensaje)
			end
			else begin

				Set @vIDMoneda = (Select top(1) isnull(Min(IDMoneda),0) From ProductoListaPrecio Where IDListaPrecio=@IDListaPrecio And IDProducto=@IDProducto)
				Set @Precio = (Select Top(1) isnull(Precio,0) From ProductoListaPrecio Where IDListaPrecio=@IDListaPrecio And IDProducto=@IDProducto And IDMoneda=@vIDMoneda)
	
				Set @vPorcentaje = Round((@vDescuento / @Precio) * 100, 6)

				--Actualizar
				Update ProductoListaPrecioExcepciones Set Porcentaje=@vPorcentaje,
													Descuento=@vDescuento,
													IDTipoDescuento=@IDTipoDescuento,
													Desde=@Desde,
													Hasta=@Hasta,
													CantidadLimite=@vCantidad
				Where IDMoneda=@vIDMoneda and (isnull(IDTransaccionPedido,0)= 0) 
				and idproducto = @IDProducto and IDCliente = @IDCliente and IDTipoDescuento = @IDTipoDescuento and Hasta >= @Desde

				--Insertar auditoria despues de actualizar
				Insert Into aProductoListaPrecioExcepciones(IDProducto, IDCliente, IDListaPrecio, IDMoneda, IDSucursal, IDTipoDescuento, Descuento, Porcentaje, Desde, Hasta, CantidadLimite, CantidadLimiteSaldo, IdUsuario, FechaModificacion, accion)
				Values(@IDProducto, @IDCliente, @IDListaPrecio, @vIDMoneda, @vIDSucursal, @IDTipoDescuento, @vDescuento, @vPorcentaje, @Desde, @Hasta, @vCantidad, 0, @IDUsuario, GETDATE(),'UPD')		
		
			end
			set @vMensaje = ''
			set @vProcesado = 0
			

		fetch next from cCFVentaCliente into @vIDListaPrecio, @vIDSucursal, @vCantidad, @vDescuento
			
		End
		
		close cCFVentaCliente 
		deallocate cCFVentaCliente
	
  end

  if @valor = 'RECHAZAR' begin
  
	if @IDTerminal = 0 begin
		goto Salir
	  end
		
		----Actualizar
		Update SolicitudProductoListaPrecioExcepciones 
		Set Estado = 0,
		IDUsuarioAprobado= @IDUsuario,
		FechaAprobado = getdate(),
		IDTerminalAprobado = @IDTerminal
		Where IDProducto = @IDProducto
		and IDCliente = @IDCliente
		and IDTipoDescuento = @IDTipoDescuento
		and IDListaPrecio = @IDListaPrecio
		and Desde = @Desde
		and Hasta = @Hasta
		--and Descuento = @vDescuento
		and Estado is null
		
  end

  if @valor = 'ELIMINAR' begin	

  print 'entra a eliminar'
		----Actualizar
		Delete from  SolicitudProductoListaPrecioExcepciones 
		Where IDProducto = @IDProducto
		and IDCliente = @IDCliente
		and IDTipoDescuento = @IDTipoDescuento
		and IDListaPrecio = @IDListaPrecio
		and Desde = @Desde
		and Hasta = @Hasta
		--and Descuento = @vDescuento

		
		Set @vListaPrecio = (select Descripcion from ListaPrecio where id = @IDListaPrecio)
		
		if Exists(Select * From vProductoListaPrecioExcepciones Where IDProducto=@IDProducto And IDCliente=@IDCliente And ListaPrecio=@vListaPrecio and IDTipoDescuento=@IDTipoDescuento and Desde = @Desde and Hasta=@Hasta) begin
		
			Declare cCFVentaCliente cursor for
			Select L.ID, L.IDSucursal, S.CantidadLimite, S.Descuento
			From vSolicitudProductoListaPrecioExcepciones S
			join ListaPrecio L on L.Descripcion = @vListaPrecio
			where IDProducto = @IDProducto
			and IDCliente = @IDCliente
			and IDTipoDescuento = @IDTipoDescuento
			and Desde = @Desde
			and Hasta = @Hasta
			and Descuento = @vDescuento
			and ListaPrecio = @vListaPrecio

			Open cCFVentaCliente   
			fetch next from cCFVentaCliente into @vIDListaPrecio, @vIDSucursal, @vCantidad, @vDescuento
		
			While @@FETCH_STATUS = 0 Begin  

				exec SpProductoListaPrecioExcepciones @IDProducto = @IDProducto, 
													@IDListaPrecio = @vIDListaPrecio,
													@IDCliente=@IDCliente,
													@IDTipoDescuento=@IDTipoDescuento,
													@ImporteDescuento=@vDescuento,
													@Desde = @Desde,
													@Hasta = @Hasta,
													@Cantidad = @vCantidad,
													@Operacion = 'DEL',
													@IDUsuario = @IDUsuario,
													@IDSucursal = @vIDSucursal,
													@Mensaje = @vMensaje out,
													@Procesado = @vProcesado out
			
				print concat(@vIDSucursal, ' INS', '|', @vProcesado, '|',@vMensaje)



			fetch next from cCFVentaCliente into @vIDListaPrecio, @vIDSucursal, @vCantidad, @vDescuento
			
			End
		
			close cCFVentaCliente 
			deallocate cCFVentaCliente
	
	end
	else begin
	print 'no'
	end

		
  end


  if @valor = 'RECUPERAR' begin	
		----Actualizar
		Update SolicitudProductoListaPrecioExcepciones 
		Set Estado = null,
		IDUsuarioAprobado= null,
		FechaAprobado = null,
		IDTerminalAprobado = null
		Where IDProducto = @IDProducto
		and IDCliente = @IDCliente
		and IDTipoDescuento = @IDTipoDescuento
		and IDListaPrecio = @IDListaPrecio
		and Desde = @Desde
		and Hasta = @Hasta
		--and Descuento = @vDescuento
		and Estado is not null
		

		Set @vListaPrecio = (select Descripcion from ListaPrecio where id = @IDListaPrecio)
		
		if Exists(Select * From vProductoListaPrecioExcepciones Where IDProducto=@IDProducto And IDCliente=@IDCliente And ListaPrecio=@vListaPrecio and IDTipoDescuento=@IDTipoDescuento and Desde = @Desde and Hasta=@Hasta) begin
		
			Declare cCFVentaCliente cursor for
			Select L.ID, L.IDSucursal, S.CantidadLimite, S.Descuento
			From vSolicitudProductoListaPrecioExcepciones S
			join ListaPrecio L on L.Descripcion = @vListaPrecio
			where IDProducto = @IDProducto
			and IDCliente = @IDCliente
			and IDTipoDescuento = @IDTipoDescuento
			and Desde = @Desde
			and Hasta = @Hasta
			and Descuento = @vDescuento
			and ListaPrecio = @vListaPrecio

			Open cCFVentaCliente   
			fetch next from cCFVentaCliente into @vIDListaPrecio, @vIDSucursal, @vCantidad, @vDescuento
		
			While @@FETCH_STATUS = 0 Begin  

				exec SpProductoListaPrecioExcepciones @IDProducto = @IDProducto, 
													@IDListaPrecio = @vIDListaPrecio,
													@IDCliente=@IDCliente,
													@IDTipoDescuento=@IDTipoDescuento,
													@ImporteDescuento=@vDescuento,
													@Desde = @Desde,
													@Hasta = @Hasta,
													@Cantidad = @vCantidad,
													@Operacion = 'DEL',
													@IDUsuario = @IDUsuario,
													@IDSucursal = @vIDSucursal,
													@Mensaje = @vMensaje out,
													@Procesado = @vProcesado out
			
				print concat(@vIDSucursal, ' INS', '|', @vProcesado, '|',@vMensaje)



			fetch next from cCFVentaCliente into @vIDListaPrecio, @vIDSucursal, @vCantidad, @vDescuento
			
			End
		
			close cCFVentaCliente 
			deallocate cCFVentaCliente
	
	end
	else begin
	print 'no'
	end


  end

  salir:

  set @autorizado = 'True'
End



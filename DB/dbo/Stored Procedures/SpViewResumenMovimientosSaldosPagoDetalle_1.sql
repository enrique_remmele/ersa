﻿

CREATE Procedure [dbo].[SpViewResumenMovimientosSaldosPagoDetalle]

	--Entrada
	@Fecha1 Date,
	@Fecha2 Date,
	@IDMoneda int = 1	
	
As

Begin
	--Crear la tabla temporal
    create table #TablaTemporal(ID tinyint,
								IDTransaccion int,
								IDProveedor int,
								Proveedor varchar(50),
								Referencia varchar(50),
								Saldo money,
								SaldoAnterior money,
								Compras money,
								Pagos money,
								Debitos money,
								Creditos money,
								IDTipoProveedor int,
								TipoProveedor varchar(50),
								IDMoneda int,
								primary key(IDTransaccion,ID))
    
    --Variables para calcular saldo
	declare @vSaldo money
	declare @vTotalEgreso money
	declare @vTotalNotaDebitoProveedor money
	declare @vTotalOrdenPagoEgreso money
	declare @vTotalNotaCreditoProveedor money
	
	--Variables para calcular saldo anterior
	declare @vTotalEgresoA money
	declare @vTotalNotaDebitoProveedorA money
	declare @vTotalOrdenPagoEgresoA money
	declare @vTotalNotaCreditoProveedorA money
	declare @vSaldoAnterior money
	
	
	declare @vCompras money
	declare @vDevoluciones money
	declare @vPagos money
	declare @vDebitos money
	declare @vCreditos money
	
	--Declarar variables
	declare @vID tinyint
	declare @vIDTransaccion Numeric (18,0)
	declare @vIDProveedor int
	declare @vProveedor varchar (100)
	declare @vTipoProveedor varchar(50)
	declare @vReferencia varchar (50)
	declare @vIDTipoProveedor int
	declare @vIDMoneda int
	
	set @vID = (Select IsNull(MAX(ID)+1,1) From #TablaTemporal )
	
	--Insertar datos	
	Begin
	
		Declare db_cursor cursor for
		
		Select 
		C.IDTransaccion, 
		P.ID,
		P.RazonSocial,
		P.Referencia,
		C.Saldo,
		P.IDTipoProveedor,
		P.TipoProveedor,
		C.IDMoneda
		From VProveedor P
		Join VCompraGasto C On C.IDProveedor=P.ID 
					
		Open db_cursor   
		Fetch Next From db_cursor Into  @vIDTransaccion,@vIDProveedor,@vProveedor,@vReferencia,@vSaldo,@vIDTipoProveedor,@vTipoProveedor,@vIDMoneda
		While @@FETCH_STATUS = 0 Begin 
		
			--Hallar Totales para Saldo Anterior
			set @vTotalEgresoA  = IsNull((Select Sum(Total) From VCompraGasto Where IDProveedor=@vIDProveedor And Fecha < @Fecha1),0)
			Set @vTotalNotaDebitoProveedorA = IsNull((Select Sum(Total) From VNotaDebitoProveedor Where IDProveedor=@vIDProveedor And Fecha < @Fecha1),0)
			Set @vTotalOrdenPagoEgresoA = IsNull((Select Sum(Importe) From VOrdenPagoEgreso  Where IDProveedor=@vIDProveedor And Fecha < @Fecha1),0)
			Set @vTotalNotaCreditoProveedorA = IsNull((Select  Sum(Total) From VNotaCreditoProveedor   Where IDProveedor=@vIDProveedor And Fecha < @Fecha1),0)	
			
			--Hallar Saldo Anterior
			Set @vSaldoAnterior = (@vTotalEgresoA + @vTotalNotaDebitoProveedorA) - (@vTotalOrdenPagoEgresoA + @vTotalNotaCreditoProveedorA) 
			
			--Hallar
			set @vTotalEgreso  = IsNull((Select Sum(Total) From VCompraGasto Where IDProveedor=@vIDProveedor And Fecha between @Fecha1 and @Fecha2),0)
			Set @vTotalNotaDebitoProveedor = IsNull((Select Sum(Total) From VNotaDebitoProveedor Where IDProveedor=@vIDProveedor And Fecha between @Fecha1 and @Fecha2),0)
			Set @vTotalOrdenPagoEgreso = IsNull((Select Sum(Importe) From VOrdenPagoEgreso  Where IDProveedor=@vIDProveedor And Fecha between @Fecha1 and @Fecha2),0)
			Set @vTotalNotaCreditoProveedor = IsNull((Select  Sum(Total) From VNotaCreditoProveedor   Where IDProveedor=@vIDProveedor And Fecha between @Fecha1 and @Fecha2),0)
			
			--Hallar Saldo			
			set @vSaldo = (@vSaldoAnterior + @vTotalEgreso + @vTotalNotaDebitoProveedor) - (@vTotalOrdenPagoEgreso + @vTotalNotaCreditoProveedor)
			
		
			Insert Into  #TablaTemporal(ID,IDTransaccion,IDProveedor,Proveedor,Referencia,Saldo,SaldoAnterior ,Compras,Pagos,Debitos,Creditos,IDTipoProveedor,TipoProveedor,IDMoneda) 
								Values (@vID,@vIDTransaccion,@vIDProveedor,@vProveedor,@vReferencia,@vSaldo,@vSaldoAnterior,@vTotalEgreso,@vTotalOrdenPagoEgreso,@vTotalNotaDebitoProveedor,@vTotalNotaCreditoProveedor,@vIDTipoProveedor,@vTipoProveedor,@vIDMoneda)
			
			Fetch Next From db_cursor Into   @vIDTransaccion,@vIDProveedor,@vProveedor,@vReferencia,@vSaldo,@vIDTipoProveedor,@vTipoProveedor,@vIDMoneda
			
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		
	End	
	
	Select 
	Referencia,
	Proveedor,
	'Saldo'=sum(Saldo),
	'SaldoAnterior'=SUM(SaldoAnterior),
	'Compras'=Sum(Compras),
	'Pagos'=Sum(Pagos),
	'Debitos'=Sum(Debitos),
	'Creditos'=Sum(Creditos),
	TipoProveedor,
	IDMoneda  
	From #TablaTemporal 
	Where ID=@vID 
	group by Referencia,
			Proveedor,
			
			TipoProveedor,
			IDMoneda
	
End
	











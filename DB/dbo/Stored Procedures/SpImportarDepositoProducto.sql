﻿CREATE Procedure [dbo].[SpImportarDepositoProducto]

	--Cabecera
	@CodigoProducto varchar(100),
	@Deposito varchar(100),
	@Actualizar bit
	
		
As

Begin
		
	--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		Declare @vOperacion varchar(50)
		Declare @vIDTransaccion int
		Declare @vIDDeposito int
		Declare @vIDProducto int
					
	End
	
	--Establecer valores predefinidos
	Begin
		
		Set @vOperacion = 'INS'

	End
	
	
	--Hayar Valores
	Begin
		
		Set @vMensaje = 'No se procesó!'
		Set @vProcesado = 'False'
		
		--validar
		--Tipo de Comprobante
		Set @vIDDeposito = (select isnull(ID,0) from Deposito where Descripcion = @Deposito)
		Set @vIDProducto = (select isnull(ID,0) from Producto where Referencia = @CodigoProducto)

		If not exists((select ID from Deposito where Descripcion = @Deposito)) Begin
			Set @vMensaje = 'No se encontro el Deposito!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		If not exists((select ID from Producto where referencia = @CodigoProducto)) Begin
			Set @vMensaje = 'No se encontro el producto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		
		--Actualizamos la referencia del proveedor
		Update Producto Set IDDeposito = @vIDDeposito
		Where ID=@vIDProducto
		Set @vMensaje = 'Procesado correctamente!'
		Set @vProcesado = 'True'
	End

Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End



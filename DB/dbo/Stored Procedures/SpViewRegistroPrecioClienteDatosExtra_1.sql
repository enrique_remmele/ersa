﻿CREATE Procedure [dbo].[SpViewRegistroPrecioClienteDatosExtra]

	@Año smallint,
	@CodigoCliente varchar(50)
	
	
As

Begin  
			Select 
			'Cantidad de Entregas'=count(distinct LD.IDTransaccion),
			'Promedio antiguedad de facturas pagadas desde su vencimiento'=0,
			'Mes'=month(LD.Fecha)
			From LoteDistribucion LD
			Join VentaLoteDistribucion VLD on LD.IDTransaccion = VLD.IDTransaccionLote
			Join Venta V on VLD.IDTransaccionVenta = V.IDTransaccion
			join Cliente C on v.IDCliente  = C.ID
			Where  year(LD.Fecha) =@Año
			and LD.Anulado = 0 
			and C.Referencia =@CodigoCliente
			group by month(LD.Fecha)
			
			union all
			
			Select
			'Cantidad de Entregas'=a.CE,
			'Promedio Dias de Vencimiento'=Avg(a.PDV),
			a.Mes
			from
			(Select 
			'CE'=0,
			'PDV'=Datediff(D,Isnull(V.FechaVencimiento, V.FechaEmision),CC.FechaEmision),
			'Mes'=month(CC.FechaEmision)
			From VentaCobranza VC
			Join CobranzaCredito CC on VC.IDTransaccionCobranza = CC.IDTransaccion
			join Venta V on VC.IDTransaccionVenta = V.IDTransaccion
			join Cliente C on CC.IDCliente  = C.ID
			Where  year(CC.FechaEmision) =@Año
			and CC.Anulado = 0 and CC.Procesado=1
			and C.Referencia =@CodigoCliente) 
			as a
			group by a.Mes, a.CE
		
	
End

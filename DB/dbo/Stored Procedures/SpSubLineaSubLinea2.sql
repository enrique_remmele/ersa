﻿CREATE Procedure [dbo].[SpSubLineaSubLinea2]

	--Entrada
	@IDSubLinea smallint,
	@IDSubLinea2 smallint,
	@Relacionar bit
	
				
As

Begin

	--BLOQUES
	
	--RELACIONAR
	if @Relacionar='True' begin
		
		--Solo agregar si no existen
		If Exists(Select * From SubLineaSubLinea2 Where IDSubLinea=@IDSubLinea And IDSubLinea2=@IDSubLinea2) Begin
			return @@rowcount
		End
		
		--Insertamos
		Insert Into SubLineaSubLinea2(IDSubLinea, IDSubLinea2)
		Values(@IDSubLinea, @IDSubLinea2)		
		
		return @@rowcount
			
	End
	
	--NO RELACIONAR
	if @Relacionar='False' begin
		
		Delete From SubLineaSubLinea2
		Where IDSubLinea=@IDSubLinea And IDSubLinea2=@IDSubLinea2
		
		return @@rowcount
			
	End

End


﻿CREATE Procedure [dbo].[SpPedidoProcesar]

	--Entrada
	@IDTransaccion numeric(18,0),
	@EsMovil bit = 'False'	
As

Begin

	--Variables
	Declare @Mensaje varchar(200)
	Declare @Procesado bit
	Declare @vNumero int
	Declare @vCantidad decimal(18,0)
	Declare @vIDProducto int
	Declare @vIDDeposito tinyint
	Declare @vIDCliente int
	Declare @vIDSucursal int
	Declare @vPedidoPermitirCantidadNegativa bit
	Declare @vIDFormaPagoFactura int 
	Declare @vIDListaPrecio int
	Declare @vDescuentoUnitario money
	Declare @vFechaFacturar date

	--Obtener valores
	Set @vIDCliente = (Select IDCliente From Pedido Where IDTransaccion=@IDTransaccion)
	set @vIDSucursal = (Select IDSucursal  from Pedido where IDTransaccion= @IDTransaccion)
	Set @vPedidoPermitirCantidadNegativa = ISNull((Select Top(1) PedidoPermitirCantidadNegativa From Configuraciones Where IDSucursal=@vIDSucursal), 'False')
	Set @vIDFormaPagoFactura = (Select IDFormaPagoFactura From Pedido Where IDTransaccion=@IDTransaccion)
	Set @vIDListaPrecio = isnull((select IDListaPrecio from Pedido where IDTransaccion = @IDTransaccion),0)
	Set @vFechaFacturar = (Select FechaFacturar from Pedido where IDTransaccion = @IDTransaccion)
	Set @Mensaje = 'No se realizo ninguna accion'
	Set @Procesado = 'False'

	
	--ActualizarStock correcto
	Update detallepedido set IDDeposito = (select IDDeposito from Pedido where IDTransaccion = @IDTransaccion)
	where IDTransaccion = @IDTransaccion
		
	--Validar
	--Pedido
	Begin

		--Transaccion
		If Not Exists(Select * From Pedido Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'Error: El sistema no encuentra el registro de Pedido! Intente nuevamente.'
			set @Procesado = 'False'
			Goto Error
		End

		--Validar Fecha
		If (Select DATEDIFF (DD   , GETDATE(), (Select FechaFacturar  From Pedido  Where IDTransaccion=@IDTransaccion) )) >30 Begin
			set @Mensaje = 'Error: La fecha de pedido no puede ser mayor a 30 dias.'
			set @Procesado = 'False'
			Goto Error
		End

		If (Select DATEDIFF (DD   , GETDATE(), (Select FechaFacturar From Pedido  Where IDTransaccion=@IDTransaccion) )) <0 Begin
			set @Mensaje = 'Error: La fecha de pedido no puede ser menor al de hoy'
			set @Procesado = 'False'
			Goto Error
		End

		--Validar forma de pago
		IF @vIDFormaPagoFactura = 0 Begin
			set @Mensaje = 'No tiene asignada correctamente la Forma De Pago'
			set @Procesado = 'False'
			Goto Error
		End

		if (select dbo.FPedidoRepetido(@IDTransaccion)) = 1 begin
			set @Mensaje = 'Pedido Repetido'
			set @Procesado = 'False'
			Goto Error
		end
	End

	--Stock
	Begin

		Declare db_cursor cursor for
		Select Cantidad, IDProducto, IDDeposito, DescuentoUnitario 
		From DetallePedido Where IDTransaccion=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vCantidad, @vIDProducto, @vIDDeposito, @vDescuentoUnitario
		While @@FETCH_STATUS = 0 Begin  

			--Cantidad
			If @vPedidoPermitirCantidadNegativa = 0 Begin
				If @vCantidad <= 0 Begin
					Set @Mensaje = 'La cantidad no puede ser igual ni menor a 0!'
					Set @Procesado = 'False'
					Goto Error		
				End	
			End
			
			--Si tiene excepcion con limite de cantidad, restar el saldo
			Update ProductoListaPrecioExcepciones 
			Set CantidadLimiteSaldo = Isnull(CantidadLimiteSaldo,0) + @vCantidad,
			IDTransaccionPedido=@IDTransaccion
			Where IDProducto = @vIDProducto 
			and IDCliente = @vIDCliente
			and (@vFechaFacturar Between Desde and Hasta)
			and (Case when isnull(UnicaVez,'False') = 'True' then 1 else Isnull(CantidadLimite,0)  end )>0 
			and ABS(Descuento - round(@vDescuentoUnitario,2)) < 10
			and IDTipoDescuento <> 3
			and IDTransaccionPedido = 0

			--Si tiene excepcion con limite de cantidad, restar el saldo
			Update ProductoPrecio 
			Set IDTransaccionPedidoPrecioUnico=@IDTransaccion
			Where IDProducto = @vIDProducto 
			and IDCliente = @vIDCliente
			and Desde <=@vFechaFacturar
			and PrecioUnico = 1

			If Exists (Select Top(1) * from Producto Where ControlarExistencia= 'False' and ID=@vIDProducto) begin
				Goto Siguiente
			End

			--Verificar Existencia
			If @vPedidoPermitirCantidadNegativa = 0 Begin
				If (Select dbo.FExistenciaProducto(@vIDProducto, @vIDDeposito)) < @vCantidad Begin
					Declare @vProducto varchar(50)
					Declare @vExistencia decimal(18,0)

					Set @vProducto = (Select Descripcion From Producto Where ID=@vIDProducto)
					Set @vExistencia = (Select dbo.FExistenciaProducto(@vIDProducto, @vIDDeposito))

					Set @Mensaje = 'Stock insuficiente! ' + CONVERT(varchar(50), @vProducto) + ': ' + CONVERT(varchar(50), @vExistencia)
					Set @Procesado = 'False'

					Close db_cursor   
					Deallocate db_cursor			

					GoTo Error

				End
			End


			Siguiente:

			Fetch Next From db_cursor Into @vCantidad, @vIDProducto, @vIDDeposito,@vDescuentoUnitario

		End

		Close db_cursor   
		Deallocate db_cursor	

	End


	--Procesar
	Begin



		--Aprobacion de pedido
		Declare @vAprobado bit = 'True'		
		set @vIDFormaPagoFactura = (Select Top(1) IDFormaPagoFactura From Pedido Where IDTransaccion=@IDTransaccion)

		If (Select Top(1) Aprobacion From VFormaPagoFactura Where ID=@vIDFormaPagoFactura) = 'True' Begin
			Set @vAprobado = 'False'
		End

		--Actualizamos el Pedido		
		Update Pedido Set Numero=(	Select IsNull(Max(Numero+1),1) 
									From Pedido 
									Where IDSucursal=@vIDSucursal AND Procesado = 'True'),
						  Aprobado=@vAprobado
		Where IDTransaccion=@IDTransaccion 

		Update Pedido Set Procesado='True' 
		Where IDTransaccion=@IDTransaccion

		--Actualizar 'Entrega Directa desde Molino' si pedido pesa mas de 25.000 Kg
		Declare @vPesoTotal as money
		Set @vPesoTotal = ISNull((Select sum(Cantidad*Peso) From VDetallePedido where IdTransaccion = @IDTransaccion),0)
		If @vPesoTotal >= 25000 Begin
			Update Pedido
			set EntregaCliente = 'True'
			where IDTransaccion = @IDTransaccion
		End

		--Insertar en PedidoVendedor
		Declare @vIDVendedor int

		Set @vIDVendedor = (Select IDVendedor From Pedido Where IDTransaccion=@IDTransaccion)
		Set @vNumero = (Select ISNULL(MAX(Numero)+1, 1) From PedidoVendedor Where IDVendedor=@vIDVendedor)

		--actualizar la cantidad del pedido a entregar para el abastecimiento desde Molino
		update DetallePedido set CantidadAEntregar = Cantidad where IDTransaccion = @IDTransaccion

		Insert Into PedidoVendedor(IDTransaccion, IDVendedor, Numero)
		Values(@IDTransaccion, @vIDVendedor, @vNumero)


		--Verificar si es para EXPORTACION
		if exists(select * from cliente where id = @vIDCLiente and IVAExento = 1) begin
			Update DetallePedido Set IDImpuesto = (select ID from impuesto where Descripcion = 'Exento')
			Where IDTransaccion = @IDTransaccion
		end

		Select 'Procesado'='True', 'Mensaje'='Registro procesado!', 'Procesados'=@@ROWCOUNT
		Return @@Rowcount

	End

	Error:

	--Si se produjo un error, eliminar toda la transaccion
	Delete From DetalleImpuesto Where IDTransaccion=@IDTransaccion	
	Delete From DescuentoPedido Where IDTransaccion=@IDTransaccion	
	Delete From DetallePedido Where IDTransaccion=@IDTransaccion	
	Delete From Pedido Where IDTransaccion=@IDTransaccion	
	Delete From Transaccion Where ID=@IDTransaccion	

	Select 'Procesado'='False', 'Mensaje'=@Mensaje, 'Procesados'=@@ROWCOUNT

End


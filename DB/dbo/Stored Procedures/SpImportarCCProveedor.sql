﻿create Procedure [dbo].[SpImportarCCProveedor]

	--Entrada
	--Identificadores
	@IDProveedor integer,
	@CC varchar(32),
	@Actualizar bit
	
As

Begin

	Declare @vID int
	Declare @vOperacion varchar(50)
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit

	Set	@vID = 0
	Set @vOperacion = 'INS'
	Set @vMensaje = 'Sin proceso'
	Set @vProcesado = 'False'
		
	If Exists(Select * From Proveedor Where ID = @IDProveedor) Begin	
		Update Proveedor
		Set CuentaContableCompra = @CC
		where ID = @IDProveedor
		set @vMensaje = 'Registro Procesado'
		set @vProcesado = 'True'
	ENd
	Else if not Exists(Select * From Proveedor Where ID = @IDProveedor) begin
		set @vMensaje = 'Proveedor no existe.'
		set @vProcesado = 'False'
	End 
	
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	
	
End

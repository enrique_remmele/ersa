﻿CREATE Procedure [dbo].[SpPaseContable]

	--Entrada
	@Codigo Varchar(50),
	@Fecha date,
	@IDSucursal int,
	@IDCentroCosto tinyint,
	@Debito money,
	@Credito money,
	@Observacion varchar(100),
	@Operacion varchar(100),
	@IDTipoComprobante int,
	@NroComprobante varchar(50),
	@Tipo varchar(50),
	
	--Configuraciones
	@IDTransaccion numeric(18,0),
	@Resumido bit,
	@IDUsuario int,
	@Primero bit
	
As

Begin
	
	Declare @vMensaje varchar(100)
	Declare @vProcesado bit
	
	Set @vMensaje = 'Sin procesar'
	Set @vProcesado = 'False'
	
	Set @Tipo = REPLACE(@Tipo, ' DETALLADO', '')
	Set @Tipo = REPLACE(@Tipo, ' RESUMIDO', '')
	
	Set @Tipo = REPLACE(@Tipo, 'COBRANZA CREDITO', 'COBRANZA')
	Set @Tipo = REPLACE(@Tipo, 'COBRANZA CONTADO', 'COBRANZA')
	
	--Validar
	
	--Variables
	Declare @vNumero int
	Declare @vNumeroEliminar int
	Declare @vID int
	Declare @vIDMoneda int
	Declare @vCotizacion money
	Declare @vIDCuentaContable int
	Declare @vTipoComprobante varchar(50)
	
	
	--Obtener valores
	If @Resumido='False' Begin
		Set @vIDMoneda = (Select IDMoneda From Asiento Where IDTransaccion=@IDTransaccion)
		Set @vCotizacion = (Select Cotizacion From Asiento Where IDTransaccion=@IDTransaccion)
	End Else Begin
		Set @vIDMoneda = 1
		Set @vCotizacion = 0
	End
	
	--Hayar si es primero
	Begin
	
		--Si es detallado
		If @Resumido='False' Begin
			If Exists(Select * From AsientoCG Where IDTransaccionOrigen=@IDTransaccion) Begin
				Set @Primero='False'
			End Else Begin
				Set @Primero='True'
			End
		End
		
		--Si es resumido
		If @Resumido='True' Begin
			If Exists(Select Numero From AsientoCG Where Resumido='True' And Fecha=@Fecha And IDSucursal=@IDSucursal And TipoOperacion=@Tipo) Begin
				Set @Primero='False'
			End Else Begin
				Set @Primero='True'
			End
		End
		
	End
	
	--Hayar el Numero de Operacion
	Begin
	
		--Si es detallado
		If @Resumido='False' Begin
			Set @vNumero = (Select Numero From AsientoCG Where IDTransaccionOrigen=@IDTransaccion)	
		End
		
		--Si es resumido
		If @Resumido='True' Begin
			Set @vNumero = (Select Numero From AsientoCG Where Resumido='True' And Fecha=@Fecha And IDSucursal=@IDSucursal And TipoOperacion=@Tipo)		
		End
		
	End
	
	Set @vID = IsNull((Select MAX(ID) + 1 From DetalleAsientoCG Where Numero=@vNumero),0)
	Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@Codigo And PlanCuentaTitular='True')
	Set @vTipoComprobante = IsNull((Select Codigo From TipoComprobante Where ID=@IDTipoComprobante), '')
	
	--Insertar primero la Cabecera
	If @Primero = 'True' Begin
		
		Set @vNumero =  ISNULL((Select MAX(Numero)+1 From AsientoCG ),1)
		
		Insert Into AsientoCG(Numero,   IDTransaccionOrigen, Resumido,  IDSucursal,  Fecha,  IDMoneda,   Cotizacion,   IDTipoComprobante,  NroComprobante,  Detalle,      Total,              Debito,  Credito,  Saldo,              Anulado, IDCentroCosto, Conciliado, FechaConciliado, IDUsuarioConciliado, Bloquear, TipoOperacion, Tipo)
		Values(               @vNumero, @IDTransaccion,      @Resumido, @IDSucursal, @Fecha, @vIDMoneda, @vCotizacion, @IDTipoComprobante, @NroComprobante, @Operacion, @Debito + @Credito, @Debito, @Credito, @Debito - @Credito, 'False',  @IDCentroCosto,         'True',     GETDATE(),       @IDUsuario,          'False',  @Tipo,         @Tipo)
		
	End	
	
	--Inserttar el detalle
	Insert Into DetalleAsientoCG(Numero,   ID,   IDCuentaContable,   CuentaContable, Credito,  Debito,  Importe,          Observacion,  IDSucursal,  IDCentroCosto, TipoComprobante,   NroComprobante)
	Values(                      @vNumero, @vID, @vIDCuentaContable, @Codigo,        @Credito, @Debito, @Credito+@Debito, @Observacion, @IDSucursal, @IDCentroCosto, @vTipoComprobante, @NroComprobante)

	--Actualizamos los Totales en Asiento
	Update AsientoCG Set	Debito=Debito+@Debito,
							Credito=Credito+@Credito,
							Saldo=(Debito+@Debito)-(Credito+@Credito),
							Total=(Debito+@Debito)+(Credito+@Credito)

	Where Numero=@vNumero --<-- Esto no estaba, agregue 23/12/2015

	Set @vMensaje = 'Registro guardado!'
	Set @vProcesado = 'True'
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	
End
	
	
	
	

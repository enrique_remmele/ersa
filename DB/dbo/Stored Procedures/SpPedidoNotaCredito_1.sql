﻿CREATE Procedure [dbo].[SpPedidoNotaCredito]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Numero int = null,
	@NumeroSolicitudCliente varchar(50) = '',
	@IDSubMotivo int = null,
	@IDCliente int = NULL,
	@Fecha date = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDDepositoOperacion tinyint = NULL,
	@Aplicar bit = 'False',
	@Descuento bit = 'False',		
	@Devolucion bit = 'False',
	@ConComprobantes bit = 'True',
	@Saldo money  = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money= NULL,
	@Observacion varchar(100) = NULL,
	@Total money = NULL,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@TotalDescuento money = NULL,
	@DesdePedidoVenta bit = null,
	@IDSucursalCliente int = 0,
	@EsVentaSucursal bit = 'False',
	@Direccion varchar(100) = NULL,
	@Telefono varchar(50) = NULL,
			
	--Operacion
	@Operacion varchar(50),
	@VersionSistema varchar(100) = '',
	
	--Transaccion
	
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	@IDOperacion int,
	

	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output	
As

Begin

	--Validar Feha Operacion
	IF @Operacion = 'INS' and @Devolucion = 'True' Begin
		If exists(Select * from PedidoNotaCredito where anulado = 0 and NumeroSolicitudCliente = @NumeroSolicitudCliente and IDSucursal=@IDSucursal) Begin
			Set @Mensaje  = 'El numero de solicitud de cliente ya fue ingresado!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from PedidoNotaCredito where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 


	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		if @Aplicar = 'True' Begin
			Set @Saldo = @Total 
		End
		Declare @vNumero int = (select max(Numero)+1 from PedidoNotaCredito where IDSucursal = @IDSucursalOperacion)
		
		--Insertar en Nota de Credito
		INSERT INTO PedidoNotaCredito(IDTransaccion,Numero,NumeroSolicitudCliente,IDCliente,IDSucursal,IDDeposito,Fecha,IDMoneda,Cotizacion,Observacion,Total,TotalImpuesto,TotalDiscriminado,TotalDescuento,Saldo,Anulado,FechaAnulado,IDUsuarioAnulado,Procesado,Devolucion,Descuento,Aplicar,ConComprobantes,IDSubMotivoNotaCredito, DesdePedidoVenta, IDSucursalCliente, EsVentaSucursal,Direccion,Telefono)
								VALUES(@IDTransaccionSalida, @vNumero,@NumeroSolicitudCliente,@IDCliente, @IDSucursalOperacion,@IDDepositoOperacion,@Fecha,@IDMoneda,@Cotizacion,@Observacion,@Total,@TotalImpuesto,@TotalDiscriminado,@TotalDescuento,@Saldo,0,NULl,NUll,@Procesado,@Devolucion,@Descuento,@Aplicar,@ConComprobantes,@IDSubMotivo, @DesdePedidoVenta, @IDSucursalCliente, @EsVentaSucursal,@Direccion,@Telefono)
		
										
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
		
		if (Select ProcesadoNC from PedidoNotaCredito where IDTransaccion = @IDTransaccion) = 1 begin
			set @Mensaje = 'No se puede anular un pedido procesado'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
		
		----Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		----Anular
		Update PedidoNotaCredito  
		Set Anulado='True', 
		IDUsuarioAnulado=@IDUsuario, 
		FechaAnulado = GETDATE()
		Where IDTransaccion = @IDTransaccion
		
		delete from DetalleNotaCreditoDiferenciaPrecio
		Where IDTransaccionPedidoNotaCredito = @IDTransaccion

		delete from DetalleNotaCreditoAcuerdo
		Where IDTransaccionPedidoNotaCredito = @IDTransaccion

		delete from PedidoNotaCreditoPedidoVenta
		Where IDTransaccionPedidoNotaCredito = @IDTransaccion
				
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
		
	End
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From PedidoNotaCredito     Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Eliminamos Anulados
		Delete DocumentoAnulado where IDTransaccion = @IDTransaccion	
		--Auditoria de documentos
		set @IDCliente = (select IDCliente from PedidoNotaCredito where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from PedidoNotaCredito where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from PedidoNotaCredito where IDTransaccion = @IDTransaccion)
		set @Numero = (select Numero from PedidoNotaCredito where IDTransaccion = @IDTransaccion)
		set @Total = (select Total from PedidoNotaCredito where IDTransaccion = @IDTransaccion)

		
		--Eliminamos el detalle
		Delete DetallePedidoNotaCredito   where IDTransaccion = @IDTransaccion	
		Delete DetalleImpuesto Where IDTransaccion=@IDTransaccion
		
		--Eliminamo la Venta asociada
		Delete PedidoNotaCreditoVenta Where IDTransaccionPedidoNotaCredito = @IDTransaccion 	
				
		--Eliminamos el registro
		Delete PedidoNotaCredito   Where IDTransaccion = @IDTransaccion
						
		
		--Eliminamos la transaccion
		--Delete Transaccion Where ID = @IDTransaccion					 	 			
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End





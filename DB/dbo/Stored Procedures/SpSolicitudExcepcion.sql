﻿CREATE Procedure [dbo].[SpSolicitudExcepcion]


	--Entrada
	@Producto varchar(50),
	@ListaPrecio varchar(50),
	@Cliente varchar(50),
	@TipoDescuento varchar(50),
	@Descuento money,
	@Moneda varchar(10),
	@Desde date = NULL,
	@Hasta date = NULL,
	@CantidadLimite int = 0,
		
	--Transaccion
	@IDUsuario smallint,
	
	--Salida
	@Mensaje varchar(200) = '' output,
	@Procesado bit output
As

Begin

	--BLOQUES
	Declare @Precio decimal(18,6)
	Declare @vPorcentaje decimal(9,6)
	Declare @vIDMoneda tinyint
	Declare @vCantidadSaldo int
	Declare @vIDSucursal int
	Declare @vIDListaPrecio int
	Declare @vIDCLiente int
	Declare @vIDProducto int
	Declare @vIDTipoDescuento int
	Declare @vIDTipoProducto int

	Set @vIDProducto = ISnull((Select id from Producto where Referencia = @Producto),0)
	Set @vIDTipoProducto = ISnull((Select idTipoProducto from Producto where Referencia = @Producto),0)
	
	if @vIDTipoProducto = 1 begin
		Set @Desde = GetDate()
		Set @Hasta = DateAdd(year,1,GetDate())
	end

	If @vIDProducto = 0 begin
		Set @Mensaje = Concat('No se encuentra referencia de producto ', @Producto, '. ')
		Set @Procesado = 'False'
	end

	Set @vIDCLiente = ISnull((Select id from Cliente where Referencia = @Cliente),0)

	If @vIDCLiente = 0 begin
		Set @Mensaje = Concat(@Mensaje,'No se encuentra referencia de cliente ', @Cliente, '. ')
		Set @Procesado = 'False'
	end

	Set @vIDTipoDescuento = ISnull((Select id from TipoDescuento where codigo = @TipoDescuento),0)

	If @vIDTipoDescuento = 0 begin
		Set @Mensaje = Concat(@Mensaje,'No se encuentra el tipo de descuento ', @TipoDescuento, '. ')
		Set @Procesado = 'False'
	end
	--Por orden de la sra karin se bloquean las nc automaticas que no sean 41050
	--if @vIDTipoDescuento = 3 and @vIDProducto <> 3 begin
	--Por nueva orden de la sra karin se bloquean las nc automaticas por diferencia de precio 11/08/2020
	if @vIDTipoDescuento = 3  begin
		Set @Mensaje = Concat(@Mensaje,'Esta bloqueado este tipo de descuento para esteproducto. ')
		Set @Procesado = 'False'
	end
	

	Set @vIDMoneda = ISnull((Select id from Moneda where Referencia = @Moneda),0)
	
	If @vIDMoneda = 0 begin
		Set @Mensaje = Concat(@Mensaje,'No se encuentra moneda ', @Moneda, ', intente con GS o US. ')
		Set @Procesado = 'False'
	end

	if @Desde > @Hasta begin
		Set @Mensaje = Concat(@Mensaje,'La fecha "DESDE" es superior a la fecha "HASTA" ')
		Set @Procesado = 'False'
	end
	--Verificamos la existencia de la lista de precio
	If not exists (Select * from ListaPrecio where Descripcion = @ListaPrecio) begin
		Set @Mensaje = Concat(@Mensaje,'No se encuentra ninguna lista de precio llamada ', @ListaPrecio, '. ')
		Set @Procesado = 'False'
	end

	--Verificar si el cliente tiene almenos una sucursal con esa lista de precios
	If not exists (Select * from VClienteSucursal where ListaPrecio = @ListaPrecio) begin
		Set @Mensaje = Concat(@Mensaje,'No se encuentra ninguna lista de precio llamada ', @ListaPrecio, ', asignada a este cliente. ')
		Set @Procesado = 'False'
	end

	IF @Procesado = 'False' begin
		return @@rowcount
	end

	if not exists(select * from vProductoListaPrecio where IDProducto = @vIDProducto and ListaPrecio = @ListaPrecio) begin
		Set @Mensaje = concat('El producto ', @Producto, ' no tiene un precio asignado en la lista de precio ', @ListaPrecio)
		Set @Procesado = 'False'
		return @@rowcount
	end

	if (select sum(Precio) from vProductoListaPrecio where IDProducto = @vIDProducto and ListaPrecio = @ListaPrecio) = 0 begin
		Set @Mensaje = concat('El producto ', @Producto, ' no tiene un precio asignado en la lista de precio ', @ListaPrecio)
		Set @Procesado = 'False'
		return @@rowcount
	end
	
	Set @vIDListaPrecio = (Select Top(1) ID from ListaPrecio where Descripcion = @ListaPrecio)

	Set @Precio = (Select Top(1) Precio From ProductoListaPrecio Where IDListaPrecio=@vIDListaPrecio And IDProducto=@vIDProducto And IDMoneda=@vIDMoneda)
	
	Set @vPorcentaje = Round((@Descuento / @Precio) * 100, 6)
	--!!!!!!!!!!!!!!!!!!!!!!!!!!!!ATENCION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	--SE RESTRINGE LA DUPLICACION DE SOLICITUDES POR DIFERENCIA DE PRECIO Y TACTICO
	--SI YA EXISTE TACTICO, NO PUEDE GENERARSE DIFERENCIA DE PRECIO Y VICEVERSA
	--if Exists(select * from vProductoListaPrecioExcepciones where IDProducto = @vIDProducto and IDCliente = @vIDCLiente and IDTipoDescuento = @vIDTipoDescuento and Hasta >= @Desde and ListaPrecio = @ListaPrecio) begin
	
	if @vIDTipoProducto <> 1 begin

		if Exists(select * from SolicitudProductoListaPrecioExcepciones where IDProducto = @vIDProducto and IDCliente = @vIDCLiente and Convert(Date,Hasta) >= Convert(Date,@Desde) and Estado is null) begin
			Set @Mensaje = 'La configuracion ya esta cargada y se encuentra pendiente de aprobacion'
			Set @Procesado = 'False'
			return @@rowcount
		end

		if Exists(select * from SolicitudProductoListaPrecioExcepciones where IDProducto = @vIDProducto and IDCliente = @vIDCLiente and IDTipoDescuento = @vIDTipoDescuento and Convert(Date,Hasta) = Convert(Date,@Hasta) and Estado is null) begin
			Set @Mensaje = 'La configuracion ya esta cargada y se encuentra pendiente de aprobacion'
			Set @Procesado = 'False'
			return @@rowcount
		end

	end 
	else begin

		if Exists(select * from SolicitudProductoListaPrecioExcepciones where Desde = @Desde and Descuento = @Descuento and IDProducto = @vIDProducto and IDCliente = @vIDCLiente and estado is null)begin --and IDTipoDescuento = @vIDTipoDescuento and Descuento = @Descuento and Estado = 0) begin
			Set @Mensaje = 'La configuracion ya esta cargada y se encuentra pendiente de aprobacion'
			Set @Procesado = 'False'
			return @@rowcount
		end

		if Exists(select * from SolicitudProductoListaPrecioExcepciones where Desde = @Desde and Descuento = @Descuento and IDProducto = @vIDProducto and IDCliente = @vIDCLiente and estado = 0)begin --and IDTipoDescuento = @vIDTipoDescuento and Descuento = @Descuento and Estado = 0) begin
			Set @Mensaje = 'La configuracion ya esta cargada y fue rechazada'
			Set @Procesado = 'False'
			return @@rowcount
		end

	end

	Insert Into SolicitudProductoListaPrecioExcepciones(IDProducto, IDCliente, IDListaPrecio, IDMoneda, IDTipoDescuento, Descuento, Porcentaje, Desde, Hasta, CantidadLimite, FechaSolicitud,IDUsuarioSolicitud,Estado)
		Values(@vIDProducto, @vIDCliente, @vIDListaPrecio, @vIDMoneda, @vIDTipoDescuento, @Descuento, @vPorcentaje, @Desde, @Hasta, @CantidadLimite, GETDATE(),@IDUsuario,null)		
	
	Set @Mensaje = 'Registro guardado'
	Set @Procesado = 'True'
	return @@rowcount	


End


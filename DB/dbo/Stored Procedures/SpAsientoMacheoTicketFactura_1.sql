﻿CREATE Procedure [dbo].[SpAsientoMacheoTicketFactura]

	@IDTransaccion numeric(18,0)
		
As

Begin
	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint =0

	--Macheo
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vFecha date
	
	--Gasto
	Declare @vCuentaContableARecibir varchar(50)
	Declare @vTotalDiscriminadoGasto money
	Declare @vCantidadTotalGasto decimal(18,3)
	Declare @vIDTransaccionGasto int
	Declare @vCotizacionGasto decimal(18,3)
	Declare @vNroAcuerdo varchar(50)
	Declare @vPrecioUnitarioAcuerdo varchar(50)

	--Ticket
	Declare @vCuentaContableProvicion varchar(50)
	Declare @vTotalDiscriminadoTicket money
	Declare @vCantidadTotalTicket decimal(18,3)

	--Ajuste
	Declare @vCuentaContableAjusteContrato varchar(50)
	Declare @vTotalAjuste money

	--Diferencia Cambio
	Declare @vCuentaContableDiferenciaCambio varchar(50)

	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	
	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vIDTipoProducto int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money=0
	Declare @vImporteHaber money=0

	Declare @vCliente as varchar(32)
	Declare @vIDCliente as int
	
	--Obtener valores
	Begin
		
		Select	@vIDSucursal=IDSucursal,
				@vFecha=Fecha,
				@vNroAcuerdo = NroAcuerdo,
				@vIDTipoComprobante =IDTipoComprobante,
				@vComprobante = NroComprobante,
				@vTipoComprobante = TipoComprobante,
				@vIDTransaccionGasto = IDTransacciongasto
		From vMacheoFacturaTicket
		Where IDTransaccion=@IDTransaccion
		and NroAcuerdo > 0
		and Anulado = 'False'
			
		Select	@vIDTipoProducto = IDTipoProducto,
				@vPrecioUnitarioAcuerdo = PrecioDiscriminado
		from VAcuerdo
		Where NroAcuerdo = @vNroAcuerdo

	End
					
	--Verificar que el asiento se pueda modificar
	Begin
	
		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
			
		--Si esta anulado
		If (Select Anulado From vMacheoFacturaTicket Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'La venta esta anulada'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		
	End
				
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = @vTipoComprobante + ' ' + @vComprobante
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, 1, 1, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	
	--Cuentas Fijas de Clientes
	Begin
	
		--Trigo a Recibir
		Declare cCFProvision cursor for
		Select Codigo, Debe, Haber From VCFMacheoTicketFactura 
		Where IDSucursal = @vIDSucursal and IDTipoProducto = @vIDTipoProducto
		and AjusteContrato = 'False' and DiferenciaCambio= 'False' and [Debe/Haber] = 'HABER'
		Open cCFProvision   
		fetch next from cCFProvision into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			--Obtener la cuenta
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			
			Set @vTotalDiscriminadoGasto = (Select TotalDiscriminado * Cotizacion From Gasto Where IDTransaccion=@vIDTransaccionGasto) 
			Set @vCantidadTotalGasto = (Select Sum(Cantidad) from GastoProducto where IDTransaccionGasto = @vIDTransaccionGasto)
			Set @vCotizacionGasto = (Select Cotizacion From Gasto Where IDTransaccion=@vIDTransaccionGasto) 
			Set @vImporte = @vTotalDiscriminadoGasto
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End	
				
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin				
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
			End
			
			fetch next from cCFProvision into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFProvision 
		deallocate cCFProvision
	End
	
	-- Proviciones
	Begin
		Select @vCodigo=Codigo
		From VCFMacheoTicketFactura 
		Where IDSucursal = @vIDSucursal and IDTipoProducto = @vIDTipoProducto
		and AjusteContrato = 'False' and DiferenciaCambio= 'False' and [Debe/Haber] = 'DEBE'

		--Obtener la cuenta
		Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
		Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

		Set @vTotalDiscriminadoTicket = (select Sum(TotalDiscriminado)  from vMacheoFacturaTicketDetalle where IDTransaccionMacheo= @IDTransaccion)
		Set @vCantidadTotalTicket = (Select Sum(PesoBascula) from vMacheoFacturaTicketDetalle where IDTransaccionMacheo = @IDTransaccion)
	
		Set @vImporte = @vTotalDiscriminadoTicket
				
		Set @vImporteDebe = @vImporte
		Set @vImporteHaber = 0
				
		Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
		Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
	End

	--Ajuste de Contrato
	Declare cCFFletes cursor for
	Select Codigo From VCFMacheoTicketFactura 
	Where IDSucursal = @vIDSucursal and IDTipoProducto = @vIDTipoProducto
	and AjusteContrato = 'True'
	Open cCFFletes   
	fetch next from cCFFletes into @vCodigo
		
	While @@FETCH_STATUS = 0 Begin  
		
		---Agregar cuenta de existencia de producto
		Set @vCodigo = (Select IsNull(CuentaContableCosto,@vCodigo) from vTipoProducto where ID = @vIDTipoProducto)

		--Obtener la cuenta
		Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
		Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

		if (@vCantidadTotalGasto - @vCantidadTotalTicket) = 0 begin
			goto SeguirAjuste
		end	

		print concat('@vCantidadTotalGasto ',@vCantidadTotalGasto)
		print concat('@vCantidadTotalTicket ',@vCantidadTotalTicket)
		print concat('@vPrecioUnitarioAcuerdo ',@vPrecioUnitarioAcuerdo)
		print concat('@vCotizacionGasto ',@vCotizacionGasto)

		Set @vImporte = (@vCantidadTotalGasto - @vCantidadTotalTicket) * @vPrecioUnitarioAcuerdo * @vCotizacionGasto
			
		print concat('@vImporte ',@vImporte)


		If @vImporte > 0 Begin
			Set @vImporteDebe = @vImporte
			Set @vImporteHaber = 0
		End
			
		If @vImporte < 0 Begin
			Set @vImporteHaber = @vImporte * -1
			Set @vImporteDebe = 0
		End	
				
		If @vImporteHaber > 0 Or @vImporteDebe>0 Begin				
			Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
			Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
		End
	SeguirAjuste:
		fetch next from cCFFletes into @vCodigo
			
	End
		
	close cCFFletes 
	deallocate cCFFletes
	
	--Diferencia de Cambio
	Declare cCFFletes cursor for
	Select Codigo From VCFMacheoTicketFactura 
	Where IDSucursal = @vIDSucursal and IDTipoProducto = @vIDTipoProducto
	and DiferenciaCambio = 'True'
	Open cCFFletes   
	fetch next from cCFFletes into @vCodigo
		
	While @@FETCH_STATUS = 0 Begin  
		
		--Obtener la cuenta
		Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
		Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
	
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		
		Set @vImporte = @vImporteDebe - @vImporteHaber
		
		Set @vImporteDebe = 0
		Set @vImporteHaber = 0
			
		If @vImporte > 0 Begin
			Set @vImporteHaber = @vImporte
		End
			
		If @vImporte < 0 Begin
			Set @vImporteDebe = @vImporte * -1
		End	
				
		If @vImporteHaber > 0 Or @vImporteDebe>0 Begin				
			Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
			Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
		End
			
		fetch next from cCFFletes into @vCodigo
			
	End
		
	close cCFFletes 
	deallocate cCFFletes

	--Actualizamos la cabecera, el total
	Begin
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
		Set @vImporteHaber = ROUND(@vImporteHaber, @vRedondeo)
		Set @vImporteDebe = ROUND(@vImporteDebe, @vRedondeo)
	
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vImporteHaber - @vImporteDebe
		Where IDTransaccion=@IDTransaccion
	End

	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
	
End



﻿
CREATE Procedure [dbo].[SpOperacionPermitida]

	--Entrada
	@ID int,
	@IDOperacion int,
	@IDTipoOperacion int,
	@IDTipoComprobante int,
	@Estado bit = NULL,
	
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if not exists(Select * From Operacion Where ID=@IDOperacion) begin
			set @Mensaje = 'No existe la operacion!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if not exists(Select * From TipoComprobante Where ID=@IDTipoComprobante) begin
			set @Mensaje = 'No existe el documento!'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Si es que la descripcion ya existe
		if not exists(Select * From TipoOperacion Where ID=@IDTipoOperacion) begin
			set @Mensaje = 'No existe el tipo de operacion!'
			set @Procesado = 'False'
			return @@rowcount
		end



		--Obtenemos el nuevo ID
		declare @vID tinyint
		set @vID = (Select IsNull((Max(ID)+1), 1) From OperacionPermitida)

		
		--Insertamos
		Insert Into OperacionPermitida(ID, IDOperacion, IDTipoComprobante, IDTipoOperacion, Estado)
		Values(@vID, @IDOperacion, @IDTipoComprobante, @IDTipoOperacion, @Estado)	
		
		
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='OperacionPermitida', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
					
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si es que la descripcion ya existe
		if not exists(Select * From OperacionPermitida Where ID=@ID) begin
			set @Mensaje = 'No existe el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if not exists(Select * From Operacion Where ID=@IDOperacion) begin
			set @Mensaje = 'No existe la operacion!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if not exists(Select * From TipoComprobante Where ID=@IDTipoComprobante) begin
			set @Mensaje = 'No existe el documento!'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Si es que la descripcion ya existe
		if not exists(Select * From TipoOperacion Where ID=@IDTipoOperacion) begin
			set @Mensaje = 'No existe el tipo de operacion!'
			set @Procesado = 'False'
			return @@rowcount
		end

		Insert Into aOperacionPermitida(ID, IDOperacion, IDTipoComprobante, IDTipoOperacion, Estado, IDUsuario, Fecha, Operacion)
		Values(@vID, @IDOperacion, @IDTipoComprobante, @IDTipoOperacion, @Estado, @IDUsuario, GETDATE(),'UPD')	
		
		--Insertamos
		update OperacionPermitida
				set IDOperacion = @IDOperacion, 
				IDTipoComprobante = @IDTipoComprobante, 
				IDTipoOperacion = @IDTipoOperacion, 
				Estado = @Estado
		Where ID = @ID
			
		
		--Auditoria
		
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='OperacionPermitida', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
					
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		
		--Auditoria
		Insert Into aOperacionPermitida(ID, IDOperacion, IDTipoComprobante, IDTipoOperacion, Estado, IDUsuario, Fecha, Operacion)
		Values(@vID, @IDOperacion, @IDTipoComprobante, @IDTipoOperacion, @Estado, @IDUsuario, GETDATE(),'INS')	
						
		--Eliminamos
		Delete From OperacionPermitida 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='OperacionPermitida', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
			
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿CREATE Procedure [dbo].[SpDesgloseBillete]

	--Entrada
	@IDTransaccion numeric(18,0),
	@ID tinyint = NULL,
	@IDBillete tinyint = NULL,
	@Cantidad smallint = NULL,
	@Total money = NULL,
		
	--Operacion
	@Operacion varchar(10),
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
As

Begin
	
	--Procesar Saldos de Ventas
	If @Operacion = 'INS' Begin
	
		--Validar
		--Asegurarse que exista la transaccion
		if Not Exists(Select * From RendicionLote Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra el registro!'
			Set @Procesado = 'False'
			Return @@rowcount
		End
		
		--si es que ya existe el detalle, eliminar primeramente
		If Exists(Select * From DesgloseBillete Where IDTransaccion=@IDTransaccion And ID=@ID) Begin
			Delete From DesgloseBillete Where IDTransaccion=@IDTransaccion 	And ID=@ID 
		End
		
		--Insertar
		Insert Into DesgloseBillete(IDTransaccion, ID, IDBillete, Cantidad, Total)
		Values(@IDTransaccion, @ID, @IDBillete, @Cantidad, @Total)
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		
		Return @@rowcount
		
	End
	
	If @Operacion = 'ANULAR' Begin
		
		--Validar
		--Que exista el registro
		if Not Exists(Select * From RendicionLote Where IDTransaccionLote=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra el registro!'
			Set @Procesado = 'False'
			Return @@rowcount
		End
	
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		Return @@rowcount
		
	End
	
	If @Operacion = 'DEL' Begin
		
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'false'
		Return @@rowcount
		
	End
		
	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'
	
End




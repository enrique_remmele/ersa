﻿CREATE Procedure [dbo].[SpCFVentaCliente]

	--Cuenta Fija
	@IDOperacion tinyint,
	@ID tinyint,
	@IDSucursal tinyint,
	@IDMoneda tinyint,
	@CuentaContable varchar(50),
	@Debe bit,
	@Haber bit,
	@Descripcion varchar(50),
	@Orden tinyint,
	
	--Cuenta Fija
	@Contado bit,
	@Credito bit,
		
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
	
As

Begin

	--BLOQUES
	
	Declare	@vID tinyint
	
	--INSERTAR
	if @Operacion='INS' begin
			
		EXEC SpCF @ID = 0,
				@IDOperacion = @IDOperacion,
				@IDSucursal = @IDSucursal,
				@IDMoneda = @IDMoneda,
				@CuentaContable = @CuentaContable,
				@Debe = @Debe,
				@Haber = @Haber,
				@Descripcion = @Descripcion,
				@Orden = @Orden,
				@Operacion = @Operacion,
				@vID = @vID OUTPUT
			
		Insert Into CFVentaCliente(IDOperacion, ID, Contado, Credito)
		Values(@IDOperacion, @vID, @Contado, @Credito)
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		EXEC SpCF @ID = @ID,
				@IDOperacion = @IDOperacion,
				@IDSucursal = @IDSucursal,
				@IDMoneda = @IDMoneda,
				@CuentaContable = @CuentaContable,
				@Debe = @Debe,
				@Haber = @Haber,
				@Descripcion = @Descripcion,
				@Orden = @Orden,
				@Operacion = @Operacion,
				@vID = @vID OUTPUT
		
		Update CFVentaCliente Set Contado=@Contado,
									Credito=@Credito
		Where ID=@ID And IDOperacion=@IDOperacion
				
		set @Mensaje = 'Registro guardado...!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		Delete From CFVentaCliente Where IDOperacion=@IDOperacion And ID=@ID
		Delete From CF Where IDOperacion=@IDOperacion And ID=@ID
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount
			
End


﻿CREATE Procedure [dbo].[SpImportarCCCentroCostoUnidadNegocio]

	--Entrada
	--Identificadores
	@CodigoCC varchar(200) = NULL,
	@UN varchar(200) = NULL,
	@CC varchar(200) = null,

	@Actualizar bit



As

Begin

	Declare @vOperacion varchar(50)

	Declare @vMensaje varchar(50)
	Declare @vProcesado bit

	Declare @vIDCentroCosto tinyint
	Declare @vIDUnidadNegocio tinyint

	
	Set @vOperacion = 'INS'
	Set @vMensaje = 'Sin proceso'
	Set @vProcesado = 'False'

	Set	@vIDUnidadNegocio = (select Isnull(ID,0) from UnidadNegocio where Descripcion = @UN)
	Set	@vIDCentroCosto = (select Isnull(ID,0) from CentroCosto where Descripcion = @CC )

	Update CuentaContable
	set IDCentroCosto = @vIDCentroCosto,
	IDUnidadNegocio = @vIDUnidadNegocio
	where Codigo = @CodigoCC

	Set @vOperacion = 'INS'
	Set @vMensaje = 'Procesado'
	Set @vProcesado = 'True'


	salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	

End

















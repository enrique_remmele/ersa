﻿CREATE Procedure [dbo].[SpNotaDebitoProcesar]

	--Entrada
	@IDTransaccion numeric(18),
	@Operacion varchar(10),
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As	
	--Validar Feha Operacion
	Declare @vIDSucursal as integer
	Declare @vFecha as Date
	Declare @vIDUsuario as integer
	Declare @vIDOperacion as integer

	Select	@vIDSucursal=IDSucursal,
			@vFecha=Fecha,
			@vIDUsuario=IDUsuario,
			@vIDOperacion=IDOperacion
	 From Transaccion Where ID = @IDTransaccion

	If dbo.FValidarFechaOperacion(@vIDSucursal, @vFecha, @vIDUsuario, @vIDOperacion) = 'False' Begin
		Set @Mensaje  = 'Fecha fuera de rango permitido!!'
		Set @Procesado = 'False'
		Return @@rowcount
	End

	If @Operacion = 'INS' Begin
		Exec SpNotaDebitoActualizarStock @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output   
		Exec SpNotaDebitoActualizarVanta @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output     			
	
		--Damos el OK Nota Credito
		If @Procesado = 'True' Begin
			
			Update NotaDebito  Set Procesado='True' Where IDTransaccion=@IDTransaccion
			Set @Mensaje = 'Registro procesado!'
			
			--Generar el Asiento
			Exec SpAsientoNotaDebito @IDTransaccion=@IDTransaccion
			Return @@rowcount
		
		End Else Begin
	
			--Si hubo un error, eliminar todo el registro
			Set @Operacion = 'DEL'			
			
		End
	
	
	
	End
	
    
	If @Operacion = 'ANULAR' Begin
	
		Exec SpNotaDebitoActualizarStock @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output   
		Exec SpNotaDebitoActualizarVanta @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output  
		
	
	End
	
	If @Operacion  = 'DEL' Begin
	
		If (Select Anulado From NotaDebito  Where IDTransaccion=@IDTransaccion) = 'True' Begin
			Set @Mensaje = 'Registro guardado'
			Set @Procesado = 'True'
			return @@rowcount
	
		Exec SpNotaDebitoActualizarStock @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output   
		Exec SpNotaDebitoActualizarVanta @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output     			
		
		Delete NotaDebitoVenta Where IDTransaccionNotaDebito =@IDTransaccion 
		
			
		End 
	END
	


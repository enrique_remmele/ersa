﻿
CREATE Procedure [dbo].[SpGraficoContable]

	--Entrada
	@ID tinyint,
	@Titulo varchar(100),
	@IDUsuario int,
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la Titulo ya existe
		if exists(Select * From GraficoContable Where Titulo=@Titulo and IDUsuario=@IDUsuario) begin
			set @Mensaje = 'La Titulo ya existe para este usuario!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDGraficoContable tinyint
		set @vIDGraficoContable = (Select IsNull((Max(ID)+1), 1) From GraficoContable)

		--Insertamos
		Insert Into GraficoContable(ID, Titulo, IDUsuario)
		Values(@vIDGraficoContable, @Titulo, @IDUsuario)
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From GraficoContable Where ID=@ID and IDUsuario=@IDUsuario) begin
			set @Mensaje = 'El registro solicitado no existe o no esta asociado a su usuario!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la Titulo ya existe
		if exists(Select * From GraficoContable Where Titulo=@Titulo And ID!=@ID and IDUsuario=@IDUsuario) begin
			set @Mensaje = 'La Titulo ya existe para este usuario!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update GraficoContable Set Titulo=@Titulo
		Where ID=@ID and IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From GraficoContable Where ID=@ID and IDUsuario=@IDUsuario) begin
			set @Mensaje = 'El registro solicitado no existe o no esta asociado a su usuario!'
			set @Procesado = 'False'
			return @@rowcount
		end
				
		
		--Eliminamos
		Delete from SerieDetalleGraficoContable
		where IDGrafico = @ID

		Delete from SerieGraficoContable
		where IDGrafico = @ID

		Delete From GraficoContable 
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End



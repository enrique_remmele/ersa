﻿Create Procedure [dbo].[SpImportarActualizarUnidadNegocio]

	--Entrada
	--Identificadores
	@UN varchar(200) = '',
	@ID varchar(100) = '',

	@Actualizar bit



As

Begin

	Declare @vOperacion varchar(50)

	Declare @vMensaje varchar(50)
	Declare @vProcesado bit

	Declare @vIDUnidadNegocio tinyint
	Declare @vIDCentroCosto tinyint
	declare @vID int

	
	Set @vOperacion = 'INS'
	Set @vMensaje = 'Sin proceso'
	Set @vProcesado = 'False'

	If not Exists(Select Top(1) ID From UnidadNegocio Where Descripcion=@UN) Begin 
	   set @vIDUnidadNegocio = Isnull((select max(ID) from UnidadNegocio),0)+1
	   Insert into UnidadNegocio
	   values(@vIDUnidadNegocio,'',@UN,'True')
	End
	Set	@vIDUnidadNegocio = (select Isnull(ID,0) from UnidadNegocio where Descripcion = @UN)

		Update CuentaContable 
		Set IDUnidadNegocio = @vIDUnidadNegocio
		Where ID = @ID

		set @vMensaje = concat('Centro de Costo ',@ID,' Procesado')
		set @vProcesado = 'True'
		GoTo Salir
	--End

	salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	

End

















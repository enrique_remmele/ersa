﻿CREATE Procedure [dbo].[SpMeta]

	--Entrada
	@ID tinyint,
	@Mes integer,
	@Año integer,
	@IDSucursal tinyint,
	@IDVendedor tinyint,
	@IDTipoProducto tinyint,
	@IDUnidadMedida tinyint,
	@IDProducto integer,
	@IDListaPrecio integer,
	@Importe money = 0,
	@Estado bit = 'True',
	@MetaKg money = 0,

	@Operacion varchar(10),

	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si ya existe la meta 
		if exists(Select * From Meta Where IDSucursal = @IDSucursal and IDVendedor = @IDVendedor
		and IDTipoProducto = @IDTipoProducto and IDUnidadMedida = @IDUnidadMedida
		and IDProducto = @IDProducto and IDListaPrecio = @IDListaPrecio and Mes = @Mes and Año = @Año) begin
			set @Mensaje = 'La Meta ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end

		set @ID = IsNull((Select max(ID) from Meta),0)
		set @ID = @ID + 1 
				--Insertamos
		Insert Into Meta(ID, Mes, Año, IDSucursal, IDVendedor,IDTipoProducto,IDUnidadMedida,IDProducto,IDListaPrecio,Importe,Estado,MetaKg)
		Values(@ID,@Mes,@Año,@IDSucursal,@IDVendedor,@IDTipoProducto,@IDUnidadMedida,@IDProducto,@IDListaPrecio,@Importe,@Estado,@MetaKg)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='META', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From Meta Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		--Si ya existe la meta 
		if exists(Select * From Meta Where IDSucursal = @IDSucursal and IDVendedor = @IDVendedor
		and IDTipoProducto = @IDTipoProducto and IDUnidadMedida = @IDUnidadMedida
		and IDProducto = @IDProducto and IDListaPrecio = @IDListaPrecio and Mes = @Mes and Año = @Año and ID <> @ID) begin
			set @Mensaje = 'La Meta ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		--Actualizamos
		Update Meta 
		set IDSucursal = @IDSucursal,
		IDVendedor = @IDVendedor,
		IDTipoProducto = @IDTipoProducto,
		IDUnidadMedida = @IDUnidadMedida,
		IDProducto = @IDProducto,
		IDListaPrecio = @IDListaPrecio,
		Importe = @Importe,
		Estado = @Estado,
		MetaKg = @MetaKg
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='META', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Meta Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Meta 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='META', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


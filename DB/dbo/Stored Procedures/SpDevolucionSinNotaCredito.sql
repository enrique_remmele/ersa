﻿CREATE Procedure dbo.SpDevolucionSinNotaCredito

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Fecha date=NULL,
	@Numero int =NULL,
	@IDCliente int=NULL,
	@Observacion varchar(200)='',
	@NumeroSolicitudCliente varchar(50) = '',
	@Total money=0,
	--@IDTransaccionMovimientoStock int =NULL,
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion int,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin	
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 


	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from DevolucionSinNotaCredito where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--Fecha de la compra que no sea mayor a hoy
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If @Fecha > getdate() Begin
				set @Mensaje = 'La fecha del comprobante no debe ser mayor a hoy!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
		End
	END

	--BLOQUES
	SET @Numero = REPLACE(@Numero,'.','')
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
	
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		
		Set @Numero = IsNull((Select Isnull(MAX(Numero),0)+1 From DevolucionSinNotaCredito Where IDSucursal = @IDSucursal),1)	
		

		--Numero --JGR 20140814 Agregando control por IDsucursal
		If Exists(Select * From DevolucionSinNotaCredito Where Numero=@Numero AND IDSucursal = @IDSucursal) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		INSERT INTO DevolucionSinNotaCredito(IDTransaccion,Fecha,IDSucursal,Numero,NumeroSolicitudCliente,IDCliente,IDDeposito,Observacion,Total,Anulado,IDTransaccionMovimientoStock)
									VALUES(@IDTransaccionSalida,@Fecha,@IDSucursal,@Numero,@NumeroSolicitudCliente,@IDCliente,@IDDeposito,@Observacion,@Total,'False',0)

		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From DevolucionSinNotaCredito Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que no este anulado
		if (Select Anulado From DevolucionSinNotaCredito Where IDTransaccion=@IDTransaccion) = 'True' Begin
			set @Mensaje = 'El registro ya esta anulado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
			
		--Anular el movimiento de stock
		--Exec SpMovimiento @IDTransaccion=@IDTransaccion, @Operacion='ANULAR', @Mensaje=@Mensaje output, @Procesado=@Procesado output
		
		--Salimos si actualizar stock no fue satisfactorio
		--If @Procesado = 0 Begin
		--	print 'El stock no se actualizo correctamente. SpDevolucionSinNotaCredito Bloque: ANULAR '
		--	return @@rowcount
		--End
		
		--Anulamos el registro
		Update DevolucionSinNotaCredito 
		Set Anulado = 'True'
		,IDUsuarioAnulado = @IDUsuario
		,FechaAnulado = GetDate()
		Where IDTransaccion = @IDTransaccion

		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
		
	End	
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From DevolucionSinNotaCredito Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que no este anulado
					
		--Actualizamos el Stock
		
		--Insertamos el registro de anulacion
		
		--Anulamos el registro
		
	End

	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End


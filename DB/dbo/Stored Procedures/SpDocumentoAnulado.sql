﻿CREATE Procedure [dbo].[SpDocumentoAnulado]

	--Entrada
	@IDTransaccion numeric(18,0),
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDTerminal int
	
As

Begin

	If Exists(Select * From DocumentoAnulado Where IDTransaccion=@IDTransaccion) begin
		goto salir
	End
	
	--Insertar
	Insert Into DocumentoAnulado(IDTransaccion, Fecha, IDUsuario, IDSucursal, IDTerminal)
	values(@IDTransaccion, GETDATE(), @IDUsuario, @IDSucursal, @IDTerminal)

Salir:
	return @@rowcount

End


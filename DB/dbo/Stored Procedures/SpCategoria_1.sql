﻿CREATE Procedure [dbo].[SpCategoria]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
						
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Categoria Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDCategoria tinyint
		set @vIDCategoria = (Select IsNull((Max(ID)+1), 1) From Categoria)

		--Insertamos
		Insert Into Categoria(ID, Descripcion, Estado)
		Values(@vIDCategoria, @Descripcion, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TIPO DE COMPROBANTE', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
	
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From Categoria Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Categoria Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Categoria Set Descripcion=@Descripcion,
						 Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TIPO DE COMPROBANTE', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Categoria Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Productos
		if exists(Select * From Producto Where IDCategoria=@ID) begin
			set @Mensaje = 'El registro tiene productos asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Categoria 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TIPO DE COMPROBANTE', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿
CREATE Procedure [dbo].[SpMotivoAnulacion]

	--Entrada
	@ID tinyint,
	@Nombre varchar(50),
	@Usuario varchar(50),
	@Password varchar(50) = '12345',
	@Identificador varchar(3),
	@Estado bit,
	@EsVendedor bit = 'False',
	@IDVendedor int = NULL,
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la Nombre ya existe
		if exists(Select * From Usuario Where Nombre=@Nombre) begin
			set @Mensaje = 'El Nombre ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		if exists(Select * From Usuario Where Usuario=@Usuario) begin
			set @Mensaje = 'El Usuario ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que el Identi ya existe
		if exists(Select * From Usuario Where Identificador=@Identificador) begin
			set @Mensaje = 'El identificador ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDUsuario tinyint
		set @vIDUsuario = (Select IsNull((Max(ID)+1), 1) From Usuario)

		Set @Password = Convert(varchar(300), HASHBYTES('Sha1', '12345'))
						
		--Insertamos
		Insert Into Usuario(ID, Nombre, Usuario, [Password], Identificador, Estado, EsVendedor, IDVendedor)
		Values(@vIDUsuario, @Nombre, @Usuario, @Password, @Identificador, @Estado, @EsVendedor, @IDVendedor)		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From Usuario Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la Nombre ya existe
		if exists(Select * From Usuario Where Nombre=@Nombre And ID!=@ID) begin
			set @Mensaje = 'El Nombre ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		if exists(Select * From Usuario Where Usuario=@Usuario And ID!=@ID) begin
			set @Mensaje = 'El Usuario ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la Nombre ya existe
		if exists(Select * From Usuario Where Identificador=@Identificador And ID!=@ID) begin
			set @Mensaje = 'El identificador ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Usuario Set Nombre=@Nombre,
						 Estado = @Estado,
						 Usuario=@Usuario, 
						 Identificador=@Identificador,
						 EsVendedor=@EsVendedor,
						 IDVendedor=@IDVendedor
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Usuario Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Productos
		if exists(Select * From Transaccion Where IDUsuario=@ID) begin
			set @Mensaje = 'El registro tiene operaciones realizadas! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Usuario 
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End






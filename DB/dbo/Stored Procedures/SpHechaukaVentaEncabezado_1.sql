﻿CREATE Procedure [dbo].[SpHechaukaVentaEncabezado]

	@Año smallint,
	@Mes tinyint,
	@TipoReporte tinyint
	
	
As
Begin

	-- Variables Cantidad de Registros
	Declare @vVentas int
	Declare @vNotaCreditos int
	Declare @vNotaDebitos int
	Declare @vTotalRegistros int
	Declare @vComprobanteLibroIVA int


	--Variables Monto Total
	Declare @vTotalVentas money
	Declare @vTotalNotaCreditos money
	Declare @vTotalNotaDebitos money
	Declare @vMontoTotal money
	Declare @vTotalComprobanteLibroIVA money

	--Cantidad Registros
	Set @vVentas  = (IsNull((Select Count(*) From VVenta Where YEAR(fecha) = @Año And MONTH(Fecha) = @Mes And Procesado='True' And Anulado= 'False' ),0))
	Set @vNotaCreditos = (IsNull((Select Count(*) From VNotaCreditoProveedor Where YEAR(fecha) = @Año And MONTH(Fecha) = @Mes And Anulado= 'False' ),0))
	Set @vNotaDebitos = (IsNull((Select Count(*) From VNotaDebito Where YEAR(fecha) = @Año And MONTH(Fecha) = @Mes And Procesado='True' And Anulado= 'False' ),0))
	Set @vComprobanteLibroIVA = (ISNUll((Select Count(*) from vComprobanteLibroIVA  V join Cliente C On V.IDCliente = ID join TipoComprobante TC on V.IDTipoComprobante = TC.ID Where YEAR(Fecha)=@Año And MONTH(Fecha)=@Mes And TC.HechaukaTipoDocumento <> 0 ANd TC.LibroVenta = 1),0))
	Set @vTotalRegistros = @vVentas  + @vNotaCreditos + @vNotaDebitos + @vComprobanteLibroIVA
	
	--Monto Total
	Set @vTotalVentas  =  (IsNull ((Select Sum(Round(Total *  Cotizacion,0)) From VVenta  Where YEAR(fecha) = @Año And MONTH(Fecha) = @Mes And Procesado='True'And Anulado= 'False' ),0))
	Set @vTotalNotaCreditos = (IsNull ((Select Sum(Round(Total *  Cotizacion,0)) From VNotaCreditoProveedor Where YEAR(Fecha)= @Año And MONTH (Fecha)= @Mes And Anulado= 'False' And IDTipoComprobante in (Select ID from TipoComprobante TC where TC.HechaukaTipoDocumento <> 0)),0))
	Set @vTotalNotaDebitos  =   (IsNull ((Select Sum(Round(Total *  Cotizacion,0)) From VNotaDebito  Where YEAR(Fecha)= @Año And MONTH (Fecha)= @Mes And Procesado='True'And Anulado= 'False'),0))
	Set @vTotalComprobanteLibroIVA = (isnull((Select Sum(Round(Total *  Cotizacion,0)) From vComprobanteLibroIVA  V join Cliente C On V.IDCliente = ID join TipoComprobante TC on V.IDTipoComprobante = TC.ID Where YEAR(Fecha)=@Año And MONTH(Fecha)=@Mes And TC.HechaukaTipoDocumento <> 0 ANd TC.LibroVenta = 1),0))
	--Set @vMontoTotal= Convert (Integer,(@vTotalVentas  + @vTotalNotaCreditos + @vTotalNotaDebitos)) 
	
	Set @vMontoTotal=  (@vTotalVentas  + @vTotalNotaCreditos + @vTotalNotaDebitos + @vTotalComprobanteLibroIVA)
	

	Select
	'TipoRegistro'=1,
	'Periodo'= convert( varchar(20), @Año) + CONVERT(varchar(20), dbo.FFormatoDosDigitos(@Mes)),
	'TipoReporte'= @TipoReporte ,
	'CodigoObligacion'= 921,
	'CodigoFormulario'= 221,
	'RUCAgente'=(Select RUC From DatoEmpresa) ,
	'DVAgente'= (Select DVRUC From DatoEmpresa) ,
	'NombreAgente'= (Select RazonSocial From DatoEmpresa),
	'RUCRepresentante'= (Select RucPropietario From DatoEmpresa),
	'DVRepresentante'=(Select DVRUCPropietario From DatoEmpresa) ,
	'NombreRepresentante'=(Select Propietario From DatoEmpresa),
	'CantidadRegistros'=@vTotalRegistros ,
	'MontoTotal'= @vMontoTotal ,
	'Version'=2
		
End



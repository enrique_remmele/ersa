﻿CREATE Procedure [dbo].[SpComisionProducto]

	--Entrada
	@IDProducto int,
	@IDVendedor int,
	@Base bit,
	@PorcentajeBase decimal (5,3)= 0,
	@PorcentajeEsteProducto decimal (5,3)= 0,
	
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
As

Begin

	--BLOQUES
	Declare @vPorcentajeAnterior decimal (5,3) 
	Declare @vIDProducto int
	Declare @vIDTipoProducto int
	
	Set @vIDTipoProducto = (Select IDTipoProducto From Producto Where ID = @IDProducto)
	
	--BASE
	If @Base = 1 Begin
		
		Declare db_cursor cursor for
		Select ID From Producto Where  IDTipoProducto=@vIDTipoProducto
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDProducto
		While @@FETCH_STATUS = 0 Begin  
		
			--Si Existe, actualizamos
			If Exists(Select * From ComisionProducto  Where IDProducto=@vIDProducto And IDVendedor=@IDVendedor And IDSucursal=@IDSucursal) Begin
			
				--Obtener Porcentaje anterior
				Set @vPorcentajeAnterior = (Select PorcentajeBase  from ComisionProducto Where IDProducto=@vIDProducto And IDVendedor=@IDVendedor And IDSucursal=@IDSucursal )
				
				--Actualizar
				Update ComisionProducto Set PorcentajeBase = @PorcentajeBase  ,
									PorcentajeEsteProducto = @PorcentajeEsteProducto,
									PorcentajeAnterior = @vPorcentajeAnterior ,
									FechaUltimoCambio = GETDATE (),
									IDUsuario= @IDUsuario 
				Where IDProducto=@vIDProducto And IDVendedor=@IDVendedor  And IDSucursal=@IDSucursal
			
			End
		
			--Si no existe Insertamos
			If Not Exists(Select * From ComisionProducto  Where IDProducto=@vIDProducto And IDVendedor=@IDVendedor   And IDSucursal=@IDSucursal) Begin 
		
				--Insertar
				Insert Into ComisionProducto (IDProducto, IDVendedor, IDSucursal, PorcentajeBase, PorcentajeEsteProducto, PorcentajeAnterior, IDUsuario)
				values(@vIDProducto, @IDVendedor, @IDSucursal, @PorcentajeBase, @PorcentajeEsteProducto, @vPorcentajeAnterior, @IDUsuario)

			End
		
			Fetch Next From db_cursor Into @vIDProducto
			
		End

		Close db_cursor   
		Deallocate db_cursor	
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
				
	End 
		
	--Si es este producto solo
	If @Base = 0 Begin
	
			--Si Existe, actualizamos
			If Exists(Select * From ComisionProducto  Where IDProducto=@IDProducto And IDVendedor=@IDVendedor   And IDSucursal=@IDSucursal) Begin
		
				--Obtener Porcentaje anterior
				Set @vPorcentajeAnterior = (Select PorcentajeEsteProducto   from ComisionProducto Where IDProducto=@IDProducto And IDVendedor=@IDVendedor   And IDSucursal=@IDSucursal)
				
				--Actualizar
				Update ComisionProducto Set PorcentajeBase = @PorcentajeBase  ,
									PorcentajeEsteProducto = @PorcentajeEsteProducto,
									PorcentajeAnterior = @vPorcentajeAnterior ,
									FechaUltimoCambio = GETDATE (),
									IDUsuario= @IDUsuario 
				Where IDProducto=@IDProducto And IDVendedor=@IDVendedor  And IDSucursal=@IDSucursal
			
			End
		
			--Si no existe Insertamos
			If Not Exists(Select * From ComisionProducto  Where IDProducto=@IDProducto And IDVendedor=@IDVendedor   And IDSucursal=@IDSucursal) Begin 
		
				--Insertar
				Insert Into ComisionProducto (IDProducto, IDVendedor, IDSucursal, PorcentajeBase, PorcentajeEsteProducto, PorcentajeAnterior, IDUsuario)
				values(@IDProducto, @IDVendedor, @IDSucursal, @PorcentajeBase, @PorcentajeEsteProducto, @vPorcentajeAnterior, @IDUsuario)
	
			End
		
	
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End	
				
	End









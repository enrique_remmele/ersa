﻿CREATE Procedure [dbo].[SpListaPrecioImportar]
	
	--Entrada
	@Referencia varchar(100),
	@Descripcion varchar(100),
	@IDSucursal int,
	
	@Actualizar bit = 'True'
	
As

Begin

	Declare @vID int
	
	--Si existe
	If Exists(Select * From ListaPrecio Where Referencia=@Referencia And IDSucursal=@IDSucursal) Begin
		Update ListaPrecio Set Descripcion=@Descripcion, Referencia=@Referencia
		Where Descripcion=@Descripcion
	End
	
	--Si existe
	If Not Exists(Select * From ListaPrecio Where Referencia=@Referencia And IDSucursal=@IDSucursal) Begin
		If @Actualizar = 'True' Begin
			Set @vID = (Select IsNull(Max(ID)+1,1) From ListaPrecio)
			Insert Into ListaPrecio(ID, Descripcion, Estado, Referencia, IDSucursal)
			Values(@vID, @Descripcion, 'True', @Referencia, @IDSucursal)
		End
	End
	
End



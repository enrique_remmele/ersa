﻿CREATE Procedure [dbo].[SpAjusteInventarioProcesar]
	
	--Entrada
	@Numero int,
	@Operacion varchar(10)
	
	
As

Begin
	
	Declare @vIDTransaccion numeric(18,0)
	Declare @vIDProducto int
	Declare @vIDDeposito tinyint
	Declare @vDiferencia decimal (10,2)
	Declare @vMensaje varchar(100)
	Declare @vProcesado bit
	Declare @vSigno char(1)
	
		--Variables Kardex
	Declare @vFecha datetime
	Declare @vIDSucursal integer
	Declare @vNroComprobante varchar(50)
	Declare @vCosto decimal (18,2)

	Select	@vIDTransaccion = IDTransaccion,
			@vFecha = Fecha, 
			@vNroComprobante = Numero
	From AjusteInicial
	Where Numero = @Numero

	Set @vIDSucursal = (Select TOP(1) @vIDSucursal From DetalleAjusteInicial Where IDTransaccion=@vIDTransaccion)

	If @vFecha Is Null Begin
		Set @vFecha = (Select GETDATE())
	End
	
	If @Operacion = 'INS' Begin
		
		Declare db_cursor cursor for
		Select IDProducto, IDDeposito, Diferencia  From DetalleAjusteInicial 
		Where IDTransaccion=@vIDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vDiferencia
		While @@FETCH_STATUS = 0 Begin 
		
			If @vDiferencia < 0 Begin
				Set @vDiferencia = @vDiferencia * -1
				Set @vSigno = '-'
			End Else Begin
				Set @vSigno = '+'		
			End
						
			EXEC SpActualizarProductoDeposito
				@IDDeposito = @vIDDeposito,
				@IDProducto = @vIDProducto,
				@Cantidad = @vDiferencia,
				@Signo = @vSigno,
				@Mensaje = @vMensaje OUTPUT,
				@Procesado = @vProcesado OUTPUT
			
			If @vSigno = '-' Begin
				--Actualiza el Kardex

				EXEC SpKardex
					@Fecha= @vFecha,
					@IDTransaccion = @vIDTransaccion,
					@IDProducto = @vIDProducto,
					@ID = 1,
					@Orden = 20, 
					@CantidadEntrada = 0,
					@CantidadSalida = @vDiferencia,
					@CostoEntrada = 0,
					@CostoSalida = @vCosto,
					@IDSucursal = @vIDSucursal,
					@Comprobante = @vNroComprobante				
			End	

			If @vSigno = '+' Begin
					--Actualiza el Kardex
				EXEC SpKardex
					@Fecha= @vFecha,
					@IDTransaccion = @vIDTransaccion,
					@IDProducto = @vIDProducto,
					@ID = 1,
					@Orden = 20, 
					@CantidadEntrada = @vDiferencia,
					@CantidadSalida = 0,
					@CostoEntrada = @vCosto,
					@CostoSalida = 0,
					@IDSucursal = @vIDSucursal,
					@Comprobante = @vNroComprobante				
			End

			Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vDiferencia
										
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
		Update AjusteInicial Set Procesado='True'
		Where IDTransaccion=@vIDTransaccion
		
	End
		
	If @Operacion = 'ANULAR' Begin
		
		Declare db_cursor_anular cursor for
		Select IDProducto, IDDeposito, Diferencia  From DetalleAjusteInicial 
		Where IDTransaccion=@vIDTransaccion
		Open db_cursor_anular   
		Fetch Next From db_cursor_anular Into @vIDProducto, @vIDDeposito, @vDiferencia
		While @@FETCH_STATUS = 0 Begin 
		
			If @vDiferencia < 0 Begin
				Set @vDiferencia = @vDiferencia * -1
				Set @vSigno = '+'
			End Else Begin
				Set @vSigno = '-'		
			End
						
			EXEC SpActualizarProductoDeposito
				@IDDeposito = @vIDDeposito,
				@IDProducto = @vIDProducto,
				@Cantidad = @vDiferencia,
				@Signo = @vSigno,
				@Mensaje = @vMensaje OUTPUT,
				@Procesado = @vProcesado OUTPUT

				Delete from kardex where idtransaccion = @vIDTransaccion
				Exec SPRecalcularKardexProducto @vIDProducto, @vFecha


			
			--If @vSigno = '+' Begin
			--	--Actualiza el Kardex
			--	--Vuelvo a multiplicar la diferencia por -1 para revertir
			--	Set @vDiferencia = @vDiferencia*-1

			--	EXEC SpKardex
			--		@Fecha= @vFecha,
			--		@IDTransaccion = @vIDTransaccion,
			--		@IDProducto = @vIDProducto,
			--		@ID = 1,
			--		@Orden = 20, 
			--		@CantidadEntrada = 0,
			--		@CantidadSalida = @vDiferencia,
			--		@CostoEntrada = 0,
			--		@CostoSalida = @vCosto,
			--		@IDSucursal = @vIDSucursal,
			--		@Comprobante = @vNroComprobante				
			--End	

			--If @vSigno = '-' Begin
			--	--Actualiza el Kardex
			--	--Vuelvo a multiplicar la diferencia por -1 para revertir
			--	Set @vDiferencia = @vDiferencia*-1
			--	EXEC SpKardex
			--		@Fecha= @vFecha,
			--		@IDTransaccion = @vIDTransaccion,
			--		@IDProducto = @vIDProducto,
			--		@ID = 1,
			--		@Orden = 20, 
			--		@CantidadEntrada = @vDiferencia,
			--		@CantidadSalida = 0,
			--		@CostoEntrada = @vCosto,
			--		@CostoSalida = 0,
			--		@IDSucursal = @vIDSucursal,
			--		@Comprobante = @vNroComprobante				
			--End

			Fetch Next From db_cursor_anular Into @vIDProducto, @vIDDeposito, @vDiferencia
										
		End
		
		--Cierra el cursor
		Close db_cursor_anular   
		Deallocate db_cursor_anular
		
		Update AjusteInicial Set Procesado='False'
		Where IDTransaccion=@vIDTransaccion
		
	End
	
End
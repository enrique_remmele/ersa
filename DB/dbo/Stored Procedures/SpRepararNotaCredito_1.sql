﻿CREATE Procedure [dbo].[SpRepararNotaCredito]

	--Entrada
	@IDTransaccion numeric(18,0)
	
As

Begin
	
	--Validar
	Declare @vMensaje varchar(200)
	Declare @vProcesado bit
	Declare @vIDTransaccionVenta int
	Declare @vIDTransaccionPedidoNC int
	Declare @vDesdePedido bit = 0
	Declare @vImporteDescuento money
	
	Set @vMensaje = 'No se proceso'
	Set @vProcesado = 'False'
	
	--Validar
	--Si existe
	If  Not Exists(Select * From NotaCredito Where IDTransaccion=@IDTransaccion) Begin
		Set @vMensaje = 'El registro no existe!'
		Set @vProcesado = 'False'
		GoTo Salir
	End

	--Si no esta procesado
	If  (Select Procesado From NotaCredito Where IDTransaccion=@IDTransaccion) = 'False' Begin
		Set @vMensaje = 'El registro no esta procesado!'
		Set @vProcesado = 'False'
		GoTo Salir
	End
	
	--Verificar que este referenciado a sus facturas
	if not exists(select * from NotaCreditoVenta where IDTransaccionNotaCredito = @IDTransaccion) begin
		Set @vIDTransaccionPedidoNC = (select IDTransaccionPedido from pedidoncnotacredito where idtransaccionnotacredito = @IDTransaccion)
		Set @vDesdePedido = (Select isnull(DesdePedidoVenta,0) from PedidoNotaCredito where idtransaccion = @vIDTransaccionPedidoNC)
		if @vDesdePedido = 1 begin

			Set @vIDTransaccionVenta = (Select IDTransaccionVenta from vPedidoNotaCreditoPedidoVenta where IDTransaccionPedidoNotaCredito = @vIDTransaccionPedidoNC)
			Set @vImporteDescuento = (Select Total from vPedidoNotaCreditoPedidoVenta where IDTransaccionPedidoNotaCredito = @vIDTransaccionPedidoNC)
			insert into notacreditoventa values(@IDTransaccion,@vIDTransaccionVenta,0,@vImporteDescuento,0,0,0)

		end
	end


	--Actualizar Saldo
	If  (Select Aplicar From NotaCredito Where IDTransaccion=@IDTransaccion and Anulado = 'False') = 'True' Begin
		Update NotaCredito set Saldo = Total -(select isnull(sum(a.Importe),0) from notacreditoventaaplicada a 
																				join notacreditoaplicacion n on a.idtransaccionnotacreditoaplicacion = n.idtransaccion 
																				where n.anulado = 0 and a.IDTransaccionNotaCredito = @IDTransaccion)
		where IDTransaccion =@IDTransaccion
	End

	
	--Si esta anulado
	If  (Select Anulado From NotaCredito Where IDTransaccion=@IDTransaccion) = 'True' Begin
				--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion

		Declare @vIDTransaccionPedidoNotaCredito int= (Select top(1) IsNull(IDTransaccionPedido,0) From PedidoNCNotaCredito Where IDTransaccionNotaCredito = @IDTransaccion)
		
		print concat('idtransaccionpedidonc',@vIDTransaccionPedidoNotaCredito)
		
		Update PedidoNotaCredito Set ProcesadoNC='False'	
		Where IDTransaccion = @vIDTransaccionPedidoNotaCredito

		
		Update DetalleNotaCreditoDiferenciaPrecio 
				set IDTransaccionNotaCredito =  null,
				IDTransaccionVenta = null
		Where IDTransaccionPedidoNotaCredito = @vIDTransaccionPedidoNotaCredito

		Update DetalleNotaCreditoAcuerdo 
				set IDTransaccionNotaCredito = null,
				IDTransaccionVenta = null
		Where IDTransaccionPedidoNotaCredito = @vIDTransaccionPedidoNotaCredito
			
		Update PedidoNotaCredito 
				set ProcesadoNC = null 
		where IDTransaccion = @vIDTransaccionPedidoNotaCredito
	
				
		--Liberar Pedido
		Delete From PedidoNCNotaCredito 
		Where IDTransaccionNotaCredito = @IDTransaccion
	End
	
	
	Set @vMensaje = 'Registro procesado'
	Set @vProcesado = 'True'
		
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


﻿CREATE Procedure [dbo].[SpMacheoProcesar]
	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	
	--Salida
	@Mensaje varchar(200)  = '' output,
	@Procesado bit  = 'True' output
		
As

Begin
	
	Declare @vFecha as date,
			@vIDProducto int,
			@vTotalEntrada money,
			@vTotalSalida money,
			@vIDSucursal int,
			@vNroComprobante varchar(50),
			@vIDTransaccionGasto int,
			@vPrecioUnitarioAcuerdo money,
			@vNroAcuerdo int,
			@vCantidadTotalTicket decimal(18,3),
			@vCantidadTotalGasto decimal(18,3),
			@vCotizacionGasto decimal(18,3),
			@vIDTipoProducto int,
			@vImporte money

			select @vFecha=Fecha,
				   @vIDProducto=IDProducto,
				   @vIDSucursal=IDSucursal,
				   @vNroComprobante ='',
				   @vIDTransaccionGasto = IDTransacciongasto,
				   @vNroAcuerdo= NroAcuerdo
			from VMacheoFacturaTicket
			where IDTransaccion = @IDTransaccion
		

			Select	@vIDTipoProducto = IDTipoProducto, @vPrecioUnitarioAcuerdo = PrecioDiscriminado
			from VAcuerdo
			Where NroAcuerdo = @vNroAcuerdo

			Set @vCantidadTotalTicket = (Select Sum(PesoBascula) from vMacheoFacturaTicketDetalle where IDTransaccionMacheo = @IDTransaccion)
			Set @vCantidadTotalGasto = (Select Sum(Cantidad) from GastoProducto where IDTransaccionGasto = @vIDTransaccionGasto)
			Set @vCotizacionGasto = (Select Cotizacion From Gasto Where IDTransaccion=@vIDTransaccionGasto) 


			if (@vCantidadTotalGasto - @vCantidadTotalTicket) = 0 begin
					goto Salir
			end	


			Set @vImporte = (@vCantidadTotalGasto - @vCantidadTotalTicket) * @vPrecioUnitarioAcuerdo * @vCotizacionGasto

			if @vImporte > 0 begin
				Set @vTotalEntrada=(select Sum(Debito) from DetalleAsiento where CuentaContable = (Select CuentaContableCosto from vTipoProducto where ID=@vIDTipoProducto) and IDTransaccion = @IDTransaccion)
				Set @vTotalSalida =0
			end 
			else begin
				Set @vTotalEntrada=0
				Set @vTotalSalida =(select Sum(Credito) from DetalleAsiento where CuentaContable = (Select CuentaContableCosto from vTipoProducto where ID=@vIDTipoProducto) and IDTransaccion = @IDTransaccion)
			end

			
	--Actualiza el Kardex			
		EXEC SpKardex
			@Fecha = @vFecha,
			@IDTransaccion = @IDTransaccion,
			@IDProducto = @vIDProducto,
			@ID = 1,
			@Orden = 1,
			@CantidadEntrada = 0,
			@CantidadSalida = 0,
			@CostoEntrada = @vTotalEntrada,
			@CostoSalida = @vTotalSalida,
			@IDSucursal = @vIDSucursal,
			@Comprobante = @vNroComprobante	
			print 'Entro KARDEX'
	Salir:
	
	return @@rowcount
		
End





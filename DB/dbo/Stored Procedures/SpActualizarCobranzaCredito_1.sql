﻿CREATE Procedure [dbo].[SpActualizarCobranzaCredito]

	--Entrada
	@IDTransaccion int,
	@Comprobante varchar(50),
	@IDCobrador int,
	@IDTipoComprobante SMALLINT,
	@Observacion varchar(150),
	@NroPlanilla varchar(10),
	@IDMoneda int = 0,
	@Cotizacion decimal(18,3) = 0,

	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin
	Declare @vReciboInterno bit = 0
	--Si la cobranza esta anulada
	If Exists(Select * From CobranzaCredito Where IDTransaccion=@IDTransaccion And Anulado='True') Begin
		set @Mensaje = 'La cobranza ya está anulada! No se puede modificar.'
		set @Procesado = 'False'
		return @@rowcount
	End	
	
	Set @vReciboInterno= (select IsNull(ReciboInterno,0) from CobranzaCredito Where IDTransaccion=@IDTransaccion)
	
	--Controlar NroRecibo
	If @IDTipoComprobante = 33 begin -- 33 IDTipoComprobante = "Recibo"
		Declare @vIDSucursal tinyint = (Select IDSucursal From CobranzaCredito Where IDTransaccion=@IDTransaccion)
		If exists(Select * from Configuraciones Where IDSucursal=@vIDSucursal And CobranzaControlarNroRecibo = 1) begin
			If exists(Select * from CobranzaCredito Where IDSucursal = @vIDSucursal And Comprobante = @Comprobante) begin
				set @Mensaje = 'El numero de recibo ya existe para esta sucursal!'
				set @Procesado = 'False'
				return @@rowcount
			End
		End
	End
	
	if @IDMoneda = 0 begin
		Set @IDMoneda = (Select Top(1) IDMoneda from CobranzaCredito where IDTransaccion = @IDTransaccion)
	end

	if @Cotizacion = 0 begin
		Set @Cotizacion = (Select Top(1) Cotizacion from CobranzaCredito where IDTransaccion = @IDTransaccion)
	end
	if @vReciboInterno = 0 begin
		--Actualizar 
		Update CobranzaCredito  Set NroComprobante=@Comprobante,
									  IDCobrador=@IDCobrador,
									  IDTipoComprobante=@IDTipoComprobante,
									  Observacion =@Observacion,
									  NroPlanilla=@NroPlanilla,
									  IDmoneda=@IDMoneda,
									  Cotizacion=@Cotizacion
		Where IDTransaccion=@IDTransaccion
	end 
	else begin
		Update CobranzaCredito  Set   IDCobrador=@IDCobrador,
									  Observacion =@Observacion,
									  NroPlanilla=@NroPlanilla,
									  IDmoneda=@IDMoneda,
									  Cotizacion=@Cotizacion
		Where IDTransaccion=@IDTransaccion
	end
	
			
	set @Mensaje = 'Registro guardado'
	set @Procesado = 'True'
	return @@rowcount
		

End
	
	


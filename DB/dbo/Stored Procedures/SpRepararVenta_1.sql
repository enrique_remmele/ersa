﻿CREATE Procedure [dbo].[SpRepararVenta]

	--Entrada
	@IDTransaccion numeric(18,0)
	
As

Begin
	
	--Validar
	Declare @vMensaje varchar(200)
	Declare @vProcesado bit
	
	Set @vMensaje = 'No se proceso'
	Set @vProcesado = 'False'
	
	--Validar
	--Si existe
	If  Not Exists(Select * From Venta Where IDTransaccion=@IDTransaccion) Begin
		Set @vMensaje = 'El registro no existe!'
		Set @vProcesado = 'False'
		GoTo Salir
	End

	--Si no esta procesado
	If  (Select Procesado From Venta Where IDTransaccion=@IDTransaccion) = 'False' Begin
		Set @vMensaje = 'El registro no esta procesado!'
		Set @vProcesado = 'False'
		GoTo Salir
	End
	
	--Si esta anulado
	If  (Select Anulado From Venta Where IDTransaccion=@IDTransaccion) = 'True' Begin
		Set @vMensaje = 'El registro esta anulado! No se puede tocar'
		Set @vProcesado = 'False'
		GoTo Salir		
	End
	
	--Variables
	Declare @vCancelarAutomatico bit
	Declare @vTotal money
	Declare @vSaldo money
	Declare @vCobrado money
	Declare @vCobradoMigracion money
	Declare @vDescontado money
	Declare @vAcreditado money
	Declare @vCancelado bit
	
	Declare @vIDTransaccionNotaCredito int 
	--Reparar Notas de Credito
	Declare db_cursorNC cursor for
	Select IDTransaccionNotaCredito
	From NotaCreditoVenta 
	Where IDTransaccionVentaGenerada=@IDTransaccion
	Open db_cursorNC   
	Fetch Next From db_cursorNC Into @vIDTransaccionNotaCredito
	While @@FETCH_STATUS = 0 Begin 
	
		exec SpRepararNotaCredito @vIDTransaccionNotaCredito
		
		Fetch Next From db_cursorNC Into @vIDTransaccionNotaCredito
									
	End
	
	--Cierra el cursor
	Close db_cursorNC   
	Deallocate db_cursorNC

	--Hayar valores
	Set @vCancelarAutomatico = (Select CancelarAutomatico From VVenta Where IDTransaccion=@IDTransaccion)

	--Si cancela automatico, el saldo es 0
	If @vCancelarAutomatico = 1 Begin
		Set @vCancelado = 'True'
		Set @vSaldo = 0
		GoTo Actualizar
	End





	Set @vTotal = (Select Total From Venta Where IDTransaccion=@IDTransaccion)
	
	Set @vCobrado = IsNull((Select Sum(Importe) From VVentaDetalleCobranza 
							Where IDTransaccion=@IDTransaccion
							And Anulado='False'),0)

	Set @vCobradoMigracion = IsNull((Select Cobrado 
									  From VentaImportada 
									  Where IDTransaccion=@IDTransaccion),0)

	Set @vDescontado = IsNull((Select Sum(V.Importe) From vNotaCreditoVenta V 
								Where V.IDTransaccionVenta=@IDTransaccion
								And V.Anulado='False'),0)
	
	Set @vAcreditado = IsNull((Select Sum(Importe) From VNotaDebitoVenta Where IDTransaccionVenta=@IDTransaccion),0)
	
	Set @vSaldo = (@vTotal + @vAcreditado) - (@vCobrado + @vDescontado + @vCobradoMigracion)
	
	If @vSaldo < 1  and @vSaldo >-10 Begin
		Set @vCancelado = 'True'
		Set @vSaldo = 0
	End Else Begin 
		Set @vCancelado = 'False'	
	End

Actualizar:

	Update Venta Set Saldo=@vSaldo,
					 Cobrado=@vCobrado,
					 Descontado=@vDescontado,
					 Acreditado=@vAcreditado,
					 Cancelado=@vCancelado
	Where IDTransaccion=@IDTransaccion
	
	
	--Recalcular sus impuestos
	Declare @vIDImpuesto int
	Declare @vTotalDiscriminado money
	Declare @vTotalImpuesto money
	Declare @vTotalDescuento money
	
	Declare db_cursor cursor for
	Select IDImpuesto, Sum(Total), Sum(TotalDiscriminado), Sum(TotalImpuesto), Sum(TotalDescuento)
	From DetalleVenta Where IDTransaccion=@IDTransaccion Group By IDImpuesto
	Open db_cursor   
	Fetch Next From db_cursor Into @vIDImpuesto, @vTotal, @vTotalDiscriminado, @vTotalImpuesto, @vTotalDescuento
	While @@FETCH_STATUS = 0 Begin 
	
		Update DetalleImpuesto Set Total=@vTotal,
									TotalDiscriminado=@vTotalDiscriminado,
									TotalImpuesto=@vTotalImpuesto,
									TotalDescuento=@vTotalDescuento
		Where IDTransaccion=@IDTransaccion And IDImpuesto=@vIDImpuesto
		
		Fetch Next From db_cursor Into @vIDImpuesto, @vTotal, @vTotalDiscriminado, @vTotalImpuesto, @vTotalDescuento
									
	End
	
	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor
	
	
	Set @vMensaje = 'Registro procesado'
	Set @vProcesado = 'True'
		
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


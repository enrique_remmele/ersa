﻿
CREATE Procedure [dbo].[SpGrupo]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Estado bit,
	@IDCuentaContable integer,
	@SoloVales bit=0,
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Grupo Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDGrupo tinyint
		set @vIDGrupo = (Select IsNull((Max(ID)+1), 1) From Grupo)

		--Insertamos
		Insert Into Grupo(ID, Descripcion, Estado, IDCuentaContable, SoloVales)
		Values(@vIDGrupo, @Descripcion, @Estado, @IDCuentaContable, @SoloVales)		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From Grupo Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Grupo Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Grupo Set Descripcion=@Descripcion, Estado=@Estado, IDCuentaContable=@IDCuentaContable, SoloVales=@SoloVales
						
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Grupo Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
				
		--Si tiene una relacion con DetalleFondoFijo
		if exists(Select * From DetalleFondoFijo Where IDGrupo=@ID) begin
			set @Mensaje = 'El registro tiene DetalleFondoFijo asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con GrupoUsuario
		if exists(Select * From GrupoUsuario Where IDGrupo=@ID) begin
			set @Mensaje = 'El registro tiene GrupoUsuario asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Grupo 
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End



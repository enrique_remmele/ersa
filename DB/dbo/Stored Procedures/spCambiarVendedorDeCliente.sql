﻿CREATE procedure [dbo].[spCambiarVendedorDeCliente]
@IdCliente integer,
@IdVendedor tinyint,
@FechaDesde Datetime,
@FechaHasta Datetime,
@ActualizarVenta bit,
@Mensaje varchar(100) output,
@Procesado varchar(5) output
as
begin
	if not exists(Select * From Cliente Where ID =@IdCliente) begin
			set @Mensaje = 'El Cliente Actual no existe!'
			set @Procesado = 'False'
			return @@rowcount
	end;
	
	if not exists(Select * From Vendedor Where ID =@IdVendedor) begin
			set @Mensaje = 'El Vendedor no existe!'
			set @Procesado = 'False'
			return @@rowcount
	end;
	
  
  
 --Actualiza el vendedor asignado al cliente
 update Cliente
 set IDVendedor = @IdVendedor
 where ID = @IdCliente;
 
 if @ActualizarVenta = 'true' begin
   update Venta
   set IDVendedor = @IdVendedor
   where IDCliente = @IdCliente
   and FechaEmision between @FechaDesde
   and @FechaHasta;
 end;
 
 set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
end











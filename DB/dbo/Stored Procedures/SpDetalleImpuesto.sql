﻿CREATE Procedure [dbo].[SpDetalleImpuesto]

	--Entrada
	@IDTransaccion numeric(18,0),
	@IDImpuesto tinyint,
	@Total money,
	@TotalImpuesto money,
	@TotalDiscriminado money,
	@TotalDescuento money = NULL,
	@Operacion varchar(20),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin

	--Variable
	Declare @RetencionIVA money
	Declare @PorcentajeRetencion tinyint

	--Validar
	--Transaccion
	If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End

	--BLOQUES
	If @Operacion='INS' Begin
	
		--RetencionIVA
		set @PorcentajeRetencion = (Select Top(1) CompraPorcentajeRetencion  From Configuraciones )
		set @RetencionIVA  = (@TotalImpuesto  * @PorcentajeRetencion) / 100
		set @RetencionIVA = ROUND(@RetencionIVA,0)
		
		Insert Into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, RetencionIVA )
		Values(@IDTransaccion, @IDImpuesto, @Total, @TotalImpuesto, @TotalDiscriminado, @TotalDescuento, @RetencionIVA )	
		
		Set @Procesado = 'False'
		return @@rowcount	
		
	End

	If @Operacion='UPD' Begin
		
		Delete From DetalleImpuesto where IDTransaccion = @IDTransaccion and IDImpuesto = @IDImpuesto
		--RetencionIVA
		set @PorcentajeRetencion = (Select Top(1) CompraPorcentajeRetencion  From Configuraciones )
		set @RetencionIVA  = (@TotalImpuesto  * @PorcentajeRetencion) / 100
		set @RetencionIVA = ROUND(@RetencionIVA,0)
		
		Insert Into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, RetencionIVA )
		Values(@IDTransaccion, @IDImpuesto, @Total, @TotalImpuesto, @TotalDiscriminado, @TotalDescuento, @RetencionIVA )	
		
		Set @Procesado = 'False'
		return @@rowcount	
		
	End
		
End


﻿CREATE Procedure [dbo].[spPedidoNotaCreditoAcuerdoComercial]
@IDTransaccionPedidoNotaCredito as int
as 
begin
	
	Declare @vDesdePedidoVenta as bit
	Declare @vMontoPedidoNC as decimal(18,3)
	Declare @vMontoTotalVenta as decimal(18,3)
	Declare @vIDCliente as int

	Declare @vTotalProducto as decimal(18,3)
	Declare @vIDProducto as decimal(18,3)
	Declare @vCantidad as int
	Declare @vImporte as decimal(18,3)
	Declare @vIDTransaccionPedido AS INT
	Declare @vIDTransaccionVenta AS INT
		


	Set @vDesdePedidoVenta = (Select IsNull(DesdepedidoVenta,0) from PedidoNotaCredito where idtransaccion = @IDTransaccionPedidoNotaCredito)
	Set @vMontoPedidoNC = (Select Total from PedidoNotaCredito where idtransaccion = @IDTransaccionPedidoNotaCredito)
	Set @vIDCliente =(Select IDCliente from PedidoNotaCredito where idtransaccion = @IDTransaccionPedidoNotaCredito)

	--PEDIDO DE VENTA
	if @vDesdePedidoVenta = 1 begin
		--Establecemos el monto total de los productos de pedidos
		--Set @vMontoTotalVenta = (select Sum(Total-TotalDescuento) from detallepedido where IDTransaccion in 
		Set @vMontoTotalVenta = (select Sum(Total) from detallepedido where IDTransaccion in 
		(select IDTransaccionPedidoVenta from PedidoNotaCreditoPedidoVenta where IDTransaccionPedidoNotaCredito = @IDTransaccionPedidoNotaCredito))
		
		Declare cPedidoVenta cursor for
				Select IDProducto, Total, Cantidad, IDTransaccion From DetallePedido where IDTransaccion in 
				(select IDTransaccionPedidoVenta from PedidoNotaCreditoPedidoVenta 
				where IDTransaccionPedidoNotaCredito = @IDTransaccionPedidoNotaCredito)
		
			Open cPedidoVenta   
			fetch next from cPedidoVenta into @vIDProducto, @vTotalProducto, @vCantidad, @vIDTransaccionPedido

			While @@FETCH_STATUS = 0 Begin  

					Set @vImporte = @vTotalProducto/@vMontoTotalVenta*@vMontoPedidoNC
					Set @vImporte = @vImporte / @vCantidad
					INSERT INTO DetalleNotaCreditoAcuerdo(IDProducto,IDCliente,DescuentoUnitario,IDTransaccionPedidoVenta,IDTransaccionPedidoNotaCredito,IDTransaccionVenta,IDTransaccionNotaCredito)
					VALUES(@vIDProducto,@vIDCliente,@vImporte,@vIDTransaccionPedido,@IDTransaccionPedidoNotaCredito,0,0)
			
				fetch next from cPedidoVenta into @vIDProducto, @vTotalProducto, @vCantidad, @vIDTransaccionPedido

			End

			close cPedidoVenta 
		deallocate cPedidoVenta
	end

	--VENTA
	if @vDesdePedidoVenta = 0 begin
		
		--Set @vMontoTotalVenta = (select Sum(Total-TotalDescuento) from detalleventa where IDTransaccion in 
		Set @vMontoTotalVenta = (select Sum(Total) from detalleventa where IDTransaccion in 
		(select IDTransaccionVentaGenerada from PedidoNotaCreditoVenta where IDTransaccionPedidoNotaCredito = @IDTransaccionPedidoNotaCredito))
		
		Declare cCFVenta cursor for
				Select IDProducto, Total, Cantidad, IDTransaccion From detalleventa where IDTransaccion in 
				(select IDTransaccionVentaGenerada from PedidoNotaCreditoVenta 
				where IDTransaccionPedidoNotaCredito = @IDTransaccionPedidoNotaCredito)
		
			Open cCFVenta   
			fetch next from cCFVenta into @vIDProducto, @vTotalProducto, @vCantidad, @vIDTransaccionVenta

			While @@FETCH_STATUS = 0 Begin  

					Set @vImporte = @vTotalProducto/@vMontoTotalVenta*@vMontoPedidoNC
					Set @vImporte = @vImporte / @vCantidad
					INSERT INTO DetalleNotaCreditoAcuerdo(IDProducto,IDCliente,DescuentoUnitario,IDTransaccionPedidoVenta,IDTransaccionPedidoNotaCredito,IDTransaccionVenta,IDTransaccionNotaCredito)
					VALUES(@vIDProducto,@vIDCliente,@vImporte,0,@IDTransaccionPedidoNotaCredito,@vIDTransaccionVenta,0)
			
				fetch next from cCFVenta into @vIDProducto, @vTotalProducto, @vCantidad, @vIDTransaccionVenta

			End

			close cCFVenta 
		deallocate cCFVenta

	end
end

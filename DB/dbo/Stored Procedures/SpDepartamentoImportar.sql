﻿CREATE Procedure [dbo].[SpDepartamentoImportar]

	--Entrada
	@Codigo varchar(100),
	@Descripcion varchar(100),
	@IDPais int,
	
	@Actualizar bit = 'True'
	
As

Begin

	Declare @vID int
	
	--Si existe
	If Exists(Select * From Departamento Where Descripcion=@Descripcion And IDPais=@IDPais) Begin
		Update Departamento Set Descripcion=@Descripcion, Codigo=@Codigo
		Where Descripcion=@Descripcion
	End
	
	--Si existe
	If Not Exists(Select * From Departamento Where Descripcion=@Descripcion And IDPais=@IDPais) Begin
		If @Actualizar = 'True' Begin
			Set @vID = (Select IsNull(Max(ID)+1,1) From Departamento)
			Insert Into Departamento(ID, IDPais, Descripcion, Orden, Estado, Codigo)
			Values(@vID, @IDPais, @Descripcion, 0, 'True', @Codigo)
		End
	End
	
End



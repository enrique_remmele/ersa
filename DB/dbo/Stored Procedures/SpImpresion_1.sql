﻿CREATE Procedure [dbo].[SpImpresion]

	--Entrada
	
As

Begin

	--FACTURA = 1
	If Not Exists(Select * From Impresion Where IDImpresion=1) Begin
		Insert into Impresion(IDImpresion, Descripcion, Detalle, Vista, VistaDetalle, Observacion)
		Values(1, 'Factura', 'True', 'VImpresionFactura', 'VImpresionFacturaDetalle', 'Documento de impresion de factura credito/contado')
	End

	--CHEQUE = 2
	If Not Exists(Select * From Impresion Where IDImpresion=2) Begin
		Insert into Impresion(IDImpresion, Descripcion, Detalle, Vista, VistaDetalle, Observacion)
		Values(2, 'Cheques', 'False', 'VImpresionCheque', '', 'Documento de impresion de cheques')
	End

	--REMISION= 3
	If Not Exists(Select * From Impresion Where IDImpresion=3) Begin
		Insert into Impresion(IDImpresion, Descripcion, Detalle, Vista, VistaDetalle, Observacion)
		Values(3, 'Remision', 'False', 'vImpresionRemision', 'vImpresionDetalleRemision', 'Documento de impresion de Remisiones')
	End

	Select * From Impresion

End

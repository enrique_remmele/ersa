﻿
CREATE Procedure [dbo].[SpCanjeRecaudacion]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursal tinyint = NULL,
	@Numero int = null,
	@Fecha date = NULL,
	@IDMoneda tinyint = null,
	@Cotizacion money= NULL,
	@Observacion varchar(100) = NULL,
	@Total money = NULL,
		
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
	
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from CanjeRecaudacion where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		Declare @vNumero int
		Set @vNumero = ISNULL((Select MAX(Numero)+1 From CanjeRecaudacion Where IDSucursal=@IDSucursal),1)
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--Insertar en CanjeRecaudacion
		Insert Into CanjeRecaudacion (IDTransaccion, IDSucursal , Numero, Fecha , IDMoneda, Cotizacion , Observacion,Total)
						Values(@IDTransaccionSalida , @IDSucursal, @vNumero, @Fecha, @IDMoneda,  @Cotizacion, @Observacion, @Total)
		
										
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From CanjeRecaudacion Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida = 0
			return @@rowcount
		End
		
		----VALIDAR
		--Que ningun cheque este rechazado
		If Exists(Select * From DetalleCanjeRecaudacion D 
							join ChequeCliente C on D.IDTransaccion = C.IDTransaccion 
							Where D.IDTransaccion=@IDTransaccion And  C.Rechazado='True') Begin 
			set @Mensaje = 'El Cheque Esta Rechazado!'
			set @Procesado = 'True'
			set @IDTransaccionSalida = 0
			return @@rowcount		
		end
		
		--Que ningun cheque este conciliado
		If Exists(Select * From DetalleCanjeRecaudacion D 
							join ChequeCliente C on D.IDTransaccion = C.IDTransaccion 
							where D.IDTransaccion=@IDTransaccion And  C.Conciliado='True') Begin 
			set @Mensaje = 'El Cheque Está Conciliado!'
			set @Procesado = 'True'
			set @IDTransaccionSalida = 0
			return @@rowcount
		end

		--Verificar que algun cheque no tenga deposito o rechazo con fecha posterior
		--Validar
		If dbo.FVerificarChequesDepositados(@IDTransaccion) = 'DEPOSITO' Begin
			Set @Mensaje  = 'No se puede Eliminar. Uno de los Cheques tiene Redeposito con fecha posterior!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida = 0
			Return @@rowcount
		End
		If dbo.FVerificarChequesDepositados(@IDTransaccion) = 'RECHAZO' Begin
			Set @Mensaje  = 'No se puede Eliminar. Uno de los Cheques tiene Rechazo con fecha posterior!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End

		--------Reestablecer el Efectivo,Cheque y Documento
		--Exec SpCanjeRecaudacionProcesar @IDTransaccion = @IDTransaccion,
		--								    @Operacion = @Operacion, 
		--								    @Mensaje = @Mensaje OUTPUT,
		--								    @Procesado = @Procesado OUTPUT
			
		--Eliminamos el detalle
		--Delete DetalleCanjeRecaudacion  where IDTransaccion = @IDTransaccion				
				
		--Reestablecemos el cheque
		EXEC SpChequeClienteSaldar @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la forma de pago		
		Delete From Efectivo Where IDTransaccion = @IDTransaccion
		Delete From FormaPagoDocumento Where IDTransaccion = @IDTransaccion
		Delete From FormaPago Where IDTransaccion = @IDTransaccion

		--Eliminamos el registro
		Update CanjeRecaudacion  
		Set Anulado = 'True'
		,IDUsuarioAnulado = @IDUsuario
		,FechaAnulado = GETDATE()
		Where IDTransaccion = @IDTransaccion
						
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		set @IDTransaccionSalida  = @IDTransaccion
		
		print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End

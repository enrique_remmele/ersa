﻿CREATE Procedure [dbo].[SpImportarCentroCosto]

	--Entrada
	--Identificadores
	@CC varchar(200) = NULL,
	@UN varchar(200) = NULL,
	@CCCodigo varchar(200) = '',

	@Actualizar bit



As

Begin

	Declare @vOperacion varchar(50)

	Declare @vMensaje varchar(50)
	Declare @vProcesado bit

	Declare @vIDUnidadNegocio tinyint
	Declare @vIDCentroCosto tinyint
	declare @vID int

	
	Set @vOperacion = 'INS'
	Set @vMensaje = 'Sin proceso'
	Set @vProcesado = 'False'

	If not Exists(Select Top(1) ID From UnidadNegocio Where Descripcion=@UN) Begin 
	   set @vIDUnidadNegocio = Isnull((select max(ID) from UnidadNegocio),0)+1
	   Insert into UnidadNegocio
	   values(@vIDUnidadNegocio,'',@UN,'True')
	End
	Set	@vIDUnidadNegocio = (select Isnull(ID,0) from UnidadNegocio where Descripcion = @UN)

	--Si no existe Centro de costo con la misma unidad de negocio
	--If not Exists(Select * From CentroCosto Where Descripcion=@CC and IDUnidadNegocio = @vIDUnidadNegocio) Begin
	--	set @vID = Isnull((select max(ID) from CentroCosto),0)+1
	--	Insert into CentroCosto
	--	values(@vID, @CCCodigo,@CC,'True',@vIDUnidadNegocio)
	--	set @vMensaje = concat('Centro de Costo ',@CC,' Procesado')
	--	set @vProcesado = 'True'
	--	GoTo Salir
	--End

	salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	

End

















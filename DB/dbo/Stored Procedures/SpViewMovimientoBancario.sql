﻿

CREATE Procedure [dbo].[SpViewMovimientoBancario]

As


--DEPOSITO

Select
'Fecha'=CONVERT (varchar (50),fecha,6),
'Movimiento'='DEP',
'Operacion'=Numero,
'Comprobante'=TipoComprobante + ' ' + Convert(varchar(50), Comprobante),
'Sucursal'=Suc,
'Debito'=Total,
'Credito'=0,
'Saldo'=0,
'Observacion'=Observacion,
'A la Orden',
'Conciliado'=Case When(Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= IDTransaccion 

From VDepositoBancario

Union All

--DEBITO

Select
'Fecha'=CONVERT (varchar (50),fecha,6),
'Movimiento'='DEB',
'Operacion'=Numero,
'Comprobante'=TipoComprobante + ' ' + Convert(varchar(50), Comprobante),
'Sucursal',
'Debito'=0,
'Credito'=Total,
'Saldo'=0,
'Observacion',
'A la Orden',
'Conciliado'=Case When(Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= IDTransaccion 
From VDebitoCreditoBancario
Where Debito = 'True'

Union All

--CREDITO
Select
'Fecha'=CONVERT (varchar (50),fecha,6),
'Movimiento'='CRE',
'Operacion'=Numero,
'Comprobante'=TipoComprobante + ' ' + Convert(varchar(50), Comprobante),
'Sucursal',
'Debito'=Total,
'Credito'=0,
'Saldo'=0,
'Observacion',
'A la Orden',
'Conciliado'=Case When(Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= IDTransaccion 

From VDebitoCreditoBancario
Where Credito = 'True'

Union All

--ORDEN DE PAGO
Select
'Fecha'=CONVERT (varchar (50),fecha,6),
'Movimiento'='PAGO',
'Operacion'=Numero,
'Comprobante'=TipoComprobante + ' ' + Convert(varchar(50), Comprobante),
'Sucursal',
'Debito'=0,
'Credito'=Importe,
'Saldo'=0,
'Observacion',
'A la Orden',
'Conciliado'=Case When(Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= IDTransaccion 
From VOrdenPago
Where Cheque = 'True'



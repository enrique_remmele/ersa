﻿
CREATE Procedure [dbo].[SpVentaEstado]

	--Entrada
	@IDPedido int,
	@IDSucursal tinyint,
	@IDTransaccionVenta numeric(18,0)
	
	
As

Begin

	Declare @vProcesado bit
	Declare @vIDTransaccionPedido int
	Declare @vMensaje varchar(50)
	Declare @vComprobante varchar(50)
	Declare @vIDTransaccionPedidoNotaCredito int
	Declare @vTotalNotaCredito decimal(18,3)
	
	Set @vProcesado = 'False'
	Set @vMensaje = 'No procesado'
	Set @vComprobante = '---'

	--Si no se guardo en la BD	
	If Not Exists(Select * From Venta Where IDTransaccion=@IDTransaccionVenta) Begin
		Set @vProcesado = 'False'
		Set @vMensaje = 'La venta no se guardo!'
		GoTo Salir
	End

	If (Select Procesado From Venta Where IDTransaccion=@IDTransaccionVenta) = 'False' Begin
		Set @vProcesado = 'False'
		Set @vMensaje = 'La venta no se proceso!'
		GoTo Salir
	End

	
	Set @vIDTransaccionPedido = (Select Max(IDTransaccion) From Pedido Where Numero=@IDPedido And IDSucursal=@IDSucursal And Procesado='True')
	

	--Si el pedido ya corresponde a otra venta sin anular, no es correcta
	If Exists(Select Top(1) V.IDTransaccion From Venta V 
										Join PedidoVenta PV On V.IDTransaccion=PV.IDTransaccionVenta 
										Where V.IDTransaccion<>@IDTransaccionVenta 
										And V.Anulado='False' 
										And PV.IDTransaccionPedido=@vIDTransaccionPedido) Begin
		Set @vProcesado = 'False'
		Set @vMensaje = 'La venta no se proceso!'
		GoTo Salir
	End

	if Exists(select * from PedidoVenta where IDTransaccionPedido = @vIDTransaccionPedido and IDTransaccionVenta = @IDTransaccionVenta) begin
		GoTo OK
	end

	Update Pedido Set Facturado='True'	
	Where IDTransaccion = @vIDTransaccionPedido
	
	Insert Into PedidoVenta(IDTransaccionPedido, IDTransaccionVenta)
	Values(@vIDTransaccionPedido, @IDTransaccionVenta)

	set @vIDTransaccionPedidoNotaCredito = isnull((select top(1) IDTransaccionPedidoNotaCredito from PedidoNotaCreditoPedidoVenta where IDTransaccionPedidoVenta = @vIDTransaccionPedido),0)
	--si tiene pedido de nc, relacionamos el pedido de nc con la venta
	if @vIDTransaccionPedidoNotaCredito <> 0 begin
		Set @vTotalNotaCredito = (Select Total from PedidoNotaCreditoPedidoVenta where IDTransaccionPedidoNotaCredito= @vIDTransaccionPedidoNotaCredito and IDTransaccionPedidoVenta = @vIDTransaccionPedido)
		
		Insert Into PedidoNotaCreditoVenta(IDTransaccionPedidoNotaCredito, IDTransaccionVentaGenerada, ID, Importe, Cobrado, Descontado, Saldo) 
		Values (@vIDTransaccionPedidoNotaCredito,@IDTransaccionVenta, 0, @vTotalNotaCredito,0,0,0)

		Update DetalleNotaCreditoDiferenciaPrecio 
				set IDTransaccionVenta = @IDTransaccionVenta
		Where IDTransaccionPedidoVenta = @vIDTransaccionPedido
	
	end

OK:
		
	Set @vProcesado = 'True'
	Set @vMensaje = 'OK'
	Set @vComprobante = (Select Comprobante From VVenta Where IDTransaccion=@IDTransaccionVenta)
	
Salir:

	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado, 'Comprobante'=@vComprobante
	
End



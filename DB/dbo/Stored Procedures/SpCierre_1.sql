﻿
CREATE Procedure [dbo].[SpCierre]

	--CARLOS 21-02-2014
	@IDTransaccion numeric(18,0) = NULL,
	@Comprobante varchar(8) = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante varchar(50) = NULL,
	@Fecha date = NULL,
	@Descripcion varchar(500) = NULL,	
	@Total money = 0,
	@Credito money = 0,
	@Debito money = 0,
	@Saldo money = 0,
	@IDAccion tinyint = NULL,
	@Accion varchar(8) = Null,
	@Anho int = NULL,
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion Int,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
	
	
As

Begin
	If @Operacion = 'INS' Begin
	
		--Numero
		Set @NroComprobante = ISNULL((Select MAX(NroComprobante+1) From CierreReapertura Where IDSucursal=@IDSucursal),1)
		
		--Si ya existe el documento
	
		If Exists(Select * From CierreReapertura Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante And IDSucursal=@IDSucursal) Begin
			set @Mensaje = 'El numero de comprobante ya existe!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Insertar Transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
			
			
			
		--Insertar en Orden de Pago
		Insert Into CierreReapertura(IDTransaccion, NroComprobante, IdTipoComprobante,Comprobante,IdSucursal, 
									FechaEmision, Descripcion,Total, Debito, Credito, Saldo,Anulado,Procesado,
									IdAccion, Accion,Anho)
		Values(@IDTransaccionSalida, @NroComprobante, @IDTipoComprobante, @Comprobante, @IDSucursal, @Fecha,
				@Descripcion,@Total, @Debito, @Credito, @Saldo,'False','False',@IdAccion,@Accion,@Anho)
		
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount	
	
	End
	
	If @Operacion = 'DEL' Begin
	 if (Select Conciliado from ASiento where IDTransaccion = @IDTransaccion) = 'True' begin
			set @Mensaje = 'El Asiento ya esta conciliado!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
	 End
	 
	 Delete from CierreReapertura where idtransaccion = @IDTransaccion
	 Delete from DetalleCierreReapertura where idtransaccion = @IDTransaccion 
	 Delete from Asiento where idtransaccion = @IDTransaccion 
	 Delete from DetalleAsiento where idtransaccion = @IDTransaccion 
	 
	 set @Mensaje = 'Registro eliminado'
	 set @Procesado = 'True'
	 return @@rowcount
	 
	End
		
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
	
	
End
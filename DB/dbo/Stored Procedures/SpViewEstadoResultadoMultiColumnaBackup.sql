﻿CREATE Procedure [dbo].[SpViewEstadoResultadoMultiColumnaBackup]

	--Entrada
	@Año smallint,
	@Mes tinyint,
	@MesHasta tinyint,
		
	@IDSucursal int = 0,
	
	@Factor int
	
As

Begin

	SET NOCOUNT ON
	
	--variables
	Declare @vID tinyint
	Declare @vM varchar(50)
	Declare @vCodigo varchar(50)
	Declare @vDenominacion varchar(200)
	Declare @vCategoria tinyint
	Declare @vImputable bit
	Declare @vDebito money
	Declare @vCredito money
	Declare @vSaldo money
	Declare @vDebitoAnterior money
	Declare @vCreditoAnterior money
	Declare @vSaldoAnterior money
	Declare @vDebitoActual money
	Declare @vMovimientoMes money
	Declare @vCreditoActual money
	Declare @vSaldoActual money
	
	--Crear la tabla
	Create table #EstadoResultado(ID tinyint,
								Año smallint,
								Mes tinyint,
								IDSucursal int,
								M varchar(50),
								Codigo varchar(50),
								Denominacion varchar(200),
								Categoria tinyint,
								Imputable bit,
								MostrarCodigo varchar(50),
								SaldoAnterior money,
								MovimientoMes money,
								SaldoActual money
								)
								
	set @vID = (Select IsNull(MAX(ID)+1,1) From #EstadoResultado)
	
	If @Factor = 0 Begin
		Set @Factor = 1
	End
	
	Declare @vMesActual tinyint = @Mes
	
	While @vMesActual <= @Meshasta Begin
		
		print @vMesActual
			
		Declare db_cursor cursor for
		Select	CC.Codigo, CC.Descripcion, CC.Categoria, CC.Imputable	From VCuentaContable CC	
		Where (SUBSTRING(CC.Codigo, 0, 2) Between '4' And '9')
		Open db_cursor   
		Fetch Next From db_cursor Into	@vCodigo, @vDenominacion, @vCategoria, @vImputable
		While @@FETCH_STATUS = 0 Begin 
			
			--Tipo de Cuenta
			Declare @vCuentaTipo varchar(50)
			Declare @vPrefijo varchar(50)

			Set @vPrefijo = SubString(@vCodigo, 0, 2)

			--Cuentas del Debe
			If @vPrefijo = '1' Or @vPrefijo = '5' Begin
				Set @vCuentaTipo = 'DEBE'
			End

			--Cuentas del Haber
			If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
				Set @vCuentaTipo = 'HABER'
			End
						
			Set @vDebitoAnterior = 0
			Set @vCreditoAnterior = 0
			
			--Saldo Anterior
			If @Mes > 1 Begin
				
				--Toda la Empresa
				If @IDSucursal = 0 Begin
					Set @vDebitoAnterior = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes<@MesHasta And Cuenta=@vCodigo),0)
					Set @vCreditoAnterior = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes<@MesHasta And Cuenta=@vCodigo),0)
				End
				
				--Por Sucursal
				If @IDSucursal > 0 Begin
					Set @vDebitoAnterior = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes<@MesHasta And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
					Set @vCreditoAnterior = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes<@MesHasta And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
				End
				
			End
			
			--Saldo Actual
			--Toda la Empresa
			If @IDSucursal = 0 Begin
				Set @vDebito = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes=@vMesActual And Cuenta=@vCodigo),0)
				Set @vCredito = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes=@vMesActual And Cuenta=@vCodigo),0)
			End
			
			--Por Sucursal
			If @IDSucursal > 0 Begin
				Set @vDebito = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes=@vMesActual And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
				Set @vCredito = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes=@vMesActual And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
			End
			
			If @vCuentaTipo = 'DEBE' Begin
				Set @vSaldoAnterior = IsNUll(@vDebitoAnterior  - @vCreditoAnterior,0)
				Set @vSaldoActual = @vSaldoAnterior + (@vDebito - @vCredito)
				Set @vMovimientoMes = @vDebito - @vCredito
			End
			
			If @vCuentaTipo = 'HABER' Begin
				Set @vSaldoAnterior = IsNUll(@vCreditoAnterior-@vDebitoAnterior,0)
				Set @vSaldoActual = @vSaldoAnterior + (@vCredito - @vDebito)
				Set @vMovimientoMes = @vCredito - @vDebito 
			End
			
			Set @vSaldoAnterior = @vSaldoAnterior / @Factor
			Set @vSaldoActual = @vSaldoActual / @Factor
			
			Set @vDebito = @vDebito / @Factor
			Set @vCredito = @vCredito / @Factor
			Set @vMovimientoMes = @vMovimientoMes / @Factor
				
			--Si ya existe, actualizar
			--Toda la Empresa
			If @IDSucursal = 0 Begin
				If Exists(Select * From #EstadoResultado Where ID=@vID And Año=@Año And Mes=@vMesActual And Codigo=@vCodigo) Begin
				
					Update #EstadoResultado Set 
					SaldoAnterior=SaldoAnterior+@vSaldoAnterior,
					MovimientoMes=MovimientoMes+@vMovimientoMes,
					SaldoActual=SaldoActual+@vSaldoActual
					Where ID=@vID And Año=@Año And Mes=@vMesActual And Codigo=@vCodigo
					
				End Else Begin
					Insert Into #EstadoResultado(ID, Año, Mes, M, Codigo, Denominacion, Categoria, Imputable, SaldoAnterior, MovimientoMes, SaldoActual, MostrarCodigo)
					Values(@vID, @Año, @vMesActual, dbo.FMes(@vMesActual), @vCodigo, @vDenominacion, @vCategoria, @vImputable, @vSaldoAnterior, @vMovimientoMes, @vSaldoActual, '1')
				End
			End
			
			--Por Sucursal
			If @IDSucursal > 0 Begin
				If Exists(Select * From #EstadoResultado Where ID=@vID And Año=@Año And Mes=@vMesActual And Codigo=@vCodigo And IDSucursal=@IDSucursal) Begin
				
					Update #EstadoResultado Set 
					SaldoAnterior=SaldoAnterior+@vSaldoAnterior,
					MovimientoMes=MovimientoMes+@vMovimientoMes,
					SaldoActual=SaldoActual+@vSaldoActual
					Where ID=@vID And Año=@Año And Mes=@vMesActual And Codigo=@vCodigo
					
				End Else Begin
					Insert Into #EstadoResultado(ID, Año, Mes, IDSucursal, M, Codigo, Denominacion, Categoria, Imputable, SaldoAnterior, MovimientoMes, SaldoActual, MostrarCodigo)
					Values(@vID, @Año, @vMesActual, @IDSucursal, dbo.FMes(@vMesActual), @vCodigo, @vDenominacion, @vCategoria, @vImputable, @vSaldoAnterior, @vMovimientoMes, @vSaldoActual, '1')
				End
			End
			
	Siguiente:
			
			Fetch Next From db_cursor Into	@vCodigo, @vDenominacion, @vCategoria, @vImputable
			
		End

		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   		
	
		Set @vMesActual = @vMesActual + 1
				
	End	
		
	Select * From #EstadoResultado Where ID=@vID
	
End


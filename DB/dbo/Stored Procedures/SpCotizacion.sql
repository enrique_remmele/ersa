﻿CREATE Procedure [dbo].[SpCotizacion]

	--Entrada
	@IDMoneda smallint,
	@Cotizacion money,
	@Fecha datetime,
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin
	--VARIABLES
	Declare @vID int

	--Verificar si ya existe
	If exists(select * from cotizacion where IDMoneda = @IDMoneda and cast(fecha as date)= @Fecha) begin
		set @vID = (select ID from cotizacion where IDMoneda = @IDMoneda and cast(fecha as date)= @Fecha)

		Update Cotizacion
		set Cotizacion = @Cotizacion
		where ID = @vID
		--Auditoria
		Exec SpLogSuceso @Operacion='UPD', @Tabla='COTIZACION', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal

	 end  else if not exists(select * from cotizacion where IDMoneda = @IDMoneda and cast(fecha as date)= @Fecha) begin
		Set @vID = IsNull((Select Max(ID) + 1 From Cotizacion), 1)
		--Insertamos
		Insert Into Cotizacion(ID, Fecha, IDMoneda, Cotizacion)
		Values(@vID, @Fecha, @IDMoneda, @Cotizacion)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion='INS', @Tabla='COTIZACION', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
	End	
		
	set @Mensaje = 'Registro guardado!'
	set @Procesado = 'True'
	return @@rowcount
				
End


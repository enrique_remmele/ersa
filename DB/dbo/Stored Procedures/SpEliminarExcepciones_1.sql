﻿
CREATE Procedure [dbo].[SpEliminarExcepciones]

	@IDUsuario int,
	@IDProducto int,
	@IDListaPrecio int

	
As

Begin
  
	--Declarar variables 
	declare @vIDTipoDescuento tinyint
	declare @vIDMoneda tinyint
	declare @vDescuento money
	declare @vPorcentaje decimal(9,6)
	declare @vDesde date
	declare @vHasta date
	declare @vIDCliente int
	declare @vIDSucursal int
	
	Begin
		
	--Crear la tabla temporal
    create table #TablaTemporal(IDCliente int,
								IDTipoDescuento tinyint,
								IDMoneda tinyint,
								Descuento money,
								Porcentaje decimal(9,6),
								Desde date,
								Hasta date)
		
		
	Declare db_cursor cursor for
		
	Select IDTipoDescuento,IDMoneda, Descuento,Porcentaje,Desde,Hasta, IDCliente, IDSucursal
	From ProductoListaPrecioExcepciones
	where IDProducto = @IDProducto
	and IDListaPrecio = @IDListaPrecio
	
	Open db_cursor   
	Fetch Next From db_cursor Into @vIDTipoDescuento,@vIDMoneda,@vDescuento,@vPorcentaje,@vDesde,@vHasta, @vIDCliente, @vIDSucursal
	While @@FETCH_STATUS = 0 Begin 
			
		--Insertar si aún no existe		
		--insertar auditoria
		Insert Into aProductoListaPrecioExcepciones(IDProducto, IDCliente, IDListaPrecio, IDMoneda, IDSucursal, IDTipoDescuento, Descuento, Porcentaje, Desde, Hasta, IdUsuario, FechaModificacion, accion)
		Values(@IDProducto, @vIDCliente, @IDListaPrecio, @vIDMoneda, @vIDSucursal, @vIDTipoDescuento, @vDescuento, @vPorcentaje, @vDesde, @vHasta, @IDUsuario, GETDATE(),'MPR')		
		

		Fetch Next From db_cursor Into @vIDTipoDescuento,@vIDMoneda,@vDescuento,@vPorcentaje,@vDesde,@vHasta, @vIDCliente, @vIDSucursal
			
	End
			
	delete from ProductoListaPrecioExcepciones 
	where IDProducto = @IDProducto
	and IDListaPrecio = @IDListaPrecio
	--and IDCliente = @vIDCliente
	--and IDSucursal = @IDSucursal
	--and IDTipoDescuento = @vIDTipoDescuento

	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor		   			
		
	End	
	
	
End



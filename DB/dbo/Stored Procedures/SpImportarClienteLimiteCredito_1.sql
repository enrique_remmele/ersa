﻿CREATE Procedure SpImportarClienteLimiteCredito

	--Entrada
	--Identificadores
	@Referencia varchar(50) = NULL,
	
	--Credito
	@LimiteCredito money = 0,
	
	--Opciones
	@Actualizar Bit = 'False'
	
As

Begin

	--ID's
	Declare @vID int
	Declare @Mensaje varchar(200)
	Declare @Procesado bit
	
	--Verificamos si existe
	If Not Exists(Select Top(1) * From Cliente Where Referencia=@Referencia) Begin
		Set @Mensaje = 'No se encontro el cliente'
		Set @Procesado = 'False'
		GoTo Salir
	End		
	
	--Si es para actualizar, verificar la configuracion
	If @Actualizar='False' Begin
		Set @Mensaje = 'Sin actualizar.'
		Set @Procesado = 'True'
		GoTo Salir
	End
	
	Set @vID = (Select Top(1) ID From Cliente Where Referencia=@Referencia)
		
	--Actualizamos la fecha de alta
	Update Cliente Set LimiteCredito=@LimiteCredito
	Where ID=@vID

	--Actualizar su saldo
	Exec SpAcualizarSaldoCliente @vID

Salir:
	Select 'Mensaje'=@Mensaje, 'Procesado'=@Procesado
End

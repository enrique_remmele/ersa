﻿CREATE Procedure [dbo].[SpViewChequeClienteDiferidoMovimiento]

	--Entrada
	@Mes tinyint,
	@Año Smallint,
	
	--Salida
	
	@Mensaje varchar(200) output ,
	@IDTransaccionSalida numeric(18,0) output
As

Begin
    
    ---Variables para CALCULAR SALDO 
	declare @vSaldo money
	declare @vIngresoSaldo money
	declare @vDepositoSaldo money
	declare @vDctoCheque money
	
	--Declarar variables Variables
	declare @vID tinyint
	declare @vFecha date
	declare @vDia varchar (10)
	declare @vIngreso money
	declare @vDeposito money
	declare @vDescuentoCheque money
	declare @vFechaFiltro date
	declare @vIDSucursal tinyint
	declare @vCondicion varchar (10)
	
	Set @vFechaFiltro = '01-' + CONVERT(varchar(50), @Mes) + '-' + CONVERT(varchar(50), @Año)
				
	set @vIngresoSaldo  = IsNull((Select Sum(Importe) From VChequeClienteVenta Where Fecha < @vFechaFiltro),0)
	set @vDepositoSaldo  = IsNull((Select Sum(Importe) From VChequesDiferidosDepositados where DescuentoCheque='False' And Fecha < @vFechaFiltro ),0)
	set @vDctoCheque  = IsNull((Select Sum(Importe) From VChequesDiferidosDepositados where DescuentoCheque='True' And Fecha < @vFechaFiltro ),0)
	
	
	
	Set @vSaldo = @vIngresoSaldo  - (@vDepositoSaldo  + @vDctoCheque )  
	set @vID = (Select IsNull(MAX(ID)+1,1) From ChequeClienteDiferidoMovimiento )
	
	--Insertar datos	
	Begin
	
		Declare db_cursor cursor for
		Select  Fecha, Dia, Ingreso, Deposito, DescuentoCheque , IDsucursal,[Cond.]   From VSaldoChequesDiferidos
		where month(fecha)=@mes and year (Fecha)= @año 
		Open db_cursor   
		Fetch Next From db_cursor Into @vFecha, @vDia, @vIngreso, @vDeposito, @vDescuentoCheque, @vIDSucursal, @vCondicion
		While @@FETCH_STATUS = 0 Begin  
		
			set @vSaldo = (@vSaldo+@vIngreso)-(@vDeposito+@vDescuentoCheque)  
			
			Insert Into ChequeClienteDiferidoMovimiento (ID,fecha, Dia, Ingreso,Deposito,DescuentoCheque,Saldo, IDSucursal, Condicion)
			Values(@vID, @vFecha, @vDia, @vIngreso, @vDeposito, @vDescuentoCheque, @vSaldo, @vIDSucursal, @vCondicion)
		
			Fetch Next From db_cursor Into @vFecha, @vDia, @vIngreso, @vDeposito, @vDescuentoCheque, @vIDSucursal, @vCondicion
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		
	End	
	set @Mensaje = 'Sin error!'
	Set @IDTransaccionSalida = @vID
	--Insertar fechas son datos	
	
End
	




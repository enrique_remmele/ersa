﻿CREATE Procedure [dbo].[SpConsultarAprobarPedidoNotaCredito]
	--Entrada
	@IDTransaccion as numeric(18,0),
	@ObservacionAutorizador varchar(500) = '',
	--@Estado as bit,
	@IDUsuario int,
	@Operacion varchar(20),
	@IDSucursal int,
	@IDTerminal int,

	@Mensaje as varchar(200) = '' output ,
	@Procesado as bit = 'False' output 
As
Begin
	
	if @Operacion = 'APROBAR' begin
		Declare @vImportePedido as decimal(18,2)
		Declare @vImporteLimiteUsuario as decimal(18,2)
		Declare @vPorcentajeLimiteUsuario as decimal(18,2)
		Declare @vMontoPorcentaje as decimal(18,2)
		Declare @vMontoLimite as decimal(18,2)= 0
		Declare @vIDSubMotivoNotaCredito as int 
		Declare @vImporteVentaReferencia as decimal(18,2)
		Declare @vSaldoVentaReferencia as decimal(18,2)
		Declare @vAplicar as bit = 0
		
		Set @vIDSubMotivoNotaCredito = IsNull((Select IDSubMotivoNotaCredito from PedidoNotaCredito where IDTransaccion = @IDTransaccion),0)
		
		if ((Select Motivo from SubMotivoNotaCredito where id = @vIDSubMotivoNotaCredito) = 'DESCUENTO') begin
			Set @vAplicar = IsNUll((Select Aplicar from PedidoNotaCredito where IDTransaccion = @IDTransaccion),0)
			Set @vSaldoVentaReferencia = Isnull((Select Sum(V.Saldo) from PedidoNotaCreditoVenta PV join Venta V on PV.IDTransaccionVentaGenerada = v.IDTransaccion where pv.IDTransaccionPedidoNotaCredito =@IDTransaccion),0)
			Set @vImporteVentaReferencia = Isnull((Select Sum(V.Total) from pedidonotacreditoventa PV join Venta V on PV.IDTransaccionVentaGenerada = v.IDTransaccion where pv.idtransaccionpedidonotacredito =@IDTransaccion),0)
			
				
			if @vImporteVentaReferencia = 0 begin
				Set @vImporteVentaReferencia = Isnull((Select Sum(V.Total) from PedidoNotaCreditoPedidoVenta PV join Pedido V on PV.IDTransaccionPedidoVenta = v.IDTransaccion and V.Facturado=0 where pv.idtransaccionpedidonotacredito =@IDTransaccion),0)
				Set @vSaldoVentaReferencia = @vImporteVentaReferencia
			end
			--if @vAplicar = 1 begin
			--	Set @vImporteVentaReferencia = Isnull((Select Sum(V.Total) from pedidonotacreditoventa PV join Venta V on PV.IDTransaccionVentaGenerada = v.IDTransaccion where pv.idtransaccionpedidonotacredito =@IDTransaccion),0)
			--end
			--else begin
			--	Set @vImporteVentaReferencia = Isnull((Select Sum(V.Saldo) from PedidoNotaCreditoVenta PV join Venta V on PV.IDTransaccionVentaGenerada = v.IDTransaccion where pv.IDTransaccionPedidoNotaCredito =@IDTransaccion),0)
			--end
			
			Set @vImportePedido = IsNULl((Select Top(1) Total from PedidoNotaCredito where IDTransaccion = @IDTransaccion),0)
			Set @vImporteLimiteUsuario = IsNULl((Select Top(1) MontoLimite from UsuarioAutorizarPedidoNotaCredito where IDUsuario = @IDUsuario and IDSubMotivo = @vIDSubMotivoNotaCredito),0)	
			Set @vPorcentajeLimiteUsuario = IsNULl((Select Top(1) PorcentajeLimite from UsuarioAutorizarPedidoNotaCredito where IDUsuario = @IDUsuario and IDSubMotivo = @vIDSubMotivoNotaCredito),0)	
			Set @vMontoPorcentaje = IsNull(((@vImporteVentaReferencia * @vPorcentajeLimiteUsuario)/100),0)

			If @vMontoPorcentaje <= @vImporteLimiteUsuario begin
				Set @vMontoLimite = @vMontoPorcentaje
			end
			else if @vImporteLimiteUsuario < @vMontoPorcentaje begin
				Set @vMontoLimite = @vImporteLimiteUsuario
			end
			else begin
				Set @vMontoLimite = @vImportePedido
			end
		
			if @vAplicar = 0 and @vSaldoVentaReferencia < @vImportePedido begin
				Set @Mensaje = 'El Monto del pedido supera el saldo de la factura, no se puede Aprobar'
				Set @Procesado = 'False'
				Goto Salir
			end
			
			if @vImportePedido > @vMontoLimite begin
					print Concat('@vImportePedido ', @vImportePedido)
					print Concat('@vMontoLimite ', @vMontoLimite)
					

					Set @Mensaje = 'El Monto del pedido supera el limite del usuario, no se puede Aprobar'
					Set @Procesado = 'False'
					Goto Salir
			end
		end

		if @IDTransaccion <> 0 begin	
			Set @Mensaje = 'Registro procesado'
			Set @Procesado = 'True'
			Goto Salir
		end
	end 

	if @Operacion = 'RECHAZAR' begin
		if @IDTransaccion <> 0 begin	
			
			Set @Mensaje = 'Registro procesado'
			Set @Procesado = 'True'
			Goto Salir
		end
	end 

	if @Operacion = 'RECUPERAR' begin
		if @IDTransaccion <> 0 begin	
			----Actualizar
			Set @Mensaje = 'Registro procesado'
			Set @Procesado = 'True'
			Goto Salir
		end
	end 

	if @Operacion = 'ANULAR' begin
		if @IDTransaccion <> 0 begin	
			----Actualizar

			if exists(Select * from PedidoNCNotaCredito where IDTransaccionPedido = @IDTransaccion) begin
				Set @Mensaje = 'El pedido ya fue procesado, no se puede anular'
				Set @Procesado = 'False'
				Goto Salir
			end


			Set @Mensaje = 'Registro procesado'
			Set @Procesado = 'True'
			Goto Salir
		end
	end 

Salir:
	Select 'Mensaje'=@Mensaje, 'Procesado'=@Procesado
  
	
End



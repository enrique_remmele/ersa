﻿CREATE Procedure [dbo].[SpActualizarTimbrado]
	
	--Entrada
	@Operacion varchar(10),
	@IDProveedor numeric(18,0),
	@NroTimbrado varchar (50) ,
	@NumeroTimbradoRemplazar varchar (50),
	@VtoTimbrado date,
	 
	
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	

As

Begin
	
	Declare @vIDProveedor numeric (18,0)
	Declare @vNroTimbrado varchar (50)
	
	
	If @Operacion = 'UPD' Begin
	
		If Exists(Select * From Compra Where IDProveedor=@IDProveedor And NroTimbrado=@NroTimbrado) Begin
			Declare db_cursor cursor for
			Select IDProveedor, NroTimbrado  From compra
			Where IDProveedor=@IDProveedor And NroTimbrado=@NroTimbrado
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDProveedor, @vNroTimbrado
			While @@FETCH_STATUS = 0 Begin 
			
			--Actualizar Compra
			Update Compra set NroTimbrado = @NumeroTimbradoRemplazar, 
							  FechaVencimientoTimbrado= @VtoTimbrado
			
			Where IDProveedor= @vIDProveedor And NroTimbrado= @vNroTimbrado  
			
			Fetch Next From db_cursor Into @vIDProveedor, @vNroTimbrado
											
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor
		End
		
		If Exists(Select * From Gasto Where IDProveedor=@IDProveedor And NroTimbrado=@NroTimbrado) Begin
			Declare db_cursor cursor for
			Select IDProveedor, NroTimbrado  From Gasto
			Where IDProveedor=@IDProveedor And NroTimbrado=@NroTimbrado
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDProveedor, @vNroTimbrado
			While @@FETCH_STATUS = 0 Begin 
			
			--Actualizar Compra
			Update Gasto set NroTimbrado = @NumeroTimbradoRemplazar, 
							  FechaVencimientoTimbrado= @VtoTimbrado
			
			Where IDProveedor= @vIDProveedor And NroTimbrado= @vNroTimbrado  
			
			Fetch Next From db_cursor Into @vIDProveedor, @vNroTimbrado
											
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor
		End
		
		Set @Mensaje = 'Registro procesado!'
		Set @Procesado = 'True'	
		Return @@rowcount
	
	End
	
End


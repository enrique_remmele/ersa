﻿CREATE Procedure [dbo].[SpProveedor]

	--Entrada
	@ID int,
	
	--Datos Obligatorios
	@RazonSocial varchar(50),
	@RUC varchar(12),
	@Referencia varchar(50),
	@Operacion varchar(10),
		
	--Datos Comerciales
	@NombreFantasia varchar(50) = NULL,
	@Direccion varchar(100) = NULL,
	@Telefono varchar(20) = NULL,
	@Estado bit = 'True',
	@TipoCompra char(1)='I',
	
	--Localizacion
	@IDPais tinyint = NULL,
	@IDDepartamento tinyint = NULL,
	@IDCiudad smallint = NULL,
	@IDBarrio smallint = NULL,
		
	--Configuraciones
	@IDTipoProveedor tinyint = NULL,
	@IDMoneda tinyint = NULL,
	@CuentaContableCompra varchar(50) = NULL,
	@CuentaContableVenta varchar(50) = NULL,	
	@CuentaContableAnticipo varchar(50) = NULL,		            		 
	
	--De Referencias
	@IDSucursal tinyint = NULL,
	
	--Adicionales
	@PaginaWeb varchar(50) = NULL,
	@Email varchar(50) = NULL,
	@Fax varchar(20) = NULL,
	
	--Crédito y Retentor
	@Credito bit,
	@PlazoCredito int,
	@Retentor bit,
	@SujetoRetencion bit,
	@Exportador bit,

	@Acopiador bit = 'False',
	
	--Localizacion
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As
	
Begin

	--BLOQUES
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	
	--INSERTAR
	If @Operacion='INS' Begin
	
		--Validar
		----Si es que la RazonSocial ya existe
		--If Exists(Select * From Proveedor Where RazonSocial=@RazonSocial) Begin
		--	Set @Mensaje = 'La Razon Social ya existe!'
		--	Set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		--Si es que el RUC ya existe
		If Exists(Select * From Proveedor Where RUC=@RUC And RUC!='' And RUC!='55555501-1' And RUC!='99999901-0') Begin
			Set @Mensaje = 'El RUC ya existe....!'
			Set @Procesado = 'False'
			return @@rowcount
		End

		--Obligar carga de Cuenta COntable Compra
		If @CuentaContableCompra = '' Begin
			Set @Mensaje = 'Ingrese Cuenta Contable Compra!'
			Set @Procesado = 'False'
			return @@rowcount
		End

		
		--Si es que la Referencia ya existe
		If Exists(Select * From Proveedor Where Referencia=@Referencia) Begin
			Set @Mensaje = 'La referencia ya existe!'
			Set @Procesado = 'False'
			return @@rowcount
		End

		--Tipo de proveedor
		If @IDTipoProveedor = null Begin
			Set @Mensaje = 'Seleccione Tipo de Proveedor!'
			Set @Procesado = 'False'
			return @@rowcount
		End

		if @CuentaContableAnticipo is null begin
			Set @CuentaContableAnticipo = '11070111'
		end
				
		--Solo Crear el Registro con los datos mas importantes,
		--Luego pasamos a actualizar!
		Declare @vID int
		Set @vID = (Select ISNULL(Max(ID)+1, 1) From Proveedor)
		
		Insert Into Proveedor(ID, RazonSocial, RUC, Referencia, FechaAlta, IDUsuarioAlta, Estado,Credito,PlazoCredito,Retentor, TipoCompra, Exportador, SujetoRetencion,Acopiador, CuentaContableAnticipo)
		values(@vID, @RazonSocial, @RUC, @Referencia, GETDATE(), @IDUsuario, @Estado,@Credito,@PlazoCredito,@Retentor, @TipoCompra, @Exportador, @SujetoRetencion,@Acopiador, @CuentaContableAnticipo)
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PROVEEDOR', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		--Actualizamos el registro
		Set @Operacion='UPD'
		Set @ID = @vID		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		
	End
	
	--ACTUALIZAR
	If @Operacion='UPD' Begin
	
		Declare @vContado bit
	
		--Validar
		--Si es que no existe el Proveedor
		If Not Exists(Select * From Proveedor Where ID=@ID) Begin
			Set @Mensaje = 'El sistema no encuentra el registro!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si es que la RazonSocial ya existe
		--If Exists(Select * From Proveedor Where RazonSocial=@RazonSocial And ID!=@ID) Begin
		--	Set @Mensaje = 'La Razon Social ya existe!'
		--	Set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		--Si es que el RUC ya existe
		If Exists(Select * From Proveedor Where RUC=@RUC And ID!=@ID And RUC!='' And RUC!='55555501-1' And RUC!='99999901-0') Begin
			Set @Mensaje = 'El RUC ya existe!'
			Set @Procesado = 'False'
			return @@rowcount
		End

		--Obligar carga de Cuenta COntable Compra
		If @CuentaContableCompra = '' Begin
			Set @Mensaje = 'Ingrese Cuenta Contable Compra!'
			Set @Procesado = 'False'
			return @@rowcount
		End

		--Si es que la Referencia ya existe
		If @IDTipoProveedor = null Begin
			Set @Mensaje = 'Seleccione Tipo de Proveedor!'
			Set @Procesado = 'False'
			return @@rowcount
		End
	
		--Actualizamos
		--Datos Comerciales
		Update Proveedor Set RazonSocial=@RazonSocial, 
							RUC=@RUC,
							Referencia=@Referencia, 
							NombreFantasia=@NombreFantasia, 	
							Direccion=@Direccion,
							Telefono= @Telefono,
							TipoCompra= @TipoCompra ,
							Acopiador = @Acopiador
		Where ID=@ID
		
		--Localizaciones
		Update Proveedor Set IDPais=@IDPais, 
						   IDDepartamento=@IDDepartamento,
						   IDCiudad=@IDCiudad,
						   IDBarrio=@IDBarrio
		Where ID=@ID
		
		--Referencia
		Update Proveedor Set IDSucursal=@IDSucursal						   
		Where ID=@ID
		
		--Configuraciones
		Update Proveedor Set	IDTipoProveedor=@IDTipoProveedor,
								IDMoneda=@IDMoneda,
								CuentaContableCompra=@CuentaContableCompra,
								CuentaContableVenta=@CuentaContableVenta
		Where ID=@ID
		
		--Estadisticos
		Update Proveedor Set	FechaModificacion=GETDATE(),
							IDUsuarioModificacion=@IDUsuario
		Where ID=@ID
									
		--Adicionales
		Update Proveedor Set	PaginaWeb=@PaginaWeb,
							Email=@Email,
							Fax=@Fax
		Where ID=@ID
		
		--Crédito y Retentor
		
		If @Credito= 'True'Begin
			Set @vContado= 'False'
		end else Begin
			Set @vContado= 'True'
		End
		
		
		Update Proveedor Set Credito=@Credito,
							Contado= @vContado, 
							PlazoCredito=@PlazoCredito,
							Retentor=@Retentor,
							SujetoRetencion= @SujetoRetencion,
							Exportador = @Exportador  
							
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PROVEEDOR', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
					
	End
	
	--ELIMNAR
	If @Operacion='DEL' Begin
	
		--Si es que no existe el Proveedor
		If Not Exists(Select * From Proveedor Where ID=@ID) Begin
			Set @Mensaje = 'El sistema no encuentra el registro!'
			Set @Procesado = 'False'
			return @@rowcount
		End	
		
		--Si es que tiene ventas
		If Exists(Select * From Compra Where IDProveedor=@ID) Begin
			Set @Mensaje = 'El Proveedor tiene compras realizadas! No se puede eliminar'
			Set @Procesado = 'False'
			return @@rowcount			
		End	
		
		--Eliminar Proveedor Division
		Delete From Division Where IDProveedor=@ID
		
		--Eliminar Proveedor Contacto
		Delete From ProveedorContacto Where IDProveedor=@ID
		
		--Eliminar Proveedor
		Delete From Proveedor Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PROVEEDOR', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		
	End
	
	return @@rowcount
	
End


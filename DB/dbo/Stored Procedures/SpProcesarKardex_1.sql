﻿CREATE Procedure [dbo].[SpProcesarKardex]
	
	----Entrada
	@IDProducto int,
	@FechaInicio date = null
As

Begin

	Set NOCOUNT ON

	Declare @vIDKardex numeric(18,0)
	Declare @vOrden tinyint
	Declare @vAño smallint
	Declare @vMes tinyint
	Declare @vFecha datetime
	Declare @vIDTransaccion numeric(18,0)
	Declare @vIDProducto int
	Declare @vID tinyint
	Declare @vCantidadEntrada decimal(10,2)
	Declare @vCantidadSalida decimal(10,2)
	Declare @vCantidadSaldo decimal(10,2)
	Declare @vCostoEntrada money
	Declare @vCostoSalida money
	Declare @vCostoPromedio money
	Declare @vTotalEntrada money
	Declare @vTotalSalida money
	Declare @vTotalSaldo money
	Declare @vIDSucursal int
	Declare @vComprobante varchar(50)
	Declare @vIDKardexAnterior int
	Declare @vCantidadSaldoAnterior decimal(10,2)
	Declare @vCostoPromedioAnterior money
	Declare @vTotalSaldoAnterior money
	Declare @vCostoPromedioOperacion money
	Declare @vIDProductoAnterior int = 0
	Declare @vIndice integer = 1 
	Declare @vFactorImpuesto decimal(18,3)
	Declare @vFactorDiscriminado decimal(18,3)
	Declare @vIDTipoComprobante int

	Declare @vFechaInicio date
	Declare @vOperacion varchar(30)

	--Set @vAño = @Año
	--Set @vMes = @Mes

	Begin
		print concat('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$', @IDProducto, '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
		if @FechaInicio is null begin 
		Set @vFechaInicio = (Select Max(Fecha) From CierreAperturaStock Where Anulado='False')
		end 
		else begin
			Set @vFechaInicio = @FechaInicio
		end

		If @vFechaInicio Is Null Begin
			Set @vFechaInicio = '20190101'
		End

		Declare db_cursorProcesarKardex cursor for
		Select	IDKardex, IDTransaccion, IDProducto, ID, CantidadEntrada, CantidadSalida, CostoUnitarioEntrada, CostoUnitarioSalida, CostoPromedio, Operacion
		from VKardex
		Where IDProducto = @IDProducto and Fecha>=@vFechaInicio
		Order by IDKardex
		Open db_cursorProcesarKardex   
		
		Fetch Next From db_cursorProcesarKardex Into @vIDKardex, @vIDTransaccion, @vIDProducto, @vID, @vCantidadEntrada, @vCantidadSalida, @vCostoEntrada, @vCostoSalida, @vCostoPromedio, @vOperacion
		While @@FETCH_STATUS = 0 Begin 
				
			If @vCantidadSalida<0 Begin
				Set @vCantidadSalida = @vCantidadSalida*-1
			End

			If @vCantidadEntrada<0 Begin
				Set @vCantidadEntrada = @vCantidadEntrada*-1
			End

			If @vCantidadEntrada = @vCantidadSalida begin
				Set @vCantidadSalida = 0
			end

			--Actualizar Costo de la venta
			If @vOperacion = 'VENTAS CLIENTES' begin
								
				Update DetalleVenta
				Set CostoUnitario = @vCostoPromedio,
					TotalCosto = (@vCantidadEntrada + @vCantidadSalida) * @vCostoPromedio
				Where IDTransaccion = @vIDTransaccion And IDProducto=@vIDProducto 
				--And ID=@VID
				
				
				----Generar Asiento de Venta
				Exec SpAsientoVenta @IDTransaccion=@vIDTransaccion
				
				--Actualizar costo operacion
				Update Kardex
				Set CostoPromedioOperacion = @vCostoSalida
				Where IDKardex=@vIDKardex

				GoTo Seguir

			End
						
			--Actualizar Costo de la Nota de Credito
			If @vOperacion = 'NOTA CREDITO' begin
			
				Update DetalleNotaCredito
				Set CostoUnitario = @vCostoPromedio,
					TotalCosto = IsNull((@vCantidadEntrada + @vCantidadSalida) * @vCostoPromedio,0)
				Where IDTransaccion = @vIDTransaccion  And IDProducto=@vIDProducto 
				--And ID=@VID
			
				----Generar Asiento
				Exec SpAsientoNotaCredito @IDTransaccion=@vIDTransaccion

				--Actualizar costo operacion
				Update Kardex
				Set CostoPromedioOperacion = @vCostoPromedio
				Where IDKardex=@vIDKardex

			End

			--Actualizar Costo de Movimiento
			If @vOperacion in('MOVIMIENTOS','DESCARGASTOCK', 'MOVIMIENTO DESCARGA DE COMPRA', 'MOVIMIENTO MATERIA PRIMA', 'DESCARGA POR CONSUMO', 'CONSUMO COMBUSTIBLE') begin
				print '$$$ ENTRO a ACTUALIZAR COSTO DE MOVIMIENTO $$'
				print @vCostoPromedio
				Set @vIDTipoComprobante = (select IDTipoComprobante from Movimiento where idtransaccion = @vIDTransaccion)
				
				if @vIDTipoComprobante = (Select top(1) IDTipoComprobanteProduccion from Configuraciones) begin
					Set @vCostoPromedio = @vCostoEntrada
				end

				if @vIDTipoComprobante = (Select top(1) IDTipoComprobanteImportacion from Configuraciones) begin
					Set @vCostoPromedio = @vCostoEntrada
				end

				select @vFactorImpuesto = FactorImpuesto, 
					   @vFactorDiscriminado= FactorDiscriminado 
				from vproducto where id = @IDProducto


				Update DetalleMovimiento
				Set PrecioUnitario = @vCostoPromedio,
					Total = IsNull((@vCantidadEntrada + @vCantidadSalida) * @vCostoPromedio,0),
					TotalDiscriminado = IsNull((@vCantidadEntrada + @vCantidadSalida) * @vCostoPromedio,0)/@vFactorDiscriminado,
					TotalImpuesto = IsNull((@vCantidadEntrada + @vCantidadSalida) * @vCostoPromedio,0)/@vFactorImpuesto
				Where IDTransaccion = @vIDTransaccion  And IDProducto=@vIDProducto And ID=@VID

				Update Movimiento
				Set Total = (Select sum(Total) from detalleMovimiento where IDTransaccion = @vIDTransaccion),
				TotalDiscriminado = (Select sum(TotalDiscriminado) from detalleMovimiento where IDTransaccion = @vIDTransaccion),
				TotalImpuesto = (Select sum(TotalImpuesto) from detalleMovimiento where IDTransaccion = @vIDTransaccion)
				Where IDTransaccion = @vIDTransaccion
				
				----Generar Asiento
				if @vOperacion = 'MOVIMIENTO MATERIA PRIMA' begin
					Exec SpAsientoMovimientoMateriaPrimaBalanceado @IDTransaccion = @vIDTransaccion
				end else begin
					Exec SpAsientoMovimiento @IDTransaccion=@vIDTransaccion
				end
				
				--Actualizar costo operacion
				Update Kardex
				Set CostoPromedioOperacion = @vCostoPromedio
				Where IDKardex=@vIDKardex

				Update Producto Set CostoCG=@vCostoPromedio,
									CostoPromedio=@vCostoPromedio
				Where ID=@IDProducto
			End

Seguir:
			
			Fetch Next From db_cursorProcesarKardex Into @vIDKardex, @vIDTransaccion, @vIDProducto, @vID, @vCantidadEntrada, @vCantidadSalida, @vCostoEntrada, @vCostoSalida ,@vCostoPromedio, @vOperacion
		
		End
		
		--Cierra el cursorv xj 
		Close db_cursorProcesarKardex   
		Deallocate db_cursorProcesarKardex
		
	End

	Begin
			
			--Actualizar Costo del producto
			if @vCostoPromedio > 1 begin
				update RecalculoKardexProducto set procesado = 'True',asiento = 'True' where idproducto = @vIDProducto
				
				Update Producto Set CostoCG=@vCostoPromedio,
									CostoPromedio=@vCostoPromedio
				Where ID=@IDProducto
			end
		

		----Actualiza la ficha de Producto
		--Declare db_cursorProcesarKardexProducto cursor for
		--Select ID from VProducto
		--Open db_cursorProcesarKardexProducto
		
		--Fetch Next From db_cursorProcesarKardexProducto Into @vIDProducto
		--While @@FETCH_STATUS = 0 Begin 

			--Set @vCostoPromedio = (Select CostoPromedio From Kardex Where IDKardex = (Select Max(IDKardex) From Kardex Where IDProducto=@vIDProducto))
			
			----Actualizar Costo del producto
			--Update Producto Set CostoCG=@vCostoPromedio,
			--					CostoPromedio=@vCostoPromedio
			--Where ID=@vIDProducto
					
		--	Fetch Next From db_cursorProcesarKardexProducto Into @vIDProducto
		
		--End
		
		----Cierra el cursor
		--Close db_cursorProcesarKardexProducto
		--Deallocate db_cursorProcesarKardexProducto

	End	

End

﻿
CREATE Procedure [dbo].[SpImportarProductoListaPrecio]

	--Entrada
	@Referencia varchar(50),
	@ListaPrecio varchar(100),
	@IDMoneda tinyint = 1,

	--
	@Precio varchar(50),
	@TPRPorcentaje varchar(50) = '0',
	@TPRFechaDesde date = '', 
	@TPRFechaHasta date = '',	
	
	@Actualizar bit = 'True' 
	
As

Begin

	Declare @vOperacion varchar(50)
	Declare @vMensaje varchar(50) = 'No se proceso'
	Declare @vProcesado bit = 'False'
	Declare @vIDListaPrecio int
	Declare @vIDProducto int
	Declare @vIDSucursal tinyint = 1
	Declare @Sucursal varchar(50) = ''
	

	Set @vOperacion = 'UPD'
	
	Set @Precio = '0' + @Precio
	Set @Precio = REPLACE(@Precio, '.','')
	Set @Precio = REPLACE(@Precio, ',','.')
	
	Set @TPRPorcentaje = '0' + @TPRPorcentaje
	Set @TPRPorcentaje = REPLACE(@TPRPorcentaje, ',','.')
	
	
	--Obtener el producto
	Begin
		Set @vIDProducto = (Select IsNull((Select Top(1) ID From Producto Where Referencia=@Referencia), 0))
	End
	
	--Si no existe el producto, salir
	If @vIDProducto = 0 Begin
		Set @vMensaje = 'No se encontro el producto'
		Set @vProcesado = 'False'
		Goto Salir
	End
	
	Set @vIDListaPrecio = IsNull((Select Top(1) ID From ListaPrecio Where Descripcion = @ListaPrecio), 0) 
	If @vIDListaPrecio = 0 Begin
		Set @vMensaje = 'No se encontro la lista de precio' + @ListaPrecio
		Set @vProcesado = 'False'
		Goto Salir
	End

	 

	Declare cSucursales cursor for
	Select V.ID, V.Descripcion
	From Sucursal V
	where V.ID = 1
	Open cSucursales   
	fetch next from cSucursales into @vIDSucursal, @Sucursal

	While @@FETCH_STATUS = 0 Begin  	
		--Si no existe la lista de precio, salir
		Set @vIDListaPrecio = IsNull((Select Top(1) ID From ListaPrecio Where Descripcion = @ListaPrecio  And IDSucursal=@vIDSucursal), 0) 

		if @vIDListaPrecio > 0 begin
		 		
			EXEC	SpProductoListaPrecio
			@IDProducto = @vIDProducto,
			@IDListaPrecio = @vIDListaPrecio,
			@IDMoneda = 1,
			@Precio = @Precio,
			@TPRPorcentaje = 0,
			@TPRDesde = @TPRFechaDesde,
			@TPRHasta = @TPRFechaHasta,
			@Operacion = 'UPD',
			@IDUsuario = 1,
			@IDSucursal = @vIDSucursal,
			@IDDeposito = 1,
			@IDTerminal = 1,
			@Mensaje = @vMensaje OUTPUT,
			@Procesado = @vProcesado OUTPUT
			
			set @vProcesado = 'true'
		end
seguir:
		fetch next from cSucursales into @vIDSucursal, @Sucursal
		
	End   
	
	close cSucursales   
	deallocate cSucursales
	
Salir:

	Select 'Mensaje'=concat(@vMensaje,'|idlistaprecio:',@vIDListaPrecio,'|idproducto:',@vIDProducto,' |idmoneda:',@IDMoneda,'|Precio:',@Precio, '|idsucursal', @vIDSucursal), 'Procesado'=@vProcesado	
	
End


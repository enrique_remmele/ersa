﻿CREATE Procedure [dbo].[SpGenerarVencimientoChequeEmitido]

As

Begin
	
	Declare @vIDTransaccion int
	Declare @vFechaVencimiento Date
	Declare cursorGenerarVencimiento cursor for
	Select IDTransaccion, FechaVencimiento from VOrdenPago
	Where PagoProveedor = 1 and Anulado = 0 and Diferido = 1 
	and FechaVencimiento <= GETDATE() and year(fecha)>2017 
	and IDTransaccion not in (select IDTransaccionOP from VencimientoChequeEmitido)
	Open cursorGenerarVencimiento   
	fetch next from cursorGenerarVencimiento into @vIDTransaccion, @vFechaVencimiento
		
	While @@FETCH_STATUS = 0 Begin  

		if year(@vFechaVencimiento)<2019 begin
			Set @vFechaVencimiento = '20200101'
		end

		Exec SpVencimientoChequeDiferidoOPAutomatico 
					@IDTransaccionOP=@vIDTransaccion, 
					@Fecha=@vFechaVencimiento,
					@Operacion='INS'

			
		fetch next from cursorGenerarVencimiento into @vIDTransaccion, @vFechaVencimiento
			
	End
		
	close cursorGenerarVencimiento 
	deallocate cursorGenerarVencimiento
	
	
End

﻿CREATE Procedure [dbo].[SpPrepararPago]

	--Entrada
	@IDTransaccion numeric(18,0),
	@Pagar bit = 'False',
	@ImporteAPagar money = 0,
	@CuentaBancaria varchar(50) = '',
	@Observacion varchar(200) = '',
	@Cuota smallint = 1,
	@Cotizacion money = 1

As

Begin

	--Variables
	Declare @vMensaje varchar(100) = 'No se proceso'
	Declare @vProcesado bit = 'False'
	Declare @vIDCuentaBancaria tinyint = 0
	
	--Obtener el IDCuentaBancaria
	Set @vIDCuentaBancaria = (Select Top(1) ID From VCuentaBancaria Where Descripcion=@CuentaBancaria)
	If @Pagar = 'False' Begin
		Set @ImporteAPagar = 0
		Set @vIDCuentaBancaria = NULL
		Set @Observacion = ''
	End

	--Gasto
	If Exists(Select Top(1) IDTransaccion From Gasto Where IDTransaccion=@IDTransaccion) Begin
		
		Update Gasto Set Pagar=@Pagar,
							ImporteAPagar=@ImporteAPagar,
							IDCuentaBancaria=@vIDCuentaBancaria,
							ObservacionPago=@Observacion,
							Cotizacion=@Cotizacion
		Where IDTransaccion=@IDTransaccion				 
		
		--Si es por cuota, configuramos tambien la tabla de cuota
		If Exists(Select Top(1) IDTransaccion From Cuota Where IDTransaccion=@IDTransaccion) Begin
			
			----La cuota sera la primera no pagada
			--If @Pagar = 'True' Begin
			--	Set @vCuota = (Select Top(1) ID From Cuota Where IDTransaccion=@IDTransaccion And Cancelado='False' Order By ID)
			--End

			--If @Pagar = 'False' Begin
			--	Set @vCuota = (Select Top(1) ID From Cuota Where IDTransaccion=@IDTransaccion And Cancelado='True' Order By ID Desc)
			--End


			Update Cuota Set Pagar=@Pagar,
							ImporteAPagar=@ImporteAPagar,
							IDCuentaBancaria=@vIDCuentaBancaria,
							ObservacionPago=@Observacion
			Where IDTransaccion=@IDTransaccion And ID = @Cuota
							
		End

		Set @vMensaje = 'Rgistro guardado!'
		Set @vProcesado = 'True'
		GoTo Salir	
				
	End

	--Compra
	If Exists(Select Top(1) IDTransaccion From Compra Where IDTransaccion=@IDTransaccion) Begin
		Update Compra Set Pagar=@Pagar,
							ImporteAPagar=@ImporteAPagar,
							IDCuentaBancaria=@vIDCuentaBancaria,
							ObservacionPago=@Observacion,
							Cotizacion=@Cotizacion
		Where IDTransaccion=@IDTransaccion				 
		
		Set @vMensaje = 'Rgistro guardado!'
		Set @vProcesado = 'True'
		GoTo Salir			
	End

	--Vale
	If Exists(Select Top(1) IDTransaccion From Vale Where IDTransaccion=@IDTransaccion) Begin
		Update Vale Set Pagar=@Pagar,
							ImporteAPagar=@ImporteAPagar,
							IDCuentaBancaria=@vIDCuentaBancaria,
							ObservacionPago=@Observacion
		Where IDTransaccion=@IDTransaccion				 
		
		Set @vMensaje = 'Rgistro guardado!'
		Set @vProcesado = 'True'
		GoTo Salir			
	End

Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End

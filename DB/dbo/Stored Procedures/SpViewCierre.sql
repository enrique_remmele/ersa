﻿CREATE Procedure [dbo].[SpViewCierre]

	--Entrada
	@Año smallint,
	@Mes tinyint,	
	@IDSucursal int = 0,	
	@Factor int
	
As

Begin

	--variables
	Declare @vID tinyint
	Declare @vM varchar(50)
	Declare @vCodigo varchar(50) 
	Declare @vIDcc integer
	Declare @vDenominacion varchar(200)
	Declare @vCategoria tinyint
	Declare @vImputable bit
	Declare @vDebito money
	Declare @vCredito money
	Declare @vSaldo money
	Declare @vDebitoAnterior money
	Declare @vCreditoAnterior money
	Declare @vSaldoAnterior money
	Declare @vDebitoActual money
	Declare @vMovimientoMes money
	Declare @vCreditoActual money
	Declare @vSaldoActual money
	
	Declare @vSaldoCredito money
	Declare @vSaldoDebito money
	
	--Crear la tabla
	Create table #BalanceGeneral(ID tinyint,
								Año smallint,
								Mes tinyint,
								IDSucursal int,
								M varchar(50),
								Codigo varchar(50),
								IDcc integer,
								Denominacion varchar(200),
								Categoria tinyint,
								Imputable bit,
								MostrarCodigo varchar(50),
								
								SaldoAnterior money,
								MovimientoMes money,
								SaldoActual money,
								SaldoCredito money,
								SaldoDebito money
								)
								
	set @vID = (Select IsNull(MAX(ID)+1,1) From #BalanceGeneral)
	
	If @Factor = 0 Begin
		Set @Factor = 1
	End
	
	Declare db_cursor cursor for
	Select	CC.Codigo, CC.ID, CC.Descripcion, CC.Categoria, CC.Imputable	From VCuentaContable CC	Where (SUBSTRING(CC.Codigo, 0, 2) Between '1' And '9') and CC.Imputable = 'true'
	Open db_cursor   
	Fetch Next From db_cursor Into	@vCodigo, @vIDcc, @vDenominacion, @vCategoria, @vImputable
	While @@FETCH_STATUS = 0 Begin 
		
		--Tipo de Cuenta
		Declare @vCuentaTipo varchar(50)
		Declare @vPrefijo varchar(50)

		Set @vPrefijo = SubString(@vCodigo, 0, 2)

		--Cuentas del Debe
		If @vPrefijo = '1' Or @vPrefijo = '5' Begin
			Set @vCuentaTipo = 'DEBE'
		End

		--Cuentas del Haber
		If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
			Set @vCuentaTipo = 'HABER'
		End
					
		Set @vDebitoAnterior = 0
		Set @vCreditoAnterior = 0
		
		--Saldo Anterior
		If @Mes > 1 Begin
			--Toda la Empresa
			If @IDSucursal = 0 Begin
				Set @vDebitoAnterior = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes<@Mes And Cuenta=@vCodigo),0)
				Set @vCreditoAnterior = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes<@Mes And Cuenta=@vCodigo),0)
			End
			
			--Por Sucursal
			If @IDSucursal > 0 Begin
				Set @vDebitoAnterior = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes<@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
				Set @vCreditoAnterior = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes<@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
			End
		End
		
		--Saldo Actual
		--Toda la Empresa
		If @IDSucursal = 0 Begin
			Set @vDebito = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes=@Mes And Cuenta=@vCodigo),0)
			Set @vCredito = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes=@Mes And Cuenta=@vCodigo),0)
		End
		
		--Por Sucursal
		If @IDSucursal > 0 Begin
			Set @vDebito = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes=@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
			Set @vCredito = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes=@Mes And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
		End
		
		If @vCuentaTipo = 'DEBE' Begin
			Set @vSaldoAnterior = IsNUll(@vDebitoAnterior  - @vCreditoAnterior,0)
			Set @vSaldoActual = @vSaldoAnterior + (@vDebito - @vCredito)
			set @vSaldoCredito = @vSaldoCredito + (@vDebito - @vCredito)
			set @vSaldoDebito = @vSaldoDebito + (@vDebito - @vCredito)
			Set @vMovimientoMes = @vDebito - @vCredito
		End
		
		If @vCuentaTipo = 'HABER' Begin
			Set @vSaldoAnterior = IsNUll(@vCreditoAnterior-@vDebitoAnterior,0)
			Set @vSaldoActual = @vSaldoAnterior + (@vCredito - @vDebito)
			Set @vSaldoDebito = @vSaldoDebito + (@vCredito - @vDebito)
			Set @vSaldoCredito = @vSaldoCredito + (@vCredito - @vDebito)
			Set @vMovimientoMes = @vCredito - @vDebito 
		End
		
		Set @vSaldoAnterior = @vSaldoAnterior / @Factor
		Set @vSaldoActual = @vSaldoActual / @Factor
		Set @vSaldoDebito = @vSaldoDebito / @Factor
		Set @vSaldoCredito = @vSaldoCredito / @Factor
		
		Set @vDebito = @vDebito / @Factor
		Set @vCredito = @vCredito / @Factor
		Set @vMovimientoMes = @vMovimientoMes / @Factor
			
		--Si ya existe, actualizar
		If @IDSucursal = 0 Begin
			If Exists(Select * From #BalanceGeneral Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo) Begin
			
				Update #BalanceGeneral Set 
				SaldoAnterior=SaldoAnterior+@vSaldoAnterior,
				MovimientoMes=MovimientoMes+@vMovimientoMes,
				SaldoActual=SaldoActual+@vSaldoActual
				Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo
				
				--Cuentas del Debe
				If @vPrefijo = '1' Or @vPrefijo = '5' Begin
					Update #BalanceGeneral Set 
					SaldoAnterior=SaldoAnterior+@vSaldoAnterior,
					MovimientoMes=MovimientoMes+@vMovimientoMes,
					SaldoActual=SaldoActual+@vSaldoActual,
					SaldoCredito = SaldoActual+@vSaldoActual,
					SaldoDebito = 0
					Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo
				End

				--Cuentas del Haber
				If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
					Update #BalanceGeneral Set 
					SaldoAnterior=SaldoAnterior+@vSaldoAnterior,
					MovimientoMes=MovimientoMes+@vMovimientoMes,
					SaldoActual=SaldoActual+@vSaldoActual,
					SaldoCredito = 0,
					SaldoDebito = SaldoActual+@vSaldoActual
					Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo
				End
				
			End Else Begin
			--Cuentas del Debe
			If @vPrefijo = '1' Or @vPrefijo = '5' Begin
				Insert Into #BalanceGeneral(ID, Año, Mes,  M, Codigo,IDcc, Denominacion, Categoria, Imputable, SaldoAnterior, MovimientoMes, SaldoActual, SaldoCredito, SaldoDebito, MostrarCodigo)
				Values(@vID, @Año, @Mes, dbo.FMes(@Mes), @vCodigo,@vIDcc ,@vDenominacion, @vCategoria, @vImputable, @vSaldoAnterior, @vMovimientoMes, @vSaldoActual,@vSaldoActual, 0, '1')
			End

			--Cuentas del Haber
			If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
				Insert Into #BalanceGeneral(ID, Año, Mes,  M, Codigo,IDcc, Denominacion, Categoria, Imputable, SaldoAnterior, MovimientoMes, SaldoActual, SaldoCredito, SaldoDebito, MostrarCodigo)
				Values(@vID, @Año, @Mes, dbo.FMes(@Mes), @vCodigo, @vIDcc ,@vDenominacion, @vCategoria, @vImputable, @vSaldoAnterior, @vMovimientoMes, @vSaldoActual,0, @vSaldoActual, '1')
			End
				--Insert Into #BalanceGeneral(ID, Año, Mes,  M, Codigo, Denominacion, Categoria, Imputable, SaldoAnterior, MovimientoMes, SaldoActual, SaldoCredito, SaldoDebito, MostrarCodigo)
				--Values(@vID, @Año, @Mes, dbo.FMes(@Mes), @vCodigo, @vDenominacion, @vCategoria, @vImputable, @vSaldoAnterior, @vMovimientoMes, @vSaldoActual,@vSaldoDebito, @vSaldoCredito, '1')
			End
		End
		
		--Por Sucursal
		If @IDSucursal > 0 Begin
			If Exists(Select * From #BalanceGeneral Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo And IDSucursal=@IDSucursal) Begin
			If @vPrefijo = '1' Or @vPrefijo = '5' Begin
				Update #BalanceGeneral Set 
				SaldoAnterior=SaldoAnterior+@vSaldoAnterior,
				MovimientoMes=MovimientoMes+@vMovimientoMes,
				SaldoActual=SaldoActual+@vSaldoActual,
				SaldoCredito=SaldoActual+@vSaldoActual,
				SaldoDebito=0
				Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo
			END 
			
			If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
				Update #BalanceGeneral Set 
					SaldoAnterior=SaldoAnterior+@vSaldoAnterior,
					MovimientoMes=MovimientoMes+@vMovimientoMes,
					SaldoCredito=0,
				    SaldoDebito=SaldoActual+@vSaldoActual,
					SaldoCredito=SaldoActual+@vSaldoActual
					Where ID=@vID And Año=@Año And Mes=@Mes And Codigo=@vCodigo
			END 
				
			End Else Begin
			    If @vPrefijo = '1' Or @vPrefijo = '5' Begin
				  Insert Into #BalanceGeneral(ID, Año, Mes, IDSucursal, M, Codigo, IDcc, Denominacion, Categoria, Imputable, SaldoAnterior, MovimientoMes, SaldoActual, SaldoDebito, SaldoCredito, MostrarCodigo)
				  Values(@vID, @Año, @Mes, @IDSucursal, dbo.FMes(@Mes), @vCodigo,@vIDcc, @vDenominacion, @vCategoria, @vImputable, @vSaldoAnterior, @vMovimientoMes, @vSaldoActual, @vSaldoActual, 0, '1')
				end 
				If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
				  Insert Into #BalanceGeneral(ID, Año, Mes, IDSucursal, M, Codigo,IDcc, Denominacion, Categoria, Imputable, SaldoAnterior, MovimientoMes, SaldoActual, SaldoDebito, SaldoCredito, MostrarCodigo)
				  Values(@vID, @Año, @Mes, @IDSucursal, dbo.FMes(@Mes), @vCodigo,@vIDcc, @vDenominacion, @vCategoria, @vImputable, @vSaldoAnterior, @vMovimientoMes, @vSaldoActual, 0, @vSaldoActual, '1')
				end 
				
			End
		End
		
Siguiente:
		
		Fetch Next From db_cursor Into	@vCodigo, @vIDcc, @vDenominacion, @vCategoria, @vImputable
		
	End

	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor		   		
	
	--RESULTADO DEL EJERCICIO
	Begin
		Declare @vIDSucursal int
		Declare @vCuentaContableSucursal varchar(50)
		
		Declare Sucursal_Cursor cursor for
		Select ID, CuentaContable From Sucursal
		Open Sucursal_Cursor   
		Fetch Next From Sucursal_Cursor Into @vIDSucursal, @vCuentaContableSucursal
		While @@FETCH_STATUS = 0 Begin
		
			Set @vDenominacion = (Select Descripcion From VCuentaContable Where Codigo=@vCuentaContableSucursal)
			Set @vCategoria = (Select Categoria From VCuentaContable Where Codigo=@vCuentaContableSucursal)
			Set @vImputable = (Select Imputable From VCuentaContable Where Codigo=@vCuentaContableSucursal)
			
			--Inicializar
			Set @vSaldoAnterior = 0
			
			--Obtener Saldos Anteriores
			If @Mes > 1 Begin
				Set @vSaldoAnterior = IsNull((Select Sum(Credito) - Sum(Debito) From PlanCuentaSaldo Where IDSucursal=@vIDSucursal And Año=@Año And Mes<@Mes And SubString(Cuenta, 0, 2)='4'),0) 
							         -IsNull((Select Sum(Debito) - Sum(Credito) From PlanCuentaSaldo Where IDSucursal=@vIDSucursal And Año=@Año And Mes<@Mes And SubString(Cuenta, 0, 2)='5'),0)
			End

			Set @vMovimientoMes = IsNull((Select Sum(Credito) - Sum(Debito) From PlanCuentaSaldo Where IDSucursal=@vIDSucursal And Año=@Año And Mes=@Mes And SubString(Cuenta, 0, 2)='4'),0) 
								 -IsNull((Select Sum(Debito) - Sum(Credito) From PlanCuentaSaldo Where IDSucursal=@vIDSucursal And Año=@Año And Mes=@Mes And SubString(Cuenta, 0, 2)='5'),0)
			
			Set @vSaldoActual = @vSaldoAnterior + @vMovimientoMes
			
			If @vPrefijo = '1' Or @vPrefijo = '5' Begin
			 set @vSaldoCredito = @vSaldoCredito+ @vSaldoActual
			end
			
			If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
			set @vSaldoDebito = @vSaldoDebito+ @vSaldoActual
			end 

			 							         			
			Insert Into #BalanceGeneral(ID, Año, Mes, M, Codigo,IDcc, Denominacion, Categoria, Imputable, SaldoAnterior, SaldoActual, SaldoCredito, SaldoDebito, MovimientoMes, MostrarCodigo)
			Values(@vID, @Año, @Mes, dbo.FMes(@Mes), @vCuentaContableSucursal, @vIDcc, @vDenominacion, @vCategoria, @vImputable, @vSaldoAnterior, @vSaldoActual,@vSaldoCredito, @vSaldoDebito, @vMovimientoMes, '1')
			
			Fetch Next From Sucursal_Cursor Into @vIDSucursal, @vCuentaContableSucursal
			
		End

		--Cierra el cursor
		Close Sucursal_Cursor   
		Deallocate Sucursal_Cursor	
	 
	End
	
	Select IDcc,Codigo,Denominacion,Isnull(SaldoCredito,0) as Credito, 
	Isnull(SaldoDebito,0) as Debito 
	From #BalanceGeneral 
	Where ID=@vID 
	and (Isnull(SaldoCredito,0)+Isnull(SaldoDebito,0)) <>0 
	order by codigo
End






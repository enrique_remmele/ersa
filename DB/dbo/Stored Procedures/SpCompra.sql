﻿CREATE Procedure [dbo].[SpCompra]
	
		
	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Numero int = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante varchar(50) = NULL,
	@NroTimbrado varchar (50)= NULL,
	@IDProveedor int = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDDepositoOperacion tinyint = NULL,
	@Fecha date = NULL,
	@Credito bit = NULL,
	@FechaVencimiento date = NULL,
	@FechaEntrada date = NULL,
	@VtoTimbrado date = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money = NULL,
	@Observacion varchar(200) = NULL,
	@Total money = NULL,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@Operacion varchar(50),
	@Directo bit = NULL,
	@IDDepartamentoEmpresa tinyint = null,
	
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

--Validar Feha Operacion
	IF @Operacion = 'INS' Begin -- or @Operacion = 'UPD' Excluimos UPD porque no se permite la actualizacion de fecha de emision pero si de vencimiento
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from Compra where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--Controlar numero de timbrado
		if @NroTimbrado is null begin
			set @Mensaje = 'Ingrese un numero de timbrado.'
			Set @Procesado = 'False'
			Set @IDTransaccionSalida = 0
		end

	--Variable
	Declare @RetencionIVA money
	Declare @PorcentajeRetencion tinyint

	--BLOQUES
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		--If Exists(Select top 1 * From Compra Where Numero=@Numero And IDSucursal=@IDSucursalOperacion) Begin
		--	set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
		--	set @Procesado = 'False'
		--	set @IDTransaccionSalida  = 0
		--	return @@rowcount
		--End
		Set @Numero =(select max(isnull(numero,0))+1 from compra where idsucursal= @IDSucursalOperacion)

		--Fecha de Emision > Fecha de vencimiento del timbrado
		if @Fecha > @VtoTimbrado Begin
			set @Mensaje = 'La Fecha de Emision no puede ser mayor a la Fecha de Vencimiento del Timbrado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Comprobante
		if Exists(Select top 1 * From Compra Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante And NroTimbrado=@NroTimbrado) Begin
			set @Mensaje = 'El sistema encontro el mismo tipo y numero de comprobante y el mismo timbrado! Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Comprobante de Gasto
		if Exists(Select top 1 * From Gasto Where NroComprobante=@NroComprobante And IDProveedor=@IDProveedor And NroTimbrado=@NroTimbrado) Begin
			set @Mensaje = 'Ya existe un Gasto con el mismo Comprobante y Timbrado! Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
	
		--La Fecha de Entrada no puede ser menor a la del comprobante
		If DATEDIFF(dd, @Fecha, @FechaEntrada) < 0 Begin
			set @Mensaje = 'La fecha de entrada no puede ser menor a la del documento!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--La Fecha de Entrada no puede ser mas de 3 dias del comprobante
		Declare @vMaximoDiaFechaEntrada tinyint
		Set @vMaximoDiaFechaEntrada = 3
		
		If DATEDIFF(dd, @FechaEntrada, @Fecha) > @vMaximoDiaFechaEntrada Begin
			set @Mensaje = 'La fecha de entrada no puede ser mas que ' + CONVERT(varchar(50), @vMaximoDiaFechaEntrada) + '(s) dias del comprobante!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Credito
		If @Credito='True' Begin
			
			--Fecha
			If DATEDIFF(dd, @Fecha, @FechaVencimiento) < 0 Begin
				set @Mensaje = 'La fecha de de vencimiento no puede ser menor al del comprobante!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
						
		End
		
		----Insertar la transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		------Insertar
		Declare @vSaldo money
		Declare @vCancelado bit
		
		If @Credito = 'True' Begin
			Set @vSaldo = @Total
			Set @vCancelado = 'False'
		End Else Begin
			Set @vSaldo = @Total
			Set @vCancelado = 'False'
		End
		
		--Retencion
		Declare @vDecimal bit = 'False'
		Set @vDecimal = IsNull((Select Top(1) [Decimales] From Moneda Where ID=@IDMoneda), 'True')
		set @PorcentajeRetencion = (Select Top(1) CompraPorcentajeRetencion  from Configuraciones )
		set @RetencionIVA  = (@TotalImpuesto  * @PorcentajeRetencion) / 100
		
		IF @IDMoneda =1 Begin
			set @RetencionIVA = ROUND(@RetencionIVA,0)
		End Else Begin
			set @RetencionIVA = ROUND(@RetencionIVA,2)
		End
		
		--Redondear si la moneda no tiene decimales!!!!!!
		If @vDecimal = 'False' Begin
			set @RetencionIVA = ROUND(@RetencionIVA, 0)
		End Else Begin
			set @RetencionIVA = ROUND(@RetencionIVA, 2)
		End

		--Insertar el Registro de Compra		
		Insert Into Compra(IDTransaccion, Numero, IDTipoComprobante, NroComprobante, NroTimbrado,  IDProveedor, IDSucursal, IDDeposito, Fecha, Credito, FechaVencimiento, FechaEntrada, FechaVencimientoTimbrado,  IDMoneda, Cotizacion, Observacion, Total, TotalImpuesto, TotalDiscriminado, Saldo, Cancelado, Directo, RetencionIVA,IDDepartamentoEmpresa)
		Values(@IDTransaccionSalida, @Numero, @IDTipoComprobante, @NroComprobante, @NroTimbrado,  @IDProveedor, @IDSucursalOperacion, @IDDepositoOperacion, @Fecha, @Credito, @FechaVencimiento, @FechaEntrada, @VtoTimbrado, @IDMoneda, @Cotizacion, @Observacion, @Total, @TotalImpuesto, @TotalDiscriminado,  @vSaldo, @vCancelado, @Directo, @RetencionIVA,@IDDepartamentoEmpresa)

		--Actualizar el proveedor
		Update Proveedor Set FechaUltimaCompra=@Fecha
		Where ID=@IDProveedor
								
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'UPD' Begin

	--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

	-- Si existe la compra
	    If Not Exists(Select * From Compra Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		If ((Select Fecha from Compra where IDTransaccion=@IDTransaccion) > @FechaVencimiento) begin
			set @Mensaje = 'La fecha de vencimiento no puede ser inferior a la fecha del documento.'
			set @Procesado = 'False'
			return @@rowcount
		End	


	 Update Compra
	   set Credito = @Credito,
	   Observacion = @Observacion,
	   FechaVencimiento = @FechaVencimiento,
	   NroTimbrado = @NroTimbrado,
	   FechaVencimientoTimbrado = @VtoTimbrado
	   
	   Where IDtransaccion = @IDTransaccion


		set @Mensaje = 'Registro Modificado'
		set @Procesado = 'True'
		return @@rowcount
	
	End

	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Compra Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que el documento no tenga otros registros asociados
		
		--Verificar que no tenga una nota se credito Proveedor alpicada
		If Exists (Select * From  NotaCreditoProveedorCompra Where IDTransaccionEgreso=@IDTransaccion ) Begin
			Set @Mensaje = 'No se puede eliminar la campra por que tine una nota de credito pendiente'
			Set @Procesado = 'False'
			print @Mensaje 
			return @@rowcount 
		End
		
		--Verificar que no tenga una nota se debito Proveedor alpicada
		If Exists (Select * From  NotaDebitoProveedorCompra  Where IDTransaccionEgreso=@IDTransaccion ) Begin
			Set @Mensaje = 'No se puede eliminar la campra por que tine una nota de debito pendiente'
			Set @Procesado = 'False'
			print @Mensaje 
			return @@rowcount 
		End		
		
		
		--Verificar que no tenga una orden de pago aplicada
		If Exists (Select * From OrdenPagoEgreso Where IDTransaccionEgreso=@IDTransaccion )Begin
		Set @Mensaje = 'No se puede eliminar la compra por que tiene una orden de pago pendiente.'
			Set @Procesado = 'False'
			print @Mensaje
			return @@rowcount			
		End
		
		--Actualizamos el Stock
		Exec SpDetalleCompra @IDTransaccion=@IDTransaccion, @Operacion='DEL', @Mensaje=@Mensaje output, @Procesado=@Procesado output
		
		
		
		--Actualizar el proveedor
		Declare @vIDProveedor int
		Declare @vUltimaCompra date
		
		Set @vIDProveedor = (Select IDProveedor From Compra Where IDTransaccion=@IDTransaccion)
		
		--Eliminamos el DetalleImpuesto
		Delete DetalleImpuesto Where IDTransaccion = @IDTransaccion
		
		----Eliminamos el registro
		Delete Compra Where IDTransaccion = @IDTransaccion
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
				
		Set @vUltimaCompra = (Select Max(Fecha) From Compra Where IDProveedor=@vIDProveedor)
		 
		Update Proveedor Set FechaUltimaCompra=@vUltimaCompra
		Where ID=@vIDProveedor
				
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End





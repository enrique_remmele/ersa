﻿CREATE Procedure [dbo].[SpDebitoCreditoBancario]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@Debito bit = NULL,
	@Credito bit = NULL,
	@IDTipoComprobante smallint = NULL,
	@Numero int = null,
	@NroComprobante bigint = NULL,
	@Fecha date = NULL,
	@IDCuentaBancaria tinyint = NULL,
	@Cotizacion money= NULL,
	@Observacion varchar(100) = NULL,
	@Total money = NULL,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@Conciliado bit = False,
	@EsCobranza bit = False,
	@EsProcesado bit = False,
	
		
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
	--SM 04082021 - Auditoria Informática
	Declare @vObservacion varchar(100)='' 
	Declare @vID int
	
--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		If Exists(Select * From DebitoCreditoBancario   Where Numero=@Numero And IDSucursal=@IDSucursalOperacion) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Si es que Numero de Cuenta se Repite
		if exists(Select * From DebitoCreditoBancario    Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante and Numero=@Numero  ) begin
			set @Mensaje = 'Numero de Cuenta Repetido!'
			set @Procesado = 'False'
			return @@rowcount
		end
			
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--Insertar en DepositoBancario
		Insert Into DebitoCreditoBancario (IDTransaccion, IDSucursal ,Debito ,Credito , IDTipoComprobante,Numero, NroComprobante, Fecha , IDCuentaBancaria , Cotizacion , Observacion , Total , TotalImpuesto  , TotalDiscriminado, Conciliado, EsCobranza, EsProcesado)
		Values(@IDTransaccionSalida , @IDSucursalOperacion,@Debito,@Credito , @IDTipoComprobante, @Numero , @NroComprobante , @Fecha, @IDCuentaBancaria ,  @Cotizacion, @Observacion, @Total , @TotalImpuesto , @TotalDiscriminado, @Conciliado, @EsCobranza, @EsProcesado)
		
		--INICIO SM 04082021 - Auditoria Informática
		set @vObservacion = @Observacion

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'INSERTAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccionSalida,'DEBITO CREDITO BANCARIO' )
		
		set @vID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccionSalida )
	    
		--Insertar en aDepositoBancario
		Insert Into aDebitoCreditoBancario (IDAuditoria,IDTransaccion, IDSucursal ,Debito ,Credito , IDTipoComprobante,Numero, NroComprobante, Fecha , IDCuentaBancaria , Cotizacion , Observacion ,Facturado, Total , TotalImpuesto  , TotalDiscriminado, Conciliado, EsCobranza, EsProcesado,IDUsuario,Accion)
	    Values(@vID,@IDTransaccionSalida , @IDSucursalOperacion,@Debito,@Credito , @IDTipoComprobante, @Numero , @NroComprobante , @Fecha, @IDCuentaBancaria ,  @Cotizacion, @Observacion,'False',@Total , @TotalImpuesto , @TotalDiscriminado, @Conciliado, @EsCobranza, @EsProcesado,@IDUsuario,'INS')
		--FIN SM 04082021 - Auditoria Informática
								
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End

	--SM 10-08-2021 Nueva Operacion para actualizar estado de la Transaccion
	if @Operacion = 'UPD' Begin
		If Exists(Select * From DebitoCreditoBancario  Where IDTransaccion=@IDTransaccion ) Begin
			update DebitoCreditoBancario
			set EsProcesado = 'True'
			where IDTransaccion=@IDTransaccion
		
		--SM 10082021 - Auditoria Informática
		set @IDTipoComprobante = (select IDTipoComprobante from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		declare @Comprobante as varchar(32) = (select NroComprobante from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		declare @VTipoComprobante varchar(16) = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		Declare @vIDCuentaBancaria int = (select IDCuentaBancaria from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		declare @Condicion varchar(16) = (select (Case when Credito = 'True' then 'CREDITO' else 'DEBITO' end) from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)

		declare @Facturado bit = (select Facturado from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Debito = (select Debito from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Credito = (select Credito from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Numero = (select Numero from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Cotizacion = (select Cotizacion from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Observacion= (select Observacion from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Total= (select Total from DebitoCreditoBancario where IDTransaccion = @IDTransaccion) 
		set @TotalImpuesto = (select TotalImpuesto from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @TotalDiscriminado = (select TotalDiscriminado from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Conciliado = (select Conciliado from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @EsCobranza = (select EsCobranza from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @vObservacion = Concat('Se actualiza estado de Transaccion. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccionSalida,'DEBITO CREDITO BANCARIO' )
		
		set @vID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion )
	    
		--Insertar en aDepositoBancario, AUXILIAR DE AUDITORIA
		Insert Into aDebitoCreditoBancario (IDAuditoria, IDTransaccion, IDSucursal, Debito, Credito, IDTipoComprobante, Numero, NroComprobante, Fecha, IDCuentaBancaria, Cotizacion, Observacion,Facturado, Total, TotalImpuesto, TotalDiscriminado, Conciliado, EsCobranza, EsProcesado, IDUsuario, Accion)
	    Values(@vID,@IDTransaccion , @IDSucursal,@Debito,@Credito , @IDTipoComprobante, @Numero , @Comprobante , getdate(), @vIDCuentaBancaria ,  @Cotizacion, @Observacion, @Facturado, @Total , @TotalImpuesto , @TotalDiscriminado, @Conciliado, @EsCobranza, 'True',@IDUsuario,'UPD')
	
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
		End
	End

	If @Operacion = 'DEL' Begin
		--INICIO SM 04082021 - Auditoria Informática	
		set @IDTerminal = (Select IDTerminal from Transaccion where ID = @IDTransaccion)
		set @IDUsuario =  (Select IDUsuario from Transaccion where ID = @IDTransaccion)
		set @IDOperacion = (Select IDOperacion from Transaccion where ID = @IDTransaccion)
		--FIN SM 04082021 - Auditoria Informática
		
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From DebitoCreditoBancario     Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--IDTransaccion
		If Exists(Select * From DetalleCuotaPrestamoBancario     Where IDTransaccionDebitoCreditoBancario=@IDTransaccion) Begin
			set @Mensaje = 'Tiene asignada cuota de Prestamo Bancario.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select NroComprobante from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @VTipoComprobante  = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @vIDCuentaBancaria  = (select IDCuentaBancaria from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Condicion = (select (Case when Credito = 'True' then 'CREDITO' else 'DEBITO' end) from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,0,@vIDCuentaBancaria,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ELI',@VTipoComprobante, @IDSucursal)

		--INICIO SM 04082021 - Auditoria Informática
		set @Facturado = (select Facturado from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Debito = (select Debito from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Credito = (select Credito from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Numero = (select Numero from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Cotizacion = (select Cotizacion from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Observacion= (select Observacion from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Total= (select Total from DebitoCreditoBancario where IDTransaccion = @IDTransaccion) 
		set @TotalImpuesto = (select TotalImpuesto from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @TotalDiscriminado = (select TotalDiscriminado from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @Conciliado = (select Conciliado from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @EsCobranza = (select EsCobranza from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @EsProcesado = (select EsProcesado from DebitoCreditoBancario where IDTransaccion = @IDTransaccion)
		set @vObservacion = CONCAT('Se elimina registro de DebitoCreditoBancario. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'DEBITO CREDITO BANCARIO' )
		
		set @vID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion )
	    
		--Insertar en aDepositoBancario, AUXILIAR DE AUDITORIA
		Insert Into aDebitoCreditoBancario (IDAuditoria, IDTransaccion, IDSucursal, Debito, Credito, IDTipoComprobante, Numero, NroComprobante, Fecha, IDCuentaBancaria, Cotizacion, Observacion,Facturado, Total, TotalImpuesto, TotalDiscriminado, Conciliado, EsCobranza, EsProcesado, IDUsuario, Accion)
	    Values(@vID,@IDTransaccion , @IDSucursal,@Debito,@Credito , @IDTipoComprobante, @Numero , @Comprobante , getdate(), @vIDCuentaBancaria ,  @Cotizacion, @Observacion, @Facturado, @Total , @TotalImpuesto , @TotalDiscriminado, @Conciliado, @EsCobranza, @EsProcesado,@IDUsuario,'ELI')
		
		set @vObservacion =  CONCAT('Se Eliminara el Detalle de Impuesto. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'DETALLE IMPUESTO' )
	
		set @vObservacion =  CONCAT('Se Eliminara el Detalle de Asiento. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'DETALLE ASIENTO' )
		
		set @vObservacion =  CONCAT('Se Eliminara el Asiento. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'ASIENTO' )
		
		set @vObservacion =  CONCAT('Se Eliminara la Transaccion. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'TRANSACCION' )
		--FIN SM 04082021 - Auditoria Informática

		--Eliminamos el registro
		Delete DebitoCreditoBancario   Where IDTransaccion = @IDTransaccion
		
		--Eliminamos el Registro
		Delete DetalleImpuesto Where IDTransaccion = @IDTransaccion 
						
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion				
				
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End









﻿CREATE Procedure [dbo].[SpAsientoNotaCreditoDevolucionProveedor]

	@IDTransaccion numeric(18,0)
		
As

Begin

	
	SET NOCOUNT ON
	
	--Variables
	Declare @vIDSucursal tinyint
	Declare @vRedondeo tinyint = 0

	--NotaCreditoProveedor
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vFecha date
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money
	Declare @vCuentaContableNotaCreditoProveedor varchar(50)
	Declare @vCuentaContableCosto varchar(50)
	Declare @vTotalDiscriminado money
	Declare @vTotalDescuentoDiscriminado money
	Declare @vTotalCosto money
	Declare @vFactorimpuesto decimal(18,7)
	Declare @vIDProducto int
	Declare @vIDTipoProducto int
	
	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	
	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money

	Declare @vProveedor as varchar(32)
	Declare @vIDProveedor as int
	
	--Obtener valores
	Begin
	

		Select	@vIDSucursal=IDSucursal,
				@vTipoComprobante=TipoComprobante,
				@vIDTipoComprobante=IDTipoComprobante,
				@vComprobante=Comprobante,
				@vFecha=Fecha,
				@vIDMoneda=IDMoneda,
				@vCotizacion=Cotizacion,
				@vProveedor = Proveedor,
				@vIDProveedor = IDProveedor
		From VNotaCreditoProveedor Where IDTransaccion=@IDTransaccion
	
		

	End
					
	--Verificar que el asiento se pueda modificar
	Begin
		
		If not exists(Select * From NotaCreditoProveedor Where IDTransaccion=@IDTransaccion) Begin
			print 'La operacion no es una NotaCreditoProveedor'
			GoTo salir
		End 
		
		If not exists(Select * From NotaCreditoProveedor Where IDTransaccion=@IDTransaccion and Devolucion = 1) Begin
			print 'La Nota de credito no es una devolucion'
			GoTo salir
		End 	

		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	
		
	End
				
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
	
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = @vTipoComprobante + ' ' + @vComprobante + ' - ' + @vProveedor
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
	End
	
	--Cuentas Fijas de Proveedors
	Begin
	
		--Variables
		Declare @vNotaCreditoProveedorCredito bit
		Declare @vNotaCreditoProveedorContado bit
		Declare @vCodigoCuentaProveedor varchar(50)
		Declare @vTotalProducto money

		Declare cCFNotaCreditoProveedorProveedor cursor for
		Select TOP(1)CuentaContableProveedorTipoProducto,
		'Total'=(Select Sum(VE.Cotizacion * VNCP.Importe)
				From vNotaCreditoProveedorCompra VNCP
				join vegreso VE on VNCP.IDTransaccionEgreso=VE.IDTransaccion
				Where VNCP.IDTransaccionNotaCreditoProveedor = @IDTransaccion) 

		From vDetalleNotaCreditoProveedor 
		Where IDTransaccion=@IDTransaccion
		--Declare cCFNotaCreditoProveedorProveedor cursor for
		--Select CuentaContableProveedorTipoProducto,
		--'Total'=Sum(VE.Cotizacion * VNCP.Importe)
		--From vNotaCreditoProveedorCompra VNCP
		--join vegreso VE on VNCP.IDTransaccionEgreso=VE.IDTransaccion

		--Where VNCP.IDTransaccionNotaCreditoProveedor = @IDTransaccion
		Open cCFNotaCreditoProveedorProveedor   
		fetch next from cCFNotaCreditoProveedorProveedor into @vCodigoCuentaProveedor, @vTotalProducto
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vCodigo = @vCodigoCuentaProveedor
			if (@vCodigo = '') begin
				Set @vCodigo = '21010101'
			end

			Set @vImporteDebe = 0
			Set @vImporteHaber = 0

			--Obtener la cuenta
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			Set @vTotalProducto = @vTotalProducto --* @vCotizacion
			--Set @vImporteHaber = @vTotalProducto
			Set @vImporteDebe = @vTotalProducto
				
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin
			print 'entro'	
			print @vImporteHaber
			print Concat('Importe Proveedor',@vImporteDebe)
			print @vRedondeo
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End
			End
			print Concat('Proveedor ',@vImporteDebe)
			fetch next from cCFNotaCreditoProveedorProveedor into @vCodigoCuentaProveedor, @vTotalProducto
			
		End
		
		close cCFNotaCreditoProveedorProveedor 
		deallocate cCFNotaCreditoProveedorProveedor
	End
					
	--Cuentas Fijas Impuesto
	Begin
		
		--Variables
		Declare @vIDImpuesto tinyint
		
		Declare cCFNotaCreditoProveedorImpuesto cursor for
		Select Codigo, IDImpuesto, Debe, Haber from VCFCompra 
		Where TipoCuentaFija = 'IMPUESTO' 
		And IDSucursal=@vIDSucursal 
		And IDMoneda=@vIDMoneda
		
		Open cCFNotaCreditoProveedorImpuesto   
		fetch next from cCFNotaCreditoProveedorImpuesto into @vCodigo, @vIDImpuesto, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Set @vImporte = (Select SUM(TotalImpuesto * @vCotizacion) From DetalleImpuesto Where IDTransaccion=@IDTransaccion And IDImpuesto=@vIDImpuesto)
			Set @vImporte = (select sum(TotalImpuesto * @vCotizacion) from DetalleNotaCreditoProveedor where IDTransaccion = @IDTransaccion and IDImpuesto = @vIDImpuesto)
			--Debe ser invertido ya que es una nc
			If @vDebe = 1 Begin
				--Set @vImporteDebe = @vImporte
				Set @vImporteHaber = @vImporte
			End
			
			If @vHaber = 1 Begin
				--Set @vImporteHaber = @vImporte
				Set @vImporteDebe = @vImporte
			End				
			
			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
										Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End
			End
			
			fetch next from cCFNotaCreditoProveedorImpuesto into @vCodigo, @vIDImpuesto, @vDebe, @vHaber
			
		End
		
		close cCFNotaCreditoProveedorImpuesto 
		deallocate cCFNotaCreditoProveedorImpuesto
	End					

	--Cuentas Fijas de Costo de Mercaderia
	Begin
	
		--Cuenta Fija NotaCreditoProveedor
		Set @vCuentaContableNotaCreditoProveedor = ''
		Set @vTotalCosto = 0
			
		Declare cDetalleCosto cursor for
		Select CuentaContableCompra,
				'Total'=Total,
				IDProducto
			    From VDetalleNotaCreditoProveedor 
				Where IDTransaccion=@IDTransaccion 
		Open cDetalleCosto
		fetch next from cDetalleCosto into @vCuentaContableCosto, @vTotalCosto, @vIDProducto
		
		While @@FETCH_STATUS = 0 Begin  
					
			set @vIDTipoProducto = (select idtipoProducto from Producto where ID = @vIDProducto)

			if @vIDTipoProducto = 27 begin
				if @vIDSucursal = 4 and @vFecha >= '20191127' begin
					set @vCuentaContableCosto = '11122102'
				end
			end
			
			set @vImporte = 0
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0

			set @vFactorimpuesto = (select FactorImpuesto from Impuesto where id = (select idimpuesto from producto where id = @vIDProducto))
			if @vFactorimpuesto > 1 begin
				Set @vTotalCosto = @vTotalCosto - Round((@vTotalCosto/@vFactorimpuesto),2)
			end 
			
			PRINT concat('factorimpuesto ', @vTotalCosto, ' ', @vFactorimpuesto)
			Set @vTotalCosto = @vTotalCosto * @vCotizacion
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vCodigo = @vCuentaContableCosto
			--Si no existe, poner el predeterminado
			If @vCuentaContableCosto = '' Begin
				Set @vCodigo = (Select IsNull((Select Top(1) Codigo From VCFCompra Where TipoCuentaFija = 'PRODUCTO' and IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda),''))								
			End
			
			--Validar codigo
			If @vCodigo = '' Begin
				fetch next from cDetalleCosto into @vCuentaContableCosto, @vTotalCosto
			End
			
			--Hayar el importe	
			Set @vImporte = @vTotalCosto
			
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')			
			
			
			Set @vImporteHaber = @vImporte
			
			--Cargamod
			If @vImporteHaber > 0 Or @vImporteDebe > 0 Begin
			
				--Si existe la cuenta, actualizar
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,@vRedondeo),
												Debito=Debito+Round(@vImporteDebe,@vRedondeo),
												Importe=Importe+Round(@vImporte,@vRedondeo)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
										Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,@vRedondeo), Round(@vImporteDebe,@vRedondeo), 0, '')
				End
			End
							
			fetch next from cDetalleCosto into @vCuentaContableCosto, @vTotalCosto, @vIDProducto
			
		End
		
		close cDetalleCosto 
		deallocate cDetalleCosto
		
	End

	--Actualizamos la cabecera, el total
	Begin
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
		Set @vImporteHaber = ROUND(@vImporteHaber, @vRedondeo)
		Set @vImporteDebe = ROUND(@vImporteDebe, @vRedondeo)
	
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vImporteHaber - @vImporteDebe
		Where IDTransaccion=@IDTransaccion
	End

	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
	
End

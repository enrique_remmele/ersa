﻿CREATE Procedure [dbo].[SpDepositoBancarioProcesar]

	--Entrada
	@IDTransaccion numeric(18),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin
    Declare @vObservacion varchar(100)='' 
	Declare @vIDTerminal int = (Select IDTerminal from Transaccion where ID = @IDTransaccion)
	

	--Validar Feha Operacion
	Declare @vIDSucursal as integer
	Declare @vFecha as Date
	Declare @vIDUsuario as integer
	Declare @vIDOperacion as integer
	Declare @vFechaDeposito as date
	
	Select	@vIDSucursal=IDSucursal,
			@vFecha=Fecha,
			@vIDUsuario=IDUsuario,
			@vIDOperacion=IDOperacion
	 From Transaccion Where ID = @IDTransaccion

--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@vIDSucursal, @vFecha, @vIDUsuario, @vIDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			--set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		If dbo.FValidarFechaOperacion(@vIDSucursal, @vFecha, @vIDUsuario, @vIDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			--set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

    ---Variables para CALCULAR SALDO DEPOSITADO
	Declare @Saldo money
	Declare @Depositado money
	Declare @Cancelado bit

		
	--Declarar variables Variables
	declare @vIDTransaccionCheque numeric (18,0)
	declare @vIDTransaccionEfectivo numeric(18,0)
	declare @vIDTransaccionDocumento numeric(18,0)
	declare @vIDDocumento numeric(18,0)
	declare @vIDTransaccionTarjeta numeric(18,0)
	declare @vIDTipoComprobante numeric(18,0)
	declare @vIDEfectivo int
	declare @vImporte money
	declare @vIDTarjeta integer
				
	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'


	If @Operacion = 'INS' Begin
		
		--Procesar los Efectivos
		Begin
			Declare db_cursor cursor for
			Select IDTransaccionEfectivo,IDEfectivo,Importe 
			From DetalleDepositoBancario D Join Efectivo E On D.IDTransaccionEfectivo=E.IDTransaccion And D.IDEfectivo=E.ID
			Where IDTransaccionDepositoBancario=@IDTransaccion
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionEfectivo, @vIDEfectivo,@vImporte
			While @@FETCH_STATUS = 0 Begin  
				
				Set @Cancelado  = 'False'
				Set @Depositado = 0
				
				-----CALCULAR SALDO DEPOSITADO
				Set @Saldo = (Select Saldo From Efectivo Where IDTransaccion=@vIDTransaccionEfectivo And ID=@vIDEfectivo)  
				Set @Depositado = (Select Depositado From Efectivo Where IDTransaccion=@vIDTransaccionEfectivo And ID=@vIDEfectivo)
				--
				If @Saldo > @vImporte Begin
					Set @Saldo = @Saldo - @vImporte
				End Else Begin
					set @Saldo = @vImporte - @Saldo 
				End 
				
				Set @Depositado = @Depositado + @vImporte 
				
				--Verificar si se cancela el Efectivo
				If @Saldo = 0 Begin
					Set @Cancelado = 'True'  
				End
			   
			   --INICIO SM 15072021 - Auditoria Informática		
               set @vObservacion = concat('Deposito: ',@Depositado, ' - Saldo:',@Saldo, ' - Cancelado:',@Cancelado, ' - IDEfectivo:', @vIDEfectivo)

		       Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		       Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@vIDTransaccionEfectivo,'EFECTIVO' )
		       --FIN SM 15072021 - Auditoria Informática

				----ACTUALIZAR LA TABLA EFECTIVO			
				Update Efectivo  Set Depositado=@Depositado,
									 Saldo= @Saldo,
									 Cancelado=@Cancelado    
				where IDTransaccion= @vIDTransaccionEfectivo And ID=@vIDEfectivo
				
				Fetch Next From db_cursor Into @vIDTransaccionEfectivo, @vIDEfectivo,@vImporte
								
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		End	
		
		--Procesar los Cheques
		Begin
			Declare db_cursor cursor for
			Select D.IDTransaccionCheque,  D.Importe From DetalleDepositoBancario D Join ChequeCliente C On D.IDTransaccioncheque=C.IDTransaccion 
			Where IDTransaccionDepositoBancario=@IDTransaccion
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionCheque, @vImporte
			While @@FETCH_STATUS = 0 Begin  
				
               --INICIO SM 15072021 - Auditoria Informática		
               set @vObservacion = concat('Depositado: True', ' - Cartera: False', ' - Rechazado: False')

		       Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		       Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@vIDTransaccionCheque ,'CHEQUE CLIENTE' )
		       --FIN SM 15072021 - Auditoria Informática

				----ACTUALIZAR LA TABLA CHEQUE			
				Update ChequeCliente Set Depositado = 'True',
										Cartera = 'False',
										Rechazado = 'False'
										--Saldo= @Saldo,
										--Cancelado=@Cancelado    
				where IDTransaccion= @vIDTransaccionCheque 
				
				Fetch Next From db_cursor Into @vIDTransaccionCheque, @vImporte
								
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		End	
		
		--Procesar los Documentos
		Begin
			Declare db_cursor cursor for
			Select D.IDTransaccionDocumento,FPD.IDTipoComprobante, D.IDDetalleDocumento ,D.Importe 
			From DetalleDepositoBancario D 
			Join VFormaPagoDocumento FPD On D.IDTransaccionDocumento=FPD.IDTransaccion and FPD.ID = D.IDDetalleDocumento
			Where IDTransaccionDepositoBancario=@IDTransaccion And Deposito='True'
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionDocumento,@vIDTipoComprobante, @vIDDocumento,@vImporte
			While @@FETCH_STATUS = 0 Begin  
				
				Set @Cancelado  = 'False'
				
				-----CALCULAR SALDO DEPOSITADO
				Set @Saldo = (Select Saldo From VFormaPagoDocumento Where IDTransaccion=@vIDTransaccionDocumento And Deposito='True' and Id = @vIDDocumento)  
								--
				Set @Saldo = @Saldo - @vImporte
			
				If @Saldo < 0 Begin
					Set @Saldo=@Saldo*-1
				End
				
						
				--Verificar si se cancela el Documento
				If @Saldo = 0 Begin
					Set @Cancelado = 'True'  
				End
				
			   --INICIO SM 15072021 - Auditoria Informática		
               set @vObservacion = concat('Saldo:',@Saldo, ' - Cancelado:',@Cancelado, ' - IDTipoComprobante:',@vIDTipoComprobante, ' - ID:',@vIDDocumento)

		       Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		       Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@vIDTransaccionDocumento ,'FORMA PAGO DOCUMENTO' )
		       --FIN SM 15072021 - Auditoria Informática
								
				----ACTUALIZAR LA TABLA FormaPagoDocumento		
				Update FormaPagoDocumento  Set  Saldo=@Saldo,
												Cancelado=@Cancelado
				Where IDTransaccion= @vIDTransaccionDocumento And IDTipoComprobante =@vIDTipoComprobante and ID = @vIDDocumento
				
				Fetch Next From db_cursor Into @vIDTransaccionDocumento,@vIDTipoComprobante, @vIDDocumento,@vImporte
								
			End
		
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		End	
		
				--Procesar las Tarjetas
		Begin
			Declare db_cursor cursor for
			Select D.IDTransaccionTarjeta,FPT.IDTipoComprobante,D.Importe , D.IDDetalleDocumento
			From DetalleDepositoBancario D 
			Join VFormaPagoTarjeta FPT On D.IDTransaccionTarjeta=FPT.IDTransaccion  and D.IDDetalleDocumento = FPT.ID
			Where IDTransaccionDepositoBancario=@IDTransaccion And Deposito='True'
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionTarjeta,@vIDTipoComprobante,@vImporte, @vIDTarjeta
			While @@FETCH_STATUS = 0 Begin  
				
				Set @Cancelado  = 'False'
				
				-----CALCULAR SALDO DEPOSITADO
				Set @Saldo = (Select Saldo 
								From VFormaPagoTarjeta 
								Where IDTransaccion=@vIDTransaccionTarjeta 
								and ID = @vIDTarjeta
								And Deposito='True')  
								--
				Set @Saldo = @Saldo - @vImporte
			
				If @Saldo < 0 Begin
					Set @Saldo=@Saldo*-1
				End
										
				--Verificar si se cancela la Tarjeta
				If @Saldo = 0 Begin
					Set @Cancelado = 'True'  
				End
				
			   --INICIO SM 15072021 - Auditoria Informática		
               set @vObservacion = concat('Saldo:',@Saldo, ' - Cancelado:',@Cancelado, ' - IDTipoComprobante:',@vIDTipoComprobante, ' - ID:',@vIDTarjeta)

		       Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		       Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@vIDTransaccionTarjeta ,'FORMA PAGO TARJETA' )
		       --FIN SM 15072021 - Auditoria Informática
								
				----ACTUALIZAR LA TABLA FormaPagoTarjeta		
				Update FormaPagoTarjeta  Set  Saldo=@Saldo,
												Cancelado=@Cancelado
				Where IDTransaccion= @vIDTransaccionTarjeta 
				And IDTipoComprobante =@vIDTipoComprobante
				and ID = @vIDTarjeta
				
				Fetch Next From db_cursor Into @vIDTransaccionTarjeta,@vIDTipoComprobante,@vImporte, @vIDTarjeta
								
			End

			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor	
		End

		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
			
	End	
	
	
	If @Operacion = 'DEL' Begin	 
					
		--Procesar los Efectivos
		Begin
			Declare db_cursor cursor for
			Select IDTransaccionEfectivo, IDEfectivo, Importe 
			From DetalleDepositoBancario D 
			Join Efectivo E On D.IDTransaccionEfectivo=E.IDTransaccion And D.IDEfectivo=E.ID
			Where IDTransaccionDepositoBancario=@IDTransaccion
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionEfectivo, @vIDEfectivo,@vImporte
			While @@FETCH_STATUS = 0 Begin  
				
				Set @Cancelado  = 'False'
				
				-----CALCULAR SALDO DEPOSITADO
				Set @Saldo = (select Saldo from Efectivo where IDTransaccion=@vIDTransaccionEfectivo And ID=@vIDEfectivo)  
				Set @Depositado = (select Depositado from Efectivo where IDTransaccion=@vIDTransaccionEfectivo And ID=@vIDEfectivo)
				--
				Set @Saldo = @Saldo + @vImporte
								
				Set @Depositado = @Depositado - @vImporte 
				
				--Verificar si se cancela el Efectivo
				If @Saldo = 0 Begin
					Set @Cancelado = 'True'  
				End
					
                --INICIO SM 15072021 - Auditoria Informática		
                set @vObservacion = concat('Deposito: ',@Depositado, ' - Saldo:',@Saldo, ' - Cancelado:',@Cancelado, ' - IDEfectivo:', @vIDEfectivo)

		        Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		        Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@vIDTransaccionEfectivo,'EFECTIVO' )
		        --FIN SM 15072021 - Auditoria Informática

				----ACTUALIZAR LA TABLA EFECTIVO			
				Update Efectivo  Set Depositado=@Depositado,
									 Saldo= @Saldo,
									 Cancelado=@Cancelado    
				where IDTransaccion= @vIDTransaccionEfectivo And ID=@vIDEfectivo
				
				Fetch Next From db_cursor Into @vIDTransaccionEfectivo, @vIDEfectivo,@vImporte
								
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		End	
		
		--Procesar los Cheques
		Begin
			Declare db_cursor cursor for
			Select D.IDTransaccionCheque,  D.Importe , DB.Fecha
			From DetalleDepositoBancario D 
			Join ChequeCliente C On D.IDTransaccioncheque=C.IDTransaccion 
			Join DepositoBancario DB on DB.IDTransaccion = D.IDTransaccionDepositoBancario
			Where IDTransaccionDepositoBancario=@IDTransaccion
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionCheque, @vImporte, @vFechaDeposito
			While @@FETCH_STATUS = 0 Begin  
			 If exists( Select IDTransaccion from ChequeClienteRechazado 
						where IDTransaccionCheque = @vIDTransaccionCheque 
						and Fecha < @vFechaDeposito and Anulado = 'False') begin
				
				--INICIO SM 15072021 - Auditoria Informática		
                set @vObservacion = concat('Depositado: False',' - Rechazado: True')

		        Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		        Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@vIDTransaccionCheque,'CHEQUE CLIENTE' )
				--FIN SM 15072021 - Auditoria Informática		
				
				Update ChequeCliente Set Depositado = 'False',
										--Cartera = 'False'
										  Rechazado = 'True'
										--Saldo= @Saldo,
										--Cancelado=@Cancelado    
				where IDTransaccion= @vIDTransaccionCheque 
			 End
			 
			 If not exists( Select IDTransaccion from ChequeClienteRechazado 
						where IDTransaccionCheque = @vIDTransaccionCheque 
						and Fecha < @vFechaDeposito and Anulado = 'False') begin
			    ----ACTUALIZAR LA TABLA CHEQUE		
				
				--INICIO SM 15072021 - Auditoria Informática		
                set @vObservacion = concat('Depositado: False',' - Cartera: True')

		        Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		        Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@vIDTransaccionCheque,'CHEQUE CLIENTE' )
				--FIN SM 15072021 - Auditoria Informática
					
				Update ChequeCliente Set Depositado = 'False',
										Cartera = 'True'
										--Rechazado = 'True'
										--Saldo= @Saldo,
										--Cancelado=@Cancelado    
				where IDTransaccion= @vIDTransaccionCheque 

			 end
			 Fetch Next From db_cursor Into @vIDTransaccionCheque, @vImporte, @vFechaDeposito
								
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		End	
		
		--Procesar los Documentos
		Begin
			declare @vId as integer
			
			Declare db_cursor cursor for			
			--Select D.IDTransaccionDocumento,FPD.IDTipoComprobante,D.Importe From DetalleDepositoBancario D Join VFormaPagoDocumento FPD On D.IDTransaccionDocumento=FPD.IDTransaccion 
			--Where IDTransaccionDepositoBancario=@IDTransaccion And Deposito='True'
			Select D.IDTransaccionDocumento,FPD.IDTipoComprobante,D.Importe, D.IDDetalleDocumento
			From DetalleDepositoBancario D 
			Join VFormaPagoDocumento FPD On D.IDTransaccionDocumento=FPD.IDTransaccion and D.IDDetalleDocumento = FPD.ID
			Where IDTransaccionDepositoBancario=@IDTransaccion And Deposito='True'

			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionDocumento,@vIDTipoComprobante,@vImporte, @vId
			While @@FETCH_STATUS = 0 Begin  
				
				Set @Cancelado  = 'False'
				
				-----CALCULAR SALDO DEPOSITADO
				Set @Saldo = (Select Saldo From VFormaPagoDocumento Where IDTransaccion=@vIDTransaccionDocumento and ID = @vId And Deposito='True')  
								--
				Set @Saldo = @Saldo + @vImporte
				
			   --INICIO SM 15072021 - Auditoria Informática		
               set @vObservacion = concat('Saldo:',@Saldo, ' - Cancelado:',@Cancelado, ' - IDTipoComprobante:',@vIDTipoComprobante, ' - ID:',@vID)

		       Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		       Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@vIDTransaccionDocumento ,'FORMA PAGO DOCUMENTO' )
		       --FIN SM 15072021 - Auditoria Informática
								
				----ACTUALIZAR LA TABLA FormaPagoDocumento		
				Update FormaPagoDocumento  Set  Saldo=@Saldo,
												Cancelado=@Cancelado
				Where IDTransaccion= @vIDTransaccionDocumento And IDTipoComprobante =@vIDTipoComprobante AND ID = @vId
				
				Fetch Next From db_cursor Into @vIDTransaccionDocumento,@vIDTipoComprobante,@vImporte, @vId
								
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		End	
		
		--Procesar las tarjetas
		Begin
			declare db_cursor cursor for
			select D.IDTransaccionTarjeta, FPT.IDTipoComprobante, D.Importe, D.IDDetalleDocumento
			from DetalleDepositoBancario D 
			join VFormaPagoTarjeta FPT on d.IDTransaccionTarjeta = FPT.IDTransaccion
			and D.IDDetalleDocumento = FPT.ID
			WHERE IDTransaccionDepositoBancario = @IDTransaccion And Deposito='True'

			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionTarjeta, @vIDTipoComprobante, @vImporte, @vIdTarjeta
			While @@FETCH_STATUS = 0 Begin  
				
				Set @Cancelado  = 'False'
				
				-----CALCULAR SALDO DEPOSITADO
				Set @Saldo = (Select Saldo 
							From VFormaPagoTarjeta 
							Where IDTransaccion=@vIDTransaccionTarjeta 
							and ID = @vIDTarjeta
							And Deposito='True')  
								--
				Set @Saldo = @Saldo + @vImporte
				
				--INICIO SM 15072021 - Auditoria Informática		
               set @vObservacion = concat('Saldo:',@Saldo, ' - Cancelado:',@Cancelado, ' - IDTipoComprobante:',@vIDTipoComprobante, ' - ID:',@vIDTarjeta)

		       Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		       Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@vIDTransaccionTarjeta ,'FORMA PAGO TARJETA' )
		       --FIN SM 15072021 - Auditoria Informática
								
				----ACTUALIZAR LA TABLA FormaPagoTarjeta		
				Update FormaPagoTarjeta  Set  Saldo=@Saldo,
												Cancelado=@Cancelado
				Where IDTransaccion= @vIDTransaccionTarjeta 
				and ID = @vIDTarjeta
				And IDTipoComprobante =@vIDTipoComprobante
				
				Fetch Next From db_cursor Into @vIDTransaccionTarjeta, @vIDTipoComprobante, @vImporte, @vIDTarjeta
								
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor
		End

		--INICIO SM 16072021 - Auditoria Informática
		set @vObservacion = 'SpDepositoBancarioProcesar: Se Eliminara el Detalle Deposito Bancario. '

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@IDTransaccion,'DETALLE DEPOSITO BANCARIO' )
		
		Declare @vidauditoria int = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion)
		Declare @vvID int
		Declare @vvIDTransaccionEfectivo numeric(18,0)
		Declare @vvIDEfectivo smallint
		Declare @vvIDTransaccionCheque numeric(18,0)
		Declare @vvIDTransaccionDocumento numeric(18,0)
		Declare @vvRedondeo bit
		Declare @vvFaltante bit
		Declare @vvSobrante bit
		Declare @vvImporte money
		Declare @vvIDTransaccionTarjeta numeric(18,0)
		Declare @vvIDDetalleDocumento int
		
		Begin
			Declare db_cursor cursor for
			
			Select ID, IDTransaccionEfectivo,IDEfectivo, IDTransaccionCheque, IDTransaccionDocumento,Redondeo,Faltante,Sobrante,Importe,IDTransaccionTarjeta, IDDetalleDocumento
			From DetalleDepositoBancario  
			Where IDTransaccionDepositoBancario=@IDTransaccion
			Open db_cursor 
			Fetch Next From db_cursor Into @vvID, @vvIDTransaccionEfectivo,@vvIDEfectivo, @vvIDTransaccionCheque, @vvIDTransaccionDocumento,@vvRedondeo,@vvFaltante,@vvSobrante,@vvImporte,@vvIDTransaccionTarjeta, @vvIDDetalleDocumento
		
			While @@FETCH_STATUS = 0 Begin 
			      
				Insert into aDetalleDepositoBancario(IDAuditoria,IDTransaccionDepositoBancario,ID,IDTransaccionefectivo,IDEfectivo,IDTransaccionCheque,IDTransaccionDocumento,Redondeo,Faltante,Sobrante,Importe,IDTransaccionTarjeta,IDDetalleDocumento,IDUsuario,Accion)
                   values (@vidauditoria,@IDTransaccion, @vvID, @vvIDTransaccionEfectivo,@vvIDEfectivo, @vvIDTransaccionCheque, @vvIDTransaccionDocumento,@vvRedondeo,@vvFaltante,@vvSobrante,@vvImporte,@vvIDTransaccionTarjeta, @vvIDDetalleDocumento,@vIDUsuario,'ELI')
		 
				Fetch Next From db_cursor Into @vvID, @vvIDTransaccionEfectivo,@vvIDEfectivo, @vvIDTransaccionCheque, @vvIDTransaccionDocumento,@vvRedondeo,@vvFaltante,@vvSobrante,@vvImporte,@vvIDTransaccionTarjeta, @vvIDDetalleDocumento
		
            End
            Close db_cursor   
			Deallocate db_cursor		   			
		End
		
		set @vObservacion = 'SpDepositoBancarioProcesar: Se Eliminara el Deposito Bancario. '

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@IDTransaccion,'DEPOSITO BANCARIO' )
		
		set @vObservacion = 'SpDepositoBancarioProcesar: Se Eliminara el Detalle de Asiento. '

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@IDTransaccion,'DETALLE ASIENTO' )
		
		set @vObservacion = 'SpDepositoBancarioProcesar: Se Eliminara el Asiento. '

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@IDTransaccion,'ASIENTO' )
		
		set @vObservacion = 'SpDepositoBancarioProcesar: Se Eliminara la Transaccion. '

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@IDTransaccion,'TRANSACCION' )
				
	    --FIN SM 16072021 - Auditoria Informática

		--Eliminamos el detalle
		Delete DetalleDepositoBancario  where IDTransaccionDepositoBancario = @IDTransaccion
				
		--Eliminamos el registro
		Delete DepositoBancario  Where IDTransaccion = @IDTransaccion
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion				

		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
	
	End
	
End
	

	







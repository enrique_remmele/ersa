﻿
CREATE Procedure [dbo].[SpViewExistenciaDepositoInventario]

	--Entrada
	@IDTransaccion numeric(18,0),
	@IDUsuario int,
	@IDDeposito int,
	@IDEquipo int
	
As

Begin

	Declare @vIDZonaDeposito int
	Set @vIDZonaDeposito = (Select IDZonaDeposito From EquipoConteo Where ID=@IDEquipo)
	
	Select 
	ED.IDProducto, 
	ED.Referencia, 
	ED.Producto, 
	ED.CodigoBarra, 
	'Averiados'=IsNull((Select ED2.Averiados From ExistenciaDepositoInventarioEquipo ED2 Where ED2.IDTransaccion=ED.IDTransaccion And ED2.IDProducto=ED.IDProducto And ED2.IDDeposito=ED.IDDeposito And ED2.IDEquipo=@IDEquipo),0),
	'Conteo'=IsNull((Select ED2.Conteo From ExistenciaDepositoInventarioEquipo ED2 Where ED2.IDTransaccion=ED.IDTransaccion And ED2.IDProducto=ED.IDProducto And ED2.IDDeposito=ED.IDDeposito And ED2.IDEquipo=@IDEquipo),0)
	
	From VExistenciaDepositoInventario ED 
	--Join ProductoZona PZ On ED.IDProducto=PZ.IDProducto 
	--Join Deposito D on PZ.IDDeposito=D.ID 
	
	Where ED.IDTransaccion=@IDTransaccion 
	--And PZ.IDZonaDeposito=@vIDZonaDeposito
	 
	Order By Producto 

End



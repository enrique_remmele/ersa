﻿CREATE  Procedure [dbo].[SpNotaDebitoActualizarVanta]

	--Entrada
	@IDTransaccion numeric(18, 0),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	--Variable
	Declare @vIDTransaccionVentaGenerada numeric(18,0)
	Declare @vAcreditado money
	Declare @vSaldo money
	Declare @vImporte money
	Declare @vCancelado bit
	
	Set @Mensaje = ''
	Set @Procesado = 'False'
	
	If @Operacion = 'INS' Begin
				
		Declare db_cursor cursor for
		Select  IDTransaccionVentaGenerada,Importe From NotaDebitoVenta 
		Where IDTransaccionVentaGenerada  Is Not Null And IDTransaccionNotaDebito=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVentaGenerada , @vImporte
		While @@FETCH_STATUS = 0 Begin 
		 
			--Hallar Valores
			--Saldo
			Set @vSaldo = (Select Saldo From Venta Where IDTransaccion=@vIDTransaccionVentaGenerada )
			Set @vSaldo = @vSaldo + @vImporte
			
			--Cancelado
			Set @vCancelado = 'False'
			
			If @vSaldo = 0 Begin
				Set @vCancelado = 'True'
			End
			
			--Acreditado
			Set @vAcreditado  = (Select Acreditado From Venta Where IDTransaccion=@vIDTransaccionVentaGenerada )
			Set @vAcreditado  = @vAcreditado  + @vImporte
			
			--Actualizar Venta
			Update Venta Set Saldo=@vSaldo,
							Cancelado = @vCancelado,
							Acreditado=@vAcreditado 
			Where IDTransaccion=@vIDTransaccionVentaGenerada 
			
			Fetch Next From db_cursor Into @vIDTransaccionVentaGenerada , @vImporte
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
		
	If @Operacion = 'ANULAR' Begin
		
		Declare db_cursor cursor for
		Select IDTransaccionVentaGenerada, Importe From NotaDebitoVenta 
		Where IDTransaccionVentaGenerada Is Not Null And IDTransaccionNotaDebito=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVentaGenerada , @vImporte
		While @@FETCH_STATUS = 0 Begin 
		 
			--Hayar Valores
			--Saldo
			Set @vSaldo = (Select Saldo From Venta Where IDTransaccion=@vIDTransaccionVentaGenerada )
			Set @vSaldo = @vSaldo - @vImporte
			
			--Cancelado
			Set @vCancelado = 'False'
			
			--Acreditado
			Set @vAcreditado  = (Select Acreditado From Venta Where IDTransaccion=@vIDTransaccionVentaGenerada )
			Set @vAcreditado  = @vAcreditado  - @vImporte
			
			--Actualizar Venta
			Update Venta Set Saldo=@vSaldo,
							Cancelado = @vCancelado,
							Acreditado=@vAcreditado 							
			Where IDTransaccion=@vIDTransaccionVentaGenerada 
			
			Fetch Next From db_cursor Into @vIDTransaccionVentaGenerada , @vImporte
		End
		
		--Cierra el cursor 
		Close db_cursor   
		Deallocate db_cursor
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		
	End
	
	If @Operacion = 'ELIMINAR' Begin
	
		If (Select Anulado From NotaDebito Where IDTransaccion=@IDTransaccion) = 'True' Begin
			Set @Mensaje = 'Registro guardado'
			Set @Procesado = 'True'
			return @@rowcount
		End
		
	    Declare db_cursor cursor for
		Select IDTransaccionVentaGenerada, Importe From NotaDebitoVenta 
		Where IDTransaccionVentaGenerada Is Not Null And IDTransaccionNotaDebito=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVentaGenerada , @vImporte
		While @@FETCH_STATUS = 0 Begin 
		 
			--Hayar Valores
			--Saldo
			Set @vSaldo = (Select Saldo From Venta Where IDTransaccion=@vIDTransaccionVentaGenerada )
			Set @vSaldo = @vSaldo - @vImporte
			
			--Cancelado
			Set @vCancelado = 'False'
		
			--Acreditado
			Set @vAcreditado  = (Select Acreditado From Venta Where IDTransaccion=@vIDTransaccionVentaGenerada )
			Set @vAcreditado  = @vAcreditado  - @vImporte
			
			--Actualizar Venta
			Update Venta Set Saldo=@vSaldo,
							Cancelado = @vCancelado,
							Acreditado=@vAcreditado 
			Where IDTransaccion=@vIDTransaccionVentaGenerada 
			
			Fetch Next From db_cursor Into @vIDTransaccionVentaGenerada , @vImporte
										
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
	
		
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount
		
	End
	
End


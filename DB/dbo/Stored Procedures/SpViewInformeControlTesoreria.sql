﻿CREATE Procedure [dbo].[SpViewInformeControlTesoreria]

	--Entrada
	@Fecha date,
	@FechaDesde date,
	@FechaHasta date
	
As

Begin
	Declare @vIDMoneda int
	--Crear la tabla
	Create table #InformeFinanciero(
	IDMoneda int,
						Fecha date,
						CobranzaDelDia Money,
						VentaContadoTotal money, 
						VentaContadoEfeChqDia money, --Solo efectivo y cheque al dia
						VentaCancelarAutomatico money,
					    ContadoCobradoDelDia money,
						ContadoCobradoOtros money,
						ContadoNoCobrado money,
						CreditoCobrado money,
						ValoresNoEfectivos money,
						ValoresEfectivos money,
						ChequesDiferidos money
								)
								
	Declare db_cursor1 cursor for
		Select M.ID as IDMoneda
		from Moneda M 
	Open db_cursor1   
	Fetch Next From db_cursor1 Into	@vIDMoneda
	While @@FETCH_STATUS = 0 Begin 
			
			--CobranzaDelDia
			declare @vCobranzaDelDia as money
			set @vCobranzaDelDia = Isnull((select Sum(Valores) from VResumenCobranzaDiariaFormaPago2 
			where fecha = @Fecha
			and IDMoneda = @vIDMoneda
			and TipoComprobante not in ('FAT')),0)

			--Venta contado Total
			declare @vVentaContadoTotal as money
			set @vVentaContadoTotal = Isnull((select sum(VD.total) from VDetalleVentaConDevolucion VD
			Join VVenta V on V.IdTransaccion = VD.IDTransaccion and V.Anulado = 0
			where V.IDMoneda = @vIDMoneda 
			and V.FechaEmision=@Fecha 
			and V.Anulado = 0 
			and V.Credito = 0
			and V.IDFormaPagoFactura in (1,6)
			and V.CancelarAutomatico = 'False'),0)

			--Venta contado Efectivo y Cheque Al dia
			declare @vVentaContadoEfectivoChequeAlDia as money
			set @vVentaContadoEfectivoChequeAlDia = Isnull((select sum(VD.total) from VDetalleVentaConDevolucion VD
			Join VVenta V on V.IdTransaccion = VD.IDTransaccion and V.anulado = 0
			where V.IDMoneda = @vIDMoneda 
			and V.FechaEmision=@Fecha 
			and V.Anulado = 0 
			and V.Credito = 0
			and V.CancelarAutomatico = 'False'
			and V.IDFormaPagoFactura in (1,6)),0)

			--Venta contado Cancelar Automatico
			declare @vVentaCancelarAutomatico as money
			set @vVentaCancelarAutomatico = Isnull((select sum(VD.total) from VDetalleVentaConDevolucion VD
			Join VVenta V on V.IdTransaccion = VD.IDTransaccion and V.anulado = 0
			where V.IDMoneda = @vIDMoneda 
			and V.FechaEmision=@Fecha 
			and V.Anulado = 0 
			and V.Credito = 0
			and V.CancelarAutomatico = 'True'),0)

			--Documentos Cobrados Contado del dia
			declare @vCobradoContadoDelDia money
			set @vCobradoContadoDelDia = Isnull((select sum(Cobrado) from VDetalleDocumentosCobrados 
			where Fecha = @Fecha 
			and IdMoneda = @vIDMoneda 
			and Credito = 0 and FechaFactura = @Fecha),0)

			--Contado de dias anteriores cobrados en la @fecha
			declare @vCobradoContadoOtros as money
			set @vCobradoContadoOtros = Isnull((select sum(Cobrado) from VDetalleDocumentosCobrados 
			where Fecha = @Fecha 
			and IdMoneda = @vIDMoneda 
			and Credito = 0
			and FechaFactura <> @Fecha),0)

			--Contado no cobrado
			declare @CobradoOtroDia money
			declare @ContadoConSaldo money
			declare @vCobradoContadoNoCobrado as money
			set @CobradoOtroDia = Isnull((select sum(Cobrado) from VDetalleDocumentosCobrados 
			where Fecha <> @Fecha 
			and IdMoneda = @vIDMoneda 
			and Credito = 0
			and FechaFactura = @Fecha),0)

			set @ContadoConSaldo = Isnull((select sum(Saldo) from VVenta 
			where FechaEmision = @Fecha 
			and IdMoneda = @vIDMoneda 
			and Credito = 0
			and IDFormaPagoFactura in (1,6)
			and Anulado = 'False'
			and Saldo <> 0),0)

			set @vCobradoContadoNoCobrado = @CobradoOtroDia + @ContadoConSaldo
			--Fin no contado no cobrado

			--Ventas Credito Cobradas del dia
			declare @vCobradoCreditoDelDia money
			set @vCobradoCreditoDelDia = Isnull((select sum(Cobrado) from VDetalleDocumentosCobrados 
			where Fecha = @Fecha 
			and IdMoneda = @vIDMoneda 
			and Credito = 1),0)

			--Valores No Efectivos
			declare @vValoresNoEfectivos as money
			set @vValoresNoEfectivos = Isnull((select Sum(Valores) from VResumenCobranzaDiariaFormaPago2 
			where fecha = @Fecha
			and IDMoneda = @vIDMoneda
			and TipoComprobante not in ('FAT','EFE','CHQ','ACBANCARIA','BDEP')),0)

			--Valores Efectivos
			declare @vValoresEfectivos as money
			set @vValoresEfectivos = Isnull((select Sum(Valores) from VResumenCobranzaDiariaFormaPago2 
			where fecha = @Fecha
			and IDMoneda = @vIDMoneda
			and TipoComprobante in ('EFE','CHQ','ACBANCARIA','BDEP')),0)

			--Cheques Diferidos Vencidos del dia 
			declare @vVencidosDiferidosDelDia as money
			set @vVencidosDiferidosDelDia = Isnull((Select Sum(ImporteMoneda) from vchequeCliente 
			where IDMoneda = @vIDMoneda and FechaVencimiento between @FechaDesde 
			and @FechaHasta and Diferido = 1
			and IDTransaccion not in (select IDTransaccionChequeCliente from DetalleDescuentoCheque)),0)


			Insert Into #InformeFinanciero	
			Values(@vIDMoneda, @Fecha, @vCobranzaDelDia, @vVentaContadoTotal, @vVentaContadoEfectivoChequeAlDia, 
			@vVentaCancelarAutomatico,@vCobradoContadoDelDia,@vCobradoContadoOtros,@vCobradoContadoNoCobrado,
			@vCobradoCreditoDelDia, @vValoresNoEfectivos, @vValoresEfectivos, @vVencidosDiferidosDelDia)
		
Siguiente:
		
		Fetch Next From db_cursor1 Into	@vIDMoneda
		
	End

	--Cierra el cursor
	Close db_cursor1   
	Deallocate db_cursor1	
		
	
	
	Select M.Referencia, M.Descripcion,#InformeFinanciero.* From #InformeFinanciero 
	Join Moneda M on M.ID = #InformeFinanciero.IDMoneda
	where CobranzaDelDia <> 0
	Order by IDMoneda, Fecha
End





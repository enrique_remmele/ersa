﻿CREATE Procedure [dbo].[SpControlInventarioControlarSistema]

	--Entrada
	@IDTransaccion numeric,
	@Operacion varchar(10),
	
	--Transaccion	
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
			
As

Begin

	--BLOQUES
		
	--ACTUALIZAR
	If @Operacion='INS' Begin

		--Validar
		--Si no existe
		If Not Exists(Select * From ControlInventario Where IDTransaccion = @IDTransaccion) Begin
			set @Mensaje = 'El sistema no encuentra el registro seleccionado!'
			set @Procesado = 'False'
			return @@rowcount	
		End
		
		--Si no esta controlado los equipos
		If (Select ControlEquipo From ControlInventario Where IDTransaccion = @IDTransaccion) = 'False' Begin
			set @Mensaje = 'Debe primeramente controlar los equipos de conteo y sus diferencias.!'
			set @Procesado = 'False'
			return @@rowcount	
		End
		
		--Si ya esta controlado
		If (Select ControlSistema From ControlInventario Where IDTransaccion = @IDTransaccion) = 'True' Begin
			set @Mensaje = 'El registro ya esta controlado!'
			set @Procesado = 'False'
			return @@rowcount	
		End
					
		Update ControlInventario Set ControlSistema = 'True', 
										IDUsuarioProcesado=@IDUsuario,
										FechaProcesado=GETDATE()
										
		Where IDTransaccion=@IDTransaccion
			
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount	
		   
	End

	--ANULAR
	If @Operacion='ANULAR' Begin
		
		--Validar
		--Si no existe
		If Not Exists(Select * From ControlInventario Where IDTransaccion = @IDTransaccion) Begin
			set @Mensaje = 'El sistema no encuentra el registro seleccionado!'
			set @Procesado = 'False'
			return @@rowcount	
		End
		
		--Si ya esta controlado
		If (Select ControlEquipo From ControlInventario Where IDTransaccion = @IDTransaccion) = 'False' Begin
			set @Mensaje = 'El registro no esta controlado aun!'
			set @Procesado = 'False'
			return @@rowcount	
		End
		
		Update ControlInventario Set ControlSistema = 'False', 
										IDUsuarioProcesado=NULL,
										FechaProcesado=NULL
										
		Where IDTransaccion=@IDTransaccion
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount	
		
	End
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


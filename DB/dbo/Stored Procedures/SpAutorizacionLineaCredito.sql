﻿CREATE Procedure [dbo].[SpAutorizacionLineaCredito]
	--Entrada
	@IDTransaccionPedido numeric(18,0),
	@Valor varchar(20),
	@IDUsuario int,
	@autorizado bit = 'False' output
As
Begin
	Declare @vIDCliente as Integer = (Select IDCliente from Pedido where IDTransaccion = @IDTransaccionPedido)
	Declare @vSaldo as money = (Select SaldoCredito from vCliente where ID = @vIDCliente)
 
  if @IDTransaccionPedido <> 0 and @valor = 'APROBAR' begin	
		----Actualizar
		Update Pedido 
		Set AutorizacionLineaCredito = 1,
		IDUsuarioAutorizacionLineaCredito = @IDUsuario,
		FechaAutorizacionLineaCredito = getdate(),
		MontoDiferencia = @vSaldo - TOtal
		Where IDTransaccion = @IDTransaccionPedido
		--and (AutorizacionLineaCredito = 'False' or  AutorizacionLineaCredito is null)
		and Facturado = 0
			
  end

  if @IDTransaccionPedido <> 0 and @valor = 'RECHAZAR' begin	
		----Actualizar
		Update Pedido 
		Set AutorizacionLineaCredito = 0,
		IDUsuarioAutorizacionLineaCredito = @IDUsuario,
		FechaAutorizacionLineaCredito =  getdate(),
		MontoDiferencia = 0
		Where IDTransaccion = @IDTransaccionPedido
		--and (AutorizacionLineaCredito = 'True' or AutorizacionLineaCredito is null)
		and Facturado = 0
  end

  if @IDTransaccionPedido <> 0 and @valor = 'RECUPERAR' begin	
		----Actualizar
		Update Pedido 
		Set AutorizacionLineaCredito = null,
		IDUsuarioAutorizacionLineaCredito = null,
		FechaAutorizacionLineaCredito =  null,
		MontoDiferencia = 0
		Where IDTransaccion = @IDTransaccionPedido
		--and (AutorizacionLineaCredito = 'True' or AutorizacionLineaCredito is null)
		and Facturado = 0
  end


  set @autorizado = 'True'
End



﻿
CREATE Procedure [dbo].[SpViewComprobantesNoEmitidos]

	--Entrada
	@Operacion varchar(50),
	@IDPuntoExpedicion int,
	
	@PorNumeracion bit = 'False',
	@NumeracionDesde int = 0,
	@NumeracionHasta int = 0,
	
	@PorFecha bit = 'False',
	@FechaDesde date = NULL,
	@FechaHasta date = NULL
	
As

Begin
	
	Declare @vID tinyint
	Declare @vOperacion varchar(50)
	Declare @vSucursal varchar(50)
	Declare @vPuntoExpedicion varchar(50)
	Declare @vReferencia varchar(50)
	Declare @vTipoComprobante varchar(50)
	Declare @vTimbrado varchar(50)
	Declare @vVencimiento date
	Declare @vDesde int
	Declare @vHasta int
	Declare @vCantidadPorTalonario int
	Declare @vTalonarios int
	Declare @vUltimoExpedido varchar(50)
	Declare @vProximoNumero int
	Declare @vTalonarioActual int
										
	Create Table #ComprobanteNoEmitido(	ID tinyint, 
										Numero varchar(50), 
										IDPuntoExpedicion int,
										Operacion varchar(50),
										Sucursal varchar(50),
										PuntoExpedicion varchar(50),
										Referencia varchar(50),
										TipoComprobante varchar(50),
										Timbrado varchar(50),
										Vencimiento date,
										Desde int,
										Hasta int,
										CantidadPorTalonario int,
										Talonarios int,
										UltimoExpedido varchar(50),
										ProximoNumero int,
										TalonarioActual int)
	
	--Obtener valores
	Select @vSucursal=Suc,
		@vPuntoExpedicion=Descripcion,
		@vReferencia = ReferenciaSucursal + '-' + ReferenciaPunto + '-',
		@vTipoComprobante=TipoComprobante,
		@vOperacion=Operacion,
		@vTimbrado=Timbrado,
		@vVencimiento=Vencimiento,
		@vDesde=NumeracionDesde,
		@vHasta=NumeracionHasta,
		@vCantidadPorTalonario=CantidadPorTalonario,
		@vTalonarios=Talonarios,
		@vUltimoExpedido=UltimoNumeroExpedido,
		@vProximoNumero=ProximoNumero,
		@vTalonarioActual=TalonarioActual
	From VPuntoExpedicion Where ID=@IDPuntoExpedicion									
	
	Set @vID = (Select IsNull(MAX(ID)+1,1) From #ComprobanteNoEmitido)									
	
	If @PorFecha = 'True' Begin
		Set @NumeracionDesde = (Select MIN(NroComprobante) From Venta Where IDPuntoExpedicion=@IDPuntoExpedicion And FechaEmision Between @FechaDesde And @FechaHasta)
		Set @NumeracionHasta = (Select MAX(NroComprobante) From Venta Where IDPuntoExpedicion=@IDPuntoExpedicion And FechaEmision Between @FechaDesde And @FechaHasta)
	End
	
	Declare @vContador int
	Declare @vInsertar bit
    Set @vContador = @NumeracionDesde
    While (@vContador <= @NumeracionHasta) Begin
		
		Set @vInsertar = 'False'
		
		If @Operacion = 'VENTAS CLIENTES' Begin
		
			If @PorNumeracion = 'True' Begin
				If Not Exists(Select * From Venta Where NroComprobante=@vContador And IDPuntoExpedicion=@IDPuntoExpedicion) Begin
					Set @vInsertar = 'True'				
				End
			End
			
			If @PorFecha = 'True' Begin
				If Not Exists(Select * From Venta Where NroComprobante=@vContador And FechaEmision Between @FechaDesde And @FechaHasta And IDPuntoExpedicion=@IDPuntoExpedicion) Begin
					Set @vInsertar = 'True'				
				End
			End			
		End
		
		If @Operacion = 'NOTA CREDITO' Begin
		
			If @PorNumeracion = 'True' Begin
				If Not Exists(Select * From NotaCredito Where NroComprobante=@vContador And IDPuntoExpedicion=@IDPuntoExpedicion) Begin
					Set @vInsertar = 'True'				
				End
			End
			
			If @PorFecha = 'True' Begin
				If Not Exists(Select * From NotaCredito Where NroComprobante=@vContador And Fecha Between @FechaDesde And @FechaHasta And IDPuntoExpedicion=@IDPuntoExpedicion) Begin
					Set @vInsertar = 'True'				
				End
			End			
		End
		
		If @Operacion = '' Begin
			
			If Not Exists(Select * From Venta Where NroComprobante=@vContador) Begin
				Set @vInsertar = 'True'				
			End
			
		End

		If @vInsertar = 'True' Begin
			Insert into #ComprobanteNoEmitido(ID, Numero, Operacion, IDPuntoExpedicion, Referencia, Sucursal, PuntoExpedicion, TipoComprobante, Timbrado, Vencimiento, Desde, Hasta, CantidadPorTalonario, Talonarios, UltimoExpedido, ProximoNumero, TalonarioActual) 
			Values(@vID, @vContador, @vOperacion, @IDPuntoExpedicion, @vReferencia,	 @vSucursal, @vPuntoExpedicion, @vTipoComprobante, @vTimbrado, @vVencimiento, @vDesde, @vHasta, @vCantidadPorTalonario, @vTalonarios, @vUltimoExpedido, @vProximoNumero, @vTalonarioActual)
		End
								
     	Set @vContador = @vContador + 1
    	
    End
    
Salir:

	Select * From #ComprobanteNoEmitido Where ID=@vID
End



﻿CREATE procedure [dbo].[SpImportRoute]

@IDSucursal int

As

Begin

	Select 
	'cdRoute'= 'RT'+Convert(varchar(50),ID),
	'nmRoute'= 'ROUTE'+Convert(Varchar(50),ID)+'_'+'VENDEDOR_'+Convert(varchar(50),ID),
	'cdStatus'= (Case When Estado='True' Then 'ACT'Else (Case When(Select Count(VV.ReferenciaCliente) From VVenta VV Where VV.ReferenciaCliente=V.Referencia)>0 Then 'ACT' Else 'DEACT' End) End)
	From VVendedor V  Where IDSucursal=@IDSucursal

End


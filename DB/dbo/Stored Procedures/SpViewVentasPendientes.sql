﻿CREATE Procedure [dbo].[SpViewVentasPendientes]

	--Entrada
		@IDCliente int,
		@FechaCotizacion date,
		@Contado bit = 0,
		@IDMoneda tinyint
	
As

Begin
If @IDCliente = 0 begin
	Select
	V.IDCliente,
	V.Cliente,
	V.IDTransaccion,
	'Sel'='False',
	V.Comprobante,
	'Tipo'=V.TipoComprobante,
	V.Condicion,
	V.Credito,
	V.FechaEmision,
	V.Fec,
	'Vencimiento'=V.[Fec. Venc.],
	V.FechaVencimiento,
	V.Total,
	V.Cobrado,
	V.Descontado,
	V.Acreditado ,
	V.Saldo,
	'SaldoGs'= 0,
	'Importe'=V.Saldo,
	'ImporteGs'=0,
	'Cancelar'='False',
	V.IDMoneda,
	V.Moneda,
	V.Cotizacion,
	V.IDSucursal,
	V.IDTipoComprobante,
	V.TotalImpuesto,
	'CotizacionHoy'=ISNull((select Cotizacion from Cotizacion where idmoneda = V.IdMOneda and Fecha = @FechaCotizacion),1),
	'IDTransaccionAnticipo' = 0,
	V.NroComprobante

	From VVenta V
	Where V.Cancelado = 'False'
	And V.Anulado='False'
	and V.IDMoneda = @IDMoneda
	and V.Credito != @Contado
	and Case When IDMoneda = 1 then V.FechaEmision else cast(FechaEmision as date) end <= Case When IDMoneda = 1 then V.FechaEmision else cast(@FechaCotizacion as date) end
end else
	Select
	V.IDCliente,
	V.Cliente,
	V.IDTransaccion,
	'Sel'='False',
	V.Comprobante,
	'Tipo'=V.TipoComprobante,
	V.Condicion,
	V.Credito,
	V.FechaEmision,
	V.Fec,
	'Vencimiento'=V.[Fec. Venc.],
	V.FechaVencimiento,
	V.Total,
	V.Cobrado,
	V.Descontado,
	V.Acreditado ,
	V.Saldo,
	'SaldoGs'= 0,
	'Importe'=V.Saldo,
	'ImporteGs'=0,
	'Cancelar'='False',
	V.IDMoneda,
	V.Moneda,
	V.Cotizacion,
	V.IDSucursal,
	V.IDTipoComprobante,
	V.TotalImpuesto,
	'CotizacionHoy'=ISNull((select Cotizacion from Cotizacion where idmoneda = V.IdMOneda and Fecha = @FechaCotizacion),1),
	'IDTransaccionAnticipo' = 0,
	V.NroComprobante

	From VVenta V
	Where V.Cancelado = 'False'
	And V.Anulado='False'
	and V.IDCliente = @IDCliente
	and V.IDMoneda = @IDMoneda
	and V.Credito != @Contado
	and Case When IDMoneda = 1 then V.FechaEmision else cast(FechaEmision as date) end <= Case When IDMoneda = 1 then V.FechaEmision else cast(@FechaCotizacion as date) end
End




﻿CREATE Procedure [dbo].[SpControlSaldosFF]

As

Begin
		Declare @vIDSucursal int
		Declare @vIDGrupo int
		Declare @vSucursal varchar(50)
		Declare @vGrupo varchar(50)
		Declare @vFecha DateTime
		Declare @vTope money
		Declare @vSaldoGasto money
		Declare @vTotalGasto money
		Declare @vCodigo varchar(50)
		Declare @vDebito money
		Declare @vCredito money
		Declare @vSaldoLibroMayor money
		Declare @vControl varchar (50)

	Begin
				
		Declare db_cursor cursor for
		With t1 as(
				Select ff.IDSucursal, ff.IDGrupo, ff.Sucursal, ff.Grupo, 'Tope'=ff.tope  
				  From VFondoFijo ff 
				  ),
			 t2 as(
				Select dff.IDSucursal, dff.IDGrupo, 'TotalGasto'=sum(dff.Total)
		          From VDetalleFondoFijo dff
				 Where dff.Cancelado='False' 
			  Group by dff.IDSucursal, dff.IDGrupo
				  ),
			 t3 as(
				Select x1.IDSucursal,x1.Sucursal, x1.IDGrupo,x1.Grupo, x1.Tope, x2.TotalGasto, 'SaldoGasto'=(x1.tope - x2.totalgasto)  
				  From t1 x1
			 Left Join t2 x2 On x1.IDSucursal = x2.IDSucursal
                   and x1.IDGrupo = x2.IDGrupo
				  ),
		     t4 as(
				--Analisis LibroMayor
				Select ffc.Grupo, lm.codigo, 'Debito'=sum(Debito), 'Credito'=sum(Credito),'SaldoLibroMayor'=(sum(Debito) - sum(Credito))
	              From vLibroMayor lm 
				  Join vFondoFijocuentacontable ffc On lm.Codigo = ffc.Codigo
				  Join grupo gr On ffc.grupo = gr.Descripcion
				 Where lm.descripcion like 'Fondo Fijo%'
                   And lm.fecha < getdate() 
				   And year(lm.fecha) = substring(CONVERT(VARCHAR,getdate(), 112),1,4)
              Group by gr.id,ffc.Grupo,lm.codigo
				)
			Select t3.IdSucursal, t3.IDGrupo, t3.Sucursal, t3.Grupo, 'Fecha'=getdate(),t3.Tope, t3.TotalGasto, t3.SaldoGasto,
                   t4.Codigo, t4.Debito, t4.Credito, t4.SaldoLibroMayor, 
				   Case When t3.SaldoGasto = t4.SaldoLibroMayor Then 'Correcto'
                        When t3.SaldoGasto <> t4.SaldoLibroMayor Then 'Incorrecto'
						When t3.SaldoGasto is null and t4.SaldoLibroMayor is not null Then 'Saldo Nulo'
						When t3.SaldoGasto is not null and t4.SaldoLibroMayor is null Then 'Saldo Nulo'
						Else 'Verificar'
						 End 'Control'
              From t3
              Left Join t4 On t3.grupo = t4.grupo

			--Ingresar al cursor la información
            Open db_cursor   
			Fetch Next From db_cursor Into	@vIDSucursal,@vIDGrupo,@vSucursal,@vGrupo,@vFecha,@vTope,@vTotalGasto,@vSaldoGasto,@vCodigo,@vDebito,@vCredito,@vSaldoLibroMayor,@vControl
			While @@FETCH_STATUS = 0 Begin 

				Insert Into ControlSaldosFF(IDSucursal,IDGrupo,Sucursal,Grupo,Fecha,Tope,TotalGasto,SaldoGasto,Codigo,Debito,Credito,SaldoLibroMayor,Control)
					Values (@vIDSucursal,@vIDGrupo,@vSucursal,@vGrupo,@vFecha,@vTope,@vTotalGasto,@vSaldoGasto,@vCodigo,@vDebito,@vCredito,@vSaldoLibroMayor,@vControl)

		        Fetch Next From db_cursor Into	@vIDSucursal,@vIDGrupo,@vSucursal,@vGrupo,@vFecha,@vTope,@vTotalGasto,@vSaldoGasto,@vCodigo,@vDebito,@vCredito,@vSaldoLibroMayor,@vControl
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		
	End	
End
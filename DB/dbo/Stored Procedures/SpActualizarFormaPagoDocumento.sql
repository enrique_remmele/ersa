﻿CREATE Procedure [dbo].[SpActualizarFormaPagoDocumento]

	--Entrada
	@IDTransaccion int,
	@Comprobante varchar(50),
	@Fecha date,
	@Observacion varchar(150),
	@ID int,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin

			
	--Actualizar 
	Update FormaPagoDocumento Set Comprobante=@Comprobante,
								  Fecha=@Fecha,
								  Observacion =@Observacion 
								  
	Where IDTransaccion=@IDTransaccion And ID=@ID
	
			
	set @Mensaje = 'Registro guardado'
	set @Procesado = 'True'
	return @@rowcount
		

End
	
	


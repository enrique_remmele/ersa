﻿CREATE Procedure [dbo].[SpAgregarNotasdeCreditoAnular]

	--Entrada
	@IDPuntoExpedicion int = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante numeric(18, 0) = NULL,
	@Comprobante varchar(50) = NULL,
	@IDCliente int = NULL,
    @Fecha date = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDDepositoOperacion tinyint = NULL,
	@DescripcionAnulacion varchar (200)= NULL,
	@Observacion varchar(200) = NULL,
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
As

Begin
	
	--Validar Feha Operacion
	If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
		Set @Mensaje  = 'Fecha fuera de rango permitido!!'
		Set @Procesado = 'False'
		set @IDTransaccionSalida  = 0
		Return @@rowcount
	End

	--BLOQUES
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		If Exists(Select * From Venta Where Comprobante=@Comprobante) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Comprobante
		if Exists(Select * From Venta Where IDPuntoExpedicion=@IDPuntoExpedicion And NroComprobante=@NroComprobante) Begin
			set @Mensaje = 'El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Comprobante fuera del Rango
		if @NroComprobante < (Select NumeracionDesde From PuntoExpedicion Where ID=@IDPuntoExpedicion) Or @NroComprobante > (Select NumeracionHasta From PuntoExpedicion Where ID=@IDPuntoExpedicion) Begin
			set @Mensaje = 'El nro de comprobante no esta dentro del rango!Cambie el punto de expedicion o actualicelo.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Agregar Comprobante anulado
		
		Declare @vIDMotivoAnulacionVenta int
		
		Begin
		
			If Exists(Select Descripcion  From MotivoAnulacionVenta Where Descripcion= @DescripcionAnulacion ) Begin
				Set @vIDMotivoAnulacionVenta = (Select ID From MotivoAnulacionVenta Where Descripcion= @DescripcionAnulacion)
			End Else Begin
				
				Set @vIDMotivoAnulacionVenta = (Select ISNULL(Max(ID) + 1, 1) From MotivoAnulacionVenta)
				Insert Into MotivoAnulacionVenta(ID,Descripcion )
				Values(@vIDMotivoAnulacionVenta, @DescripcionAnulacion )
			End
			
		End
		
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--Insertar el Registro de Venta		
		Insert Into NotaCredito  (IDTransaccion, IDPuntoExpedicion, IDTipoComprobante, NroComprobante, IDCliente, IDSucursal, IDDeposito,  Fecha, IDMoneda, Cotizacion, Observacion, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, Saldo, Anulado,FechaAnulado, IDUsuarioAnulado, Procesado, Aplicar, Devolucion, Descuento,ConComprobantes )
		Values(@IDTransaccionSalida, @IDPuntoExpedicion, @IDTipoComprobante, @NroComprobante, @IDCliente,  @IDSucursalOperacion, @IDDepositoOperacion , @Fecha, 1, 1, @Observacion, 0, 0, 0, 0, 0, 'True', NULL, NULL, 'True', 'False' ,'False','False','False' )
 
		--Insertamos el registro de anulacion     
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccionSalida, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		
		
			
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			      
	End
	
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	
	return @@rowcount
		
End








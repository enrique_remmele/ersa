﻿CREATE Procedure [dbo].[SpDevolucionLote]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDTransaccionLote numeric(18,0) = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@Numero int = NULL,
	@IDTipoComprobante smallint = NULL,
	@Comprobante varchar(50) = NULL,	
	@Observacion varchar(100) = NULL,
	@Operacion varchar(10),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
	
As

Begin
	
	--Procesar Saldos de Ventas
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		If Exists(Select * From DevolucionLote Where Numero=@Numero And IDSucursal=@IDSucursal) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Si ya existe el documento
		If Exists(Select * From DevolucionLote Where IDTipoComprobante=@IDTipoComprobante And Comprobante=@Comprobante And IDSucursal=@IDSucursal) Begin
			set @Mensaje = 'El numero de comprobante ya existe!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Insertar Transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT

		--Insertamos el registro
		Insert Into DevolucionLote(IDTransaccion, IDTransaccionLote, IDSucursal, Numero, IDTipoComprobante, Comprobante, Fecha, Anulado, Observacion)
		Values(@IDTransaccionSalida, @IDTransaccionLote, @IDSucursalOperacion, @Numero, @IDTipoComprobante, @Comprobante, GETDATE(), 'False', @Observacion)
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		
		Return @@rowcount
		
	End
	
	If @Operacion = 'ANULAR' Begin
		
		--Si existe
		If Not Exists(Select * From DevolucionLote Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si ya esta anulado
		If (Select Anulado From DevolucionLote Where IDTransaccion=@IDTransaccion) = 'True' Begin
			set @Mensaje = 'El registro ya esta anulado!'
			set @Procesado = 'False'
			return @@rowcount
		End
								
		--Anular
		Update DevolucionLote Set Anulado='True' 								
		Where IDTransaccion=@IDTransaccion
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		--Eliminar las relaciones
		Delete From DetalleDevolucionLote Where IDTransaccion=@IDTransaccion
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		Return @@rowcount
		
	End
	
	If @Operacion = 'DEL' Begin
		
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'false'
		Return @@rowcount
		
	End
		
	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'
	
End


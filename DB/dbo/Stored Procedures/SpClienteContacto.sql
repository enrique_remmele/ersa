﻿
CREATE Procedure [dbo].[SpClienteContacto]

	--Entrada
	@IDCliente int,
	@ID tinyint = NULL,
	@Nombres varchar(50) = NULL,
	@Cargo varchar(50) = NULL,
	@Email varchar(50) = NULL,
	@Telefono varchar(50) = NULL,
	@Cumpleaños date = NULL,
	
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
	
	--Si el ID existe dbs
		if not exists(Select * From Cliente Where ID=@IDCliente) begin
			set @Mensaje = 'El cliente aun no ha sido registrado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vID tinyint
		set @vID = (Select IsNull((Max(ID)+1), 1) From ClienteContacto Where IDCliente=@IDCliente)

		--Insertamos
		Insert Into ClienteContacto(IDCliente,  ID,   Nombres,  Cargo,  Email,  Telefono, Cumpleaños)
		Values (@IDCliente, @vID, @Nombres, @Cargo, @Email, @Telefono, @Cumpleaños)		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From ClienteContacto Where IDCliente=@IDCliente And ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update ClienteContacto Set	Nombres=@Nombres, 
									Cargo=@Cargo,
									Email = @Email,
									Telefono = @Telefono,
									Cumpleaños= @Cumpleaños
		Where ID=@ID And IDCliente=@IDCliente 
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From ClienteContacto Where ID=@ID And IDCliente=@IDCliente ) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From ClienteContacto 
		Where ID=@ID And IDCliente=@IDCliente 
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


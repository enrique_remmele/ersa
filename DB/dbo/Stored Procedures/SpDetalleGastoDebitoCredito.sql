﻿

CREATE Procedure [dbo].[SpDetalleGastoDebitoCredito]

	--Entrada
	@IDTransaccionGasto numeric(18,0) = NULL,
	@IDTransaccionDebitoCrtedito numeric(18,0) = NULL,
	@Operacion varchar(20),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin

	--Validar
		
	--BLOQUES
	If @Operacion='INS' Begin
		
		Insert Into DetalleGastoDebitoCredito(IDTransaccionGasto, IDTransaccionDebitoCredito)
		Values(@IDTransaccionGasto , @IDTransaccionDebitoCrtedito)	
				
		--ACTUALIZAR LA TABLA DEBITO/CREDITO 			
		Update DebitoCreditoBancario Set Facturado='True'
		Where IDTransaccion=@IDTransaccionDebitoCrtedito
		
		Set @Mensaje = 'Registro procesado!'
		Set @Procesado = 'True'
		return @@rowcount	
		
	End
		
		
	If @Operacion='DEL' Begin
		
		print 'Eliminar'
		
		--DESFActurar los Debitos y Creditos Bancarios
		Begin
		
			Declare @vIDTransaccionDebitoCredito numeric(18,0)
			
			Declare db_cursor cursor for
			Select IDTransaccionDebitoCredito From DetalleGastoDebitoCredito Where IDTransaccionGasto=@IDTransaccionGasto
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionDebitoCredito
			While @@FETCH_STATUS = 0 Begin  
			
				
				--Actualizar la Tabla DebitoCredito
				Update DebitoCreditoBancario Set Facturado = 'False'
				Where IDTransaccion=@vIDTransaccionDebitoCredito				
				
				Fetch Next From db_cursor Into @vIDTransaccionDebitoCredito
								
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		End
		
		--Eliminar DetalleGastoDebitoCredito
		Delete From DetalleGastoDebitoCredito Where IDTransaccionGasto=@IDTransaccionGasto
			
		Set @Mensaje = 'Registro procesado!'
		Set @Procesado = 'True'
		return @@rowcount	
		
	End	
		
	
		
End



﻿CREATE Procedure [dbo].[SpRecalcularKardexProducto]
	
	--Entrada
	
	@IDProducto int,
	@FechaInicio Date = null

As

Begin
	
	Set NoCount On

	Declare @vIDKardex int
	Declare @vOrden tinyint
	Declare @vAño smallint
	Declare @vMes tinyint
	Declare @vFecha datetime
	Declare @vIDTransaccion int
	Declare @vIDProducto int
	Declare @vID tinyint
	Declare @vCantidadEntrada decimal(10,2)
	Declare @vCantidadSalida decimal(10,2)
	Declare @vCantidadSaldo decimal(10,2)
	Declare @vCostoEntrada decimal(18,6)
	Declare @vCostoSalida decimal(18,6)
	Declare @vCostoEntradaAnterior decimal(18,6)
	Declare @vCostoSalidaAnterior decimal(18,6)
	Declare @vCostoPromedio decimal(18,6)
	Declare @vTotalEntrada bigint
	Declare @vTotalSalida bigint
	Declare @vTotalSaldo bigint
	Declare @vIDSucursal int
	Declare @vComprobante varchar(50)
	Declare @vIDKardexAnterior int
	Declare @vCantidadSaldoAnterior decimal(10,2)
	Declare @vCostoPromedioAnterior decimal(18,6) = 0
	Declare @vTotalSaldoAnterior Money
	Declare @vCostoPromedioOperacion decimal(18,6)
	Declare @vIDProductoAnterior int = 0
	Declare @vIndice int = 0 
	Declare @vFechaInicio Date
	Declare @vTipo varchar(50)

	if @FechaInicio is null begin 
		Set @vFechaInicio = (Select Max(Fecha) From CierreAperturaStock Where Anulado='False')
	end 
	else begin
		Set @vFechaInicio = @FechaInicio
	end

	If @vFechaInicio Is Null Begin
		Set @vFechaInicio = '20190101'
	End
	print 'asignacion de fecha'
	--Cursor de reordenamiento Indice
	Begin
					
		Set @vIndice = IsNull(((Select top(1) Max(Indice) 
		From vKardex Where IDProducto = @IDProducto and 
		Fecha < @vFechaInicio) + 1),0)
		
		Declare db_cursorIndice cursor for
		Select	IDKardex, IDProducto
		from VKardex
		Where Fecha>=@vFechaInicio And IDProducto=@IDProducto
				
		Order by Fecha Asc, Orden Asc, IDTransaccion Asc--, CantidadEntrada Desc
		--Order by Fecha Asc, Comprobante	Asc, CantidadEntrada Desc
		Open db_cursorIndice
		
		Fetch Next From db_cursorIndice Into @vIDKardex, @vIDProducto
		While @@FETCH_STATUS = 0 Begin 
			
			
			Update Kardex Set Indice=@vIndice
			Where IDKardex=@vIDKardex
			
			Set @vIndice = @vIndice + 1	

			Fetch Next From db_cursorIndice Into @vIDKardex, @vIDProducto
		
		End
		
		--Cierra el cursor
		Close db_cursorIndice   
		Deallocate db_cursorIndice
	
	End
	
	Declare @vRegistros int 
	Declare @vContador int = 1 
	Declare @vPorcentaje decimal(5,2) = 0
	Declare @vPrimero bit = 'True'
	Declare @vCantidadAnterior decimal(18,2) = 0
	Declare @vIDTransaccionCierreStock numeric(18,0)
	
	Begin
		
		Declare db_cursorRecalcularKardexProducto1 cursor for
		Select	
		K.IDKardex, 
		K.Indice, 
		K.Fecha, 
		K.IDTransaccion, 
		K.IDProducto, 
		K.ID, 
		K.Orden, 
		K.IDSucursal, 
		K.Comprobante, 
		K.CantidadEntrada,
		'CostoUnitarioEntrada'=(Case When K.Operacion = 'TICKET DE BASCULA' then isnull(CK.CostoEntrada,K.CostoUnitarioEntrada)  
							else (Case when K.Operacion = 'MACHEO FACTURA TICKET' then isnull(CK.CostoEntrada,K.CostoUnitarioEntrada) 
							else  K.CostoUnitarioEntrada end) end),  
		K.TotalEntrada, 
		K.CantidadSalida, 
		K.CostoUnitarioSalida, 
		K.TotalSalida, 
		K.CantidadSaldo, 
		K.CostoPromedio, 
		K.TotalSaldo, 
		K.CostoPromedioOperacion, 
		isnull(CK.Tipo, '')
		from VKardex K 
		left outer join VCargaKardex CK on K.IDTransaccion = CK.IDTransaccion
		Where Fecha>=@vFechaInicio And K.IDProducto = @IDProducto
		
		
		Order by Indice
		Open db_cursorRecalcularKardexProducto1   
		
		Fetch Next From db_cursorRecalcularKardexProducto1 Into @vIDKardex, @vIndice, @vFecha, @vIDTransaccion, @vIDProducto, @vID, @vOrden, @vIDSucursal, @vComprobante, @vCantidadEntrada, @vCostoEntrada, @vTotalEntrada, @vCantidadSalida, @vCostoSalida, @vTotalSalida, @vCantidadSaldo, @vCostoPromedio, @vTotalSaldo, @vCostoPromedioOperacion, @vTipo
		While @@FETCH_STATUS = 0 Begin 
				
			if @vIDKardex = @vIDKardexAnterior begin
				Goto Siguiente
			end

			print concat(' 139 IDTransaccion ', @vIDTransaccion)
			
			Set @vCostoPromedioOperacion = IsNull(@vCostoEntrada+@vCostoSalida,0)
			print concat('146 ', @vCostoPromedioOperacion)
			Set @vTotalEntrada = Round(isnull(@vCostoEntrada,0) * isnull(@vCantidadEntrada,0),0)
			Set @vTotalSalida = Round(isnull(@vCostoSalida,0) * isnull(@vCantidadSalida,0),0)

			if @vFecha = '20180101' and @vIDPROducto = 3716 begin
				Set @vCostoEntrada = (Select Costo from DetalleProductoCosto where IDProducto = 3716 and IDTransaccion = @vIDTransaccion)
				print @vCostoEntrada
			end

			if isnull(@vCostoEntrada,0) = 0 and isnull(@vCantidadEntrada,0) > 0 and @vIndice > 0 begin
				Set @vCostoEntrada = @vCostoPromedioAnterior
			end
			if isnull(@vCostoSalida,0) = 0 and isnull(@vCantidadSalida,0) > 0 and @vIndice > 0 begin
				Set @vCostoSalida = @vCostoPromedioAnterior
			end

			print concat(@vCostoEntrada, ', ', @vCantidadEntrada , ', ', @vIndice)
			if @vCostoPromedioOperacion < 0 begin
				Set @vCostoPromedioOperacion = @vCostoPromedioOperacion * -1
			end

			IF @vIndice = IsNull(((Select top(1) Max(Indice) From vKardex Where IDProducto = @IDProducto and Fecha < @vFechaInicio) + 1),0)  Begin
				Set @vPrimero = 'True'
			end
			else  begin
				Set @vPrimero = 'False'
			End

			print concat('@vPrimero ',@vPrimero)
			print concat('@vIndice ',@vIndice)
			print concat(' 172 IDTransaccion ', @vIDTransaccion)
			If @vPrimero = 'True' Begin
				print concat(' 174 entra primero IDTransaccion ', @vIDTransaccion)
				--Verificar Ultimo Cierre
				Select Top(1) @vIDTransaccionCierreStock = IDTransaccion,
							  @vCantidadAnterior =  IsNull(Existencia,0),
							  @vCostoPromedioAnterior = IsNull(Costo,0),
							  @vTotalSaldoAnterior = IsNull(Total,0)	 
				From vDetalleCierreReAperturaStock Where IDProducto=@vIDProducto and Fecha>= @vFechaInicio And Anulado='False' Order by Fecha desc

				--Verificar Kardex Anterior
				If @vCantidadAnterior = 0 Begin
					Select Top(1)  @vCantidadAnterior =  IsNull(CantidadSaldo,0),
								  @vCostoPromedioAnterior = IsNull(CostoPromedio,0),
								  @vTotalSaldoAnterior = IsNull(CantidadSaldo,0) * IsNull(CostoPromedio,0)	 
					From Kardex Where IDProducto=@vIDProducto and indice < @vIndice Order by Indice desc
				end

				--Si no existe cierre, y es el primero
				If @vCantidadAnterior = 0 Begin
					print 'no existe cierre'
					if isnull(@vCostoPromedioOperacion,0) = 0 begin
						print 'asignacion de costo'
						Set @vCostoPromedioOperacion = (Select top(1) CostoEntrada from VCargaKardex where Tipo in ('COMPRA', 'AJUSTECOSTO') and CostoEntrada > 0 and IDProducto = @vIDProducto order by FechaEntrada, Orden)
						Set @vCostoEntrada =@vCostoPromedioOperacion
						print concat('Costo Entrada ',@vCostoEntrada)
					end

					Set @vTotalSaldo = (IsNull(@vCantidadEntrada,0)*IsNull(@vCostoEntrada, 0)) - (IsNull(@vCantidadSalida,0)*IsNull(@vCostoSalida, 0)) 
					Set @vCantidadSaldo = IsNull(@vCantidadEntrada,0) - IsNull(@vCantidadSalida,0)
					Set @vCostoPromedio = @vCostoPromedioOperacion
					
					Set @vIDKardexAnterior = @vIDKardex
				End
				
				--Si existe un cierre, se convierte en la primera operacion
				If @vCantidadAnterior > 0 Begin
				
					Set @vCantidadSaldo = (IsNull(@vCantidadAnterior,0)+IsNull(@vCantidadEntrada,0))-(IsNull(@vCantidadSalida,0))
				
					If @vCostoEntrada=0 And @vCostoSalida=0 Begin --si no se pasa costo se pone totalsaldo cero para que al dividir por la cantidad el costo promedio sea 0
						Set @vTotalSaldo = 0
					End 
					Else Begin
						Set @vTotalSaldo = (IsNull((@vTotalSaldoAnterior),0)+IsNull((@vCostoEntrada * @vCantidadEntrada), 0))-(ISNULL(@vCostoSalida * @vCantidadSalida,0)) 	
					End
					
					If @vTipo not in ('TICKET DE BASCULA', 'COMPRA DE MERCADERIA', 'PRODUCCION', 'COMPRA', 'MACHEO FACTURA TICKET')  Begin
						Set @vCostoPromedio = @vCostoPromedioAnterior
					end

					Set @vIDKardexAnterior = @vIDKardex

				End
				
				Set @vPrimero = 'False'


				If @vCantidadEntrada = 0 and @vCantidadSalida=0 and @vTipo = 'MACHEO FACTURA TICKET' begin
						
						print 'Entro PRIMERO a calcular macheo factura ticket / ajuste de contrato'
						print concat('@vCantidadSaldo: ',@vCantidadSaldo)
						print concat('@vTotalSaldoAnterior: ',@vTotalSaldoAnterior)
						print concat('@vCantidadSaldo: ',@vTotalEntrada)
						print concat('@vCantidadSaldo: ',@vTotalSalida)
						print concat('@vCostoPromedio: ',@vCostoPromedio)
						
						Set @vCantidadSaldo = (IsNull(@vCantidadSaldo,0))
						Set @vTotalEntrada = (IsNull(@vCostoEntrada, 0))
						Set @vTotalSalida = (IsNull(@vCostoSalida, 0))
						Set @vTotalSaldo = (IsNull((@vTotalSaldoAnterior),0)+IsNull((@vCostoEntrada), 0))-(ISNULL(@vCostoSalida,0)) 	
						Set @vCostoPromedio = @vTotalSaldo/@vCantidadSaldo

						print concat('promedia MACHEO', @vIDKardex, ' ', @vCostoPromedio, ' Cantidad = ', @vCantidadSaldoAnterior)
						print 'Entro a calcular macheo factura ticket / ajuste de contrato'
						print concat('@vCantidadSaldo: ',@vCantidadSaldo)
						print concat('@vTotalSaldoAnterior: ',@vTotalSaldoAnterior)
						print concat('@vCantidadSaldo: ',@vTotalEntrada)
						print concat('@vCantidadSaldo: ',@vTotalSalida)
						print concat('@vCostoPromedio: ',@vCostoPromedio)
						
						GoTo Actualizar
						
					end



				print concat(' 228 va a actualizar IDTransaccion ', @vIDTransaccion)
				GoTo Actualizar

			End
			print concat('*******************************TIPO**********************',@vtipo )
			--Si NO es compra, tomar el costo anterior
			
			If @vTipo not in ('TICKET DE BASCULA', 'COMPRA DE MERCADERIA', 'PRODUCCION', 'COMPRA','MACHEO FACTURA TICKET')  Begin
				print concat(' 235 IDTransaccion ', @vIDTransaccion)
				print concat('Cambia el costo ', @vtipo)
				If @vTipo in ('PRODUCTO-COSTO','AJUSTECOSTO') begin
					Set @vCostoPromedioAnterior = @vCostoEntrada + @vCostoSalida 
				end
				--En caso de que el costo promedio anterior sea cero, se toma el costo de la operacion.
				If @vCostoPromedioAnterior = 0 Begin
					Set @vCostoPromedioAnterior = @vCostoEntrada + @vCostoSalida 
				End

				If isnull(@vCostoEntrada,0) <> 0 and isnull(@vCostoSalida,0) = 0 begin
					Set @vCostoEntrada = @vCostoPromedioAnterior
				end

				If isnull(@vCostoSalida,0) <> 0 and isnull(@vCostoEntrada,0) = 0 begin
					Set @vCostoSalida = @vCostoPromedioAnterior
				end

				If @vTipo in ('PRODUCTO-COSTO','AJUSTECOSTO') begin
					Set @vCantidadEntrada = 0
					Set @vCantidadSalida = 0
				end


				Set @vCostoPromedioOperacion = @vCostoPromedioAnterior
				Set @vCostoPromedio = @vCostoPromedioAnterior
				print concat(' 261 IDTransaccion ', @vIDTransaccion)
				Set @vTotalEntrada = Round(isnull(Round(@vCostoEntrada,4),0) * isnull(@vCantidadEntrada,0),0)
				Set @vTotalSalida = Round(isnull(Round(@vCostoSalida,4),0) * isnull(@vCantidadSalida,0),0)
				
				Set @vCantidadSaldo = @vCantidadSaldoAnterior+@vCantidadEntrada-@vCantidadSalida								
				Set @vTotalSaldo = @vTotalSaldoAnterior + (IsNull(@vCantidadEntrada,0)*IsNull(@vCostoEntrada, 0)) - (IsNull(@vCantidadSalida,0)*IsNull(@vCostoSalida, 0)) 
				print concat('Transaccion ', @vIDTransaccion)
				print concat('no promedia ', @vIDKardex, ' ', @vCostoPromedio)
				print concat('@vCantidadSaldo: ',@vCantidadSaldo, ' incluye: ', @vCantidadSaldoAnterior , '+', @vCantidadEntrada, '-', @vCantidadSalida)
				print concat('@vTotalSaldo: ',@vTotalSaldo, ' incluye: ', @vTotalSaldoAnterior , '+', (IsNull(@vCantidadEntrada,0)*IsNull(@vCostoEntrada, 0)), '-', (IsNull(@vCantidadSalida,0)*IsNull(@vCostoSalida, 0)))
				print concat('@vTotalSaldoAnterior: ',@vTotalSaldoAnterior)
				print concat('@@vTotalEntrada: ',@vTotalEntrada)
				print concat('@@vTotalSalida: ',@vTotalSalida)
				print concat('@vCostoPromedio: ',@vCostoPromedio)

				If @vTipo in ('PRODUCTO-COSTO','AJUSTECOSTO') begin
					Set @vTotalSaldo = Round(@vCantidadSaldo * @vCostoEntrada,0)
					Set @vCostoPromedio = @vCostoEntrada
					Set @vCostoPromedioOperacion = @vCostoEntrada
					print 'producto costo'
					print @vCostoEntrada


					GoTo Actualizar
				end
				GoTo Actualizar
			End
			
			-- Si ES compra o ticket calcula el costo promedio
			else begin

				if ISNULL(@vCantidadSalida,0) <> 0 begin
					Set @vCostoPromedio = @vCostoPromedioAnterior
					
				end
				else begin
					-- Si es compra y la cantidad anterior es menor o igual a cero NO PROMEDIA
					If  @vCantidadSaldoAnterior <= 0 And (@vTipo = 'COMPRA' or @vTipo = 'COMPRA DE MERCADERIA' or @vTipo = 'TICKET DE BASCULA' or @vTipo = 'PRODUCCION') Begin
						--Set @vCantidadSaldo = (IsNull(@vCantidadEntrada,0))+(IsNull(@vCantidadSalida,0))
						Set @vCantidadSaldo = (IsNull(@vCantidadSaldoAnterior,0)+IsNull(@vCantidadEntrada,0))-(IsNull(@vCantidadSalida,0))
						print 'no recalcular cantidad saldo anterior <0'
						print concat('@vCantidadSaldo: ',@vCantidadSaldo)
						print concat('@vTotalSaldoAnterior: ',@vTotalSaldoAnterior)
						print concat('@vTotalEntrada: ',@vTotalEntrada)
						print concat('@vTotalSalida: ',@vTotalSalida)
						print concat('@vCostoPromedio: ',@vCostoPromedio)

						Set @vTotalSaldo = @vTotalSaldoAnterior + (IsNull(@vCantidadEntrada,0)*IsNull(@vCostoEntrada, 0)) - (IsNull(@vCantidadSalida,0)*IsNull(@vCostoSalida, 0)) 
						Set @vCostoPromedio = @vCostoPromedioOperacion
						print concat('no promedia COMPRA', @vIDKardex, ' ', @vCostoPromedio, ' Cantidad = ', @vCantidadSaldoAnterior)
						GoTo Actualizar
						
					End
					

					If @vCantidadEntrada = 0 and @vCantidadSalida=0 and @vTipo = 'MACHEO FACTURA TICKET' begin
						
						Set @vCantidadSaldo = (IsNull(@vCantidadSaldoAnterior,0))
						print 'Entro a calcular macheo factura ticket / ajuste de contrato'
						print concat('@vCantidadSaldo: ',@vCantidadSaldo)
						print concat('@vTotalSaldoAnterior: ',@vTotalSaldoAnterior)
						print concat('@vCantidadSaldo: ',@vTotalEntrada)
						print concat('@vCantidadSaldo: ',@vTotalSalida)
						print concat('@vCostoPromedio: ',@vCostoPromedio)

						Set @vTotalSaldo = @vTotalSaldoAnterior + (IsNull(@vCostoEntrada, 0)) -(IsNull(@vCostoSalida, 0)) 
						Set @vCostoPromedio = @vTotalSaldo/@vCantidadSaldoAnterior
						Set @vTotalEntrada = (IsNull(@vCostoEntrada, 0))
						Set @vTotalSalida = (IsNull(@vCostoSalida, 0))

						print concat('promedia MACHEO', @vIDKardex, ' ', @vCostoPromedio, ' Cantidad = ', @vCantidadSaldoAnterior)
						print 'Entro a calcular macheo factura ticket / ajuste de contrato'
						print concat('@vCantidadSaldo: ',@vCantidadSaldo)
						print concat('@vTotalSaldoAnterior: ',@vTotalSaldoAnterior)
						print concat('@vCantidadSaldo: ',@vTotalEntrada)
						print concat('@vCantidadSaldo: ',@vTotalSalida)
						print concat('@vCostoPromedio: ',@vCostoPromedio)
						
						GoTo Actualizar
						
					end

					Set @vCantidadSaldo = (IsNull(@vCantidadSaldoAnterior,0)+IsNull(@vCantidadEntrada,0))-(IsNull(@vCantidadSalida,0))
					print concat('@vCantidadSaldo ', @vCantidadSaldo)
					Set @vTotalSaldo = isnull(@vTotalSaldoAnterior,0) + (IsNull(@vCantidadEntrada,0)*IsNull(@vCostoEntrada, 0)) - (IsNull(@vCantidadSalida,0)*IsNull(@vCostoSalida, 0)) 
					print concat('@vTotalSaldo ', @vTotalSaldo)
					if @vCantidadSaldo <=0 or @vTotalSaldo <=0 begin
						print concat('@vCostoPromedio = @vCostoPromedioOperacion ', @vCostoPromedioOperacion) 
						Set @vCostoPromedio = @vCostoPromedioOperacion
					end
					else begin
						print concat('@vTotalSaldo/@vCantidadSaldo ', @vTotalSaldo/@vCantidadSaldo)
						Set @vCostoPromedio = @vTotalSaldo/@vCantidadSaldo
					end
					print concat('@vCostoPromedio ', @vCostoPromedio)
					print 'recalcular'
						print concat('@vCantidadSaldo: ',@vCantidadSaldo, ' incluye: ', @vCantidadSaldoAnterior , '+', @vCantidadEntrada, '-', @vCantidadSalida)
						print concat('@vTotalSaldo: ',@vTotalSaldo, ' incluye: ', @vTotalSaldoAnterior , '+', @vTotalEntrada, '-', @vTotalSalida)
						print concat('@vTotalSaldoAnterior: ',@vTotalSaldoAnterior)
						print concat('@@vTotalEntrada: ',@vTotalEntrada)
						print concat('@@vTotalSalida: ',@vTotalSalida)
						print concat('@vCostoPromedio: ',@vCostoPromedio)

					print concat('promedia ', @vIDKardex, ' ', @vCostoPromedio)
				end
			end
				
Actualizar:
			
			--Actualizar Kardex
			Update Kardex
			Set CostoUnitarioSalida = @vCostoSalida,
				CostoUnitarioEntrada = @vCostoEntrada,
				CantidadSaldo = @vCantidadSaldo,
				TotalEntrada = @vTotalEntrada,
				TotalSalida = @vTotalSalida,
				TotalSaldo = @vTotalSaldo,
				IDKardexAnterior=@vIDKardexAnterior,
				CostoPromedio=@vCostoPromedio,
				CostoPromedioOperacion=@vCostoPromedioOperacion
			Where IDKardex = @vIDKardex
			Print Concat('Total Entrada ',@vTotalEntrada)
			Print Concat('Total Salida ',@vTotalSalida)
			--Fija el IDKardex actual como IDKardexAnterior
			Set @vIDKardexAnterior = @vIDKardex
			
			Set @vIDProductoAnterior = @vIDProducto
			
			IF @vCantidadSaldo > 0 Begin
				Set @vCostoPromedioAnterior=@vCostoPromedio
			end else begin
				Set @vCostoPromedioAnterior=@vCostoPromedioOperacion
			End
			
			Set @vCostoEntradaAnterior = @vCostoPromedioAnterior
			Set @vCostoSalidaAnterior = @vCostoPromedioAnterior
			
			Set @vCantidadSaldoAnterior = @vCantidadSaldo
			Set @vTotalSaldoAnterior = @vTotalSaldo
Siguiente:		

			Fetch Next From db_cursorRecalcularKardexProducto1 Into @vIDKardex, @vIndice, @vFecha, @vIDTransaccion, @vIDProducto, @vID, @vOrden, @vIDSucursal, @vComprobante, @vCantidadEntrada, @vCostoEntrada, @vTotalEntrada, @vCantidadSalida, @vCostoSalida, @vTotalSalida, @vCantidadSaldo, @vCostoPromedio, @vTotalSaldo, @vCostoPromedioOperacion, @vTipo
		
		End
		
		--Cierra el cursor
		Close db_cursorRecalcularKardexProducto1   
		Deallocate db_cursorRecalcularKardexProducto1
		
	End

End


﻿CREATE Procedure [dbo].[spProductoModificarPrecio]

	--Entrada
	@IDProducto int,
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From ProductoModificarPrecio Where IDProducto = @IDProducto) begin
			set @Mensaje = 'El Producto ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end


		--Insertamos
		Insert Into ProductoModificarPrecio(IDProducto, Estado)
		Values(@IDProducto,@Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PRODUCTOMODIFICARPRECIO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
				 		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From ProductoModificarPrecio Where IDProducto = @IDProducto) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
				
		--Actualizamos
		Update ProductoModificarPrecio Set	Estado =@Estado
		Where  IDProducto = @IDProducto
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PRODUCTOMODIFICARPRECIO', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		If not exists(Select * From ProductoModificarPrecio Where IDProducto = @IDProducto) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From ProductoModificarPrecio 
		Where IDProducto = @IDProducto
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PRODUCTOMODIFICARPRECIO', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


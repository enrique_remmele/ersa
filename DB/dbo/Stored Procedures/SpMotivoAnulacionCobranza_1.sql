﻿Create Procedure [dbo].[SpMotivoAnulacionCobranza]

	--Entrada
	@ID int=NULL,
	@Descripcion varchar(50)=NULL,
	@Estado bit=NULL,
	
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From MotivoAnulacionCobranza Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDMotivoAnulacionCobranza int
		set @vIDMotivoAnulacionCobranza = (Select IsNull((Max(ID)+1), 1) From MotivoAnulacionCobranza)

		--Insertamos
		Insert Into MotivoAnulacionCobranza(ID, Descripcion, Estado)
		Values(@vIDMotivoAnulacionCobranza, @Descripcion, @Estado)	
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='MOTIVOANULACIONCOBRANZA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
					
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From MotivoAnulacionCobranza Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From MotivoAnulacionCobranza Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update MotivoAnulacionCobranza Set Descripcion=@Descripcion, Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='MOTIVOANULACIONCOBRANZA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
					
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From MotivoAnulacionCobranza Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con
		--if exists(Select * From Cobranza Where IDMotivo=@ID) begin
		--	set @Mensaje = 'El registro tiene movimientos asociados! No se puede eliminar.'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--end
		
		--Eliminamos
		Delete From MotivoAnulacionCobranza 
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='MOTIVOANULACIONCOBRANZA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
					
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End

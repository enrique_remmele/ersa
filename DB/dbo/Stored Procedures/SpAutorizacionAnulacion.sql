﻿CREATE Procedure [dbo].[SpAutorizacionAnulacion]


	--Entrada
	@IDTransaccion int,
	@Operacion varchar(10),
	@IDUsuario int,
	@IDMotivo int,

	--Transaccion
	@IDSucursal int,
    @IDDeposito int,
    @IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) = '' output,
	@Procesado bit = 0 output
As

Begin
		
	Declare @vDocumento varchar(50)
	Declare @vIDOperacion int
	Declare @vFechaTransaccion Date

	--IDTransaccion
	If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
		set @Mensaje = 'El sistema no encontro el registro solicitado.'
		set @Procesado = 'False'
		goto Salir
	End
	
	Set @vIDOperacion = (Select top(1) IDOperacion from Transaccion where id = @IDTransaccion)
	Set @vDocumento = (Select top(1) Descripcion from Operacion where ID = @vIDOperacion)
	--print 'Difine operacion y documento'

	if @Operacion = 'SOLICITAR' begin
		
		set @Mensaje = (Select dbo.FValidarAnulacionDocumento(@IDTransaccion, @Operacion, @IDUsuario, @IDMotivo, @IDSucursal,@IDDeposito,@IDTerminal))
		
		if exists (select * from SolicitudAnulacion where IDTransaccion = @IDTransaccion AND Estado IS NULL) begin
			set @Mensaje = 'El documento ya tiene una solicitud de anulacion PENDIENTE'
		end

		if @Mensaje <> 'OK' begin
			set @Procesado = 'False'
			goto Salir
		end
		
		--Asientos Cerrados
		If Exists(Select * From Asiento A Where A.IDTransaccion=@IDTransaccion And A.Conciliado='True') Begin
			set @Mensaje = 'El asiento de este documento ya esta conciliado! No se puede anular.'
			set @Procesado = 'False'
			goto Salir
		End
		--SM, faltaban campos
		Insert Into SolicitudAnulacion(IDTransaccion, IDOperacion, IDMotivoAnulacion, IDUsuarioSolicitante, FechaSolicitud, IDUsuarioAprobacion, FechaAprobacion,  Estado)
								Values(@IDTransaccion, @vIDOperacion, @IDMotivo, @IDUsuario, GETDATE(), @IDUsuario, GETDATE(), null)		
	
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		goto Salir
	end

	if @vDocumento = 'VENTAS CLIENTES' begin
		--print 'fecha venta'
		Set @vFechaTransaccion = (select top(1) FechaEmision from Venta where idtransaccion = @IDTransaccion)
	end

	if @vDocumento = 'NOTA CREDITO' begin
		--print 'fecha nc'
		Set @vFechaTransaccion = (select top(1) Fecha from NotaCredito where idtransaccion = @IDTransaccion)
	end

	--print 'Verifica permisos de usuario'
	--Verifica permisos de usuario
	if exists(select * from UsuarioAutorizarAnulacion where IDUsuario = @IDUsuario) begin
		if Month(@vFechaTransaccion) < MONTH(GETDATE()) begin
			If (Select PeriodoFueraDelMes from vUsuarioAutorizarAnulacion where IDUsuario = @IDUsuario) = 0 begin
				set @Mensaje = 'No tiene permisos para realizar esta operacion.'
				set @Procesado = 'False'
				goto Salir
			end
		end
	end
	else begin
		set @Mensaje = 'No tiene permisos para realizar esta operacion.'
		set @Procesado = 'False'
		goto Salir
	end


	if @Operacion = 'APROBAR' begin
		--print 'APROBAR'
		if not exists(select * from SolicitudAnulacion where IDTransaccion = @IDTransaccion) begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			goto Salir
		end
		
		if exists(select * from SolicitudAnulacion where IDTransaccion = @IDTransaccion and Estado = 1) begin
			set @Mensaje = 'El documento ya esta Anulado'
			set @Procesado = 'False'
			goto Salir
		end
		
		set @Mensaje = (Select dbo.FValidarAnulacionDocumento(@IDTransaccion, @Operacion, @IDUsuario, @IDMotivo, @IDSucursal,@IDDeposito,@IDTerminal))
		if @Mensaje <> 'OK' begin
			set @Procesado = 'False'
			goto Salir
		end

		Declare @vIDUsuarioSolicitante int = (select IDUsuarioSolicitante from SolicitudAnulacion where IDTransaccion = @IDTransaccion)

		if @vDocumento = 'VENTAS CLIENTES' begin

			Exec SpVenta
			@IDTransaccion=@IDTransaccion
			,@Operacion='ANULAR'
			,@IDMotivo=@IDMotivo
			,@IDUsuario=@vIDUsuarioSolicitante
			,@IDSucursal=@IDSucursal
			,@IDDeposito=@IDDeposito
			,@IDTerminal=@IDTerminal
			,@Mensaje=''
			,@Procesado='False'
			,@IDTransaccionSalida='0'
			,@SolicitudAprobada=1

			Update SolicitudAnulacion
			Set Estado = 1,
			IDUsuarioAprobacion= @IDUsuario,
			FechaAprobacion = GETDATE()
			Where IDTransaccion = @IDTransaccion

			Set @Mensaje = 'Registro Actualizado'
			Set @Procesado = 'True'
			goto Salir

		end

		if @vDocumento = 'NOTA CREDITO' begin
		
			Exec SpNotaCredito
			@IDTransaccion=@IDTransaccion
			,@Operacion='ANULAR'
			,@IDOperacion='5'
			,@IDUsuario=@vIDUsuarioSolicitante
			,@IDSucursal=@IDSucursal
			,@IDDeposito=@IDDeposito
			,@IDTerminal=@IDTerminal
			,@IDMotivo=@IDMotivo
			,@Mensaje=''
			,@Procesado='False'
			,@IDTransaccionSalida='0'
			,@SolicitudAprobada=1

			Update SolicitudAnulacion
			Set Estado = 1,
			IDUsuarioAprobacion= @IDUsuario,
			FechaAprobacion = GETDATE()
			Where IDTransaccion = @IDTransaccion

			Set @Mensaje = 'Registro Actualizado'
			Set @Procesado = 'True'
			goto Salir
		end

		Set @Mensaje = 'No se realizo ninguna operacion'
		Set @Procesado = 'False'
		goto Salir

	end

	if @Operacion = 'RECHAZAR' begin
		--print 'RECHAZAR'
		if not exists(select * from SolicitudAnulacion where IDTransaccion = @IDTransaccion) begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		if exists(select * from SolicitudAnulacion where IDTransaccion = @IDTransaccion and Estado = 1) begin
			set @Mensaje = 'El documento ya esta Anulado'
			set @Procesado = 'False'
			goto Salir
		end

		Update SolicitudAnulacion
		Set Estado = 0,
		IDUsuarioAprobacion= @IDUsuario,
		FechaAprobacion = GETDATE()
		Where IDTransaccion = @IDTransaccion

		Set @Mensaje = 'Registro Actualizado'
		Set @Procesado = 'True'
		goto Salir


	end
	
	if @Operacion = 'RECUPERAR' begin
		--print 'RECUPERAR'
		if not exists(select * from SolicitudAnulacion where IDTransaccion = @IDTransaccion) begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			goto Salir
		end

		if exists(select * from SolicitudAnulacion where IDTransaccion = @IDTransaccion and Estado = 1) begin
			set @Mensaje = 'El documento ya esta Anulado, no se puede recuperar'
			set @Procesado = 'False'
			goto Salir
		end

		Update SolicitudAnulacion
		Set Estado = null,
		IDUsuarioAprobacion= null,
		FechaAprobacion = null
		Where IDTransaccion = @IDTransaccion

		Set @Mensaje = 'Registro Actualizado'
		Set @Procesado = 'True'
		goto Salir

	end

Salir:
	--print 'Salir'
	Select 'Mensaje' = isnull(@Mensaje,''), 'Procesado' = isnull(@Procesado, 'False'), 'Operacion'= Isnull(@vIDOperacion,0), 'Documento'= isnull(@vDocumento,'')
End


﻿CREATE Procedure [dbo].[SpProductoListaPrecioExcepciones]


	--Entrada
	@IDProducto int = NULL,
	@IDListaPrecio tinyint = NULL,
	@IDCliente int = NULL,
	@IDTipoDescuento tinyint = NULL,
	@ImporteDescuento money = 0,
	@Desde date = NULL,
	@Hasta date = NULL,
	@Cantidad int = 0,
	@Operacion varchar(10),
	
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint = null,
	@IDTerminal int = null,
	
	--Salida
	@Mensaje varchar(200) = null output,
	@Procesado bit  = Null output
As

Begin

	--BLOQUES
	Declare @Precio decimal(18,6)
	Declare @vPorcentaje decimal(9,6)
	Declare @vIDMoneda tinyint
	Declare @vCantidadSaldo int
	Declare @vIDTipoProducto int 
	Declare @vUnicaVez bit = 0

	Set @vIDTipoProducto = (select IDTipoProducto from producto where ID = @IDProducto)
	if @vIDTipoProducto = 1 begin
		Set @vUnicaVez = 1
	end
	--Obtener Sucursal de la lista de precio, porque el sistema le tira la sucursal de donde se opera
	Set @IDSucursal = (Select top(1) IDSucursal from ListaPrecio where ID =  @IDListaPrecio)
	
	--Obtener el precio unitario
	Set @vIDMoneda = (Select top(1) isnull(Min(IDMoneda),0) From ProductoListaPrecio Where IDListaPrecio=@IDListaPrecio And IDProducto=@IDProducto)
	Set @Precio = (Select Top(1) isnull(Precio,0) From ProductoListaPrecio Where IDListaPrecio=@IDListaPrecio And IDProducto=@IDProducto And IDMoneda=@vIDMoneda)
	
	--If @Porcentaje Is Not Null Begin
	--	Set @vDescuento = Round((@Precio * @Porcentaje) / 100, 0, 0)
	--End
	
	Set @vPorcentaje = Round((@ImporteDescuento / @Precio) * 100, 6)

	if @Operacion <> 'INS' begin
		set @vCantidadSaldo = Isnull((select top(1) CantidadLimiteSaldo from ProductoListaPrecioExcepciones Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDListaPrecio=@IDListaPrecio And IDMoneda=@vIDMoneda and IDTipoDescuento=@IDTipoDescuento and IDSucursal = @IDSucursal order by desde desc),0)
	end


	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la configuracion ya existe
		if exists(Select * From ProductoListaPrecioExcepciones Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDListaPrecio=@IDListaPrecio And IDSucursal=@IDSucursal And IDTipoDescuento=@IDTipoDescuento and Hasta >= @Desde AND IDTransaccionPedido = 0) begin
			set @Mensaje = concat('La configuracion ya existe!', '|',@IDProducto, '|',@IDCliente,'|',@IDListaPrecio, '|', @IDSucursal,'|',@IDTipoDescuento)
			set @Procesado = 'False'
			return @@rowcount
		end
		if (@vIDMoneda = 0)  or (@Precio = 0) begin
			set @Mensaje = 'No existe relacion entre este producto y esta lista de precios!'
			set @Procesado = 'False'
			return @@rowcount
		end
		--si es que los montos son 0
		if @vPorcentaje = 0 Begin
			set @Mensaje = 'El descuento no puede ser 0!'
			set @Procesado = 'False'
			return @@rowcount
		End
					  
		--Delete from ProductoListaPrecioExcepciones where IDProducto = @IDProducto and IDCliente = @IDCliente and IDTransaccionPedido=0
		--Insertamos
		Insert Into ProductoListaPrecioExcepciones(IDProducto, IDCliente, IDListaPrecio, IDMoneda, IDSucursal, IDTipoDescuento, Descuento, Porcentaje, Desde, Hasta, CantidadLimite, CantidadLimiteSaldo, UnicaVez, IDTransaccionPedido)
		Values(@IDProducto, @IDCliente, @IDListaPrecio, @vIDMoneda, @IDSucursal, @IDTipoDescuento, @ImporteDescuento, @vPorcentaje, @Desde, @Hasta, @Cantidad,0,@vUnicaVez,0)		

		--insertar auditoria
		Insert Into aProductoListaPrecioExcepciones(IDProducto, IDCliente, IDListaPrecio, IDMoneda, IDSucursal, IDTipoDescuento, Descuento, Porcentaje, Desde, Hasta, CantidadLimite, CantidadLimiteSaldo, UnicaVez, IdUsuario, FechaModificacion, accion)
		Values(@IDProducto, @IDCliente, @IDListaPrecio, @vIDMoneda, @IDSucursal, @IDTipoDescuento, @ImporteDescuento, @vPorcentaje, @Desde, @Hasta, @Cantidad,0,@vUnicaVez, @IDUsuario, GETDATE(),'INS')		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si es que la configuracion ya existe
		--if Not exists(Select * From ProductoListaPrecioExcepciones Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDListaPrecio=@IDListaPrecio And IDSucursal=@IDSucursal And IDTipoDescuento=@IDTipoDescuento) begin
		if Not exists(Select * From ProductoListaPrecioExcepciones Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDListaPrecio=@IDListaPrecio And IDTipoDescuento=@IDTipoDescuento AND IDTransaccionPedido = 0) begin
			set @Mensaje = 'La configuracion no existe!'
			set @Procesado = 'False'
			return @@rowcount
		end

		--si es que los montos son 0
		if @vPorcentaje = 0 Begin
			set @Mensaje = 'El descuento no puede ser 0!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Actualizar
		Update ProductoListaPrecioExcepciones Set Porcentaje=@vPorcentaje,
											Descuento=@ImporteDescuento,
											IDTipoDescuento=@IDTipoDescuento,
											Desde=@Desde,
											Hasta=@Hasta,
											CantidadLimite=@Cantidad
		Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio 
		And IDCliente=@IDCliente And IDMoneda=@vIDMoneda and IDTipoDescuento=@IDTipoDescuento and Desde= @Desde

		--Insertar auditoria despues de actualizar
		Insert Into aProductoListaPrecioExcepciones(IDProducto, IDCliente, IDListaPrecio, IDMoneda, IDSucursal, IDTipoDescuento, Descuento, Porcentaje, Desde, Hasta, CantidadLimite, CantidadLimiteSaldo, IdUsuario, FechaModificacion, accion)
		Values(@IDProducto, @IDCliente, @IDListaPrecio, @vIDMoneda, @IDSucursal, @IDTipoDescuento, @ImporteDescuento, @vPorcentaje, @Desde, @Hasta, @Cantidad, @vCantidadSaldo, @IDUsuario, GETDATE(),'UPD')		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		print 'entro'
		if Not Exists(Select * From ProductoListaPrecioExcepciones Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDListaPrecio=@IDListaPrecio and IDTipoDescuento=@IDTipoDescuento) begin
			set @Mensaje = 'El registro no existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		Declare @vListaPrecio varchar(50)
		Set @vListaPrecio = (Select top(1) Descripcion from ListaPrecio where id = @IDListaPrecio)
		Declare @vIDTransaccionPedido int

		Declare @vIDListaPrecio int
		Declare @vIDSucursal decimal(18,6)
		Declare cCFTranferencia cursor for
			select 
			IDListaPrecio,
			IDSucursal
			
			from VProductoListaPrecioExcepciones 
			Where ListaPrecio = @vListaPrecio
			and IDTipoDescuento=@IDTipoDescuento
			and IDProducto = @IDProducto
			and IDCliente = @IDCliente
			
			Open cCFTranferencia
			fetch next from cCFTranferencia into @vIDListaPrecio, @vIDSucursal
		
			While @@FETCH_STATUS = 0 Begin  
				--Insertar auditoria antes de eliminar
				set @ImporteDescuento = ISNUll((select Top(1) Descuento from ProductoListaPrecioExcepciones Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDListaPrecio=@vIDListaPrecio And IDMoneda=@vIDMoneda and IDTipoDescuento=@IDTipoDescuento and Desde= @Desde and IDSucursal = @vIDSucursal),0)
				set @vPorcentaje = ISNUll((select Top(1) Porcentaje from ProductoListaPrecioExcepciones Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDListaPrecio=@vIDListaPrecio And IDMoneda=@vIDMoneda and IDTipoDescuento=@IDTipoDescuento and Desde= @Desde and IDSucursal = @vIDSucursal),0)
				Set @vIDTransaccionPedido = ISNUll((select Top(1) IDTransaccionPedido from ProductoListaPrecioExcepciones Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDListaPrecio=@vIDListaPrecio And IDMoneda=@vIDMoneda and IDTipoDescuento=2 and Desde= @Desde),0)
				
				if @ImporteDescuento = 0 begin
					goto Seguir
				end
				
				Insert Into aProductoListaPrecioExcepciones(IDProducto, IDCliente, IDListaPrecio, IDMoneda, IDSucursal, IDTipoDescuento, 
				Descuento, Porcentaje, Desde, Hasta, CantidadLimite, CantidadLimiteSaldo, UnicaVez, IDTransaccionPedido, IdUsuario, FechaModificacion, accion)
				Values(@IDProducto, @IDCliente, @vIDListaPrecio, @vIDMoneda, @vIDSucursal, @IDTipoDescuento,
				@ImporteDescuento, @vPorcentaje, @Desde, @Hasta, @Cantidad,@vCantidadSaldo, @vUnicaVez, @vIDTransaccionPedido, @IDUsuario, GETDATE(),'ELI')		
		
				Delete From ProductoListaPrecioExcepciones 
				Where IDProducto=@IDProducto And IDCliente=@IDCliente 
				And (Case when @vIDTipoProducto = 1 then 0 else IDListaPrecio end)=(Case when @vIDTipoProducto = 1 then 0 else @vIDListaPrecio end)
				And IDMoneda=@vIDMoneda 
				and IDTipoDescuento=@IDTipoDescuento 
				and Desde= @Desde
		
		Seguir:
				fetch next from cCFTranferencia into @vIDListaPrecio, @vIDSucursal
			
			End
		
			close cCFTranferencia
			deallocate cCFTranferencia
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿CREATE Procedure [dbo].[SpViewExtractoExistenciaValorizadaCuentaContable]

	--Entrada
	@IDTipoProducto int = 0,
	@IDLinea int = 0,
	@IDSubLinea int = 0,
	@IDSucursal int = 0,
	@IDDeposito int = 0,
	@IDProducto int = 0,
	@CodigoCuentaContable varchar(20) = '',
	@Fecha date,
	@Resumido bit = 'False'
	
As

Begin

	--Variables
	Begin
		declare @vIDSucursal int
		declare @vIDDeposito int
		declare @vDeposito varchar(100)
		declare @vSucDep varchar(100)
		declare @vCuentaContable varchar(100)
		declare @vCodigoCuentaContable varchar(100)
		declare @vIDLinea int
		declare @vLinea varchar(100)
		declare @vIDTipoProducto int
		declare @vTipoProducto varchar(100)
		declare @vIDMarca int
		declare @vMarca varchar(100)
		declare @vIDProducto int
		declare @vReferencia varchar(100)
		declare @vProducto varchar(500)
		declare @vCostoPromedio money
		declare @vExistencia decimal(18,2)
		declare @vExistenciaSinAnulados decimal(18,2)
		declare @vCantidadAnulados decimal(18,2)
		declare @vTotalValorizadoCostoPromedio money
		declare @vPesoUnitario money
		
	End
	
	
	--Crear la tabla temporal
	create table #TablaTemporal(IDTransaccion int,
								Fecha date,
								IDSucursal int,
								IDDeposito int,
								Deposito varchar(100),
								[Suc-Dep] varchar(100),
								CuentaContable varchar(100),
								CodigoCuentaContable varchar(100),
								IDProducto int,
								Referencia varchar(100),
								Producto varchar(500),
								CantidadEntrada decimal(18,2), 
								CantidadSalida decimal(18,2), 
								TotalValorizadoEntrada money,  
								TotalValorizadoSalida money)
																	
		Begin
				
			Declare db_cursor cursor for
			
			Select  
			E.IDSucursal,
			E.IDDeposito,
			E.Deposito,
			E.[Suc-Dep],
			E.CuentaContable,
			E.CodigoCuentaContable,
			E.IDLinea,
			E.Linea,
			E.IDTipoProducto,
			E.TipoProducto,
			E.IDMarca,
			E.Marca,
			E.IDProducto,
			E.Referencia,
			E.Producto,
			ISNULL(AVG(E.PesoUnitario),1) PesoUnitario  
			From VExistenciaDepositoCuentaContable E  
			Where E.Estado = 1
			and ((Case when @IDTipoProducto = 0 then 0  else E.IDTipoProducto end) = (Case when @IDTipoProducto = 0 then 0  else @IDTipoProducto end))
			and ((Case when @IDLinea = 0 then 0  else E.IDLinea end) = (Case when @IDLinea = 0 then 0  else @IDLinea end))
			and ((Case when @IDSubLinea = 0 then 0  else E.IDSubLinea end) = (Case when @IDSubLinea = 0 then 0  else @IDSubLinea end))
			and ((Case when @IDSucursal = 0 then 0  else E.IDSucursal end) = (Case when @IDSucursal = 0 then 0  else @IDSucursal end))
			and ((Case when @IDDeposito = 0 then 0  else E.IDDeposito end) = (Case when @IDDeposito = 0 then 0  else @IDDeposito end))
			and ((Case when @IDProducto = 0 then 0  else E.IDProducto end) = (Case when @IDProducto = 0 then 0  else @IDProducto end))
			and ((Case when @CodigoCuentaContable = '' then ''  else E.CodigoCuentaContable end) = (Case when @CodigoCuentaContable = '' then ''  else @CodigoCuentaContable end))

			GROUP BY E.IDSucursal,
			E.IDDeposito,
			E.Deposito,
			E.[Suc-Dep],
			E.CuentaContable,
			E.CodigoCuentaContable,
			E.IDLinea,
			E.Linea,
			E.IDTipoProducto,
			E.TipoProducto, 
			E.IDMarca,
			E.Marca,
			E.IDProducto,
			E.Referencia,
			E.Producto
			Order by E.TipoProducto, 
			E.Linea, 
			E.Producto 
			ASC
			Open db_cursor   
			Fetch Next From db_cursor Into	@vIDSucursal,@vIDDeposito,@vDeposito, @vSucDep, @vCuentaContable, @vCodigoCuentaContable, @vIDLinea,@vLinea,@vIDTipoProducto,@vTipoProducto,@vIDMarca,@vMarca,@vIDProducto,@vReferencia,@vProducto,@vPesoUnitario 
			While @@FETCH_STATUS = 0 Begin 
			
				--Set @vCostoPromedio = (dbo.FCostoProductoFechaKardex(@vIDProducto, @Fecha))
				--Set @vExistencia = (dbo.FExtractoMovimientoProductoDeposito(@vIDProducto, @vIDDeposito, DateAdd(day,1,@Fecha), '', 'False'))
				--Set @vTotalValorizadoCostoPromedio = @vCostoPromedio * @vExistencia
				--Set @vCantidadAnulados = (dbo.FExtractoMovimientoProductoDepositoSinAnulados(@vIDProducto, @vIDDeposito, DateAdd(day,1,@Fecha), '', 'False'))
				Insert Into #TablaTemporal(IDTransaccion, Fecha,IDSucursal, IDDeposito, Deposito, [Suc-Dep], CuentaContable, CodigoCuentaContable, IDProducto, Referencia, Producto,CantidadEntrada,CantidadSalida, TotalValorizadoEntrada, TotalValorizadoSalida) 
							      Select IDTransaccion, Fecha,@vIDSucursal, @vIDDeposito, @vDeposito, @vSucDep, @vCuentaContable, @vCodigoCuentaContable,@vIDProducto,@vReferencia,@vProducto,Entrada,Salida,TotalEntrada,TotalSalida
				From VExtractoMovimientoProductoDetalleDatosMinimosValorizado
				Where IDDeposito=@vIDDeposito And IDProducto=@vIDProducto And Fecha < DateAdd(day,1,@Fecha)
				And isnull(Anulado,0) = 0

			Fetch Next From db_cursor Into	@vIDSucursal,@vIDDeposito,@vDeposito, @vSucDep, @vCuentaContable, @vCodigoCuentaContable, @vIDLinea,@vLinea,@vIDTipoProducto,@vTipoProducto,@vIDMarca,@vMarca,@vIDProducto,@vReferencia,@vProducto,@vPesoUnitario 
				
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
			
		End	
		
		
			Select * From #TablaTemporal

End



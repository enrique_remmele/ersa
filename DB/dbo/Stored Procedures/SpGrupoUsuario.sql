﻿
CREATE Procedure [dbo].[SpGrupoUsuario]

	--Entrada
	@IDGrupo tinyint,
	@IDUsuario smallint = NULL,
	@Operacion varchar(20),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin

	--BLOQUES
	--INSERTAR
	if @Operacion='INS' Begin
		
		--Validar que no exista
		If Exists(Select * From GrupoUsuario Where IDGrupo=@IDGrupo and IDUsuario=@IDUsuario) Begin
			set @Mensaje = 'Este GrupoUsuario ya tiene asignado estos parametros!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		Insert Into GrupoUsuario(IDGrupo, IDUsuario)
		Values(@IDGrupo, @IDUsuario)
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount	
		
				
	End
	
		
	--ELIMINAR
	if @Operacion='DEL' Begin
			
		--Eliminamos los registros
		Delete From GrupoUsuario Where IDGrupo=@IDGrupo and IDUsuario=@IDUsuario	
		Set @Mensaje = 'Registro guardado'
		Set @Procesado = 'True'
		return @@rowcount	
		
	End
				

	
End





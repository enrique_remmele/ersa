﻿CREATE Procedure [dbo].[SpCiudadImportar]

	--Entrada
	@Referencia varchar(100),
	@Descripcion varchar(100),
	@IDPais int,
	@IDDepartamento int,
	
	@Actualizar bit = 'True'
	
As

Begin

	Declare @vID int
	
	--Si existe
	If Exists(Select * From Ciudad Where Descripcion=@Descripcion) Begin
		Update Ciudad Set Descripcion=@Descripcion, Referencia=@Referencia
		Where Descripcion=@Descripcion
	End
	
	--Si existe
	If Not Exists(Select * From Ciudad Where Descripcion=@Descripcion) Begin
		If @Actualizar = 'True' Begin
			Set @vID = (Select IsNull(Max(ID)+1,1) From Ciudad)
			Insert Into Ciudad(ID, IDPais, IDDepartamento, Descripcion, Codigo, Orden, Estado, Referencia)
			Values(@vID, @IDPais, @IDDepartamento, @Descripcion,  @Referencia, 0, 'True', @Referencia)
		End
	End
	
End



﻿CREATE Procedure [dbo].[SpKardex]

	--Entrada
	@Fecha datetime,
	@IDTransaccion numeric(18,0),
	@IDProducto int,
	@ID tinyint,
	@Orden int,
	@CantidadEntrada decimal(10,2),
	@CantidadSalida decimal(10,2),
	@CostoEntrada money,
	@CostoSalida money,
	@IDSucursal int,
	@Comprobante varchar(50)

As

Begin
		Declare @vIDKardex numeric(18,0)
		Declare @vCostoPromedio decimal(18,6)
		Declare @vCantidadSaldo decimal(10,2)
		Declare @vAño smallint
		Declare @vIDKardexAnterior int
		Declare @vCantidadSaldoAnterior decimal(10,2)
		Declare @vCostoPromedioAnterior decimal(18,6) = 0
		Declare @vTotalSaldoAnterior money
		Declare @vTotalSaldo bigint
		Declare @TotalEntrada bigint
		Declare @TotalSalida bigint
		Declare @vCostoPromedioOperacion decimal(18,6)
		Declare @vMes tinyint		
		Declare @vIndice int
		Declare @vFechaPrimerRegistroProducto as date
		Declare @vTipo varchar(50)=''
		Declare @vObservacion varchar(100)=''

		Set @vAño = Datepart(yyyy,@Fecha)
		Set @vMes = Datepart(mm,@Fecha)
		Set @vTipo = (Select Operacion from VTransaccion where id= @IDTransaccion)
	
		--INICIO SM - 18062021 - Implementacion de Auditoria
		Declare @IDTerminal int = (Select IDTerminal from Transaccion where ID = @IDTransaccion)
		Declare @IDUsuario int = (Select IDUsuario from Transaccion where ID = @IDTransaccion)
		Declare @IDOperacion int = (Select IDOperacion from Transaccion where ID = @IDTransaccion)
		--FIN SM - 18062021 - Implementacion de Auditoria

		--'MACHEO FACTURA TICKET'

		if @vTipo like '%MOVIMIENTO%' begin
			if (select idtipocomprobante from Movimiento where IDTransaccion = @IDTransaccion) = (select top(1) IDTipoComprobanteProduccion from configuraciones) begin
				Set @vTipo = 'PRODUCCION'
			end
		end
		
		Set @vIDKardex = IsNull((Select Top(1) IDKardex From Kardex Where IDProducto=@IDProducto),0)
		Set @vCostoPromedioOperacion = IsNull(@CostoEntrada+@CostoSalida,0)

		if @vCostoPromedioOperacion < 0 begin
			Set @vCostoPromedioOperacion = @vCostoPromedioOperacion * -1
		end

		Set @vFechaPrimerRegistroProducto = ISnull((Select Fecha from Kardex where IDProducto = @IDProducto and IDTransaccion = 0), '19000101')
		if Convert(date,@vFechaPrimerRegistroProducto) > Convert(date,@Fecha) begin
			goto Salir
		end
		
		if Exists (select * from Kardex where IDTransaccion = @IDTransaccion and ID=@ID and CantidadEntrada=@CantidadEntrada and CantidadSalida=@CantidadSalida and IDProducto = @IDProducto) begin
			print 'salir 67'
			goto Salir
		end

		--Si la operacion del producto es la primera
		If @vIDKardex = 0 Begin
		
			Set @vIDKardex = (Select ISNULL(Max(IDKardex) + 1, 1) from Kardex)

			Set @vTotalSaldo = (IsNull(@CantidadEntrada,0)*IsNull(@CostoEntrada, 0)) - (IsNull(@CantidadSalida,0)*IsNull(@CostoSalida, 0)) 
			Set @vCantidadSaldo = IsNull(@CantidadEntrada,0) - IsNull(@CantidadSalida,0)

			-- Insert Into Kardex(IDKardex, Fecha, IDTransaccion, IDProducto, ID, Orden, Indice, IDSucursal, Comprobante, CantidadEntrada, CantidadSalida, CantidadSaldo, CostoUnitarioEntrada, CostoUnitarioSalida, TotalSaldo, IDKardexAnterior, CostoPromedioOperacion, CostoPromedio)
			-- Values(@vIDKardex, @Fecha, @IDTransaccion, @IDProducto, @ID, @Orden, 1, @IDSucursal, @Comprobante, @CantidadEntrada, @CantidadSalida, @vCantidadSaldo, @CostoEntrada, @CostoSalida, @vTotalSaldo, @vIDKardexAnterior,@vCostoPromedioOperacion,@vCostoPromedioOperacion)
           
		   --Desde aquí 11-06-2021 SC para probar violación referencial en el idKardex
			if exists (select idkardex from kardex where idkardex = @vIDKardex) begin
			 				
			   	Set @vIDKardex = (Select ISNULL(Max(IDKardex) + 1, 1) from Kardex)

				Insert Into Kardex(IDKardex, Fecha, IDTransaccion, IDProducto, ID, Orden, Indice, IDSucursal, Comprobante, CantidadEntrada, CantidadSalida, CantidadSaldo, CostoUnitarioEntrada, CostoUnitarioSalida, TotalSaldo, IDKardexAnterior, CostoPromedioOperacion, CostoPromedio)
				Values(@vIDKardex, @Fecha, @IDTransaccion, @IDProducto, @ID, @Orden, 1, @IDSucursal, @Comprobante, @CantidadEntrada, @CantidadSalida, @vCantidadSaldo, @CostoEntrada, @CostoSalida, @vTotalSaldo, @vIDKardexAnterior,@vCostoPromedioOperacion,@vCostoPromedioOperacion)
            
				set  @vObservacion = concat('Opcion1:', '1avez Kardex - IDprod:', @IDProducto, ' - IdKarvex:',@vIDKardex)

				--INICIO SM 18062021 - Auditoria Informática
				Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
				Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'INSERTAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'KARDEX' )
			    --FIN SM 18062021 - Auditoria Informática
						
			end else begin
							
				Insert Into Kardex(IDKardex, Fecha, IDTransaccion, IDProducto, ID, Orden, Indice, IDSucursal, Comprobante, CantidadEntrada, CantidadSalida, CantidadSaldo, CostoUnitarioEntrada, CostoUnitarioSalida, TotalSaldo, IDKardexAnterior, CostoPromedioOperacion, CostoPromedio)
				Values(@vIDKardex, @Fecha, @IDTransaccion, @IDProducto, @ID, @Orden, 1, @IDSucursal, @Comprobante, @CantidadEntrada, @CantidadSalida, @vCantidadSaldo, @CostoEntrada, @CostoSalida, @vTotalSaldo, @vIDKardexAnterior,@vCostoPromedioOperacion,@vCostoPromedioOperacion)
			
				set  @vObservacion = concat('Opcion2:', '1avez Kardex - IDprod:', @IDProducto, ' - IdKarvex:',@vIDKardex)
				
				--INICIO SM 18062021 - Auditoria Informática
				Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
				Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'INSERTAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'KARDEX' )
			    --FIN SM 18062021 - Auditoria Informática
						
			end
			--Hasta aquí 11-06-2021 SC para probar violación referencial en el idKardex
	
		End Else Begin

			Select @vIDKardexAnterior=Max(IDKardex),
					@vIndice=ISNULL(Max(Indice) + 1, 1) 
			from Kardex Where IDProducto=@IDProducto And Convert(date,Fecha)<=Convert(date,@Fecha)

			Select	@vCantidadSaldoAnterior=CantidadSaldo,
					@vTotalSaldoAnterior=TotalSaldo,
					@vCostoPromedioAnterior =CostoPromedio
			from Kardex Where IDKardex=@vIDKardexAnterior


			Set @vCostoPromedioOperacion = IsNull(@CostoEntrada+@CostoSalida,0)
			Set @TotalEntrada = Round(isnull(@CostoEntrada,0) * isnull(@CantidadEntrada,0),0)
			Set @TotalSalida = Round(isnull(@CostoSalida,0) * isnull(@CantidadSalida,0),0)

			If @CantidadEntrada = 0 and @CantidadSalida=0 and @vTipo = 'MACHEO FACTURA TICKET' begin
				Set @vCostoPromedioOperacion = ABS(IsNull(@CostoEntrada-@CostoSalida,0))/@vCantidadSaldoAnterior
				Set @TotalEntrada = Round(isnull(@CostoEntrada,0),0)
				Set @TotalSalida = Round(isnull(@CostoSalida,0),0)
			end
			
			If @vTipo not in ('TICKET DE BASCULA', 'COMPRA DE MERCADERIA', 'PRODUCCION','MACHEO FACTURA TICKET')  Begin

				If @vTipo = 'PRODUCTO-COSTO' begin
					Set @vCostoPromedioAnterior = @CostoEntrada + @CostoSalida 
				end

				--En caso de que el costo promedio anterior sea cero, se toma el costo de la operacion.
				If @vCostoPromedioAnterior = 0 Begin
					Set @vCostoPromedioAnterior = @CostoEntrada + @CostoSalida 
				End

				If isnull(@CostoEntrada,0) <> 0 and isnull(@CostoSalida,0) = 0 begin
					Set @CostoEntrada = @vCostoPromedioAnterior
				end

				If isnull(@CostoSalida,0) <> 0 and isnull(@CostoEntrada,0) = 0 begin
					Set @CostoSalida = @vCostoPromedioAnterior
				end
				
				---
				If isnull(@CostoEntrada,0) = 0 and ABS(isnull(@CantidadEntrada,0)) > 0 begin
					Set @CostoEntrada = @vCostoPromedioAnterior
				end

				If isnull(@CostoSalida,0) = 0 and ABS(isnull(@CantidadSalida,0)) > 0 begin
					Set @CostoSalida = @vCostoPromedioAnterior
				end

				If @vTipo = 'PRODUCTO-COSTO' begin
					Set @CantidadEntrada = 0
					Set @CantidadSalida = 0
				end


				Set @vCostoPromedioOperacion = @vCostoPromedioAnterior
				Set @vCostoPromedio = @vCostoPromedioAnterior
				
				Set @TotalEntrada = Round(isnull(Round(@CostoEntrada,4),0) * isnull(@CantidadEntrada,0),0)
				Set @TotalSalida = Round(isnull(Round(@CostoSalida,4),0) * isnull(@CantidadSalida,0),0)
				
				Set @vCantidadSaldo = @vCantidadSaldoAnterior+@CantidadEntrada-@CantidadSalida								
				Set @vTotalSaldo = @vTotalSaldoAnterior + (IsNull(@CantidadEntrada,0)*IsNull(@CostoEntrada, 0)) - (IsNull(@CantidadSalida,0)*IsNull(@CostoSalida, 0)) 
				print concat('Transaccion ', @IDTransaccion)
				print concat('No promedia ', @vIDKardex, ' ', @vCostoPromedio)
				print concat('@vCantidadSaldo: ',@vCantidadSaldo, ' incluye: ', @vCantidadSaldoAnterior , '+', @CantidadEntrada, '-', @CantidadSalida)
				print concat('@vTotalSaldo: ',@vTotalSaldo, ' incluye: ', @vTotalSaldoAnterior , '+', (IsNull(@CantidadEntrada,0)*IsNull(@CostoEntrada, 0)), '-', (IsNull(@CantidadSalida,0)*IsNull(@CostoSalida, 0)))
				print concat('@vTotalSaldoAnterior: ',@vTotalSaldoAnterior)
				print concat('@@TotalEntrada: ',@TotalEntrada)
				print concat('@@TotalSalida: ',@TotalSalida)
				print concat('@vCostoPromedio: ',@vCostoPromedio)

				If @vTipo = 'PRODUCTO-COSTO' begin
					Set @vTotalSaldo = Round(@vCantidadSaldo * @CostoEntrada,0)
					Set @vCostoPromedio = @CostoEntrada
					Set @vCostoPromedioOperacion = @CostoEntrada
					print 'producto costo'
					print @CostoEntrada
					
					GoTo Actualizar
				end
				GoTo Actualizar
			End
			
			-- Si ES compra o ticket calcula el costo promedio
			else begin

				if ISNULL(@CantidadSalida,0) <> 0 begin
					Set @vCostoPromedio = @vCostoPromedioAnterior
				end
				else begin
					-- Si es compra y la cantidad anterior es menor o igual a cero NO PROMEDIA
					If @CantidadEntrada > 0 and @vCantidadSaldoAnterior <= 0 And (@vTipo = 'COMPRA DE MERCADERIA' or @vTipo = 'TICKET DE BASCULA') Begin
						Set @vCantidadSaldo = (IsNull(@vCantidadSaldoAnterior,0)+IsNull(@CantidadEntrada,0))-(IsNull(@CantidadSalida,0))
						print 'no recalcular cantidad saldo anterior <0'
						print concat('@vCantidadSaldo: ',@vCantidadSaldo)
						print concat('@vTotalSaldoAnterior: ',@vTotalSaldoAnterior)
						print concat('@vCantidadSaldo: ',@TotalEntrada)
						print concat('@vCantidadSaldo: ',@TotalSalida)
						print concat('@vCostoPromedio: ',@vCostoPromedio)

						Set @vTotalSaldo = @vTotalSaldoAnterior + (IsNull(@CantidadEntrada,0)*IsNull(@CostoEntrada, 0)) - (IsNull(@CantidadSalida,0)*IsNull(@CostoSalida, 0)) 
						Set @vCostoPromedio = @vCostoPromedioOperacion
						print concat('no promedia COMPRA', @vIDKardex, ' ', @vCostoPromedio, ' Cantidad = ', @vCantidadSaldoAnterior)
						GoTo Actualizar
						
					End

					If @CantidadEntrada = 0 and @CantidadSalida=0 and @vTipo = 'MACHEO FACTURA TICKET' begin
						
						Set @vCantidadSaldo = (IsNull(@vCantidadSaldoAnterior,0))
					
						Set @vTotalSaldo = @vTotalSaldoAnterior + (IsNull(@CostoEntrada, 0)) -(IsNull(@CostoSalida, 0)) 
						Set @vCostoPromedio = @vCostoPromedioOperacion
						Set @TotalEntrada = (IsNull(@CostoEntrada, 0))
						Set @TotalSalida = (IsNull(@CostoSalida, 0))
						print concat('promedia MACHEO', @vIDKardex, ' ', @vCostoPromedio, ' Cantidad = ', @vCantidadSaldoAnterior)
						print 'Entro a calcular macheo factura ticket / ajuste de contrato'
						print concat('@vCantidadSaldo: ',@vCantidadSaldo)
						print concat('@vTotalSaldoAnterior: ',@vTotalSaldoAnterior)
						print concat('@@TotalEntrada: ',@TotalEntrada)
						print concat('@@TotalSalida: ',@TotalSalida)
						print concat('@vCostoPromedio: ',@vCostoPromedio)
						GoTo Actualizar
						
					end

					Set @vCantidadSaldo = (IsNull(@vCantidadSaldoAnterior,0)+IsNull(@CantidadEntrada,0))-(IsNull(@CantidadSalida,0))

					Set @vTotalSaldo = @vTotalSaldoAnterior + (IsNull(@CantidadEntrada,0)*IsNull(@CostoEntrada, 0)) - (IsNull(@CantidadSalida,0)*IsNull(@CostoSalida, 0)) 

					if @vCantidadSaldo <=0 or @vTotalSaldo <=0 begin
						Set @vCostoPromedio = @vCostoPromedioOperacion
					end
					else begin
						Set @vCostoPromedio = @vTotalSaldo/@vCantidadSaldo
					end
					print 'recalcular'
						print concat('@vCantidadSaldo: ',@vCantidadSaldo, ' incluye: ', @vCantidadSaldoAnterior , '+', @CantidadEntrada, '-', @CantidadSalida)
						print concat('@vTotalSaldo: ',@vTotalSaldo, ' incluye: ', @vTotalSaldoAnterior , '+', @TotalEntrada, '-', @TotalSalida)
						print concat('@vTotalSaldoAnterior: ',@vTotalSaldoAnterior)
						print concat('@vCantidadSaldo: ',@TotalEntrada)
						print concat('@vCantidadSaldo: ',@TotalSalida)
						print concat('@vCostoPromedio: ',@vCostoPromedio)

					print concat('promedia ', @vIDKardex, ' ', @vCostoPromedio)
				end
			end

		Actualizar:

			Set @vIDKardex = (Select ISNULL(Max(IDKardex) + 1, 1) from Kardex)

			--Insert Into Kardex(IDKardex, Fecha, IDTransaccion, IDProducto, ID, Orden, Indice ,IDSucursal, Comprobante, CantidadEntrada, CantidadSalida, CantidadSaldo, CostoUnitarioEntrada, CostoUnitarioSalida, TotalEntrada, TotalSalida, TotalSaldo, IDKardexAnterior, CostoPromedioOperacion, CostoPromedio)
			--Values(@vIDKardex, @Fecha, @IDTransaccion, @IDProducto, @ID, @Orden, @vIndice, @IDSucursal, @Comprobante, @CantidadEntrada, @CantidadSalida, @vCantidadSaldo, @CostoEntrada, @CostoSalida, @TotalEntrada, @TotalSalida, @vTotalSaldo, @vIDKardexAnterior, @vCostoPromedioOperacion, @vCostoPromedio)		
			
			--Desde aqui 11-06-2021 SC para probar violacion referencial en el idKardex
			if exists (select idkardex from kardex where idkardex = @vIDKardex) begin
			 
			   Set @vIDKardex = (Select ISNULL(Max(IDKardex) + 1, 1) from Kardex)

			   Insert Into Kardex(IDKardex, Fecha, IDTransaccion, IDProducto, ID, Orden, Indice, IDSucursal, Comprobante, CantidadEntrada, CantidadSalida, CantidadSaldo, CostoUnitarioEntrada, CostoUnitarioSalida, TotalSaldo, IDKardexAnterior, CostoPromedioOperacion, CostoPromedio)
			   Values(@vIDKardex, @Fecha, @IDTransaccion, @IDProducto, @ID, @Orden, 1, @IDSucursal, @Comprobante, @CantidadEntrada, @CantidadSalida, @vCantidadSaldo, @CostoEntrada, @CostoSalida, @vTotalSaldo, @vIDKardexAnterior,@vCostoPromedioOperacion,@vCostoPromedioOperacion)
        	
				set  @vObservacion = concat('Opcion3:', 'En Kardex - IDprod:', @IDProducto, ' - IdKarvex:',@vIDKardex)
				
				--INICIO SM 18062021 - Auditoria Informática
				Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
				Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'INSERTAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'KARDEX' )
			    --FIN SM 18062021 - Auditoria Informática
			
			end else begin
				
				Insert Into Kardex(IDKardex, Fecha, IDTransaccion, IDProducto, ID, Orden, Indice, IDSucursal, Comprobante, CantidadEntrada, CantidadSalida, CantidadSaldo, CostoUnitarioEntrada, CostoUnitarioSalida, TotalSaldo, IDKardexAnterior, CostoPromedioOperacion, CostoPromedio)
				Values(@vIDKardex, @Fecha, @IDTransaccion, @IDProducto, @ID, @Orden, 1, @IDSucursal, @Comprobante, @CantidadEntrada, @CantidadSalida, @vCantidadSaldo, @CostoEntrada, @CostoSalida, @vTotalSaldo, @vIDKardexAnterior,@vCostoPromedioOperacion,@vCostoPromedioOperacion)
			
				set  @vObservacion = concat('Opcion4:', 'En Kardex - IDprod:', @IDProducto, ' - IdKarvex:',@vIDKardex)
				
				--INICIO SM 18062021 - Auditoria Informática
				Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
				Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'INSERTAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'KARDEX' )
			    --FIN SM 18062021 - Auditoria Informática
						
			end
			 --Hasta aquí 11-06-2021 SC para probar violación referencial en el idKardex
		End
Salir:
		
End


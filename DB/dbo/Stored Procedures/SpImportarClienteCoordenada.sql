﻿CREATE Procedure [dbo].[SpImportarClienteCoordenada]

	--Entrada
	--Identificadores
	@Cliente varchar(200) = '',
	@Latitud varchar(150) = '0',
	@Longitud varchar(50) = '0',
	
	--Opciones
	@Actualizar Bit = 'False'
	
As

Begin

	--ID's
	Declare @vIDCliente int
	Declare @vOperacion varchar(10) 
	Declare @Mensaje varchar(200)
	Declare @Procesado bit
		
	Set	@vIDCliente = 0
	Set @vOperacion = 'INS'
	
	Set @vIDCliente = IsNull((Select Top(1) ID From Cliente Where RazonSocial=@Cliente), 0)

	--Verificamos si ya existe
	If @vIDCliente = 0 Begin
		Set @Mensaje = 'No se encontro el cliente'
		Set @Procesado = 'False'
		GoTo Salir
	End		
	
	--Actualizamos la fecha de alta
	Update Cliente Set Latitud=@Latitud, Longitud=@Longitud
	Where ID=@vIDCliente

	Set @Mensaje = 'Registro actualizado!'
	Set @Procesado = 'True'

	GoTo Salir

Salir:
	Select 'Mensaje'=@Mensaje + ' ' + @vOperacion, 'Procesado'=@Procesado
End

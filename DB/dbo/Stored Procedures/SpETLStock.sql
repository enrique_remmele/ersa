﻿CREATE Procedure [dbo].[SpETLStock]

	@IDSucursal tinyint
As

Begin
	
	Declare @vCodigoDistribuidor int
	Set @vCodigoDistribuidor = (Select CodigoDistribuidor From Sucursal Where ID=@IDSucursal)
	
	Select
	'CodDistribuidor'= dbo.FFormatoTamañoCaracter(15, @vCodigoDistribuidor),
	'CodProducto'= dbo.FFormatoTamañoCaracter (15, P.Referencia),
	'EAN13Producto'=dbo.FFormatoTamañoCaracter (15, P.CodigoBarra),
	'Cantidad'= dbo.FFormatoTamañoNumerico (20.2,ISNULL((Select SUM(ED.Existencia) From VExistenciaDeposito ED Where  ED.IDProducto=P.ID And ED.IDSucursal=@IDSucursal),0)),
	'DiaTomaStock'=CONVERT(varchar(5), Year(GETDATE ())) + dbo.FFormatoDosDigitos(Month(GETDATE ())) + dbo.FFormatoDosDigitos(Day(GETDATE ()))
	From VProducto P
	
	Where P.IDTipoProducto=7 --<- Este es FOOD, solo estos se debe exportar
	And P.Estado='True'
	And P.ControlarExistencia='True'
End
	






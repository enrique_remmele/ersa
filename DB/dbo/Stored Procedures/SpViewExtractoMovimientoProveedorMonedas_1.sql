﻿CREATE Procedure [dbo].[SpViewExtractoMovimientoProveedorMonedas]

	--Entrada
	@Fecha1 Date,
	@Fecha2 Date,
	@IDProveedor int,	
	@Comprobante varchar (30) = '%%',
	@IDMoneda int = 1
As

Begin
	--Crear la tabla temporal
    create table #TablaTemporal(ID tinyint,
								ID2 int,
								IDTransaccion int,
								Codigo int,
								RazonSocial varchar(50),
								Referencia varchar(50),
								Fecha date,
								Operacion varchar(50),
								Documento varchar(50),
								[Detalle/Concepto] varchar(50),
								Debitos money,
								Creditos money,
								Saldo money,
								SaldoAnterior money,
								SaldoInicial money,
								IDMoneda int,
								DescripcionMoneda varchar(50),
								Movimiento char(20),
								RRHH bit,
								primary key(ID,ID2))
								
	--Variables para calcular saldo
	declare @vSaldo money
	declare @vTotalDebito money
	declare @vTotalCredito money
	declare @vSaldoAnterior money
	declare @vSaldoInicial money
	declare @vIDMonedaCambio tinyint = 0
	
	--Declarar variables
	declare @vID tinyint
	declare @vID2 int
	declare @vIDTransaccion Numeric (18,0)
	declare @vIDProveedor int
	declare @vRazonSocial varchar (100)
	declare @vReferencia varchar(50)
	declare @vFecha date
	declare @vOperacion varchar(50)
	declare @vDocumento varchar(50)
	declare @vDetalleConcepto varchar(50)
	declare @vDebitos money
	declare @vCreditos money
	declare @vIDMoneda int
	declare @vMoneda varchar(50)
	declare @vMovimiento varchar(20)
	declare @vRRHH bit
	
	set @vID = 0
	set @vID2 = 1

	--Insertar datos	
	Begin
	
		Declare db_cursor cursor for
		
		Select
		IDTransaccion,
		Codigo,
		RazonSocial,
		Referencia,
		Fecha,
		Operacion,
		Documento,
		[Detalle/Concepto],
		Debito,
		Credito,
		IDMoneda,
		DescripcionMoneda,
		Movimiento,
		RRHH
		From VExtractoMovimientoProveedor
		Where Codigo=@IDProveedor and Fecha between @Fecha1 and @Fecha2 And Documento Like @Comprobante
		Order By IDMoneda, Fecha, Credito Desc
		Open db_cursor  
		Fetch Next From db_cursor Into   @vIDTransaccion,@vIDProveedor,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vDebitos,@vCreditos,@vIDMoneda,@vMoneda,@vMovimiento,@vRRHH
		
		While @@FETCH_STATUS = 0 Begin 
		
			--Insertar Saldo Anterior
			If @vIDMonedaCambio!= @vIDMoneda Begin
				
				Set @vIDMonedaCambio = @vIDMoneda
				Set @vID = @vID + 1

				--Hallar Totales para Saldo Anterior
				Set @vTotalDebito  = IsNull((Select Sum(Debito) From VExtractoMovimientoProveedor Where Codigo=@IDProveedor And Fecha < @Fecha1 And IDMoneda=@vIDMoneda),0)
				Set @vTotalCredito = IsNull((Select Sum(Credito) From VExtractoMovimientoProveedor Where Codigo=@IDProveedor And Fecha < @Fecha1 And IDMoneda=@vIDMoneda),0)
		
				--Hallar Saldo Anterior
				Set @vSaldoAnterior =  @vTotalCredito - @vTotalDebito
				Set @vSaldoInicial = @vSaldoAnterior 
		
				Insert Into  #TablaTemporal(ID, ID2, IDTransaccion, Codigo, RazonSocial, Referencia, Fecha, Operacion, Documento, [Detalle/Concepto], Saldo, SaldoAnterior, SaldoInicial, Debitos, Creditos, IDMoneda, DescripcionMoneda, Movimiento, RRHH) 
				Values (@vID, @vID2, 0, @vIDProveedor, @vRazonSocial, @vReferencia, @Fecha1, '', '', '==== SALDO ANTERIOR ' + @vMoneda + '====' , @vSaldoInicial, @vSaldoInicial, 0, 0, 0, @vIDMoneda, @vMoneda, 'SALDO', @vRRHH)
		
				--Sumar 1 al ID2 de la Tabla Temporal
				Set @vID2 = @vID2 + 1

			End 
			
			--Hallar Saldo			
			--set @vSaldo = (@vSaldoAnterior + @vDebitos) - @vCreditos
			set @vSaldo = (@vSaldoAnterior + @vCreditos) - @vDebitos
									
			--Insertar Fila en la Tabla Temporal
			Insert Into  #TablaTemporal(ID,ID2,IDTransaccion,Codigo,RazonSocial,Referencia,Fecha,Operacion,Documento,[Detalle/Concepto],Saldo,SaldoAnterior,SaldoInicial,Debitos,Creditos,IDMoneda,DescripcionMoneda,Movimiento, RRHH) 
								Values (@vID,@vID2,@vIDTransaccion,@vIDProveedor,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vSaldo,@vSaldoAnterior,@vSaldoInicial,@vDebitos,@vCreditos,@vIDMoneda,@vMoneda,@vMovimiento, @vRRHH)
				
			--Actualizar Saldo
			set @vSaldoAnterior = @vSaldo 
			
			--Sumar 1 al ID2 de la Tabla Temporal
			Set @vID2 = @vID2 + 1
			
			Fetch Next From db_cursor Into @vIDTransaccion,@vIDProveedor,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vDebitos,@vCreditos,@vIDMoneda,@vMoneda,@vMovimiento,@vRRHH
			
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		
	End	
	
	--Insertar Totales Acumulados
	Begin
	
		Declare db_cursorAcumulado cursor for
		
		Select IDMoneda, 'Debitos'=Sum(Debitos), 'Creditos'=Sum(Creditos) 
		From #TablaTemporal 
		Group By IDMoneda
		Open db_cursorAcumulado
		Fetch Next From db_cursorAcumulado Into @vIDMoneda, @vDebitos, @vCreditos
		
		While @@FETCH_STATUS = 0 Begin 
		
			Set @vID = (Select Max(ID) From #TablaTemporal Where IDMoneda=@vIDMoneda)
			Set @vID2 = (Select Max(ID2)+1 From #TablaTemporal Where IDMoneda=@vIDMoneda)
			
			Select Top(1)	@vIDProveedor=Codigo, 
							@vRazonSocial=RazonSocial, 
							@vReferencia=Referencia,
							@vMoneda=DescripcionMoneda,
							@vSaldo=Saldo
			From #TablaTemporal 
			Where ID=@vID
			Order By ID2 Desc
			
			Insert Into  #TablaTemporal(ID, ID2, IDTransaccion, Codigo, RazonSocial, Referencia, Fecha, Operacion, Documento, [Detalle/Concepto], Saldo, SaldoAnterior, SaldoInicial, Debitos, Creditos, IDMoneda, DescripcionMoneda, Movimiento, RRHH) 
			Values (@vID, @vID2, 1, @vIDProveedor, @vRazonSocial, @vReferencia, @Fecha2, '', '', '==== SALDO ACUMULADO ' + @vMoneda + '====' , @vSaldo, 0, 0, @vDebitos, @vCreditos, @vIDMoneda, @vMoneda, 'ACUMULADO', @vRRHH)
		
			Fetch Next From db_cursorAcumulado Into @vIDMoneda, @vDebitos, @vCreditos
			
		End
		
		--Cierra el cursor
		Close db_cursorAcumulado
		Deallocate db_cursorAcumulado	   			
		
	End
	
	Select *, Fec=CONVERT(varchar(50), Fecha, 5) From #TablaTemporal Order By ID, ID2
	
End

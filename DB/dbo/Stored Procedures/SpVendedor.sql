﻿
CREATE Procedure [dbo].[SpVendedor]

	--Entrada
	@ID tinyint,
	@Nombres varchar(50),
	@Referencia varchar(50),
	@Resumen varchar(50),
	@Apellidos varchar(50) = NULL,
	@NroDocumento varchar(15) = NULL,
	@Telefono varchar(20) = NULL,
	@Celular varchar(20) = NULL,
	@Direccion varchar(100) = NULL,
	@Email varchar(50) = NULL,
	@Estado bit,
	@IDSucursal tinyint,
	@IDDeposito TINYINT = NULL,
	@Operacion varchar(10),

	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la Nombres ya existe
		if exists(Select * From Vendedor Where Nombres=@Nombres) begin
			set @Mensaje = 'El nombre ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDVendedor tinyint
		set @vIDVendedor = (Select IsNull((Max(ID)+1), 1) From Vendedor)

		--Insertamos
		Insert Into Vendedor(ID, Nombres, Resumen, Referencia, Apellidos, NroDocumento, Telefono, Celular, Direccion, Email, Estado,IDSucursal, IDDeposito)
		Values(@vIDVendedor, @Nombres, @Resumen, @Referencia, @Apellidos,@NroDocumento, @Telefono, @Celular, @Direccion, @Email, @Estado,@IDSucursal, @IDDeposito)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='VENDEDOR', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From Vendedor Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la Nombres ya existe
		if exists(Select * From Vendedor Where Nombres=@Nombres And ID!=@ID) begin
			set @Mensaje = 'El nombre ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Vendedor Set Nombres=@Nombres,
							Resumen=@Resumen,
							Referencia=@Referencia,
							Apellidos=@Apellidos,
							NroDocumento=@NroDocumento, 
							Telefono=@Telefono, 
							Celular=@Celular, 
							Direccion=@Direccion, 
							Email=@Email,
							Estado = @Estado,
							IDSucursal=@IDSucursal,
							IDDeposito=@IDDeposito
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='VENDEDOR', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Vendedor Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Ventas
		if exists(Select * From Venta Where IDVendedor=@ID And Anulado='False') begin
			set @Mensaje = 'El registro tiene ventas asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
			
		end
		--Si tiene una relacion con CLIENTES
		if exists(Select * From Cliente Where IDVendedor=@ID) begin
			set @Mensaje = 'El registro tiene clientes asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Vendedor 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='VENDEDOR', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End






﻿CREATE Procedure [dbo].[SpActualizarAsientoMovimientoConsumoCombustible]

	@IDTransaccion numeric(18,0)
		
As

Begin
	

	--Asiento
	Declare @vImporte money
	Declare @vImporteDebe money
	Declare @vImporteHaber money
	
	Set @vImporte = (select Total from Movimiento where IDTransaccion = @IDTransaccion)
	
	Update DetalleAsiento set Credito = @vImporte,
							  Importe = @vImporte
							  where IDTransaccion = @IDTransaccion
							  and Credito > 0
	
	Update DetalleAsiento set Debito = @vImporte
	   						  where IDTransaccion = @IDTransaccion
							  and Debito > 0

	--Actualizamos la cabecera, el total
	Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
	Set @vImporteHaber = ROUND(@vImporteHaber, 0)
	Set @vImporteDebe = ROUND(@vImporteDebe, 0)
	
	Update Asiento Set Total = @vImporteHaber,
						Credito = @vImporteHaber,
						Debito = @vImporteDebe,
						Saldo = @vImporteHaber - @vImporteDebe
	Where IDTransaccion=@IDTransaccion
	
End


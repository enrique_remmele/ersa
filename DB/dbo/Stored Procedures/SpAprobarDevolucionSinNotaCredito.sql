﻿CREATE Procedure [dbo].[SpAprobarDevolucionSinNotaCredito]
	--Entrada
	@IDTransaccion as numeric(18,0),
	@ObservacionAutorizador varchar(500) = '',
	--@Estado as bit,
	@IDUsuario int,
	@Operacion varchar(20),
	@IDSucursal int,
	@IDTerminal int,

	@Mensaje as varchar(200) output,
	@Procesado as bit output 
As
Begin
	Declare @vIDTransaccionMovimientoStock int
	
	Declare @vFecha Date
	Declare @vIDTipoComprobante int -- Tipo de comprobante DevolucionSinNotaCredito registrado en configuraciones
	Declare @vNroComprobante varchar(50)
	Declare @vIDDepositoEntrada int
	Declare @vObservacion varchar(200)
	Declare @vTotal money
	Declare @vIDUsuario int
	Declare @vIDSucursal int
	Declare @vIDDeposito int
	Declare @vIDTerminal int

	if @Operacion = 'APROBAR' begin

		if cast((Select dbo.FExistenciaUsuarioAutorizarNotaCredito(@IDUsuario,1)) as bit) = Cast('False' as bit) begin
			Set @Mensaje = 'No posee permisos suficientes para autorizar esta solicitud'
			Set @Procesado = 'False'
			return @@Rowcount
		end

		
		if not Exists (select * from DevolucionSinNotaCredito where IDTransaccion = @IDTransaccion )begin
			Set @Mensaje = 'No se encuentra el registro solicitado'
			Set @Procesado = 'False'
			return @@Rowcount
		end
		
		if @IDTransaccion <> 0 begin	
			----Actualizar
			
			Set @vIDTipoComprobante=(Select top(1) IDTipoComprobanteDevolucionSinNotaCredito from configuraciones)

			Select @vFecha = Fecha, @vNroComprobante = Numero, @vIDDepositoEntrada=IDDeposito,
					@vObservacion=Observacion, @vTotal=Total, @vIDUsuario = IDUsuario, @vIDSucursal=IDSucursal, 
					@vIDDeposito = IDDeposito, @vIDTerminal=IDTerminal
			from vDevolucionSinNotaCredito
			where IDTransaccion = @IDTransaccion

			Exec SpMovimiento
			@Numero='0'
			,@Fecha=@vFecha
			,@IDTipoOperacion='1'
			,@IDMotivo='1'
			,@IDTipoComprobante=@vIDTipoComprobante
			,@NroComprobante=@vNroComprobante
			,@IDDepositoSalida=null
			,@IDDepositoEntrada=@vIDDeposito
			,@Observacion=@vObservacion
			,@Autorizacion=''
			,@Total=@vTotal
			,@TotalImpuesto='0'
			,@TotalDiscriminado='0'
			,@Operacion='INS'
			,@MovimientoStock='True'
			,@IDOperacion='15'
			,@IDUsuario=@vIDUsuario
			,@IDSucursal=@vIDSucursal
			,@IDDeposito=@vIDDeposito
			,@IDTerminal=@vIDTerminal
			,@Mensaje=@Mensaje OUTPUT
			,@Procesado=@Procesado OUTPUT
			,@IDTransaccionSalida=@vIDTransaccionMovimientoStock OUTPUT


			Declare @vIDProducto int
			Declare @vID int
			Declare @vCantidad decimal(10,2)
			Declare @vObservacionDetalle varchar(200) = ''
			Declare @vPrecioUnitario money
			Declare @vIDImpuesto int
			Declare @vTotalDetalle money

			Declare cCFCompraProveedor cursor for
			Select D.IDProducto,
			D.ID,
			Isnull(D.Observacion,''),
			D.Cantidad,
			D.PrecioUnitario,
			P.IDImpuesto
			From detalleDevolucionSinNotaCredito D
			join Producto P on D.IDProducto = P.ID
			Where D.IDTransaccion=@IDTransaccion
			Open cCFCompraProveedor   
			fetch next from cCFCompraProveedor into @vIDProducto,@vID,@vObservacionDetalle,@vCantidad,@vPrecioUnitario,@vIDImpuesto
		
			While @@FETCH_STATUS = 0 Begin  
				
				Set @vTotalDetalle = (@vPrecioUnitario * @vCantidad)

				Exec SpDetalleMovimiento
						@IDTransaccion=@vIDTransaccionMovimientoStock
						,@IDProducto=@vIDProducto
						,@ID=@vID
						,@IDDepositoEntrada=@vIDDepositoEntrada
						,@IDDepositoSalida=0
						,@Observacion=@vObservacionDetalle
						,@Cantidad=@vCantidad
						,@IDImpuesto=@vIDImpuesto
						,@PrecioUnitario=@vPrecioUnitario
						,@Total=@vTotalDetalle
						,@TotalImpuesto='0'
						,@TotalDiscriminado='0'
						,@Caja='False'
						,@CantidadCaja='0'
						,@Operacion='INS'
						,@Mensaje=''
						,@Procesado=''
			
				fetch next from cCFCompraProveedor into @vIDProducto,@vID,@vObservacion,@vCantidad,@vPrecioUnitario,@vIDImpuesto
			
			End
		
			close cCFCompraProveedor 
			deallocate cCFCompraProveedor

			Exec SpAsientoMovimiento @IDTransaccion

			Update DevolucionSinNotaCredito
			Set Aprobado  = 1,
			ObservacionAutorizador = @ObservacionAutorizador,
			IDUsuarioAprobado = @IDUsuario,
			FechaAprobado = getdate(),
			IDTransaccionMovimientoStock = @vIDTransaccionMovimientoStock
			Where IDTransaccion = @IDTransaccion



			Set @Mensaje = 'Registro procesado'
			Set @Procesado = 'True'
			return @@Rowcount
		end
	end 

	if @Operacion = 'RECHAZAR' begin
		if @IDTransaccion <> 0 begin	
			----Actualizar
			Update DevolucionSinNotaCredito
			Set Aprobado  = 0,
			ObservacionAutorizador = @ObservacionAutorizador,
			IDUsuarioAprobado = @IDUsuario,
			FechaAprobado = getdate()
			Where IDTransaccion = @IDTransaccion

			Set @Mensaje = 'Registro procesado'
			Set @Procesado = 'True'
			return @@Rowcount
		end
	end 

	if @Operacion = 'RECUPERAR' begin
		if @IDTransaccion <> 0 begin	
			----Actualizar
			Update DevolucionSinNotaCredito
			Set Aprobado  = Null,
			ObservacionAutorizador = '',
			IDUsuarioAprobado = Null,
			FechaAprobado = Null
			Where IDTransaccion = @IDTransaccion

			Set @Mensaje = 'Registro procesado'
			Set @Procesado = 'True'
			return @@Rowcount
		end
	end 
  
	
End



﻿CREATE procedure [dbo].[CastActualizarLatitudLongitud]
As

Begin
Declare @IDCliente as int
Declare @CCLongitud as decimal(18,16)
Declare @CCLatitud as decimal(18,16) 
Declare @Longitud as decimal(18,16)
Declare @Latitud as decimal(18,16)

Declare cActualizaMatriz cursor for
		select 
		c.id,
		cc.latitud, 
		cc.longitud, 
		'clilat' = c.latitud, 
		'clilong'=c.longitud 
		from cast_cliente cc 
		join Cliente c on cc.codigo = c.Referencia
		where cc.sucursal = 0
		and cc.Latitud <> c.Latitud
		Open cActualizaMatriz   
		fetch next from cActualizaMatriz into @IDCliente ,@CCLatitud ,@CCLongitud ,@Latitud ,@Longitud
		Declare @vContador int = 0
		While @@FETCH_STATUS = 0 Begin  
		
			--if @Latitud = @CCLatitud begin
			--	if @Longitud = @CCLongitud begin
			--		Print @CCLatitud
			--		Set @vContador = @vContador +1
			--		print @vContador
			--		goto seguirmatriz
					
			--	end

			--end
			
			Update Cliente set Latitud = @CCLatitud, Longitud = @CCLongitud
			where id =  @IDCliente
seguirmatriz:
			
			fetch next from cActualizaMatriz into @IDCliente ,@CCLatitud ,@CCLongitud ,@Latitud ,@Longitud
			
		End
		
		close cActualizaMatriz 
		deallocate cActualizaMatriz

		print 'prueba'



Declare @IDClienteSuc as int
Declare @ID as int
Declare @CCLongitudSuc as decimal(18,16)
Declare @CCLatitudSuc as decimal(18,16) 
Declare @LongitudSuc as decimal(18,16)
Declare @LatitudSuc as decimal(18,16)

Declare cActualizaSucursal cursor for
		select 
		c.IDCliente,
		c.ID,
		cc.latitud, 
		cc.longitud, 
		'clilat' = c.latitud, 
		'clilong'=c.longitud 
		from cast_cliente cc,
		--join Cliente c on cc.codigo = c.Referencia
		 vClienteSucursal c 
		where cc.codigo = c.CodigoCast
		and cc.sucursal = c.id
		and cc.longitud <> c.Longitud
				
		Open cActualizaSucursal   
		fetch next from cActualizaSucursal into @IDClienteSuc, @ID, @CCLatitudSuc ,@CCLongitudSuc ,@LatitudSuc ,@LongitudSuc
		Declare @vContador1 int = 0
		While @@FETCH_STATUS = 0 Begin  
		
			--if @LatitudSuc = @CCLatitudSuc begin
			--	if @LongitudSuc = @CCLongitudSuc begin
			--	Print @CCLatitud
			--		Set @vContador1 = @vContador1 +1
			--		print @vContador1
			--		goto seguirsucursal
			--	end

			--end
			--Print Concat('Entro a actualizar Sucursal ',@IDClienteSuc)
			Update ClienteSucursal set Latitud = @CCLatitudSuc, Longitud = @CCLongitudSuc
			where idcliente =  @IDClienteSuc
			and id = @ID
			Set @vContador1 = @vContador1 +1
			--print @vContador1
			--print Concat('Latitud Cast ',@CCLatitudSuc)
			--print Concat('Latitud Cliente ',@LatitudSuc)
seguirsucursal:
			
			fetch next from cActualizaSucursal into @IDClienteSuc, @ID, @CCLatitudSuc ,@CCLongitudSuc ,@LatitudSuc ,@LongitudSuc
			
		End
		
		close cActualizaSucursal 
		deallocate cActualizaSucursal

end

﻿CREATE Procedure [dbo].[SpActualizarActividad]
	
	--Entrada
	@Operacion varchar(10),
	@IDTransaccion numeric(18,0),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	

As

Begin

	--Actualizar los descuentos tacticos
	Declare @vIDTransaccion numeric(18,0)
	Declare @vIDProducto int
	Declare @vIDDescuento int
	Declare @vTotalDescuento money
	Declare @vIDActividad int
	Declare @vIDTipoDescuento tinyint
	Declare @vSaldo money
	Declare @vPorcentajeUtilizado money
	Declare @vExcedente money
	
	Declare db_cursorDescuento cursor for
	Select IDTransaccion, IDProducto, IDDescuento, IDActividad, Total, IDTipoDescuento 
	From VDescuento Where IDTransaccion=@IDTransaccion And IDActividad>0
	
	Open db_cursorDescuento   
	Fetch Next From db_cursorDescuento Into @vIDTransaccion, @vIDProducto, @vIDDescuento, @vIDActividad, @vTotalDescuento, @vIDTipoDescuento
	While @@FETCH_STATUS = 0 Begin  
		
		Declare @vTotal money
		
		If @vIDTipoDescuento = 1 Begin
			
			Set @vTotal = (Select Top(1) Tactico From Actividad Where ID=@vIDActividad)
			
			If @Operacion = 'INS' Begin
				Set @vTotal = @vTotal + @vTotalDescuento
			End
			
			If @Operacion = 'ANULAR' Begin
				Set @vTotal = @vTotal - @vTotalDescuento
			End
			
			If @Operacion = 'DEL' Begin
				Set @vTotal = @vTotal - @vTotalDescuento
			End
			
			Update Actividad Set Tactico=@vTotal Where ID=@vIDActividad
						
		End
		
		If @vIDTipoDescuento = 3 Begin
			
			Set @vTotal = (Select Top(1) Acuerdo From Actividad Where ID=@vIDActividad)
			
			If @Operacion = 'INS' Begin
				Set @vTotal = @vTotal + @vTotalDescuento
			End
			
			If @Operacion = 'ANULAR' Begin
				Set @vTotal = @vTotal - @vTotalDescuento
			End
			
			If @Operacion = 'DEL' Begin
				Set @vTotal = @vTotal - @vTotalDescuento
			End
			
			Update Actividad Set Acuerdo=@vTotal Where ID=@vIDActividad
			
		End

		--Insertar historial
		declare @vIndice numeric(18,0)
		Set @vIndice = (Select ISNull(Max(ID) + 1, 1) From ActividadHistorialDetalle)
		
		Select @vTotal=Total, @vSaldo=Saldo, @vPorcentajeUtilizado=PorcentajeUtilizado, @vExcedente=Excedente
		From Actividad Where ID=@vIDActividad
		
		Insert Into ActividadHistorialDetalle(ID, Fecha, IDActividad, IDTransaccion, IDProducto, IDDescuento, Descuento, IDTipoDescuento, Total, Saldo, PorcentajeUtilizado, Excedente)
		values(@vIndice, GetDate(), @vIDActividad, @vIDTransaccion, @vIDProducto, @vIDDescuento, @vTotalDescuento, @vIDTipoDescuento, @vTotal, @vSaldo, @vPorcentajeUtilizado, @vExcedente)
		
		Fetch Next From db_cursorDescuento Into @vIDTransaccion, @vIDProducto, @vIDDescuento, @vIDActividad, @vTotalDescuento, @vIDTipoDescuento
		
	End
	
	Close db_cursorDescuento   
	Deallocate db_cursorDescuento	

	Set @Mensaje = 'Registro guardado'
	Set @Procesado = 'True'
	
End


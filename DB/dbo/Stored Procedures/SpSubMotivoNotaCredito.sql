﻿CREATE Procedure [dbo].[SpSubMotivoNotaCredito]

	--Entrada
	@ID tinyint,
	@Motivo varchar(50),
	@Descripcion varchar(50),
	@Estado bit,
	@Devolucion bit,
	@Anulacion bit,
	@DiferenciaPrecio bit,
	@DiferenciaPeso bit,
	@Financiero bit,
	@Acuerdo bit,
	@Operacion varchar(10),
		
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From SubMotivoNotaCredito Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
			Select * from SubMotivoNotaCredito
		--Insertamos
		Insert Into SubMotivoNotaCredito(ID, Motivo, Descripcion, Estado, Devolucion, Anulacion,Financiero,DiferenciaPrecio, AcuerdoComercial,DiferenciaPeso)
		Values(@ID, @Motivo, @Descripcion, @Estado, @Devolucion, @Anulacion,@Financiero,@DiferenciaPrecio,@Acuerdo, @DiferenciaPeso)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='SUB MOTIVO NOTA DE CREDITO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From SubMotivoNotaCredito Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From SubMotivoNotaCredito Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		--Si tiene una relacion con Productos
		if exists(Select * From PedidoNotaCredito Where IDSubMotivoNotaCredito=@ID) begin
			set @Mensaje = 'El registro tiene Pedidos de Notas de Credito asociadas! No se puede modificar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update SubMotivoNotaCredito Set Motivo = @Motivo,
										Descripcion=@Descripcion,
										Estado = @Estado,
										Devolucion = @Devolucion,
										Anulacion = @Anulacion,
										Financiero = @Financiero,
										DiferenciaPrecio = @DiferenciaPrecio,
										AcuerdoComercial = @Acuerdo,
										DiferenciaPeso = @DiferenciaPeso

		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='SUB MOTIVO NOTA DE CREDITO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From SubMotivoNotaCredito Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Si tiene una relacion
		if exists(Select * From UsuarioAutorizarPedidoNotaCredito Where IDSubMotivo=@ID) begin
			set @Mensaje = 'El registro tiene configuraciones de aprobacion de pedido de nota de credito asociadas! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Si tiene una relacion con Productos
		if exists(Select * From PedidoNotaCredito Where IDSubMotivoNotaCredito=@ID) begin
			set @Mensaje = 'El registro tiene Pedidos de Notas de Credito asociadas! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
			
		--Eliminamos
		Delete From SubMotivoNotaCredito 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='SUB MOTIVO NOTA DE CREDITO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


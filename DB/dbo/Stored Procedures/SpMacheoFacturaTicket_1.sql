﻿CREATE Procedure [dbo].[SpMacheoFacturaTicket]
	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Numero int = NULL,
	@IDTipoComprobante varchar (50) = NUll,		
	@NroComprobante varchar (100)= Null,
	@IDSucursalOperacion tinyint = NULL,
	@Fecha date = NULL,
	@IDMoneda int = NULL,
	@Cotizacion money= NULL,
	@Observacion varchar(200) = NULL,
	@NroAcuerdo int = NULL,
	@IDTransaccionGasto numeric(18,0) = null,			
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Comprobante
		if Exists(Select * From MacheoFacturaTicket  Where Numero=@Numero And IDSucursal=@IDSucursalOperacion) Begin
			set @Mensaje = 'El numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		
		--Insertar en Transferencia Nota Credito
		Insert Into MacheoFacturaTicket(IDTransaccion, Numero, IDTipoComprobante, NroComprobante, IDSucursal, Fecha, IDMoneda, Cotizacion, Observacion, NroAcuerdo, IDTransaccionGasto,Anulado)
		Values(@IDTransaccionSalida, @Numero,@IDTipoComprobante,@NroComprobante,@IDSucursalOperacion,@Fecha,@IDMoneda,@Cotizacion,@Observacion,@NroAcuerdo,@IDTransaccionGasto, 'False')

		----NotaCreditoAplicacionProcesar
		--EXEC SpNotaCreditoAplicacionProcesar @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output  
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	
	
	
	--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
		
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		--NotaCreditoAplicacionProcesar
		--EXEC SpNotaCreditoAplicacionProcesar @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output  
		
		--Anular
		Update MacheoFacturaTicket   
		Set Anulado='True'
		Where IDTransaccion = @IDTransaccion
		
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
		
	End
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From MacheoFacturaTicket Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		
		--Eliminamos el Detalle
		Delete DetalleMacheo where IDTransaccionMacheo = @IDTransaccion
				
		--Eliminamos el registro
		Delete MacheoFacturaTicket Where IDTransaccion = @IDTransaccion
						
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion				
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		set @IDTransaccionSalida  = 0
		print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = 0
	return @@rowcount
		
End





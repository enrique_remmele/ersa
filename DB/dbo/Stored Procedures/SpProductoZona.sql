﻿
CREATE Procedure [dbo].[SpProductoZona]

	--Entrada
	@IDDeposito smallint,
	@IDZona integer,
	@IDProducto int,
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	Declare @vIDSucursal tinyint
	Set @vIDSucursal = (Select IDSucursal From Deposito Where ID = @IDDeposito)
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la convinacion ya existe
		If Exists(Select * From ProductoZona Where IDSucursal=@vIDSucursal and IDDeposito=@IDDeposito And IDZonaDeposito=@IDZona And IDProducto=@IDProducto) begin
			set @Mensaje = 'La combinacion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Insertamos
		Insert Into ProductoZona(IDSucursal, IDDeposito, IDZonaDeposito, IDProducto)
		Values(@vIDSucursal, @IDDeposito, @IDZona, @IDProducto)		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si es que la convinacion ya existe
		If Exists(Select * From ProductoZona Where IDSucursal=@vIDSucursal and IDDeposito=@IDDeposito And IDZonaDeposito=@IDZona And IDProducto=@IDProducto) begin
			set @Mensaje = 'La combinacion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
			
		
		--Actualizamos
		Update ProductoZona Set	IDDeposito=@IDDeposito,
								IDZonaDeposito=@IDZona,
								IDSucursal = @vIDSucursal
		Where IDProducto=@IDProducto --Se sacaron los demas filtros porque traia los nuevos y no encontraba nada con esa condicion
		--Where IDSucursal=@vIDSucursal and IDDeposito=@IDDeposito And IDZonaDeposito=@IDZona And IDProducto=@IDProducto
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si es que la convinacion ya existe
		If Not Exists(Select * From ProductoZona Where IDSucursal=@vIDSucursal and IDDeposito=@IDDeposito And IDZonaDeposito=@IDZona And IDProducto=@IDProducto) begin
			set @Mensaje = 'La combinacion no existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From ProductoZona 
		Where IDSucursal=@vIDSucursal and IDDeposito=@IDDeposito And IDZonaDeposito=@IDZona And IDProducto=@IDProducto
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


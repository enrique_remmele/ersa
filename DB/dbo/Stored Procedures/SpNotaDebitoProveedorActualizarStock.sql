﻿ CREATE Procedure [dbo].[SpNotaDebitoProveedorActualizarStock]

	--Entrada
	@IDTransaccion numeric(18),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	--Variable
	Declare @vIDDeposito tinyint
	Declare @vIDProducto int
	Declare @vCantidad decimal(10,2)
	
	Set @Mensaje = ''
	Set @Procesado = 'False'
	
	If @Operacion = 'INS' Begin
				
		Declare db_cursor cursor for
		Select DNCP.IDProducto, DNCP.IDDeposito, DNCP.Cantidad From DetalleNotaDebitoProveedor DNCP Join Producto P On DNCP.IDProducto=P.ID Where DNCP.IDTransaccion=@IDTransaccion And P.ControlarExistencia='True'			
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vCantidad 
		While @@FETCH_STATUS = 0 Begin  
		
			--Actualizar Stock
			Exec SpActualizarProductoDeposito @IDDeposito=@vIDDeposito, @IDProducto = @vIDProducto, @Cantidad= @vCantidad, @Signo='+', @Mensaje= @Mensaje, @Procesado = @Procesado
			
			Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vCantidad
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
	End	
		
	
	
	If @Operacion = 'DEL' Begin
	
		
		Declare db_cursor cursor for
		Select DNCP.IDProducto, DNCP.IDDeposito, DNCP.Cantidad From DetalleNotadebitoProveedor DNCP Join Producto P On DNCP.IDProducto=P.ID Where DNCP.IDTransaccion=@IDTransaccion And P.ControlarExistencia='True'			
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vCantidad 
		While @@FETCH_STATUS = 0 Begin  
		
			--Actualizar Stock
			Exec SpActualizarProductoDeposito @IDDeposito=@vIDDeposito, @IDProducto = @vIDProducto, @Cantidad= @vCantidad, @Signo='-', @Mensaje= @Mensaje output, @Procesado = @Procesado output
			
			Fetch Next From db_cursor Into @vIDProducto, @vIDDeposito, @vCantidad
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
	End
	
End
	


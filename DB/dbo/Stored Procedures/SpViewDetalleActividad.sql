﻿CREATE Procedure [dbo].[SpViewDetalleActividad]
	--Identificadores
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int
	
As

Begin

	Declare @vFecha date
	Set @vFecha = convert(date, (SELECT TOP 1 VentaFechaFacturacion FROM dbo.Configuraciones Where IDSucursal=@IDSucursal))
	
	Select 
	DA.IDActividad,
	DA.ID,
	DA.IDProducto,
	
	'Producto'=P.Descripcion,
	D.*,
	PLA.Desde,
	PLA.Hasta
	
	From DetalleActividad DA
	Join Producto P On DA.IDProducto=P.ID
	Join VDetallePlanillaDescuentoTactico D On DA.IDActividad=D.ID
	Join VPlanillaDescuentoTactico PLA On D.IDTransaccion=PLA.IDTransaccion
	
	Where D.IDSucursal=@IDSucursal 
	And @vFecha Between PLA.Desde And PLA.Hasta
	AND PLA.Anulado = 0
	
	
	Order By DA.IDActividad, DA.ID	
End



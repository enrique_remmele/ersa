﻿create Procedure [dbo].[SpAplicacionAnticipoAProveedoresDetalle]

	@IDTransaccion numeric(18,0) = NULL,
	@IDTransaccionOrdenPago numeric(18,0) = NULL,
	@IDTransaccionProveedores numeric(18,0) = NULL,
	@Fecha date = NULL,
	@TotalEgreso money = 0,
	@TotalOrdenPago money = 0,
		
	--Operacion
	@Operacion varchar(50),

	--@vIdTransaccion numeric(18,0) = NULL,

	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
	
	
As

Begin
	--Variables
	 Declare @vCancelado bit

	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			--set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	If @Operacion = 'INS' Begin	

       --select * from AplicacionAnticipoProveedoresDetalle where IDTransaccion = @IDTransaccion		
		set @IDTransaccionSalida = 	@IDTransaccion
		--Insertar en Orden de Pago
		Insert Into AplicacionAnticipoProveedoresDetalle(IDTransaccion,IdTransaccionOrdenPago,IdTransaccionProveedores,Fecha,TotalEgreso,TotalOrdenPago)
		Values(@IDTransaccion,@IdTransaccionOrdenPago,@IdTransaccionProveedores,@Fecha,@TotalEgreso,@TotalOrdenPago)
		
		--Actualiza el estado de la Orden de Pago para que ya no habilite en las proximas busquedas
		update OrdenPago
		set Cancelado =  'True'
		where IDTransaccion = @IDTransaccionOrdenPago

		set @Mensaje = 'Registro guardado y Procesado'
	    set @Procesado = 'True'
		return @@rowcount	
	
	End

	If @Operacion = 'ANULAR' Begin
	
		--Actualiza el estado de la Orden de Pago para que ya no habilite en las proximas busquedas
		update OrdenPago
		set Cancelado =  'False'
		where IDTransaccion = @IDTransaccionOrdenPago
		
		exec SpRepararTodosLosEgresos

		set @Mensaje = 'Registro Anulado y Guardado'
		set @Procesado = 'True'
		return @@rowcount	
		
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
	
	
End

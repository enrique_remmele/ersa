﻿CREATE Procedure [dbo].[SpDeposito]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@IDSucursal tinyint,
	@IDSucursalDestinoTransito tinyint = 0,
	@IDTipoDeposito tinyint,
	@CuentaContableMercaderia varchar(10) = '',
	@CuentaContableCombustible varchar(10)='',
	@Estado bit,
	@Operacion varchar(10),
	@Compra bit = 'false',
	@Venta bit = 'false',
	@DescargaStock bit = 'False',
	@ConsumoCombustible bit = 'False',
	@MovimientoMateriaPrima bit = 'False',
	@DescargaCompra bit = 'False',

	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Deposito Where Descripcion=@Descripcion And IDSucursal=@IDSucursal) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDDeposito tinyint
		set @vIDDeposito = (Select IsNull((Max(ID)+1), 1) From Deposito)

		--Insertamos
		Insert Into Deposito(ID, Descripcion, IDSucursal, IDSucursalDestinoTransito,IDTipoDeposito,CuentaContableMercaderia, CuentaContableCombustible, Estado, Compra, Venta, DescargaStock, ConsumoCombustible, MovimientoMateriaPrima, DescargaCompra)
		Values(@vIDDeposito, @Descripcion, @IDSucursal, @IDSucursalDestinoTransito, @IDTipoDeposito, @CuentaContableMercaderia, @CuentaContableCombustible, @Estado, @Compra, @Venta, @DescargaStock, @ConsumoCombustible, @MovimientoMateriaPrima, @DescargaCompra)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='DEPOSITO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From Deposito Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Deposito Where Descripcion=@Descripcion And ID!=@ID And IDSucursal=@IDSucursal) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Deposito Set Descripcion=@Descripcion, 
						IDSucursal=@IDSucursal,
						IDTipoDeposito = @IDTipoDeposito,
						CuentaContableMercaderia=@CuentaContableMercaderia,
						CuentaContableCombustible=@CuentaContableCombustible,
						Estado = @Estado,
						Compra = @Compra,
						Venta = @Venta,
						DescargaStock = @DescargaStock,
						ConsumoCombustible = @ConsumoCombustible,
						MovimientoMateriaPrima = @MovimientoMateriaPrima,
						DescargaCompra = @DescargaCompra,
						IDSucursalDestinoTransito = @IDSucursalDestinoTransito
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='DEPOSITO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Deposito Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una transaccion
		if exists(Select * From Transaccion Where IDDeposito=@ID) begin
			set @Mensaje = 'El registro tiene transacciones asociadas! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene productos asociados
		if exists(Select * From ExistenciaDeposito Where IDDeposito=@ID) begin
			set @Mensaje = 'El registro tiene productos asociadas! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		
		--Eliminamos
		Delete From Deposito 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='DEPOSITO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


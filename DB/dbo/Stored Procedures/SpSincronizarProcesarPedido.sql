﻿CREATE Procedure [dbo].[SpSincronizarProcesarPedido]

	--Entrada
	@IDTransaccion	numeric(18, 0),
	
	--Pedido
	@IDVendedor int,
	@IDCliente int,
	@Fecha date,
	@TotalPedido money,
	
	--Transaccion
	@IDOperacion int,
	@IDUsuario int,
	@IDSucursalTransaccion int,
	@IDDepositoTransaccion int,
	@IDTerminal int
	
As

Begin

	Declare @vIDTransaccion numeric(18,0)
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	
	Set @vMensaje = 'No se proceso'
	Set @vProcesado = 'False'
	
	--Verificar si existe
	If Not Exists(Select * From PedidoMovil Where IDVendedor=@IDVendedor And IDCliente=@IDCliente And DATEDIFF(dd,Fecha, @Fecha) = 0 And Total=@TotalPedido) Begin
		Set @vMensaje = 'El registro no se encuentra!'
		Set @vProcesado = 'False'
		GoTo Salir
	End 
 
	--Si no existe en Pedido
	Set @vIDTransaccion = (Select Top(1) IDTransaccion From PedidoMovil Where IDVendedor=@IDVendedor And IDCliente=@IDCliente And DATEDIFF(dd,Fecha, @Fecha) = 0 And Total=@TotalPedido)
	
	--Si ya esta procesado, no hacer nada
	If (Select Procesado From Pedido Where IDTransaccion=@vIDTransaccion) = 'True' Begin
		
		--Salir
		Set @vMensaje = 'El registro ya esta procesado'
		Set @vProcesado = 'True'
		GoTo Salir
		
	End
	
	--Procesar
	print @vIDTransaccion
	
	Exec SpPedidoProcesar @IDTransaccion=@vIDTransaccion
	
	Set @vMensaje = 'Registro Guardado!'
	Set @vProcesado = 'True'
	GoTo Salir
			
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


﻿CREATE Procedure [dbo].[SpMotivoAnulacionVenta]

	--Entrada
	@ID int=NULL,
	@Descripcion varchar(50)=NULL,
	@Estado bit=1,
	@MantenerPedidoVigente bit = 'False',
	
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From MotivoAnulacionVenta Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDMotivoAnulacionVenta int
		set @vIDMotivoAnulacionVenta = (Select IsNull((Max(ID)+1), 1) From MotivoAnulacionVenta)

		--Insertamos
		Insert Into MotivoAnulacionVenta(ID, Descripcion, Estado, MantenerPedidoVigente)
		Values(@vIDMotivoAnulacionVenta, @Descripcion, @Estado, @MantenerPedidoVigente)	
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='MOTIVOANULACIONVENTA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
					
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From MotivoAnulacionVenta Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From MotivoAnulacionVenta Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update MotivoAnulacionVenta Set Descripcion=@Descripcion, 
										MantenerPedidoVigente=@MantenerPedidoVigente,
										Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='MOTIVOANULACIONVENTA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
					
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From MotivoAnulacionVenta Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Venta
		if exists(Select * From Venta Where IDMotivo=@ID) begin
			set @Mensaje = 'El registro tiene ventas asociadas! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From MotivoAnulacionVenta 
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='MOTIVOANULACIONVENTA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
					
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End
﻿CREATE  Procedure [dbo].[SpEfectivizacionProcesar]

	--Entrada
	@IDTransaccion numeric(18),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	--Validar Feha Operacion
	Declare @vIDSucursal as integer
	Declare @vFecha as Date
	Declare @vIDUsuario as integer
	Declare @vIDOperacion as integer

	Select	@vIDSucursal=IDSucursal,
			@vFecha=Fecha,
			@vIDUsuario=IDUsuario,
			@vIDOperacion=IDOperacion
	 From Transaccion Where ID = @IDTransaccion

	If dbo.FValidarFechaOperacion(@vIDSucursal, @vFecha, @vIDUsuario, @vIDOperacion) = 'False' Begin
		Set @Mensaje  = 'Fecha fuera de rango permitido!!'
		Set @Procesado = 'False'
		Return @@rowcount
	End

    ---Variables para CALCULAR SALDO DEPOSITADO
	declare @Saldo money
	declare @Cancelado bit

		
	--Declarar variables Variables
	declare @vIDTransaccionEfectivo numeric(18,0)
	declare @vIDEfectivo tinyint
	declare @vImporte money
				
	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'


	If @Operacion = 'INS' Begin
		
		--Procesar los Efectivos
		Begin
			Declare db_cursor cursor for
			Select IDTransaccionEfectivo, IDEfectivo, Importe From DetalleEfectivizacion 
			Where IDTransaccion=@IDTransaccion
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionEfectivo,@vIDEfectivo,@vImporte
			While @@FETCH_STATUS = 0 Begin  
				
				Set @Cancelado  = 'False'
				
				--CALCULAR SALDO 
				Set @Saldo = (Select Saldo From Efectivo Where IDTransaccion=@vIDTransaccionEfectivo)  
								--
				If @Saldo > @vImporte Begin
					Set @Saldo = @Saldo - @vImporte
				End Else Begin
					set @Saldo = @vImporte - @Saldo 
				End 
				
						
				--Verificar si se cancela el Efectivo
				If @Saldo = 0 Begin
					Set @Cancelado = 'True'  
				End
					
				----ACTUALIZAR LA TABLA EFECTIVO			
				Update Efectivo  Set Saldo= @Saldo,
									 Cancelado=@Cancelado    
				Where IDTransaccion= @vIDTransaccionEfectivo
				
				Fetch Next From db_cursor Into @vIDTransaccionEfectivo,@vIDEfectivo,@vImporte
								
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		End	
		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
			
	End	
	
	
	If @Operacion = 'ANULAR' Begin	 
					
			--Procesar los Efectivos
		Begin
			Declare db_cursor cursor for
			Select IDTransaccionEfectivo, IDEfectivo, Importe From DetalleEfectivizacion 
			Where IDTransaccion=@IDTransaccion
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionEfectivo,@vIDEfectivo,@vImporte
			While @@FETCH_STATUS = 0 Begin  
				
				Set @Cancelado  = 'False'
				
				--CALCULAR SALDO 
				Set @Saldo = (Select Saldo From Efectivo Where IDTransaccion=@vIDTransaccionEfectivo)  
								--
				Set @Saldo = @Saldo + @vImporte
								
								
				----ACTUALIZAR LA TABLA EFECTIVO			
				Update Efectivo  Set Saldo= @Saldo,
									 Cancelado=@Cancelado    
				Where IDTransaccion= @vIDTransaccionEfectivo
				
				Fetch Next From db_cursor Into @vIDTransaccionEfectivo,@vIDEfectivo,@vImporte
								
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
		End	
		
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
	
	
	
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	return @@rowcount
		
	
End
	

	






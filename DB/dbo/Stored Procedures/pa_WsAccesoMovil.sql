﻿

CREATE Procedure [dbo].[pa_WsAccesoMovil] /*JGR 20140715*/

	--Entrada
	@Usuario varchar(50),
	@Password varchar(300)

As

Begin

	Declare @vIDUsuario int
	Declare @vMensaje varchar(150)
	Declare @vProcesado bit
	
	Set @vMensaje = ''
	Set @vProcesado = 'False'
	Set @vIDUsuario = 0
	
	--Si las credenciales no existen
	Set @Password = Convert(varchar(300), HASHBYTES('Sha1', @Password))
	SET @vIDUsuario = (Select TOP 1 ID From Usuario where Usuario=@Usuario And [Password]=@Password AND Estado=1)
	If @vIDUsuario<=0 Begin
			Set @vMensaje = 'Credenciales no validas!'
			Set @vProcesado = 'False'
			GoTo Salir
	End
		
	Set @vMensaje = 'Acceso correcto!'
	Set @vProcesado = 'True'
		
Salir:
	If @vProcesado = 'True' Begin
		Select V.*, 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado From VUsuario V Where V.ID=@vIDUsuario
	End Else Begin
		Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
	End	
End



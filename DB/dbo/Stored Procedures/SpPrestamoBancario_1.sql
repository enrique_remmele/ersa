﻿CREATE Procedure [dbo].[SpPrestamoBancario]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Numero Int = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante varchar(50) = NULL,
	@Fecha date = NULL,
	@IDCuentaBancaria tinyint = NULL,
	@Cotizacion money = NULL,
	@Observacion varchar(500) = NULL,
	@PlazoDias int = NULL,
	@PorcentajeInteres numeric(4,2) = NULL,
	@Capital money = 0,
	@Interes money = NULL,
	@ImpuestoInteres money = NULL,
	@Gastos money = NULL,
	@Neto money = 0,
	@TipoGarantia varchar(50) = null,
	@Operacion varchar(10),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
	
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from PrestamoBancario where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
	SET @Numero = REPLACE(@Numero,'.','')
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero --Agregando control por IDsucursal
		If Exists(Select * From VPrestamoBancario Where Numero=@Numero AND IDSucursal = @IDSucursal) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		------Insertar
		Insert Into PrestamoBancario(IDTransaccion, Numero,IDTipoComprobante,NroComprobante,IDSucursal,Fecha,IDCuentaBancaria,Cotizacion,Observacion,PlazoDias,PorcentajeInteres,Capital,Interes,ImpuestoInteres,Gastos,Neto,Anulado,TipoGarantia)
		Values(@IDTransaccionSalida, @Numero, @IDTipoComprobante, @NroComprobante, @IDSucursal, @Fecha, @IDCuentaBancaria, @Cotizacion, @Observacion, @PlazoDias, @PorcentajeInteres,@Capital,@Interes,@ImpuestoInteres,@Gastos,@Neto,'False',@TipoGarantia)
				
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From PrestamoBancario Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--IDTransaccion debito credito bancario
		If Not Exists(Select * From PrestamoBancario Where IDTransaccionDebitoCreditoBancario Is Not Null) Begin
			set @Mensaje = 'El prestamo está asociado a un Débito Crédito Bancario.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--IDTransaccion debito credito bancario en detalle prestamo
		If Not Exists(Select * From DetallePrestamoBancario Where IDTransaccionDebitoCreditoBancario Is Not Null) Begin
			set @Mensaje = 'Una o más cuotas están asociadas a un Débito Crédito Bancario.'
			set @Procesado = 'False'
			return @@rowcount
		End

			--IDTransaccion debito credito bancario en detalle prestamo
		If Exists(Select * From DetalleCuotaPrestamoBancario Where IDTransaccionPrestamoBancario = @IDTransaccion) Begin
			set @Mensaje = 'Una o más cuotas están asociadas a por lo menos un Débito Crédito Bancario.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que no este anulado
		if (Select Anulado From PrestamoBancario Where IDTransaccion=@IDTransaccion) = 'True' Begin
			set @Mensaje = 'El registro ya esta anulado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		----Verificar la configuracion de anulacion
		--If (Select Top(1) MovimientoBloquearAnulacion From Configuraciones) = 'True' Begin
		--	Declare @vDias tinyint
		--	Set @vDias = (Select Top(1) MovimientoDiasBloqueo From Configuraciones)

		--	If (Select DATEDIFF(dd,(Select Fecha From Movimiento Where IDTransaccion=@IDTransaccion), GETDATE())) > @vDias Begin
			 			
		--		set @Mensaje = 'El sistema no puede anular este registro ya que la configuracion lo restringe por la antigüedad del documento. Cambie la configuracion de bloqueo para anular!'
		--		set @Procesado = 'False'
		--		return @@rowcount
				
		--	End
			
		--End
		 	
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
				
		--Anulamos el registro
		Update PrestamoBancario Set Anulado = 'True'
		Where IDTransaccion = @IDTransaccion
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From PrestamoBancario Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que no este anulado
					
		--Actualizamos el Stock
		
		--Insertamos el registro de anulacion
		
		--Anulamos el registro
		
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End


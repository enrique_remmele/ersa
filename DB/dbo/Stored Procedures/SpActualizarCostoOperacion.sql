﻿CREATE Procedure [dbo].[SpActualizarCostoOperacion]

	@IDTransaccion int,
	@IDTransaccionOperacion int,
	@IDProducto int,
	@CostoAnterior decimal(18,3),
	@Costo decimal(18,3),
	@CantidadSaldo decimal(18,2),
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

BEGIN	
	
	If Not Exists(Select ID From Transaccion Where ID=@IDTransaccionOperacion) Begin
		--Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	Declare @IDImpuesto as int
	Declare @FactorImpuesto as decimal(18,2)
	Declare @Total as int --Total de todos los productos con el mismo tipo de impuesto
	Declare @TotalImpuesto as decimal(18,3)--IVA de todos los productos con el mismo tipo de impuesto
	Declare @TotalDiscriminado as decimal(18,3)--Discriminado de todos los productos con el mismo tipo de impuesto

	--Nota de Crédito Cliente
	if exists (Select *	From VNotaCredito NC join VDetalleNotaCredito DNC on NC.IDTransaccion=DNC.IDTransaccion Join Transaccion T On NC.IDTransaccion=T.ID Where NC.Procesado='True' and NC.IDTransaccion = @IDTransaccionOperacion) begin
		goto Salir
	End

	--Movimiento Entrada
	If exists (Select * From VMovimiento M join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion Join Deposito D On DM.IDDepositoEntrada=D.ID Join Transaccion T On M.IDTransaccion=T.ID Where DM.Entrada='True'and DM.Salida='False' and M.IDTransaccion = @IDTransaccionOperacion) begin
		-- Set @CostoAnterior = (Select Top(1) PrecioUnitario from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto)
		Set @IDImpuesto = (Select Top(1)IDImpuesto from Producto where id = @IDProducto)
		Set @FactorImpuesto = (Select FactorImpuesto from Impuesto where id = @IDImpuesto)


		Update DetalleMovimiento Set PrecioUnitario = @Costo,
									Total = @Costo * Cantidad,
									TotalImpuesto = (@Costo * Cantidad)/@FactorImpuesto,
									TotalDiscriminado = (@Costo * Cantidad) - ((@Costo * Cantidad)/@FactorImpuesto)
									Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto
					
		insert into aControlCostoDetalle values(@IDTransaccion, @IDTransaccionOperacion, @IDProducto, @CostoAnterior, @Costo, @CantidadSaldo)
		select  top(0) * from aControlCostoDetalle
		Set @Total = (Select Total from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalDiscriminado = (Select TotalDiscriminado from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalImpuesto = (Select TotalImpuesto from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)

		goto ActiualizarImpuesto
	End

	--Movimiento Salida Anulada
	if exists (Select * From VMovimiento M  join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion Join Deposito D On DM.IDDepositoSalida=D.ID Join Transaccion T On M.IDTransaccion=T.ID Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion Where DM.Entrada='False'And DM.Salida='True' And M.Anulado = 'True' and M.IDTransaccion = @IDTransaccionOperacion) begin
		-- Set @CostoAnterior = (Select Top(1) PrecioUnitario from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto)
		Set @IDImpuesto = (Select Top(1)IDImpuesto from Producto where id = @IDProducto)
		Set @FactorImpuesto = (Select FactorImpuesto from Impuesto where id = @IDImpuesto)


		Update DetalleMovimiento Set PrecioUnitario = @Costo,
									Total = @Costo * Cantidad,
									TotalImpuesto = (@Costo * Cantidad)/@FactorImpuesto,
									TotalDiscriminado = (@Costo * Cantidad) - ((@Costo * Cantidad)/@FactorImpuesto)
									Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto
		insert into aControlCostoDetalle values(@IDTransaccion, @IDTransaccionOperacion, @IDProducto, @CostoAnterior, @Costo, @CantidadSaldo)

		Set @Total = (Select Total from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalDiscriminado = (Select TotalDiscriminado from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalImpuesto = (Select TotalImpuesto from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)

		goto ActiualizarImpuesto
	end
	--Transferencia Entrada
	if exists(Select * From VMovimiento M join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion Join Deposito D On DM.IDDepositoEntrada=D.ID Join Transaccion T On M.IDTransaccion=T.ID Where DM.Entrada='True'and DM.Salida='True' and M.IDTransaccion = @IDTransaccionOperacion) begin
		-- Set @CostoAnterior = (Select Top(1) PrecioUnitario from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto)
		Set @IDImpuesto = (Select Top(1)IDImpuesto from Producto where id = @IDProducto)
		Set @FactorImpuesto = (Select FactorImpuesto from Impuesto where id = @IDImpuesto)


		Update DetalleMovimiento Set PrecioUnitario = @Costo,
									Total = @Costo * Cantidad,
									TotalImpuesto = (@Costo * Cantidad)/@FactorImpuesto,
									TotalDiscriminado = (@Costo * Cantidad) - ((@Costo * Cantidad)/@FactorImpuesto)
									Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto
		insert into aControlCostoDetalle values(@IDTransaccion, @IDTransaccionOperacion, @IDProducto, @CostoAnterior, @Costo, @CantidadSaldo)

		Set @Total = (Select Total from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalDiscriminado = (Select TotalDiscriminado from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalImpuesto = (Select TotalImpuesto from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)

		goto ActiualizarImpuesto
	End

	--Transferencia Salida Anulada
	if exists(Select * From VMovimiento M join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion Join Deposito D On DM.IDDepositoSalida=D.ID Join Transaccion T On M.IDTransaccion=T.ID Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion Where DM.Entrada='True'And DM.Salida='True'	And M.Anulado = 'True' and M.IDTransaccion = @IDTransaccionOperacion) begin
		-- Set @CostoAnterior = (Select Top(1) PrecioUnitario from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto)
		Set @IDImpuesto = (Select Top(1)IDImpuesto from Producto where id = @IDProducto)
		Set @FactorImpuesto = (Select FactorImpuesto from Impuesto where id = @IDImpuesto)


		Update DetalleMovimiento Set PrecioUnitario = @Costo,
									Total = @Costo * Cantidad,
									TotalImpuesto = (@Costo * Cantidad)/@FactorImpuesto,
									TotalDiscriminado = (@Costo * Cantidad) - ((@Costo * Cantidad)/@FactorImpuesto)
									Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto
		insert into aControlCostoDetalle values(@IDTransaccion, @IDTransaccionOperacion, @IDProducto, @CostoAnterior, @Costo, @CantidadSaldo)

		Set @Total = (Select Total from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalDiscriminado = (Select TotalDiscriminado from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalImpuesto = (Select TotalImpuesto from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)

		goto ActiualizarImpuesto
	end

	--Movimiento Salida
	if exists(Select * From VMovimiento M join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion Join Deposito D On DM.IDDepositoSalida=D.ID Join Transaccion T On M.IDTransaccion=T.ID	Where DM.Entrada='False'and DM.Salida='True' and M.IDTransaccion=@IDTransaccionOperacion) begin
		-- Set @CostoAnterior = (Select Top(1) PrecioUnitario from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto)
		Set @IDImpuesto = (Select Top(1)IDImpuesto from Producto where id = @IDProducto)
		Set @FactorImpuesto = (Select FactorImpuesto from Impuesto where id = @IDImpuesto)


		Update DetalleMovimiento Set PrecioUnitario = @Costo,
									Total = @Costo * Cantidad,
									TotalImpuesto = (@Costo * Cantidad)/@FactorImpuesto,
									TotalDiscriminado = (@Costo * Cantidad) - ((@Costo * Cantidad)/@FactorImpuesto)
									Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto
		insert into aControlCostoDetalle values(@IDTransaccion, @IDTransaccionOperacion, @IDProducto, @CostoAnterior, @Costo, @CantidadSaldo)

		Set @Total = (Select Total from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalDiscriminado = (Select TotalDiscriminado from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalImpuesto = (Select TotalImpuesto from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)

		goto ActiualizarImpuesto
	End

	--Movimiento Entrada Anulado
	if exists(Select * From VMovimiento M  join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion Join Deposito D On DM.IDDepositoEntrada=D.ID Join Transaccion T On M.IDTransaccion=T.ID Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion Where DM.Entrada='True'And DM.Salida='False' And M.Anulado = 'True' and M.IDTransaccion = @IDTransaccionOperacion) begin
		-- Set @CostoAnterior = (Select Top(1) PrecioUnitario from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto)
		Set @IDImpuesto = (Select Top(1)IDImpuesto from Producto where id = @IDProducto)
		Set @FactorImpuesto = (Select FactorImpuesto from Impuesto where id = @IDImpuesto)


		Update DetalleMovimiento Set PrecioUnitario = @Costo,
									Total = @Costo * Cantidad,
									TotalImpuesto = (@Costo * Cantidad)/@FactorImpuesto,
									TotalDiscriminado = (@Costo * Cantidad) - ((@Costo * Cantidad)/@FactorImpuesto)
									Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto
		insert into aControlCostoDetalle values(@IDTransaccion, @IDTransaccionOperacion, @IDProducto, @CostoAnterior, @Costo, @CantidadSaldo)

		Set @Total = (Select Total from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalDiscriminado = (Select TotalDiscriminado from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalImpuesto = (Select TotalImpuesto from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)

		goto ActiualizarImpuesto
	End


	--Transferencia Salida
	if exists(Select * From VMovimiento M  join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion Join Deposito D On DM.IDDepositoSalida=D.ID Join Transaccion T On M.IDTransaccion=T.ID Where DM.Entrada='True'and DM.Salida='True' and M.IDTransaccion = @IDTransaccionOperacion) begin
		-- Set @CostoAnterior = (Select Top(1) PrecioUnitario from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto)
		Set @IDImpuesto = (Select Top(1)IDImpuesto from Producto where id = @IDProducto)
		Set @FactorImpuesto = (Select FactorImpuesto from Impuesto where id = @IDImpuesto)


		Update DetalleMovimiento Set PrecioUnitario = @Costo,
									Total = @Costo * Cantidad,
									TotalImpuesto = (@Costo * Cantidad)/@FactorImpuesto,
									TotalDiscriminado = (@Costo * Cantidad) - ((@Costo * Cantidad)/@FactorImpuesto)
									Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto
		insert into aControlCostoDetalle values(@IDTransaccion, @IDTransaccionOperacion, @IDProducto, @CostoAnterior, @Costo, @CantidadSaldo)

		Set @Total = (Select Total from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalDiscriminado = (Select TotalDiscriminado from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalImpuesto = (Select TotalImpuesto from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)

		goto ActiualizarImpuesto
	End
	
	--Transferencia Entrada Anulada
	if exists(Select * From VMovimiento M  join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion  Join Deposito D On DM.IDDepositoEntrada=D.ID Join Transaccion T On M.IDTransaccion=T.ID	Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion Where DM.Entrada='True'And DM.Salida='True' And M.Anulado = 'True' and M.IDTransaccion = @IDTransaccionOperacion) begin
		-- Set @CostoAnterior = (Select Top(1) PrecioUnitario from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto)
		Set @IDImpuesto = (Select Top(1)IDImpuesto from Producto where id = @IDProducto)
		Set @FactorImpuesto = (Select FactorImpuesto from Impuesto where id = @IDImpuesto)
		
		Update DetalleMovimiento Set PrecioUnitario = @Costo,
									Total = @Costo * Cantidad,
									TotalImpuesto = (@Costo * Cantidad)/@FactorImpuesto,
									TotalDiscriminado = (@Costo * Cantidad) - ((@Costo * Cantidad)/@FactorImpuesto)
									Where IDTransaccion = @IDTransaccionOperacion and IDProducto = @IDProducto
		insert into aControlCostoDetalle values(@IDTransaccion, @IDTransaccionOperacion, @IDProducto, @CostoAnterior, @Costo, @CantidadSaldo)

		Set @Total = (Select Total from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalDiscriminado = (Select TotalDiscriminado from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)
		Set @TotalImpuesto = (Select TotalImpuesto from DetalleMovimiento Where IDTransaccion = @IDTransaccionOperacion and IDImpuesto = @IDImpuesto)

		goto ActiualizarImpuesto
	End

	insert into aControlCostoDetalle values(@IDTransaccion, @IDTransaccionOperacion, @IDProducto, @CostoAnterior, @Costo, 0)
	goto Salir

ActiualizarImpuesto:
	Exec SpDetalleImpuesto
		@IDTransaccion = @IDTransaccionOperacion,
		@IDImpuesto =@IDImpuesto,
		@Total =@Total,
		@TotalImpuesto =@TotalImpuesto,
		@TotalDiscriminado =@TotalDiscriminado,
		@TotalDescuento  = NULL,
		@Operacion = 'UPD',
		@Mensaje = @Mensaje,
		@Procesado =@Procesado
		


Salir:

	return @@rowcount	



End


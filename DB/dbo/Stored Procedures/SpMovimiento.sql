﻿CREATE Procedure [dbo].[SpMovimiento]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Numero VARCHAR(50) = NULL,
	@Fecha date = NULL,
	@IDTipoOperacion tinyint = NULL,
	@IDMotivo tinyint = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante varchar(50) = NULL,
	@IDDepositoSalida tinyint = NULL,
	@IDDepositoEntrada tinyint = NULL,
	@Observacion varchar(200) = NULL,
	@Autorizacion varchar(50) = NULL,
	@Total money = NULL,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@Operacion varchar(50),

	@MovimientoStock bit = 'False',
	@MovimientoCombustible bit = 'False',
	@MovimientoMateriaPrima bit = 'False',
	@DescargaStock bit = 'False',
	@DescargaCompra bit = 'False',
	@Kilometro money = 0,
	@IDChofer int = null,
	@IDCamion int = null,
	@IDDestinoCombustible int = null,
	@Enviar bit = null,
	@IDTransaccionEnvio int = null,
	@Recibir bit = null,
	@Recibido bit = null,

	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin	
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
		--Validar
		If dbo.FMovimientoHabilitado(@IDTipoComprobante, @IDTipoOperacion) = 0 Begin
			Set @Mensaje  = 'Ver relacion Comprobante y Tipo de Operacion!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 


	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from Movimiento where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--Fecha de la compra que no sea mayor a hoy
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If @Fecha > getdate() Begin
				set @Mensaje = 'La fecha del comprobante no debe ser mayor a hoy!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
		End
	END

	--BLOQUES
	SET @Numero = REPLACE(@Numero,'.','')
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		
		
		
		--Establecer los depositos
		if (Select Entrada From TipoOperacion Where ID=@IDTipoOperacion) = 0 Begin
			set @IDDepositoEntrada = NULL
		End
		
		if (Select Salida From TipoOperacion Where ID=@IDTipoOperacion) = 0 Begin
			set @IDDepositoSalida = NULL
		End
		
		if ISNUll(@IDDepositoEntrada,0)>0 and ISNUlL(@IDDepositoSalida,0)>0 begin
			if isnull(@Enviar,'False') = 'False' and isnull(@Recibir,'False')= 'False' begin
				Set @Mensaje  = 'Es necesario actualizar el sistema para guardar la operacion!!'
				Set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				Return @@rowcount
			end
		end

		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		------Insertar
		If @Numero=@NroComprobante Begin
			Set @Numero = IsNull((Select MAX(Numero)+1 From VMovimiento Where IDSucursal = @IDSucursal),1)
			Set @NroComprobante = CONVERT(varchar(50), @Numero)	
		End Else Begin
			Set @Numero = IsNull((Select MAX(Numero)+1 From VMovimiento Where IDSucursal = @IDSucursal),1)	
		End

		--Numero --JGR 20140814 Agregando control por IDsucursal
		If Exists(Select * From VMovimiento Where Numero=@Numero AND IDSucursal = @IDSucursal) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		if ISNUll(@IDDepositoEntrada,0)>0 and ISNUlL(@IDDepositoSalida,0)>0 begin
			if (select isnull(IDSucursalDestinoTransito,idsucursal) from deposito where id = @IDDepositoEntrada) = (select idsucursal from deposito where id = @IDDepositoSalida) begin
				Set @Enviar = 0
			end
		end


		Insert Into Movimiento(IDTransaccion, Numero,Fecha,IDTipoOperacion,IDMotivo,IDTipoComprobante,NroComprobante,IDDepositoSalida,IDDepositoEntrada,Observacion,Autorizacion,Total, TotalImpuesto, TotalDiscriminado, Anulado, MovimientoStock, MovimientoCombustible, DescargaStock, Kilometro, IDChofer, IDCamion,IDDestinoCombustible, MovimientoMateriaPrima, DescargaCompra, Enviar,Recibir,Recibido,IDTransaccionEnvio)
		Values(@IDTransaccionSalida, @Numero, @Fecha, @IDTipoOperacion, @IDMotivo, @IDTipoComprobante, @NroComprobante, @IDDepositoSalida, @IDDepositoEntrada, @Observacion, @Autorizacion, @Total, @TotalImpuesto, @TotalDiscriminado, 'False', @MovimientoStock, @MovimientoCombustible, @DescargaStock, @Kilometro, @IDChofer, @IDCamion, @IDDestinoCombustible,@MovimientoMateriaPrima, @DescargaCompra,@Enviar,@Recibir,@Recibido,@IDTransaccionEnvio)
				

		if (isnull(@Recibir,'False') = 'True') begin
			if (@IDTransaccionEnvio > 0 ) begin
				update movimiento 
					set Recibido  = 'True'
					where idtransaccion = @IDTransaccionEnvio
			end
		end

		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Movimiento Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que no este anulado
		if (Select Anulado From Movimiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			set @Mensaje = 'El registro ya esta anulado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Verificar la configuracion de anulacion
		If (Select Top(1) MovimientoBloquearAnulacion From Configuraciones) = 'True' Begin
			Declare @vDias tinyint
			Set @vDias = (Select Top(1) MovimientoDiasBloqueo From Configuraciones)

			If (Select DATEDIFF(dd,(Select Fecha From Movimiento Where IDTransaccion=@IDTransaccion), GETDATE())) > @vDias Begin
			 			
				set @Mensaje = 'El sistema no puede anular este registro ya que la configuracion lo restringe por la antigüedad del documento. Cambie la configuracion de bloqueo para anular!'
				set @Procesado = 'False'
				return @@rowcount
				
			End
			
		End
		
		--Actualizamos el Stock
		Exec SpDetalleMovimiento @IDTransaccion=@IDTransaccion, @Operacion='ANULAR', @Mensaje=@Mensaje output, @Procesado=@Procesado output
		
		--Salimos si actualizar stock no fue satisfactorio
		If @Procesado = 0 Begin
			print 'El stock no se actualizo correctamente. [SpMovimiento] Bloque: ANULAR '
			return @@rowcount
		End
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal

		--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
		
		--Anulamos el registro
		Update Movimiento Set Anulado = 'True'
		Where IDTransaccion = @IDTransaccion
		
		update DevolucionSinNotaCredito set IDTransaccionMovimientoStock =0
			where IDTransaccionMovimientoStock =@IDTransaccion

		Set @IDTransaccionEnvio = (Select isnull(IDTransaccionEnvio,0) from Movimiento where IDTransaccion = @IDTransaccion)
			if (@IDTransaccionEnvio > 0 ) begin
				update movimiento 
					set Recibido  = 'False'
				where idtransaccion = @IDTransaccionEnvio
		end
		
		print '[SpMovimiento] ANULADO'
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Movimiento Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que no este anulado
					
		--Actualizamos el Stock
		
		--Insertamos el registro de anulacion
		
		--Anulamos el registro
		
	End

	If @Operacion = 'UPD' Begin
		Update Movimiento 
		set IDTipoComprobante = @IDTipoComprobante,
		Numero = @Numero,
		IDMotivo = @IDMotivo,
		NroComprobante = @NroComprobante,
		Observacion = @Observacion
		where IDTransaccion = @IDTransaccion

		set @Mensaje = 'Registro Modificado'
		set @Procesado = 'True'
		return @@rowcount
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End


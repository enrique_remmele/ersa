﻿
CREATE Procedure [dbo].[SpPedidoActualizarFechaOperacion]

	--Entrada
	@IDTransaccion as numeric(18,0),
	@IDTransaccionClonado numeric (18,0)
	
	
As

Begin
	--Asignar Fecha de Pedido del cual se clono
	Update Transaccion
	set Fecha = (Select Fecha from Transaccion where ID = @IDTransaccionClonado)
	where ID = @IDTransaccion	

	--Asignar Fecha de Pedido del cual se clono
	Update Pedido
	set Fecha = (Select Fecha from Pedido where IDTransaccion = @IDTransaccionClonado)
	where IDTransaccion = @IDTransaccion	

	--Agregar observacion de PEDIDO CLONADO
	Update Pedido
	Set Observacion = concat(Observacion,'  PEDIDO REPROCESADO')
	where IDTransaccion = @IDTransaccion


	--Incertar Registro de Reproceso
	Insert into PedidoReprocesado values(@IDTransaccionClonado, @IDTransaccion, Getdate())


End


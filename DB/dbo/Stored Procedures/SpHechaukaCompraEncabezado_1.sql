﻿CREATE Procedure [dbo].[SpHechaukaCompraEncabezado]

	@Año smallint,
	@Mes tinyint,
	@TipoReporte tinyint,
	@Exportador varchar(2)
	
As

Begin
	
	-- Variables Cantidad de Registros
	Declare @vCompras int
	Declare @vGastos int
	Declare @vNotaCreditos int
	Declare @vNotaDebitos int
	Declare @vComprobantesLibro int
	Declare @vTotalRegistros int
	
	--Variables Monto Total
	Declare @vTotalCompras money
	Declare @vTotalGastos money
	Declare @vTotalNotaCreditos money
	Declare @vTotalNotaDebitos money
	Declare @vTotalComprobantesLibro money
	Declare @vMontoTotal money
	
	--Cantidad Registros
	Set @vCompras = (IsNull((Select Count(*) From VCompra C Join VLibroIVA L On C.IDTransaccion=L.IDTransaccion JOIN TipoComprobante TC ON C.IDTipoComprobante=TC.ID Where YEAR(C.fecha) = @Año And MONTH(C.Fecha) = @Mes AND TC.LibroCompra='True'),0))
	Set @vGastos = (IsNull((Select Count(*) From VGasto G JOIN TipoComprobante TC ON G.IDTipoComprobante=TC.ID Join VLibroIVA L On G.IDTransaccion=L.IDTransaccion  Where YEAR(G.fecha) = @Año And MONTH(G.Fecha) = @Mes AND TC.LibroCompra='True'),0))
	Set @vNotaCreditos = (IsNull((Select Count(*) From VNotaCredito NC Join VLibroIVA L On NC.IDTransaccion=L.IDTransaccion JOIN TipoComprobante TC ON NC.IDTipoComprobante=TC.ID Where YEAR(NC.fecha) = @Año And MONTH(NC.Fecha) = @Mes AND NC.Anulado='False' AND TC.LibroCompra='True'),0))
	Set @vNotaDebitos = (IsNull((Select Count(*) From VNotaDebitoProveedor ND Join VLibroIVA L On ND.IDTransaccion=L.IDTransaccion JOIN TipoComprobante TC ON ND.IDTipoComprobante=TC.ID Where YEAR(ND.fecha) = @Año And MONTH(ND.Fecha) = @Mes),0))
	Set @vComprobantesLibro = (IsNull((Select Count(*) From VComprobanteLibroIVA ND Join VLibroIVA L On ND.IDTransaccion=L.IDTransaccion JOIN TipoComprobante TC ON ND.IDTipoComprobante=TC.ID Where YEAR(ND.fecha) = @Año And MONTH(ND.Fecha) = @Mes And TC.LibroCompra='True'),0))
	
	print 'Compras: ' + convert(varchar(50), @vCompras)
	print 'Gastos: ' + convert(varchar(50), @vGastos)
	print 'NC: ' + convert(varchar(50), @vNotaCreditos)
	print 'ND: ' + convert(varchar(50), @vNotaDebitos)
	print 'Comprobantes Libro: ' + convert(varchar(50), @vComprobantesLibro)
	
	Set @vTotalRegistros = @vCompras + @vGastos + @vNotaCreditos + @vNotaDebitos  + @vComprobantesLibro
	
	--Monto Total
	Set @vTotalCompras =  (IsNull ((Select Sum(L.[GRAVADO10%] + L.[GRAVADO5%] + L.EXENTO + L.[IVA10%] + L.[IVA5%])  
									From VCompra C Join VLibroIVA L On C.IDTransaccion=L.IDTransaccion 
									JOIN TipoComprobante TC ON C.IDTipoComprobante=TC.ID
									Where YEAR(C.fecha) = @Año And MONTH(C.Fecha) = @Mes And TC.LibroCompra='True'),0))
									
	Set @vTotalGastos =  (IsNull ((Select  Sum(L.[GRAVADO10%] + L.[GRAVADO5%] + L.EXENTO + L.[IVA10%] + L.[IVA5%])  
									From VGasto G 
									Join VLibroIVA L On G.IDTransaccion=L.IDTransaccion 
									JOIN VDetalleImpuestoDesglosadoTotales2 DI ON G.IDTransaccion=DI.IDTransaccion
									JOIN TipoComprobante TC ON G.IDTipoComprobante=TC.ID 
									Where YEAR(G.fecha) = @Año And MONTH(G.Fecha) = @Mes 
									AND TC.LibroCompra='True' ),0))
									
	Set @vTotalNotaCreditos = -1 * (IsNull ((Select Sum(L.[GRAVADO10%] + L.[GRAVADO5%] + L.EXENTO + L.[IVA10%] + L.[IVA5%]) 
										From VNotaCredito NC
										Join VLibroIVA L On NC.IDTransaccion=L.IDTransaccion 
										JOIN TipoComprobante TC ON NC.IDTipoComprobante=TC.ID
										Where YEAR(NC.Fecha)= @Año And MONTH (NC.Fecha)= @Mes AND NC.Anulado='False'),0))
										
	Set @vTotalNotaDebitos  = (IsNull ((Select Sum(L.[GRAVADO10%] + L.[GRAVADO5%] + L.EXENTO + L.[IVA10%] + L.[IVA5%]) 
										From VNotaDebitoProveedor ND
										Join VLibroIVA L On ND.IDTransaccion=L.IDTransaccion  
										JOIN TipoComprobante TC ON ND.IDTipoComprobante=TC.ID
										Where YEAR(ND.Fecha)= @Año And MONTH (ND.Fecha)= @Mes And TC.LibroCompra='True'),0))
	
	Set @vTotalComprobantesLibro = (IsNull ((Select Sum(L.[GRAVADO10%] + L.[GRAVADO5%] + L.EXENTO + L.[IVA10%] + L.[IVA5%]) 
										From VComprobanteLibroIVA C
										Join VLibroIVA L On C.IDTransaccion=L.IDTransaccion  
										JOIN TipoComprobante TC ON C.IDTipoComprobante=TC.ID
										Where YEAR(C.Fecha)= @Año And MONTH (C.Fecha)= @Mes And TC.LibroCompra='True'),0))
	
	Set @vMontoTotal= (@vTotalCompras + @vTotalGastos + @vTotalNotaCreditos + @vTotalNotaDebitos + @vTotalComprobantesLibro) 
	
	Select
	'TipoRegistro'=1,
	'Periodo'= convert( varchar(20), @Año) + CONVERT(varchar(20), dbo.FFormatoDosDigitos(@Mes)),
	'TipoReporte'= @TipoReporte ,
	'CodigoObligacion'= 911,
	'CodigoFormulario'= 211,
	'RUCAgente'=(Select RUC From DatoEmpresa) ,
	'DVAgente'= (Select DVRUC From DatoEmpresa) ,
	'NombreAgente'= (Select RazonSocial From DatoEmpresa),
	'RUCRepresentante'= (Select RucPropietario From DatoEmpresa),
	'DVRepresentante'=(Select DVRUCPropietario From DatoEmpresa) ,
	'NombreRepresentante'=(Select Propietario From DatoEmpresa),
	'CantidadRegistros'=@vTotalRegistros,
	'MontoTotal'= @vMontoTotal, 
	'Exportador'=@Exportador,
	'Version'=2
	
End




﻿CREATE Procedure [dbo].[SpDepartamento]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@IDPais tinyint,
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Departamento Where Descripcion=@Descripcion And IDPais=@IDPais) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDDepartamento tinyint
		set @vIDDepartamento = (Select IsNull((Max(ID)+1), 1) From Departamento)

		--Insertamos
		Insert Into Departamento(ID, Descripcion, IDPais, Orden, Estado)
		Values(@vIDDepartamento, @Descripcion, @IDPais, 0, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='DEPARTAMENTO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From Departamento Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Departamento Where Descripcion=@Descripcion And ID!=@ID And IDPais=@IDPais) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Departamento Set Descripcion=@Descripcion, 
						IDPais=@IDPais,
						Estado = @Estado
		Where ID=@ID
				
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='DEPARTAMENTO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Departamento Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con CIUDAD
		if exists(Select * From Ciudad Where IDDepartamento=@ID) begin
			set @Mensaje = 'El registro tiene ciudades asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con CLIENTES
		if exists(Select * From Cliente Where IDDepartamento=@ID) begin
			set @Mensaje = 'El registro tiene clientes asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Departamento 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='DEPARTAMENTO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
				
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


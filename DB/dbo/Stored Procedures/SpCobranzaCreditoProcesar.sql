﻿CREATE Procedure [dbo].[SpCobranzaCreditoProcesar]

	--Entrada
	@IDTransaccion numeric(18,0),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin Tran Transaccion Begin
Begin Try

	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'
		
	--Validar Feha Operacion
	Declare @vIDSucursal as integer
	Declare @vFecha as Date
	Declare @vIDUsuario as integer
	Declare @vIDOperacion as integer

	Select	@vIDSucursal=IDSucursal,
			@vFecha=Fecha,
			@vIDUsuario=IDUsuario,
			@vIDOperacion=IDOperacion
	 From Transaccion Where ID = @IDTransaccion

--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@vIDSucursal, @vFecha, @vIDUsuario, @vIDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'

			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @vFecha = (Select FechaEmision from CobranzaCredito where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@vIDSucursal, @vFecha, @vIDUsuario, @vIDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'

			Return @@rowcount
		End
	End 

	--Aplicar Cobranza
	If @Operacion = 'INS' Begin
		Print 'Recalcular los saldos de las ventas'
		--Procesamos las Ventas y Saldo del Cliente
		Set @Mensaje = 'Recalcular los saldos de las ventas'
		--print @Mensaje
		EXEC SpVentaCobranza @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Si no se proceso, anular la operacion
		If @Procesado = 0 Begin
			print 'No se pudo saldar las ventas!'
			RollBack Tran Transaccion
			Set @Mensaje = 'No se pudo saldar las ventas!'
			Set @Procesado = 'False'
			--print @Mensaje
			GoTo EliminarError
		End
		print 'Saldos de ventas recalculados.'
		Set @Mensaje = 'Saldos de ventas recalculados.'
		--print @Mensaje
		
		--Procesamos los Cheques si es que hay
		Set @Mensaje = 'Procesando los cheques.'
		--print @Mensaje
		EXEC SpChequeClienteSaldar @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		print @Mensaje
		--Si no se proceso, anular la operacion
		If @Procesado = 0 Begin
			print 'Errores en el proceso de cheques...'
			RollBack Tran Transaccion
			Set @Mensaje = 'Errores en el proceso de cheques...'
			Set @Procesado = 'False'
			--print @Mensaje
			GoTo EliminarError
		End
		
		Set @Mensaje = 'Cheques saldados..'
		--print @Mensaje
		Print @Mensaje
		--Damos el OK a la Cobranza
		If @Procesado = 1 Begin
			
			Set @Mensaje = 'Procesamos la cobranza.'
			Print @Mensaje
			--print @Mensaje
			Update CobranzaCredito Set Procesado='True' 
			Where IDTransaccion=@IDTransaccion
			
			--Generar Asiento
			Exec SpAsientoCobranzaCredito @IDTransaccion=@IDTransaccion
			
			Set @Mensaje = 'Asiento generado...'
			--print @Mensaje
			Print @Mensaje

			Set @Mensaje = 'Registro procesado!'
			Set @Procesado = 'True'
			GoTo Salir
						
		End
				
	End
	
	If @Operacion = 'DEL' Begin
		
		--Procesar
		--Eliminar la Forma de Pago (Eliminar el efectivo y actualizarlo - Reestablecer el saldo del cheque)
		Delete From FormaPagoTarjeta Where IDTransaccion=@IDTransaccion
		Delete From FormaPagoDocumentoRetencion Where IDTransaccion=@IDTransaccion
		Delete From FormaPagoDocumento Where IDTransaccion=@IDTransaccion
		Delete From Efectivo Where IDTransaccion=@IDTransaccion
		Delete From FormaPago Where IDTransaccion=@IDTransaccion
				
		--Eliminar relacion de las ventas asociadas (Actualizar saldos de la venta y cliente)
		Delete From VentaCobranza Where IDTransaccionCobranza=@IDTransaccion
		
		--Eliminamos el registro
		Delete CobranzaCredito Where IDTransaccion = @IDTransaccion
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion
		
		GoTo Salir
		
	End
	
Salir:
	
	--Actualizar saldos de clientes
	Begin
	
		Declare @vIDTransaccionVenta numeric(18,0)
		Declare @vIDCliente int
		
		Declare db_cursor cursor for
		Select V.IDCliente From VentaCobranza VC Join Venta V On VC.IDTransaccionVenta=V.IDTransaccion 
		Where IDTransaccionCobranza=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDCliente
		While @@FETCH_STATUS = 0 Begin 
			
			Exec SpAcualizarSaldoCliente @IDCliente=@vIDCliente
			
			Fetch Next From db_cursor Into @vIDCliente
										
		End
		
		Close db_cursor   
		Deallocate db_cursor
		
	End
	
	--Establecemos los procesos
	Commit Tran Transaccion 
	Return @@rowcount
	
End Try
Begin Catch
	
	Set @Mensaje = ERROR_MESSAGE()
	Set @Procesado = 'False'
	Set @Mensaje = 'Saldar ventas...'
	print 'Error: ' + ERROR_MESSAGE()
	RollBack Tran Transaccion
	RAISERROR(@Mensaje, 16, 0)		 
	
	GoTo EliminarError
	
End Catch

EliminarError:
	
	--Eliminamos todo el registro si hubo un error en la inserccion
	If @Operacion = 'INS' Begin
		print 'EliminarError'
		Delete From FormaPagoTarjeta Where IDTransaccion=@IDTransaccion
		Delete From FormaPagoDocumentoRetencion Where IDTransaccion=@IDTransaccion
		Delete From FormaPagoDocumento Where IDTransaccion=@IDTransaccion
		Delete From Efectivo Where IDTransaccion=@IDTransaccion
		Delete From FormaPago Where IDTransaccion=@IDTransaccion
				
		--Eliminar relacion de las ventas asociadas (Actualizar saldos de la venta y cliente)
		Delete From VentaCobranza Where IDTransaccionCobranza=@IDTransaccion
		
		--Eliminamos el registro
		Delete CobranzaCredito Where IDTransaccion = @IDTransaccion
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion
		Set @Procesado = 'False'
		--print @Mensaje
		
		--Establecemos los procesos
		Return @@rowcount
					
	End
	
End


﻿CREATE Procedure [dbo].[SpReciboVenta]
	
	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@FechaAnulado date = NULL,
	@IDUsuarioAnulado int=NULL,
	@Operacion varchar(50),
	
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin

	--BLOQUES
	Declare @vIDCliente int 
	Declare @vMaxIDMotivo int
	Declare @CancelarAutomatico bit 
	Declare @NroRecibo int 
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Solo generar el recibo cuando el Total = Saldo
		If Exists(Select * From Venta Where IDTransaccion = @IDTransaccion and Saldo <> Total) Begin
			set @Mensaje = 'La venta ya fue cobrada parcial o totalmente.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Verificar que el documento ya este anulado
		If Exists(Select * From Venta Where IDTransaccion=@IDTransaccion And Anulado='True') Begin
			set @Mensaje = 'La Venta esta anulada'
			set @Procesado = 'False'
			return @@rowcount
		End
				
		------Insertar
		Declare @vSaldo money
		Declare @vCancelado bit
		--Inicializar con la fecha de la venta
		Declare @Fecha date = (Select FechaEmision from Venta where IDTransaccion = @IDTransaccion)
		Set @vCancelado = 'False'
		
		--Insertar el Registro de Venta		
		Declare @IDRecibo int = (Select top(1) ID from Recibo where Estado = 'False')
		--obtener el siguiente numero a utilizar
		set @NroRecibo = IsNull((Select UltimoNumero from Recibo where ID = @IDRecibo),0) + 1

		--verificar si la venta ya tiene recibo asociado, si tiene entonces asignar fecha del dia al recibo
		if exists(Select * from ReciboVenta where IDTransaccionVenta = @IDTransaccion) Begin
			if (Select convert(date,FechaEmision) from Venta where IDTransaccion = @IDTransaccion) <= convert(date,GETDATE()) begin
				set @Fecha = getdate()
			end
		End 

		--insertar datos
		Insert Into ReciboVenta(IDTransaccionVenta, IDRecibo,NroRecibo, Fecha,Anulado, IDUsuarioAnulacion, FechaAnulacion)
		Values(@IDTransaccion, @IDRecibo,@NroRecibo,@Fecha, 'False', null, null)
		
			
		--Actualizar el ultimo numero utilizado del recibo
		Update Recibo 
		Set  UltimoNumero =@NroRecibo
		Where ID=@IDRecibo
		
		
								
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'

			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Venta Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End	

		Declare @vComprobante as varchar(20)
		Set @vComprobante = (select Recibo from VReciboVenta where IDTransaccionVenta = @IDTransaccion)

		If Not Exists(Select * From CobranzaCredito Where Comprobante = @vComprobante and anulado = 0 and procesado = 1) Begin
			set @Mensaje = 'El recibo esta asociado a una cobranza.'
			set @Procesado = 'False'
			return @@rowcount
		End	
	
		--Anulamos el registro
		set @IDRecibo = (Select IDRecibo from ReciboVenta where IDTransaccionVenta = @IDTransaccion and Anulado = 'False')

		Update ReciboVenta Set Anulado = 'True',
						FechaAnulacion=getdate(),
						IDUsuarioAnulacion = @IDUsuario
		Where IDTransaccionVenta = @IDTransaccion
		and IDRecibo = @IDRecibo
			
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End

	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	return @@rowcount
		
End
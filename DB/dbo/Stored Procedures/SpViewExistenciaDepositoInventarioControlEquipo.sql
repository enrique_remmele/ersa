﻿CREATE Procedure [dbo].[SpViewExistenciaDepositoInventarioControlEquipo]

	--Entrada
	@IDTransaccion numeric(18,0)
		
As

Begin


	Select 
	ED.IDProducto, 
	ED.Referencia, 
	ED.Producto, 
	ED.CodigoBarra
		
	From VExistenciaDepositoInventario ED 
	Where ED.IDTransaccion=@IDTransaccion
	Order By Producto 

End



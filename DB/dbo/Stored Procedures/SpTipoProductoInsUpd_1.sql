﻿CREATE Procedure [dbo].[SpTipoProductoInsUpd]

	--Entrada
	@Descripcion varchar(100)
	
As

Begin

	Declare @vID int
	
	If @Descripcion = '' Begin
		Set @Descripcion = '---'
	End
	
	--Si existe
	If Exists(Select * From TipoProducto Where Descripcion = @Descripcion) Begin
		Set @vID = (Select Top(1) ID From TipoProducto Where Descripcion = @Descripcion)
		
	End Else Begin
		Set @vID = (Select IsNull(Max(ID)+1,1) From TipoProducto)
		Insert Into TipoProducto(ID, Descripcion, Estado)
		Values(@vID, @Descripcion, 'True')
	End
	
	return @vID
	
End


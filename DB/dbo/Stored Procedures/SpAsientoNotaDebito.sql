﻿CREATE Procedure [dbo].[SpAsientoNotaDebito]

	@IDTransaccion numeric(18,0)
		
As

Begin
	
	--********IGUAL QUE VENTA******************
	
	--Variables
	Declare @vIDSucursal tinyint
	
	--Nota de Credito
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vContado bit
	Declare @vCredito bit
	Declare @vCondicion varchar(50)
	Declare @vFechaEmision date
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money
	Declare @vConComprobantes bit
	
	--Asiento
	Declare @vImporte money
	Declare @vCodigo varchar(50)
	
	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money
	
	--Obtener valores
	Begin
	
		Set @vContado='False'
		
		Select	@vIDSucursal=IDSucursal,
				@vTipoComprobante=TipoComprobante,
				@vIDTipoComprobante=IDTipoComprobante,
				@vComprobante=Comprobante,
				@vCredito=Credito,
				@vCondicion=@vCondicion,
				@vFechaEmision=Fecha,
				@vIDMoneda=IDMoneda,
				@vCotizacion=Cotizacion,
				@vCondicion=Condicion
		From VNotaDebito Where IDTransaccion=@IDTransaccion	
		
		If @vCredito=0 Begin
			Set @vContado='True'
		End
				
		Set @vConComprobantes = (Case When Exists(Select * From NotaCreditoVenta Where IDTransaccionNotaCredito=@IDTransaccion) Then 'True' Else 'False' End)
		
	End
					
	--Verificar que el asiento se pueda modificar
	Begin
	
		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'Registro conciliado'
			GoTo salir
		End 	
		
		--Si esta procesado
		If (Select Procesado From NotaDebito Where IDTransaccion=@IDTransaccion) = 'False' Begin
			print 'Registro no esta procesado'
			GoTo salir
		End 	
		
		--Si esta anulado
		If (Select Anulado From NotaDebito Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'Registro anulado'
			GoTo salir
		End 	
		
		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'Registro no esta bloqueado'
			GoTo salir
		End	
		
	End
			
	--Eliminar primero el asiento
	Begin
		
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
		
	End
	
	--Cargamos la Cabecera
	Begin
		
		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)
		
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = @vTipoComprobante + ' ' + @vComprobante + ' ' + @vCondicion
		print 'Tipo de Comprobante:' + convert(varchar(50), @vTipoComprobante)
		print 'Comprobante:' + convert(varchar(50), @vComprobante)
		print 'Condicion:' + convert(varchar(50), @vCondicion)
		
		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFechaEmision, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')
		
		
	End
	
	--Cuentas Fijas de Clientes
	Begin
	
		--Variables
		Declare @vVentaCredito bit
		Declare @vVentaContado bit
		Declare cCFVentaCliente cursor for
		Select Codigo, Debe, Haber, Credito, Contado From VCFVentaCliente Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFVentaCliente   
		fetch next from cCFVentaCliente into @vCodigo, @vDebe, @vHaber, @vVentaCredito, @vVentaContado
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			Set @vImporte = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
							
			--Credito
			If @vConComprobantes = 'True' Begin
				If @vVentaCredito = 'True' Begin			
					Set @vImporte = (Select 
									SUM(Importe)
									From NotaDebitoVenta NCV
									Join VVenta V On NCV.IDTransaccionVentaGenerada=V.IDTransaccion
									Where NCV.IDTransaccionNotaDebito=@IDTransaccion And V.Credito='True')
				End Else Begin
				
					--Contado
				
					Set @vImporte = (Select 
									SUM(Importe)
									From NotaDebitoVenta NCV
									Join VVenta V On NCV.IDTransaccionVentaGenerada=V.IDTransaccion
									Where NCV.IDTransaccionNotaDebito=@IDTransaccion And V.Credito='False')
				End
			End
			
			If @vConComprobantes = 'False' Begin
				Set @vImporte = (Select Total From NotaDebito Where IDTransaccion=@IDTransaccion)				
			End
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				

			If @vCredito = 'True' Begin
				If @vVentaCredito = 'False' Begin
					Set @vImporte = 0
				End
			End Else Begin
				If @vVentaContado = 'False' Begin
					Set @vImporte = 0
				End
			End
			
			If @vImporte > 0 Begin				
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFVentaCliente into @vCodigo, @vDebe, @vHaber, @vVentaCredito, @vVentaContado
			
		End
		
		close cCFVentaCliente 
		deallocate cCFVentaCliente
	End
		
	--Cuentas Fijas Descuentos
	Begin
		
		--Variables
		Declare @vIDTipoDescuento tinyint
		
		Declare cCFVentaDescuento cursor for
		Select Codigo, IDTipoDescuento, Debe, Haber From VCFVentaDescuento Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFVentaDescuento
		
		fetch next from cCFVentaDescuento into @vCodigo, @vIDTipoDescuento, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			If @vIDTipoDescuento = 4 Begin
				Set @vIDTipoDescuento = 0
			End
			
			Set @vImporte = (Select SUM(TotalDescuentoDiscriminado) From VDescuento Where IDTransaccion=@IDTransaccion And IDTipoDescuento=@vIDTipoDescuento)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporte > 0 Begin
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFVentaDescuento into @vCodigo, @vIDTipoDescuento, @vDebe, @vHaber
			
		End
		
		close cCFVentaDescuento
		deallocate cCFVentaDescuento
	End
					
	--Cuentas Fijas de Venta 
	Begin
	
		--Cuenta Fija Venta
		Declare @vCuentaContableVenta varchar(50)
		Declare @vTotalDiscriminado money
		Declare @vTotalDescuentoDiscriminado money
	
		Declare cDetalleVenta cursor for
		Select CuentaContableVenta, 'TotalDiscriminado'=Sum(TotalDiscriminado), 'TotalDescuentoDiscriminado'=Sum(TotalDescuentoDiscriminado) From VDetalleNotaDebito Where IDTransaccion=@IDTransaccion Group By IDTransaccion, CuentaContableVenta
		Open cDetalleVenta 
		fetch next from cDetalleVenta into @vCuentaContableVenta, @vTotalDiscriminado, @vTotalDescuentoDiscriminado
		
		While @@FETCH_STATUS = 0 Begin  
			
			set @vImporte = 0
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vCodigo = @vCuentaContableVenta
			
			--Si no existe, poner el predeterminado
			If @vCuentaContableVenta Is Null or @vCuentaContableVenta = '' Begin
				Set @vCodigo = (Select Top(1) Codigo From VCFVentaVenta Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda)
			End
			
			--Hayar el importe	
			Set @vImporte = @vTotalDiscriminado + @vTotalDescuentoDiscriminado
									
			Set @vDebe = (Select Top(1) Debe From VCFVentaVenta Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda)
			Set @vHaber = (Select Top(1) Haber From VCFVentaVenta Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')			
										
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			--Cargamod
			If @vImporte > 0 And @vIDCuentaContable Is Not Null Begin
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vImporteHaber, @vImporteDebe, 0, '')
			End
				
			fetch next from cDetalleVenta into @vCuentaContableVenta, @vTotalDiscriminado, @vTotalDescuentoDiscriminado
			
		End
		
		close cDetalleVenta 
		deallocate cDetalleVenta
		
	End
			
	--Cuentas Fijas Impuesto
	Begin
		
		--Variables
		Declare @vIDImpuesto tinyint
		
		Declare cCFVentaImpuesto cursor for
		Select Codigo, IDImpuesto, Debe, Haber From VCFVentaImpuesto Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFVentaImpuesto   
		fetch next from cCFVentaImpuesto into @vCodigo, @vIDImpuesto, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			Set @vImporte = (Select SUM(TotalImpuesto) From DetalleImpuesto Where IDTransaccion=@IDTransaccion And IDImpuesto=@vIDImpuesto)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporte > 0 Begin
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFVentaImpuesto into @vCodigo, @vIDImpuesto, @vDebe, @vHaber
			
		End
		
		close cCFVentaImpuesto 
		deallocate cCFVentaImpuesto
	End
	
	--Cuentas Fijas de Mercaderia
	Begin
	
		--Variables
		
		Declare cCFVentaMercaderia cursor for
		Select Codigo, Debe, Haber From VCFVentaMercaderia Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda
		Open cCFVentaMercaderia   
		fetch next from cCFVentaMercaderia into @vCodigo, @vDebe, @vHaber
		
		While @@FETCH_STATUS = 0 Begin  
			
			Set @vImporteDebe = 0
			Set @vImporteHaber = 0
			
			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

			--Set @vImporte = (Select SUM(DNC.TotalCosto) From VDetalleNotaDebito DNC Join NotaDebitoVenta NCV On DNC.IDTransaccion=NCV.IDTransaccionNotaDebito Join VDetalleVenta DV On NCV.IDTransaccionVentaGenerada=DV.IDTransaccion And DNC.IDProducto=DV.IDProducto Where DNC.IDTransaccion=@IDTransaccion)
			Set @vImporte = (Select SUM(DNC.TotalCosto) From VDetalleNotaDebito DNC  Where DNC.IDTransaccion=@IDTransaccion)
			
			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
			End
			
			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
			End				
			
			If @vImporte > 0 Begin				
				Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, Credito, Debito, Importe, Observacion)	
				Values(@IDTransaccion, @vID, @vIDCuentaContable, @vImporteHaber, @vImporteDebe, 0, '')
			End
			
			fetch next from cCFVentaMercaderia into @vCodigo, @vDebe, @vHaber
			
		End
		
		close cCFVentaMercaderia 
		deallocate cCFVentaMercaderia
	End
	
	----Actualizamos la cabecera, el total
	Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	Set @vImporteDebe = IsNull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	
	Update Asiento Set Total = @vImporteHaber,
						Credito = @vImporteHaber,
						Debito = @vImporteDebe,
						Saldo = @vImporteHaber - @vImporteDebe
	Where IDTransaccion=@IDTransaccion
	
	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:
	
End


﻿
CREATE Procedure [dbo].[SpViewFormaPagoFactura]

	--Entrada
	@IDUsuario integer,
	@FacturacionSinPedido Bit = Null,
	@Condicion Varchar(20) = ''
	
As

Begin

	if @Condicion = 'Contado' begin
		Set @Condicion = 'CONT'
	end

	if @Condicion = 'Credito' begin
		set @Condicion = 'CRED'
	end


	--variables
	Declare @vID tinyint
	Declare @Referencia varchar(50)
	Declare @vFacturacionSinPedido bit
	Declare @vCondicion varchar(20)
		
	--Crear la tabla
	Create table #Table(ID tinyint,
						Referencia varchar(50)
						)
								

	Declare db_cursor cursor for
	Select distinct
	FPF.ID, 
	FPF.Referencia,
	FPF.FacturacionSinPedido,
	FPF.Condicion
	From FormaPagoFactura FPF
	inner join FormaPagoFacturaUsuario FPFU on FPF.ID = FPFU.IDFormaPagoFactura
	Where FPFU.IDUsuario = @IDUsuario
	Open db_cursor   
	Fetch Next From db_cursor Into	@vId, @Referencia, @vFacturacionSinPedido, @vCondicion
	While @@FETCH_STATUS = 0 Begin 
	
	If @FacturacionSinPedido Is Not null Begin If @FacturacionSinPedido!=@vFacturacionSinPedido GoTo Siguiente End
	If @vCondicion <> '' Begin If @Condicion!=@vCondicion Begin If @Condicion <> '' GoTo Siguiente End End
	
			Insert Into #Table
			Values(@vID, @Referencia)
		
Siguiente:
		
		Fetch Next From db_cursor Into	@vId, @Referencia, @vFacturacionSinPedido, @vCondicion
		
	End

	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor		   		
	
	
	Select * From #Table 
End





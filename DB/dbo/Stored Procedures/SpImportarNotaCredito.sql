﻿CREATE Procedure [dbo].[SpImportarNotaCredito]

--Entrada
	--Punto de Expedicion
	@TipoComprobante varchar(10),
	@ReferenciaSucursal varchar(5),
	@ReferenciaPuntoExpedicion varchar(5),
	@NroComprobante varchar(50),

--Cliente
	@Cliente varchar(50),
	
	
	--
	@FechaEmision varchar(50),
	@Sucursal varchar(50),
	@Deposito varchar(50),
	
	
	
	@Cotizacion tinyint,
	@Observacion varchar(200),
	
	--Totales
	@Total money,
	@TotalImpuesto money,
	@TotalDescuento money,
	@Saldo money,
	
	--Impuesto
	@Exento as money,
	@DescuentoExento as money,
	@Gravado10 as money,
	@IVA10 as money,
	@Descuento10 as money,
	@Gravado5 as money,
	@IVA5 as money,
	@Descuento5 as money,
	
	--Datos Extras
	@NroOperacion int,
	@Operador varchar(10),
	
	--Configuraciones
	@Anulado bit,
	@FechaAnulado varchar (50),
	
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	
	@Actualizar bit

As

Begin

--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		
		Declare @vOperacion varchar(50)
		Declare @vIDTransaccion int
		
		Declare @vComprobante varchar(50)
		Declare @vTelefono varchar(100)
		Declare @vTotalDiscriminado money
		Declare @vTotalDescuento money
		
		
		--Codigos
		Declare @vIDPuntoExpedicion int 
		Declare @vIDTipoComprobante int
		Declare @vIDCliente int
		Declare @vIDSucursalCliente int
		Declare @vIDSucursal int
		Declare @vIDDeposito int
		
		
	End
	
	
	--Establecer valores predefinidos
	Begin
		
		Set @vOperacion = 'INS'
		Set @vTelefono = ''
		Set @vTotalDiscriminado = @Total - @TotalImpuesto
		
	End
	
	--Verificar si ya existe
	If Exists(Select * From NotaCreditoImportada Where NroOperacion=@NroOperacion) Begin
		set @vIDTransaccion = (Select IDTransaccion From NotaCreditoImportada Where NroOperacion=@NroOperacion)
		Set @vOperacion = 'UPD'
	End
	
	If @vOperacion = 'UPD' Begin
	
		If @Actualizar = 'True' Begin
		
			--NOta Credito
			Update NotaCredito  Set Total =@Total,
									Saldo=@Total,									
									Anulado=@Anulado,
									Aplicar = 'True',
									Descuento='True',
									Devolucion='False'
			Where IDTransaccion = @vIDTransaccion
			
			--Venta Importacion
			--Insertar en NotaCreditoImportacion
			Update NotaCreditoImportada Set IDTransaccion =@vIDTransaccion, 
											Total=@Total 										
			Where IDTransaccion=@vIDTransaccion
			
			Set @vMensaje = 'Actualizado!'
			Set @vProcesado = 'True'
			GoTo Salir
			
		End
		
		Set @vMensaje = 'Sin Act.!'
		Set @vProcesado = 'True'
		GoTo Salir
		
	End
	
	--Hayar Valores
	Begin
		
		Set @vMensaje = 'No se proceso!'
		Set @vProcesado = 'False'
		
		--Punto de Expedicion
		Set @ReferenciaSucursal = '0' + dbo.FFormatoDosDigitos(CONVERT(tinyint, @ReferenciaSucursal))
		Set @ReferenciaPuntoExpedicion = '0' + dbo.FFormatoDosDigitos(CONVERT(tinyint, @ReferenciaPuntoExpedicion))
		Set @vComprobante = @ReferenciaSucursal + '-' + @ReferenciaPuntoExpedicion + '-' + @NroComprobante
		Set @vIDPuntoExpedicion = IsNull((Select Top(1) ID From VPuntoExpedicion Where ReferenciaSucursal=@ReferenciaSucursal And ReferenciaPunto=@ReferenciaPuntoExpedicion And TipoComprobante=@TipoComprobante And NumeracionDesde <= @NroComprobante And NumeracionHasta >= @NroComprobante),0)
		
		print @ReferenciaSucursal
		print @ReferenciaPuntoExpedicion
		print @vComprobante
		print @vIDPuntoExpedicion
		
		--Punto de Expedicion
		If @vIDPuntoExpedicion = 0 Begin
			Set @vMensaje = 'No se encontro el Punto de Expedicion!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
				
		--Tipo de Comprobante
		Set @vIDTipoComprobante = (IsNull((Select Top(1) ID From TipoComprobante Where Codigo=@TipoComprobante And IDOperacion=@IDOperacion), 0))
		If @vIDTipoComprobante = 0 Begin
			Set @vMensaje = 'Tipo de comprobante incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Cliente
		Set @vIDCliente=(IsNull((Select Top(1) ID From Cliente Where Referencia=@Cliente),0))
		If @vIDCliente = 0 Begin
			Set @vMensaje = 'Cliente incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
	
		--Sucursal
		Set @vIDSucursal = (IsNull((Select Top(1) ID From Sucursal Where Descripcion=@Sucursal),0))
		If @vIDSucursal = 0 Begin
			Set @vMensaje = 'Sucursal incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Deposito
		Set @vIDDeposito = (IsNull((Select Top(1) ID From Deposito Where IDSucursal=@vIDSucursal And Descripcion=@Deposito),0))
		If @vIDDeposito = 0 Begin
			Set @vMensaje = 'Deposito incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Actualizamos el Cliente
		If (Select IDSucursal From Cliente Where ID=@vIDCliente) Is Null Begin
			Update Cliente Set IDSucursal=@vIDSucursal
			Where ID=@vIDCliente
		End
		
	End
	
	--INSERTAR
	If @vOperacion = 'INS' Begin
		
		--Validar Informacion
		Begin
		
			--Numero
			If Exists(Select * From NotaCredito Where NroComprobante = 1127 ) Begin
				set @vMensaje = 'El sistema detecto que el numero de comprobante ya esta registrado! Obtenga un numero siguiente.'
				set @vProcesado = 'False'
				GoTo Salir
			End
			
			--Comprobante
			if Exists(Select * From NotaCredito  Where IDPuntoExpedicion=@vIDPuntoExpedicion And NroComprobante=@NroComprobante) Begin
				set @vMensaje = 'El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
				set @vProcesado = 'False'
				GoTo Salir
			End
			
			--Comprobante fuera del Rango
			if @NroComprobante < (Select NumeracionDesde From PuntoExpedicion Where ID=@vIDPuntoExpedicion) Or @NroComprobante > (Select NumeracionHasta From PuntoExpedicion Where ID=@vIDPuntoExpedicion) Begin
				set @vMensaje = 'El numero de comprobante no esta dentro del rango correcto! Cambie el punto de expedicion o actualicelo.'
				set @vProcesado = 'False'
				GoTo Salir
			End
			
		End
		
		----Insertar la transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@MostrarTransaccion = 'False',
			@Mensaje = @vMensaje OUTPUT,
			@Procesado = @vProcesado OUTPUT,
			@IDTransaccion = @vIDTransaccion OUTPUT
		
		If @vProcesado = 'False' Begin
			GoTo Salir
		End
			
		--Insertar en Nota Credito
		Insert Into NotaCredito (IDTransaccion, IDPuntoExpedicion,  IDTipoComprobante,  NroComprobante, IDCliente,  Fecha ,IDSucursal,   IDDeposito,	IDMoneda, Cotizacion,  Observacion,   Total,  TotalImpuesto,  TotalDiscriminado,   TotalDescuento, Saldo, Anulado,  FechaAnulado, IDUsuarioAnulado, Procesado, Aplicar, Descuento, Devolucion)
						Values(@vIDTransaccion, @vIDPuntoExpedicion, @vIDTipoComprobante, @NroComprobante, @vIDCliente,  @FechaEmision, @vIDSucursal, @vIDDeposito ,          1,        @Cotizacion, @Observacion,    @Total, @TotalImpuesto, @vTotalDiscriminado, @TotalDescuento,  @Total,  @Anulado, @FechaAnulado ,  1, 'True', 'True', 'True', 'False')
		
		--Insertar en VentaImportacion
		Insert Into NotaCreditoImportada (IDTransaccion, NroOperacion, Total,  Operador)
		Values(@vIDTransaccion, @NroOperacion, @Total, @Operador)
		
		--Insertar Impuesto
		--10%
		Insert into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado)
		Values(@vIDTransaccion, 1, @Gravado10, @IVA10, @Descuento10, @Gravado10 - (@IVA10 + @Descuento10))
		
		--5%
		Insert into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado)
		Values(@vIDTransaccion, 2, @Gravado5, @IVA5, @Descuento5, @Gravado5 - (@IVA5 + @Descuento5))
		
		--Exento
		Insert into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado)
		Values(@vIDTransaccion, 3, @Exento, 0, @DescuentoExento, @Exento - @DescuentoExento)
		
		Set @vMensaje = 'Registro guardado!'
		Set @vProcesado = 'True'
		GoTo Salir
				
	End
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


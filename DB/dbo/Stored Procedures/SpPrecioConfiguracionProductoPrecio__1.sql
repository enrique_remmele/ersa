﻿CREATE Procedure [dbo].[SpPrecioConfiguracionProductoPrecio_]

	--Entrada
	@Fecha Date,
	@IDProducto int,
	@IDCliente int,
	@IDClienteSucursal int = 0,
	@IDDeposito tinyint = 0,
	@ControlarReserva bit = 'False',
	@IDMoneda Tinyint,
	@Cotizacion decimal(18,2) = 1
	
As

Begin

	--Variables
	Declare @vExistencia decimal(10,2)
	Declare @vFecha date
	Declare @vIDTipoProducto int 
	Declare @vIDListaPrecioClienteSucural int
	Declare @vIDSucursalCliente int
	Declare @vListaPrecio varchar(50)


	Set @vIDTipoProducto = (Select IDTipoProducto from Producto where ID = @IDProducto)
	
	if @IDClienteSucursal = 0 begin
		Set @vIDListaPrecioClienteSucural = (Select IDListaPrecio from vCliente where ID = @IDCliente)
		set @vIDSucursalCliente = (select idsucursal from Cliente where ID = @IDCliente)
	end
	else begin
		Set @vIDListaPrecioClienteSucural = (Select IDListaPrecio from vClienteSucursal where IDCliente = @IDCliente and ID = @IDClienteSucursal)
		set @vIDSucursalCliente = (select idsucursal from ClienteSucursal where IDCliente = @IDCliente and ID = @IDClienteSucursal)
	end
	
	Set @vListaPrecio = (Select Descripcion from ListaPrecio where ID = @vIDListaPrecioClienteSucural)


	if @Fecha is null begin
		Set @Fecha = convert(Date,getdate())
	end
	
	Set @vFecha = convert(Date,@Fecha)
	Set @vExistencia = dbo.FExistenciaProducto(@IDProducto, @IDDeposito)
	
	If @ControlarReserva = 'True' Begin
		Set @vExistencia = @vExistencia - ISNULL((dbo.FExistenciaProductoReservado(@IDProducto, @IDDeposito)), 0)
	End

	--Precio de Lista controlar existencia
	Select 'IDTipo'=-1,'Tipo'='Precio Lista', 'Importe'=P.Precio, 'Existencia'=@vExistencia, IDMoneda,'CantidadMinima'=0
	From vProductoListaPrecio P
	Join Producto Prod on P.IDProducto = Prod.ID
	Where P.IDProducto=@IDProducto 
	and P.IDMoneda = @IDMoneda
	and P.ListaPrecio = (Case when @vIDTipoProducto = 1 then 'MAYORISTA' else @vListaPrecio end)
	and P.IDSucursal = 1--(Case when @vIDTipoProducto = 1 then 1 else @vIDSucursalCliente end)
	and Prod.ControlarExistencia = 1

	Union All
	--Precio de Lista No controlar existencia
	Select 'IDTipo'=-1,
	'Tipo'='Precio Lista', 
	'Importe'=1, 
	'Existencia'=@vExistencia, 
	@IDMoneda,
	'CantidadMinima'=0
	From Producto Prod 
	Where Prod.ID=@IDProducto 
	and Prod.ControlarExistencia = 'False'
	and (select count(*) from productomodificarprecio where idproducto = @IDProducto and estado = 1)=1
	and (select count(*)
		From vProductoListaPrecio P
		Join Producto Prod on P.IDProducto = Prod.ID
		Where P.IDProducto=@IDProducto 
		and P.IDMoneda = @IDMoneda
		and P.ListaPrecio = (Case when @vIDTipoProducto = 1 then 'MAYORISTA' else @vListaPrecio end)
		and P.IDSucursal = 1--(Case when @vIDTipoProducto = 1 then 1 else @vIDSucursalCliente end)
		and Prod.ControlarExistencia = 1) = 0

	Union All
	
		
	--Descuento Fijo
	Select 'IDTipo'=4,'Tipo'='DESCUENTO FIJO', 'Importe'=P.Precio, 'Existencia'=@vExistencia, IDMoneda,'CantidadMinima'=isnull(P.CantidadMinimaPrecioUnico,0)
	From ProductoPrecio P
	Where P.IDProducto=@IDProducto 
	and P.IDCliente = @IDCliente
	and P.IDMoneda = @IDMoneda
	and P.PrecioUnico = 0
	and Desde<= @vFecha
	and Isnull(IDTransaccionPedidoPrecioUnico,0) = 0
		
	Union All
	
	--Descuento Unico
	Select 'IDTipo'=5,'Tipo'='DESCUENTO UNICO', 'Importe'=P.Precio, 'Existencia'=@vExistencia, IDMoneda,'CantidadMinima'=isnull(P.CantidadMinimaPrecioUnico,0)
	From ProductoPrecio P
	Where P.IDProducto=@IDProducto 
	and P.IDCliente = @IDCliente
	and P.IDMoneda = @IDMoneda
	and P.PrecioUnico = 1
	and Desde<= @vFecha
	and Isnull(IDTransaccionPedidoPrecioUnico,0) = 0

	Union All

	---Descuentos Producto - Lista de Precio - Precio  SOLO PARA BALANCEADO
	Select 'IDTipo'=4,'Tipo'='DESCUENTO FIJO', 'Importe'=(Select top(1) PLP.Precio From vProductoListaPrecio PLP
												Where PLP.IDProducto=@IDProducto 
												and PLP.IDMoneda = @IDMoneda
												and PLP.ListaPrecio = (Case when @vIDTipoProducto = 1 then 'MAYORISTA' else @vListaPrecio end)
												--and PLP.IDListaPrecio = (Case when @vIDTipoProducto = 1 then 1 else @vIDListaPrecioClienteSucural end)
												--and PLP.IDSucursal = (Case when @vIDTipoProducto = 1 then 1 else @vIDSucursalCliente end)
												) - PLPE.Descuento, 
	'Existencia'=@vExistencia, PLPE.IDMoneda,'CantidadMinima'=1
	From vProductoListaPrecioExcepciones PLPE 
	Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
	Where PLPE.IDProducto=@IDProducto 
	And PLPE.IDCliente=@IDCliente  
	And @vFecha between convert(Date,PLPE.Desde) And convert(Date,PLPE.Hasta)
	And TD.Descripcion = 'TACTICO'
	And (Case When isnull(PLPE.CantidadLimite,0)=0 then 1 else PLPE.CantidadLimite end)>(Case When Isnull(PLPE.CantidadLimite,0)=0 then 0 else PLPE.CantidadLimiteSaldo end) 
	And ISnull(PLPE.IDTransaccionPedido,0) = 0	
	And (Case when ISnull(PLPE.IDTipoProducto,0) = 1 then '' else PLPE.ListaPrecio end) = (Case when ISnull(PLPE.IDTipoProducto,0) = 1 then '' else @vListaPrecio end)
	And PLPE.Desde = (Select top(1) PLPE.Desde
						From vProductoListaPrecioExcepciones PLPE 
						--Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
						Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
						Where PLPE.IDProducto=@IDProducto 
						--And PLPE.ListaPrecio=@vListaPrecio
						And PLPE.IDCliente=@IDCliente  
						And @vFecha between convert(Date,PLPE.Desde) And convert(Date,PLPE.Hasta)
						And TD.Descripcion = 'TACTICO'
						And (Case When isnull(PLPE.CantidadLimite,0)=0 then 1 else PLPE.CantidadLimite end)>(Case When Isnull(PLPE.CantidadLimite,0)=0 then 0 else PLPE.CantidadLimiteSaldo end) 
						And ISnull(PLPE.IDTransaccionPedido,0) = 0	
						And (Case when ISnull(PLPE.IDTipoProducto,0) = 1 then '' else PLPE.ListaPrecio end) = (Case when ISnull(PLPE.IDTipoProducto,0) = 1 then '' else @vListaPrecio end)
						Order by PLPE.Desde desc)

End





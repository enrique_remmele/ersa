﻿CREATE Procedure [dbo].[SpTipoProducto]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Estado bit,
	@Referencia varchar(50) = '',
	@CuentaContableCliente varchar(50) = '',
	@CuentaContableVenta varchar(50) = '',
	@CuentaContableCosto varchar(50) = '',
	@CuentaContableProveedor varchar(50) = '',
	@Produccion bit = 0,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From TipoProducto Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDTipoProducto tinyint
		set @vIDTipoProducto = (Select IsNull((Max(ID)+1), 1) From TipoProducto)

		--Insertamos
		Insert Into TipoProducto(ID, Descripcion, Estado, Referencia, CuentaContableCliente, CuentaContableCosto, CuentaContableVenta, CuentaContableProveedor, Producido)
		Values(@vIDTipoProducto, @Descripcion, @Estado, @Referencia, @CuentaContableCliente, @CuentaContableCosto, @CuentaContableVenta, @CuentaContableProveedor,@Produccion)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TIPO DE PRODUCTO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From TipoProducto Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From TipoProducto Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update TipoProducto Set	Descripcion=@Descripcion,
								Estado = @Estado,
								Referencia=@Referencia,
								CuentaContableCliente=@CuentaContableCliente,
								CuentaContableCosto=@CuentaContableCosto,
								CuentaContableVenta=@CuentaContableVenta,
								CuentaContableProveedor=@CuentaContableProveedor,
								Producido=@Produccion
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TIPO DE PRODUCTO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From TipoProducto Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Productos
		if exists(Select * From Producto Where IDTipoProducto=@ID) begin
			set @Mensaje = 'El registro tiene productos asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con lineas
		if exists(Select * From TipoProductoLinea Where IDTipoProducto=@ID) begin
			set @Mensaje = 'El registro tiene lineas asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From TipoProducto 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TIPO DE PRODUCTO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


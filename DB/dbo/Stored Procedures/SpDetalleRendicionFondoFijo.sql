﻿CREATE  Procedure [dbo].[SpDetalleRendicionFondoFijo]

	--Entrada
	@IDSucursal tinyint,
	@IDGrupo tinyint,	
	@IDTransaccionGasto numeric (18,0)=NULL,
	@IDTransaccionVale numeric (18,0)=NULL,
	@Importe money=NULL,
	@Saldo money=NULL,
		
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	    --Si es que la descripcion ya existe
		if exists(Select * From DetalleFondoFijo Where IDTransaccionGasto = @IDTransaccionGasto) begin
			Update DetalleFondoFijo
			set IDTransaccionVale = @IDTransaccionVale,
			Importe = Importe + @Importe
			where IDTransaccionGasto = @IDTransaccionGasto

			set @Mensaje = 'Registro guardado!'
		    set @Procesado = 'True'
			return @@rowcount
		end
			
		declare @vID numeric(18,0)
			
		--Obtenemos el nuevo ID
		set @vID = (Select ISNull(Max(ID+1),1) From DetalleFondoFijo )
		set @Saldo = @Saldo - @Importe
					
		--Insertamos en Detalle Fondo Fijo
		Insert Into DetalleFondoFijo (IDSucursal,IDGrupo,ID,IDTransaccionVale,IDTransaccionGasto,Importe,Cancelado,IDTransaccionRendicionFondoFijo)
		values (@IDSucursal,@IDGrupo,@vID,0,@IDTransaccionGasto, @Importe,'False', NULL )
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		

End


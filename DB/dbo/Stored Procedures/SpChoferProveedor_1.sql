﻿
CREATE Procedure [dbo].[SpChoferProveedor]

	--Entrada
	@ID int,
	@IDCamion int,
	@Nombres varchar(50),
	@NroDocumento varchar(15) = NULL,
	@Telefono varchar(20) = NULL,
	@Celular varchar(20) = NULL,
	@Direccion varchar(100) = NULL,
	@Email varchar(50) = NULL,
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la Nombres ya existe
		if exists(Select * From ChoferProveedor Where Nombres=@Nombres) begin
			set @Mensaje = 'El nombre ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDChofer int
		set @vIDChofer = (Select IsNull((Max(ID)+1), 1) From ChoferProveedor)

		--Insertamos
		Insert Into ChoferProveedor(ID,IDCamion, Nombres, NroDocumento, Telefono, Celular, Direccion, Email, Estado)
		Values(@vIDChofer,@IDCamion, @Nombres, @NroDocumento, @Telefono, @Celular, @Direccion, @Email, @Estado)	
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CHOFERPROVEEDOR', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
					
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From ChoferProveedor Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la Nombres ya existe
		if exists(Select * From ChoferProveedor Where Nombres=@Nombres And ID!=@ID) begin
			set @Mensaje = 'El nombre ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update ChoferProveedor Set Nombres=@Nombres,
							NroDocumento=@NroDocumento, 
							Telefono=@Telefono, 
							Celular=@Celular, 
							Direccion=@Direccion, 
							Email=@Email,
							Estado = @Estado,
							IDcamion = @IDCamion
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CHOFERPROVEEDOR', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
			
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From ChoferProveedor Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		----Si tiene una relacion con CLIENTES
		--if exists(Select * From TicketBascula Where IDChofer=@ID) begin
		--	set @Mensaje = 'El registro tiene registros asociados! No se puede eliminar.'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--end
		
		--Eliminamos
		Delete From ChoferProveedor
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='CHOFERPROVEEDOR', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
			
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End




﻿CREATE Procedure [dbo].[SpPromotor]

	--Entrada
	@ID tinyint,
	@Nombres varchar(50),
	@NroDocumento varchar(15) = NULL,
	@Telefono varchar(20) = NULL,
	@Celular varchar(20) = NULL,
	@Direccion varchar(100) = NULL,
	@Email varchar(50) = NULL,
	@Estado bit,
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la Nombres ya existe
		if exists(Select * From Promotor Where Nombres=@Nombres) begin
			set @Mensaje = 'El nombre ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDPromotor tinyint
		set @vIDPromotor = (Select IsNull((Max(ID)+1), 1) From Promotor)

		--Insertamos
		Insert Into Promotor(ID, Nombres, NroDocumento, Telefono, Celular, Direccion, Email, Estado)
		Values(@vIDPromotor, @Nombres, @NroDocumento, @Telefono, @Celular, @Direccion, @Email, @Estado)		
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From Promotor Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la Nombres ya existe
		if exists(Select * From Promotor Where Nombres=@Nombres And ID!=@ID) begin
			set @Mensaje = 'El nombre ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update Promotor Set Nombres=@Nombres,
							NroDocumento=@NroDocumento, 
							Telefono=@Telefono, 
							Celular=@Celular, 
							Direccion=@Direccion, 
							Email=@Email,
							Estado = @Estado
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Promotor Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con CLIENTES
		if exists(Select * From Cliente Where IDPromotor=@ID) begin
			set @Mensaje = 'El registro tiene clientes asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Promotor 
		Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


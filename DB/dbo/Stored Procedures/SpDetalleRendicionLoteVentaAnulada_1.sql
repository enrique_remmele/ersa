﻿CREATE Procedure [dbo].[SpDetalleRendicionLoteVentaAnulada]

	--Entrada
	@IDTransaccion numeric(18,0),
	@ID tinyint = NULL,
	@IDTransaccionVenta numeric(18,0) = NULL,
	@IDMotivo smallint,
	@Importe money = NULL,
		
	--Operacion
	@Operacion varchar(10),
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
As

Begin
	
	--Procesar Saldos de Ventas
	If @Operacion = 'INS' Begin
	
		--Validar
		--Asegurarse que exista la transaccion
		if Not Exists(Select * From RendicionLote Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra el registro!'
			Set @Procesado = 'False'
			Return @@rowcount
		End
		
		--si es que ya existe el detalle, eliminar primeramente
		If Exists(Select * From DetalleRendicionLoteVentaAnulada Where IDTransaccion=@IDTransaccion) Begin
			Delete From DetalleRendicionLoteVentaAnulada Where IDTransaccion=@IDTransaccion 	
		End
		
		--Insertar
		Insert Into DetalleRendicionLoteVentaAnulada(IDTransaccion, ID, IDTransaccionVenta, IDMotivo, Importe)
		Values(@IDTransaccion, @ID, @IDTransaccionVenta, @IDMotivo, @Importe)
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		
		Return @@rowcount
		
	End
	
	If @Operacion = 'ANULAR' Begin
		
		--Validar
		--Que exista el registro
		if Not Exists(Select * From RendicionLote Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra el registro!'
			Set @Procesado = 'False'
			Return @@rowcount
		End
	
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'True'
		Return @@rowcount
		
	End
	
	If @Operacion = 'DEL' Begin
		
		
		Set @Mensaje = 'Registo procesado!'
		Set @Procesado = 'false'
		Return @@rowcount
		
	End
		
	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'
	
End




﻿CREATE Procedure [dbo].[SpViewEstadoResultadoMultiColumna]

	--Entrada
	@Año smallint,
	@Mes tinyint,
	@MesHasta tinyint,
		
	@IDSucursal int = 0,
	@IDUnidadNegocio int = 0,
	
	@Factor int
	
As

Begin

	SET NOCOUNT ON
	
	--variables
	Declare @vID tinyint
	Declare @vM varchar(50)
	Declare @vCodigo varchar(50)
	Declare @vDenominacion varchar(200)
	Declare @vCategoria tinyint
	Declare @vImputable bit
	Declare @vDebito money
	Declare @vCredito money
	Declare @vSaldo money
	Declare @vIDUnidadNegocio int
	
	Declare @vMovimientoCierre money
	Declare @vCreditoCierre money
	Declare @vDebitoCierre money
		
	--Crear la tabla
	Create table #EstadoResultado(ID tinyint,
								Año smallint,
								IDSucursal int,
								M varchar(50),
								Codigo varchar(50),
								Denominacion varchar(200),
								Categoria tinyint,
								Imputable bit,
								MostrarCodigo varchar(50),
								IDUnidadNegocio int,
								[1] money,
								[2] money,
								[3] money,
								[4] money,
								[5] money,
								[6] money,
								[7] money,
								[8] money,
								[9] money,
								[10] money,
								[11] money,
								[12] money,
								Acumulado As ([1]+[2]+[3]+[4]+[5]+[6]+[7]+[8]+[9]+[10]+[11]+[12])
								)
																
	set @vID = (Select IsNull(MAX(ID)+1,1) From #EstadoResultado)
	
	If @Factor = 0 Begin
		Set @Factor = 1
	End
	
	Declare @vMesActual tinyint = @Mes
	
	While @vMesActual <= @Meshasta Begin
			
		Declare db_cursor cursor for
		Select	CC.Codigo, CC.Descripcion, CC.Categoria, CC.Imputable, Isnull(CC.IDUnidadNegocio,0)	
		From VCuentaContable CC	
		Where (SUBSTRING(CC.Codigo, 0, 2) Between '4' And '9')
		And (Case When @IDUnidadNegocio = 0 then 0 else IDUnidadNegocio end) = (Case When @IDUnidadNegocio = 0 then 0 else @IDUnidadNegocio end)
		Open db_cursor   
		Fetch Next From db_cursor Into	@vCodigo, @vDenominacion, @vCategoria, @vImputable, @vIDUnidadNegocio
		While @@FETCH_STATUS = 0 Begin 
			
			--Tipo de Cuenta
			Declare @vCuentaTipo varchar(50)
			Declare @vPrefijo varchar(50)

			Set @vPrefijo = SubString(@vCodigo, 0, 2)

			--Cuentas del Debe
			If @vPrefijo = '1' Or @vPrefijo = '5' Begin
				Set @vCuentaTipo = 'DEBE'
			End

			--Cuentas del Haber
			If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
				Set @vCuentaTipo = 'HABER'
			End
			
			--Saldo Actual
			--Toda la Empresa
			If @IDSucursal = 0 Begin
				Set @vDebito = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes=@vMesActual And Cuenta=@vCodigo),0)
				Set @vCredito = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes=@vMesActual And Cuenta=@vCodigo),0)
			End
			
			--Por Sucursal
			If @IDSucursal > 0 Begin
				Set @vDebito = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@Año And Mes=@vMesActual And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
				Set @vCredito = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@Año And Mes=@vMesActual And Cuenta=@vCodigo And IDSucursal=@IDSucursal),0)
			End
			
--Saldo de CierreContable
		if @IdSucursal = 0 begin
			Set @vDebitoCierre = Isnull((select DA.Debito from Asiento A
								join DetalleAsiento DA on DA.IDTransaccion = A.IDTransaccion
								join CierreReapertura CR on CR.IDtransaccion = A.Idtransaccion
								where CR.Comprobante = 'CIERRE'
								and DA.CuentaContable = @vCodigo
								and Anho = @Año),0)
			Set @vCreditoCierre = Isnull((select DA.Credito from Asiento A
								join DetalleAsiento DA on DA.IDTransaccion = A.IDTransaccion
								join CierreReapertura CR on CR.IDtransaccion = A.Idtransaccion
								where CR.Comprobante = 'CIERRE'
								and DA.CuentaContable = @vCodigo
								and Anho = @Año),0)
	   End
	   If @IdSucursal > 0 begin
	   Set @vDebitoCierre = Isnull((select DA.Debito from Asiento A
								join DetalleAsiento DA on DA.IDTransaccion = A.IDTransaccion
								join CierreReapertura CR on CR.IDtransaccion = A.Idtransaccion
								where CR.Comprobante = 'CIERRE'
								and DA.CuentaContable = @vCodigo
								and CR.IDSucursal =@IdSucursal
								and Anho = @Año),0)
				Set @vCreditoCierre = Isnull((select DA.Credito from Asiento A
								join DetalleAsiento DA on DA.IDTransaccion = A.IDTransaccion
								join CierreReapertura CR on CR.IDtransaccion = A.Idtransaccion
								where CR.Comprobante = 'CIERRE'
								and DA.CuentaContable = @vCodigo
								and CR.IDSucursal =@IdSucursal
								and Anho = @Año),0)
	   end			
			
			If @vCuentaTipo = 'DEBE' Begin
			    Set @vSaldo = @vDebito - @vCredito
			End
			
			If @vCuentaTipo = 'HABER' Begin
			    Set @vSaldo = @vCredito - @vDebito
			End
			
			Set @vSaldo = @vSaldo / @Factor
				
			--Si ya existe, actualizar
			--Toda la Empresa
			--If @IDSucursal = 0 Begin
			
				If Not Exists(Select * From #EstadoResultado Where ID=@vID And Codigo=@vCodigo) Begin
					Insert Into #EstadoResultado(ID, Año, M, Codigo, Denominacion, Categoria, Imputable, [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12] ,MostrarCodigo, IDUnidadNegocio)
					Values(@vID, @Año, dbo.FMes(@vMesActual), @vCodigo, @vDenominacion, @vCategoria, @vImputable, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '1', @vIDUnidadNegocio)
				End
				
				--Enero
				If @vMesActual = 1 Begin Update #EstadoResultado Set [1]=@vSaldo Where ID=@vID And Codigo=@vCodigo End
			
				--Febrero
				If @vMesActual = 2 Begin Update #EstadoResultado Set [2]=@vSaldo Where ID=@vID And Codigo=@vCodigo End
				
				--MArzo
				If @vMesActual = 3 Begin Update #EstadoResultado Set [3]=@vSaldo Where ID=@vID And Codigo=@vCodigo End
				
				--Abril
				If @vMesActual = 4 Begin Update #EstadoResultado Set [4]=@vSaldo Where ID=@vID And Codigo=@vCodigo End
				
				--Mayo
				If @vMesActual = 5 Begin Update #EstadoResultado Set [5]=@vSaldo Where ID=@vID And Codigo=@vCodigo End
				
				--Junio
				If @vMesActual = 6 Begin Update #EstadoResultado Set [6]=@vSaldo Where ID=@vID And Codigo=@vCodigo End
				
				--Julio
				If @vMesActual = 7 Begin Update #EstadoResultado Set [7]=@vSaldo Where ID=@vID And Codigo=@vCodigo End
				
				--Agosto
				If @vMesActual = 8 Begin Update #EstadoResultado Set [8]=@vSaldo Where ID=@vID And Codigo=@vCodigo End
				
				--Setiembre
				If @vMesActual = 9 Begin Update #EstadoResultado Set [9]=@vSaldo Where ID=@vID And Codigo=@vCodigo End
				
				--Octubre
				If @vMesActual = 10 Begin Update #EstadoResultado Set [10]=@vSaldo Where ID=@vID And Codigo=@vCodigo End
				
				--Noviembre
				If @vMesActual = 11 Begin Update #EstadoResultado Set [11]=@vSaldo Where ID=@vID And Codigo=@vCodigo End
				
				
				--Diciembre
				--If @vMesActual = 12 Begin Update #EstadoResultado Set [12]=@vSaldo Where ID=@vID And Codigo=@vCodigo End
				
				--Prueba Dani
				If @vCuentaTipo = 'DEBE' Begin
			      Set @vSaldo = @vDebito - @vCredito
			      If @vMesActual = 12 Begin Update #EstadoResultado Set [12]=@vSaldo+(@vCreditoCierre+@vDebitoCierre) Where ID=@vID And Codigo=@vCodigo End
				End
			
		     	If @vCuentaTipo = 'HABER' Begin
			      If @vMesActual = 12 Begin Update #EstadoResultado Set [12]=@vSaldo+(@vDebitoCierre+@vCreditoCierre) Where ID=@vID And Codigo=@vCodigo End
			    End				
				
			--End
			
			----Por Sucursal
			--If @IDSucursal > 0 Begin
				
			--	If Not Exists(Select * From #EstadoResultado Where ID=@vID And Codigo=@vCodigo And IDSucursal=@IDSucursal) Begin
			--		Insert Into #EstadoResultado(ID, Año, M, Codigo, Denominacion, Categoria, Imputable, [1], [2], [3], MostrarCodigo)
			--		Values(@vID, @Año, dbo.FMes(@vMesActual), @vCodigo, @vDenominacion, @vCategoria, @vImputable, 0, 0, 0, '1')
			--	End
				
			--	--Enero
			--	If @vMesActual = 1 Begin Update #EstadoResultado Set [1]=@vSaldo Where ID=@vID And Codigo=@vCodigo And IDSucursal=@IDSucursal End
			
			--	--Febrero
			--	If @vMesActual = 2 Begin Update #EstadoResultado Set [2]=@vSaldo Where ID=@vID And Codigo=@vCodigo And IDSucursal=@IDSucursal End
				
			--	--Marzo
			--	If @vMesActual = 3 Begin Update #EstadoResultado Set [3]=@vSaldo Where ID=@vID And Codigo=@vCodigo And IDSucursal=@IDSucursal End
				
			--	--Abril
			--	If @vMesActual = 4 Begin Update #EstadoResultado Set [4]=@vSaldo Where ID=@vID And Codigo=@vCodigo And IDSucursal=@IDSucursal End
				
			--	--Mayo
			--	If @vMesActual = 5 Begin Update #EstadoResultado Set [5]=@vSaldo Where ID=@vID And Codigo=@vCodigo And IDSucursal=@IDSucursal End
				
			--	--Junio
			--	If @vMesActual = 6 Begin Update #EstadoResultado Set [6]=@vSaldo Where ID=@vID And Codigo=@vCodigo And IDSucursal=@IDSucursal End
				
			--	--Julio
			--	If @vMesActual = 7 Begin Update #EstadoResultado Set [7]=@vSaldo Where ID=@vID And Codigo=@vCodigo And IDSucursal=@IDSucursal End
				
			--	--Agosto
			--	If @vMesActual = 8 Begin Update #EstadoResultado Set [8]=@vSaldo Where ID=@vID And Codigo=@vCodigo And IDSucursal=@IDSucursal End
				
			--	--Setiembre
			--	If @vMesActual = 9 Begin Update #EstadoResultado Set [9]=@vSaldo Where ID=@vID And Codigo=@vCodigo And IDSucursal=@IDSucursal End
				
			--	--Octubre
			--	If @vMesActual = 10 Begin Update #EstadoResultado Set [10]=@vSaldo Where ID=@vID And Codigo=@vCodigo And IDSucursal=@IDSucursal End
				
			--	--Noviembre
			--	If @vMesActual = 11 Begin Update #EstadoResultado Set [11]	=@vSaldo Where ID=@vID And Codigo=@vCodigo And IDSucursal=@IDSucursal End
				
			--	--Diciembre
			--	If @vMesActual = 12 Begin Update #EstadoResultado Set [12]=@vSaldo Where ID=@vID And Codigo=@vCodigo And IDSucursal=@IDSucursal End
				
			--End
			
	Siguiente:
			
			Fetch Next From db_cursor Into	@vCodigo, @vDenominacion, @vCategoria, @vImputable, @vIDUnidadNegocio
			
		End

		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   		
	
		Set @vMesActual = @vMesActual + 1
				
	End	
		
	Select * From #EstadoResultado Where ID=@vID order by codigo
	
End

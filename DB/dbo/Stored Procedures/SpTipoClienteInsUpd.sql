﻿CREATE Procedure [dbo].[SpTipoClienteInsUpd]

	--Entrada
	@Descripcion varchar(100)
	
As

Begin

	Declare @vID int
	
	If @Descripcion = '' Begin
		Set @Descripcion = 'VARIOS'
	End
	
	--Si existe
	If Exists(Select * From TipoCliente Where Descripcion = @Descripcion) Begin
		Set @vID = (Select Top(1) ID From TipoCliente Where Descripcion = @Descripcion)
		
	End Else Begin
		Set @vID = (Select IsNull(Max(ID)+1,1) From TipoCliente)
		Insert Into TipoCliente(ID, Descripcion, Estado)
		Values(@vID, @Descripcion, 'True')
	End
	
	return @vID
	
End


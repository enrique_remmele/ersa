﻿CREATE Procedure [dbo].[SpTipoProductoCuentaContableMotivoNC]

	--Entrada
	@IDTipoProducto int,
	@CuentaContableAcuerdo varchar(50) = '',
	@CuentaContableDescuento varchar(50) = '',
	@CuentaContableDiferenciaPrecio varchar(50) = '',
	--Auditoria
	@IDUsuario int,
	@IDSucursal int,
	@IDDeposito int,
	@IDTerminal int,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

		Declare @vNuevo bit = 'False'
		
		--Si el ID existe
		If not exists(Select * From TipoProducto Where ID=@IDTipoProducto) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la configuracion no existe
		if not exists(Select * From TipoProductoCuentaContableMotivoNC Where IDTipoProducto=@IDTipoProducto ) begin
			Set @vNuevo = 'True'
		end
		

		if @vNuevo = 'True' begin
			--Insertamos
			Insert Into TipoProductoCuentaContableMotivoNC(IDTipoProducto, CuentaContableAcuerdo, CuentaContableDescuento, CuentaContableDiferenciaPrecio, UltimoUsuarioModificacion, UltimaFechaModificacion)
			Values(@IDTipoProducto, @CuentaContableAcuerdo, @CuentaContableDescuento, @CuentaContableDiferenciaPrecio, @IDUsuario, GETDATE())
		
			--Auditoria
			Exec SpLogSuceso @Operacion='INS', @Tabla='TipoProductoCuentaContableMotivoNC', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
			set @Mensaje = 'Registro guardado!'
			set @Procesado = 'True'
			return @@rowcount
		end 
		else if @vNuevo = 'False' begin
			--Actualizamos
			select * from TipoProductoCuentaContableMotivoNC
			Update TipoProductoCuentaContableMotivoNC Set CuentaContableAcuerdo=@CuentaContableAcuerdo,
									CuentaContableDescuento=@CuentaContableDescuento,
									CuentaContableDiferenciaPrecio=@CuentaContableDiferenciaPrecio,
									UltimoUsuarioModificacion=@IDUsuario,
									UltimaFechaModificacion=GetDate()
			Where IDTipoProducto=@IDTipoProducto
		
			--Auditoria
			Exec SpLogSuceso @Operacion='UPD', @Tabla='TipoProductoCuentaContableMotivoNC', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
			set @Mensaje = 'Registro guardado!'
			set @Procesado = 'True'
			return @@rowcount
			
		end
	
		
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


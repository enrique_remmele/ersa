﻿CREATE Procedure [dbo].[SpImportarProveedor]

	--Entrada
	--Identificadores
	@Referencia varchar(50) = NULL,
	@RazonSocial varchar(150),
	@RUC varchar(50),
	@NombreFantasia varchar(150) = NULL,
	@Direccion1 varchar(200) = NULL,
	@Direccion2 varchar(200) = NULL,
	@Telefono varchar(200) = NULL,
	@Contado bit = 'True',
	@Credito bit = 'False',
	@PlazoCredito tinyint = 0,
	
	--Agrupadores
	@Pais varchar(20) = NULL,
	@Departamento varchar(20) = NULL,
	@Ciudad varchar(20) = NULL,
	@Barrio varchar(20) = NULL,
	@IDSucursal int = 1,
	@TipoProveedor varchar(100) = NULL,
	@IDMoneda int =1,
	
	@PaginaWeb varchar(200) = NULL,
	@Fax varchar(50) = NULL,
	@Email varchar(50) = NULL,
	
	@CodigoCuentaContableCompra varchar(100) = NULL,
	
	--Opciones
	@Actualizar Bit = 'False'
	
As

Begin

	--ID's
	Declare @vIDPais int
	Declare @vIDDepartamento int
	Declare @vIDCiudad int
	Declare @vIDTipoProveedor int
	Declare @vDireccion varchar(200)
	
	Declare @vID int
	Declare @vOperacion varchar(10) 
	Declare @Mensaje varchar(200)
	Declare @Procesado bit
		
	Begin
	
		--Pais
		Set @vIDPais = 1
		
		--Departamento
		Set @vIDDepartamento = (Select Top(1) ID From Departamento Where Codigo=@Departamento)
		
		--Ciudad
		Set @vIDCiudad = (Select Top(1) ID From Ciudad Where Referencia=@Ciudad)
		
		--Tipo de Proveedor
		Set @vIDTipoProveedor = (Select Top(1) ID From TipoProveedor Where Referencia=@TipoProveedor)
		
		Set @vDireccion = @Direccion1
		
		If @Direccion2 != '' Begin
			Set @vDireccion = @Direccion1 + ' ' + @Direccion2
		End
		
		If @PlazoCredito>0 Begin
			Set @Credito = 'True'
			Set @Contado = 'False'
		End	
		
	End

	Set	@vID = 0
	Set @vOperacion = 'INS'
	
	If Exists(Select * From Proveedor Where RUC=@RUC) And @Actualizar='True' Begin
		Set @vID = (Select Top(1) ID From Proveedor Where RUC=@RUC)		
		Set @vOperacion = 'UPD'
	End 
	
	--Si no existe por ruc, buscamos por codigo
	If @vID = 0 And Exists(Select * From Proveedor Where Referencia=@Referencia) And @Actualizar='True' Begin
		Set @vID = (Select Top(1) ID From Proveedor Where Referencia=@Referencia)		
		Set @vOperacion = 'UPD'
	End

	--Ejecutamos
	EXEC SpProveedor
		@ID = @vID,
		@RazonSocial = @RazonSocial,
		@RUC = @RUC,
		@Referencia = @Referencia,
		@Operacion = @vOperacion,
		@NombreFantasia = @NombreFantasia,
		@Direccion = @vDireccion,
		@Telefono = @Telefono,
		@Estado = 'True',
		@TipoCompra = 'I',
		@IDPais = 1,
		@IDDepartamento = 1,
		@IDCiudad = @vIDCiudad,
		@IDBarrio = NULL,
		@IDTipoProveedor = @vIDTipoProveedor,
		@IDMoneda = 1,
		@CuentaContableCompra = @CodigoCuentaContableCompra,
		@IDSucursal = 1,
		@PaginaWeb = @PaginaWeb,
		@Email = @Email,
		@Fax = @Fax,
		@Credito = @Credito,
		@PlazoCredito = @PlazoCredito,
		@Retentor = 'False',
		@SujetoRetencion = 'True',
		@Exportador = 'False',
		@IDUsuario = 1,
		@IDTerminal = 1,
		@Mensaje = @Mensaje OUTPUT,
		@Procesado = @Procesado OUTPUT


Salir:
	Select 'Mensaje'=@Mensaje + ' ' + @vOperacion, 'Procesado'=@Procesado, 'Cantidad'=@@ROWCOUNT
End



﻿CREATE Procedure [dbo].[SpViewExtractoMovimientoProveedorMonedas2]

	--Entrada
	@Fecha1 Date,
	@Fecha2 Date,
	@IDProveedor int = null,
	@Comprobante varchar (30) = '%%',
	@IDMoneda int = null
As

Begin
	--Crear la tabla temporal
    create table #TablaTemporal(
		IDTransaccion int,
		Codigo int,
		RazonSocial varchar(50),
		Referencia varchar(50),
		Fecha date,
		Operacion varchar(50),
		Documento varchar(50),
		[Detalle/Concepto] varchar(50),
		Debitos money,
		Creditos money,
		Saldo money,
		SaldoAnterior money,
		SaldoInicial money,
		IDMoneda int,
		DescripcionMoneda varchar(50),
		Movimiento char(20)
	)
								
	--Variables para calcular saldo
	declare @vSaldo money
	declare @vTotalDebito money
	declare @vTotalCredito money
	declare @vSaldoAnterior money
	declare @vSaldoInicial money
	declare @vIDMonedaCambio tinyint = 0
	
	--Declarar variables
	declare @vIDTransaccion Numeric (18,0)
	declare @vIDProveedor int
	declare @vRazonSocial varchar (100)
	declare @vReferencia varchar(50)
	declare @vFecha date
	declare @vOperacion varchar(50)
	declare @vDocumento varchar(50)
	declare @vDetalleConcepto varchar(50)
	declare @vDebitos money
	declare @vCreditos money
	declare @vIDMoneda int
	declare @vMoneda varchar(50)
	declare @vMovimiento varchar(20)

	begin
		
		if @IDMoneda is null begin
			if @IDProveedor is null begin
				declare cMoneda cursor for
				SELECT distinct idmoneda
				From VExtractoMovimientoProveedor
				Where Fecha between @Fecha1 and @Fecha2
			end
			else begin
				declare cMoneda cursor for
				SELECT distinct idmoneda
				From VExtractoMovimientoProveedor
				Where Fecha between @Fecha1 and @Fecha2 and Codigo = @IDProveedor
			end
		end
		else begin
			if @IDProveedor IS NULL BEGIN
				declare cMoneda cursor for
				SELECT distinct idmoneda
				From VExtractoMovimientoProveedor
				Where Fecha between @Fecha1 and @Fecha2 and IDMoneda = @idmoneda				
			END
			ELSE BEGIN
				declare cMoneda cursor for
				SELECT distinct idmoneda
				From VExtractoMovimientoProveedor
				Where Fecha between @Fecha1 and @Fecha2 and IDMoneda = @idmoneda and Codigo = @IDProveedor			
			END
		end

		open cMoneda
		fetch cMoneda into @vIdMoneda

		WHILE (@@FETCH_STATUS = 0 ) BEGIN
			
			IF @IDProveedor is null begin
				Declare cExtractoMoneda cursor for
				Select
					IDTransaccion,
					Codigo,
					RazonSocial,
					Referencia,
					Fecha,
					Operacion,
					Documento,
					[Detalle/Concepto],
					Debito,
					Credito,
					IDMoneda,
					DescripcionMoneda,
					Movimiento
				From VExtractoMovimientoProveedor
				Where IDMoneda = @vIDMoneda and Fecha between @Fecha1 and @Fecha2 And Documento Like @Comprobante
				Order By Fecha, RazonSocial, IdTransaccion Desc
			end
			else begin
				Declare cExtractoMoneda cursor for
				Select
					IDTransaccion,
					Codigo,
					RazonSocial,
					Referencia,
					Fecha,
					Operacion,
					Documento,
					[Detalle/Concepto],
					Debito,
					Credito,
					IDMoneda,
					DescripcionMoneda,
					Movimiento
				From VExtractoMovimientoProveedor
				Where Codigo=@IDProveedor and IDMoneda = @vIDMoneda and Fecha between @Fecha1 and @Fecha2 And Documento Like @Comprobante
				Order By Fecha, RazonSocial, IdTransaccion Desc
			end

			Open cExtractoMoneda  
			Fetch cExtractoMoneda Into @vIDTransaccion,@vIDProveedor,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vDebitos,@vCreditos,@vIDMoneda,@vMoneda,@vMovimiento

			--Hallar Totales para Saldo Anterior
			if @IDProveedor is null begin
				Set @vTotalDebito  = IsNull((Select Sum(Debito) From VExtractoMovimientoProveedor Where Fecha < @Fecha1 And IDMoneda=@vIDMoneda),0)
				Set @vTotalCredito = IsNull((Select Sum(Credito) From VExtractoMovimientoProveedor Where Fecha < @Fecha1 And IDMoneda=@vIDMoneda),0)
			end
			else begin
				Set @vTotalDebito  = IsNull((Select Sum(Debito) From VExtractoMovimientoProveedor Where Codigo=@IDProveedor And Fecha < @Fecha1 And IDMoneda=@vIDMoneda),0)
				Set @vTotalCredito = IsNull((Select Sum(Credito) From VExtractoMovimientoProveedor Where Codigo=@IDProveedor And Fecha < @Fecha1 And IDMoneda=@vIDMoneda),0)				
			end
			--Hallar Saldo Anterior
			Set @vSaldoAnterior =  @vTotalCredito - @vTotalDebito
			Set @vSaldoInicial = @vSaldoAnterior 
			--if @IDProveedor is null begin
			--	set @vRazonSocial = 'Todos'
			--end
			Insert Into  #TablaTemporal(IDTransaccion, Codigo, RazonSocial, Referencia, Fecha, Operacion, Documento, [Detalle/Concepto], Saldo, SaldoAnterior, SaldoInicial, Debitos, Creditos, IDMoneda, DescripcionMoneda, Movimiento) 
			Values (0, @vIDProveedor, @vRazonSocial, @vReferencia, @Fecha1, '', '', '==== SALDO ANTERIOR ' + @vMoneda + '====' , @vSaldoInicial, @vSaldoInicial, 0, 0, 0, @vIDMoneda, @vMoneda, 'SALDO')

			IF OBJECT_ID('tempdb..#Total') IS NOT NULL DROP TABLE #Total
			create table #Total(debito money, credito money)
			WHILE (@@FETCH_STATUS = 0 ) BEGIN

				--Hallar Saldo			
				set @vSaldo = (@vSaldoAnterior + @vCreditos) - @vDebitos

				--Insertar Fila en la Tabla Temporal
				--if @IDProveedor is null begin
				--	set @vRazonSocial = 'Todos'
				--end
				Insert Into  #TablaTemporal(IDTransaccion,Codigo,RazonSocial,Referencia,Fecha,Operacion,Documento,[Detalle/Concepto],Saldo,SaldoAnterior,SaldoInicial,Debitos,Creditos,IDMoneda,DescripcionMoneda,Movimiento) 
				Values (@vIDTransaccion,@vIDProveedor,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vSaldo,@vSaldoAnterior,@vSaldoInicial,@vDebitos,@vCreditos,@vIDMoneda,@vMoneda,@vMovimiento)

				insert into #Total(debito, credito) values (@vDebitos, @vCreditos)

				--Actualizar Saldo
				set @vSaldoAnterior = @vSaldo 

				Fetch cExtractoMoneda Into @vIDTransaccion,@vIDProveedor,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vDebitos,@vCreditos,@vIDMoneda,@vMoneda,@vMovimiento
		
			end

			--Insertar total acumulado
			select @vDebitos = ISNULL(SUM(debito),0), @vCreditos = ISNULL(sum(credito),0) from #Total
			--if @IDProveedor is null begin
			--	set @vRazonSocial = 'Todos'
			--end
			Insert Into  #TablaTemporal(IDTransaccion, Codigo, RazonSocial, Referencia, Fecha, Operacion, Documento, [Detalle/Concepto], Saldo, SaldoAnterior, SaldoInicial, Debitos, Creditos, IDMoneda, DescripcionMoneda, Movimiento) 
			Values (1, @vIDProveedor, @vRazonSocial, @vReferencia, @Fecha2, '', '', '==== SALDO ACUMULADO ' + @vMoneda + '====' , @vSaldo, 0, 0, @vDebitos, @vCreditos, @vIDMoneda, @vMoneda, 'ACUMULADO')

			--Cierra el cursor
			Close cExtractoMoneda   
			Deallocate cExtractoMoneda

			fetch cMoneda into @vIdMoneda
		end

		--Cierra el cursor
		Close cMoneda   
		Deallocate cMoneda

		--Fin del while de itereacion
	end
	Select *, Fec=CONVERT(varchar(50), Fecha, 5) From #TablaTemporal --ORder by Fecha, RazonSocial, IdTransaccion
end
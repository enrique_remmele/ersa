﻿CREATE Procedure [dbo].[SpImportarRetencion]

	--Entrada
	--Punto de Expedicion
	@TipoComprobante varchar(10),
	@Comprobante varchar(50),
	@Timbrado varchar(20) = '',

	--Proveedor
	@Proveedor varchar(50),
	@RUC varchar(50),
	
	--Cabecera
	@Sucursal varchar(50) = '',
	@Fecha varchar(50),
	@Moneda varchar(50) = '',
	@Cotizacion tinyint = 1,
	@Observacion varchar(200),
	
	--Totales
	@Total money,
	@TotalImpuesto money,
	
	--Impuesto
	@Exento as money,
	@Gravado10 as money,
	@IVA10 as money,
	@Gravado5 as money,
	@IVA5 as money,
		
	--Configuraciones
	@Anulado bit,
	
	--Compra
	@ComprobanteFactura as varchar(20), 
	@FechaFactura as date, 
		
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	@Actualizar bit

As

Begin

--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		
		Declare @vOperacion varchar(50)
		Declare @vIDTransaccion int
		Declare @Comprobantetemp varchar(50) = @Comprobante
		Declare @vReferenciaSucursal varchar(5) = ''
		Declare @vReferenciaPuntoExpedicion varchar(5) = ''
		Declare @vNroComprobante varchar(15) = ''
	
		Declare @vTotalDiscriminado money
		Declare @vRetencionIVA as money
		Declare @vGravado5Factura as money
		Declare @vIVA5Factura as money
		Declare @vGravado10Factura as money
		Declare @vIVA10Factura as money
		
		--Codigos
		Declare @vIDPuntoExpedicion int 
		Declare @vIDTipoComprobante int
		Declare @vIDProveedor int
		Declare @vIDSucursal int
		Declare @vIDDeposito int
		
		
	End
	
	
	--Establecer valores predefinidos
	Begin
		
		Set @vOperacion = 'INS'
		Set @vTotalDiscriminado = @Total - @TotalImpuesto
		
		--Comprobante
		Set @vReferenciaSucursal = SUBSTRING(@Comprobantetemp, 0, CharIndex('-', @Comprobantetemp, 0))
		Set @Comprobantetemp = SUBSTRING(@Comprobantetemp, CharIndex('-', @Comprobantetemp, 0) + 1, LEN(@Comprobantetemp) - 1 )
		Set @vReferenciaPuntoExpedicion = SUBSTRING(@Comprobantetemp, 0, CharIndex('-', @Comprobantetemp, 0))
		Set @Comprobantetemp = SUBSTRING(@Comprobantetemp, CharIndex('-', @Comprobantetemp, 0) + 1, LEN(@Comprobantetemp) - 1 )
		Set @vNroComprobante = Convert(int, @Comprobantetemp) 
		
		Set @vGravado5Factura = @Gravado5
		Set @vIVA5Factura = @IVA5
		Set @vGravado10Factura = @Gravado10
		Set @vIVA10Factura = @IVA10
		
		Set @vRetencionIVA = (@vIVA5Factura + @vIVA10Factura) * 0.3
		
	End
	
	--Verificar si ya existe
	If Exists(Select * From RetencionIVAImportada Where Timbrado=@Timbrado And Comprobante=@Comprobante) Begin
		set @vIDTransaccion = (Select Top(1) IDTransaccion From RetencionIVAImportada Where Timbrado=@Timbrado And Comprobante=@Comprobante)
		Set @vOperacion = 'UPD'
	End
	
	If @vOperacion = 'UPD' Begin
	
		If @Actualizar = 'True' Begin
		
			--NOta Credito
			Update RetencionIVA  Set Total =@Total,
									Anulado=@Anulado									
			Where IDTransaccion = @vIDTransaccion
			
			--Compra
			If Not Exists(Select IDTransaccion From DetalleRetencion Where IDTransaccion=@vIDTransaccion And TipoyComprobante=@ComprobanteFactura) Begin
				--Insertar en el detalle retencion
				Insert Into DetalleRetencion(IDTransaccion, ID, TipoyComprobante, Fecha, Gravado5, IVA5, Gravado10, IVA10, Total5, Total10, PorcRetencion, RetencionIVA, PorRenta, Renta)
				Values(@vIDTransaccion, 0, @ComprobanteFactura, @FechaFactura, @vGravado5Factura - @vIVA5Factura, @vIVA5Factura, @vGravado10Factura - @vIVA10Factura, @vIVA10Factura, @vGravado5Factura, @vGravado10Factura, 30, @vRetencionIVA, 0, 0)
			End
			
			Set @vMensaje = 'Actualizado!'
			Set @vProcesado = 'True'
			GoTo Salir
			
		End
		
		Set @vMensaje = 'Sin Act.!'
		Set @vProcesado = 'True'
		GoTo Salir
		
	End
	
	--Hayar Valores
	Begin
		
		Set @vMensaje = 'No se proceso!'
		Set @vProcesado = 'False'
		
		--Punto de Expedicion
		Set @vIDPuntoExpedicion = IsNull((Select Top(1) ID From VPuntoExpedicion Where ReferenciaSucursal=@vReferenciaSucursal And ReferenciaPunto=@vReferenciaPuntoExpedicion And TipoComprobante=@TipoComprobante And NumeracionDesde <= @vNroComprobante And NumeracionHasta >= @vNroComprobante),0)
		If @vIDPuntoExpedicion = 0 Begin
			Set @vMensaje = 'No se encontro el Punto de Expedicion!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
				
		--Tipo de Comprobante
		Set @vIDTipoComprobante = (IsNull((Select Top(1) ID From TipoComprobante Where Codigo=@TipoComprobante And IDOperacion=@IDOperacion), 0))
		If @vIDTipoComprobante = 0 Begin
			Set @vMensaje = 'Tipo de comprobante incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Proveedor
		Set @vIDProveedor=(IsNull((Select Top(1) ID From Proveedor Where RUC=@RUC And RazonSocial=@Proveedor),0))
		--Insertamos si no existe
		If @vIDProveedor = 0 Begin
			Set @vIDProveedor = IsNull((Select Max(ID) + 1 From Proveedor), 1)
			Insert Into Proveedor(ID, Referencia, RazonSocial, RUC, Estado, Credito, Retentor, SujetoRetencion, Exportador)
			Values(@vIDProveedor, @RUC, @Proveedor, @RUC, 1, 1, 0, 1, 0)			
		End
		
		--Sucursal
		Set @vIDSucursal = (IsNull((Select Top(1) ID From Sucursal Where Codigo=@Sucursal),0))
		If @vIDSucursal = 0 Begin
			Set @vMensaje = 'Sucursal incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
	End
	
	--INSERTAR
	If @vOperacion = 'INS' Begin
		
		--Validar Informacion
		Begin
		
			--Comprobante
			if Exists(Select * From RetencionIVA  Where IDPuntoExpedicion=@vIDPuntoExpedicion And NroComprobante=@vNroComprobante) Begin
				set @vMensaje = 'El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
				set @vProcesado = 'False'
				GoTo Salir
			End
			
			--Comprobante fuera del Rango
			if @vNroComprobante < (Select NumeracionDesde From PuntoExpedicion Where ID=@vIDPuntoExpedicion) Or @vNroComprobante > (Select NumeracionHasta From PuntoExpedicion Where ID=@vIDPuntoExpedicion) Begin
				set @vMensaje = 'El numero de comprobante no esta dentro del rango correcto! Cambie el punto de expedicion o actualicelo.'
				set @vProcesado = 'False'
				GoTo Salir
			End
			
		End
		
		----Insertar la transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@MostrarTransaccion = 'False',
			@Mensaje = @vMensaje OUTPUT,
			@Procesado = @vProcesado OUTPUT,
			@IDTransaccion = @vIDTransaccion OUTPUT
		
		If @vProcesado = 0 Begin
			GoTo Salir
		End
			
		--Insertar en Nota Credito
		Insert Into RetencionIVA (IDTransaccion, IDPuntoExpedicion,  IDTipoComprobante,  NroComprobante, IDProveedor, IDSucursal, Fecha, IDMoneda, Cotizacion, Observacion, TotalIVA, TotalRenta,  Anulado, FechaAnulado, IDUsuarioAnulado, Procesado, IDTransaccionOrdenPago, Total, OP)
						Values(@vIDTransaccion, @vIDPuntoExpedicion, @vIDTipoComprobante, @vNroComprobante, @vIDProveedor,  @vIDSucursal, @Fecha, 1, @Cotizacion, @Observacion, @Total, 0, @Anulado, @Fecha,  NULL,  1, NULL,  @Total, 'False')
		
		--Insertar en VentaImportacion
		Insert Into RetencionIVAImportada (IDTransaccion, Timbrado, Comprobante)
		Values(@vIDTransaccion, @Timbrado, @Comprobante)
		
		--Insertar en el detalle retencion
		Insert Into DetalleRetencion(IDTransaccion, ID, TipoyComprobante, Fecha, Gravado5, IVA5, Gravado10, IVA10, Total5, Total10, PorcRetencion, RetencionIVA, PorRenta, Renta)
		Values(@vIDTransaccion, 0, @ComprobanteFactura, @FechaFactura, @Gravado5, @IVA5, @Gravado10, @IVA10, @IVA5 + @Gravado5, @IVA10+@Gravado10, 30, @vRetencionIVA, 0, 0)
	
		--Insertar Impuesto
		--10%
		Insert into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado)
		Values(@vIDTransaccion, 1, @Gravado10 + @IVA10, @IVA10, 0, @Gravado10)
		
		--5%
		Insert into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado)
		Values(@vIDTransaccion, 2, @Gravado5 + @IVA5, @IVA5, 0, @Gravado5)
		
		--Exento
		Insert into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado)
		Values(@vIDTransaccion, 3, @Exento, 0, 0, @Exento - 0)
		
		Set @vMensaje = 'Registro guardado!'
		Set @vProcesado = 'True'
		GoTo Salir
				
	End
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


﻿CREATE Procedure [dbo].[SpBarrioInsUpd]

	--Entrada
	@Descripcion varchar(100)
	
As

Begin

	Declare @vID int
	
	If @Descripcion = '' Begin
		Set @Descripcion = 'CENTRAL'
	End
	
	--Si existe
	If Exists(Select * From Barrio Where Descripcion = @Descripcion) Begin
		Set @vID = (Select Top(1) ID From Barrio Where Descripcion = @Descripcion)
		
	End Else Begin
		Set @vID = (Select IsNull(Max(ID)+1,1) From Barrio)
		Insert Into Barrio(ID, Descripcion, Orden, Estado)
		Values(@vID, @Descripcion, 0, 'True')
	End
	
	return @vID
	
End


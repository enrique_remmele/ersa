﻿CREATE Procedure [dbo].[SpImportarGastoFechaEmision]
	--Cabecera
	@TipoComprobante varchar(10),
	@Comprobante varchar(50),
	@Referencia varchar(50),
	@Proveedor varchar(50),
	@RUC varchar(50),
	@Fecha date,
	@Credito varchar(50),
	@FechaVencimiento date,
	@Sucursal varchar(50),
	@ReferenciaSucursal varchar(50) = 1,
	@ReferenciaTerminal varchar(50) = 1,

	@Moneda varchar(50),		
	@Cotizacion money,
	@Observacion varchar(200),
	
	--Timbrado
	@NroTimbrado varchar(50)=NULL,
	@FechaVencimientoTimbrado varchar(10)=NULL,
	
	--Totales
	@Total money,
	@TotalImpuesto money,
	@Saldo money,
	
	--Impuesto
	@Exento as money,
	@Gravado10 as money,
	@IVA10 as money,
	@Gravado5 as money,
	@IVA5 as money,
			
	--Datos Extras
	@RetencionIVA as money,
	
	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
		
	@Actualizar bit
	
		
As

Begin
		
	--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		
		Declare @vCredito bit
		Declare @vCancelado bit
		Declare @vIDMoneda int
		Declare @vOperacion varchar(50)
		Declare @vIDTransaccion int
		Declare @vComprobante varchar(50)

		--Codigos
		Declare @vNumero int
		Declare @vIDTipoComprobante int
		Declare @vIDProveedor int
		Declare @vIDSucursal int
		Declare @vCajaChica int
					
	End
	
	--Hayar Valores
	Begin
		
		Set @vMensaje = 'No se procesó!'
		Set @vProcesado = 'False'
		
		--validar
		If IsDate(@FechaVencimientoTimbrado) = 0 Begin
			Set @vMensaje = 'La fecha de vencimiento del timbrado no es correcto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Comprobante anulado
		If CharIndex('_', @Comprobante) > 0 Begin
			Set @vMensaje = 'El comprobante no es correcto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End

		If IsDate(@FechaVencimientoTimbrado) = 0 Begin
			Set @vMensaje = 'La fecha de vencimiento del timbrado no es correcto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End

		--Tipo de Comprobante
		Set @vIDTipoComprobante = (IsNull((Select Top(1) ID From TipoComprobante Where Codigo=@TipoComprobante And IDOperacion=@IDOperacion), 0))
		--Insertamos el tipo de comprobante si no existe
		If @vIDTipoComprobante = 0 Begin
			Set @vIDTipoComprobante = IsNull((Select Max(ID) + 1 From TipoComprobante), 1)
			
			Insert into TipoComprobante(ID, Cliente, Proveedor, IDOperacion, Descripcion, Codigo, Resumen, Estado, ComprobanteTimbrado, CalcularIVA, IVAIncluido, LibroVenta, LibroCompra, Signo, Autonumerico, Deposito, Restar, HechaukaTipoDocumento, HechaukaTimbradoReemplazar, HechaukaNumeroTimbrado, EsInterno)
			Values(@vIDTipoComprobante, 'False', 'True', @IDOperacion, @TipoComprobante, @TipoComprobante, @TipoComprobante, 1, 1, 0, 1, 0, 1, '+', 0, 0, 0, 0, 0, '', 0)
			
		End
		
		--Proveedor
		Set @vIDProveedor=(IsNull((Select Top(1) ID From Proveedor Where RUC=@RUC),0))
		--Insertamos si no existe
		If @vIDProveedor = 0 Begin
			Set @vIDProveedor = IsNull((Select Max(ID) + 1 From Proveedor), 1)
			Insert Into Proveedor(ID, Referencia, RazonSocial, RUC, Estado, Credito, Retentor, SujetoRetencion, Exportador)
			Values(@vIDProveedor, @Referencia, @Proveedor, @RUC, 1, 1, 0, 1, 0)			
		End 
		
		--Actualizamos el proveedor
		
		--Sucursal
		Set @vIDSucursal = (IsNull((Select Top(1) ID From Sucursal Where Codigo=@Sucursal),0))
		If @vIDSucursal = 0 Begin
			Set @vMensaje = 'Sucursal incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Moneda
		Set @vIDMoneda = ISNULL((Select Top(1) ID From Moneda Where ID=@Moneda), 0)
		--Insertamos
		If @vIDMoneda = 0 Begin
			Set @vIDMoneda = IsNull((Select Max(ID) + 1 From Moneda), 1)
			Insert Into Moneda(ID, Descripcion, Referencia, Estado, Decimales, Divide, Multiplica)
			Values(@vIDMoneda, @Moneda, @Moneda, 1, 0, 0, 1)
		End
		
		Set @vComprobante = '0' + dbo.FFormatoDosDigitos(@ReferenciaSucursal) + '-' + '0' + dbo.FFormatoDosDigitos(@ReferenciaTerminal) + '-' + @Comprobante 		
	
	End


	--Verificar si ya existe
	If Exists(Select * From GastoImportado GI Join Gasto G On GI.IDTransaccion=G.IDTransaccion 
	Join TipoComprobante TC On G.IDTipoComprobante=TC.ID 
	Where G.NroComprobante=@vComprobante 
	And G.IDProveedor=@vIDProveedor 
	And G.NroTimbrado=@NroTimbrado 
	And TC.Codigo=@TipoComprobante) Begin
		set @vIDTransaccion = (Select Top(1) GI.IDTransaccion From GastoImportado GI Join Gasto G On GI.IDTransaccion=G.IDTransaccion Join TipoComprobante TC On G.IDTipoComprobante=TC.ID Where G.NroComprobante=@vComprobante And G.IDProveedor=@vIDProveedor And G.NroTimbrado=@NroTimbrado And TC.Codigo=@TipoComprobante)
		Set @vOperacion = 'UPD'
	End
	
	--ACTUALIZAR
	If @vOperacion = 'UPD' Begin
	
		If @Actualizar = 'True' Begin
			
			--Gasto
			Update Gasto Set	fecha = @Fecha
			Where IDTransaccion = @vIDTransaccion
			
			Set @vMensaje = 'Actualizado!'
			Set @vProcesado = 'True'
		End
		
		Set @vMensaje = 'Sin Act.!'
		Set @vProcesado = 'True'
		
	End
		
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


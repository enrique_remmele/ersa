﻿
CREATE Procedure [dbo].[SpComprobanteLibroIVA]

	--Entrada
	@IDTransaccion numeric(18,0)=NULL,
	@Numero INT=NULL,
	@NroComprobante varchar(50) = NULL,
	@IDSucursalOperacion int = NULL,
	@IDTipoComprobante smallint = NULL,
	@Fecha date = NULL,
	@Detalle varchar(200) = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money = NULL,
	@NroTimbrado varchar(50)=NULL,
	@FechaVencimientoTimbrado date=NULL,
	@IDProveedor int = NULL,
	@IDCliente int = NULL,
	@Credito bit = NULL,
	@IDOperacionComprobante int=NULL,
	@Directo bit= NULL,
	
	--Totales
	@Total money =NULL,
	@TotalImpuesto money=NULL ,
	@TotalDiscriminado money=NULL ,
		
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion int,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
	
		--Comprobante Proveedor
		If @IDProveedor = 0 Begin
			if Exists(Select TOP 1 * From ComprobanteLibroIVA Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante And IDProveedor=@IDProveedor AND NroTimbrado = @NroTimbrado) Begin
				set @Mensaje = 'El sistema encontro el mismo tipo y numero de comprobante del mismo proveedor! Asegurese de introducir los datos correctamente. Prov'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
		End
		
		--Comprobante Cliente
		If @IDCliente = 0 Begin
			if Exists(Select * From ComprobanteLibroIVA Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante And IDCliente=@IDCliente AND NroTimbrado = @NroTimbrado) Begin
				set @Mensaje = 'El sistema encontro el mismo tipo y numero de comprobante del mismo cliente! Asegurese de introducir los datos correctamente.'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
		End
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		

		--Insertar el Registro de Comprobante	
		Insert Into ComprobanteLibroIVA(IDTransaccion,NroComprobante,Fecha,Detalle,IDMoneda,Cotizacion,Total, TotalImpuesto, TotalDiscriminado, IDTipoComprobante,NroTimbrado,FechaVencimientoTimbrado,IDProveedor,IDCliente,Credito,Numero,IDSucursal,IDOperacion,Directo)
								Values(@IDTransaccionSalida,@NroComprobante,@Fecha,@Detalle,@IDMoneda,@Cotizacion,@Total, @TotalImpuesto, @TotalDiscriminado,@IDTipoComprobante,@NroTimbrado,cast(@FechaVencimientoTimbrado as date),@IDProveedor,@IDCliente,@Credito,@Numero,@IDSucursalOperacion,@IDOperacionComprobante,@Directo )
								
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From ComprobanteLibroIVA  Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que el documento no tenga otros registros asociados
		
		--Actualizar el proveedor
		Declare @vIDProveedor int
		Declare @vUltimaCompra date
		
		Set @vIDProveedor = (Select IDProveedor From ComprobanteLibroIVA  Where IDTransaccion=@IDTransaccion)
		
		--Eliminamos el DetalleImpuesto
		Delete DetalleImpuesto Where IDTransaccion = @IDTransaccion
		
			
		--Eliminamos el registro
		Delete ComprobanteLibroIVA  Where IDTransaccion = @IDTransaccion
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion
							
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	If @Operacion = 'UPD' Begin
								
			--Validar que los Asientos no esten conciliados
			If Exists(Select * from Asiento Where Conciliado='True' And IDTransaccion=@IDTransaccion)Begin 
				set @Mensaje = 'El asiento ya esta conciliado!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
				
			--Eliminamos los asientos
			Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
			Delete Asiento Where IDTransaccion = @IDTransaccion
				
			--Eliminamos el DetalleImpuesto
			Delete DetalleImpuesto Where IDTransaccion = @IDTransaccion
	
			--Actualizar Datos
			Update ComprobanteLibroIVA  Set 
						IDTipoComprobante=@IDTipoComprobante,
						NroComprobante=@NroComprobante, 
						Fecha=@Fecha,	
						Detalle=@Detalle,    
						IDProveedor=@IDProveedor,
						IDCliente=@IDCliente,  	
						IDSucursal=@IDSucursalOperacion, 	
						Credito=@Credito,
						IDMoneda=@IDMoneda,
						Cotizacion=@Cotizacion,
						NroTimbrado=@NroTimbrado,
						FechaVencimientoTimbrado=@FechaVencimientoTimbrado, 
						IDOperacion=@IDOperacionComprobante,
						Directo=@Directo,
						Total=@Total,
						TotalImpuesto=@TotalImpuesto,
						TotalDiscriminado=@TotalDiscriminado
			Where IDTransaccion=@IDTransaccion     
					 	
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
	
		
End







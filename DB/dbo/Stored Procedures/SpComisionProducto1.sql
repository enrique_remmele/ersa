﻿
CREATE Procedure [dbo].[SpComisionProducto1]

	--Entrada
	@IDProducto int,
	@IDListaVendedor int,
	@PorcentajeBase decimal (5,3)= 0,
	@PorcentajeEsteProducto decimal (5,3)= 0,
	
	
	
	@Operacion varchar(10),
	
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	Declare @PorcentajeAnterior decimal (5,3)
	
	
	--INSERTAR
	If @Operacion='INS' Begin
	
		--Verificar que los ID´s ya no esten cargados
		If Exists(Select * From ComisionProducto Where IDProducto=@IDProducto And IDVendedor=@IDListaVendedor ) Begin
			set @Mensaje = 'El producto y la lista de vendedor ya estan cargados! Seleccione una lista de vendedor diferencte.'
			set @Procesado = 'True'
			return @@rowcount
		End
		
		--Insertar
		
		Insert Into ComisionProducto (IDProducto, IDVendedor, IDSucursal, PorcentajeBase, PorcentajeEsteProducto, PorcentajeAnterior, IDUsuario)
		values(@IDProducto, @IDListaVendedor, @IDSucursal, @PorcentajeBase, @PorcentajeEsteProducto, @PorcentajeAnterior, @IDUsuario)
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
	End
	
	--ACTUALIZAR
	If @Operacion='UPD' Begin
	
		--Verificar que los ID´s ya no esten cargados
		If Not Exists(Select * From ComisionProducto Where IDProducto=@IDProducto And IDVendedor=@IDListaVendedor ) Begin
			set @Mensaje = 'El sistema no encontro el registro seleccionado! Favor de verificar la informacion y continuar.'
			set @Procesado = 'True'
			return @@rowcount
		End
		
		
		Set @PorcentajeAnterior =(Select IsNull(PorcentajeBase, 0) From ComisionProducto Where IDProducto=@IDProducto And IDVendedor=@IDListaVendedor ) 
		
		
		--Actualizar
		Update ComisionProducto Set PorcentajeBase = @PorcentajeBase  ,
									PorcentajeEsteProducto = @PorcentajeEsteProducto,
									PorcentajeAnterior = @PorcentajeAnterior ,
									FechaUltimoCambio = GETDATE (),
									IDUsuario= @IDUsuario 
		Where IDProducto=@IDProducto And IDVendedor=@IDListaVendedor  And IDSucursal=@IDSucursal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
	End
	
	--ELIMINAR
	If @Operacion='DEL' Begin

		--Verificar que los ID´s ya no esten cargados
		If Not Exists(Select * From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@IDListaVendedor ) Begin
			set @Mensaje = 'El sistema no encontro el registro seleccionado! Favor de verificar la informacion y continuar.'
			set @Procesado = 'True'
			return @@rowcount
		End
		
		--Eliminar
		Delete From ProductoListaPrecio
		Where IDProducto=@IDProducto And IDListaPrecio=@IDListaVendedor  And IDSucursal=@IDSucursal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount	
	End
		

	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount
		
End







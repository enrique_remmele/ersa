﻿CREATE procedure [dbo].[SpImportRegionUser]

@IDSucursal int
As

Begin

	Select 
	Top 1
	'CodRegion'='REGION'+'_'+Sucursal,
	'CodRegionType'='TERRI',
	'DsType'=Sucursal,
	'CodRegionStatus'=(Case When Estado=1 Then 'ACT' Else 'DESACT' End),
	'CodRegionParent'='',
	'CodUser'=convert(varchar(10),''),
	'nmFirstName'='',
	'nmLastName'='',
	'nrPhone1'='',
	'nrPhone2'='',
	'Email'='',
	'CodUserStatus'=''
	From VVendedor  Where IDSucursal=@IDSucursal

	Union All

	Select 
	'CodRegion'='R'+Sucursal+'_'+'VENDEDOR_'+Convert(varchar(50),ID),
	'CodRegionType'='ZONE',
	'DsType'=Sucursal+'_'+'VENDEDOR_'+Convert(varchar(50),ID),
	'CodRegionStatus'=(Case When Estado=1 Then 'ACT' Else 'DESACT' End),
	'CodRegionParent'='REGION'+'_'+Sucursal,
	'CodUser'=ID,
	'nmFirstName'=Nombres,
	'nmLastName'=Apellidos,
	'nrPhone1'=Telefono,
	'nrPhone2'=Celular,
	'Email'=Email,
	'CodUserStatus'=(Case When Estado=1 Then 'ACT' Else 'DESACT' End)
	From VVendedor  Where IDSucursal=@IDSucursal

End



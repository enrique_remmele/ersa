﻿
CREATE Procedure [dbo].[SpAgregarComprobantesAnular]

	--Entrada
	@IDPuntoExpedicion int = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante numeric(18, 0) = NULL,
	@Comprobante varchar(50) = NULL,
	@IDCliente int = NULL,
    @Fecha date = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDDepositoOperacion tinyint = NULL,
	@DescripcionAnulacion varchar (200)= NULL,
	@Observacion varchar(200) = NULL,
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
As

Begin
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select FechaEmision from Venta where Comprobante=@Comprobante and IDPuntoExpedicion = @IDPuntoExpedicion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		If Exists(Select * From Venta Where Comprobante=@Comprobante and Procesado = 1) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Comprobante
		select * from venta where NroComprobante = '135359'
		if Exists(Select * From Venta Where IDPuntoExpedicion=@IDPuntoExpedicion And NroComprobante=@NroComprobante and Procesado = 1) Begin
			set @Mensaje = 'El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Comprobante fuera del Rango
		if @NroComprobante < (Select NumeracionDesde From PuntoExpedicion Where ID=@IDPuntoExpedicion) Or @NroComprobante > (Select NumeracionHasta From PuntoExpedicion Where ID=@IDPuntoExpedicion) Begin
			set @Mensaje = 'El nro de comprobante no esta dentro del rango!Cambie el punto de expedicion o actualicelo.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Agregar Comprobante anulado
		
		Declare @vIDMotivoAnulacionVenta int
		
		Begin
		
			If Exists(Select Descripcion  From MotivoAnulacionVenta Where Descripcion= @DescripcionAnulacion ) Begin
				Set @vIDMotivoAnulacionVenta = (Select ID From MotivoAnulacionVenta Where Descripcion= @DescripcionAnulacion)
			End Else Begin
				
				Set @vIDMotivoAnulacionVenta = (Select ISNULL(Max(ID) + 1, 1) From MotivoAnulacionVenta)
				Insert Into MotivoAnulacionVenta(ID,Descripcion )
				Values(@vIDMotivoAnulacionVenta, @DescripcionAnulacion )
			End
			
		End
		
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--Insertar el Registro de Venta		
		Insert Into Venta(IDTransaccion, IDPuntoExpedicion, IDTipoComprobante, NroComprobante, Comprobante, IDCliente, FechaEmision, IDSucursal, IDDeposito, Credito,  Observacion, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, Cobrado, Descontado, Saldo, Cancelado, Despachado, EsVentaSucursal, Anulado, FechaAnulado, IDUsuarioAnulado, Procesado, IDMotivo)
		Values(@IDTransaccionSalida, @IDPuntoExpedicion, @IDTipoComprobante, @NroComprobante, @Comprobante, @IDCliente, @Fecha, @IDSucursalOperacion, @IDDepositoOperacion, 'False', @Observacion, 0, 0, 0, 0, 0, 0, 0, 'True', 'False', 'False', 'True', NULL, NULL, 'True', @vIDMotivoAnulacionVenta)
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccionSalida, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		--Actualizar Punto de Expedicion
		IF (Select UltimoNumero from PuntoExpedicion where ID = @IDPuntoExpedicion) = (@NroComprobante - 1) Begin
			Update PuntoExpedicion Set UltimoNumero=@NroComprobante
			Where ID=@IDPuntoExpedicion
		end

		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			      
	End
	
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	
	return @@rowcount
		
End








﻿CREATE procedure [dbo].[SpImportStock]
	@IDSucursal int,
	@Desde date,
	@Hasta date
as
begin

	Select
	'cdProduct'=VEMC.Referencia,
	'cdWarehouse'=VEMC.IDDeposito,
	'vlBeginningMonthAmount'=VEMC.CostoPromedio,
	'vlBeginnignMonthQuantity'=(Existencia - SUM(Compras))+SUM(Ventas),
	'vlReceivedQuantity'=SUM(Entradas)+SUM(Compras),
	'vlDispatchedQuantity'=SUM(Salidas)+SUM(Ventas),
	'dtStock'=CONVERT(varchar(5),Year(Fecha)) + '-' + CONVERT(varchar(5), dbo.FFormatoDosDigitos( Month(Fecha))) + '-' + CONVERT(varchar(5), dbo.FFormatoDosDigitos( Day(Fecha))),
	'cdStatus'=(Case When VEMC.Estado=1 Then 'ACT' Else 'DEACT'End)
	From VExistenciaMovimientoCalculado VEMC
	JOIN dbo.VExistenciaDeposito VED ON VEMC.IDSucursal = VED.IDSucursal AND VEMC.IDDeposito = VED.IDDeposito AND VEMC.IDProducto = VED.IDProducto
	Where VEMC.IDSucursal=@IDSucursal and Fecha Between @Desde And @Hasta
	group by VEMC.Referencia,VEMC.IDDeposito,VEMC.CostoPromedio,Existencia,Fecha,VEMC.Estado

End


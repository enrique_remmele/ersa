﻿
CREATE procedure [dbo].[spCambiarVendedor]
@IdVendedorActual tinyint,
@IdVendedorNuevo tinyint,
@FechaDesde Datetime,
@FechaHasta Datetime,
@ActualizarVenta bit,
@Mensaje varchar(100) output,
@Procesado varchar(5) output
as
begin
	if not exists(Select * From Vendedor Where ID =@IdVendedorActual) begin
			set @Mensaje = 'El Vendedor Actual no existe!'
			set @Procesado = 'False'
			return @@rowcount
	end;
	
	if not exists(Select * From Vendedor Where ID =@IdVendedorNuevo) begin
			set @Mensaje = 'El Vendedor Nuevo no existe!'
			set @Procesado = 'False'
			return @@rowcount
	end;
	
  
  
 --Actualiza el vendedor asignado al cliente
 update Cliente
 set IDVendedor = @IdVendedorNuevo
 where IDVendedor = @IdVendedorActual;
 
 if @ActualizarVenta = 'true' begin
   update Venta
   set IDVendedor = @IdVendedorNuevo
   where IDVendedor = @IdVendedorActual
   and FechaEmision between @FechaDesde
   and @FechaHasta;
 end;
 
 set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
end










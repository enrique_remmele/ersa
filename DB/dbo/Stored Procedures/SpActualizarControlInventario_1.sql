﻿CREATE Procedure [dbo].[SpActualizarControlInventario]

	--Entrada 
	@IDTransaccion numeric(18,0)

As

Begin

	Declare @vEquipo1 decimal(10,2) 
	Declare @vAveriados1 decimal(10,2)
	Declare @vEquipo2 decimal(10,2)
	Declare @vAveriados2 decimal(10,2)
	Declare @vAveriadosDiferencia decimal(10,2) 
	Declare @vConteoDiferencia decimal(10,2)
	Declare @vTotalDiferencia decimal(10,2)
	
	Declare @vTotalFisico decimal(10,2)
	Declare @vExistencia decimal(10,2)
	Declare @vDiferenciaSistema decimal(10,2)
		
	Declare @vIDDeposito int
	Declare @vIDProducto int
	
	Declare @vIDEquipo1 int
	Declare @vIDEquipo2 int
	
	Declare db_cursor cursor for
	Select IDDeposito, IDProducto, Existencia From ExistenciaDepositoInventario Where IDTransaccion=@IDTransaccion
	Open db_cursor   
	Fetch Next From db_cursor Into @vIDDeposito, @vIDProducto, @vExistencia
	While @@FETCH_STATUS = 0 Begin  
	
		set @vIDEquipo1 = (Select Top 1 IDEquipo From ExistenciaDepositoInventarioEquipo EDE Join EquipoConteo EC On EDE.IDEquipo=EC.ID Where EDE.IDTransaccion=@IDTransaccion And EC.EquipoConteo = 1)
		set @vIDEquipo2 = (Select Top 1 IDEquipo From ExistenciaDepositoInventarioEquipo EDE Join EquipoConteo EC On EDE.IDEquipo=EC.ID Where EDE.IDTransaccion=@IDTransaccion And EC.EquipoConteo = 2 )
		
		set @vEquipo1 = (Select IsNull((Select Conteo From ExistenciaDepositoInventarioEquipo Where IDTransaccion=@IDTransaccion And IDDeposito=@vIDDeposito And IDProducto=@vIDProducto And IDEquipo=@vIDEquipo1),0))
		set @vEquipo2 = (Select IsNull((Select Conteo From ExistenciaDepositoInventarioEquipo Where IDTransaccion=@IDTransaccion And IDDeposito=@vIDDeposito And IDProducto=@vIDProducto And IDEquipo=@vIDEquipo2),0))
		
		set @vAveriados1 = (Select IsNull((Select Averiados From ExistenciaDepositoInventarioEquipo Where IDTransaccion=@IDTransaccion And IDDeposito=@vIDDeposito And IDProducto=@vIDProducto And IDEquipo=@vIDEquipo1),0))
		set @vAveriados2 = (Select IsNull((Select Averiados From ExistenciaDepositoInventarioEquipo Where IDTransaccion=@IDTransaccion And IDDeposito=@vIDDeposito And IDProducto=@vIDProducto And IDEquipo=@vIDEquipo2),0))
		
		Set @vAveriadosDiferencia = @vAveriados1 - @vAveriados2
		If @vAveriadosDiferencia < 0 Begin
			Set @vAveriadosDiferencia = @vAveriadosDiferencia * -1
		End
		
		Set @vConteoDiferencia = @vEquipo1 - @vEquipo2
		If @vConteoDiferencia < 0 Begin
			Set @vConteoDiferencia = @vConteoDiferencia * -1
		End
		
		Set @vTotalDiferencia = @vConteoDiferencia - @vAveriadosDiferencia
		If @vTotalDiferencia < 0 Begin
			Set @vTotalDiferencia = @vTotalDiferencia * -1
		End
		
		If @vEquipo1 > 0 or @vEquipo2 > 0  Begin  
			Print '-----------'
			Print @vIDProducto
			Print @vEquipo1
			Print @vAveriados1
			Print @vEquipo2
			Print @vAveriados2
		End
		
		--Diferencia contra el sitema
		Set @vTotalFisico = @vEquipo1 + @vAveriados1
		Set @vDiferenciaSistema = @vTotalFisico - @vExistencia
		
		Update ExistenciaDepositoInventario Set Equipo1=@vEquipo1, 
												Averiados1=@vAveriados1, 
												Equipo2=@vEquipo2, 
												Averiados2=@vAveriados2, 
												AveriadosDiferencia=@vAveriadosDiferencia, 
												ConteoDiferencia=@vConteoDiferencia, 
												TotalDiferencia=@vTotalDiferencia,
												DiferenciaSistema=@vDiferenciaSistema												
		Where IDTransaccion = @IDTransaccion And IDDeposito=@vIDDeposito And IDProducto=@vIDProducto
	
		Fetch Next From db_cursor Into @vIDDeposito, @vIDProducto, @vExistencia
			
	End
	
	Close db_cursor   
	Deallocate db_cursor		   			
		
	

End


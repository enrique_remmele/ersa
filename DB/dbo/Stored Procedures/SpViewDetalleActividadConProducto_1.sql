﻿CREATE Procedure [dbo].[SpViewDetalleActividadConProducto]	
	--Identificadores
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	@IDProducto int	
As

Begin

	Declare @vFecha date
	Set @vFecha = convert(date, (SELECT TOP 1 VentaFechaFacturacion FROM dbo.Configuraciones Where IDSucursal=@IDSucursal))
	
	Select 
	DA.IDActividad,
	DA.ID,
	DA.IDProducto,
	
	'Producto'=P.Descripcion,
	D.*,
	--Al saldo restamos los pedidos pendientes de facturacion
	'SaldoReal'=D.Saldo - IsNull((Select Sum(DP.Total) 
							From VDescuentoPedido DP 
							Join Pedido PE On DP.IDTransaccion=PE.IDTransaccion 
							JOin TipoDescuento TD On DP.IDTipoDescuento=TD.ID
							Where PE.Facturado='False' 
							And TD.PlanillaTactico='True'
							And PE.Anulado='False'
							And DP.IDProducto=P.ID
							And PE.IDSucursal=@IDSucursal
							And PE.FechaFacturar>=@vFecha),0),
	PLA.Desde,
	PLA.Hasta
	
	From DetalleActividad DA
	Join Producto P On DA.IDProducto=P.ID
	Join VDetallePlanillaDescuentoTactico D On DA.IDActividad=D.ID
	Join VPlanillaDescuentoTactico PLA On D.IDTransaccion=PLA.IDTransaccion
	
	Where D.IDSucursal=@IDSucursal 
	And @vFecha Between PLA.Desde And PLA.Hasta
	AND PLA.Anulado = 0
	And DA.IDProducto=@IDProducto
	
	Order By DA.IDActividad, DA.ID	
	
End



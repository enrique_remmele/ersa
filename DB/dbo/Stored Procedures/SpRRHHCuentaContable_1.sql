﻿CREATE Procedure [dbo].[SpRRHHCuentaContable]

	--Entrada
	@IDCuentaContable int,
	@Estado bit='True',
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From RRHHCuentaContable Where IDCuentaContable = @IDCuentaContable) begin
			set @Mensaje = 'La Cuenta Contable ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end


		--Insertamos
		Insert Into RRHHCuentaContable(IDCuentaContable, Estado)
		Values(@IDCuentaContable,@Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='RRHHCUENTACONTABLE', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
				 		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From RRHHCuentaContable Where IDCuentaContable = @IDCuentaContable) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
				
		--Actualizamos
		Update RRHHCuentaContable Set	Estado =@Estado
		Where  IdCuentaContable = @IDCuentaContable
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='RRHHCUENTACONTABLE', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		If not exists(Select * From RRHHCuentaContable Where IDCuentaContable = @IDCuentaContable) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From RRHHCuentaContable 
		Where IdCuentaContable = @IDCuentaContable
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='RRHHCUENTACONTABLE', @IDUsuario=@IDUsuario
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


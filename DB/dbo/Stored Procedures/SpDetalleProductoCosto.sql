﻿CREATE Procedure [dbo].[SpDetalleProductoCosto]

	--Entrada
	@IDTransaccion numeric(18,0),
	@IDProducto int = NULL,
	@Observacion varchar(50) = NULL,
	@Costo money = NULL,
	@Operacion varchar(20),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin

	--Validar
	--Transaccion
	If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End
	
	--Transaccion
	If Not Exists(Select IDTransaccion From ProductoCosto Where IDTransaccion=@IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra la transaccion!'
		Set @Procesado = 'False'
		return @@rowcount
	End


	--Variables Kardex
	Declare @vFecha datetime
	Declare @vIDSucursal integer
	Declare @vNroComprobante varchar(50)
	Declare @vCostoEntrada decimal(10,2)
	Declare @vTotalDiscriminado decimal(10,2)
	Declare @CostoProduccion bit
	Declare @vFechaHasta datetime

	Set @vFecha = (Select Fecha From ProductoCosto Where IDTransaccion = @IDTransaccion)
	Set @vFechaHasta = isnull((Select Top(1) Fecha From VDetalleProductoCosto where Fecha > @vFecha and IDProducto = @IDProducto), DateAdd(year,1000,@vFecha))
	Set @vIDSucursal = 1
	Set @vNroComprobante = (Select Numero From ProductoCosto Where IDTransaccion = @IDTransaccion)
	Set @CostoProduccion = (Select isnull(CostoProduccion,'False') from ProductoCosto where idtransaccion = @IDTransaccion)
	Set @vCostoEntrada = @Costo
	
	--BLOQUES
	if @Operacion='INS' Begin
		
		Declare @vCostoAnterior money
		
		Set @vCostoAnterior = dbo.FCostoProducto(@IDProducto)
		
				--Insertar el detalle
		Insert Into DetalleProductoCosto(IDTransaccion, IDProducto, Observacion, Costo, CostoAnterior, Anulado)
								  Values(@IDTransaccion, @IDProducto, @Observacion, @Costo, @vCostoAnterior, 'False')
		
		--Actualizar el costo
		if @CostoProduccion = 'False' begin

			Declare @vConfiguracionCosto varchar(50)
	
			--Obtenemos la configuracion del Costo
			Set @vConfiguracionCosto = (Select IsNull((Select Top(1) MovimientoCostoProducto From Configuraciones), 'PROMEDIO'))
		
			If @vConfiguracionCosto = 'ULTIMO' Begin
				Update Producto Set CostoSinIVA=@Costo 
				Where ID=@IDProducto
			End
		
			If @vConfiguracionCosto = 'PROMEDIO' Begin
				Update Producto Set CostoPromedio=@Costo 
				Where ID=@IDProducto
			End

			EXEC SpKardex
				@Fecha = @vFecha,
				@IDTransaccion = @IDTransaccion,
				@IDProducto = @IDProducto,
				@ID = 1,
				@Orden = 1,
				@CantidadEntrada = 0,
				@CantidadSalida = 0,
				@CostoEntrada = @Costo,
				@CostoSalida = 0,
				@IDSucursal = @vIDSucursal,
				@Comprobante = @vNroComprobante	
		end
		else begin
			Update Kardex set CostoUnitarioEntrada = @vCostoEntrada,
								TotalEntrada = CantidadEntrada * @vCostoEntrada
								where idproducto = @IDProducto 
								and Fecha between @vFecha and @vFechaHasta
			and idtransaccion in (Select distinct IDTransaccion 
									from Movimiento 
										where Fecha between @vFecha and @vFechaHasta
										and IDTipoComprobante = (select top(1) IDTipoComprobanteProduccion from Configuraciones)
										)

			
		--	Exec SpRecalcularKardex @IDProducto, @vFecha, 'PRODUCTO'
			
		end
		
		Set @Procesado = 'True'
		return @@rowcount					
			
	End
	
	if @Operacion='DEL' Begin
		
		--Validar
		--Transaccion
		If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra la transaccion!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--Transaccion
		If Not Exists(Select IDTransaccion From ProductoCosto Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El sistema no encuentra la transaccion!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	if @Operacion='ANULAR' Begin
		
		--Verificar que exista
		If Not Exists(Select * From DetalleProductoCosto Where IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = 'El Registro ya esta anulado!'
			Set @Procesado = 'True'
			return @@rowcount
		End
		
		Update DetalleProductoCosto Set Anulado='True'
		Where IDProducto=@IDProducto
		and idtransaccion = @IDTransaccion
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	return @@rowcount
	
End




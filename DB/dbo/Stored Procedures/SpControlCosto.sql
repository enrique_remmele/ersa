﻿CREATE Procedure [dbo].[SpControlCosto]

	
	--Entrada
	@Numero VARCHAR(50) = NULL,
	@Fecha date = NULL,
	@Observacion varchar(200) = NULL,
	@Operacion varchar(50),

	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--INSERTAR
	If @Operacion = 'INS' Begin
	
			
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		------Insertar
		Insert Into aControlCosto(IDTransaccion, Numero, Observacion)
		Values(@IDTransaccionSalida, @Numero, @Observacion)
				
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End


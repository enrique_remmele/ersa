﻿CREATE Procedure [dbo].[SpTipoProveedorImportar]

	--Entrada
	@Referencia varchar(100),
	@Descripcion varchar(100),
	
	@Actualizar bit = 'True'
	
As

Begin

	Declare @vID int
	
	--Si existe
	If Exists(Select * From TipoProveedor Where Descripcion=@Descripcion) Begin
		Update TipoProveedor Set Descripcion=@Descripcion, Referencia=@Referencia
		Where Descripcion=@Descripcion
	End
	
	--Si existe
	If Not Exists(Select * From TipoProveedor Where Descripcion=@Descripcion) Begin
		If @Actualizar = 'True' Begin
			Set @vID = (Select IsNull(Max(ID)+1,1) From TipoProveedor)
			Insert Into TipoProveedor(ID, Descripcion, Estado, Referencia)
			Values(@vID, @Descripcion,  'True', @Referencia)
		End
	End
	
End



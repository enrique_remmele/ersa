﻿
CREATE Procedure [dbo].[SpProducto]

	--Entrada
	--Datos Obligatorios
	@ID int,
	@Descripcion varchar(50),
	@Operacion varchar(10),
	
	--Identificadores
	@CodigoBarra varchar(30) = NULL,
	@Referencia varchar(30) = NULL,
			
	--Agrupadores
	@IDClasificacion smallint = NULL,
	@IDTipoProducto tinyint = NULL,
	@IDLinea smallint = NULL,
	@IDSubLinea smallint = NULL,
	@IDSubLinea2 smallint = NULL,
	@IDMarca tinyint = NULL,
	@IDPresentacion smallint = NULL,
	@IDCategoria tinyint = NULL,
	@IDProveedor int = NULL,
	@IDDivision smallint = NULL,
	@IDProcedencia tinyint = NULL,
	
	--Unidades de Medida
	@IDUnidadMedida tinyint = NULL,
	@UnidadPorCaja smallint = NULL,
	@IDUnidadMedidaConvertir tinyint = NULL,
	@UnidadConvertir varchar(50) = NULL,
	@Peso varchar(50) = NULL,
	@Volumen varchar(15) = NULL,

	--@FactorCosto DECIMAL(10,4) = 1,
		
	--Configuraciones
	@MargenMinimo decimal(5,2) = 0,
	@IDImpuesto tinyint = 1,
	@Estado bit = 'True',
	@ControlarExistencia bit = 'True',
	@CuentaContableCompra varchar(50) = NULL,
	@CuentaContableVenta varchar(50) = NULL,
	@CuentaContableCosto varchar(50) = NULL,
	@CuentaContableDeudor varchar(50) = NULL,
	@Vendible bit = 'False',

	--Opciones de descarga
	@ConsumoCombustible bit = 'False',
	@MateriaPrima bit = 'false',
	@DescargaCompra bit = 'False',

	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As
	
Begin

	--BLOQUES
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	
	Set @UnidadConvertir = REPLACE(@UnidadConvertir, '.','')
	Set @UnidadConvertir = REPLACE(@UnidadConvertir, ',','.')
	
	If ISNUMERIC(@UnidadConvertir) = 0 Begin
		Set @UnidadConvertir = '0'
	End
	
	--INSERTAR
	If @Operacion='INS' Begin
	
		--Variables
		Declare @vEnEspera bit
		
		--Validar
		--Referencia
		If @Referencia='' Begin
			Set @Mensaje = 'La Referencia no puede estar vacio!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		If Exists(Select TOP 1 * From Producto Where Referencia=@Referencia And Estado='False') Begin
			Set @Mensaje = 'El Codigo ya existe y esta Inactivo! INS'
			Set @Procesado = 'False'
			return @@rowcount
		End

		If Exists(Select TOP 1 * From Producto Where Referencia=@Referencia And Estado='True') Begin
			Set @Mensaje = 'El Codigo ya existe! INS'
			Set @Procesado = 'False'
			return @@rowcount
		End

		if @IDTipoProducto is null Begin
			Set @Mensaje = 'Seleccione Tipo de Producto!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--Codigo de Barra
		If @CodigoBarra != '' Begin
			If Exists(Select TOP 1 * From Producto Where CodigoBarra=@CodigoBarra And Estado='True') Begin
				Set @Mensaje = 'El codigo de barra ya existe! INS'
				Set @Procesado = 'False'
				return @@rowcount
			End
		End
				
		--If isnull(@CuentaContableCompra,'') = '' Begin
		--	Set @Estado = 'False'
		--	print 'Estado false'
		--End
		
		--If isnull(@CuentaContableCosto,'') = '' Begin
		--	Set @Estado = 'False'
		--	print 'Estado false'
		--End
		
		--Descripcion
		--If Exists(Select * From Producto Where Descripcion=@Descripcion And Estado='True') Begin
		--	Set @Mensaje = 'La Descripción ya existe! INS'
		--	Set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		--Codigo de Barra
		--Si ya existe el codigo, poner en espera e inactivar.
		If Exists(Select TOP 1 * From Producto Where CodigoBarra=@CodigoBarra And CodigoBarra!='') Begin
			Set @vEnEspera = 'True'
			Set @Estado='False'					
		End
		Else Begin
			Set @vEnEspera = 'False'					
		End
	
		--Insertar	
		Declare @vID int
		Set @vID = (Select ISNULL(Max(ID)+1, 1) From Producto)
		
		Insert Into Producto(ID, Descripcion, CodigoBarra, Referencia, IDImpuesto,Estado,ControlarExistencia,ExistenciaGeneral,EnEspera, Reemplazado,Vendible, ConsumoCombustible, MateriaPrima, DescargaCompra)
		values(@vID, @Descripcion, @CodigoBarra, @Referencia, @IDImpuesto, @Estado, @ControlarExistencia, 0, @vEnEspera, 'False',@Vendible, @ConsumoCombustible, @MateriaPrima, @DescargaCompra)
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PRODUCTO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		--Actualizamos el registro
		Set @Operacion='UPD'
		Set @ID = @vID		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		
	End
	
	--ACTUALIZAR
	If @Operacion='UPD' Begin
	
		--Validar
		--Referencia
		If @Referencia='' Begin
			Set @Mensaje = 'La Referencia no puede estar vacio'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		If Exists(Select TOP 1 * From Producto Where Referencia=@Referencia And ID!=@ID And Estado='True') Begin
			Set @Mensaje = 'La Referencia ya existe! UPD'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--Codigo de Barar
		If @CodigoBarra != '' Begin
			If Exists(Select TOP 1 * From Producto Where CodigoBarra=@CodigoBarra And ID!=@ID And Estado='True') Begin
				Set @Mensaje = 'El codigo de barra ya existe! UPD'
				Set @Procesado = 'False'
				return @@rowcount
			End
		End
		
		--Descripcion
		--If Exists(Select * From Producto Where Descripcion=@Descripcion And ID!=@ID And Estado='True') Begin
		--	Set @Mensaje = 'La Descripción ya existe! UPD'
		--	Set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		--Codigo de Barrra
		If Exists(Select TOP 1 * From Producto Where ID!=@ID And CodigoBarra=@CodigoBarra And CodigoBarra!='' And ExistenciaGeneral>0 And Estado='True' And Reemplazado='False') Begin
			Set @Mensaje = 'El codigo de barra ya existe!!'
			Set @Procesado = 'False'
			return @@rowcount
		End
		
		--If @CuentaContableCosto Is Null Begin
		--	Set @Mensaje = 'Debe especificar la cuenta del costo!'
		--	Set @Procesado = 'False'
		--	return @@rowcount
		--End
		--If isnull(@CuentaContableCompra,'') = '' Begin
		--	Set @Estado = 'False'
		--	print 'Estado false'
		--End
		
		--If isnull(@CuentaContableCosto,'') = '' Begin
		--	Set @Estado = 'False'
		--	print 'Estado false'
		--End
		print concat('1 ',@Estado	)
		--No se puede actualizar si es que el producto tiene stock
		If @Estado = 'False' Begin
			
			If (Select SUM(Existencia) From ExistenciaDeposito Where IDProducto=@ID) > 0 Begin
				Set @Mensaje = 'El producto tiene stock! No se puede inactivar'
				Set @Procesado = 'False'
				print @Mensaje
				return @@rowcount
			End
			
		End
		print concat('2',@Estado	)
		--Actualizamos
		--Identificadores
		Update Producto Set Descripcion=@Descripcion,
							Referencia=@Referencia,
							CodigoBarra=@CodigoBarra
		Where ID=@ID
		print concat('3',@Estado	)
		--Agrupadores
		Update Producto Set IDClasificacion=@IDClasificacion,
							IDTipoProducto=@IDTipoProducto,
							IDLinea=@IDLinea,
							IDSubLinea=@IDSubLinea,
							IDSubLinea2=@IDSubLinea2,
							IDMarca=@IDMarca,
							IDPresentacion=@IDPresentacion,
							IDCategoria=@IDCategoria,
							IDProveedor=@IDProveedor,
							IDDivision=@IDDivision,
							IDProcedencia=@IDProcedencia,
							Vendible = @Vendible,
							ConsumoCombustible = @ConsumoCombustible,
							MateriaPrima = @MateriaPrima,
							DescargaCompra = @DescargaCompra
		Where ID=@ID
					
		--Unidad de Medida
		--Si @UnidadPorCaja es NULL o es 0, poner 1
		if @UnidadPorCaja = null or @UnidadPorCaja= 0 begin
			set @UnidadPorCaja = 1
		End 
		print concat('4',@Estado	)
		Update Producto Set IDUnidadMedida=@IDUnidadMedida,
							UnidadPorCaja=@UnidadPorCaja,
							IdunidadMedidaConvertir=@IDUnidadMedidaConvertir ,
							UnidadConvertir=@UnidadConvertir ,
							Peso=@Peso,
							Volumen=@Volumen
		Where ID=@ID
		print concat('5',@Estado	)
		--Configuraciones
		Update Producto Set	MargenMinimo=@MargenMinimo,
							IDImpuesto=@IDImpuesto,
							Estado=@Estado,
							ControlarExistencia=@ControlarExistencia,
							CuentaContableCompra=@CuentaContableCompra,
							CuentaContableVenta=@CuentaContableVenta,
							CuentaContableCosto=@CuentaContableCosto,
							CuentaContableDeudor=@CuentaContableDeudor
							--FactorCosto = @FactorCosto
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PRODUCTO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
					
	End
	
	--ELIMNAR
	If @Operacion='DEL' Begin
	
		--Si es que se encuentra en detalle venta
		If Exists(Select * From DetalleVenta Where IDProducto=@ID) Begin
			Set @Mensaje = 'El producto ya tiene ventas realizadas! No se puede eliminar.'
			Set @Procesado = 'False'
			return @@rowcount	
		End
		
		--Eliminar Producto y Lista de Precios
		Delete From ProductoListaPrecio Where IDProducto=@ID
		
		--Eliminar de Existencia Deposito
		Delete From ExistenciaDeposito Where IDProducto=@ID
		
		--Eliminar de Producto Lote
		Delete From ProductoLote Where IDProducto=@ID
		
		--Eliminamos de Producto
		Delete From Producto Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='PRODUCTO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		
	End
	
	return @@rowcount
	
End


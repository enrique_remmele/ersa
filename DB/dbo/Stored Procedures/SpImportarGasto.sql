﻿--SELECT * FROM PROVEEDOR WHERE RAZONSOCIAL LIKE '%JUAN JORGE%'
CREATE Procedure [dbo].[SpImportarGasto]
	--Cabecera
	@TipoComprobante varchar(10),
	@Comprobante varchar(50),
	@Referencia varchar(50),
	@Proveedor varchar(50) = '',
	@RUC varchar(50)= '',
	@Fecha date,
	@Credito varchar(50),
	@FechaVencimiento date,
	@Sucursal varchar(50) = '001',
	@ReferenciaSucursal varchar(50) = 1,
	@ReferenciaTerminal varchar(50) = 1,
	
	@Moneda varchar(50),		
	@Cotizacion money,
	@Observacion varchar(200),
	
	--Timbrado
	@NroTimbrado varchar(50)=NULL,
	@FechaVencimientoTimbrado varchar(10)=NULL,
	
	--Totales
	@Total money = 0,
	@TotalImpuesto money = 0,
	@Saldo money = 0,
	
	--Impuesto
	@Exento as money = 0,
	@Gravado10 as money = 0,
	@IVA10 as money = 0,
	@Gravado5 as money = 0,
	@IVA5 as money = 0,
			
	--Datos Extras
	@RetencionIVA as money = 0,
	
	--Transaccion
	@IDOperacion smallint = 1,
	@IDUsuario smallint = 1,
	@IDSucursal tinyint = 1,
	@IDDeposito tinyint = 1,
	@IDTerminal int = 1,
		
	@Actualizar bit
	
		
As

Begin
		
	--Variables
	Begin
		
		Declare @vMensaje varchar(100)
		Declare @vProcesado bit
		
		Declare @vCredito bit
		Declare @vCancelado bit
		Declare @vIDMoneda int
		Declare @vOperacion varchar(50)
		Declare @vIDTransaccion int
		Declare @vComprobante varchar(50)

		--Codigos
		Declare @vNumero int
		Declare @vIDTipoComprobante int
		Declare @vIDProveedor int
		Declare @vIDSucursal int
		Declare @vCajaChica int
					
	End
	
	--Establecer valores predefinidos
	Begin
		
		Set @vOperacion = 'INS'
		Set @vIDMoneda = 1
			
		Set @vCredito = 'False'
		If DateDiff(dd, @Fecha, @FechaVencimiento) = 0 Begin
			Set @vCredito = 'True'
		End
		
		Set @vCancelado = 'False'
		If @Saldo = 0 Begin
			Set @vCancelado = 'True'
		End	
			
	End
	
	
	--Hayar Valores
	Begin
		
		Set @vMensaje = 'No se procesó!'
		Set @vProcesado = 'False'
		
		--validar
		If IsDate(@FechaVencimientoTimbrado) = 0 Begin
			Set @vMensaje = 'La fecha de vencimiento del timbrado no es correcto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Comprobante anulado
		If CharIndex('_', @Comprobante) > 0 Begin
			Set @vMensaje = 'El comprobante no es correcto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End

		If IsDate(@FechaVencimientoTimbrado) = 0 Begin
			Set @vMensaje = 'La fecha de vencimiento del timbrado no es correcto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Tipo de Comprobante
		Set @vIDTipoComprobante = (IsNull((Select Top(1) ID From TipoComprobante Where Codigo=@TipoComprobante And IDOperacion=@IDOperacion), 0))
		--Insertamos el tipo de comprobante si no existe
		If @vIDTipoComprobante = 0 Begin
			Set @vIDTipoComprobante = IsNull((Select Max(ID) + 1 From TipoComprobante), 1)
			
			Insert into TipoComprobante(ID, Cliente, Proveedor, IDOperacion, Descripcion, Codigo, Resumen, Estado, ComprobanteTimbrado, CalcularIVA, IVAIncluido, LibroVenta, LibroCompra, Signo, Autonumerico, Deposito, Restar, HechaukaTipoDocumento, HechaukaTimbradoReemplazar, HechaukaNumeroTimbrado, EsInterno)
			Values(@vIDTipoComprobante, 'False', 'True', @IDOperacion, @TipoComprobante, @TipoComprobante, @TipoComprobante, 1, 1, 0, 1, 0, 1, '+', 0, 0, 0, 0, 0, '', 0)
			
		End
		
		--Proveedor 
		Set @vIDProveedor=(IsNull((Select Top(1) ID From Proveedor Where Referencia=@Referencia),0))
		--Insertamos si no existe
		If @vIDProveedor = 0 Begin
			Set @vIDProveedor = IsNull((Select Max(ID) + 1 From Proveedor), 1)
			Insert Into Proveedor(ID, Referencia, RazonSocial, RUC, Estado, Credito, Retentor, SujetoRetencion, Exportador)
			Values(@vIDProveedor, @Referencia, @Proveedor, @RUC, 1, 1, 0, 1, 0)			
		End 
		
		--Actualizamos el proveedor
		
		--Sucursal 
		Set @vIDSucursal = (IsNull((Select Top(1) ID From Sucursal Where Codigo=@Sucursal),0))
		If @vIDSucursal = 0 Begin
			Set @vMensaje = 'Sucursal incorrecto!'
			Set @vProcesado = 'False'
			GoTo Salir
		End
		
		--Moneda--select * from moneda
		Set @vIDMoneda = ISNULL((Select Top(1) ID From Moneda Where Descripcion=@Moneda), 0)
		--Insertamos
		If @vIDMoneda = 0 Begin
			Set @vIDMoneda = IsNull((Select Max(ID) + 1 From Moneda), 1)
			Insert Into Moneda(ID, Descripcion, Referencia, Estado, Decimales, Divide, Multiplica)
			Values(@vIDMoneda, @Moneda, @Moneda, 1, 0, 0, 1)
		End
		
		Set @vComprobante = '0' + dbo.FFormatoDosDigitos(@ReferenciaSucursal) + '-' + '0' + dbo.FFormatoDosDigitos(@ReferenciaTerminal) + '-' + @Comprobante 		
	
	End
	
	--Verificar si ya existe
	If Exists(Select * From GastoImportado GI Join Gasto G On GI.IDTransaccion=G.IDTransaccion Join TipoComprobante TC On G.IDTipoComprobante=TC.ID Where G.NroComprobante=@vComprobante And G.IDProveedor=@vIDProveedor And G.NroTimbrado=@NroTimbrado And TC.Codigo=@TipoComprobante) Begin
		set @vIDTransaccion = (Select Top(1) GI.IDTransaccion From GastoImportado GI Join Gasto G On GI.IDTransaccion=G.IDTransaccion Join TipoComprobante TC On G.IDTipoComprobante=TC.ID Where G.NroComprobante=@vComprobante And G.IDProveedor=@vIDProveedor And G.NroTimbrado=@NroTimbrado And TC.Codigo=@TipoComprobante)
		Set @vOperacion = 'UPD'
	End
	
	--ACTUALIZAR
	If @vOperacion = 'UPD' Begin
	
		If @Actualizar = 'True' Begin
			
			--Gasto
			Update Gasto Set	NroComprobante=@vComprobante,
								Credito=@vCredito,
								FechaVencimiento=@FechaVencimiento,
								Total=@Total,
								TotalImpuesto=@TotalImpuesto,
								TotalDiscriminado=@Total - @TotalImpuesto,
								Saldo=@Saldo,
								Cancelado=@vCancelado,
								IDMoneda=@vIDMoneda,
								Cotizacion=@Cotizacion
			Where IDTransaccion = @vIDTransaccion
			
			--Actualizar Impuesto
			--10%
			Update DetalleImpuesto Set  Total = @Gravado10, 
										TotalImpuesto = @IVA10, 
										TotalDescuento = 0, 
										TotalDiscriminado = @Gravado10 - (@IVA10 + 0)
			Where IDTransaccion = @vIDTransaccion And IDImpuesto = 1
			
			--5%
			Update DetalleImpuesto Set  Total = @Gravado5, 
										TotalImpuesto = @IVA5, 
										TotalDescuento = 0, 
										TotalDiscriminado = @Gravado5 - (@IVA5 + 0)
			Where IDTransaccion = @vIDTransaccion And IDImpuesto = 2
			
			--10%
			Update DetalleImpuesto Set  Total = @Exento, 
										TotalImpuesto = 0, 
										TotalDescuento = 0, 
										TotalDiscriminado = @Exento - 0
			Where IDTransaccion = @vIDTransaccion And IDImpuesto = 3

			Set @vMensaje = 'Actualizado!'
			Set @vProcesado = 'True'
			GoTo OrdenPago
		End
		
		Set @vMensaje = 'Sin Act.!'
		Set @vProcesado = 'True'
		GoTo OrdenPago
		
	End
	
	--INSERTAR
	If @vOperacion = 'INS' Begin
		
		--Validar Informacion
		Begin
		
			--Comprobante
			if Exists(Select * From Gasto Where NroComprobante=@vComprobante And IDProveedor=@vIDProveedor) Begin
				set @vMensaje = 'El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
				set @vProcesado = 'False'
				GoTo Salir
			End
			
		End
		
		----Insertar la transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@MostrarTransaccion = 'False',
			@Mensaje = @vMensaje OUTPUT,
			@Procesado = @vProcesado OUTPUT,
			@IDTransaccion = @vIDTransaccion OUTPUT
		
		If @vProcesado = 'False' Begin
			GoTo Salir
		End
		
		Set @vNumero = ISNULL((Select MAX(Numero)+1 From Gasto Where IDSucursal=@vIDSucursal),1)	
		
		--Insertar en Gasto
		Insert Into Gasto(IDTransaccion, Numero, IDTipoComprobante, NroComprobante,  NroTimbrado, IDProveedor, IDSucursal, Fecha,  Credito, FechaVencimiento, FechaVencimientoTimbrado, IDMoneda, Cotizacion,  Observacion, Total, TotalImpuesto, TotalDiscriminado, Saldo,  Cancelado, Directo, RetencionIVA, RetencionRenta, CajaChica, IncluirLibro)
		Values(@vIDTransaccion, @vNumero, @vIDTipoComprobante, @vComprobante,  @NroTimbrado, @vIDProveedor, @vIDSucursal, @Fecha,  @vCredito, @FechaVencimiento, @FechaVencimientoTimbrado, @vIDMoneda, @Cotizacion,  @Observacion, @Total, @TotalImpuesto, @Total - @TotalImpuesto, @Saldo,  @vCancelado, 'True', @RetencionIVA, 0,'False', 'True')
		
		--Insertar en GastoTipoComprobante
		Insert into GastoTipoComprobante(IDTransaccion,Numero,IDSucursal)
		values(@vIDTransaccion,@vNumero,@vIDSucursal)
		
		--Insertar en GastoImportacion
		Insert Into GastoImportado(IDTransaccion, NroOperacion)
		Values(@vIDTransaccion, 0)
		
		--Insertar Impuesto
		--10%
		Insert into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado)
		Values(@vIDTransaccion, 1, @Gravado10 + @IVA10, @IVA10, 0, @Gravado10)
		
		--5%
		Insert into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado)
		Values(@vIDTransaccion, 2, @Gravado5 + @IVA5, @IVA5, 0, @Gravado5)
		
		--Exento
		Insert into DetalleImpuesto(IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado)
		Values(@vIDTransaccion, 3, @Exento, 0, 0, @Exento)
		
		Set @vMensaje = 'Registro guardado!'
		Set @vProcesado = 'True'
		GoTo OrdenPago
				
	End

OrdenPago:
	
	--Solo si es a cuenta el pago
	If @Total = @Saldo Begin
		GoTo Salir
	End
	
	--Orden de Pago	
	--If Not Exists(Select * From OrdenPago Where IDProveedor=@vIDProveedor And NroComprobante=@Comprobante) Begin
		
	--	Declare @vIDTransaccionOP as numeric(18,0)		
	--	Declare @vIDOperacionOrdenPago int
	--	Declare @vIDTipoComprobanteOrdenPago int
	--	Declare @vIDCuentaBancaria int
	--	Declare @vImporteOP money
		
	--	Set @vIDOperacionOrdenPago = (Select Top(1) ID From Operacion Where FormName='frmOrdenPago')
	--	Set @vIDTipoComprobanteOrdenPago = (Select Top(1) ID From TipoComprobante Where IDOperacion=@vIDOperacionOrdenPago)
	--	Set @vImporteOP = @Total - @Saldo
	--	Set @vNumero = IsNull((Select MAX(Numero) + 1 From OrdenPago) , 1)
		
	--	--Insertar Transaccion
	--	EXEC SpTransaccion
	--		@IDUsuario = @IDUsuario,
	--		@IDSucursal = @IDSucursal,
	--		@IDDeposito = @IDDeposito,
	--		@IDTerminal = @IDTerminal,
	--		@IDOperacion = @IDOperacion,
	--		@MostrarTransaccion = 'False',
	--		@Mensaje = @vMensaje OUTPUT,
	--		@Procesado = @vProcesado OUTPUT,
	--		@IDTransaccion = @vIDTransaccionOP OUTPUT
			
	--	--Insertar en Orden de Pago
	--	Insert Into OrdenPago(IDTransaccion, Numero, IDTipoComprobante, NroComprobante, IDSucursal, Fecha, IDProveedor, Observacion, Cotizacion, Total, Cancelado, Anulado, FechaAnulado, IDUsuarioAnulado, Conciliado,PagoProveedor, AnticipoProveedor, EgresoRendir, AplicarRetencion)
	--	Values(@vIDTransaccionOP, @vNumero, @vIDTipoComprobanteOrdenPago, @Comprobante, @vIDSucursal, @Fecha, @vIDProveedor, @Observacion, @Cotizacion, @vImporteOP, 'False', 'False', NULL, NULL, 'False', 'True', 'False', 'False', 'False')
		
	--	--Cheque
	--	Set @vNumero = IsNull((Select MAX(Numero) + 1 From Cheque) , 1)
	--	Set @vIDCuentaBancaria = (Select Top(1) ID From CuentaBancaria)
		
	--	Insert Into Cheque(IDTransaccion, IDSucursal, Numero, IDCuentaBancaria, NroCheque, Fecha, FechaPago, Cotizacion, ImporteMoneda, Importe, Diferido, FechaVencimiento, ALaOrden, Conciliado, Anulado )
	--	Values(@vIDTransaccionOP, @vIDSucursal, @vNumero, @vIDCuentaBancaria, '', @Fecha, @Fecha, @Cotizacion, @vImporteOP, @vImporteOP, 'False', @FechaVencimiento, '---', 'False', 'False')
		
	--	--Insertar en la relacion 
	--	Insert into OrdenPagoEgreso(IDTransaccionEgreso, IDTransaccionOrdenPago, Importe, Procesado, Saldo)
	--	Values(@vIDTransaccion, @vIDTransaccionOP, @vImporteOP, 'True', 0)
				
	--End
		
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
End


﻿CREATE Procedure [dbo].[SpHabilitarLimiteCredito]
	
	--Entrada
	@Usuario varchar(50),
	@Password varchar(300),
	
	@IDPerfil int,
	@NombreFormulario varchar(50),
	@NombreFuncion varchar(50),
		
	--Auditoria
	@IDTerminal smallint = NULL
			
As

Begin

	Declare @vIDUsuario int
	Declare @vIDPerfil int
	Declare @vMensaje varchar(150)
	Declare @vProcesado bit
	
	Set @vMensaje = ''
	Set @vProcesado = 'False'
	Set @vIDUsuario = 0
	
	--Convertir Password
	Set @Password = Convert(varchar(300), HASHBYTES('Sha1', @Password))

	--Verificar credenciales
	If  Not Exists(Select * From Usuario where Usuario=@Usuario And [Password]=@Password) Begin
		Set @vMensaje = 'Credenciales no validas!'
		Set @vProcesado = 'False'
		GoTo Salir
	End
	
	--Seleccionamos el usuario
	Set @vIDUsuario = (Select Top(1) ID From Usuario where Usuario=@Usuario And Password=@Password)
	Set @vIDPerfil = (Select Top(1) IDPerfil From Usuario where ID=@vIDUsuario)
	
	--Que el usuario este activo
	If (Select Estado From Usuario where ID=@vIDUsuario) = 'False' Begin
		Set @vMensaje = 'Usuario inabilitado!'
		Set @vProcesado = 'False'
		GoTo Salir
	End
	
	--Verificar que el perfil del usuario tenga permiso
	
	--Obtener el IDMenu	
	Declare @vIDMenu int
	Set @vIDMenu = (Select IsNull((Select Top(1) Codigo From Menu Where Tag=@NombreFormulario), -1))
	
	If @vIDMenu = -1 Begin
		Set @vMensaje = 'No se encuentra el formulario!'
		Set @vProcesado = 'False'
		GoTo Salir
	End
	
	--Verificar que exista la configuracion
	Declare @vIDAccesoEspecifico int
	Set @vIDAccesoEspecifico = (Select IsNull((Select Top(1) ID From AccesoEspecifico Where IDMenu=@vIDMenu And NombreFuncion=@NombreFuncion), -1))
	
	If @vIDAccesoEspecifico = -1 Begin
		Set @vMensaje = 'No se encuentra el la configuracion!'
		Set @vProcesado = 'False'
		GoTo Salir
	End
	
	Set @vProcesado = (Select IsNull((Select Habilitar From AccesoEspecificoPerfil Where IDPerfil=@vIDPerfil And IDAccesoEspecifico=@vIDAccesoEspecifico), 'False'))
	
	If @vProcesado = 'False' Begin
		Set @vMensaje = 'Acceso denegado!'
		GoTo Salir
	End
	
	Set @vMensaje = 'Acceso correcto!'
	Set @vProcesado = 'True'
				
	--Auditoria
	Exec SpLogSuceso @Operacion='INS', @Tabla='HABILITARLIMITECREDITO', @IDUsuario=@vIDUsuario, @IDTerminal=@IDTerminal
		
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	
End
	
	


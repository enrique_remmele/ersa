﻿
CREATE Procedure [dbo].[SpPedidoNotaCreditoProcesar]

	@IDTransaccion numeric(18,0),
	@Operacion varchar(10)
	
As

Begin

	Declare @vProcesado bit
	Declare @vMensaje varchar(50)

	Declare @vIDProducto int
	Declare @vIDCliente int
	Declare @vIDListaPrecio int
	Declare @vCantidad decimal(18,3)
	Declare @vFecha date
	
	
	Set @vProcesado = 'True'
	Set @vMensaje = 'OK'

	--Si no se guardo en la BD	
	If Not Exists(Select * From PedidoNotaCredito Where IDTransaccion=@IDTransaccion) Begin
		Set @vProcesado = 'False'
		Set @vMensaje = 'El Pedido de Nota de Credito no existe!'
		GoTo Salir
	End
	if @Operacion = 'INS' begin
		--actualizar la cantidad de la excepcion
		if (select DesdePedidoVenta from PedidoNotaCredito where idtransaccion = @IDTransaccion) = 1 begin
		
			--Actualizar los productos
			Declare db_cursorProducto cursor for
			Select DV.Cantidad, 
			DV.IDProducto, 
			NC.IDCliente, 
			V.IDListaPrecio,
			NC.Fecha
			From DetalleNotaCreditoDiferenciaPrecio DV
			join PedidoNotaCredito NC on DV.IDTransaccionPedidoNotaCredito = NC.IDTransaccion
			join PedidoNotaCreditoPedidoVenta NCV on NCV.IDTransaccionPedidoNotaCredito = NC.IDTransaccion
			join Pedido V on NCV.IDTransaccionPedidoVenta = V.IDTransaccion
			Where DV.IDTransaccionPedidoNotaCredito=@IDTransaccion
		
			Open db_cursorProducto   
			Fetch Next From db_cursorProducto Into @vCantidad, @vIDProducto, @vIDCliente, @vIDListaPrecio, @vFecha
			While @@FETCH_STATUS = 0 Begin  
		
				--Si tiene excepcion con limite de cantidad, restar el saldo
				Update ProductoListaPrecioExcepciones 
				Set CantidadLimiteSaldo = Isnull(CantidadLimiteSaldo,0) + @vCantidad
				Where IDProducto = @vIDProducto 
				and IDListaPrecio = @vIDListaPrecio 
				and IDCliente = @vIDCliente
				and (@vFecha Between Desde and Hasta) 
				and Isnull(CantidadLimite,0)>0 
				and IDTipoDescuento = 3
			

			Fetch Next From db_cursorProducto Into @vCantidad, @vIDProducto, @vIDCliente, @vIDListaPrecio, @vFecha
			
			End
		
			Close db_cursorProducto   
			Deallocate db_cursorProducto	
		end

	end

	If @Operacion = 'ANULAR' begin
		--actualizar la cantidad de la excepcion
		if (select DesdePedidoVenta from PedidoNotaCredito where idtransaccion = @IDTransaccion) = 1 begin
		
			--Actualizar los productos
			Declare db_cursorProducto cursor for
			Select DV.Cantidad, 
			DV.IDProducto, 
			NC.IDCliente, 
			V.IDListaPrecio,
			NC.Fecha
			From DetalleNotaCreditoDiferenciaPrecio DV
			join PedidoNotaCredito NC on DV.IDTransaccionPedidoNotaCredito = NC.IDTransaccion
			join PedidoNotaCreditoPedidoVenta NCV on NCV.IDTransaccionPedidoNotaCredito = NC.IDTransaccion
			join Pedido V on NCV.IDTransaccionPedidoVenta = V.IDTransaccion
			Where DV.IDTransaccionPedidoNotaCredito=@IDTransaccion
		
			Open db_cursorProducto   
			Fetch Next From db_cursorProducto Into @vCantidad, @vIDProducto, @vIDCliente, @vIDListaPrecio, @vFecha
			While @@FETCH_STATUS = 0 Begin  
		
				--Si tiene excepcion con limite de cantidad, restar el saldo
				Update ProductoListaPrecioExcepciones 
				Set CantidadLimiteSaldo = Isnull(CantidadLimiteSaldo,0) - @vCantidad
				Where IDProducto = @vIDProducto 
				and IDListaPrecio = @vIDListaPrecio 
				and IDCliente = @vIDCliente
				and (@vFecha Between Desde and Hasta) 
				and Isnull(CantidadLimite,0)>0 
				and IDTipoDescuento = 3
			

			Fetch Next From db_cursorProducto Into @vCantidad, @vIDProducto, @vIDCliente, @vIDListaPrecio, @vFecha
			
			End
		
			Close db_cursorProducto   
			Deallocate db_cursorProducto	
		end

	end


	Set @vProcesado = 'True'
	Set @vMensaje = 'OK'
	
	
Salir:

	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado
	
End



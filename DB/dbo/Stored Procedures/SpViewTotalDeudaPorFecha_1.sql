﻿CREATE Procedure [dbo].[SpViewTotalDeudaPorFecha]
	
	--Entrada
	
	@Hasta date,
	@IDBanco int = 0,
	@TipoGarantia varchar(50) = '',
	@IDMoneda int = 0

As
Begin
		
		
		--Crear la tabla temporal
		create table #TablaTemporal
								(IDTransaccion int,
								Numero int,
								NroComprobante varchar(50),
								FechaPrestamo Date,
								IDMoneda int,
								Moneda varchar(15),
								IDCuentaBancaria int,
								CuentaBancaria varchar(50),
								IDBanco int,
								Banco varchar(100),
								Cotizacion decimal(18,2),
								Observacion varchar(500),
								PlazoDias int,
								PorcentajeInteres decimal(18,2),
								Capital decimal(18,3),
								Interes decimal(18,3),
								ImpuestoIntereses decimal(18,3),
								Gastos decimal(18,3),
								Neto decimal(18,3),
								TipoGarantia varchar(50),
								SaldoAFechaAmortizacion decimal(18,3),
								SaldoAFechaInteres decimal(18,3),
								SaldoAFechaImpuesto decimal(18,3)								
								)
								
 
	
		--Declarar variables 
		declare @vIDTransaccion int
		declare @vNumero int
		declare @vNroComprobante varchar(50)
		declare @vFechaPrestamo Date
		declare @vIDMoneda int
		declare @vMoneda varchar(15)
		declare @vIDCuentaBancaria int
		declare @vCuentaBancaria varchar(50)
		declare @vIDBanco int
		declare @vBanco varchar(100)
		declare @vCotizacion decimal(18,2)
		declare @vObservacion varchar(500)
		declare @vPlazoDias int
		declare @vPorcentajeInteres decimal(18,2)
		declare @vCapital decimal(18,3)
		declare @vInteres decimal(18,3)
		declare @vImpuestoIntereses decimal(18,3)
		declare @vGastos decimal(18,3)
		declare @vNeto decimal(18,3)
		declare @vTipoGarantia varchar(50)
		declare @vSaldoAFechaAmortizacion decimal(18,3)
		declare @vSaldoAFechaInteres decimal(18,3)
		declare @vSaldoAFechaImpuesto decimal(18,3)	
		declare @vTotalPagadoAFechaAmortizacion decimal(18,3)
		declare @vTotalPagadoAFechaInteres decimal(18,3)
		declare @vTotalPagadoAFechaImpuesto decimal(18,3)
		
		
		Declare db_cursor cursor for
		
		Select
		IDTransaccion,
		Numero,
		NroComprobante,
		Fecha,
		IDMoneda,
		Moneda,
		IDCuentaBancaria,
		CuentaBancaria,
		IDBanco,
		Banco,
		Cotizacion,
		Observacion,
		PlazoDias,
		PorcentajeInteres,
		Capital,
		Interes,
		ImpuestoInteres,
		Gastos,
		Neto,
		TipoGarantia
		From vPrestamoBancario
		Where Fecha <= @Hasta and Anulado=0
		and (Case When @IDBanco = 0 then 0 else IDBanco end) = (Case When @IDBanco = 0 then 0 else @IDBanco end)
		and (Case When @TipoGarantia = '' then '' else TipoGarantia end) = (Case When @TipoGarantia = '' then '' else @TipoGarantia end)
		and (Case When @IDMoneda = 0 then 0 else IDMoneda end) = (Case When @IDMoneda = 0 then 0 else @IDMoneda end)
					
		Open db_cursor   
		
		Fetch Next From db_cursor Into @vIDTransaccion,@vNumero,@vNroComprobante,@vFechaPrestamo,@vIDMoneda,@vMoneda,@vIDCuentaBancaria,@vCuentaBancaria,@vIDBanco,@vBanco,@vCotizacion,@vObservacion,@vPlazoDias,@vPorcentajeInteres,@vCapital,@vInteres,@vImpuestoIntereses,@vGastos,@vNeto,@vTipoGarantia
		
		While @@FETCH_STATUS = 0 Begin
		
			
			Select @vTotalPagadoAFechaAmortizacion = Sum(AmortizacionCapital), 
					@vTotalPagadoAFechaInteres=Sum(Interes),
					@vTotalPagadoAFechaImpuesto = Sum(Impuesto)
			from DetalleCuotaPrestamoBancario 
			where IDTransaccionPrestamoBancario = @vIDTransaccion
			and Fecha <= @Hasta
			Group by IDTransaccionPrestamoBancario

			Set @vSaldoAFechaAmortizacion = @vCapital - @vTotalPagadoAFechaAmortizacion
			Set @vSaldoAFechaInteres = @vInteres - @vTotalPagadoAFechaInteres
			Set @vSaldoAFechaImpuesto = @vImpuestoIntereses - @vTotalPagadoAFechaImpuesto

			Insert into #TablaTemporal(IDTransaccion,Numero,NroComprobante,FechaPrestamo,IDMoneda,Moneda,IDCuentaBancaria,CuentaBancaria,IDBanco,Banco,Cotizacion,Observacion,PlazoDias,PorcentajeInteres,Capital,Interes,ImpuestoIntereses,Gastos,Neto,TipoGarantia,SaldoAFechaAmortizacion,SaldoAFechaInteres,SaldoAFechaImpuesto) 
			Values (@vIDTransaccion,@vNumero,@vNroComprobante,@vFechaPrestamo,@vIDMoneda,@vMoneda,@vIDCuentaBancaria,@vCuentaBancaria,@vIDBanco,@vBanco,@vCotizacion,@vObservacion,@vPlazoDias,@vPorcentajeInteres,@vCapital,@vInteres,@vImpuestoIntereses,@vGastos,@vNeto,@vTipoGarantia,@vSaldoAFechaAmortizacion,@vSaldoAFechaInteres,@vSaldoAFechaImpuesto) 
	
			Fetch Next From db_cursor Into  @vIDTransaccion,@vNumero,@vNroComprobante,@vFechaPrestamo,@vIDMoneda,@vMoneda,@vIDCuentaBancaria,@vCuentaBancaria,@vIDBanco,@vBanco,@vCotizacion,@vObservacion,@vPlazoDias,@vPorcentajeInteres,@vCapital,@vInteres,@vImpuestoIntereses,@vGastos,@vNeto,@vTipoGarantia
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		

		Select * From #TablaTemporal 
	
	
	
End

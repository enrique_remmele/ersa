﻿CREATE Procedure [dbo].[SpRedondearAsientoNotaCreditoERSA]

	@IDTransaccion numeric(18,0),
	@Credito bit = 'True'
	
		
As

Begin
	
	SET NOCOUNT ON
	
	--Redondear a 0
	Begin
	
		Declare @vID tinyint 
		Declare @vCuentaContable varchar(50) 
		Declare @vCredito money 
		Declare @vDebito money
		Declare @vImporte money
		
		Declare db_cursor cursor for
		Select ID, CuentaContable, Credito, Debito, Importe From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vID, @vCuentaContable, @vCredito, @vDebito, @vImporte 
		While @@FETCH_STATUS = 0 Begin 
		
			If @vCredito > 0 Begin
				Set @vCredito = ROUND(@vCredito, 0)
			End
			
			If @vDebito > 0 Begin
				Set @vDebito = ROUND(@vDebito, 0)
			End
			
			Set @vImporte = @vDebito + @vCredito
			
			--Actualizar El detalle
			Update DetalleAsiento Set Credito = @vCredito, 
									  Debito = @vDebito,
									  Importe = @vImporte
			
			Where IDTransaccion=@IDTransaccion And ID=@vID And CuentaContable=@vCuentaContable
		
			Fetch Next From db_cursor Into @vID, @vCuentaContable, @vCredito, @vDebito, @vImporte 
										
		End	
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
		--Actualizamos la cabecera, el total
		Declare @vImporteDebe money
		Declare @vImporteHaber money
		Declare @vSaldo money
		Declare @vTotalDebito money
		Declare @vTotalCredito money
		
		Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vSaldo = @vImporteHaber - @vImporteDebe
		Set @vTotalCredito = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		Set @vTotalDebito = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
		
		Update Asiento Set Total = @vImporteHaber,
							Credito = @vImporteHaber,
							Debito = @vImporteDebe,
							Saldo = @vSaldo
		Where IDTransaccion=@IDTransaccion
		print @vsaldo
		--Reparar
		If @vSaldo != 0 Begin
			if @vSaldo < 0 begin
				set @vSaldo = @vSaldo * (-1)
			End

			--If @vSaldo > @vSaldoMinimo or @vSaldo < @vSaldoMaximo Begin
				declare @vIDDetalle int = (select isnull(max(ID),0)+1 from DetalleAsiento where IDTransaccion = @IDTransaccion)
				Declare @vCuenta varchar(50)
			
				--Hayamos la cuenta a afectar
				Set @vCuenta = '4120201'
			
				--Si es mayor a 0, Restar el Credito en Ventas
				If @vTotalCredito < @vTotalDebito Begin
					insert into DetalleAsiento(IDTransaccion,ID, IDCuentaContable, CuentaContable, Credito, Debito, IMporte, Observacion)
					values(@IDTransaccion,@vIDDetalle,1426,@vCuenta, @vSaldo,0,@vSaldo,'AJUSTE EN NC')
				End

				--Si es menor a 0, Restar el Credito en Ventas
				If @vTotalCredito > @vTotalDebito Begin
					insert into DetalleAsiento(IDTransaccion,ID, IDCuentaContable, CuentaContable, Credito, Debito, IMporte, Observacion)
					values(@IDTransaccion,@vIDDetalle,1426,@vCuenta, 0,@vSaldo,@vSaldo,'AJUSTE EN NC')
				End
		End
	End
	
	
Salir:
	
End


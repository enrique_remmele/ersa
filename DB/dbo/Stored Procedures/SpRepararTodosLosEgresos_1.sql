﻿CREATE Procedure [dbo].[SpRepararTodosLosEgresos]
	
As

Begin
	
	Declare @vProcesado as bit = 'False'
	--Validar
	--Si existe
	If  Not Exists(Select * From VEgreso Where Saldo >TOtal) Begin
		Set @vProcesado = 'True'
		GoTo Salir
	End

	Declare @vIDTransaccion int 
	Declare db_cursorRepararEgresos cursor for
	Select IDTransaccion
	From vEgreso 
	Where Saldo>Total
	Open db_cursorRepararEgresos   
	Fetch Next From db_cursorRepararEgresos Into @vIDTransaccion
	While @@FETCH_STATUS = 0 Begin 
	
		exec SpRepararEgreso @vIDTransaccion
		
		Fetch Next From db_cursorRepararEgresos Into @vIDTransaccion
									
	End
	
	--Cierra el cursor
	Close db_cursorRepararEgresos   
	Deallocate db_cursorRepararEgresos

	Set @vProcesado = 'True'
		
Salir:
	Select 'Procesado'=@vProcesado
End


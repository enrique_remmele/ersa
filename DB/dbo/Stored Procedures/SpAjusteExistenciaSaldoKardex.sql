﻿CREATE Procedure [dbo].[SpAjusteExistenciaSaldoKardex]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Numero VARCHAR(50) = NULL,
	@Fecha date = NULL,
	@Observacion varchar(200) = NULL,
	@Operacion varchar(50),

	--Transaccion
	@IDOperacion smallint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--BLOQUES
	SET @Numero = REPLACE(@Numero,'.','')
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	

	--Obtenemos el nuevo ID
		declare @vNumero tinyint
		set @vNumero = (Select IsNull((Max(Numero)+1), 1) From AjusteExistenciaSaldoKardex)

		--Validar
		If Exists(Select * From vAjusteExistenciaSaldoKardex Where Numero=@vNumero AND IDSucursal = @IDSucursal) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		------Insertar
		Insert Into AjusteExistenciaSaldoKardex(IDTransaccion, Numero, Fecha, Observacion, IDUsuario,Anulado)
									Values(@IDTransaccionSalida, @vNumero, @Fecha, @Observacion, @IDUsuario, 'False')
				
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From AjusteExistenciaSaldoKardex Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que no este anulado
		if (Select Anulado From AjusteExistenciaSaldoKardex Where IDTransaccion=@IDTransaccion) = 'True' Begin
			set @Mensaje = 'El registro ya esta anulado!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		--Anulamos el registro
		Update AjusteExistenciaSaldoKardex 
		Set Anulado = 'True'
		,FechaAnulado=GetDate()
		,IDUsuarioAnulado = @IDUsuario
		Where IDTransaccion = @IDTransaccion

		Delete from Kardex where IDTransaccion=@IDTransaccion
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End	
		
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End

﻿CREATE Procedure [dbo].[SpActualizarNroCheque]
	
	--Entrada
	@IDTransaccion numeric(18,0),
	@NroCheque varchar(50),
	@NroCuentaBancaria varchar(50),
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	

As

Begin
	--Variables
	declare @IdChequera tinyint
	declare @NroChequeAUtilizar int
	declare @NroHasta int

	Set @Mensaje = 'No se proceso'
	Set @Procesado = 'False'
	
	--Validar
	--Existe
	If Not Exists(Select * From Cheque Where IDTransaccion = @IDTransaccion) Begin
		Set @Mensaje = 'El sistema no encuentra el registro'
		Set @Procesado = 'False'	
		Return @@rowcount
	End
	
	--Si el cheque ya tiene un numero asignado
	If (Select NroCheque From Cheque Where IDTransaccion = @IDTransaccion ) != '' Begin
		Set @Mensaje = 'El cheque ya tiene un numero asignado!'
		Set @Procesado = 'False'	
		Return @@rowcount
	End
	
	--Si ya existe el numero de cheque
	If Exists(Select * From Cheque Where IDTransaccion = @IDTransaccion and NroCheque = @NroCheque) Begin
		Set @Mensaje = 'Otro cheque ya tiene el mismo numero!'
		Set @Procesado = 'False'	
		Return @@rowcount
	End
	
	--Verificar que la cuenta utilizada se encuentre en el listado de chequeras y el numero de cheque corresponda a dicha chequera
	select @IdChequera = ID, @NroChequeAUtilizar = AUtilizar, @NroHasta = NroHasta from VChequera where CuentaBancaria = @NroCuentaBancaria and @NroCheque between NroDesde and NroHasta and Estado = 1

	if @IdChequera is null begin
		set @Mensaje = 'El numero no existe en chequera.'
		set @Procesado = 'False'
		return @@rowcount
	end
	if @NroChequeAUtilizar > @NroCheque begin
		set @Mensaje = CONCAT('El numero de cheque correlativo debe ser: ',@NroChequeAUtilizar)
		set @Procesado = 'False'
		return @@rowcount
	end
	if @NroChequeAUtilizar < @NroCheque begin
		set @Mensaje = CONCAT('Numero de cheque ya utilizado, debe ser: ',@NroChequeAUtilizar)
		set @Procesado = 'False'
		return @@rowcount
	end


	--Actualizar
	Update Cheque Set NroCheque = @NroCheque
	Where IDTransaccion = @IDTransaccion
	
	--Actualizar Chequera
	if @NroChequeAUtilizar = @NroHasta begin
		Update Chequera set Estado = 0 where ID = @IdChequera
		Update Chequera set AUtilizar = 0 where ID = @IdChequera
	end else begin
		Update Chequera set AUtilizar = AUtilizar + 1 where ID = @IdChequera
	end

	Set @Mensaje = 'Registro procesado!'
	Set @Procesado = 'True'	
	Return @@rowcount
	
End


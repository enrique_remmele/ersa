﻿CREATE Procedure [dbo].[SpTipoClienteImportar]

	--Entrada
	@Referencia varchar(100),
	@Descripcion varchar(100),
	
	@Actualizar bit = 'True'
	
As

Begin

	Declare @vID int
	
	--Si existe
	If Exists(Select * From TipoCliente Where Descripcion=@Descripcion) Begin
		Update TipoCliente Set Descripcion=@Descripcion, Referencia=@Referencia
		Where Descripcion=@Descripcion
	End
	
	--Si existe
	If Not Exists(Select * From TipoCliente Where Descripcion=@Descripcion) Begin
		If @Actualizar = 'True' Begin
			Set @vID = (Select IsNull(Max(ID)+1,1) From TipoCliente)
			Insert Into TipoCliente(ID, Descripcion, Estado, Referencia)
			Values(@vID, @Descripcion,  'True', @Referencia)
		End
	End
	
End



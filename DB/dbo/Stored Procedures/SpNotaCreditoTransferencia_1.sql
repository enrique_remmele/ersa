﻿CREATE Procedure [dbo].[SpNotaCreditoTransferencia]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDTransaccionNotaCredito numeric = NULL,
	@Numero int = NULL,	
	@NroComprobante varchar (100)= Null,
	@IDTipoComprobante varchar (50) = NUll,		
	@IDCliente int = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@Fecha date = NULL,
	@IDMoneda int = NULL,
	@Cotizacion money= NULL,
	@Observacion varchar(200) = NULL,
	
			
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Comprobante
		if Exists(Select * From TransferenciaNotaCredito    Where Numero=@Numero And IDSucursal=@IDSucursalOperacion) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		
		
		--Insertar en Transferencia Nota Credito
		Insert Into TransferenciaNotaCredito (IDTransaccion, IDTransaccionNotaCredito, Numero, IDTipoComprobante, NroComprobante,  IDSucursal, IDCliente, Fecha, IDMoneda, Cotizacion, Observacion)
		Values(@IDTransaccionSalida, @IDTransaccionNotaCredito, @Numero, @IDTipoComprobante, @NroComprobante, @IDSucursalOperacion, @IDCliente, @Fecha, @IDMoneda, @Cotizacion, @Observacion)

		----NotaCreditoAplicacionProcesar
		--EXEC SpNotaCreditoAplicacionProcesar @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output  
		
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	
	
	
	--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
		
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		----NotaCreditoAplicacionProcesar
		--EXEC SpNotaCreditoAplicacionProcesar @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output  
		
		--Anular
		Update NotaCreditoAplicacion   Set Anulado='True', IDUsuarioAnulado=@IDUsuario, FechaAnulado = GETDATE()
		Where IDTransaccion = @IDTransaccion
		
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
		
	End
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From TransferenciaNotaCredito       Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		
		--Eliminamos el Detalle
		Delete DetalleTransferenciaNotaCredito     where IDTransaccionTransferenciaNotaCredito  = @IDTransaccion				
				
		--Eliminamos el registro
		Delete TransferenciaNotaCredito     Where IDTransaccion = @IDTransaccion
						
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion				
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End





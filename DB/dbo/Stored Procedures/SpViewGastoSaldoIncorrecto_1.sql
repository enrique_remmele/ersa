﻿
CREATE Procedure [dbo].[SpViewGastoSaldoIncorrecto]

	--Entrada
	@Desde date,
	@Hasta date

As

Begin
	
	--Variables
	Declare @vIDTransaccion numeric(18, 0)
	Declare @vTotal money
	Declare @vSaldo money
	Declare @vPagado money
	Declare @vPagadoSistemaAnterior money
	Declare @vDescontado money
	Declare @vSaldoActual money
	Declare @vRetencionIVA money
	Declare @vCancelado bit
	Declare @vMensaje varchar(200)
	Declare @vValido bit
	Declare @vOP int

	--Tabla temporal
	Create Table #TablaTemporal(Mensaje varchar(200),
								IDTransaccion numeric(18,0),
								Sucursal varchar(10),
								Numero int,
								TipoComprobante varchar(10),
								Comprobante varchar(20),
								IDProveedor int,
								Referencia varchar(15),
								Proveedor varchar(50),
								RUC varchar(20),
								Fecha date,
								Condicion varchar(10),
								Vencimiento date,
								Moneda varchar(10),
								Cotizacion money,
								Observacion varchar(200),
								Total money,
								TotalImpuesto money,
								TotalDiscriminado money,
								RetencionIVA money,
								Saldo money,
								SaldoCalculado money,
								Pagado money,
								Descontado money,
								OP int)


	--Cursor
	Declare db_cursor cursor for
	Select IDTransaccion, Total, RetencionIVA, Saldo, Cancelado From VGasto
	Where Fecha Between @Desde And @Hasta
	Open db_cursor   
	Fetch Next From db_cursor Into @vIDTransaccion, @vTotal, @vRetencionIVA, @vSaldoActual, @vCancelado
	While @@FETCH_STATUS = 0 Begin 
	
		Set @vMensaje = ''
		Set @vValido = 'True'

		Set @vSaldo = (Select [dbo].[FSaldoEgreso](@vIDTransaccion))

		--Cancelado
		If @vCancelado=1 And @vSaldoActual<>0 Begin
			Set @vMensaje = 'Cancelado pero con saldo!'
			Set @vValido = 'False'
			GoTo Insertar
		End

		--Cancelado
		If @vCancelado=0 And @vSaldoActual=0 Begin
			Set @vMensaje = 'Son saldo y cancelado!'
			Set @vValido = 'False'
			GoTo Insertar
		End

		If @vSaldo != @vSaldoActual Begin
			
			--Si el saldo es el importe de la retencion
			If @vSaldo = @vRetencionIVA Begin
				Set @vSaldo = @vSaldo - @vRetencionIVA
			End Else Begin
				Set @vMensaje = 'El saldo no es correcto!'
				Set @vValido = 'False'
				GoTo Insertar
			End

		End

		If @vValido = 'True' Begin
			GoTo Siguiente
		End

Insertar:

		Set @vOP = (Select top(1) OP.Numero 
					From OrdenPagoEgreso OPE 
					Join OrdenPago OP On OPE.IDTransaccionOrdenPago=OP.IDTransaccion
					Where OP.Anulado='False' 
							And OPE.IDTransaccionEgreso=@vIDTransaccion
							And OPE.Procesado='True')

		Insert Into #TablaTemporal
			Select @vMensaje, IDTransaccion, Sucursal, Numero, TipoComprobante, Comprobante, IDProveedor, Referencia, Proveedor, RUC, Fecha, Condicion, FechaVencimiento, Moneda, Cotizacion, Observacion, Total, TotalImpuesto, TotalDiscriminado, RetencionIVA, Saldo, @vSaldo, @vPagado, @vDescontado, @vOP
			From VGasto 
			Where IDTransaccion=@vIDTransaccion

Siguiente:

		Fetch Next From db_cursor Into @vIDTransaccion, @vTotal, @vRetencionIVA, @vSaldoActual, @vCancelado
											
	End
			
	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor
	
	Select * From #TablaTemporal

End

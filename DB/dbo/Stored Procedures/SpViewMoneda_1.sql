﻿CREATE Procedure [dbo].[SpViewMoneda]

	--Entrada
	@Fecha date
	
As

Begin

	--variables
	Declare @vID tinyint
	Declare @Local varchar(16)
	Declare @Moneda varchar(4)
	Declare @Descripcion varchar(50)
	Declare @Referencia varchar(50)
	Declare @Decimales bit
	Declare @Divide bit
	Declare @Estado bit
	Declare @Multiplica bit
	Declare @Cotizacion money
	Declare @FechaCotizacion varchar(16)
	
	--Crear la tabla
	Create table #Moneda(ID tinyint,
						TLocal varchar(16),
						Moneda varchar(4),
						Descripcion varchar(50),
						Referencia varchar(50),
						Decimales bit,
						Divide bit,
						Estado bit,
						Multiplica bit,
						Cotizacion money,
						FechaCotizacion varchar(16)
								)
								

	Declare db_cursor cursor for
	Select 
	M.ID, 
	'Local'=Case When (M.ID) = 1 Then 'True' Else 'False' End,
	'Moneda'=Case When (M.ID) = 1 Then 'LOC' Else 'EXT' End,
	M.Descripcion, 
	M.Referencia, 
	M.Decimales, 
	M.Estado, 
	M.Divide, 
	M.Multiplica,
	'Cotizacion'=isnull(dbo.FCotizacionAlDiaFecha(M.ID,1,@Fecha),1),
	'Fec. Cot.'=dbo.FFechaCotizacionAlDia(M.ID,1,5)
	From Moneda M
	Open db_cursor   
	Fetch Next From db_cursor Into	@vId, @Local,@Moneda,@Descripcion,@Referencia,@Decimales,@Estado,@Divide,@Multiplica,@Cotizacion,@FechaCotizacion
	While @@FETCH_STATUS = 0 Begin 
			Insert Into #Moneda
			Values(@vID,@Local,@Moneda,@Descripcion,@Referencia,@Decimales,@Divide,@estado,@Multiplica,@Cotizacion,@FechaCotizacion)
		
Siguiente:
		
		Fetch Next From db_cursor Into	@vId, @Local,@Moneda,@Descripcion,@Referencia,@Decimales,@Estado,@Divide,@Multiplica,@Cotizacion,@FechaCotizacion
		
	End

	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor		   		
	
	
	Select * From #Moneda 
End





﻿CREATE Procedure [dbo].[SpRegistroCosto]

	--Entrada
	@IDProducto int,
	@Cantidad decimal(10,2),
	@Costo money,
	@PrecioUnitario money,
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	--Variables
	Declare @vCantidad decimal(10,2)
	Declare @vImporte money
	Declare @vPromedio money
	Declare @Cotizacion int
	
	--INSERTAR
	If @Operacion='INS' Begin
		

		--Insertamos el registro si es que no existe todavia
		If Not Exists(Select * From RegistroCosto Where IDProducto=@IDProducto) Begin
			Insert Into RegistroCosto(IDProducto)
			Values(@IDProducto)
		End
		
		--Obtener los nuevos valores
		Set @vCantidad = (Select IsNull((Select Cantidad From RegistroCosto Where IDProducto=@IDProducto), 0))
		Set @vImporte = (Select IsNull((Select Importe From RegistroCosto Where IDProducto=@IDProducto), 0))
													
		Set @vCantidad = @vCantidad + @Cantidad
		Set @vImporte = @vImporte + (@Cantidad * @PrecioUnitario)

		Set @vPromedio = @vImporte / @vCantidad


		--Actualizamos
		Update RegistroCosto Set	UltimaActualizacion = GETDATE(), 
									UltimoCosto = @PrecioUnitario,
									Cantidad = @vCantidad,
									Importe = @vImporte,
									Promedio = @vPromedio
		Where IDProducto=@IDProducto
										
				 												
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		Return @@rowcount
		
	End
	
	--ELIMINAR
	If @Operacion='DEL' Begin
				
		--Si no existe, salir
		If Not Exists(Select * From RegistroCosto Where IDProducto=@IDProducto) Begin
			Set @Mensaje = 'No hay registro!'
			Set @Procesado = 'True'
			Return @@rowcount
		End
				
		Set @vCantidad = IsNull((Select Cantidad From RegistroCosto Where IDProducto=@IDProducto), 0)
		Set @vImporte = IsNull((Select Importe From RegistroCosto Where IDProducto=@IDProducto), 0)

		--Si no existen los valores
		If @vCantidad = 0 Or @vImporte = 0 Begin
			
			--Actualizamos
			Update RegistroCosto 
			Set UltimaActualizacion = GETDATE(), UltimoCosto = 0, Cantidad = 0, Importe = 0, Promedio = 0
			Where IDProducto=@IDProducto
			
			Set @Mensaje = 'Registro guardado!'
			Set @Procesado = 'True'
			Return @@rowcount
			
		End
		 													 
		Set @vCantidad = @vCantidad - @Cantidad
		Set @vImporte = @vImporte - (@Cantidad * @PrecioUnitario)
		
		--- Si la cantidad es 0
		if @vCantidad = 0 Begin
			Set @vpromedio = 0	
		End Else Begin
			Set @vPromedio = @vImporte / @vCantidad
		End	
		
		--Ultimos datos
		Declare @vIDTransaccion numeric(18,0)
		Declare @vUltimaActualizacion datetime
		Declare @vUltimoCosto money
				
		Set @vIDTransaccion = IsNull((Select Max(IDTransaccion) PrecioUnitario From DetalleCompra Where IDProducto=@IDProducto),0)
		set @Cotizacion = (select ISNULL(Cotizacion,1) from Compra where IDTransaccion = @vIDTransaccion)
		Set @vUltimaActualizacion = (Select C.Fecha From Compra C Where C.IDTransaccion=@vIDTransaccion)
				
		Declare @vID tinyint
		Set @vID = (Select Max(ID) From DetalleCompra Where IDTransaccion=@vIDTransaccion And IDProducto=@IDProducto)
		Set @vUltimoCosto = ((Select PrecioUnitario From DetalleCompra Where IDTransaccion=@vIDTransaccion And IDProducto=@IDProducto And ID=@vID) * @Cotizacion)
		
		--Si no existen los valores
		If @vIDTransaccion = 0 Begin
			
			--Actualizamos
			Update RegistroCosto 
			Set UltimaActualizacion = GETDATE(), UltimoCosto = 0, Cantidad = 0, Importe = 0, Promedio = 0
			Where IDProducto=@IDProducto
			
			Set @Mensaje = 'Registro guardado!'
			Set @Procesado = 'True'
			Return @@rowcount
		
		End
		
		--Actualizamos
		Update RegistroCosto Set	UltimaActualizacion = @vUltimaActualizacion, 
									UltimoCosto = @vUltimoCosto,
									Cantidad = @vCantidad,
									Importe = @vImporte,
									Promedio = @vPromedio
		Where IDProducto=@IDProducto
														 												
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		Return @@rowcount
		
	End
	
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	Return @@rowcount
	
End


﻿
CREATE Procedure [dbo].[SpAcuerdo]
	--Entrada
	@Numero int,
	@Fecha date,
	@IDProveedor Int,
	@IDProducto int,
	@NroAcuerdo int,
	@IDMoneda tinyint,
	@Cantidad money,
	@Precio money,
	@TipoFlete varchar(16),
	@Zafra int,
	@Observacion varchar(200),
	@Anulado bit,
	@Finalizado bit = 'False',
	@FechaHastaEmbarque date,
	@FechaDesdeEmbarque date,
	@IDMonedaFlete tinyint,
	@CostoFlete money = 0,
	@Operacion varchar(10),	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Acuerdo Where NroAcuerdo=@NroAcuerdo) begin
			set @Mensaje = 'La Nro de acuerdo ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo Numero
		declare @vNumero int
		set @vNumero = (Select IsNull((Max(Numero)+1), 1) From Acuerdo)

		--Insertamos
		Insert Into Acuerdo(Numero, Fecha, IDProveedor, NroAcuerdo, IDMoneda, IDProducto, Cantidad, Precio, TipoFlete, Zafra,Observacion,Anulado, Finalizado,FechaDesdeEmbarque,FechaHastaEmbarque, IDMonedaFlete, CostoFlete)
		Values(@vNumero,@Fecha,@IDProveedor, @NroAcuerdo, @IDMOneda, @IDProducto, @Cantidad, @Precio, @TipoFlete, @Zafra, @Observacion, @Anulado, @Finalizado,@FechaDesdeEmbarque,@FechaHastaEmbarque, @IDMonedaFlete, @CostoFlete)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='ACUERDO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante =@vNumero
				 		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' or @Operacion='REASIGNAR' begin
		
		--Si el ID existe
		If not exists(Select * From Acuerdo Where Numero=@Numero) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end


		
		--Actualizamos
		Update Acuerdo 
		Set NroAcuerdo = @NroAcuerdo,
			idMoneda=@IDMoneda,
			Cantidad = @Cantidad,
			Anulado =@Anulado,
			Finalizado = @Finalizado,
			FechaDesdeEmbarque = @FechaDesdeEmbarque,
			FechaHastaEmbarque=@FechaHastaEmbarque,
			IDProducto = @IDProducto,
			Zafra = @Zafra,TipoFlete = @TipoFlete,
			Observacion = @Observacion 
		Where Numero = @Numero

				
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='ACUERDO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante =@Numero
		
		if @Operacion = 'REASIGNAR' begin
			
			if exists (Select vTicketBascula.* From vTicketBascula Where NroAcuerdo = @NroAcuerdo and NroAcuerdo >0 and Anulado = 'False' and IDTransaccion in (select IDTransaccionTicket from detallemacheo)) begin
				set @Mensaje = 'El Acuerdo tiene tickets macheados!'
				set @Procesado = 'False'
				return @@rowcount
			end

			if (Select Finalizado From Acuerdo Where Numero = @Numero) = 1 begin
				set @Mensaje = 'El Acuerdo ya esta Finalizado!'
				set @Procesado = 'False'
				return @@rowcount
			end

			
			Update Acuerdo 
			Set Precio = @Precio,
				IDMoneda = @IDMoneda,
				IDMonedaFlete = @IDMonedaFlete,
				CostoFlete = @CostoFlete
			Where Numero = @Numero

				Begin
	
				--Variables
				Declare @vIDTransaccion numeric(18,0)
				Declare @vIDDeposito int
				Declare @vIDSucursal int
				Declare @vIDMoneda int
				Declare @vFactorDiscriminado decimal(10,7)
				Declare @vFactorImpuesto decimal(10,7)
				Declare @vCostoUnitarioAdicional as money
				Declare @vIDProducto int
				Declare @vIDProductoKardex int
				Declare @vPrecioUnitarioKardex int
				Declare @vFechaInicialKardex Date = '20190101'
				Declare @vFecha date
				Declare @vPesoBascula money
				Declare @vCotizacion money

				Declare cCFVentaCliente cursor for
				Select 
				t.IDTransaccion, 
				t.IDDeposito, 
				t.IDSucursal,
				A.IDMoneda,
				t.FactorDiscriminado,
				t.FactorImpuesto,
				t.IDProducto,
				t.PesoBascula,
				t.Fecha,
				t.Cotizacion
				From vTicketBascula t 
				join Acuerdo A on t.NroAcuerdo = A.NroAcuerdo
				Where t.NroAcuerdo = @NroAcuerdo 
				and t.Anulado = 'False'
				Open cCFVentaCliente   
				fetch next from cCFVentaCliente into @vIDTransaccion, @vIDDeposito,@vIDSucursal, @vIDMoneda, @vFactorDiscriminado, @vFactorImpuesto,@vIDProducto,@vPesoBascula,@vFecha,@vCotizacion
		
				While @@FETCH_STATUS = 0 Begin  

					if @vIDMoneda = 1 begin --si es gs
						Update TicketBascula 
						Set PrecioUnitario = @Precio,
						PrecioUnitarioUS = @Precio/Cotizacion,
						Total = @Precio * PesoBascula,
						TotalUS =(@Precio * PesoBascula)/Cotizacion,
						TotalDiscriminado = (@Precio * PesoBascula) / @vFactorDiscriminado,
						TotalDiscriminadoUS = ((@Precio * PesoBascula) / @vFactorDiscriminado) / Cotizacion,
						TotalImpuesto = (@Precio * PesoBascula) / @vFactorImpuesto,
						TotalImpuestoUS = ((@Precio * PesoBascula) / @vFactorImpuesto)/Cotizacion					
						where IDTransaccion = @vIDTransaccion

						Set @vPrecioUnitarioKardex = (@Precio / @vFactorDiscriminado)
						set @vCostoUnitarioAdicional = (select Isnull(sum(ImporteSinIVA),0) from TicketBasculaGastoAdicional where IDTransaccion = @vIDTransaccion) / @vPesoBascula
						set @vPrecioUnitarioKardex = @vPrecioUnitarioKardex+ @vCostoUnitarioAdicional
					end
					else begin --si no es gs
						Update TicketBascula 
						Set PrecioUnitario = @Precio*Cotizacion,
						PrecioUnitarioUS = @Precio,
						Total = (@Precio * PesoBascula)*Cotizacion,
						TotalUS =(@Precio * PesoBascula),
						TotalDiscriminado = ((@Precio * PesoBascula) / @vFactorDiscriminado)* Cotizacion ,
						TotalDiscriminadoUS = ((@Precio * PesoBascula) / @vFactorDiscriminado),
						TotalImpuesto = ((@Precio * PesoBascula) / @vFactorImpuesto)*Cotizacion,
						TotalImpuestoUS = ((@Precio * PesoBascula) / @vFactorImpuesto)					
						where IDTransaccion = @vIDTransaccion

						Set @vPrecioUnitarioKardex = ((@Precio ) / @vFactorDiscriminado) * @vCotizacion
						set @vCostoUnitarioAdicional = (select Isnull(sum(ImporteSinIVA),0) from TicketBasculaGastoAdicional where IDTransaccion = @vIDTransaccion) / @vPesoBascula
						set @vPrecioUnitarioKardex = @vPrecioUnitarioKardex+ @vCostoUnitarioAdicional
					end

					if @vFecha > @vFechaInicialKardex begin
						Set @vFechaInicialKardex = @vFecha
						print @vFechaInicialKardex
					end
					print @vIDProducto
					if @vIDProducto>0 begin
						
						--Verificar IVA
						Update kardex
						set CostoUnitarioEntrada = @vPrecioUnitarioKardex,
						TotalEntrada = @vPrecioUnitarioKardex * CantidadEntrada
						where IDTransaccion = @vIDTransaccion
						and IDProducto = @vIDProducto

					
						Set @vIDProductoKardex = @vIDProducto
					end

					fetch next from cCFVentaCliente into @vIDTransaccion, @vIDDeposito,@vIDSucursal, @vIDMoneda, @vFactorDiscriminado, @vFactorImpuesto,@vIDProducto,@vPesoBascula,@vFecha,@vCotizacion
			
				End
		
				close cCFVentaCliente 
				deallocate cCFVentaCliente
				print @vIDProductoKardex
				print concat('Recalcula @vIDProductoKardex = ', @vIDProductoKardex, ' @vFechaInicialKardex = ', @vFechaInicialKardex)
				exec SpRecalcularKardex @vIDProductoKardex, @vFechaInicialKardex, 'PRODUCTO'

			End


		end

		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From Acuerdo Where Numero = @Numero) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con ClienteCuentaBancaria
		if exists(Select * From TicketBascula Where NroAcuerdo = @Numero) begin
			set @Mensaje = 'El registro tiene Ticket de Bascula asociado! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Si tiene una relacion con AcuerdoCompra
		if exists(Select * From GastoProducto Where NroAcuerdo = @Numero) begin
			set @Mensaje = 'El registro tiene Factura asociada! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Acuerdo 
		Where Numero = @Numero
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='ACUERDO', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante =@Numero
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	


	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


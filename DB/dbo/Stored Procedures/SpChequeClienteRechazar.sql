﻿CREATE Procedure [dbo].[SpChequeClienteRechazar]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDTransaccionCheque numeric(18,0) = NULL,
	@Numero int = Null,
	@Fecha date = NULL,
	@IDCuentaBancaria tinyint = NULL,
	@Observacion varchar(100) = NULL,
	@IDMotivoRechazo tinyint = NULL,
	
			
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
	--INICIO SM 14072021 - Auditoria Informática
		Declare @vObservacion varchar(100)='' 
		Declare @vvID int 
		
		Declare @vvIDAuditoria int
	    Declare @vvIDSucursal tinyint
		Declare @vvNumero int
		Declare @vvIDCliente int
		Declare @vvIDBanco tinyint
		Declare @vvIDCuentaBancaria int
		Declare @vvNroCheque varchar (50)
		Declare @vvIDMoneda tinyint
		Declare @vvCotizacion money
		Declare @vvImporteMoneda money
		Declare @vvImporte money
		Declare @vvDiferido bit
		Declare @vvFechaVencimiento date
		Declare @vvChequeTercero bit
		Declare @vvLibrador varchar (50)
		Declare @vvSaldo money
		Declare @vvCancelado bit
		Declare @vvCartera bit
		Declare @vvDepositado bit
		Declare @vvRechazado bit
		Declare @vvConciliado bit
		Declare @vvIDMotivoRechazo tinyint
		Declare @vvSaldoACuenta money
		Declare @vvTitular varchar (150)
		Declare @vvFechaCobranza date
		Declare @vvDescontado bit
		Declare @vvAccion varchar(3)
	
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from ChequeClienteRechazado where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
			
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		Set @Numero =ISNULL((Select max(Numero+1) From ChequeClienteRechazado Where IDSucursal=@IDSucursalOperacion),1)
							
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--INICIO SM 23072021 - Auditoria Informática
		set @vObservacion = concat('Inserta un cheque de Cliente Rechazado. IDTransaccion: ',@IDTransaccionSalida)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'INSERTAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccionSalida,'CHEQUE CLIENTE RECHAZADO' )
		
		set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccionSalida)
	    
		Insert Into aChequeClienteRechazado (IDAuditoria, IDTransaccion, IDSucursal,IDCuentaBAncaria, IDTRansaccionCheque,IDMotivoRechazo, Numero, Fecha, Observacion, Anulado, FechaAnulado, IDUsuarioAnulado, Conciliado,IDUsuario, Accion)
		Values(@vvID, @IDTransaccionSalida, @IDSucursalOperacion,@IDCuentaBancaria,  @IDTransaccionCheque, @IDMotivoRechazo,@Numero, @Fecha, @Observacion, 'False', Null, Null, 'False',@IDUsuario,'INS' )
	

		--FIN SM 23072021 - Auditoria Informática

		--Insertar
		Insert Into ChequeClienteRechazado (IDTransaccion, IDSucursal,IDCuentaBAncaria, IDTRansaccionCheque,IDMotivoRechazo, Numero, Fecha, Observacion, Anulado, FechaAnulado, IDUsuarioAnulado, Conciliado)
		Values(@IDTransaccionSalida, @IDSucursalOperacion,@IDCuentaBancaria,  @IDTransaccionCheque, @IDMotivoRechazo,@Numero, @Fecha, @Observacion, 'False', Null, Null, 'False' )
		
		--INICIO SM 23072021 - Auditoria Informática
		set @vObservacion = concat('Actualiza un cheque de Cliente. IDTransaccion: ',@IDTransaccionCheque)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccionCheque,'CHEQUE CLIENTE' )
		
		set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccionCheque)
	    --FIN SM 23072021 - Auditoria Informática
		
		--Actualizar el Cheque	
		Update ChequeCliente set	Rechazado = 'True', 
									Depositado = 'False',
									Cartera = 'False',
									SaldoACuenta = Importe,
									IDMotivoRechazo=@IDMotivoRechazo									
		Where IDTransaccion=@IDTransaccionCheque
		
		set @vvNumero = (select Numero from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvIDBanco = (select IDBanco from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvIDCliente = (select IDCliente from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvIDCuentaBancaria = (select IDCuentaBancaria from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvNroCheque = (select NroCheque from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvIDMoneda = (select IDMoneda from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvCotizacion = (select Cotizacion from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvImporteMoneda = (select ImporteMoneda from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvImporte = (select Importe from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvDiferido = (select Diferido from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvFechaVencimiento = (select FechaVencimiento from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvChequeTercero = (select ChequeTercero from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvLibrador = (select Librador from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvSaldo = (select Saldo from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvCancelado = (select Cancelado from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvCartera = (select Cartera from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvDepositado = (select Depositado from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvRechazado = (select Rechazado from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvConciliado = (select Conciliado from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvIDMotivoRechazo = (select IDMotivoRechazo from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvSaldoACuenta = (select SaldoACuenta from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvTitular = (select Titular from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvFechaCobranza = (select FechaCobranza from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvDescontado = (select Descontado from ChequeCliente where IDTransaccion = @IDTransaccionCheque)

		Insert Into aChequeCliente(IdAuditoria,IDTransaccion, IDSucursal, Numero, IDCliente, IDBanco, IDCuentaBancaria, Fecha, NroCheque, IDMoneda, Cotizacion,  ImporteMoneda,Importe, Diferido, FechaVencimiento, ChequeTercero, Librador, Saldo, Cancelado, Cartera, Depositado, Rechazado, Conciliado,IDMotivoRechazo,SaldoACuenta, Titular, FechaCobranza, Descontado,IDUsuario,Accion)
		values(@vvID,@IDTransaccionCheque, @IDSucursal, @vvNumero, @vvIDCliente, @vvIDBanco, @vvIDCuentaBancaria, @Fecha, @vvNroCheque, @vvIDMoneda, @vvCotizacion,  @vvImporteMoneda, @vvImporte,@vvDiferido, @vvFechaVencimiento, @vvChequeTercero, @vvLibrador, @vvSaldo, @vvCancelado, @vvCartera, @vvDepositado, @vvRechazado, @vvConciliado, @vvIDMotivoRechazo, @vvSaldoACuenta, @vvTitular,@vvFechaCobranza, @vvDescontado,@IDUsuario,'UPD')
							
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'DEL' Begin
	--24/07/2021 SM Para resguardar datos de transaccion a eliminar
	Declare @aIDUsuario smallint = (select IDUsuario from Transaccion where ID=@IDTransaccion)
	Declare @aIDTerminal int = (select IDTerminal from Transaccion where ID=@IDTransaccion)
	Declare @aIDOperacion tinyint = (select IDOperacion from Transaccion where ID=@IDTransaccion)

		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From ChequeClienteRechazado Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--INICIO SM 23072021 - Auditoria Informática
		set @vObservacion = concat('Actualiza un cheque de Cliente. IDTransaccion: ',@IDTransaccionCheque)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@aIDUsuario,@aIDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@aIDOperacion),@vObservacion,@IDTransaccionCheque,'CHEQUE CLIENTE' )
		
		set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccionCheque)
	    --FIN SM 23072021 - Auditoria Informática
		
		--Actualizar el Cheque
		Update ChequeCliente set	Rechazado = 'False', 
									Depositado = 'True',
									Cartera = 'False',
									SaldoACuenta = 0,
									IDMotivoRechazo=NULL
									
		Where IDTransaccion=@IDTransaccionCheque
		
		set @vvNumero = (select Numero from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvIDBanco = (select IDBanco from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvIDCliente = (select IDCliente from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvIDCuentaBancaria = (select IDCuentaBancaria from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvNroCheque = (select NroCheque from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvIDMoneda = (select IDMoneda from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvCotizacion = (select Cotizacion from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvImporteMoneda = (select ImporteMoneda from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvImporte = (select Importe from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvDiferido = (select Diferido from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvFechaVencimiento = (select FechaVencimiento from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvChequeTercero = (select ChequeTercero from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvLibrador = (select Librador from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvSaldo = (select Saldo from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvCancelado = (select Cancelado from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvCartera = (select Cartera from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvDepositado = (select Depositado from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvRechazado = (select Rechazado from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvConciliado = (select Conciliado from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvIDMotivoRechazo = (select IDMotivoRechazo from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvSaldoACuenta = (select SaldoACuenta from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvTitular = (select Titular from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvFechaCobranza = (select FechaCobranza from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvDescontado = (select Descontado from ChequeCliente where IDTransaccion = @IDTransaccionCheque)

		Insert Into aChequeCliente(IdAuditoria,IDTransaccion, IDSucursal, Numero, IDCliente, IDBanco, IDCuentaBancaria, Fecha, NroCheque, IDMoneda, Cotizacion,  ImporteMoneda,Importe, Diferido, FechaVencimiento, ChequeTercero, Librador, Saldo, Cancelado, Cartera, Depositado, Rechazado, Conciliado,IDMotivoRechazo,SaldoACuenta, Titular, FechaCobranza, Descontado,IDUsuario,Accion)
		values(@vvID,@IDTransaccionCheque, @IDSucursal, @vvNumero, @vvIDCliente, @vvIDBanco, @vvIDCuentaBancaria, @Fecha, @vvNroCheque, @vvIDMoneda, @vvCotizacion,  @vvImporteMoneda, @vvImporte,@vvDiferido, @vvFechaVencimiento, @vvChequeTercero, @vvLibrador, @vvSaldo, @vvCancelado, @vvCartera, @vvDepositado, @vvRechazado, @vvConciliado, @vvIDMotivoRechazo, @vvSaldoACuenta, @vvTitular,@vvFechaCobranza, @vvDescontado,@aIDUsuario,'UPD')
			
		--INICIO SM 26072021 - Auditoria Informática
		set @vObservacion = concat('Elimina un cheque de Cliente. IDTransaccion: ',@IDTransaccionCheque)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccion,'CHEQUE CLIENTE RECHAZADO' )
		
		set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion)
	    --FIN SM 26072021 - Auditoria Informática
	
		Declare @avNumero int = (select Numero from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		Declare @avIDCuentaBancaria tinyint = (select IDCuentaBancaria from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		Declare @avIDMotivoRechazo int = (select IDMotivoRechazo from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		Declare @avCotizacion money = (select Cotizacion from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		Declare @avObservacion varchar(100) =(select Observacion from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		Declare @avAnulado bit = (select Anulado from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		Declare @avFechaAnulado date = (select FechaAnulado from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		Declare @avIDUsuarioAnulado smallint = (select IDUsuarioAnulado from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		Declare @avConciliado bit = (select Conciliado from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
	
	    Insert Into aChequeClienteRechazado (IDAuditoria, IDTransaccion, IDSucursal,IDCuentaBancaria, IDTRansaccionCheque,IDMotivoRechazo, Numero, Fecha, Observacion, Anulado, FechaAnulado, IDUsuarioAnulado, Conciliado,IDUsuario, Accion)
	    values(@vvID, @IDTransaccion, @IDSucursalOperacion,@avIDCuentaBancaria, @IDTransaccionCheque, @avIDMotivoRechazo,@avNumero, @Fecha, @Observacion, @avAnulado, @avFechaAnulado, @avIDUsuarioAnulado, @avConciliado,@aIDUsuario,'DEL' )
	
		--Eliminamos el registro
		Delete ChequeClienteRechazado Where IDTransaccion = @IDTransaccion
				
		--INICIO SM 26072021 - Auditoria Informática
		set @vObservacion = concat('Se Eliminara el Detalle de Asiento: IDTransaccion',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@aIDUsuario,@aIDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@aIDOperacion),@vObservacion,@IDTransaccion,'DETALLE ASIENTO' )
		
		set @vObservacion = concat('Se Eliminara el Asiento. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@aIDUsuario,@aIDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@aIDOperacion),@vObservacion,@IDTransaccion,'ASIENTO' )
		
		set @vObservacion = concat('Se Eliminara la Transaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@aIDUsuario,@aIDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@aIDOperacion),@vObservacion,@IDTransaccion,'TRANSACCION' )
		--FIN SM 26072021 - Auditoria Informática
		
						
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion				
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	--24/07/2021 SM Para resguardar datos de transaccion a eliminar
	set @aIDUsuario  = (select IDUsuario from Transaccion where ID=@IDTransaccion)
	set @aIDTerminal  = (select IDTerminal from Transaccion where ID=@IDTransaccion)
	set @aIDOperacion  = (select IDOperacion from Transaccion where ID=@IDTransaccion)
	
	--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From ChequeClienteRechazado Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--IDTransaccion
		If Exists(Select * From ChequeClienteRechazado Where IDTransaccion=@IDTransaccion And Anulado = 'True') Begin
			set @Mensaje = 'El registro solicitado ya se encuentra anulado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		If Exists(select * from vDetallePagoChequeCliente DP Join PagoChequeCliente PCC 
		on DP.IDTransaccionPagoChequeCliente = PCC.IDTransaccion 
		where DP.IDTransaccion = @IDTransaccionCheque and Anulado = 0) begin
			set @Mensaje = 'El cheque tiene canje asociado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--INICIO SM 26072021 - Auditoria Informática
		set @vObservacion = concat('Anula un cheque de Cliente. IDTransaccion: ',@IDTransaccionCheque)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@aIDUsuario,@aIDTerminal,'ANULA',(Select Descripcion from Operacion where id=@aIDOperacion),@vObservacion,@IDTransaccionCheque,'CHEQUE CLIENTE' )
		
		set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccionCheque)
	    --FIN SM 26072021 - Auditoria Informática

		--Actualizar el Cheque
		Update ChequeCliente set	Rechazado = 'False', 
									Depositado = 'True',
									Cartera = 'False',
									SaldoACuenta = 0,
									IDMotivoRechazo=NULL
									
		Where IDTransaccion=@IDTransaccionCheque

		set @vvNumero = (select Numero from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvIDCliente = (select IDCliente from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvIDBanco = (select IDBanco from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvIDCuentaBancaria = (select IDCuentaBancaria from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvNroCheque = (select NroCheque from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvIDMoneda = (select IDMoneda from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvCotizacion = (select Cotizacion from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvImporteMoneda = (select ImporteMoneda from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvImporte = (select Importe from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvDiferido = (select Diferido from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvFechaVencimiento = (select FechaVencimiento from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvChequeTercero = (select ChequeTercero from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvLibrador = (select Librador from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvSaldo = (select Saldo from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvCancelado = (select Cancelado from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvCartera = (select Cartera from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvDepositado = (select Depositado from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvRechazado = (select Rechazado from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvConciliado = (select Conciliado from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvIDMotivoRechazo = (select IDMotivoRechazo from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvSaldoACuenta = (select SaldoACuenta from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvTitular = (select Titular from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvFechaCobranza = (select FechaCobranza from ChequeCliente where IDTransaccion = @IDTransaccionCheque)
		set @vvDescontado = (select Descontado from ChequeCliente where IDTransaccion = @IDTransaccionCheque)

		Insert Into aChequeCliente(IdAuditoria,IDTransaccion, IDSucursal, Numero, IDCliente, IDBanco, IDCuentaBancaria, Fecha, NroCheque, IDMoneda, Cotizacion,  ImporteMoneda,Importe, Diferido, FechaVencimiento, ChequeTercero, Librador, Saldo, Cancelado, Cartera, Depositado, Rechazado, Conciliado,IDMotivoRechazo,SaldoACuenta, Titular, FechaCobranza, Descontado,IDUsuario,Accion)
		values(@vvID,@IDTransaccionCheque, @IDSucursal, @vvNumero, @vvIDCliente, @vvIDBanco, @vvIDCuentaBancaria, @Fecha, @vvNroCheque, @vvIDMoneda, @vvCotizacion,  @vvImporteMoneda, @vvImporte,@vvDiferido, @vvFechaVencimiento, @vvChequeTercero, @vvLibrador, @vvSaldo, @vvCancelado, @vvCartera, @vvDepositado, @vvRechazado, @vvConciliado, @vvIDMotivoRechazo, @vvSaldoACuenta, @vvTitular,@vvFechaCobranza, @vvDescontado,@aIDUsuario,'UPD')
			
		--Anular
		Update ChequeClienteRechazado Set Anulado='True', IDUsuarioAnulado=@IDUsuario, FechaAnulado = GETDATE()
		Where IDTransaccion = @IDTransaccion
		
		--INICIO SM 26072021 - Auditoria Informática
		set @vObservacion = concat('Anula un cheque rechazado de Cliente. IDTransaccion: ',@IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@aIDUsuario,@aIDTerminal,'UPDATE',(Select Descripcion from Operacion where id=@aIDOperacion),@vObservacion,@IDTransaccion,'CHEQUE CLIENTE RECHAZADO' )
		
		set @vvID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion)
	    --FIN SM 26072021 - Auditoria Informática
		
		set @avNumero = (select Numero from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		set @avIDCuentaBancaria = (select IDCuentaBancaria from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		set @avIDMotivoRechazo  = (select IDMotivoRechazo from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		set @avCotizacion  = (select Cotizacion from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		set @avObservacion =(select Observacion from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		set @avAnulado  = (select Anulado from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		set @avFechaAnulado  = (select FechaAnulado from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		set @avIDUsuarioAnulado = (select IDUsuarioAnulado from ChequeClienteRechazado where idtransaccion = @IDTransaccion)
		set @avConciliado = (select Conciliado from ChequeClienteRechazado where idtransaccion = @IDTransaccion)		
	
	    Insert Into aChequeClienteRechazado (IDAuditoria, IDTransaccion, IDSucursal,IDCuentaBancaria, IDTRansaccionCheque,IDMotivoRechazo, Numero, Fecha, Observacion, Anulado, FechaAnulado, IDUsuarioAnulado, Conciliado,IDUsuario, Accion)
	    values(@vvID, @IDTransaccion, @IDSucursalOperacion,@avIDCuentaBancaria, @IDTransaccionCheque, @avIDMotivoRechazo,@avNumero, @Fecha, @Observacion, @avAnulado, @avFechaAnulado, @avIDUsuarioAnulado, @avConciliado,@aIDUsuario,'ANU' )
	
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal, @IDTerminal=@IDTerminal		
		
		--INICIO SM 26072021 - Auditoria Informática
		set @vObservacion = concat ('Se Eliminara el Detalle de Asiento. IDTransaccion; ', @IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@aIDUsuario,@aIDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@aIDOperacion),@vObservacion,@IDTransaccion,'DETALLE ASIENTO' )
		
		set @vObservacion = concat ('Se Eliminara el Asiento. IDTransaccion; ', @IDTransaccion)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@aIDOperacion),@vObservacion,@IDTransaccion,'ASIENTO' )
		--FIN SM 26072021 - Auditoria Informática
						
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		Set @IDTransaccionSalida = 0
		return @@rowcount
			
	End
	
		
End





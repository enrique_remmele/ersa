﻿create Procedure [dbo].[SpViewExtractoMovimientoClienteTodos]

	--Entrada
	@Fecha1 Date,
	@Fecha2 Date,
	@IDSucursal INT = 0	
	
As

Begin

	PRINT 'Dia: ' + Convert(varchar(50), DAY(@Fecha1))  + ' - Mes: ' + Convert(varchar(50), MONTH(@Fecha1))  + ' - Año: ' + Convert(varchar(50), Year(@Fecha1))  + ' ' 
	PRINT 'Dia: ' + Convert(varchar(50), DAY(@Fecha2))  + ' - Mes: ' + Convert(varchar(50), MONTH(@Fecha2))  + ' - Año: ' + Convert(varchar(50), Year(@Fecha2))  + ' ' 
	
	--Crear la tabla temporal
    create table #TablaTemporal(ID tinyint,
								IDTransaccion int,
								IDSucursal INT,
								Codigo int,
								RazonSocial varchar(50),
								Referencia varchar(50),
								Fecha date,
								Operacion varchar(50),
								Documento varchar(50),
								[Detalle/Concepto] varchar(100),
								Saldo money,
								SaldoAnterior money,
								SaldoInicial money,
								Debito money,
								Credito money,
								IDMoneda int,
								DescripcionMoneda varchar(50),
								Movimiento varchar(30))
								
	--Variables para calcular saldo
	declare @vSaldo money
	declare @vTotalDebito money
	declare @vTotalCredito money
	declare @vSaldoAnterior money
	declare @vSaldoInicial money
	
	--Declarar variables
	declare @vID tinyint
	declare @vIDTransaccion Numeric (18,0)
	declare @vIDSucursal int
	declare @vIDCliente int
	declare @vRazonSocial varchar (100)
	declare @vReferencia varchar(50)
	declare @vFecha date
	declare @vOperacion varchar(50)
	declare @vDocumento varchar(50)
	declare @vDetalleConcepto varchar(100)
	declare @vDebito money
	declare @vCredito money
	declare @vIDMoneda int
	declare @vMoneda varchar(50)
	declare @vMovimiento varchar(10)
	
	set @vID = (Select IsNull(MAX(ID)+1,1) From #TablaTemporal )
	
	--Insertar datos	
	Begin
		
		--Hallar Totales para Saldo Anterior
		--Por cliente
		IF @IDSucursal = 0 Begin
			Set @vTotalDebito  = IsNull((Select Sum(Debito) From VExtractoMovimientoClienteTodos Where Fecha < @Fecha1),0)
			Set @vTotalCredito = IsNull((Select Sum(Credito) From VExtractoMovimientoClienteTodos Where Fecha < @Fecha1),0)
		END
		
		--Por Sucursal
		IF @IDSucursal > 0 Begin
			Set @vTotalDebito  = IsNull((Select Sum(Debito) From VExtractoMovimientoClienteTodos Where Fecha < @Fecha1 AND @IDSucursal=@IDSucursal),0)
			Set @vTotalCredito = IsNull((Select Sum(Credito) From VExtractoMovimientoClienteTodos Where Fecha < @Fecha1 AND @IDSucursal=@IDSucursal),0)
		END
		
		--Hallar Saldo Anterior
		Set @vSaldoAnterior = @vTotalDebito - @vTotalCredito
		Set @vSaldoInicial = @vSaldoAnterior 
		
		Declare db_cursor cursor for
		
		Select 
		IDTransaccion,
		IDSucursal,
		Codigo,
		RazonSocial,
		Referencia,
		Fecha,
		Operacion,
		Documento,
		[Detalle/Concepto],
		IDMoneda,
		DescripcionMoneda,
		Debito,
		Credito,
		Movimiento
		From VExtractoMovimientoClienteTodos
		Where Fecha between @Fecha1 and @Fecha2
		Order By Fecha, IDTransaccion		
		Open db_cursor   
		Fetch Next From db_cursor Into	@vIDTransaccion, @vIDSucursal, @vIDCliente,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vIDMoneda,@vMoneda,@vDebito,@vCredito,@vMovimiento
		While @@FETCH_STATUS = 0 Begin 
					
			--Hallar Saldo			
			Set @vSaldo = (@vSaldoAnterior + @vDebito) - @vCredito
			
			Insert Into  #TablaTemporal(ID,IDTransaccion, IDSucursal, Codigo,RazonSocial,Referencia,Fecha,Operacion,Documento,[Detalle/Concepto],Saldo,SaldoAnterior,SaldoInicial,Debito,Credito,IDMoneda,DescripcionMoneda,Movimiento) 
								Values (@vID,@vIDTransaccion, @vIDSucursal, @vIDCliente,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vSaldo,@vSaldoAnterior,@vSaldoInicial,@vDebito,@vCredito,@vIDMoneda,@vMoneda,@vMovimiento)
							
			--Actualizar Saldo
			set @vSaldoAnterior = @vSaldo 
			
			Fetch Next From db_cursor Into	@vIDTransaccion, @vIDSucursal, @vIDCliente,@vRazonSocial,@vReferencia,@vFecha,@vOperacion,@vDocumento,@vDetalleConcepto,@vIDMoneda,@vMoneda,@vDebito,@vCredito,@vMovimiento
			
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		
	End	
	
	IF @IDSucursal = 0 Begin
		Select * From #TablaTemporal Where ID=@vID
	End
	
	IF @IDSucursal > 0 Begin
		Select * From #TablaTemporal Where ID=@vID AND IDSucursal=@IDSucursal
	END
	
End
	






























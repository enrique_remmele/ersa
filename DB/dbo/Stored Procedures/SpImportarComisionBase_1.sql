﻿CREATE Procedure [dbo].[SpImportarComisionBase]

	--Entrada
	@TipoProducto varchar(50),
	@Vendedor varchar(50),
	@IDSucursal int,
	@PorcentajeBase varchar(50),
	
	@Actualizar bit = 'True'

As

Begin

	--Variables
	Declare @vIDTipoProducto int
	Declare @vIDVendedor int
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	
	--Obtener valores
	Set @vMensaje = 'No se proceso'
	Set @vProcesado = 'False'
	Set @vIDTipoProducto=(Select ID From TipoProducto Where Referencia=@TipoProducto And Estado='True')
	Set @vIDVendedor=(Select ID From Vendedor Where Referencia=@Vendedor And IDSucursal=@IDSucursal And Estado='True')
	
	--Modificar Porcentaje
	Set @PorcentajeBase = REPLACE(@PorcentajeBase, ',','.')
	Set @PorcentajeBase = CONVERT(varchar(50), convert(decimal(10,2), @PorcentajeBase))
	
	--Verificar que existan los
	If @vIDTipoProducto Is Null Begin
		Set @vMensaje = 'No se encontro el tipo de producto!'
		Set @vProcesado = 'False'
		GoTo Salir
	End
	
	If @vIDVendedor Is Null Begin
		Set @vMensaje = 'No se encontro el vendedor!'
		Set @vProcesado = 'False'
		GoTo Salir
	End
	
	Declare @vIDProducto int
	Declare @vPorcentajeAnterior decimal(5,3)
	
	Declare db_cursor cursor for
	Select ID From Producto Where  IDTipoProducto=@vIDTipoProducto
	Open db_cursor   
	Fetch Next From db_cursor Into @vIDProducto
	While @@FETCH_STATUS = 0 Begin  
	
		--Si Existe, actualizamos
		If Exists(Select * From ComisionProducto  Where IDProducto=@vIDProducto And IDVendedor=@vIDVendedor ) Begin
		
			If @Actualizar = 'True' Begin
				--Obtener Porcentaje anterior
				Set @vPorcentajeAnterior = (Select PorcentajeBase  from ComisionProducto Where IDProducto=@vIDProducto And IDVendedor=@vIDVendedor )
				
				--Actualizar
				Update ComisionProducto Set PorcentajeBase = @PorcentajeBase  ,
											PorcentajeAnterior = @vPorcentajeAnterior ,
											FechaUltimoCambio = GETDATE (),
											IDUsuario= 1
				Where IDProducto=@vIDProducto And IDVendedor=@vIDVendedor  And IDSucursal=@IDSucursal
			End
		End
	
		--Si no existe Insertamos
		If Not Exists(Select * From ComisionProducto  Where IDProducto=@vIDProducto And IDVendedor=@vIDVendedor ) Begin 
	
			--Insertar
			Insert Into ComisionProducto (IDProducto, IDVendedor, IDSucursal, PorcentajeBase, PorcentajeEsteProducto, PorcentajeAnterior, IDUsuario)
			values(@vIDProducto, @vIDVendedor, @IDSucursal, @PorcentajeBase, 0, @vPorcentajeAnterior, 1)

		End
	
		Fetch Next From db_cursor Into @vIDProducto
		
	End

	Close db_cursor   
	Deallocate db_cursor

	Set @vMensaje = 'Registro guardado!'
	Set @vProcesado = 'True'
	GoTo Salir
	
Salir:	
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	
End
	


	


﻿CREATE Procedure [dbo].[SpRegenerarAsientoVenta]

	@Desde date,
	@Hasta date,
	@Todos bit = 'False',
	@SoloConDiferencias bit = 'False',
	@IDSucursal tinyint
	
As

Begin
	
	--Variables
	Begin
		Declare @vIDTransaccion numeric(18,0)		
		Declare @vComprobante varchar(50)
		Declare @vCantidad int
		Declare @vRegistrosProcesados int
		Declare @vPorcentaje int
	End
	
	If Not Exists (Select * From sys.objects Where object_id = OBJECT_ID(N'[dbo].[GeneracionAsientoNotaCredito]') And type in (N'U')) Begin
		Create Table GeneracionAsientoNotaCredito(ID int, Cantidad int, RegistrosProcesados int, Porcentaje int)
	End
	
	Delete From GeneracionAsientoNotaCredito
	
	Set @vCantidad = (Select Count(*) From VVenta V Where V.FechaEmision Between @Desde And @Hasta And V.IDSucursal=@IDSucursal And (Case When @SoloConDiferencias = 0 Then 1 Else (Case When (Select A.Saldo From Asiento A Where A.IDTransaccion=V.IDTransaccion)<>0 Then 1 Else 0 End) End) = 1)
	Set @vRegistrosProcesados = 0
	Set @vPorcentaje = 0
	
	Insert Into GeneracionAsientoNotaCredito(ID, Cantidad, RegistrosProcesados, Porcentaje)
	Values(1, @vCantidad, 0, 0)
	
	Declare cVenta cursor for
	Select V.IDTransaccion, V.Comprobante 
	From VVenta V
	Where V.FechaEmision Between @Desde And @Hasta And V.IDSucursal=@IDSucursal and IDSucursal <> 5 -- No regenerar Mercury porque hacen asiento de Costo a mano.
	And (Case When @SoloConDiferencias = 0 Then 1 Else (Case When (Select A.Saldo From Asiento A Where A.IDTransaccion=V.IDTransaccion)<>0 Then 1 Else 0 End) End) = 1
	Open cVenta   
	fetch next from cVenta into @vIDTransaccion, @vComprobante

	While @@FETCH_STATUS = 0 Begin  			  
		
		--Generar
		Exec SpAsientoVenta @IDTransaccion=@vIDTransaccion		
		
		----Reparar, se coment porque en SpAsientoVenta ya hace
		--Exec SpRedondearAsientoVenta @IDTransaccion=@vIDTransaccion
		
		--Contador
		Set @vRegistrosProcesados = @vRegistrosProcesados + 1
		Set @vPorcentaje = (@vRegistrosProcesados / @vCantidad) * 100
		
		Update GeneracionAsientoNotaCredito Set RegistrosProcesados=@vRegistrosProcesados,
													Porcentaje=@vPorcentaje
		Where ID=1
					
		--Siguiente
		fetch next from cVenta into @vIDTransaccion, @vComprobante
		
	End   
	
	close cVenta   
	deallocate cVenta
			
	Select 'Mensaje'='Registros procesados: ' + CONVERT(varchar(50), @vCantidad), 'Procesado'='True'
	
End

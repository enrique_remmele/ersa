﻿CREATE Procedure [dbo].[SpInicializarTerminal]

	--Entrada
	@Terminal varchar(50),
	@CodigoActivacion varchar(500),
	@Version varchar(100)
		
As

Begin

	--Varialbes
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	Declare @vIDTerminal int
	Declare @vIDSucursal tinyint
	Declare @vIDDeposito smallint
		
	Set @vMensaje = 'No funciona.'
	Set @vProcesado = 'false'
	Set @vIDTerminal = 0
	Set @vIDSucursal = 0
	Set @vIDDeposito = 0
	
	--Verificar la version del sistema
	If (Select Top(1) VersionSistema From Configuraciones) != @Version Begin
	
		Set @vMensaje = 'La version del sistema que esta utilizando no es la correcta! Version actual: ' + CONVERT(varchar(200), (Select Top(1) VersionSistema From Configuraciones ))
		Set @vProcesado = 'false'
		Set @vIDTerminal = 0
		
		GoTo Salir
		
	End
		
	--Verificar que exista la terminal
	If Not Exists(Select * From Terminal Where CodigoActivacion=@CodigoActivacion) Begin
	
		Set @vIDTerminal = (Select ISNULL(Max(ID) + 1, 1) From Terminal)
		
		Insert into Terminal(ID, Descripcion, CodigoActivacion, Activado, UltimoAcceso)
		Values(@vIDTerminal, @Terminal, @CodigoActivacion, 'True', GETDATE())
		
		--Si es la primera terminal en entrar, darle acceso
		If (Select COUNT(*) From Terminal) = 1 Begin
			Set @vMensaje = 'Ingreso al sistema por primera vez!'
			Set @vProcesado = 'True'
			GoTo Salir
		End
		
		Set @vMensaje = 'Debe solicitar acceso desde esta terminal al administrador del sistema!'
		Set @vProcesado = 'false'
		Set @vIDTerminal = 0
		
		GoTo Salir
		
	End
	
	--Obtener el ID de la Terminal
	Set @vIDTerminal = (Select Top(1) ID From Terminal Where CodigoActivacion=@CodigoActivacion)
	
	--Verificar la terminal este activa
	If (Select IsNull(Activado, 'False') From Terminal Where ID=@vIDTerminal) = 'False' Begin
	
		Set @vMensaje = 'Debe solicitar acceso desde esta terminal al administrador del sistema!'
		Set @vProcesado = 'false'
				
		GoTo Salir
		
	End
	
	Set @vMensaje = 'Acceso correcto!'
	Set @vProcesado = 'True'
	Goto Salir
			
Salir:
	print @vMensaje
	print @vIDTerminal
	Select T.*, 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado From VTerminal T Where ID=@vIDTerminal
End


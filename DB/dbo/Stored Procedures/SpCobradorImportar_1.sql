﻿CREATE Procedure [dbo].[SpCobradorImportar]

	--Entrada
	@Referencia varchar(100),
	@Nombres varchar(100),
	
	@Actualizar bit = 'True'
	
As

Begin

	Declare @vID int
	
	--Si existe
	If Exists(Select * From Cobrador Where Nombres=@Nombres) Begin
		Update Cobrador Set Nombres=@Nombres, Referencia=@Referencia
		Where Nombres=@Nombres
	End
	
	--Si existe
	If Not Exists(Select * From Cobrador Where Nombres=@Nombres) Begin
		If @Actualizar = 'True' Begin
			Set @vID = (Select IsNull(Max(ID)+1,1) From Cobrador)
			Insert Into Cobrador(ID, Nombres,  Estado, Referencia)
			Values(@vID, @Nombres,  'True', @Referencia)
		End
	End
	
End



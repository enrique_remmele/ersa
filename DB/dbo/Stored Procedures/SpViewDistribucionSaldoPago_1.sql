﻿CREATE Procedure [dbo].[SpViewDistribucionSaldoPago]

	--Entrada
	@Fecha1 Date,
	@Fecha2 Date,
	@Fecha3 Date,
	@Fecha4 Date,
	@Fecha5 Date,
	@Fecha6 Date,
	@FechaReferencia date,
	@Condicion bit,
	@IDMoneda int = 1
	
		
As

	
Begin
	
	--Crear la tabla temporal
    create table #TablaTemporal(ID tinyint,
								IDTransaccion int
								,Proveedor varchar(50),
								Referencia varchar(50),
								Total Money,
								Saldo money,
								Fecha1 money,
								Fecha2 money,
								Fecha3 money,
								Fecha4 money,
								Fecha5 money,
								Fecha6 money,
								Fecha7 money,
								IDTipoComprobante int,
								IDTipoProveedor int,
								IDSucursal int,
								IDDeposito int,
								IDMoneda int--,
								--primary key(IDTransaccion,ID))
								)
 
	
	--Declarar variables 
	declare @vID tinyint
	declare @vIDTransaccion numeric (18,0)
	declare @vRazonSocial varchar (100)
	declare @vReferencia varchar(50)
	declare @vSaldo money
	declare @vFechaVencimiento date
	declare @vIDTipoComprobante int
	declare @vIDTipoProveedor int
	declare @vIDSucursal int
	declare @vIDDeposito int
	declare @vIDMoneda int
	declare @vTotal money
	declare @vFecha1 money
	declare @vFecha2 money
	declare @vFecha3 money
	declare @vFecha4 money
	declare @vFecha5 money
	declare @vFecha6 money
	declare @vFecha7 money
		
	set @vID=(Select isnull(MAX(ID)+ 1,1) From #TablaTemporal)
	
	--Condicion= A Vencer
	if @Condicion = 0 Begin
	
		--set @vFecha1 = 0
		--set @vFecha2 = 0
		--set @vFecha3 = 0
		--set @vFecha4 = 0
		--set @vFecha5 = 0
		--set @vFecha6 = 0
		--set @vFecha7 = 0
		--set @vTotal = 0
		
		Declare db_cursor cursor for
		Select  
		C.IDtransaccion,
		P.RazonSocial,
		P.Referencia,
		C.Saldo, 
		'FechaVencimiento'= Isnull (C.FechaVencimiento, C.Fecha),
		C.IDTipoComprobante,
		'IDTipoProveedor'=IsNull(P.IDTipoProveedor,0),
		C.IDSucursal,
		C.IDDeposito,
		C.IDMoneda
		From Proveedor P
		Join VCompraGasto C On C.IDProveedor=P.ID 
		Where 
		--C.Saldo > 0 
		C.Cancelado = 'False'
		And C.IDMoneda=@IDMoneda
		and C.CajaChica = 0
		
	
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccion, @vRazonSocial,@vReferencia, @vSaldo,@vFechaVencimiento,@vIDTipoComprobante,@vIDTipoProveedor,@vIDSucursal,@vIDDeposito,@vIDMoneda
		While @@FETCH_STATUS = 0 Begin  

		set @vFecha1 = 0
		set @vFecha2 = 0
		set @vFecha3 = 0
		set @vFecha4 = 0
		set @vFecha5 = 0
		set @vFecha6 = 0
		set @vFecha7 = 0
		set @vTotal = 0
								
			--Fecha1
			If @vFechaVencimiento <= @Fecha1 And @vFechaVencimiento >= @FechaReferencia
			Begin
				Set @vFecha1 = @vSaldo
			End
			
			print @vFecha1
			
			--Fecha2
			If @vFechaVencimiento > @Fecha1 and @vFechaVencimiento <= @Fecha2
			Begin
				Set @vFecha2 = @vSaldo
			End
					
			--Fecha3
			If @vFechaVencimiento > @Fecha2 And @vFechaVencimiento <= @Fecha3 
			Begin
				Set @vFecha3 = @vSaldo
			End
			
			--Fecha4
			If @vFechaVencimiento > @Fecha3 And @vFechaVencimiento <= @Fecha4 
			Begin
				Set @vFecha4 = @vSaldo
			End
			
			--Fecha5
			If @vFechaVencimiento > @Fecha4 And @vFechaVencimiento <= @Fecha5 
			Begin
				Set @vFecha5 = @vSaldo
			End
			
			--Fecha6
			If @vFechaVencimiento > @Fecha5 And @vFechaVencimiento<= @Fecha6 
			Begin
				Set @vFecha6 = @vSaldo
			End
			
			--Fecha7
			If @vFechaVencimiento > @Fecha6 
			Begin
				Set @vFecha7 = @vSaldo
			End
							
			set @vTotal = @vSaldo 
						
			Insert into #TablaTemporal(ID,IDTransaccion,Proveedor,Referencia,Total,Saldo,Fecha1,Fecha2,Fecha3,Fecha4,Fecha5,Fecha6,Fecha7,IDTipoComprobante,IDTipoProveedor,IDSucursal,IDDeposito,IDMoneda) 
			Values (@vID,@vIDTransaccion,@vRazonSocial,@vReferencia,@vTotal,@vSaldo,@vFecha1,@vFecha2,@vFecha3,@vFecha4,@vFecha5,@vFecha6,@vFecha7,@vIDTipoComprobante,@vIDTipoProveedor,@vIDSucursal,@vIDDeposito,@vIDMoneda) 	
			
			
			Fetch Next From db_cursor Into @vIDTransaccion, @vRazonSocial,@vReferencia, @vSaldo,@vFechaVencimiento,@vIDTipoComprobante,@vIDTipoProveedor,@vIDSucursal,@vIDDeposito,@vIDMoneda
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor
		
	End	
	
	--VENCIDOS
	Else Begin -- else de @Condicion
		
		--set @vFecha1 = 0
		--set @vFecha2 = 0
		--set @vFecha3 = 0
		--set @vFecha4 = 0
		--set @vFecha5 = 0
		--set @vFecha6 = 0
		--set @vFecha7 = 0
		--set @vTotal = 0
		
		Declare db_cursor cursor for
		Select  
		C.IDtransaccion,
		P.RazonSocial,
		P.Referencia,
		C.Saldo, 
		'FechaVencimiento'= Isnull (C.FechaVencimiento, C.Fecha),
		C.IDTipoComprobante,
		'IDTipoProveedor'=IsNull(P.IDTipoProveedor,0),
		C.IDSucursal,
		C.IDDeposito,
		C.IDMoneda
		From Proveedor P
		Join VCompraGasto C On C.IDProveedor=P.ID 
		--And C.Saldo > 0
		and C.Cancelado = 'False'
		And C.IDMoneda=@IDMoneda
		and C.CajaChica = 0
		and C.Cuota=0

		union all

		Select  
		C.IDtransaccion,
		P.RazonSocial,
		P.Referencia,
		CU.Saldo,
		'FechaVencimiento'= ISNULL (CU.Vencimiento, C.Fecha),
		C.IDTipoComprobante,
		'IDTipoProveedor'=IsNull(P.IDTipoProveedor,0),
		C.IDSucursal,
		C.IDDeposito,
		C.IDMoneda
		From Proveedor P
		Join VCompraGasto C On C.IDProveedor=P.ID 
		Join Cuota CU On C.IDTransaccion=CU.IDTransaccion
		--And CU.Saldo > 0
		and C.Cancelado = 'False'
		And C.IDMoneda=@IDMoneda
		and C.CajaChica = 0
		and C.Cuota>0
		
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccion, @vRazonSocial,@vReferencia, @vSaldo,@vFechaVencimiento,@vIDTipoComprobante,@vIDTipoProveedor,@vIDSucursal,@vIDDeposito,@vIDMoneda
		While @@FETCH_STATUS = 0 Begin  
			set @vFecha1 = 0
			set @vFecha2 = 0
			set @vFecha3 = 0
			set @vFecha4 = 0
			set @vFecha5 = 0
			set @vFecha6 = 0
			set @vFecha7 = 0
			set @vTotal = 0
								
			--Fecha1
			If @vFechaVencimiento >= @Fecha1  And @vFechaVencimiento <= @FechaReferencia 
			Begin
				Set @vFecha1 = @vSaldo
			End
					
			--Fecha2
			If @vFechaVencimiento >= @Fecha2 And @vFechaVencimiento < @Fecha1
			Begin
				Set @vFecha2 = @vSaldo
			End
			
			--Fecha3
			If @vFechaVencimiento >= @Fecha3 And @vFechaVencimiento < @Fecha2
			Begin
				Set @vFecha3 = @vSaldo
			End
			
			--Fecha4
			If @vFechaVencimiento >= @Fecha4 And @vFechaVencimiento < @Fecha3 
			Begin
				Set @vFecha4 = @vSaldo
			End
			
			--Fecha5
			If @vFechaVencimiento >= @Fecha5 And @vFechaVencimiento < @Fecha4
			 Begin
				Set @vFecha5= @vSaldo
			 End
			
			--Fecha6
			If @vFechaVencimiento >= @Fecha6 And @vFechaVencimiento < @Fecha5 
			 Begin
				Set @vFecha6= @vSaldo
			 End
							
			--Fecha7
			if @vFechaVencimiento < @Fecha6
			 Begin
				Set @vFecha7= @vSaldo
			 End				
			set @vTotal = @vSaldo 
						
			 Insert into #TablaTemporal(ID,IDTransaccion,Proveedor,Referencia,Total,Saldo,Fecha1,Fecha2,Fecha3,Fecha4,Fecha5,Fecha6,Fecha7,IDTipoComprobante,IDTipoProveedor,IDSucursal,IDDeposito,IDMoneda) 
			 Values (@vID,@vIDTransaccion,@vRazonSocial,@vReferencia,@vTotal,@vSaldo,@vFecha1,@vFecha2,@vFecha3,@vFecha4,@vFecha5,@vFecha6,@vFecha7,@vIDTipoComprobante,@vIDTipoProveedor,@vIDSucursal,@vIDDeposito,@vIDMoneda) 	
			
			
			Fetch Next From db_cursor Into @vIDTransaccion, @vRazonSocial,@vReferencia, @vSaldo,@vFechaVencimiento,@vIDTipoComprobante,@vIDTipoProveedor,@vIDSucursal,@vIDDeposito,@vIDMoneda
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		
		set @vFecha1 = 0
		set @vFecha2 = 0
		set @vFecha3 = 0
		set @vFecha4 = 0
		set @vFecha5 = 0
		set @vFecha6 = 0
		set @vFecha7 = 0
		set @vTotal = 0
		
	End	
					
	--Desplegamos los datos de la Tabla Temporal	
		
	Select  
	Proveedor,
	Referencia,
	--IDDeposito,
	IDMoneda,
	--IDTipoComprobante,
	IDTipoProveedor,
	--IDSucursal,
	'Total'=SUM(Total),
	'Fecha1'=sum(Fecha1),
	'Fecha2'=sum(Fecha2),
	'Fecha3'=sum(Fecha3),
	'Fecha4'=sum(Fecha4),
	'Fecha5'=sum(Fecha5),
	'Fecha6'=sum(Fecha6),
	'Fecha7'=sum(Fecha7) 
	From #TablaTemporal Where ID = @vID 
	And (Fecha1 > 0 or Fecha2 > 0 or Fecha3 > 0 or Fecha4 > 0 or Fecha5 > 0 or Fecha6 > 0 or Fecha7 > 0)
	--and Referencia = '81BOLSI'
	group by Proveedor,
	Referencia,
	--IDDeposito,
	IDMoneda,
	--IDTipoComprobante,
	IDTipoProveedor
	--IDSucursal
					

	
End










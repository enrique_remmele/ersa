﻿
 CREATE Procedure [dbo].[SpViewEstadoCheque]
	--Entrada
	@IDBanco Int,
	@IDCuentaBancaria int,
	@NroDesde int,
	@NroHasta int,
	@EstadoCheque varchar(16) = null,
	@EstadoChequera varchar(16) = null,
	@OrderBy varchar(16) = 'NroCheque',
	@OrderFormat varchar (5) = 'Asc'
As

Begin
  
	--variables
	Declare @vID tinyint
	Declare @NroOp varchar(32)
	Declare @CuentaBancaria varchar(50)
	Declare @Banco varchar(50)
	Declare @NroCheque varchar(32)
	Declare @Fecha Date
	Declare @Importe money
	Declare @Moneda varchar(6)
	Declare @i int = @NroDesde
	declare @estado varchar(16)
	declare @ChequeraEstado varchar(16)

	Set @Banco = (Select B.Descripcion from Banco B join CuentaBancaria CB on Cb.IDBanco = B.Id where CB.Id = @IDCuentaBancaria)
	Set @Moneda = (Select Mon from VCuentaBancaria where ID = @IDCuentaBancaria)
	Set @CuentaBancaria = (Select CuentaBancaria from CuentaBancaria where id = @IDCuentaBancaria)

	Begin
		
	--Crear la tabla temporal
    Create table #TablaChequera(IdCuentaBancaria int,
								CuentaBancaria varchar(50),
								Banco varchar(50),
								Moneda varchar(6),
								NroCheque varchar (32),
								Importe money,
								Fecha date,
								Estado Varchar (16),
								NroOP int,
								ChequeraEstado varchar(16)
								)
										
	WHILE @i <= @NroHasta BEGIN
		
		set @estado = null
		set @Importe = 0
		set @Fecha = SYSDATETIME()
		set @NroOp = ''
		set @ChequeraEstado = ''

		Select top 1 @estado = CH.Anulado, @Importe = ISNULL(CH.ImporteMoneda,0), @Fecha = CH.Fecha, @NroOp = CH.Numero
		from Cheque CH
		where IDCuentaBancaria = @IDCuentaBancaria and NroCheque = cast(@i as varchar(100)) order by IDTransaccion desc

		select @ChequeraEstado = isnull(Estado,'') from Chequera where IDCuentaBancaria = @IDCuentaBancaria and @i between NroDesde and NroHasta

		if @estado = 1 begin
			set @estado = 'Anulado'
		end else if @estado = 0 begin
			set @estado = 'Utilizado'
		end else if @estado is null begin
			set @estado = 'En blanco'
		end

		Insert Into #TablaChequera(IdCuentaBancaria,CuentaBancaria,Banco,Moneda,NroCheque,Importe, Fecha, Estado, NroOP, ChequeraEstado)
		Values(@IDCuentaBancaria,@CuentaBancaria, @Banco,@Moneda,@i,@Importe, @Fecha,@estado,@NroOp, @ChequeraEstado)
		set @i = @i +1	
				 
		END
	
		if @EstadoCheque is null
		begin
			if @EstadoChequera is null begin
				if @OrderBy = 'Fecha'begin
					Select * From #TablaChequera order by Fecha
				end else if @OrderBy = 'NroCheque' begin
					Select * From #TablaChequera order by NroCheque
				end else if @OrderBy = 'Estado' begin
					Select * From #TablaChequera order by Estado
				end else begin
					Select * From #TablaChequera
				end
			end else begin
				if @OrderBy = 'Fecha'begin
					select * from #TablaChequera where ChequeraEstado = @EstadoChequera order by Fecha
				end else if @OrderBy = 'NroCheque' begin
					select * from #TablaChequera where ChequeraEstado = @EstadoChequera order by NroCheque
				end else if @OrderBy = 'Estado' begin
					select * from #TablaChequera where ChequeraEstado = @EstadoChequera order by Estado
				end else begin
					select * from #TablaChequera where ChequeraEstado = @EstadoChequera
				end
			end	
		end else begin
			if @EstadoChequera is null begin
				if @OrderBy = 'Fecha'begin
					Select * From #TablaChequera where estado = @EstadoCheque order by Fecha
				end else if @OrderBy = 'NroCheque' begin
					Select * From #TablaChequera where estado = @EstadoCheque order by NroCheque
				end else if @OrderBy = 'Estado' begin
					Select * From #TablaChequera where estado = @EstadoCheque order by Estado
				end else begin
					Select * From #TablaChequera where estado = @EstadoCheque
				end
			end else begin
				if @OrderBy = 'Fecha'begin
					select * from #TablaChequera where Estado = @EstadoCheque and ChequeraEstado = @EstadoChequera order by Fecha
				end else if @OrderBy = 'NroCheque' begin
					select * from #TablaChequera where Estado = @EstadoCheque and ChequeraEstado = @EstadoChequera order by NroCheque
				end else if @OrderBy = 'Estado' begin
					select * from #TablaChequera where Estado = @EstadoCheque and ChequeraEstado = @EstadoChequera order by Estado
				end else begin
					select * from #TablaChequera where Estado = @EstadoCheque and ChequeraEstado = @EstadoChequera
				end				
			end
		end
			
	End
End

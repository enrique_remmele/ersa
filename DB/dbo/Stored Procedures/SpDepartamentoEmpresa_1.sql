﻿CREATE Procedure [dbo].[SpDepartamentoEmpresa]

	--Entrada
	@ID tinyint,
	@Departamento varchar(50),
	@IDSucursal tinyint = 1,
	@IDUsuarioJefe int,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la Departamento ya existe
		if exists(Select * From DepartamentoEmpresa Where Departamento=@Departamento) begin
			set @Mensaje = 'La Departamento ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDDepartamentoEmpresa tinyint
		set @vIDDepartamentoEmpresa = (Select IsNull((Max(ID)+1), 1) From DepartamentoEmpresa)

		--Insertamos
		Insert Into DepartamentoEmpresa(ID, Departamento, IDUsuarioJefe,IDSucursal)
		Values(@vIDDepartamentoEmpresa, @Departamento, @IDUsuarioJefe,@IDSucursal)
		
		Insert Into aDepartamentoEmpresa(ID, Departamento, IDUsuarioJefe,IDSucursal,IDUsuarioModificacion,FechaModificacion, IDTerminalModificacion, Operacion)
		Values(@vIDDepartamentoEmpresa, @Departamento, @IDUsuarioJefe,@IDSucursal,@IDUsuario,GetDate(), @IDTerminal, @Operacion)		
		
		--select * from aDepartamentoEmpresa
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='DepartamentoEmpresa', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		if not exists(Select * From DepartamentoEmpresa Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la Departamento ya existe
		if exists(Select * From DepartamentoEmpresa Where Departamento=@Departamento And ID!=@ID) begin
			set @Mensaje = 'La Departamento ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update DepartamentoEmpresa Set Departamento=@Departamento, 
						IDUsuarioJefe=@IDUsuarioJefe,
						IDSucursal=@IDSucursal
		Where ID=@ID
		
		Insert Into aDepartamentoEmpresa(ID, Departamento, IDUsuarioJefe,IDSucursal,IDUsuarioModificacion,FechaModificacion, IDTerminalModificacion, Operacion)
		Values(@vIDDepartamentoEmpresa, @Departamento, @IDUsuarioJefe,@IDSucursal,@IDUsuario,GetDate(), @IDTerminal, @Operacion)			
				
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='DepartamentoEmpresa', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
				
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From DepartamentoEmpresa Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		
		Insert Into aDepartamentoEmpresa(ID, Departamento, IDUsuarioJefe,IDSucursal,IDUsuarioModificacion,FechaModificacion, IDTerminalModificacion, Operacion)
		Values(@vIDDepartamentoEmpresa, @Departamento, @IDUsuarioJefe,@IDSucursal,@IDUsuario,GetDate(), @IDTerminal, @Operacion)		
		
		--Eliminamos
		Delete From DepartamentoEmpresa 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='DepartamentoEmpresa', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
				
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿




CREATE Procedure [dbo].[SpNotaDebitoProveedorProcesar]

	--Entrada
	@IDTransaccion numeric(18),
	@Operacion varchar(10),
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
As	
	If @Operacion = 'INS' Begin
		Exec SpNotaDebitoProveedorActualizarStock @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output   
		Exec SpNotaDebitoProveedorActualizarCompra @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output     			
	
	--Damos el OK Nota Credito
		If @Procesado = 'True' Begin
			Update NotaDebitoProveedor   Set Procesado='True' Where IDTransaccion=@IDTransaccion
			Set @Mensaje = 'Registro procesado!'
			Return @@rowcount
		End Else Begin
	
			--Si hubo un error, eliminar todo el registro
			Set @Operacion = 'DEL'			
			
		End
	
	
	
	End
	
    
	If @Operacion = 'ANULAR' Begin
	
		Exec SpNotaDebitoProveedorActualizarStock @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output   
		Exec SpNotaDebitoProveedorActualizarCompra @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output  
		
	
	End
	
	If @Operacion  = 'DEL' Begin
	
		If (Select Anulado From NotaDebitoProveedor  Where IDTransaccion=@IDTransaccion) = 'True' Begin
			Set @Mensaje = 'Registro guardado'
			Set @Procesado = 'True'
			return @@rowcount
	
		Exec SpNotaDebitoProveedorActualizarStock @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output   
		Exec SpNotaDebitoProveedorActualizarCompra @IDTransaccion=@IDTransaccion , @Operacion=@Operacion,@Mensaje=@Mensaje output,@Procesado=@Procesado output     			
		
		Delete NotaDebitoProveedorCompra  Where IDTransaccionNotaDebitoProveedor=@IDTransaccion 
		
			
		End 
	END
	


﻿CREATE procedure [dbo].[SpETLCliente]

	@IDSucursal tinyint

As

begin
	
	Declare @vCodigoDistribuidor int
	Set @vCodigoDistribuidor = (Select CodigoDistribuidor From Sucursal Where ID=@IDSucursal)
	
	Select
	'CodDistribuidor'= dbo.FFormatoTamañoCaracter (15 , @vCodigoDistribuidor ),
	'CodCliente'= dbo.FFormatoTamañoCaracter (15, C.Referencia),
	'NombreCliente'= dbo.FFormatoTamañoCaracter (50, RazonSocial),
	'CodCluster'= dbo.FFormatoTamañoCaracter (15,IsNull(TC.Referencia, '')),
	'DescripCluster'= dbo.FFormatoTamañoCaracter (50,C.TipoCliente),
	'CuitCliente'= dbo.FFormatoTamañoCaracter (20,C.RUC),
	'DireccionCliente'= dbo.FFormatoTamañoCaracter (50,C.Direccion),
	'LocalidadCliente'= dbo.FFormatoTamañoCaracter (50,C.Barrio),
	'CiudadCliente'= dbo.FFormatoTamañoCaracter (50,Ciudad),
	'ProvinciaCliente'= dbo.FFormatoTamañoCaracter (50,Departamento),
	'CodPostalCliente'= dbo.FFormatoTamañoCaracter (10,'0000000000'),
	'NroTelefCliente'= dbo.FFormatoTamañoCaracter (20,C.Telefono),
	'RespComercialCliente'= dbo.FFormatoTamañoCaracter (30,Vendedor),
	'CodZonaCliente'= dbo.FFormatoTamañoCaracter (15, V.Referencia),
	'StatusCliente'= dbo.FFormatoTamañoCaracter (1, Case When(IDEStado) = 1 Then 'A' Else 'I' End),
	'FechaActivacion'= CONVERT(varchar(5), Year(FechaAlta)) + dbo.FFormatoDosDigitos(Month(FechaAlta)) + dbo.FFormatoDosDigitos(Day(FechaAlta))
	From VCliente C
	Left Outer Join TipoCliente TC On C.IDTipoCliente=TC.ID
	Left Outer Join Vendedor V On C.IDVendedor=V.ID
	
	Where C.IDSucursal=@IDSucursal
	
End

	






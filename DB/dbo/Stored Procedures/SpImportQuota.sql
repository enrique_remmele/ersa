﻿CREATE procedure [dbo].[SpImportQuota]

@IDSucursal int
as

Begin

	Select 
	'cdQuotaDef'='SxPH',
	'cdQuota'='R_'+Sucursal+'_VENDEDOR_'+''+convert(varchar(50),ID)+CONVERT(varchar(5),Year(GetDate())) + '-' + CONVERT(varchar(5), dbo.FFormatoDosDigitos( Month(GetDate()))) + '-' + CONVERT(varchar(5), dbo.FFormatoDosDigitos( Day(GetDate()))),
	'cdOrgUnit'='',
	'cdStoreClassif'='',
	'cdRegion'='R'+Sucursal+'_VENDEDOR_'+convert(varchar(50),ID),
	'cdProductClassif'='CM 00',
	'cdProduct'='',
	'vlQuota1'=0,
	'vlQuota2'=0,
	'vlQuota3'=0,
	'dtMonthYear'=CONVERT(varchar(5),Year(GetDate())) + '-' + CONVERT(varchar(5), dbo.FFormatoDosDigitos( Month(GetDate()))) + '-' + CONVERT(varchar(5), dbo.FFormatoDosDigitos( Day(GetDate()))),
	'cdStatus'=(Case When Estado=1 Then'ACT'Else 'DEACT'End)
	From VVendedor Where IDSucursal=@IDSucursal 

End







﻿--select * from AnticipoAplicacion where numero = 510
--exec SpAsientoAnticipoCliente 1107986
--Select 'Importe'=AV.Importe * AV.Cotizacion
--		From vDetalleAsiento DA
--		Join VVentaDetalleCobranzaCredito AV on DA.IDTransaccion = AV.IDTransaccion
--		Where AV.IDTransaccionAnticipo = 1107986
--		select * from VVentaDetalleCobranzaCredito
CREATE Procedure [dbo].[SpAsientoAnticipoCliente]
	@IDTransaccion numeric(18,0)
As
Begin
	--Variables
	Declare @vIDSucursal tinyint

	--Cobranzas
	Declare @vTipoComprobante varchar(50)
	Declare @vIDTipoComprobante smallint
	Declare @vComprobante varchar(50)
	Declare @vFecha date
	Declare @vIDMoneda tinyint
	Declare @vCotizacion money

	--Venta
	Declare @vCredito bit
	Declare @vContado bit
	Declare @vDiferenciaCambio bit

	--Efectivo
	Declare @vIDTipoComprobanteEfectivo TINYINT
	Declare @vIDTipoComprobanteTarjeta tinyint

	--Cheque
	Declare @vDiferido bit
	Declare @vIDCuentaBancaria int

	--Documento
	Declare @vIDTipoComprobanteDocumento tinyint

	--Detalle Asiento
	Declare @vID tinyint
	Declare @vIDCuentaContable int
	Declare @vCodigo varchar(50)
	Declare @vDebe bit
	Declare @vHaber bit
	Declare @vImporteDebe money
	Declare @vImporteHaber money
	Declare @vImporte money
	Declare @TotalDiferenciaCambio money
	declare @vCliente varchar(200)
	declare @vCotizacionAnticipo money

	Declare @vIDMonedaFormaPago as integer

	Declare @vIDTransaccionCobranza as integer
	Declare @vCotizacionCobranza money
	Declare @vIDTipoAnticipo as integer
	--Obtener valores
	Begin


		Select	@vIDSucursal=IDSucursalCobranza,
				@vFecha=Fecha,
				@vIDTipoComprobante=IDTipoComprobante,
				@vComprobante=NroComprobante,
				@vIDMoneda=IDMoneda,
				@vCotizacion=Cotizacion,
				@TotalDiferenciaCambio = DiferenciaCambio,
				@vIDTransaccionCobranza = IDTransaccionCobranza,
				@vCotizacionAnticipo = Cotizacion
		From VAnticipoAplicacion Where IDTransaccion=@IDTransaccion
		Set @vCotizacionCobranza = (Select isnull(Cotizacion,1) from CobranzaCredito where idtransaccion = @vIDTransaccionCobranza)
		Set @vIDTipoAnticipo = (Select isnull(IDTipoAnticipo,0) from CobranzaCredito where idtransaccion = @vIDTransaccionCobranza)
	End

	--Verificar que el asiento se pueda modificar
	Begin

		--Si esta conciliado
		If (Select ISNULL(Conciliado, 'False') From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta conciliado'
			GoTo salir
		End 	

		--Si esta procesado
		If (Select Procesado From AnticipoAplicacion Where IDTransaccion=@IDTransaccion) = 'False' Begin
			print 'El anticipo no es valido'
			GoTo salir
		End 	

		--Si esta anulado
		If (Select Anulado From AnticipoAplicacion Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El anticiop esta anulado'
			GoTo salir
		End 	

		--Si esta bloqueado
		If (Select Bloquear From Asiento Where IDTransaccion=@IDTransaccion) = 'True' Begin
			print 'El asiento esta bloquedado'
			GoTo salir
		End 	

		print 'Los datos son correctos!'

	End

	--Eliminar primero el asiento
	Begin

		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion

		print 'Registros de Asiento y Detalle eliminados!'

	End

	--Cargamos la Cabecera
	Begin

		Declare @vNumero int
		Declare @vDetalleAsiento varchar(50)

		set @vCliente = (select Cliente from VAnticipoAplicacion where IDTransaccion = @IDTransaccion)
		Set @vNumero = (Select ISNULL(MAx(Numero) + 1, 1) From Asiento)
		Set @vDetalleAsiento = 'Aplicacion de anticipo del ' + CONVERT(varchar(50), @vFecha) + ' de ' + @vCliente

		print 'Tipo de Comprobante:' + convert(varchar(50), @vTipoComprobante)
		print 'Comprobante:' + convert(varchar(50), @vComprobante)


		Insert Into Asiento(IDTransaccion, Numero, IDSucursal, Fecha, IDMoneda, Cotizacion, IDTipoComprobante, NroComprobante, Detalle, Total, Debito, Credito, Saldo, Anulado, IDCentroCosto, Conciliado)
		Values(@IDTransaccion, @vNumero, @vIDSucursal, @vFecha, @vIDMoneda, @vCotizacion, @vIDTipoComprobante, @vComprobante, @vDetalleAsiento, 0, 0, 0, 0, 'False', NULL, 'False')

		print 'Cabecera cargada!'

	End

	Begin

		--Variables
		Declare cCFVenta cursor for
		--Select Codigo, 'Debe'=0, 'Haber'=1, 'Importe'=AV.Importe * @vCotizacionAnticipo
		--From vDetalleAsiento DA
		--Join AnticipoVenta AV on DA.IDTransaccion = AV.IDTransaccionVenta
		--Where AV.IDTransaccionAnticipo = @IDTransaccion
		--and Da.Cuenta like '%Deudor%'
		Select Codigo, 'Debe'=0, 'Haber'=1, 'Importe'=AV.Importe * AV.Cotizacion
		From vDetalleAsiento DA
		Join VVentaDetalleCobranzaCredito AV on DA.IDTransaccion = AV.IDTransaccion
		Where AV.IDTransaccionAnticipo = @IDTransaccion
		and Da.Cuenta like '%Deudor%'
		
		Open cCFVenta   
		fetch next from cCFVenta into @vCodigo, @vDebe, @vHaber, @vImporte

		While @@FETCH_STATUS = 0 Begin  

			Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
			Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')
			print concat('@vIDCuentaContable: ' ,@vIDCuentaContable)
			--Set @vImporte = (Select SUM(Isnull(Importe,0)) From VVentaAnticipoAplicacionAsiento Where IDTransaccion=@IDTransaccion)

			If @vDebe = 1 Begin
				Set @vImporteDebe = @vImporte
				Set @vImporteHaber = 0
			End

			If @vHaber = 1 Begin
				Set @vImporteHaber = @vImporte
				Set @vImporteDebe = 0
			End				
			
			print concat('importe haber ',@vImporteHaber)
			print concat('importe debe ',@vImporteDebe)

			If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
				If Exists(Select * From DetalleAsiento Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo) Begin
					Update DetalleAsiento Set Credito=Credito+Round(@vImporteHaber,0),
												Debito=Debito+Round(@vImporteDebe,0),
												Importe=Importe+Round(@vImporte,0)
					Where IDTransaccion=@IDTransaccion And CuentaContable=@vCodigo
				End Else Begin
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, Round(@vImporteHaber,0), Round(@vImporteDebe,0), 0, '')
				End
			End

			fetch next from cCFVenta into @vCodigo, @vDebe, @vHaber, @vImporte

		End

		close cCFVenta 
		deallocate cCFVenta

	End

	--DiferenciaCambio
	begin
		Declare cCFDiferencia cursor for
		Select Codigo, Debe, Haber, Credito, Credito, DiferenciaCambio From VCFCobranzaVenta 
		Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda and DiferenciaCambio = 1 
		Open cCFDiferencia 
		fetch next from cCFDiferencia into @vCodigo, @vDebe, @vHaber, @vCredito, @vContado, @vDiferenciaCambio

		While @@FETCH_STATUS = 0 Begin  

			If @TotalDiferenciaCambio = 0  Begin
				set @vImporte = 0
			End

			set @vImporte = @TotalDiferenciaCambio
			If @TotalDiferenciaCambio < 0 Begin
				set @vImporte = @TotalDiferenciaCambio * -1
			End 

			If @TotalDiferenciaCambio < 0 Begin
				Set @vImporteDebe = @TotalDiferenciaCambio
				Set @vImporteHaber = 0
				set @vDebe = 1
				set @vHaber = 0
			End 

			If @TotalDiferenciaCambio > 0 Begin
				Set @vImporteDebe = 0
				Set @vImporteHaber = @TotalDiferenciaCambio
				set @vDebe = 0
				set @vHaber = 1
			End 
		  	
			
				Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

				--Set @vImporte = (Select SUM(Isnull(ImporteGS,0)) From VentaCobranza Where IDTransaccionCobranza=@IDTransaccion)
				If @vDebe = 1 Begin
					Set @vImporteDebe = @vImporte
				End

				If @vHaber = 1 Begin
					Set @vImporteHaber = @vImporte
				End				

				If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
				End

			--Diferencia de cambio cuando las formas de pagos tiene cotizaciones diferentes a la cobranza
			declare @DiferenciaCambioFP decimal
			set @DiferenciaCambioFP =  ISnull((select ((Total*Cotizacion) - (select sum(importe) from FormaPago where idtransaccion = VCobranzaCredito.IDTransaccion)) 
			from VCobranzaCredito where IDTransaccion = @IDTransaccion),0)
			
			If @DiferenciaCambioFP = 0  Begin
				set @vImporte = 0
			End

			set @vImporte = @DiferenciaCambioFP
			If @DiferenciaCambioFP < 0 Begin
				set @vImporte = @DiferenciaCambioFP * -1
			End 

			If @DiferenciaCambioFP > 0 Begin
				Set @vImporteDebe = @DiferenciaCambioFP
				Set @vImporteHaber = 0
				set @vDebe = 1
				set @vHaber = 0
			End 

			If @DiferenciaCambioFP < 0 Begin
				Set @vImporteDebe = 0
				Set @vImporteHaber = @DiferenciaCambioFP
				set @vDebe = 0
				set @vHaber = 1
			End 
		  	
			
				Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

				--Set @vImporte = (Select SUM(Isnull(ImporteGS,0)) From VentaCobranza Where IDTransaccionCobranza=@IDTransaccion)
				If @vDebe = 1 Begin
					Set @vImporteDebe = @vImporte
				End

				If @vHaber = 1 Begin
					Set @vImporteHaber = @vImporte
				End				

				If @vImporteHaber > 0 Or @vImporteDebe>0 Begin		
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, 'DIFERENCIA FORMA DE PAGO')
				End

				fetch next from cCFDiferencia into @vCodigo, @vDebe, @vHaber, @vCredito, @vContado, @vDiferenciaCambio

		End

		close cCFDiferencia
		deallocate cCFDiferencia

	End

	--
	begin
		Declare cCFDiferencia cursor for
		Select Codigo, Debe, Haber, Credito, Credito, DiferenciaCambio From VCFCobranzaVenta 
		Where IDSucursal=@vIDSucursal And IDMoneda=@vIDMoneda and AnticipoCliente = 1 
		Open cCFDiferencia 
		fetch next from cCFDiferencia into @vCodigo, @vDebe, @vHaber, @vCredito, @vContado, @vDiferenciaCambio

		While @@FETCH_STATUS = 0 Begin  
			
			set @vImporte = (Select  round((Total * @vCotizacionCobranza),0) from AnticipoAplicacion where IDTransaccion = @IDTransaccion)
								
			If @vImporte > 0 Begin
				Set @vImporteDebe = @vImporte
				Set @vImporteHaber = 0
				set @vDebe = 1
				set @vHaber = 0
			End 

			If @vImporte < 0 Begin
				Set @vImporteDebe = 0
				Set @vImporteHaber = @vImporte
				set @vDebe = 0
				set @vHaber = 1
			End 
		  	
			
				Set @vID = (Select ISNULL(Max(ID)+1,1) From DetalleAsiento Where IDTransaccion=@IDTransaccion)
				
				Set @vCodigo = (select Codigo from VTipoAnticipo where ID = @vIDTipoAnticipo)
				Set @vIDCuentaContable = (Select Top(1) ID From VCuentaContable Where Codigo=@vCodigo And PlanCuentaTitular='True')

				print concat('@vIDCuentaContable: ' ,@vIDCuentaContable)
				If @vDebe = 1 Begin
					Set @vImporteDebe = @vImporte
				End

				If @vHaber = 1 Begin
					Set @vImporteHaber = @vImporte
				End				

				If @vImporteHaber > 0 Or @vImporteDebe>0 Begin	
				print concat('@vIDCuentaContable: ' ,@vIDCuentaContable)	
					Insert Into DetalleAsiento(IDTransaccion, ID, IDCuentaContable, CuentaContable, Credito, Debito, Importe, Observacion)	
					Values(@IDTransaccion, @vID, @vIDCuentaContable, @vCodigo, @vImporteHaber, @vImporteDebe, 0, '')
				End

				fetch next from cCFDiferencia into @vCodigo, @vDebe, @vHaber, @vCredito, @vContado, @vDiferenciaCambio

		End

		close cCFDiferencia
		deallocate cCFDiferencia

	End

	--Actualizamos la cabecera, el total
	Set @vImporteHaber = IsNull((Select SUM(Credito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)
	Set @vImporteDebe = Isnull((Select SUM(Debito) From DetalleAsiento Where IDTransaccion=@IDTransaccion),0)

	Set @vImporteHaber = ROUND(@vImporteHaber, 0)
	Set @vImporteDebe = ROUND(@vImporteDebe, 0)

	Update Asiento Set Total = @vImporteHaber,
						Credito = @vImporteHaber,
						Debito = @vImporteDebe,
						Saldo = @vImporteHaber - @vImporteDebe
	Where IDTransaccion=@IDTransaccion

	--Por el momento solo actualiza la unidad de negocio y el centro de costo
	Execute SpDetalleAsientoActualizar @IDTransaccion = @IDTransaccion

Salir:

End


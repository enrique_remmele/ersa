﻿CREATE Procedure [dbo].[SpNotaDebito]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDPuntoExpedicion int = Null,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante int = NULL,			
	@IDCliente int = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDDepositoOperacion tinyint = NULL,
	@Fecha date = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money= NULL,
	@Observacion varchar(100) = NULL,
	@Total money = NULL,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@TotalDescuento money = NULL,
	@Aplicar bit = 'False',
	@Reposicion bit = 'False',
	@Debito bit = 'False',	
	@ConComprobantes bit = 'True',
	@Saldo money  = NULL,
			
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
	
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from NotaDebito where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin

		--Validar Fecha Timbrado 
		declare @fechaTimbrado as datetime
		select @fechaTimbrado = Vencimiento from PuntoExpedicion where Id = @IdPuntoExpedicion
		if @fechaTimbrado < @Fecha begin
			Set @Mensaje  = 'La fecha del timbrado ha vencido. Por favor seleccione otro numero de timbrado.'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		end

	--Comprobante fuera del Rango
		if @NroComprobante < (Select NumeracionDesde From PuntoExpedicion Where ID=@IDPuntoExpedicion) Or @NroComprobante > (Select NumeracionHasta From PuntoExpedicion Where ID=@IDPuntoExpedicion) Begin
			set @Mensaje = 'El numero de comprobante no esta dentro del rango correcto! Cambie el punto de expedicion o actualicelo.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Validar
		--Comprobante
		if Exists(Select * From NotaDebito  Where IDPuntoExpedicion=@IDPuntoExpedicion And NroComprobante=@NroComprobante) Begin
			set @Mensaje = 'El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--Calcular Saldo....
		--Si es Sin Comprobantes, SALDO = @TOTAL
		--Si no, SALDO = 0
		If @ConComprobantes = 'False' Begin
			Set @Saldo = @Total 		
		End Else Begin
			Set @Saldo = 0
		End
		
		--Insertar en Nota de Debito
		Insert Into NotaDebito  (IDTransaccion, IDPuntoExpedicion, IDTipoComprobante, NroComprobante, IDCliente, IDSucursal, IDDeposito,  Fecha, IDMoneda, Cotizacion, Observacion, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, Saldo, Anulado,FechaAnulado, IDUsuarioAnulado, Procesado, Aplicar, Reposicion, Debito,ConComprobantes )
		Values(@IDTransaccionSalida, @IDPuntoExpedicion, @IDTipoComprobante, @NroComprobante, @IDCliente,  @IDSucursalOperacion, @IDDeposito, @Fecha, @IDMoneda, @Cotizacion, @Observacion, @Total, @TotalImpuesto, @TotalDiscriminado, @TotalDescuento, @Saldo, 'False', NULL, NULL, 'False', @Aplicar,@Reposicion , @Debito ,@ConComprobantes )

		--Actualizar Punto de Expedicion
		Update PuntoExpedicion Set UltimoNumero=@NroComprobante
		Where ID=@IDPuntoExpedicion
											
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ANULAR
	If @Operacion = 'ANULAR' Begin
	
		--Validar
		
		----IDTransaccion
		--If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
		--	set @Mensaje = 'El sistema no encontro el registro solicitado.'
		--	set @Procesado = 'False'
		--	set @IDTransaccionSalida  = 0
		--	return @@rowcount
		--End
		
		----IDTransaccion
		--If Not Exists(Select * From NotaCredito Where IDTransaccion=@IDTransaccion) Begin
		--	set @Mensaje = 'El sistema no encontro el registro solicitado.'
		--	set @Procesado = 'False'
		--	return @@rowcount
		--End
		
		--Si tiene facturas aplicadas
		If Exists(Select * From NotaCreditoVentaAplicada Where IDTransaccionNotaCredito=@IDTransaccion) Begin
			set @Mensaje = 'La Nota de Credito tiene facturas aplicadas! Anule primeramente la aplicacion para continuar.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Si esta transferida
		If Exists(Select * From TransferenciaNotaCredito Where IDTransaccionNotaCredito=@IDTransaccion) Begin
			set @Mensaje = 'La Nota de Credito ya se uso para una transferencia de credito!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Asientos Cerrados
		If Exists(Select * From Asiento A Where A.IDTransaccion=@IDTransaccion And A.Conciliado='True') Begin
			set @Mensaje = 'El asiento de este documento ya esta conciliado! No se puede anular.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Actualizamos el Stock
		Exec SpNotaDebitoActualizarStock  @IDTransaccion = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje output, @Procesado=@Procesado output
		print @Procesado
		
		If @Procesado = 1 Begin
			set @Mensaje = 'No se puede anular'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Actualizamos de saldos de las ventas
		Exec SpNotaDebitoActualizarVanta @IDTransaccion = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje output, @Procesado=@Procesado output
		
		If @Procesado = 'False' Begin
			return @@rowcount
		End
				
		--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
		
		----Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from NotaDebito where IDTransaccion = @IDTransaccion)
		set @IDCliente = (select IDCliente from NotaDebito where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from NotaDebito where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from Venta where IDTransaccion = @IDTransaccion)
		declare @Comprobante as varchar(16) = (select Comprobante from vNotaDebito where IDTransaccion = @IDTransaccion)
		declare @VTipoComprobante varchar(16) = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from NotaDebito where IDTransaccion = @IDTransaccion)
		declare @Condicion varchar(16) = 'CREDITO'

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,@IDCliente,0,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ANU',@VTipoComprobante, @IDSucursal)


		----Anular
		Update NotaDebito  Set Anulado='True', IDUsuarioAnulado=@IDUsuario, FechaAnulado = GETDATE()
		Where IDTransaccion = @IDTransaccion
		
		--Actualizar el Cliente (Saldo Total, Saldo Credito, Ultima Compra)
		Set @IDCliente = (Select IDCliente From Venta Where IDTransaccion=@IDTransaccion)
		
		EXEC SpAcualizarClienteVenta
			@IDCliente = @IDCliente,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT
			
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
		
	End
	
	--ELIMNAR
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From NotaDebito Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Solo se puede eliminar si esta aulado
		If Not Exists(Select * From NotaDebito Where IDTransaccion=@IDTransaccion And Anulado='True') Begin
			set @Mensaje = 'El comprobante tiene que estar anulado para poder eliminar!'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from NotaDebito where IDTransaccion = @IDTransaccion)
		set @IDCliente = (select IDCliente from NotaDebito where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from NotaDebito where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from Venta where IDTransaccion = @IDTransaccion)
		set @Comprobante = (select Comprobante from vNotaDebito where IDTransaccion = @IDTransaccion)
		set @VTipoComprobante = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from NotaDebito where IDTransaccion = @IDTransaccion)
		set @Condicion = 'CREDITO'

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,@IDCliente,0,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ELI',@VTipoComprobante, @IDSucursal)

		--Eliminamos el detalle
		Delete DetalleNotaDebito   where IDTransaccion = @IDTransaccion				
		
		--Eliminamos el impuesto
		Delete DetalleImpuesto Where IDTransaccion = @IDTransaccion
		
		--Eliminamos el registro
		Delete NotaDebito   Where IDTransaccion = @IDTransaccion
						
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion				
				
		Set @Mensaje = 'Reistro guardado!'
		Set @Procesado = 'True'
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End







﻿CREATE Procedure [dbo].[SpCambiarDepositoTerminal]

	--Entrada
	@IDTerminal int,
	@IDDeposito int,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	Declare @IDSucursal int
	Set @IDSucursal=(Select IDSucursal From Deposito Where ID = @IDDeposito)		
		
	--Si el ID existe
	if not exists(Select * From Terminal Where ID=@IDTerminal) begin
		set @Mensaje = 'El sistema no encuentra el registro solicitado!'
		set @Procesado = 'False'
		return @@rowcount
	end
			
	--Actualizamos
	Update Terminal Set IDSucursal=@IDSucursal, 
						IDDeposito=@IDDeposito						
	Where ID=@IDTerminal
		
	set @Mensaje = 'Registro guardado!'
	set @Procesado = 'True'
	return @@rowcount
	
End



﻿CREATE Procedure [dbo].[SpVale]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Numero int = NULL,
	@NroComprobante int=NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDGrupo tinyint = 0, 
	@Fecha date = NULL,
	@IDMoneda tinyint = NULL,
	@Nombre  varchar(200) = NULL,
	@Cotizacion money = NULL,
	@Motivo varchar(200) = NULL,
	@Total money = NULL,
	@ARendir bit = NUll,
	@Pagado bit = NULL,
	
	
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin
	
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from Vale where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
		
		--Obtenemos el nuevo ID
		declare @vID numeric(18,0)
		set @vID = (Select IsNull((Max(ID)+1), 1) From DetalleFondoFijo )
		
		--Validar
		--Numero
		If Exists(Select * From Vale Where Numero=@Numero And IDSucursal=@IDSucursalOperacion) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Grupo
		If @IDGrupo = 0 Or @IDGrupo Is Null Begin
			set @Mensaje = 'Debe seleccionar un grupo para continuar!'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
	
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
			
		--Insertar
		Declare @vCancelado bit
		If @ARendir = 'False' Begin
			Set @vCancelado = 'False'
			Set @Pagado = 'False'
		End
		
		If @ARendir = 'True' Begin
			Set @vCancelado = 'False'
			Set @Pagado = 'False'
		End
		
		--Insertar el Registro 		
		Insert Into Vale (IDTransaccion,Numero,NroComprobante ,Nombre,IDSucursal, IDGrupo, Fecha, IDMoneda, Anulado,FechaAnulado, IDUsuarioAnulado, Cotizacion, Motivo, Total, ARendir, Saldo, Cancelado,Pagado)
		Values(@IDTransaccionSalida,@Numero,@NroComprobante,@Nombre,@IDSucursalOperacion,@IDGrupo,@Fecha,@IDMoneda,'False',Null, Null, @Cotizacion, @Motivo, @Total, @ARendir, @Total, @vCancelado,@Pagado)
		
		--Insertar el vale en DetalleFondoFijo si ARendir=False
		If @ARendir = 'False' Begin
			Insert Into DetalleFondoFijo(IDSucursal,IDGrupo,ID,IDTransaccionVale,IDTransaccionGasto,Importe,Cancelado,IDTransaccionRendicionFondoFijo)
			values(@IDSucursal,@IDGrupo,@vID,@IDTransaccionSalida,0,@Total,'False',NULL)
		End
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		If Exists(Select * From Vale Where Numero=@Numero And IDSucursal=@IDSucursalOperacion And Anulado='True') Begin
			set @Mensaje = 'El sistema detecto que el vale esta anulado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Si es que ya esta relacionado
		If Exists(Select * From GastoVale Where IDTransaccionVale=@IDTransaccion) Begin
			set @Mensaje = 'El registro esta relacionado con gasto.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Asientos Cerrados
		If Exists(Select * From Asiento A Where A.IDTransaccion=@IDTransaccion And A.Conciliado='True') Begin
			set @Mensaje = 'El asiento de este documento ya esta conciliado! No se puede anular.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		If @ARendir = 'False' Begin
			Set @vCancelado = 'False'
			Set @Pagado = 'False'
		End
		
		If @ARendir = 'True' Begin
			Set @vCancelado = 'True'
			Set @Pagado = 'True'
		End
											
		--Actualizamos
		Update Vale Set IDSucursal=@IDSucursal,
						IDGrupo=@IDGrupo,
						Numero=@Numero,
						Nombre=@Nombre,
						NroComprobante=@NroComprobante,
						Fecha=@Fecha,
						IDMoneda=@IDMoneda, 
						Cotizacion=@Cotizacion,
						Total=@Total,
						Saldo=@Total,
						Motivo=@Motivo,
						ARendir=@ARendir,
						Cancelado=@vCancelado,
						Pagado=@Pagado
						
		Where IDTransaccion=@IDTransaccion
		
			
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
					
	end
	
	If @Operacion = 'ANULAR' Begin

		--Si es que ya esta relacionado
		If Exists(Select * From GastoVale Where IDTransaccionVale=@IDTransaccion) Begin
			set @Mensaje = 'El registro esta relacionado con gasto.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Asientos Cerrados
		If Exists(Select * From Asiento A Where A.IDTransaccion=@IDTransaccion And A.Conciliado='True') Begin
			set @Mensaje = 'El asiento de este documento ya esta conciliado! No se puede anular.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
		
		----Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		----Anular
		Update Vale  Set Anulado='True', IDUsuarioAnulado=@IDUsuario, FechaAnulado = GETDATE()
		Where IDTransaccion = @IDTransaccion
		
		
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		Set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
		
	End
End

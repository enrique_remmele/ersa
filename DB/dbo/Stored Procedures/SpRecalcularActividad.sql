﻿CREATE Procedure [dbo].[SpRecalcularActividad]
	
	--Entrada
	@Desde date,
	@Hasta date
	
As

Begin
	
	Declare @vTotal money
	Declare @vIDTransaccion numeric(18,0)
	Declare @vIDProducto int
	Declare @vID int
	Declare @vIDDescuento int
	Declare @vIDTipoDescuento int
	Declare @vIDActividad int
	Declare @vFecha date
	Declare @vIDTransaccionPlanilla numeric(18,0)
	Declare @vProducto varchar(100)
	Declare @vReferencia varchar(100)
	Declare @vActividad varchar(100)
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	
	Set @vIDTipoDescuento = 1
	
	Declare db_cursor cursor for
	Select D.IDTransaccion, D.IDProducto, D.ID, D.IDDescuento, D.Total, V.FechaEmision  From VDescuento D Join DetalleVenta DV On D.IDTransaccion=DV.IDTransaccion And D.IDProducto=DV.IDProducto And D.ID=DV.ID Join Venta V On DV.IDTransaccion=V.IDTransaccion
	Where (V.FechaEmision Between @Desde And @Hasta) And V.Anulado='False' 
	And D.IDTipoDescuento=@vIDTipoDescuento
	
	Open db_cursor   
	Fetch Next From db_cursor Into @vIDTransaccion, @vIDProducto, @vID, @vIDDescuento, @vTotal, @vFecha
	While @@FETCH_STATUS = 0 Begin 
		
		Set @vProducto = IsNull((Select Descripcion From Producto Where ID=@vIDProducto), '---')
		Set @vReferencia = IsNull((Select Referencia From Producto Where ID=@vIDProducto),'0')
		
		print 'Producto: ' + convert(varchar(50), @vIDProducto) + '	' + @vReferencia + '	' + @vProducto + '	' +  convert(varchar(50), @vTotal)
				
		Set @vIDActividad = IsNull((Select A.ID From DetalleActividad DA Join Actividad A On DA.IDActividad=A.ID Join DetallePlanillaDescuentoTactico DPDT On A.ID=DPDT.IDActividad Join PlanillaDescuentoTactico PDT On DPDT.IDTransaccion=PDT.IDTransaccion
							 Where (@vFecha Between PDT.Desde And PDT.Hasta) And DA.IDProducto=@vIDProducto),0)
		
		If @vIDActividad > 0 Begin
			
			Set @vActividad=(Select Codigo From Actividad Where ID=@vIDActividad)
			
			--Actualizamos los descuentos
			Update Descuento Set IDActividad=@vIDActividad
			Where IDTransaccion=@vIDTransaccion And IDProducto=@vIDProducto And ID=@vID And IDDescuento=@vIDDescuento And IDTipoDescuento=@vIDTipoDescuento			
			
		End
		 		
		Fetch Next From db_cursor Into @vIDTransaccion, @vIDProducto, @vID, @vIDDescuento, @vTotal, @vFecha
									
	End
	
	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor
	
Salir:
	
End





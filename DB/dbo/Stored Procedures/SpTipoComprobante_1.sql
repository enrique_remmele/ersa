﻿CREATE Procedure [dbo].[SpTipoComprobante]

	--Entrada
	@ID smallint,
	@Tipo varchar(10),
	@IDOperacion int,
	@Descripcion varchar(50),
	@Codigo varchar(10),
	@Resumen varchar(20) = NULL,
	@Estado bit,

	--Opciones
	@ComprobanteTimbrado bit = 'False',
	@CalcularIVA bit = 'False',
	@IVAIncluido bit = 'False',
	@LibroVenta bit = 'False',
	@LibroCompra bit = 'False',
	@Signo char(1) = '+',
	@Autonumerico bit = 'True',
	@Exento bit = 'False',
	--JGR 20140910
	@EsInterno bit = 'False',
	@Deposito bit = 'False',
	@Restar bit = 'False',

	--Hechauka
	@HechaukaTipoDocumento	varchar(50),
	@HechaukaTimbradoReemplazar	bit,
	@HechaukaNumeroTimbrado	varchar(50),

	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,

	@Operacion varchar(10),

	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--VARIABLES
	Declare @vCliente bit
	Declare @vProveedor bit

	Set @vCliente = 'False'
	Set @vProveedor = 'False'

	--BLOQUES

	--INSERTAR
	if @Operacion='INS' begin

		--Validar
		--Descripcion dentro de la misma operacion
		If Exists(Select * From TipoComprobante Where Descripcion=@Descripcion And IDOperacion=@IDOperacion) Begin
			set @Mensaje = 'La descripcion ya existe para esta operacion!'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Codigo dentro de la misma operacion
		If Exists(Select * From TipoComprobante Where Codigo=@Codigo And IDOperacion=@IDOperacion) Begin
			set @Mensaje = 'El codigo ya existe para esta operacion!'
			set @Procesado = 'False'
			return @@rowcount
		End


		--Obtenemos el nuevo ID
		declare @vID tinyint
		set @vID = (Select IsNull((Max(ID)+1), 1) From TipoComprobante)

		--Establecemos el tipo
		if @Tipo = 'CLIENTE' Set @vCliente='True'
		if @Tipo = 'PROVEEDOR' Set @vProveedor='True'

		--Insertamos
		Insert Into TipoComprobante(ID, Cliente, Proveedor, IDOperacion, Descripcion, Codigo, Resumen, Estado, ComprobanteTimbrado, CalcularIVA, IVAIncluido, LibroVenta, LibroCompra, Signo, Autonumerico, HechaukaTipoDocumento, HechaukaTimbradoReemplazar, HechaukaNumeroTimbrado, EsInterno, Deposito,Restar,Exento)
		Values(@vID, @vCliente, @vProveedor, @IDOperacion, @Descripcion, @Codigo,@Resumen,@Estado, @ComprobanteTimbrado,@CalcularIVA,@IVAIncluido,@LibroVenta,@LibroCompra,@Signo, @Autonumerico, @HechaukaTipoDocumento, @HechaukaTimbradoReemplazar, @HechaukaNumeroTimbrado, @EsInterno,@Deposito,@Restar,@Exento)

		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TIPO DE COMPROBANTE', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID

		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount

	end

	--ACTUALIZAR
	if @Operacion='UPD' begin

		--Validar
		--Si el sistema no encuentra el registro
		If Not Exists(Select * From TipoComprobante Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Descripcion dentro de la misma operacion
		If Exists(Select * From TipoComprobante Where Descripcion=@Descripcion And IDOperacion=@IDOperacion And ID!=@ID) Begin
			set @Mensaje = 'La descripcion ya existe para esta operacion!'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Codigo dentro de la misma operacion
		If Exists(Select * From TipoComprobante Where Codigo=@Codigo And IDOperacion=@IDOperacion And ID!=@ID) Begin
			set @Mensaje = 'El codigo ya existe para esta operacion!'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Establecemos el tipo
		if @Tipo = 'CLIENTE' Set @vCliente='True'
		if @Tipo = 'PROVEEDOR' Set @vProveedor='True'

		--Actualizamos
		Update TipoComprobante Set	Cliente=@vCliente, 
									Proveedor=@vProveedor, 
									IDOperacion=@IDOperacion, 
									Descripcion=@Descripcion, 
									Codigo=@Codigo, 
									Resumen=@Resumen, 
									Estado=@Estado, 
									ComprobanteTimbrado=@ComprobanteTimbrado, 
									CalcularIVA=@CalcularIVA, 
									IVAIncluido=@IVAIncluido, 
									LibroVenta=@LibroVenta, 
									LibroCompra=@LibroCompra, 
									Signo=Signo,
									Autonumerico=@Autonumerico,
									HechaukaTipoDocumento=@HechaukaTipoDocumento, 
									HechaukaTimbradoReemplazar=@HechaukaTimbradoReemplazar, 
									HechaukaNumeroTimbrado=@HechaukaNumeroTimbrado ,
									EsInterno = @EsInterno,
									Deposito = @Deposito,
									Restar = @Restar,
									Exento = @Exento
		Where ID=@ID

		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TIPO DE COMPROBANTE', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID

		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount

	end

	--ELIMINAR
	if @Operacion='DEL' begin

		--Validar
		--Movimientos
		If Exists(Select * From Movimiento Where IDTipoComprobante=@ID) Begin
			set @Mensaje = 'El registro tiene movimientos realizados! No se puede eliminar'
			set @Procesado = 'True'
			return @@rowcount
		End

		--Ventas
		If Exists(Select * From Venta Where IDTipoComprobante=@ID) Begin
			set @Mensaje = 'El registro tiene ventas realizadas! No se puede eliminar'
			set @Procesado = 'True'
			return @@rowcount
		End

		--Eliminamos
		Delete From TipoComprobante 
		Where ID=@ID

		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='TIPO DE COMPROBANTE', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID

		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount

	end

	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End

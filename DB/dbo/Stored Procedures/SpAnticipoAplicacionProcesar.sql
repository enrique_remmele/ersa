﻿CREATE Procedure [dbo].[SpAnticipoAplicacionProcesar]

	--Entrada
	@IDTransaccion numeric(18,0),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin Tran Transaccion Begin
Begin Try

	Set @Mensaje = 'No procesado'
	Set @Procesado = 'False'
		
	--Validar Feha Operacion
	Declare @vIDSucursal as integer
	Declare @vFecha as Date
	Declare @vIDUsuario as integer
	Declare @vIDOperacion as integer

	Select	@vIDSucursal=IDSucursal,
			@vFecha=Fecha,
			@vIDUsuario=IDUsuario,
			@vIDOperacion=IDOperacion
	 From Transaccion Where ID = @IDTransaccion

--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@vIDSucursal, @vFecha, @vIDUsuario, @vIDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'

			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @vFecha = (Select Fecha from AnticipoAplicacion where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@vIDSucursal, @vFecha, @vIDUsuario, @vIDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'

			Return @@rowcount
		End
	End 

	--Aplicar Cobranza
	If @Operacion = 'INS' Begin
	
		--Procesamos las Ventas y Saldo del Cliente
		Set @Mensaje = 'Recalcular los saldos de las ventas'
		--print @Mensaje
		EXEC SpVentaAnticipo @IDTransaccionAnticipo = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Si no se proceso, anular la operacion
		If @Procesado = 0 Begin
			RollBack Tran Transaccion
			Set @Mensaje = 'No se pudo saldar las ventas!'
			Set @Procesado = 'False'
			--print @Mensaje
			GoTo EliminarError
		End
		
		Set @Mensaje = 'Saldos de ventas recalculados.'
		
		--Damos el OK a la Cobranza
		If @Procesado = 1 Begin
			
			Set @Mensaje = 'Procesamos la aplicacion de venta a anticipo.'
			--print @Mensaje
			Update AnticipoAplicacion Set Procesado='True' 
			Where IDTransaccion=@IDTransaccion
			--5
			--Generar Asiento
			Exec SpAsientoAnticipoCliente @IDTransaccion=@IDTransaccion
			
			Set @Mensaje = 'Asiento generado...'
			--print @Mensaje
			
			Set @Mensaje = 'Registro procesado!'
			Set @Procesado = 'True'
			GoTo Salir
						
		End
				
	End
	
	If @Operacion = 'DEL' Begin
		
		--Procesar
				
		--Eliminar relacion de las ventas asociadas (Actualizar saldos de la venta y cliente)
		Delete From AnticipoVenta Where IDTransaccionAnticipo=@IDTransaccion
		
		--Eliminamos el registro
		Delete AnticipoAplicacion Where IDTransaccion = @IDTransaccion
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion
		
		GoTo Salir
		
	End
	
Salir:
	
	--Actualizar saldos de clientes
	Begin
	
		Declare @vIDTransaccionVenta numeric(18,0)
		Declare @vIDCliente int
		
		Declare db_cursor cursor for
		Select V.IDCliente From AnticipoVenta VC Join Venta V On VC.IDTransaccionVenta=V.IDTransaccion 
		Where IDTransaccionAnticipo=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDCliente
		While @@FETCH_STATUS = 0 Begin 
			
			Exec SpAcualizarSaldoCliente @IDCliente=@vIDCliente
			
			Fetch Next From db_cursor Into @vIDCliente
										
		End
		
		Close db_cursor   
		Deallocate db_cursor
		
	End
	
	--Establecemos los procesos
	Commit Tran Transaccion 
	Return @@rowcount
	
End Try
Begin Catch
	
	Set @Mensaje = ERROR_MESSAGE()
	Set @Procesado = 'False'
	Set @Mensaje = 'Saldar ventas...'
	print 'Error: ' + ERROR_MESSAGE()
	RollBack Tran Transaccion
	RAISERROR(@Mensaje, 16, 0)		 
	
	GoTo EliminarError
	
End Catch

EliminarError:
	
	--Eliminamos todo el registro si hubo un error en la inserccion
	If @Operacion = 'INS' Begin				
		--Eliminar relacion de las ventas asociadas (Actualizar saldos de la venta y cliente)
		Delete From AnticipoVenta Where IDTransaccionAnticipo = @IDTransaccion
		
		--Eliminamos el registro
		Delete AnticipoAplicacion Where IDTransaccion = @IDTransaccion
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion
		Set @Procesado = 'False'
		--print @Mensaje
		
		--Establecemos los procesos
		Return @@rowcount
					
	End
	
End


﻿CREATE Procedure [dbo].[SpProductoClasificacion]

	--Entrada
	--Datos Obligatorios
	@ID int,
	@Operacion varchar(10),
	
	--Identificadores
	@IDClasificacion smallint = NULL,
	
	--LOG	
	@IDUsuario smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As
	
Begin

	--BLOQUES
	Set @Mensaje = 'No se proceso!'
	Set @Procesado = 'False'
	
	--ACTUALIZAR
	If @Operacion='UPD' Begin
	
		--Actualizamos
		--Identificadores
		Update Producto Set IDClasificacion=@IDClasificacion
		Where ID=@ID
				
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'False'
					
	End
	
	return @@rowcount
	
End


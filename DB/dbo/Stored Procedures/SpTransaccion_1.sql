﻿CREATE Procedure [dbo].[SpTransaccion]

	--CARLOS 21-02-2014
	
	--Entrada
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	@IDOperacion int,

	--Beta, sacar si algo sale mal
	@MostrarTransaccion bit = 'True',
	
	@IDTransaccionCaja numeric(18,0)=0,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccion numeric(18,0) output
As

Begin

	--Validar
	--Usuario
	
	--Sucursal
	
	---Deposito
	
	--Terminal
	
	--Obtener el nuevo ID
	Set @IDTransaccion = (Select ISNULL((Select MAX(ID + 1) From Transaccion),1))
	
	--Todo OK, Insertamos
	Insert Into Transaccion(ID, Fecha, IDUsuario, IDSucursal, IDDeposito, IDTerminal, IDOperacion, IDTransaccionCaja)
	values(@IDTransaccion, GETDATE(), @IDUsuario, @IDSucursal, @IDDeposito, @IDTerminal, @IDOperacion, @IDTransaccionCaja)
	
	--Auditoria
	Declare @vTabla varchar(50)
	Set @vTabla = (Select Descripcion From Operacion Where ID=@IDOperacion)
	
	Exec SpLogSuceso @Operacion='INS', @Tabla=@vTabla, @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @IDTransaccion=@IDTransaccion
	
	If @MostrarTransaccion = 'True' Begin	
		Select 'IDTransaccion'=@IDTransaccion
	End
			
	Set @Mensaje = 'Registro cargado!'
	Set @Procesado = 'True'
	return @@rowcount

End


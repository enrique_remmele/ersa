﻿CREATE Procedure [dbo].[SpCambioGlobalCodigoCuentaContable]

	--Entrada
	@Desde date,
	@Hasta date,
	@CodigoOriginal varchar(50),
	@CodigoNuevo varchar(50),
	
	@IncluirConciliado bit,
	@IDOperacion int = NULL,
	@IDSucursal int = NULL
	
As

Begin
	
	Declare @vMensaje varchar(200)
	Declare @vProcesado bit
	Declare @vIDTransaccion numeric(18,0)
	Declare @vID int
	Declare @vConciliado bit
	Declare @vIDOperacion int
	Declare @vIDSucursal int
	Declare @vIDCuentaContableOriginal int
	Declare @vIDCuentaContableNuevo int
	Declare @vIDPlanCuenta int
	Declare @vRegistrosProcesados int = 0

	Set @vMensaje = 'No se proceso'
	Set @vProcesado = 'False'
	
	--Validar
	Begin
	
		--Hayar la cuenta contable
		Set @vIDCuentaContableOriginal = (Select IsNull((Select ID From VCuentaContable Where Codigo = @CodigoOriginal And PlanCuentaTitular = 'True'),0))
		Set @vIDCuentaContableNuevo = (Select IsNull((Select ID From VCuentaContable Where Codigo = @CodigoNuevo And PlanCuentaTitular = 'True'),0))
		
		--Validar Cuenta
		If @vIDCuentaContableOriginal = 0 Begin
			Set @vMensaje = 'La cuenta contable original no existe!'
			Set @vProcesado = 'False'
			Goto Salir	
		End
		
		If @vIDCuentaContableNuevo = 0 Begin
			Set @vMensaje = 'La cuenta contable nuevo no existe!'
			Set @vProcesado = 'False'
			Goto Salir	
		End
	End
	
	--Recorrer
	Declare db_cursor cursor for
	Select DA.IDTransaccion, DA.ID, A.Conciliado, A.IDSucursal, T.IDOperacion 
	From DetalleAsiento DA 
	Join Asiento A on DA.IDTransaccion=A.IDTransaccion 
	Join Transaccion T On DA.IDTransaccion=T.ID
	Where (A.Fecha Between @Desde And @Hasta) And DA.IDCuentaContable=@vIDCuentaContableOriginal
	Open db_cursor   
	Fetch Next From db_cursor Into @vIDTransaccion, @vID, @vConciliado, @vIDSucursal, @vIDOperacion
	While @@FETCH_STATUS = 0 Begin 
		
		--Incluir Conciliado
		If @vConciliado = 'True' And @IncluirConciliado = 'False' Begin
			GoTo Seguir			
		End
		
		--Tipo de Operacion
		If @IDOperacion Is Not Null Begin
			If @IDOperacion != @vIDOperacion Begin
				GoTo Seguir			
			End
		End
		
		--Sucursal
		If @IDSucursal Is Not Null Begin
			If @IDSucursal != @vIDSucursal Begin
				GoTo Seguir			
			End
		End
		
		--Actualizar
		Update DetalleAsiento Set IDCuentaContable=@vIDCuentaContableNuevo,
								 CuentaContable=@CodigoNuevo
		Where IDTransaccion=@vIDTransaccion And ID=@vID

		Set @vRegistrosProcesados = @vRegistrosProcesados + 1

Seguir:			
		Fetch Next From db_cursor Into @vIDTransaccion, @vID, @vConciliado, @vIDSucursal, @vIDOperacion
									
	End
	
	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor
	
	Set @vMensaje = 'Registro guardado'
	Set @vProcesado = 'True'
	
Salir:
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado, 'Registros'=@vRegistrosProcesados
End



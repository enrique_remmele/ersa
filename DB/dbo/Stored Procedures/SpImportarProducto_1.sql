﻿CREATE Procedure [dbo].[SpImportarProducto]

	--Entrada
	--Identificadores
	@Producto varchar(200) = NULL,
	@CodigoBarra varchar(200) = NULL,
	@Referencia varchar(200) = NULL,
	
	--Configuraciones
	@UnidadCaja decimal(10,5) = 0,
	@Peso varchar(50) = '0',
	@Volumen varchar(50) = '0',
	@Zona1 varchar(50),
	@Zona2 varchar(50),
	
	@Impuesto varchar(50) = '10.00',
	@Estado bit = 'True',
	@ControlarExistencia bit = 'False',
	@CodigoCuentaContableCompra varchar(50) = '11122001',
	@CodigoCuentaContableCosto varchar(50) = '544020403',
	@Costo varchar(50) = '0',
	@Procedencia varchar(100) = '',
	@ProcedenciaCodigo varchar(100) = '',
	@Actualizar bit
	
As

Begin

	Declare @vID int
	Declare @vOperacion varchar(50)
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	Declare @vIDImpuesto tinyint
	Declare @vIDTipoProducto tinyint
	Declare @vIDZona1 int
	Declare @vIDZona2 int

	Set	@vID = 0
	Set @vOperacion = 'INS'
	Set @vMensaje = 'Sin proceso'
	Set @vProcesado = 'False'
		
	If Exists(Select * From Producto Where Referencia=@Referencia) And @Actualizar='True' Begin
		Set @vID = (Select Top(1) ID From Producto Where Referencia=@Referencia)		
		Set @vOperacion = 'UPD'
	End 
	
	Set @vIDZona1 = (select top(1) isnull(id,0) from zonadeposito where descripcion like concat('%',@Zona1,'%'))
	Set @vIDZona2 = (select top(1) isnull(id,0) from zonadeposito where descripcion like concat('%',@Zona2,'%'))

	Set @Peso = '0' + @Peso
	Set @Impuesto = '0' + @Impuesto
	Set @Costo = '0' + @Costo
	
	Set @Peso = REPLACE(@Peso, ',','.')
	Set @Impuesto = REPLACE(@Impuesto, ',','.')
	Set @Impuesto = CONVERT(varchar(50), convert(decimal(10,2),@Impuesto))
	Set @Costo = REPLACE(@Costo, ',','.')
	Set @Costo = CONVERT(varchar(50), convert(money,@Costo))
	
	--Impuesto
	Begin
		If @Impuesto = '10.00' Begin
			Set @vIDImpuesto = 1
		End
		
		If @Impuesto = '5.00' Begin
			Set @vIDImpuesto = 2
		End
		
		If @Impuesto = '0.00' Begin
			Set @vIDImpuesto = 3
		End
		
	End
	
	----Cuenta Contable
	--Begin
		
	--	--Unilever
	--	If @ProcedenciaCodigo = '001' Begin
	--		Set @CodigoCuentaContableVenta = '411'
	--	End
		
	--	--Conti
	--	If @ProcedenciaCodigo = '002' Begin
	--		Set @CodigoCuentaContableVenta = '412'
	--	End
		
	--End
	
	----Tipo de Producto
	--If Exists(Select Top(1) ID From TipoProducto Where Descripcion=@TipoProducto) Begin
	--	Set @vIDTipoProducto = (Select Top(1) ID From TipoProducto Where Descripcion=@TipoProducto)
	--End

	EXEC SpProducto
	@ID = @vID,
	@Descripcion = @Producto,
	@CodigoBarra = @CodigoBarra,
	@Referencia = @Referencia,
	
	@IDUnidadMedida = 1,
	@UnidadPorCaja = @UnidadCaja,
	@Peso = @Peso,
	@Volumen = @Volumen,
	
	@IDImpuesto = @vIDImpuesto,
	@Estado = 'True',
	@ControlarExistencia = 'True',
	
	@CuentaContableCompra = @CodigoCuentaContableCompra,
	@CuentaContableCosto = @CodigoCuentaContableCosto,
	@IDUsuario = 1,
	@IDTerminal = 1,
	@IDTipoProducto = 14,
		
	@Operacion = @vOperacion,
	
	@Mensaje = @vMensaje OUTPUT,
	@Procesado = @vProcesado OUTPUT

	--Actualizar el Costo
	If (Select ISNull(CostoCG,0) FRom Producto Where ID=@vID) = 0 Begin
		If @vOperacion = 'UPD' Begin
			Update Producto Set CostoCG=@Costo,
								CostoPromedio=@Costo,
								Estado=@Estado
			Where ID=@vID
		End
	End

	--if @vIDZona1 > 0 begin
	--	If not Exists(Select * From ProductoZona Where IDSucursal=4 and IDDeposito=48 And IDZonaDeposito=@vIDZona1 And IDProducto=@vID) begin
	--		Insert Into ProductoZona(IDSucursal, IDDeposito, IDZonaDeposito, IDProducto)
	--		Values(4, 48, @vIDZona1, @vID)			
	--	end
	--end

	--if @vIDZona2 > 0 begin
	--	If not Exists(Select * From ProductoZona Where IDSucursal=4 and IDDeposito=48 And IDZonaDeposito=@vIDZona2 And IDProducto=@vID) begin
	--		Insert Into ProductoZona(IDSucursal, IDDeposito, IDZonaDeposito, IDProducto)
	--		Values(4, 48, @vIDZona2, @vID)		
	--	end
	--end
	
	Select 'Mensaje'=@vMensaje, 'Procesado'=@vProcesado	
	
End

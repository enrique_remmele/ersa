﻿CREATE Procedure [dbo].[SpUnidadMedida]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Referencia varchar(5) = NULL,
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion='INS' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From UnidadMedida Where Descripcion=@Descripcion) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDUnidadMedida tinyint
		set @vIDUnidadMedida = (Select IsNull((Max(ID)+1), 1) From UnidadMedida)

		--Insertamos
		Insert Into UnidadMedida(ID, Descripcion, Referencia, Estado)
		Values(@vIDUnidadMedida, @Descripcion, @Referencia, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='UNIDAD DE MEDIDA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion='UPD' begin
		
		--Si el ID existe
		If not exists(Select * From UnidadMedida Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From UnidadMedida Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = 'La descripcion ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Actualizamos
		Update UnidadMedida Set Descripcion=@Descripcion,
								Referencia=@Referencia,
								Estado = @Estado	
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='UNIDAD DE MEDIDA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion='DEL' begin
		
		--Si el ID existe
		if not exists(Select * From UnidadMedida Where ID=@ID) begin
			set @Mensaje = 'El sistema no encuentra el registro solicitado!'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Si tiene una relacion con Productos
		if exists(Select * From Producto Where IDUnidadMedida=@ID) begin
			set @Mensaje = 'El registro tiene productos asociados! No se puede eliminar.'
			set @Procesado = 'False'
			return @@rowcount
		end
		
		--Eliminamos
		Delete From UnidadMedida 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla='UNIDAD DE MEDIDA', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
			
	end
	
	set @Mensaje = 'No se realizo ninguna operacion!'
	return @@rowcount

End


﻿CREATE Procedure [dbo].[SpViewKardexProductoResumen]

	--Entrada
	@Fecha1 date,
	@Fecha2 date,
	@IDProducto int = 0,
	@IDTipoProducto int = 0,
	@IDLinea int = 0,
	@IDSubLinea int = 0,
	@IDSubLinea2 int = 0

As

Begin
	
	
	--Variables
	Begin
		declare @vIDKardex int
		declare @vIndice int
		declare @vFecha DateTime
		
		--Transaccion
		declare @vOperacion varchar(50)
		declare @vIDTransaccion int
		declare @vIDProducto int
		declare @vIDTipoComprobante int
		declare @vID int

		declare @vOrden int
		declare @vIDSucursal int
		declare @vComprobante varchar(100)
		declare @vObservacion varchar(200)

		--Saldos Iniciales
		declare @vCantidadInicial decimal(10,2)
		declare @vCostoPromedioPonderadoInicial money
		declare @vTotalValorInicial decimal(18,6)
		declare @vTotalPesoInicial decimal(18,3)

		--Compra/Produccion
		declare @vCantidadCompraProduccion decimal(10,2)
		declare @vCostoUnitarioCompraProduccion money
		declare @vTotalCompraProduccion decimal(18,6)
		declare @vTotalPesoCompraProduccion decimal(18,3)

		--Entrada
		declare @vCantidadEntrada decimal(10,2)
		declare @vCostoUnitarioEntrada money
		declare @vTotalEntrada decimal(18,6)
		declare @vTotalPesoEntrada decimal(18,3)

		--Salida
		declare @vCantidadSalida decimal(10,2)
		declare @vCostoUnitarioSalida money
		declare @vTotalSalida decimal(18,6)
		declare @vTotalPesoSalida decimal(18,3)

		--Venta/Consumo
		declare @vCantidadVentaConsumo decimal(10,2)
		declare @vCostoUnitarioVentaConsumo money
		declare @vTotalVentaConsumo decimal(18,6)
		declare @vTotalPesoVentaConsumo decimal(18,3)

		--Saldos
		declare @vCantidadSaldo decimal(10,2)
		declare @vTotalSaldo money
		declare @vCostoPromedio decimal(18,6)
		declare @vTotalPesoSaldo decimal(18,3)
		declare @vIDKardexAnterior int
		
		--Producto
		declare @vReferencia varchar(50)
		declare @vProducto varchar(100)
		declare @vTipoProducto varchar(50)
		declare @vIDTipoProducto int
		declare @vLinea varchar(50)
		declare @vIDLinea int
		declare @vSubLinea varchar(50)
		declare @vIDSubLinea int
		declare @vSubLinea2 varchar(50)
		declare @vIDSubLinea2 int
		declare @vPesoUnitario decimal(18,3)
		
	End
		--Crear la tabla temporal
		create table #TablaTemporal(ID int,
									IDKardex int,
									Indice int,
									Fecha DateTime,
		
									--Transaccion
									Operacion varchar(50),
									TipoComprobante varchar(100),
									Numero varchar(20),

									IDTransaccion int,
									IDProducto int,
									--Producto
									Referencia varchar(50),
									Producto varchar(100),
									TipoProducto varchar(50),
									IDTipoProducto int,
									Linea varchar(50),
									IDLinea int,
									SubLinea varchar(50),
									IDSubLinea int,
									SubLinea2 varchar(50),
									IDSubLinea2 int,
									PesoUnitario decimal(18,3),

									Orden int,
									IDSucursal int,
									Comprobante varchar(100),
									Observacion varchar(200),

									CantidadInicial decimal(10,2),
									CostoPromedioPonderadoInicial money,
									TotalValorInicial decimal(18,6),
									TotalPesoInicial decimal(18,3),

									--Compra/Produccion
									CantidadCompraProduccion decimal(10,2),
									CostoUnitarioCompraProduccion money,
									TotalCompraProduccion decimal(18,6),
									TotalPesoCompraProduccion decimal(18,3),

									--Entrada
									CantidadEntrada decimal(10,2),
									CostoUnitarioEntrada money,
									TotalEntrada decimal(18,6),
									TotalPesoEntrada decimal(18,3),

									--Salida
									CantidadSalida decimal(10,2),
									CostoUnitarioSalida money,
									TotalSalida decimal(18,6),
									TotalPesoSalida decimal(18,3),

									--Venta/Consumo
									CantidadVentaConsumo decimal(10,2),
									CostoUnitarioVentaConsumo money,
									TotalVentaConsumo decimal(18,6),
									TotalPesoVentaConsumo decimal(18,3),

									--Saldos
									CantidadSaldo decimal(10,2),
									TotalSaldo money,
									CostoPromedio decimal(18,6),
									CostoPromedioOperacion decimal(18,6),
									TotalPesoSaldo decimal(18,3),
									IDKardexAnterior int
		
									)

																	
		set @vID = (Select IsNull(MAX(ID)+1,1) From #TablaTemporal)
		--Insertar datos	
		Begin
				
			Declare db_cursor cursor for
			
			Select 
			--Producto
			IDProducto,
			Referencia,
			Producto,
			TipoProducto,
			IDTipoProducto,
			Linea,
			IDLinea,
			SubLinea,
			IDSubLinea,
			SubLinea2,
			IDSubLinea2,
			PesoUnitario,
			
			--Compra/Produccion
			Sum(CantidadCompraProduccion),
			Sum(TotalCompraProduccion),
			'TotalPesoCompraProduccion'= PesoUnitario *Sum(CantidadCompraProduccion),

			--Entrada
			Sum(CantidadEntrada),
			Sum(TotalEntrada),
			'TotalPesoEntrada'= PesoUnitario *Sum(CantidadEntrada),

			--Salida
			Sum(CantidadSalida),
			Sum(TotalSalida),
			'TotalPesoSalida'= PesoUnitario *Sum(CantidadSalida),

			--Venta/Consumo
			Sum(CantidadVentaConsumo),
			Sum(TotalVentaConsumo),
			'TotalPesoVentaConsumo'= PesoUnitario *Sum(CantidadCompraProduccion)

			From VKardexInforme
			Where Fecha between @Fecha1 and @Fecha2
			and (Case When @IDProducto = 0 then 0 else IDProducto end) = (Case When @IDProducto = 0 then 0 else @IDProducto end)
			and (Case When @IDTipoProducto = 0 then 0 else IDTipoProducto end) = (Case When @IDTipoProducto = 0 then 0 else @IDTipoProducto end)
			and (Case When @IDLinea = 0 then 0 else IDLinea end) = (Case When @IDLinea = 0 then 0 else @IDLinea end)
			and (Case When @IDSubLinea = 0 then 0 else IDSubLinea end) = (Case When @IDSubLinea = 0 then 0 else @IDSubLinea end)
			and (Case When @IDSubLinea2 = 0 then 0 else IDSubLinea2 end) = (Case When @IDSubLinea2 = 0 then 0 else @IDSubLinea2 end)
		
			Group by IDProducto,Referencia,Producto,TipoProducto,IDTipoProducto,Linea,IDLinea,SubLinea,IDSubLinea,SubLinea2,IDSubLinea2, PesoUnitario
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDProducto,@vReferencia,@vProducto,@vTipoProducto,@vIDTipoProducto,@vLinea,@vIDLinea,@vSubLinea,@vIDSubLinea,@vSubLinea2,@vIDSubLinea2,@vPesoUnitario,@vCantidadCompraProduccion,@vTotalCompraProduccion,@vTotalPesoCompraProduccion,@vCantidadEntrada,@vTotalEntrada,@vTotalPesoEntrada, @vCantidadSalida, @vTotalSalida, @vTotalPesoSalida, @vCantidadVentaConsumo, @vTotalVentaConsumo, @vTotalPesoVentaConsumo

				While @@FETCH_STATUS = 0 Begin 
					
					--Iniciales
					SET @vCantidadInicial = (Select TOP(1) CantidadSaldo from Kardex where idproducto = @vIDProducto AND Fecha < @Fecha1 order by indice desc)
					SET @vCostoPromedioPonderadoInicial = (Select TOP(1) CostoPromedio from Kardex where idproducto = @vIDProducto AND Fecha < @Fecha1 order by indice desc)
					SET @vTotalValorInicial = (Select TOP(1) TotalSaldo from Kardex where idproducto = @vIDProducto AND Fecha < @Fecha1 order by indice desc) 
					Set @vTotalPesoInicial = @vCantidadInicial * @vPesoUnitario
					--Saldos
					SET @vCantidadSaldo = (Select TOP(1) CantidadSaldo from Kardex where idproducto = @vIDProducto AND Fecha <= @Fecha2 order by indice desc)
					SET @vTotalSaldo =(Select TOP(1) TotalSaldo from Kardex where idproducto = @vIDProducto AND Fecha <= @Fecha2 order by indice desc)
					SET @vCostoPromedio =(Select TOP(1) CostoPromedio from Kardex where idproducto = @vIDProducto AND Fecha <= @Fecha2 order by indice desc)
					Set @vTotalPesoSaldo = @vCantidadSaldo * @vPesoUnitario

					Insert Into  #TablaTemporal(IDProducto,Referencia,Producto,TipoProducto,IDTipoProducto,Linea,IDLinea,SubLinea,IDSubLinea,SubLinea2,IDSubLinea2,PesoUnitario,CantidadInicial,TotalValorInicial, TotalPesoinicial, CostoPromedioPonderadoInicial,CantidadCompraProduccion,TotalCompraProduccion, TotalPesoCompraProduccion, CantidadEntrada,TotalEntrada, TotalPesoEntrada, CantidadSalida,TotalSalida, TotalPesoSalida, CantidadVentaConsumo, TotalVentaConsumo,TotalPesoVentaConsumo, CantidadSaldo,TotalSaldo, TotalPesoSaldo, CostoPromedio) 
									Values (@vIDProducto,@vReferencia,@vProducto,@vTipoProducto,@vIDTipoProducto,@vLinea,@vIDLinea,@vSubLinea,@vIDSubLinea,@vSubLinea2,@vIDSubLinea2,@vPesoUnitario,@vCantidadInicial,@vTotalValorInicial, @vTotalPesoinicial, @vCostoPromedioPonderadoInicial, @vCantidadCompraProduccion,@vTotalCompraProduccion, @vTotalPesoCompraProduccion, @vCantidadEntrada,@vTotalEntrada, @vTotalPesoEntrada, @vCantidadSalida, @vTotalSalida, @vTotalPesoSalida, @vCantidadVentaConsumo, @vTotalVentaConsumo, @vTotalPesoVentaConsumo, @vCantidadSaldo,@vTotalSaldo, @vTotalPesoSaldo, @vCostoPromedio)
				
				Fetch Next From db_cursor Into @vIDProducto,@vReferencia,@vProducto,@vTipoProducto,@vIDTipoProducto,@vLinea,@vIDLinea,@vSubLinea,@vIDSubLinea,@vSubLinea2,@vIDSubLinea2,@vPesoUnitario,@vCantidadCompraProduccion,@vTotalCompraProduccion,@vTotalPesoCompraProduccion,@vCantidadEntrada,@vTotalEntrada,@vTotalPesoEntrada, @vCantidadSalida, @vTotalSalida, @vTotalPesoSalida, @vCantidadVentaConsumo, @vTotalVentaConsumo, @vTotalPesoVentaConsumo
				
			End
			
			--Cierra el cursor
			Close db_cursor   
			Deallocate db_cursor		   			
			
		End	

		Select * From #TablaTemporal 
	
	
	

End

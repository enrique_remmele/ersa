﻿CREATE Procedure [dbo].[SpControlClientesSainCast]

As

Begin
		Declare @vFechaOperacion DateTime
		Declare @vCodigo varchar(50)
		Declare @vFecha DateTime
		Declare @vLatitud decimal(18, 16)
		Declare @vLongitud decimal(18, 16)
		Declare @vSincronizado bit
		Declare @vObservacionSincronizacion varchar(200)
		Declare @vLocal varchar(50)
		Declare @vPlataforma varchar(50)
		set @vFechaOperacion = getdate()
		
	Begin
				
		Declare db_cursor1 cursor for
		
		--Control Matriz (CAST vs SAIN)
		with t1 as(
			select cc.codigo--, 'castlatitud'=cc.latitud, 'castlongitud'=cc.longitud
			from cast_cliente cc
			where cc.sucursal = 0 

			except

			select cc.codigo--,'sainlatitud'= c.Latitud, 'sainlongitud'= c.Longitud
			from cast_cliente cc
			join Cliente c on cc.codigo =c.Referencia
		)
		select  cc.codigo, cc.fecha, cc.latitud, cc.longitud, cc.sincronizado, cc.ObservacionSincronizacion,'Local'= 'MATRIZ','Plataforma'= 'CAST'
		from t1 t
		join cast_cliente cc on t.codigo = cc.codigo 

		--Ingresar al cursor la información
        Open db_cursor1   
		Fetch Next From db_cursor1 Into	@vCodigo,@vFecha,@vLatitud,@vLongitud,@vSincronizado,@vObservacionSincronizacion,@vLocal,@vPlataforma
		While @@FETCH_STATUS = 0 Begin 

			Insert Into ControlClientesSainCast(FechaOperacion, Codigo,Fecha,Latitud,Longitud,Sincronizado,ObservacionSincronizacion,Local,Plataforma)
		                       	      Values (@vFechaOperacion, @vCodigo,@vFecha,@vLatitud,@vLongitud,@vSincronizado,@vObservacionSincronizacion,@vLocal,@vPlataforma)

		    Fetch Next From db_cursor1 Into	@vCodigo,@vFecha,@vLatitud,@vLongitud,@vSincronizado,@vObservacionSincronizacion,@vLocal,@vPlataforma
		End
			
		--Cierra el cursor
		Close db_cursor1   
		Deallocate db_cursor1		   			
		
	End	

	Begin
				
		Declare db_cursor2 cursor for
		--Control Matriz (SAIN vs CAST)
		with t2 as (
			select cc.codigo--,'sainlatitud'= c.Latitud, 'sainlongitud'= c.Longitud
			from cast_cliente cc
			join Cliente c on cc.codigo =c.Referencia

			except

			select cc.codigo--, 'castlatitud'=cc.latitud, 'castlongitud'=cc.longitud
			from cast_cliente cc
			where cc.sucursal = 0 
		)
		select  cc.codigo, cc.fecha, cc.latitud, cc.longitud, cc.sincronizado, cc.ObservacionSincronizacion,'Local'= 'MATRIZ','Plataforma'= 'SAIN'
		from t2 t
		join cast_cliente cc on t.codigo = cc.codigo 

		--Ingresar al cursor la información
        Open db_cursor2   
		Fetch Next From db_cursor2 Into	@vCodigo,@vFecha,@vLatitud,@vLongitud,@vSincronizado,@vObservacionSincronizacion,@vLocal,@vPlataforma
		While @@FETCH_STATUS = 0 Begin 

			Insert Into ControlClientesSainCast(FechaOperacion, Codigo,Fecha,Latitud,Longitud,Sincronizado,ObservacionSincronizacion,Local,Plataforma)
			Values (@vFechaOperacion, @vCodigo,@vFecha,@vLatitud,@vLongitud,@vSincronizado,@vObservacionSincronizacion,@vLocal,@vPlataforma)

		    Fetch Next From db_cursor2 Into	@vCodigo,@vFecha,@vLatitud,@vLongitud,@vSincronizado,@vObservacionSincronizacion,@vLocal,@vPlataforma
		End
			
		--Cierra el cursor
		Close db_cursor2   
		Deallocate db_cursor2		   			
		
	End	

	Begin
				
		Declare db_cursor3 cursor for
		--Control Sucursal (CAST vs SAIN)
		with t3 as(
			select cc.codigo--, cc.sucursal, 'castlatitud'=cc.latitud, 'castlongitud'=cc.longitud
			from cast_cliente cc
			where cc.sucursal <> 0
			and cc.codigo like '%-%'
             
			except 

			select cc.codigo--, cc.sucursal, 'castlatitud'=cc.latitud, 'castlongitud'=cc.longitud
			from cast_cliente cc
			join vClienteSucursal c on cc.codigo like concat(c.Referencia,'%') 
									and cc.sucursal = c.id
			where cc.codigo like '%-%'
		)
		select cc.codigo, cc.fecha, cc.latitud, cc.longitud, cc.sincronizado, cc.ObservacionSincronizacion,'Local'= 'SUCURSAL','Plataforma'= 'CAST'
		from t3 t
		join cast_cliente cc on t.codigo = cc.codigo 
		
		--Ingresar al cursor la información
        Open db_cursor3   
		Fetch Next From db_cursor3 Into	@vCodigo,@vFecha,@vLatitud,@vLongitud,@vSincronizado,@vObservacionSincronizacion,@vLocal,@vPlataforma
		While @@FETCH_STATUS = 0 Begin 

			Insert Into ControlClientesSainCast(FechaOperacion, Codigo,Fecha,Latitud,Longitud,Sincronizado,ObservacionSincronizacion,Local,Plataforma)
			Values (@vFechaOperacion, @vCodigo,@vFecha,@vLatitud,@vLongitud,@vSincronizado,@vObservacionSincronizacion,@vLocal,@vPlataforma)

		    Fetch Next From db_cursor3 Into	@vCodigo,@vFecha,@vLatitud,@vLongitud,@vSincronizado,@vObservacionSincronizacion,@vLocal,@vPlataforma
		End
			
		--Cierra el cursor
		Close db_cursor3   
		Deallocate db_cursor3		   			
		
	End	
	Begin
				
		Declare db_cursor4 cursor for
		--Control Sucursal (SAIN vs Cast)
		with t4 as(
			select cc.codigo--, cc.sucursal, 'castlatitud'=cc.latitud, 'castlongitud'=cc.longitud
			from cast_cliente cc
			join vClienteSucursal c on cc.codigo like concat(c.Referencia,'%') 
									and cc.sucursal = c.id
			where cc.codigo like '%-%'

		except

		select cc.codigo--, cc.sucursal, 'castlatitud'=cc.latitud, 'castlongitud'=cc.longitud
		from cast_cliente cc
		where cc.sucursal <> 0
		and cc.codigo like '%-%'
		)
		select cc.codigo, cc.fecha, cc.latitud, cc.longitud, cc.sincronizado, cc.ObservacionSincronizacion,'Local'= 'SUCURSAL','Plataforma'= 'SAIN'
		from t4 t
		join cast_cliente cc on t.codigo = cc.codigo 
		
		--Ingresar al cursor la información
        Open db_cursor4   
		Fetch Next From db_cursor4 Into	@vCodigo,@vFecha,@vLatitud,@vLongitud,@vSincronizado,@vObservacionSincronizacion,@vLocal,@vPlataforma
		While @@FETCH_STATUS = 0 Begin 

			Insert Into ControlClientesSainCast(FechaOperacion, Codigo,Fecha,Latitud,Longitud,Sincronizado,ObservacionSincronizacion,Local,Plataforma)
			Values (@vFechaOperacion, @vCodigo,@vFecha,@vLatitud,@vLongitud,@vSincronizado,@vObservacionSincronizacion,@vLocal,@vPlataforma)

		    Fetch Next From db_cursor4 Into	@vCodigo,@vFecha,@vLatitud,@vLongitud,@vSincronizado,@vObservacionSincronizacion,@vLocal,@vPlataforma
		End
			
		--Cierra el cursor
		Close db_cursor4   
		Deallocate db_cursor4		   			
		
	End	

End
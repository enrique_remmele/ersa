﻿CREATE Procedure [dbo].[SpVenta2]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDPuntoExpedicion int = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante numeric(18, 0) = NULL,
	@Comprobante varchar(50) = NULL,
	@IDCliente int = NULL,
	@IDSucursalCliente tinyint = NULL,
	@Direccion varchar(100) = NULL,
	@Telefono varchar(50) = NULL,
	@IDVendedor tinyint = NULL,	
	@Fecha date = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDDepositoOperacion tinyint = NULL,
	@Credito bit = NULL,
	@FechaVencimiento date = NULL,
	@IDListaPrecio tinyint = NULL,
	@Descuento numeric(2,0) = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money = NULL,
	@Observacion varchar(200) = NULL,
	@NroComprobanteRemision varchar(50) = NULL,
	@Total money = 0,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@TotalDescuento money = NULL,
	
	@EsVentaSucursal bit = 'False',
	@Operacion varchar(50),
	
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		If Exists(Select * From Venta Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El codigo de transaccion ya existe! Favor genere nuevamente.'
			RAISERROR (@Mensaje, 11,1)
			--return @@rowcount
		End
		
		------Insertar
		Declare @vSaldo money
		Declare @vCancelado bit
		Set @vSaldo = @Total
		Set @vCancelado = 'False'
		
		--Insertar el Registro de Venta		
		Insert Into Venta(IDTransaccion, IDPuntoExpedicion, IDTipoComprobante, NroComprobante, Comprobante, IDCliente, IDSucursalCliente, Direccion, Telefono, FechaEmision, IDSucursal, IDDeposito, Credito, FechaVencimiento, IDListaPrecio, Descuento, IDMoneda, Cotizacion, Observacion, NroComprobanteRemision, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, Cobrado, Descontado, Saldo, Cancelado, Despachado, EsVentaSucursal, Anulado, FechaAnulado, IDUsuarioAnulado, IDVendedor)
		Values(@IDTransaccion, @IDPuntoExpedicion, @IDTipoComprobante, @NroComprobante, @Comprobante, @IDCliente, @IDSucursalCliente, @Direccion, @Telefono, @Fecha, @IDSucursalOperacion, @IDDepositoOperacion, @Credito, @FechaVencimiento, @IDListaPrecio, @Descuento, @IDMoneda, @Cotizacion, @Observacion, @NroComprobanteRemision, @Total, @TotalImpuesto, @TotalDiscriminado, @TotalDescuento, 0, 0, @vSaldo, @vCancelado, 'False', @EsVentaSucursal, 'False', NULL, NULL, @IDVendedor)
								
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	
	return @@rowcount
		
End








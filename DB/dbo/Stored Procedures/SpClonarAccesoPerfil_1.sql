﻿CREATE Procedure [dbo].[SpClonarAccesoPerfil]

	--Entrada
	@IDPerfil tinyint,
	@IDPerfilClonar tinyint,
	
	--Auditoria
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDTerminal int
	
As

Begin

	--Primero Eliminamos todos los accesos
	Delete From AccesoPerfil Where IDPerfil=@IDPerfilClonar
	
	--Variables
	declare @vIDMenu numeric(18,0)
	declare @vVisualizar bit
	declare @vAgregar bit
	declare @vModificar bit
	declare @vEliminar bit
	declare @vAnular bit
	declare @vImprimir bit
	declare @vNombreControl varchar(200)
	
	--Ahora recorremos el cursor del perfil matriz e insertamo
	--con el nuevo a clonar
	Declare db_cursor cursor for
	Select IDMenu, Visualizar, Agregar, Modificar, Eliminar, Anular, Imprimir, NombreControl From AccesoPerfil Where IDPerfil=@IDPerfil
	Open db_cursor   
	Fetch Next From db_cursor Into @vIDMenu, @vVisualizar, @vAgregar, @vModificar, @vEliminar, @vAnular, @vImprimir, @vNombreControl
	While @@FETCH_STATUS = 0 Begin  
	
		Insert Into AccesoPerfil(IDMenu, IDPerfil, Visualizar, Agregar, Modificar, Eliminar, Anular, Imprimir,NombreControl)
		Values(@vIDMenu, @IDPerfilClonar, @vVisualizar, @vAgregar, @vModificar, @vEliminar, @vAnular, @vImprimir,@vNombreControl)
		
		Fetch Next From db_cursor Into @vIDMenu, @vVisualizar, @vAgregar, @vModificar, @vEliminar, @vAnular, @vImprimir, @vNombreControl
		
	End
	
	Close db_cursor   
	Deallocate db_cursor		   			
	
End


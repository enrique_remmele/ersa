﻿CREATE Procedure [dbo].[SpEnviarCorreo]

	--Entrada
	@IDUsuario int,
	@Destinatarios varchar(500),
	@CC varchar(500),
	@Asunco varchar(50),
	@Cuerpo varchar(1000),
	@Contraseña varchar(50),
	@ID int output,
	@Mensaje varchar(200) ='' output,
	@Procesado bit ='False' output
As

Begin
		Declare @vIDPerfilMail as int = 0
		Declare @vIDCuentaMail as int = 0
		Declare @vPerfilMail as varchar(100) = ''
		Declare @vDireccionCorreoUsuario as varchar(100)= ''

		Set @vDireccionCorreoUsuario = (Select Email from usuario where id = @IDUsuario)
		Set @vIDCuentaMail = (Select top(1) account_id FROM msdb.dbo.sysmail_account where email_address = @vDireccionCorreoUsuario)
		Set @vIDPerfilMail = (SELECT top(1) profile_id FROM msdb.dbo.sysmail_profileaccount where account_id = @vIDCuentaMail)
		Set @vPerfilMail = (SELECT name FROM msdb.dbo.sysmail_profile where profile_id = @vIDPerfilMail)
		

		exec SpActualizarDatosCorreo @Correo = @vDireccionCorreoUsuario, @Contraseña = @Contraseña



		EXECUTE msdb.dbo.sp_send_dbmail 
		@profile_name = @vPerfilMail, 
		@recipients = @Destinatarios, 
		@copy_recipients = @CC,
		@subject = @Asunco, 
		@body = @Cuerpo,
		@mailitem_id = @ID output


		Select @ID

		--set @Mensaje = 'Registro guardado!'
		--set @Procesado = 'True'
		--return @@rowcount
			
End


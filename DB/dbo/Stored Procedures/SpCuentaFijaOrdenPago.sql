﻿CREATE Procedure [dbo].[SpCuentaFijaOrdenPago]

	--Entrada
	@ID tinyint = NULL,
	@IDCuentaContable smallint = NULL,
	@Debe bit = NULL,
	@Haber bit = NULL,
	@IDTipoComprobante smallint = NULL,
	@IDMoneda tinyint = NULL,
	@TipoProveedor bit = NULL,
	@TipoCheque bit = NULL,
	@TipoEfectivo bit = NULL,
	@Orden tinyint = NULL,
	@Descripcion varchar(50) = NULL,
	@BuscarEnProveedor bit = NULL,
	@BuscarCuentaBancaria bit = NULL,
	
	--Operacion
	@Operacion varchar(10) = NULL,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
As

Begin

	
	--INSERTAR
	If @Operacion='INS' Begin
	
		--Validar
		If Exists(Select * From CuentaFijaOrdenPago Where IDCuentaContable=@IDCuentaContable And IDTipoComprobante=@IDTipoComprobante And IDMoneda=@IDMoneda And TipoProveedor=@TipoProveedor And TipoCheque=@TipoCheque And TipoEfectivo=@TipoEfectivo And Debe=@Debe And Haber=@Haber) Begin
			set @Mensaje = 'El registro ya existe!'
			set @Procesado = 'false'
			return @@rowcount
		End
		
		--Insertar
		Declare @vID tinyint
		Set @vID = (Select ISNULL((Max(ID)+1), 1) From CuentaFijaOrdenPago)
		
		Insert Into CuentaFijaOrdenPago(ID, IDCuentaContable, Debe, Haber, IDTipoComprobante, IDMoneda, TipoProveedor, TipoCheque, TipoEfectivo, Orden, Descripcion, BuscarProveedor, BuscarCuentaBancaria )
		Values(@vID, @IDCuentaContable, @Debe, @Haber, @IDTipoComprobante, @IDMoneda, @TipoProveedor, @TipoCheque, @TipoEfectivo, @Orden, @Descripcion, @BuscarenProveedor, @BuscarCuentaBancaria )
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End	

	--ACTUALIZAR
	If @Operacion='UPD' Begin
	
		--Validar
		If Not Exists(Select * From CuentaFijaOrdenPago Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro!'
			set @Procesado = 'True'
			return @@rowcount		
		End
		
		--El mismo tipo no debe existir
		If Exists(Select * From CuentaFijaOrdenPago Where IDCuentaContable=@IDCuentaContable And IDTipoComprobante=@IDTipoComprobante And IDMoneda=@IDMoneda And TipoProveedor=@TipoProveedor And TipoCheque=@TipoCheque And TipoEfectivo=@TipoEfectivo And Debe=@Debe And Haber=@Haber And ID!=ID) Begin
			set @Mensaje = 'El tipo ya esta cargado en esta configuracion!'
			set @Procesado = 'True'
			return @@rowcount		
		End
		
		--Actualizar
		Update CuentaFijaOrdenPago Set	IDCuentaContable=@IDCuentaContable,
									Debe=@Debe,
									Haber=@Haber,
									IDTipoComprobante=@IDTipoComprobante,
									IDMoneda=@IDMoneda,
									TipoProveedor=@TipoProveedor, 
									TipoCheque=@TipoCheque, 
									TipoEfectivo=@TipoEfectivo,
									Orden=@Orden,
									Descripcion=@Descripcion,
									BuscarProveedor=@BuscarEnProveedor,
									BuscarCuentaBancaria=@BuscarCuentaBancaria
		Where ID=@ID 
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	--ELIMINAR
	If @Operacion='DEL' Begin
	
		--Validar
		If Not Exists(Select * From CuentaFijaOrdenPago Where ID=@ID) Begin
			set @Mensaje = 'El sistema no encuentra el registro!'
			set @Procesado = 'True'
			return @@rowcount		
		End
		
		Delete From CuentaFijaOrdenPago Where ID=@ID
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
	End	
	
End


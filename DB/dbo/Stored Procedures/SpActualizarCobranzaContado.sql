﻿CREATE Procedure [dbo].[SpActualizarCobranzaContado]

	--Entrada
	@IDTransaccion int,
	@Comprobante varchar(50),
	@Observacion varchar(150),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin
	
	--Si la cobranza esta anulada
	If Exists(Select * From CobranzaContado Where IDTransaccion=@IDTransaccion And Anulado='True') Begin
		set @Mensaje = 'La cobranza ya está anulada! No se puede modificar.'
		set @Procesado = 'False'
		return @@rowcount
	End
		
			
	--Actualizar 
	Update CobranzaContado  Set NroComprobante=@Comprobante,
								Observacion =@Observacion 
	Where IDTransaccion=@IDTransaccion 
	
			
	set @Mensaje = 'Registro guardado'
	set @Procesado = 'True'
	return @@rowcount
		

End
	
	


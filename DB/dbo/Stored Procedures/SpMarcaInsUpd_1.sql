﻿CREATE Procedure [dbo].[SpMarcaInsUpd]

	--Entrada
	@Descripcion varchar(100)
	
As

Begin

	Declare @vID int
	
	If @Descripcion = '' Begin
		Set @Descripcion = '---'
	End
	
	--Si existe
	If Exists(Select * From Marca Where Descripcion = @Descripcion) Begin
		Set @vID = (Select Top(1) ID From Marca Where Descripcion = @Descripcion)
		
	End Else Begin
		Set @vID = (Select IsNull(Max(ID)+1,1) From Marca)
		Insert Into Marca(ID, Descripcion, Estado)
		Values(@vID, @Descripcion, 'True')
	End
	
	return @vID
	
End


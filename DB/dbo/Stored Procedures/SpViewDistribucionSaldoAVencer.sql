﻿CREATE Procedure [dbo].[SpViewDistribucionSaldoAVencer]

	--Entrada
	@Fecha1 Date,
	@Fecha2 Date,
	@Fecha3 Date,
	@Fecha4 Date,
	@Fecha5 Date,
	@Fecha6 Date,
	@Fecha7 Date,
	@Fecha8 Date,
	@Fecha9 Date,


	--Salida
    @Mensaje varchar(200) output ,
	@IDTransaccionSalida numeric(18,0) output
As

Begin
    
    
	
	--Declarar variables 
	declare @vID tinyint
	declare @vIDTransaccion numeric (18,0)
	declare @vRazonSocial varchar (100)
	declare @vSaldo money
	declare @vFechaVencimiento date
	declare @vTotal money

	declare @vFecha1 money
	declare @vFecha2 money
	declare @vFecha3 money
	declare @vFecha4 money
	declare @vFecha5 money
	declare @vFecha6 money
	declare @vFecha7 money
	declare @vFecha8 money
	declare @vFecha9 money
	declare @vFecha10 money
	
	set @vID= (Select ISNULL (Max(ID)+1,1) From DistribucionSaldo )

	--Insertar datos	
	Begin
	
		Declare db_cursor cursor for
		Select  
		V.IDtransaccion,
		C.RazonSocial,
		V.Saldo ,
		V.FechaVencimiento
		From Cliente C
		Join venta V On C.ID=V.IDCliente
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccion,  @vRazonSocial, @vSaldo, @vFechaVencimiento
		While @@FETCH_STATUS = 0 Begin  
			
			set @vFecha1 = 0
			set @vFecha2 = 0
			set @vFecha3 = 0
			set @vFecha4 = 0
			set @vFecha5 = 0
			set @vFecha6 = 0
			set @vFecha7 = 0
			set @vFecha8 = 0
			set @vFecha9 = 0
			set @vFecha10 = 0
			set @vTotal = 0
						
			--Fecha10
			If @vFechaVencimiento  > @Fecha9  Begin
				Set @vFecha10 = @vSaldo
			End
					
			--Fecha9
			If @vFechaVencimiento <= @Fecha9 And @vFechaVencimiento > @Fecha8 Begin
				Set @vFecha9 = @vSaldo
			End
					
			--Fecha8
			If @vFechaVencimiento <= @Fecha8 And @vFechaVencimiento > @Fecha7 Begin
				Set @vFecha8 = @vSaldo
			End
			
			--Fecha7
			If @vFechaVencimiento <= @Fecha7 And @vFechaVencimiento > @Fecha6 Begin
				Set @vFecha7 = @vSaldo
			End
			
			--Fecha6
			If @vFechaVencimiento <= @Fecha6 And @vFechaVencimiento > @Fecha5 Begin
				Set @vFecha6 = @vSaldo
			End
			
			--Fecha5
			If @vFechaVencimiento <= @Fecha5 And @vFechaVencimiento > @Fecha4 Begin
				Set @vFecha5 = @vSaldo
			End
			
			--Fecha4
			If @vFechaVencimiento <= @Fecha4 And @vFechaVencimiento > @Fecha3 Begin
				Set @vFecha4 = @vSaldo
			End
			
			--Fecha3
			If @vFechaVencimiento <= @Fecha3 And @vFechaVencimiento > @Fecha2 Begin
				Set @vFecha3 = @vSaldo
			End
			
			--Fecha2
			If @vFechaVencimiento <= @Fecha2 And @vFechaVencimiento > @Fecha1 Begin
				Set @vFecha2 = @vSaldo
			End
			
			--Fecha1
			If @vFechaVencimiento <= @Fecha1 Begin
				Set @vFecha1 = @vSaldo
			End
			
			set @vTotal = @vSaldo 
			
			 Insert into DistribucionSaldo(ID,IDTransaccionVenta, RazonSocial, Fecha1, Fecha2, Fecha3, Fecha4, Fecha5, Fecha6, Fecha7, Fecha8, Fecha9, Total, Fecha10 ) 
			 Values (@vID,@vIDTransaccion, @vRazonSocial, @vFecha1, @vFecha2, @vFecha3, @vFecha4, @vFecha5, @vFecha6, @vFecha7, @vFecha8, @vFecha9, @vTotal, @vFecha10 ) 	
			
			
			Fetch Next From db_cursor Into @vIDTransaccion, @vRazonSocial, @vSaldo,@vFechaVencimiento
							
		End
		
		--Cierra el cursor
		Close db_cursor   
		Deallocate db_cursor		   			
		
	End	
	set @Mensaje = 'Sin error!'
	Set @IDTransaccionSalida = @vID
	--Insertar fechas son datos	
	
End
	




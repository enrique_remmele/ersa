﻿CREATE Procedure [dbo].[SpViewPlanillaDescuentoTactico]
	
	--Identificadores
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int
	
As

Begin

	Select 
	*
	From VPlanillaDescuentoTactico
	Where IDSucursal=@IDSucursal 
	And (GETDATE() Between Desde And Hasta)
		
End


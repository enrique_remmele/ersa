﻿CREATE Procedure [dbo].[SpDepositoBancario]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDTipoComprobante smallint = NULL,
	@Numero int = null,
	@NroComprobante varchar(50) = '',
	@Fecha date = NULL,
	@IDCuentaBancaria tinyint = NULL,
	@Cotizacion money= NULL,
	@Observacion varchar(100) = NULL,
	@TotalEfectivo money = NULL,
	@TotalChequeLocal money = NULL,
	@TotalChequeOtros money = NULL,
	@TotalDocumento money = NULL,
	@TotalTarjeta money = NULL,
	@Total money = NULL,
	@Conciliado bit = False,
	@DiferenciaCambio money = null,
		
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--INICIO SM 14072021 - Auditoria Informática
	Declare @vObservacion varchar(100)='' 

	Declare @vID int
	Declare @vIDTerminal int
	Declare @vIDUsuario smallint
	Declare @vIDOperacion tinyint
	
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from DepositoBancario where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursal, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--BLOQUES
		
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		--Numero
		Declare @vNumero int
		Set @vNumero = ISNULL((Select MAX(Numero)+1 From DepositoBancario Where IDSucursal=@IDSucursalOperacion),1)
		
		--Si es que la descripcion ya existe
		if exists(Select * From DepositoBancario   Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante and Numero=@Numero  ) begin
			set @Mensaje = 'El deposito  ya existe!'
			set @Procesado = 'False'
			return @@rowcount
		end
			
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		
		--INICIO SM 14072021 - Auditoria Informática
		set @vObservacion = @Observacion

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@IDUsuario,@IDTerminal,'INSERTAR',(Select Descripcion from Operacion where id=@IDOperacion),@vObservacion,@IDTransaccionSalida,'DEPOSITO BANCARIO' )
		--FIN SM 14072021 - Auditoria Informática
		
		--Insertar en DepositoBancario
		Insert Into DepositoBancario (IDTransaccion, IDSucursal , Numero, IDTipoComprobante, NroComprobante, Fecha , IDCuentaBancaria , Cotizacion , Observacion , TotalEfectivo , TotalChequeLocal , TotalChequeOtros , TotalDocumento, TotalTarjeta,Total,Conciliado,DiferenciaCambio)
		Values(@IDTransaccionSalida , @IDSucursalOperacion, @vNumero, @IDTipoComprobante , @NroComprobante , @Fecha, @IDCuentaBancaria ,  @Cotizacion, @Observacion, @TotalEfectivo , @TotalChequeLocal , @TotalChequeOtros,@TotalDocumento, @TotalTarjeta, @Total,@Conciliado,@DiferenciaCambio)
		
		--INICIO SM 14072021 - Auditoria Informática
		set @vID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccionSalida )
	    
		--Insertar en aDepositoBancario
		Insert Into aDepositoBancario (IDAuditoria,IDTransaccion, IDSucursal , Numero, IDTipoComprobante, NroComprobante, Fecha , IDCuentaBancaria , Cotizacion , Observacion , TotalEfectivo , TotalChequeLocal , TotalChequeOtros , TotalDocumento, TotalTarjeta,Total,Conciliado,DiferenciaCambio,IDUsuario,Accion,MofiComprobante,ModiFecha, ModiObservacion)
		Values(@vID,@IDTransaccionSalida, @IDSucursalOperacion, @vNumero, @IDTipoComprobante, @NroComprobante, @Fecha, @IDCuentaBancaria,  @Cotizacion, @Observacion, @TotalEfectivo, @TotalChequeLocal, @TotalChequeOtros, @TotalDocumento, @TotalTarjeta, @Total,@Conciliado,@DiferenciaCambio,@IDUsuario,'INS','False','False','False')
		--FIN SM 14072021 - Auditoria Informática
				
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'DEL' Begin
		
		--INICIO SM 14072021 - Auditoria Informática	
		set @vIDTerminal = (Select IDTerminal from Transaccion where ID = @IDTransaccion)
		set @vIDUsuario =  (Select IDUsuario from Transaccion where ID = @IDTransaccion)
		set @vIDOperacion = (Select IDOperacion from Transaccion where ID = @IDTransaccion)
		--FIN SM 14072021 - Auditoria Informática

		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From DepositoBancario Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End
		
		----VALIDAR
		--Que ningun cheque este rechazado
		If Exists(Select * From DetalleDepositoBancario D join ChequeCliente C on D.IDTransaccionDepositoBancario = C.IDTransaccion Where D.IDTransaccionDepositoBancario=@IDTransaccion And  C.Rechazado='True') Begin 
			set @Mensaje = 'El Cheque Esta Rechazado!'
			set @Procesado = 'False'
			return @@rowcount		
		end
		
		--Que ningun cheque este conciliado
		If Exists(Select * From DetalleDepositoBancario D join ChequeCliente C on D.IDTransaccionDepositoBancario = C.IDTransaccion where D.IDTransaccionDepositoBancario=@IDTransaccion And  C.Conciliado='True') Begin 
			set @Mensaje = 'El Cheque Está Conciliado!'
			set @Procesado = 'False'
			return @@rowcount
		end

		--Verificar que algun cheque no tenga deposito o rechazo con fecha posterior
		--Validar
		If dbo.FVerificarChequesDepositados(@IDTransaccion) = 'DEPOSITO' Begin
			Set @Mensaje  = 'No se puede Eliminar. Uno de los Cheques tiene Redeposito con fecha posterior!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
		If dbo.FVerificarChequesDepositados(@IDTransaccion) = 'RECHAZO' Begin
			Set @Mensaje  = 'No se puede Eliminar. Uno de los Cheques tiene Rechazo con fecha posterior!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End

		--Auditoria de documentos
		set @IDTipoComprobante = (select IDTipoComprobante from DepositoBancario where IDTransaccion = @IDTransaccion)
		set @Fecha = (select Fecha from DepositoBancario where IDTransaccion = @IDTransaccion)
		set @IDSucursal = (select IDSucursal from DepositoBancario where IDTransaccion = @IDTransaccion)
		declare @Comprobante as varchar(32) = (select NroComprobante from DepositoBancario where IDTransaccion = @IDTransaccion)
		declare @VTipoComprobante varchar(16) = (select codigo from TipoComprobante where ID =  @IDTipoComprobante)
		set @Total = (select Total from DepositoBancario where IDTransaccion = @IDTransaccion)
		Declare @vIDCuentaBancaria int = (select IDCuentaBancaria from DepositoBancario where IDTransaccion = @IDTransaccion)
		declare @Condicion varchar(16) = 'DEPOSITO'
		Declare @vNroComprobante varchar(50) = (select NroComprobante from DepositoBancario where IDTransaccion = @IDTransaccion)
		Declare @vCotizacion money = (select Cotizacion from DepositoBancario where IDTransaccion = @IDTransaccion)
		Declare @vTotalEfectivo money = (select TotalEfectivo from DepositoBancario where IDTransaccion = @IDTransaccion)
		Declare @vTotalChequeLocal money = (select TotalChequeLocal from DepositoBancario where IDTransaccion = @IDTransaccion)
		Declare @vTotalChequeOtros money = (select TotalChequeOtros from DepositoBancario where IDTransaccion = @IDTransaccion)
		Declare @vTotalDocumento money = (select TotalDocumento from DepositoBancario where IDTransaccion = @IDTransaccion)
		Declare @vTotalTarjeta money = (select TotalTarjeta from DepositoBancario where IDTransaccion = @IDTransaccion)
		Declare @vConciliado money = (select Conciliado from DepositoBancario where IDTransaccion = @IDTransaccion)
		Declare @vDiferenciaCambio money = (select DiferenciaCambio from DepositoBancario where IDTransaccion = @IDTransaccion)

		--INICIO SM 14072021 - Auditoria Informática
		set @vObservacion = concat('Se Eliminara el Deposito Bancario. Comprobante:',@Comprobante, ' - IDCuentaBancaria:',@vIDCuentaBancaria)

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@IDTransaccion,'DEPOSITO BANCARIO' )
		
		--set @vID = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion)
		--FIN SM 14072021 - Auditoria Informática

		Insert into AuditoriaDocumentos	values(@IDTipoComprobante,0,@vIDCuentaBancaria,@Fecha,@Comprobante,@Total,@Condicion, getdate(),@IDUsuario,'ELI',@VTipoComprobante, @IDSucursal)
				
		------Reestablecer el Efectivo,Cheque y Documento y Eliminar registros
		Exec SpDepositoBancarioProcesar @IDTransaccion = @IDTransaccion,
										    @Operacion = @Operacion, 
										    @Mensaje = @Mensaje OUTPUT,
										    @Procesado = @Procesado OUTPUT

		--INICIO SM 16072021 - Auditoria Informática
		set @vObservacion = 'Se Eliminara el Detalle Deposito Bancario. '

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@IDTransaccion,'DETALLE DEPOSITO BANCARIO' )
		
		Declare @vidauditoria int = (select max(id) from AuditoriaTI where IDTransaccionOperacion = @IDTransaccion)
		Declare @vvID int
		Declare @vvIDTransaccionEfectivo numeric(18,0)
		Declare @vvIDEfectivo smallint
		Declare @vvIDTransaccionCheque numeric(18,0)
		Declare @vvIDTransaccionDocumento numeric(18,0)
		Declare @vvRedondeo bit
		Declare @vvFaltante bit
		Declare @vvSobrante bit
		Declare @vvImporte money
		Declare @vvIDTransaccionTarjeta numeric(18,0)
		Declare @vvIDDetalleDocumento int
		
		Begin
			Declare db_cursor cursor for
			
			Select ID, IDTransaccionEfectivo,IDEfectivo, IDTransaccionCheque, IDTransaccionDocumento,Redondeo,Faltante,Sobrante,Importe,IDTransaccionTarjeta, IDDetalleDocumento
			From DetalleDepositoBancario  
			Where IDTransaccionDepositoBancario=@IDTransaccion
			Open db_cursor 
			Fetch Next From db_cursor Into @vvID, @vvIDTransaccionEfectivo,@vvIDEfectivo, @vvIDTransaccionCheque, @vvIDTransaccionDocumento,@vvRedondeo,@vvFaltante,@vvSobrante,@vvImporte,@vvIDTransaccionTarjeta, @vvIDDetalleDocumento
		
			While @@FETCH_STATUS = 0 Begin 
		
				Insert into aDetalleDepositoBancario(IDAuditoria,IDTransaccionDepositoBancario,ID,IDTransaccionefectivo,IDEfectivo,IDTransaccionCheque,IDTransaccionDocumento,Redondeo,Faltante,Sobrante,Importe,IDTransaccionTarjeta,IDDetalleDocumento,IDUsuario,Accion)
                   values (@vidauditoria,@IDTransaccion, @vvID, @vvIDTransaccionEfectivo,@vvIDEfectivo, @vvIDTransaccionCheque, @vvIDTransaccionDocumento,@vvRedondeo,@vvFaltante,@vvSobrante,@vvImporte,@vvIDTransaccionTarjeta, @vvIDDetalleDocumento,@vIDUsuario,'ELI')
		 
				Fetch Next From db_cursor Into @vvID, @vvIDTransaccionEfectivo,@vvIDEfectivo, @vvIDTransaccionCheque, @vvIDTransaccionDocumento,@vvRedondeo,@vvFaltante,@vvSobrante,@vvImporte,@vvIDTransaccionTarjeta, @vvIDDetalleDocumento
		
            End
            Close db_cursor   
			Deallocate db_cursor		   			
		End
		--FIN SM 16072021 - Auditoria Informática

		--Eliminamos el registro
		Delete DepositoBancario  Where IDTransaccion = @IDTransaccion

	    --Eliminamos el detalle
		Delete DetalleDepositoBancario  where IDTransaccionDepositoBancario = @IDTransaccion				
		
		--INICIO SM 15072021 - Auditoria Informática
		set @vObservacion = 'Se Eliminara el Detalle de Asiento. '

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@IDTransaccion,'DETALLE ASIENTO' )
		
		set @vObservacion = 'Se Eliminara el Asiento. '

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@IDTransaccion,'ASIENTO' )
		
		set @vObservacion = 'Se Eliminara la Transaccion. '

		Insert into AuditoriaTI(id,fecha,idusuario,idterminal,evento,operacion,observacion,IDTransaccionOperacion,Tabla)
		Values ((select ISNULL(max(id)+1,1) from AuditoriaTI),getdate(),@vIDUsuario,@vIDTerminal,'ELIMINAR',(Select Descripcion from Operacion where id=@vIDOperacion),@vObservacion,@IDTransaccion,'TRANSACCION' )
		
		--FIN SM 15072021 - Auditoria Informática
					
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion				
				
		Set @Mensaje = 'Registro guardado!'
		Set @Procesado = 'True'
		print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End





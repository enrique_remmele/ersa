﻿CREATE Procedure [dbo].[SpTicketBascula]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@Numero int = NULL,
	@Fecha date = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante varchar(50) = NULL,
	@IDProveedor int = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money = 0,
	@IDProducto int = NULL,
	@IDImpuesto tinyint = NULL,
	@PesoRemision money = 0,
	@PesoBascula money = 0,
	@Diferencia Money = 0,
	@PrecioUnitario Money = 0,
	@PrecioUnitarioUS Money = 0,
	@Total Money = 0,
	@TotalUS Money = 0,
	@TotalImpuesto Money = 0,
	@TotalImpuestoUS Money = 0,
	@TotalDiscriminado Money = 0,
	@TotalDiscriminadoUS Money = 0,
	@CostoAdicional Money = 0,
	@CostoAdicionalUS Money = 0,
	@TipoFlete varchar(16) = NULL,
	@IDCamion int = NULL,
	@IDChofer int = NULL,
	@Observacion varchar(200) = NULL,
	--@IDTransaccionGasto numeric(18,0) = NULL,
	@NroAcuerdo int = NULL,
	@NroRemision varchar(16) = null,
	@NroRecepcion varchar(16) = null,
	@Operacion varchar(50),	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint = NULL,
	@IDDeposito tinyint = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDDepositoOperacion tinyint = NULL,
	@IDTerminal int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin	

	--Variables Kardex
	Declare @vFecha datetime
	Declare @vIDSucursal integer
	Declare @vNroComprobante varchar(50)
	Declare @vCostoEntrada decimal(10,2)
	Declare @vTotalDiscriminado decimal(10,2)

	Set @vFecha = (Select Fecha From TicketBascula Where IDTransaccion = @IDTransaccion)
	Set @vIDSucursal = (Select IDSucursal From Deposito Where ID = @IDDeposito)
	Set @vNroComprobante = (Select NroComprobante From TicketBascula Where IDTransaccion = @IDTransaccion)
	
	if @Operacion <> 'DESASOCIAR' begin
		Set @vCostoEntrada = IsNull(@TotalDiscriminado/@PesoBascula,0) 
	end
	



	if @IDSucursalOperacion is null begin
		Set @IDSucursalOperacion = @IDSucursal
		Set @IDDepositoOperacion = @IDDeposito
	end
	--Validar Feha Operacion
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If dbo.FValidarFechaOperacion(@IDSucursalOperacion, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 


	IF @Operacion = 'DEL' Begin
		set @Fecha = (Select Fecha from TicketBascula where IDTransaccion = @IDTransaccion)
		If dbo.FValidarFechaOperacion(@IDSucursalOperacion, @Fecha, @IDUsuario, @IDOperacion) = 'False' Begin
			Set @Mensaje  = 'Fecha fuera de rango permitido!!'
			Set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			Return @@rowcount
		End
	End 

	--Fecha de la compra que no sea mayor a hoy
	IF @Operacion = 'INS' or @Operacion = 'UPD' Begin
		If @Fecha > getdate() Begin
				set @Mensaje = 'La fecha del comprobante no debe ser mayor a hoy!'
				set @Procesado = 'False'
				set @IDTransaccionSalida  = 0
				return @@rowcount
		End
	END

	--BLOQUES
	SET @Numero = REPLACE(@Numero,'.','')
	
	--INSERTAR
	If @Operacion = 'INS' Begin
	
		--Validar
		
		--Fecha
		
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursalOperacion,
			@IDDeposito = @IDDepositoOperacion,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		------Insertar

		--Validar Numero de operacion
		If Exists(Select * From VTicketBascula Where Numero=@Numero AND IDSucursal = @IDSucursalOperacion) Begin
			set @Mensaje = 'El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		If Exists(Select * From Acuerdo Where NroAcuerdo=@NroAcuerdo and Finalizado = 'True') Begin
			set @Mensaje = 'El Acuerdo asociado al Ticket de bascula ya esta finalizado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		Insert Into TicketBascula
		Values(@IDTransaccionSalida,@IDSucursal,@Numero,@Fecha,@IDTipoComprobante,@NroComprobante,@IDProveedor,@IDMoneda,
		@Cotizacion,@IDDeposito,@IDProducto,@IDImpuesto,@PesoRemision,@PesoBascula,@Diferencia,0,
		0,0,0,0,0,0,0,0,0,'',@IDCamion,@IDChofer,@Observacion,'False',0,0,null,null,'False',@NroRemision,@NroRecepcion)

		--if @IDDeposito Is Not NULL Begin
		--	EXEC SpActualizarProductoDeposito
		--		@IDDeposito = @IDDeposito,
		--		@IDProducto = @IDProducto,
		--		@Cantidad = @PesoBascula,
		--		@Signo = N'+',
		--		@Mensaje = @Mensaje OUTPUT,
		--		@Procesado = @Procesado OUTPUT
		--End
				
		set @Mensaje = 'Registro guardado'
		set @Procesado = 'True'
		return @@rowcount
			
	End
	
	If @Operacion = 'ANULAR' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From TicketBascula Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--IDTransaccion
		If Exists(Select * From TicketBascula Where IDTransaccion=@IDTransaccion and NroAcuerdo > 0) Begin
			set @Mensaje = 'El ticket esta asociado a un acuerdo. Desasocie para seguir.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que no este anulado
		if (Select Anulado From TicketBascula Where IDTransaccion=@IDTransaccion) = 'True' Begin
			set @Mensaje = 'El registro ya esta anulado!'
			set @Procesado = 'False'
			return @@rowcount
		End	
		
		--Verificar que no este anulado
		if Exists(Select * From DetalleMacheo Where IDTransaccionTicket=@IDTransaccion) Begin
			set @Mensaje = 'El Ticket ya esta Asociado en un macheo!'
			set @Procesado = 'False'
			return @@rowcount
		End		 	
		
		--Actualizamos el Stock
		--Exec SpDetalleMovimiento @IDTransaccion=@IDTransaccion, @Operacion='ANULAR', @Mensaje=@Mensaje output, @Procesado=@Procesado output
		
		--Salimos si actualizar stock no fue satisfactorio
		--If @Procesado = 0 Begin
		--	print 'El stock no se actualizo correctamente. [SpTicketBascula] Bloque: ANULAR '
		--	return @@rowcount
		--End
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursalOperacion,@IDTerminal=@IDTerminal
				
		--Anulamos el registro
		Update TicketBascula 
		Set Anulado = 'True',
		Procesado = 'False',
		IDUsuarioAnulacion = @IDUsuario,
		FechaAnulacion = GETDATE()
		Where IDTransaccion = @IDTransaccion
		
		print '[SpTicketBascula] ANULADO'
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	If @Operacion = 'DEL' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From TicketBascula Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que no este anulado
		if Exists(Select * From DetalleMacheo Where IDTransaccionTicket=@IDTransaccion) Begin
			set @Mensaje = 'El Ticket ya esta Asociado en un macheo!'
			set @Procesado = 'False'
			return @@rowcount
		End	

		--Verificar que no este anulado
					
		--Actualizamos el Stock
		
		--Insertamos el registro de anulacion
		
		--Anulamos el registro
		
	End

	If @Operacion = 'UPD' Begin
	 Update TicketBascula 
	 set IDTipoComprobante = @IDTipoComprobante,
	 Numero = @Numero,
	 NroComprobante = @NroComprobante,
	 Observacion = @Observacion,
	 NroRemision = @NroRemision,
	 NroRecepcion = @NroRecepcion,
	 TipoFlete = @TipoFlete
	 where IDTransaccion = @IDTransaccion

	 set @Mensaje = 'Registro Modificado'
		set @Procesado = 'True'
		return @@rowcount
	End

	If @Operacion = 'ASIGNAR' Begin
	
		--Validar
		If Exists(Select * From Acuerdo Where NroAcuerdo=@NroAcuerdo and Finalizado = 'True') Begin
			set @Mensaje = 'El Acuerdo asociado al Ticket de bascula ya esta finalizado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Eliminar asientos asociados
		delete from DetalleAsiento where idtransaccion = @IDTransaccion
		delete from Asiento where idtransaccion = @IDTransaccion

		Update TicketBascula
		set NroAcuerdo = @NroAcuerdo,
		PrecioUnitario = @PrecioUnitario,
		PrecioUnitarioUS = @PrecioUnitarioUS,
		Total = @Total,
		TotalUs = @TotalUS,
		TotalImpuesto = @TotalImpuesto,
		TotalImpuestoUs = @TotalImpuestoUS,
		TotalDiscriminado = @TotalDiscriminado,
		TotalDiscriminadoUS = @TotalDiscriminadoUS,
		CostoAdicional =  @CostoAdicional, 
		CostoAdicionalUS = @CostoAdicionalUS,
		TipoFlete = @TipoFlete,
		Procesado = 'True'
		where IDTransaccion = @IDTransaccion

		if @IDDeposito Is Not NULL Begin
			EXEC SpActualizarProductoDeposito
				@IDDeposito = @IDDeposito,
				@IDProducto = @IDProducto,
				@Cantidad = @PesoBascula,
				@Signo = N'+',
				@Mensaje = @Mensaje OUTPUT,
				@Procesado = @Procesado OUTPUT
		End
		Exec SpActualizarStockProductoCalculado  
						@IDProducto = @IDProducto, 
						@IDDeposito = @IDDeposito, 
						@IDtransaccion = @IDTransaccion, 
						@IDUsuarioOperacion = @IDUsuario,
						@IDSucursalOperacion = @IDSucursalOperacion,
						@IDDepositoOperacion = @IDDepositoOperacion,
						@IDTerminalOperacion = @IDTerminal,
						@Mensaje = @Mensaje OUTPUT,
						@Procesado = @Procesado OUTPUT

		--Actualizar el Costo del Producto
		Declare @vCostoUnitarioAdicional as money = 0
		--Verificar IVA
		Set @PrecioUnitario = @TotalDiscriminado/@PesoBascula
		set @vCostoUnitarioAdicional = (select Isnull(sum(ImporteSinIVA),0) from TicketBasculaGastoAdicional where IDTransaccion = @IDTransaccion) / @PesoBascula
		set @PrecioUnitario = @PrecioUnitario+ @vCostoUnitarioAdicional

		--Actualiza el Kardex			
		EXEC SpKardex
			@Fecha = @vFecha,
			@IDTransaccion = @IDTransaccion,
			@IDProducto = @IDProducto,
			@ID = 1,
			@Orden = 1,
			@CantidadEntrada = @PesoBascula,
			@CantidadSalida = 0,
			@CostoEntrada = @PrecioUnitario,
			@CostoSalida = 0,
			@IDSucursal = @vIDSucursal,
			@Comprobante = @vNroComprobante	

		--EXEC SpActualizarCostoProducto @ID = @IDProducto, @Costo = @PrecioUnitario, @IDImpuesto = @IDImpuesto, @IDTransaccion = @IDTransaccion, @Operacion = 'INSTICKET'
		EXEC SpActualizarCostoProducto @ID = @IDProducto, @Costo = @PrecioUnitario, @IDImpuesto = @IDImpuesto, @IDTransaccion = @IDTransaccion, @Operacion = 'INS'
		
		exec spasientoTicketBascula @IDTransaccion=	@IDTransaccion

		set @Mensaje = 'Registro modificado'
		set @Procesado = 'True'
		set @IDTransaccionSalida = @IDTransaccion
		return @@rowcount
			
	End

	If @Operacion = 'DESASOCIAR' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From TicketBascula Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = 'El sistema no encontro el registro solicitado.'
			set @Procesado = 'False'
			return @@rowcount
		End

		--Verificar que no este anulado
		if (Select Anulado From TicketBascula Where IDTransaccion=@IDTransaccion) = 'True' Begin
			set @Mensaje = 'El registro ya esta anulado!'
			set @Procesado = 'False'
			return @@rowcount
		End	
		
		--Verificar que no este anulado
		if Exists(Select * From DetalleMacheo Where IDTransaccionTicket=@IDTransaccion) Begin
			set @Mensaje = 'El Ticket ya esta Asociado en un macheo!'
			set @Procesado = 'False'
			return @@rowcount
		End		

		--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
		Delete from TicketBasculaGastoAdicional where IDTransaccion = @IDTransaccion

		set @IDDeposito = (Select IDDeposito from TicketBascula where IdTransaccion = @IDTransaccion)
		set @IDProducto = (Select IDProducto from TicketBascula where IdTransaccion = @IDTransaccion)
		set @PesoBascula = (Select PesoBascula from TicketBascula where IdTransaccion = @IDTransaccion)

		EXEC SpActualizarProductoDeposito
							@IDDeposito = @IDDeposito,
							@IDProducto = @IDProducto,
							@Cantidad = @PesoBascula,
							@Signo = N'-',
							@ControlStock  = 'False',
							@Mensaje = @Mensaje OUTPUT,
							@Procesado = @Procesado OUTPUT

		--Actualizar el Costo del Producto
		set @vCostoUnitarioAdicional = (select Isnull(sum(ImporteGs),0) from TicketBasculaGastoAdicional where IDTransaccion = @IDTransaccion) / @PesoBascula
		set @PrecioUnitario = @PrecioUnitario + @vCostoUnitarioAdicional
--		EXEC SpActualizarCostoProducto @ID = @IDProducto, @Costo = @PrecioUnitario, @IDImpuesto = @IDImpuesto, @IDTransaccion = @IDTransaccion, @Operacion = 'DELTICKET'
		EXEC SpActualizarCostoProducto @ID = @IDProducto, @Costo = @PrecioUnitario, @IDImpuesto = @IDImpuesto, @IDTransaccion = @IDTransaccion, @Operacion = 'DEL'
		
		--Anulamos el registro
		Update TicketBascula
		set NroAcuerdo = 0,
		PrecioUnitario = 0,
		PrecioUnitarioUS = 0,
		Total = 0,
		TotalUs = 0,
		TotalImpuesto = 0,
		TotalImpuestoUs = 0,
		TotalDiscriminado = 0,
		TotalDiscriminadoUS = 0,
		CostoAdicional =  0, 
		CostoAdicionalUS = 0,
		TipoFlete = '',
		Procesado = 0
		where IDTransaccion = @IDTransaccion
		
		--Eliminar el registro del kardex
		Delete From Kardex Where IDTransaccion=@IDTransaccion And IDProducto=@IDProducto

		--Recalcular kardex
		EXEC SpRecalcularKardexProducto @IDProducto = @IDProducto, @FechaInicio=@vFecha

		--Actualizar el Costo del Producto
		EXEC SpActualizarCostoProducto @ID = @IDProducto, @Costo = @PrecioUnitario, @IDImpuesto = @IDImpuesto, @IDTransaccion = @IDTransaccion, @Operacion = 'DEL'
		

		--Eliminamos los asientos
		delete from detalleasiento where IDTransaccion = @IDTransaccion
		delete from asiento where IDTransaccion = @IDTransaccion

		print '[SpTicketBascula] DESASOCIADO'
		
		set @Mensaje = 'Registro guardado!'
		set @Procesado = 'True'
		return @@rowcount
		
	End	
	
	set @Mensaje = 'No se proceso ninguna transaccion!'
	set @Procesado = 'False'
	set @IDTransaccionSalida  = '0'
	return @@rowcount
		
End


﻿Imports System.Net
Imports System
Imports System.IO
Imports System.Diagnostics
Imports System.Xml
Imports System.Xml.XPath
Imports System.Configuration.ConfigurationManager
Imports System.Data.OleDb
Imports System.Data.Odbc

Public Class frmPrincipal
    'Clases
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim Consultando As Boolean = False
    Dim Pausa As Boolean = False
    Dim dtAnterior As DataTable

    Sub Inicializar()

        lblHoraActual.Text = Date.Now.ToLongTimeString
        CrearCarpetaTemporal()
        txtServidor.Text = CArchivoInicio.IniGet(VGPATHINI, "CONFIGURACION", "SERVIDORFTP", "")
        txtServidorBD.Text = CArchivoInicio.IniGet(VGPATHINI, "CONFIGURACION", "SERVIDORBD", ".\SQLExpress")
        txtUsuario.Text = CArchivoInicio.IniGet(VGPATHINI, "CONFIGURACION", "USUARIO", "")
        txtPassword.Text = CArchivoInicio.IniGet(VGPATHINI, "CONFIGURACION", "PASSWORD", "")
        VGCadenaConexion = "Data Source=" & txtServidorBD.Text & ";Initial Catalog=SAIN1;User Id=sa;Password=Alimentos931"
    End Sub

    Sub CrearCarpetaTemporal()
        If IO.Directory.Exists(My.Application.Info.DirectoryPath & "\Temporal") = False Then
            'Crear
            IO.Directory.CreateDirectory(My.Application.Info.DirectoryPath & "\Temporal")


        End If
    End Sub

    Sub GuardarConfiguracion()

        CArchivoInicio.IniWrite(VGPATHINI, "CONFIGURACION", "SERVIDORFTP", txtServidor.Text)
        CArchivoInicio.IniWrite(VGPATHINI, "CONFIGURACION", "SERVIDORBD", txtServidorBD.Text)
        CArchivoInicio.IniWrite(VGPATHINI, "CONFIGURACION", "USUARIO", txtUsuario.Text)
        CArchivoInicio.IniWrite(VGPATHINI, "CONFIGURACION", "PASSWORD", txtPassword.Text)

    End Sub

    Sub Consultar()
        Consultando = True
        Dim sBase As String = txtFic.Text
        Dim sSelect As String = "SELECT * FROM er_laboratorio where year(fecha)=2018 "
        Dim sConn As String
        Dim columnas As String = ""

        '  sConn = "Driver={Microsoft dBase Driver (*.dbf)};DriverID=277;Dbq=C:\er_vfpwork\er_laboratorio.DBF"

        Try


            sConn = "Provider=vfpoledb.1;Data Source=C:\er_vfpwork\er_laboratorio.DBF;Collating Sequence=general;"
            Dim dbConn As New System.Data.OleDb.OleDbConnection(sConn)

            dbConn.Open()

            Dim Adapter As New System.Data.OleDb.OleDbDataAdapter(sSelect, dbConn)
            Dim dt As New DataTable

            Adapter.Fill(dt)

            dbConn.Close()

            For Each col As DataColumn In dt.Columns
                columnas = columnas & col.ColumnName & "(" & col.DataType.ToString & "), "
            Next

            dtAnterior = dt.Copy

        Catch ex As Exception
            MessageBox.Show("Error al abrir la base de datos" & vbCrLf & ex.Message)
            Pausa = True
            Button2.Text = "Reanudar"
            Exit Sub

        End Try


        'Using dbConn As New System.Data.Odbc.OdbcConnection(sConn)
        '    Try
        '        dbConn.Open()

        '        Dim da As New System.Data.Odbc.OdbcDataAdapter(sSelect, dbConn)
        '        Dim dt As New DataTable

        '        da.Fill(dt)

        '        dbConn.Close()

        '        dtAnterior.Clear()
        '        dtAnterior = dt.Clone()
        '    Catch ex As Exception
        '        MessageBox.Show("Error al abrir la base de datos" & vbCrLf & ex.Message)
        '        Pausa = True
        '        Button2.Text = "Reanudar"
        '        Exit Sub
        '    End Try
        'End Using

    End Sub




    Private Sub frmPrincipal_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As System.EventArgs) Handles Timer1.Tick
        lblHoraActual.Text = "Hora Actual:" & Date.Now.ToLongTimeString
        If Consultando = False And Pausa = False Then
            Consultar()
            Consultando = False
        End If
    End Sub

    Private Sub btnAplicar_Click(sender As Object, e As System.EventArgs)
        GuardarConfiguracion()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim oFD As New OpenFileDialog
        With oFD
            .Filter = "Ficheros DBF (*.dbf)|*.dbf|Todos (*.*)|*.*"
            If .ShowDialog = DialogResult.OK Then
                txtFic.Text = .FileName
                ' El nombre del fichero
                txtSelect.Text = System.IO.Path.GetFileNameWithoutExtension(txtFic.Text)
            End If
        End With
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Pausa Then
            Button2.Text = "Pausar"
            Pausa = False
        Else
            Button2.Text = "Reanudar"
            Pausa = True
        End If
    End Sub
End Class

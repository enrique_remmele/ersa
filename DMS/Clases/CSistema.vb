﻿Imports System.Data.SqlClient

Public Class CSistema
    Inherits CError
#Region "CONEXION CON BASE DE DATOS"

    ''' <summary>
    ''' Retorna un DataTable segun la consulta pasada por parametro
    ''' </summary>
    ''' <param name="consulta">Cualquier consulta tipo TSQL</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExecuteToDataTable(ByVal consulta As String, Optional ByVal vConexion As String = "", Optional ByVal Timeout As Integer = 30) As DataTable

        ExecuteToDataTable = Nothing

        Try
            If vConexion <> "" Then
                VGCadenaConexion = vConexion
            End If

            Using Adapter As New SqlDataAdapter(consulta, VGCadenaConexion)
                Dim dt As New DataTable
                'Adapter.ContinueUpdateOnError = True
                Adapter.SelectCommand.CommandTimeout = Timeout
                Adapter.Fill(dt)
                Adapter.SelectCommand.Connection.Close()
                Adapter.Dispose()
                Return (dt)
            End Using

        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteToDataTable", "", consulta)
            Return Nothing

        End Try


    End Function

    Public Function ExecuteToDataTableXLS(ByVal consulta As String, ByVal Archivo As String, Optional ByVal vConexion As String = "") As DataTable

        ExecuteToDataTableXLS = Nothing
        Try
            If vConexion <> "" Then
                VGCadenaConexion = vConexion
            End If

            Dim MiConexion As New System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Archivo & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'")
            Dim MiAdaptador As New System.Data.OleDb.OleDbDataAdapter(consulta, MiConexion)
            Dim MiDataSet As New DataSet()
            Dim MiEnlazador As New BindingSource

            Dim commandbuilder As New OleDb.OleDbCommandBuilder(MiAdaptador)
            MiConexion.Open()
            MiAdaptador.Fill(MiDataSet)
            Return MiDataSet.Tables(0)

        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteToDataTable", "", consulta)
            Return Nothing
        End Try


    End Function

    Public Function ExecuteToDataTableTXT(ByVal path As String, Optional ByVal vConexion As String = "") As DataTable
        ExecuteToDataTableTXT = Nothing

        Try

            If vConexion <> "" Then
                VGCadenaConexion = vConexion
            End If

            Dim dt As New DataTable
            Dim myStream As System.IO.StreamReader = New System.IO.StreamReader(path)
            Dim Linea As String
            Dim Separador As String = vbTab
            Dim Cabecera As Boolean = True
            Do
                Linea = myStream.ReadLine()
                If Linea Is Nothing Then
                    Exit Do
                End If

                Dim Vector As String() = Split(Linea, Separador)

                If Cabecera = True Then

                    For i As Integer = 0 To Vector.GetLength(0) - 1
                        dt.Columns.Add(Vector(i).ToString)
                    Next

                    Cabecera = False

                Else

                    'Cargamos los registros
                    Dim NewRow As DataRow = dt.NewRow
                    For i As Integer = 0 To Vector.GetLength(0) - 1
                        NewRow(i) = Vector(i).ToString
                    Next

                    dt.Rows.Add(NewRow)

                End If

            Loop

            myStream.Close()

            Return dt

        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteToDataTableTXT", "", path)
            Return Nothing
        End Try



    End Function

    ''' <summary>
    ''' Sirve para extraer alguna informacion especifica de la base de datos. No confundir con ExecuteReader
    ''' </summary>
    ''' <param name="Consulta">Consulta TSQL, debe ser especifica, o sea, se debe solicitar un unico valor.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExecuteScalar(ByVal Consulta As String, Optional ByVal vConexion As String = "", Optional ByVal Timeout As Integer = 10) As Object

        ExecuteScalar = Nothing
        Dim cadena As String = Nothing
        If vConexion <> "" Then
            VGCadenaConexion = vConexion
        End If
        cadena = VGCadenaConexion
        If Not cadena.Contains("Timeout") Then
            cadena = cadena & ";Connection Timeout=" & Timeout.ToString
        End If

        Dim conn As New SqlConnection(cadena)
        Dim cmd As New SqlCommand(Consulta, conn)
        Dim result As Object
        Dim previousConnectionState As ConnectionState = conn.State
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            result = cmd.ExecuteScalar()
            Return result
        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteScalar", "", Consulta)
        Finally
            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
        End Try

    End Function

    Public Function ExecuteNonQuery(ByVal Consulta As String, Optional ByVal vConexion As String = "") As Integer

        ExecuteNonQuery = 0

        If vConexion <> "" Then
            VGCadenaConexion = vConexion
        End If

        Dim conn As New SqlConnection(VGCadenaConexion)
        Dim cmd As New SqlCommand(Consulta, conn)
        Dim result As Integer
        Dim previousConnectionState As ConnectionState = conn.State
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            result = cmd.ExecuteNonQuery
            Return result
        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteNonQuery", "", Consulta)
            Debug.Print(ex.Message)
        Finally
            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
        End Try

    End Function

    Public Function CerrarConexion(Optional ByVal vConexion As String = "") As Boolean

        CerrarConexion = 0

        If vConexion <> "" Then
            VGCadenaConexion = vConexion
        End If

        Dim conn As New SqlConnection(VGCadenaConexion)
        Dim previousConnectionState As ConnectionState = conn.State
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            Return True
        Catch ex As Exception
            CargarError(ex, "CSistema", "CerrarConexion", "", "")
        Finally
            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
            SqlConnection.ClearAllPools()
        End Try

    End Function

    Sub ConcatenarParametro(ByRef SQL As String, ByVal Campo As String, ByVal Valor As String, Optional ByVal AgregarComa As Boolean = False)

        'Buscamos si existe una "," coma
        If SQL.IndexOf("@") > 0 Then
            SQL = SQL & ", " & Campo & "='" & Valor & "' "
        Else
            SQL = SQL & " " & Campo & "='" & Valor & "' "
        End If


        If AgregarComa = True Then
            SQL = SQL & ","
        End If

    End Sub

#End Region

    Function FormatDobleDigito(ByVal value As String) As String

        FormatDobleDigito = "00"

        If IsNumeric(value) = False Then
            Exit Function
        End If

        If CInt(value) < 10 Then
            FormatDobleDigito = "0" & value
        Else
            FormatDobleDigito = value
        End If

    End Function

    Function FormatoFechaDesdeHastaBaseDatos(ByVal value As Date, Optional ByVal Desde As Boolean = True, Optional ByVal Hasta As Boolean = False) As String

        Dim Retorno As String = ""

        Retorno = value.Day & "-" & value.Month & "-" & value.Year
       
        If Desde = True Then
            Retorno = Retorno & " 00:00:00.000"
        End If

        If Hasta = True Then
            Retorno = Retorno & " 23:59:59.999"
        End If

        Return Retorno

    End Function


End Class

